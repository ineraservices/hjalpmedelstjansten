package se.inera.hjalpmedelstjansten.business.logging.view;

import jakarta.ejb.ConcurrencyManagement;
import jakarta.ejb.ConcurrencyManagementType;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.enterprise.inject.Produces;
import jakarta.enterprise.inject.spi.InjectionPoint;
import org.slf4j.LoggerFactory;

@Singleton
@Startup
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class LoggerService {

    @Produces
    public HjmtLogger getLogger(InjectionPoint ip) {
        final var aClass = ip.getMember().getDeclaringClass();
        final var systemLogger = LoggerFactory.getLogger(aClass.getName());
        final var performanceLogger = LoggerFactory.getLogger("se.inera.hjalpmedelstjansten.performance");
        final var auditLogger = LoggerFactory.getLogger("se.inera.hjalpmedelstjansten.audit");
        return new StandardLogger(systemLogger, performanceLogger, auditLogger);
    }

    public static HjmtLogger getLogger(String className) {
        final var logger = LoggerFactory.getLogger(className);
        final var performanceLogger = LoggerFactory.getLogger("se.inera.hjalpmedelstjansten.performance");
        final var auditLogger = LoggerFactory.getLogger("se.inera.hjalpmedelstjansten.audit");
        return new StandardLogger(logger, performanceLogger, auditLogger);
    }
}
