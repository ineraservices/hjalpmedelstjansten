package se.inera.hjalpmedelstjansten.business.logging.view;

import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_ACTION;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_CATEGORY;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_CATEGORY_PROCESS;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_CLASS;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_DURATION;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_ID_PATTERN;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_METHOD;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_TYPE;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_TYPE_ACCESS;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_TYPE_CHANGE;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_TYPE_CREATION;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_TYPE_DELETION;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_TYPE_INFO;

import java.util.Map;
import org.slf4j.MDC;

public class MdcToolkit {

    private static final String DID_CREATE_ACTION = "create";
    private static final String DID_UPDATE_ACTION = "update";
    private static final String DID_DELETE_ACTION = "delete";
    private static final String DID_LOGIN_ACTION = "login";
    private static final String DID_LOGOUT_ACTION = "logout";

    private static final Map<String, String> ACTION_EVENT_TYPE_MAP = Map.of(
        DID_CREATE_ACTION, MDC_EVENT_TYPE_CREATION,
        DID_UPDATE_ACTION, MDC_EVENT_TYPE_CHANGE,
        DID_DELETE_ACTION, MDC_EVENT_TYPE_DELETION,
        DID_LOGIN_ACTION, MDC_EVENT_TYPE_INFO,
        DID_LOGOUT_ACTION, MDC_EVENT_TYPE_INFO
    );

    private MdcToolkit() {
    }

    public static void decoratePerformance(String className, String methodName, long duration) {
        MDC.put(MDC_EVENT_CATEGORY, MDC_EVENT_CATEGORY_PROCESS);
        MDC.put(MDC_EVENT_TYPE, MDC_EVENT_TYPE_INFO);
        MDC.put(MDC_EVENT_CLASS, className);
        MDC.put(MDC_EVENT_METHOD, methodName);
        MDC.put(MDC_EVENT_DURATION, String.valueOf(duration));
    }

    public static void clearPerformance() {
        MDC.remove(MDC_EVENT_CATEGORY);
        MDC.remove(MDC_EVENT_TYPE);
        MDC.remove(MDC_EVENT_CLASS);
        MDC.remove(MDC_EVENT_METHOD);
        MDC.remove(MDC_EVENT_DURATION);
    }

    public static void decorateAudit(String did, String toType, String toId) {
        final var eventAction = eventAction(did, toType);
        if (eventAction != null) {
            MDC.put(MDC_EVENT_ACTION, eventAction);
        }
        MDC.put(MDC_EVENT_CATEGORY, MDC_EVENT_CATEGORY_PROCESS);
        if (did == null) {
            MDC.put(MDC_EVENT_TYPE, MDC_EVENT_TYPE_ACCESS);
        } else {
            MDC.put(MDC_EVENT_TYPE, ACTION_EVENT_TYPE_MAP.getOrDefault(did, MDC_EVENT_TYPE_ACCESS));
        }
        if (useEventIdKey(toType, toId)) {
            MDC.put(eventId(toType), toId);
        }
    }

    public static void clearAudit(String toType, String toId) {
        MDC.remove(MDC_EVENT_ACTION);
        MDC.remove(MDC_EVENT_CATEGORY);
        MDC.remove(MDC_EVENT_TYPE);
        if (useEventIdKey(toType, toId)) {
            MDC.remove(eventId(toType));
        }
    }

    public static Map<String, String> copyOfMdc() {
        return MDC.getCopyOfContextMap();
    }

    public static void setMdc(Map<String, String> mdc) {
        MDC.setContextMap(mdc);
    }

    public static void clearMdc() {
        MDC.clear();
    }

    private static boolean useEventIdKey(String toType, String toId) {
        return toId != null && toType != null;
    }

    private static String eventId(String toType) {
        return String.format(MDC_EVENT_ID_PATTERN, toType.toLowerCase());
    }

    private static String eventAction(String did, String toType) {
        if (did == null && toType == null) {
            return null;
        }

        if (did == null) {
            return toType.toLowerCase();
        }

        if (toType == null) {
            return did.toLowerCase();
        }

        return toType.toLowerCase() + "-" + did.toLowerCase();
    }
}
