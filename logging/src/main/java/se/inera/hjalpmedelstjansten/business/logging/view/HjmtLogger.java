package se.inera.hjalpmedelstjansten.business.logging.view;

import java.util.logging.Level;


public interface HjmtLogger {
    
    enum Action {
        CREATE, UPDATE, READ, DELETE
    }
    
    void log(Level level, String message, Object[] params);
    
    void log(Level level, String message);
    
    void log(Level level, String message, Throwable t);
    
    void logPerformance(String className, String methodName, long duration, String sessionId);
    
    void logAudit(String who, String did, String toType, String toId, String sessionId);
    
}
