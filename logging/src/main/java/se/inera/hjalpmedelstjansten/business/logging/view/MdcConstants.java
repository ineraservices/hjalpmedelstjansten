package se.inera.hjalpmedelstjansten.business.logging.view;

public class MdcConstants {

    private MdcConstants() {
    }

    public static final String MDC_TRACE_ID = "trace.id";
    public static final String MDC_SESSION_ID = "session.id";

    public static final String MDC_USER_ID = "user.id";
    public static final String MDC_USER_NAME = "user.name";
    public static final String MDC_USER_ROLE = "user.role";

    public static final String MDC_ORGANIZATION_ID = "organization.id";

    public static final String MDC_EVENT_ACTION = "event.action";
    public static final String MDC_EVENT_CATEGORY = "event.category";
    public static final String MDC_EVENT_TYPE = "event.type";
    public static final String MDC_EVENT_CLASS = "event.class";
    public static final String MDC_EVENT_METHOD = "event.method";
    public static final String MDC_EVENT_DURATION = "event.duration";

    public static final String MDC_EVENT_ID_PATTERN = "event.%s.id";

    public static final String MDC_EVENT_TYPE_ACCESS = "access";
    public static final String MDC_EVENT_TYPE_CREATION = "creation";
    public static final String MDC_EVENT_TYPE_CHANGE = "change";
    public static final String MDC_EVENT_TYPE_DELETION = "deletion";
    public static final String MDC_EVENT_TYPE_INFO = "info";

    public static final String MDC_EVENT_CATEGORY_PROCESS = "process";

}
