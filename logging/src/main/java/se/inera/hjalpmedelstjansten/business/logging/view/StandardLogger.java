package se.inera.hjalpmedelstjansten.business.logging.view;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.event.Level;

public class StandardLogger implements HjmtLogger {

    private final Logger systemLogger;
    private final Logger performanceLogger;
    private final Logger auditLogger;

    private static final Map<java.util.logging.Level, Level> LEVEL = Map.of(
        java.util.logging.Level.FINEST, Level.DEBUG,
        java.util.logging.Level.FINE, Level.DEBUG,
        java.util.logging.Level.WARNING, Level.WARN,
        java.util.logging.Level.SEVERE, Level.ERROR,
        java.util.logging.Level.INFO, Level.INFO
    );

    StandardLogger(Logger systemLogger, Logger performanceLogger, Logger auditLogger) {
        this.systemLogger = systemLogger;
        this.performanceLogger = performanceLogger;
        this.auditLogger = auditLogger;
    }

    @Override
    public void log(java.util.logging.Level level, String message, Object[] params) {
        message = message.replaceAll("\\{\\d}", "{}");
        this.systemLogger.atLevel(LEVEL.get(level)).log(message, params);
    }

    @Override
    public void log(java.util.logging.Level level, String message) {
        this.systemLogger.atLevel(LEVEL.get(level)).log(message);
    }

    @Override
    public void log(java.util.logging.Level level, String message, Throwable t) {
        this.systemLogger.atLevel(LEVEL.get(level)).log(message, t);
    }

    @Override
    public void logPerformance(String className, String methodName, long duration, String sessionId) {
        MdcToolkit.decoratePerformance(className, methodName, duration);
        this.performanceLogger.info("Class: {}, Method: {}, Duration: {}, SessionId: {}", className, methodName, duration, sessionId);
        MdcToolkit.clearPerformance();
    }

    @Override
    public void logAudit(String who, String did, String toType, String toId, String sessionId) {
        MdcToolkit.decorateAudit(did, toType, toId);
        this.auditLogger.info("who: {}, did: {}, toType: {}, toId: {}, sessionId: {}", who, did, toType, toId, sessionId);
        MdcToolkit.clearAudit(toType, toId);
    }
}
