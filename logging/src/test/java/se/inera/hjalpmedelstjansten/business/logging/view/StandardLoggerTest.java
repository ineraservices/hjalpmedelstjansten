package se.inera.hjalpmedelstjansten.business.logging.view;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_ACTION;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_ID_PATTERN;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.MDC;

@ExtendWith(MockitoExtension.class)
class StandardLoggerTest {

    private static final String EXISTING_KEY = "existingKey";
    private static final String EXISTING_VALUE = "existingValue";

    @Mock
    private Logger systemLogger;
    @Mock
    private Logger performanceLogger;
    @Mock
    private Logger auditLogger;

    private StandardLogger standardLogger;

    @BeforeEach
    void setUp() {
        standardLogger = new StandardLogger(systemLogger, performanceLogger, auditLogger);
    }

    @Test
    void shouldClearEventCategoryPerformanceLog() {
        standardLogger.logPerformance("class", "method", 999, "sessionId");
        assertNull(MDC.get(MdcConstants.MDC_EVENT_CATEGORY));
    }

    @Test
    void shouldClearEventTypePerformanceLog() {
        standardLogger.logPerformance("class", "method", 999, "sessionId");
        assertNull(MDC.get(MdcConstants.MDC_EVENT_TYPE));
    }

    @Test
    void shouldClearEventClassPerformanceLog() {
        standardLogger.logPerformance("class", "method", 999, "sessionId");
        assertNull(MDC.get(MdcConstants.MDC_EVENT_CLASS));
    }

    @Test
    void shouldClearEventMethodPerformanceLog() {
        standardLogger.logPerformance("class", "method", 999, "sessionId");
        assertNull(MDC.get(MdcConstants.MDC_EVENT_METHOD));
    }

    @Test
    void shouldClearEventDurationPerformanceLog() {
        standardLogger.logPerformance("class", "method", 999, "sessionId");
        assertNull(MDC.get(MdcConstants.MDC_EVENT_DURATION));
    }

    @Test
    void shouldNotClearExistingPerformanceLog() {
        MDC.put(EXISTING_KEY, EXISTING_VALUE);
        standardLogger.logPerformance("class", "method", 999, "sessionId");
        assertEquals(EXISTING_VALUE, MDC.get(EXISTING_KEY));
    }

    @Test
    void shouldClearEventActionAuditLog() {
        standardLogger.logAudit("who", "did", "toType", "toId", "sessionId");
        assertNull(MDC.get(MDC_EVENT_ACTION));
    }

    @Test
    void shouldClearEventCategoryAuditLog() {
        standardLogger.logAudit("who", "did", "toType", "toId", "sessionId");
        assertNull(MDC.get(MDC_EVENT_ACTION));
    }

    @Test
    void shouldClearEventTypeAuditLog() {
        standardLogger.logAudit("who", "did", "toType", "toId", "sessionId");
        assertNull(MDC.get(MdcConstants.MDC_EVENT_TYPE));
    }

    @Test
    void shouldClearEventIdAuditLog() {
        standardLogger.logAudit("who", "did", "toType", "toId", "sessionId");
        assertNull(MDC.get(String.format(MDC_EVENT_ID_PATTERN, "toType")));
    }

    @Test
    void shouldNotClearExistingAuditLog() {
        MDC.put(EXISTING_KEY, EXISTING_VALUE);
        standardLogger.logAudit("who", "did", "toType", "toId", "sessionId");
        assertEquals(EXISTING_VALUE, MDC.get(EXISTING_KEY));
    }
}
