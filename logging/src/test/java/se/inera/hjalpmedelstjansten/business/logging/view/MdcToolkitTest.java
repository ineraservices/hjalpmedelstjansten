package se.inera.hjalpmedelstjansten.business.logging.view;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_ACTION;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_CATEGORY;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_CATEGORY_PROCESS;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_CLASS;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_DURATION;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_METHOD;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_TYPE;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_TYPE_ACCESS;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_TYPE_DELETION;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_EVENT_TYPE_INFO;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.slf4j.MDC;

class MdcToolkitTest {

    @Test
    void shallCopyMdc() {
        MDC.put("key1", "value1");
        final var copyMdc = MdcToolkit.copyOfMdc();
        assertEquals(MDC.getCopyOfContextMap(), copyMdc);
    }

    @Test
    void shallClearMdc() {
        MDC.put("key1", "value1");
        MdcToolkit.clearMdc();
        assertNull(MDC.getCopyOfContextMap());
    }

    @Nested
    class TestDecoratePerformance {

        private static final String CLASS_NAME = "className";
        private static final String METHOD_NAME = "methodName";
        private static final int DURATION = 999;
        private static final String DURATION_AS_STR = "999";

        @Test
        void shallDecorateEventCategory() {
            MdcToolkit.decoratePerformance(CLASS_NAME, METHOD_NAME, DURATION);
            assertEquals(MDC_EVENT_CATEGORY_PROCESS, MDC.get(MDC_EVENT_CATEGORY));
        }

        @Test
        void shallDecorateEventType() {
            MdcToolkit.decoratePerformance(CLASS_NAME, METHOD_NAME, DURATION);
            assertEquals(MDC_EVENT_TYPE_INFO, MDC.get(MDC_EVENT_TYPE));
        }

        @Test
        void shallDecorateEventClass() {
            MdcToolkit.decoratePerformance(CLASS_NAME, METHOD_NAME, DURATION);
            assertEquals(CLASS_NAME, MDC.get(MDC_EVENT_CLASS));
        }

        @Test
        void shallDecorateEventMethod() {
            MdcToolkit.decoratePerformance(CLASS_NAME, METHOD_NAME, DURATION);
            assertEquals(METHOD_NAME, MDC.get(MDC_EVENT_METHOD));
        }

        @Test
        void shallDecorateEventDuration() {
            MdcToolkit.decoratePerformance(CLASS_NAME, METHOD_NAME, DURATION);
            assertEquals(DURATION_AS_STR, MDC.get(MDC_EVENT_DURATION));
        }
    }

    @Nested
    class TestDecorateAudit {

        private static final String DID = "DID";
        private static final String TYPE = "TYPE";
        private static final String ID = "ID";

        @Test
        void shallDecorateEventCategory() {
            MdcToolkit.decorateAudit(DID, TYPE, ID);
            assertEquals(MDC_EVENT_CATEGORY_PROCESS, MDC.get(MDC_EVENT_CATEGORY));
        }

        @Test
        void shallDecorateEventActionWhenBothDIDAndTYPEExists() {
            MdcToolkit.decorateAudit(DID, TYPE, ID);
            assertEquals("type-did", MDC.get(MDC_EVENT_ACTION));
        }

        @Test
        void shallDecorateEventActionWhenOnlyDIDExists() {
            MdcToolkit.decorateAudit(DID, null, ID);
            assertEquals("did", MDC.get(MDC_EVENT_ACTION));
        }

        @Test
        void shallDecorateEventActionWhenOnlyTYPEExists() {
            MdcToolkit.decorateAudit(null, TYPE, ID);
            assertEquals("type", MDC.get(MDC_EVENT_ACTION));
        }

        @Test
        void shallDecorateEventTypeWhenOnlyTYPEExists() {
            MdcToolkit.decorateAudit(null, TYPE, ID);
            assertEquals(MDC_EVENT_TYPE_ACCESS, MDC.get(MDC_EVENT_TYPE));
        }

        @Test
        void shallDecorateEventType() {
            MdcToolkit.decorateAudit("delete", TYPE, ID);
            assertEquals(MDC_EVENT_TYPE_DELETION, MDC.get(MDC_EVENT_TYPE));
        }

        @Test
        void shallDecorateEventId() {
            MdcToolkit.decorateAudit(DID, TYPE, ID);
            assertEquals(ID, MDC.get("event.type.id"));
        }
    }
}
