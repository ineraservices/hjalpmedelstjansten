package se.inera.hjalpmedelstjansten.model.api;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 *
 */
public class UserEngagementAPI implements Serializable {

    private Long id;

    @NotNull(message = "{userEngagement.organizationId.notNull}")
    private Long organizationId;

    @NotNull(message = "{userEngagement.validFrom.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{userEngagement.validFrom.invalid}")
    private Long validFrom;

    @DecimalMin(value = "0", inclusive = true, message = "{userEngagement.validTo.invalid}")
    private Long validTo;

    private List<BusinessLevelAPI> businessLevels;

    private List<RoleAPI> roles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Long getValidTo() {
        return validTo;
    }

    public void setValidTo(Long validTo) {
        this.validTo = validTo;
    }

    public List<BusinessLevelAPI> getBusinessLevels() {
        return businessLevels;
    }

    public void setBusinessLevels(List<BusinessLevelAPI> businessLevels) {
        this.businessLevels = businessLevels;
    }

    public List<RoleAPI> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleAPI> roles) {
        this.roles = roles;
    }

}
