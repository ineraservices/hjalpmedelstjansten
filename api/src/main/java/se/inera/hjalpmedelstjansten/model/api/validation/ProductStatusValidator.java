package se.inera.hjalpmedelstjansten.model.api.validation;

import se.inera.hjalpmedelstjansten.model.entity.Product;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;


public class ProductStatusValidator implements ConstraintValidator<ValidProductStatus, String> {


    @Override
    public void initialize(ValidProductStatus constraintAnnotation) {

    }

    @Override
    public boolean isValid(String status, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        if( status != null ) {
            try {
                Product.Status enumStatus = Product.Status.valueOf(status);
            } catch( IllegalArgumentException ex ) {
                context.
                        buildConstraintViolationWithTemplate( "{product.status.invalid}" ).
                        addPropertyNode("status").
                        addConstraintViolation();
                return false;
            }
        }
        return true;
    }

}
