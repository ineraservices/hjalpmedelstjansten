package se.inera.hjalpmedelstjansten.model.api;

import se.inera.hjalpmedelstjansten.model.api.validation.ValidEmail;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidURL;

import jakarta.validation.constraints.Size;
import java.io.Serializable;

/**
 * ElectronicAddressAPI is used from both OrganizationAPI, UserAPI and ProductAPI.
 * For a user, email is mandatory, but not for organization and for product only
 * web can be set. Those validations is not done here in order to reuse the same
 * object. Validation is instead done in business logic.
 *
 */
public class ElectronicAddressAPI implements Serializable {

    private Long id;

    @ValidEmail(message = "{electronicAddress.email.invalid}")
    @Size(max = 255, message = "{electronicAddress.email.wrongLength}")
    private String email;

    @Size(max = 255, message = "{electronicAddress.mobile.wrongLength}")
    private String mobile;

    @Size(max = 255, message = "{electronicAddress.telephone.wrongLength}")
    private String telephone;

    @ValidURL(message = "{electronicAddress.web.invalid}")
    @Size(max = 255, message = "{electronicAddress.web.wrongLength}")
    private String web;

    @Size(max = 255, message = "{electronicAddress.fax.wrongLength}")
    private String fax;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

}
