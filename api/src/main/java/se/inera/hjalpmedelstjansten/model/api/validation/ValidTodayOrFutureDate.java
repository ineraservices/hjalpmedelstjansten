package se.inera.hjalpmedelstjansten.model.api.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for validating a time (as a long) to be during the current day
 * or future.
 *
 */
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {TodayOrFutureDateValidator.class})
public @interface ValidTodayOrFutureDate {

    String message() default "Felaktigt datum";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
