package se.inera.hjalpmedelstjansten.model.api.validation;

import jakarta.mail.internet.AddressException;
import jakarta.mail.internet.InternetAddress;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;


public class EmailValidator implements ConstraintValidator<ValidEmail, String> {


    @Override
    public void initialize(ValidEmail constraintAnnotation) {

    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {
        if( email == null || email.isEmpty() ) {
            return true;
        }
        try {
            new InternetAddress(email, true);
        } catch (AddressException ex) {
            return false;
        }
        return true;
    }



}
