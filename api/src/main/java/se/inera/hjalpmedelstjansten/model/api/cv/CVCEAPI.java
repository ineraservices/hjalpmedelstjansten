package se.inera.hjalpmedelstjansten.model.api.cv;

import java.util.List;


public class CVCEAPI {
    
    private List<CVCEDirectiveAPI> directives;
    private List<CVCEStandardAPI> standards;

    public List<CVCEDirectiveAPI> getDirectives() {
        return directives;
    }

    public void setDirectives(List<CVCEDirectiveAPI> directives) {
        this.directives = directives;
    }

    public List<CVCEStandardAPI> getStandards() {
        return standards;
    }

    public void setStandards(List<CVCEStandardAPI> standards) {
        this.standards = standards;
    }
    
}
