package se.inera.hjalpmedelstjansten.model.api.media;

import se.inera.hjalpmedelstjansten.model.api.cv.CVDocumentTypeAPI;


public class MediaDocumentAPI extends MediaAPI {
    
    private CVDocumentTypeAPI documentType;
    
    private String fileType;
    
    private String fileName;
    
    public CVDocumentTypeAPI getDocumentType() {
        return documentType;
    }

    public void setDocumentType(CVDocumentTypeAPI documentType) {
        this.documentType = documentType;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

}
