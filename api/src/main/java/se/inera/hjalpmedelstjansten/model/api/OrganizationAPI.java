package se.inera.hjalpmedelstjansten.model.api;

import se.inera.hjalpmedelstjansten.model.api.cv.CVCountryAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountyAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidGlnOrGtin;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidOrganizationType;

import jakarta.validation.Valid;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 *
 */
@ValidOrganizationType
public class OrganizationAPI implements Serializable {

    private Long id;

    // null checked by @ValidOrganizationType on class
    private String organizationType;

    @NotNull(message = "{organization.name.notNull}")
    @Size(min = 1, max = 255, message = "{organization.name.wrongLength}")
    private String organizationName;

    @NotNull(message = "{organization.country.notNull}")
    @Valid
    private CVCountryAPI country;

    @NotNull(message = "{organization.number.notNull}")
    @Size(min = 1, max = 255, message = "{organization.number.wrongLength}")
    private String organizationNumber;

    @NotNull(message = "{organization.gln.notNull}")
    @ValidGlnOrGtin(message = "{organization.gln}")
    private String gln;

    @NotNull(message = "{organization.validFrom.notNull}")
    @DecimalMin(value = "0", message = "{organization.validFrom.invalid}")
    private Long validFrom;

    @DecimalMin(value = "0", message = "{organization.validTo.invalid}")
    private Long validTo;

    @Valid
    @NotNull(message = "{organization.electronicAddress.notNull}")
    private ElectronicAddressAPI electronicAddress;

    @Valid
    @NotNull(message = "{organization.postAddresses.wrongSize}")
    @Size(min = 2, max = 2, message = "{organization.postAddresses.wrongSize}")
    private List<PostAddressAPI> postAddresses;

    private List<CVCountyAPI> counties;
    private boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrganizationType() {
        return organizationType;
    }

    public void setOrganisationType(String organizationType) {
        this.organizationType = organizationType;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public CVCountryAPI getCountry() {
        return country;
    }

    public void setCountry(CVCountryAPI country) {
        this.country = country;
    }

    public String getOrganizationNumber() {
        return organizationNumber;
    }

    public void setOrganizationNumber(String organizationNumber) {
        this.organizationNumber = organizationNumber;
    }

    public String getGln() {
        return gln;
    }

    public void setGln(String gln) {
        this.gln = gln;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Long getValidTo() {
        return validTo;
    }

    public void setValidTo(Long validTo) {
        this.validTo = validTo;
    }

    public ElectronicAddressAPI getElectronicAddress() {
        return electronicAddress;
    }

    public void setElectronicAddress(ElectronicAddressAPI electronicAddress) {
        this.electronicAddress = electronicAddress;
    }

    public List<PostAddressAPI> getPostAddresses() {
        return postAddresses;
    }

    public void setPostAddresses(List<PostAddressAPI> postAddresses) {
        this.postAddresses = postAddresses;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<CVCountyAPI> getCounties() { return counties; }

    public void setCounties(List<CVCountyAPI> counties) {
        this.counties = counties;
    }
}
