package se.inera.hjalpmedelstjansten.model.api.external;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


public class ExpressionAPI {
    
    private List<String> idPath;
    private List<String> matchIds;
    private String isTrue;
    private String includeSubMatches;

    @JsonProperty("IdPath")
    public List<String> getIdPath() {
        return idPath;
    }

    @JsonProperty("IdPath")
    public void setIdPath(List<String> idPath) {
        this.idPath = idPath;
    }

    @JsonProperty("MatchIds")
    public List<String> getMatchIds() {
        return matchIds;
    }

    @JsonProperty("MatchIds")
    public void setMatchIds(List<String> matchIds) {
        this.matchIds = matchIds;
    }

    @JsonProperty("IsTrue")
    public String getIsTrue() {
        return isTrue;
    }

    @JsonProperty("IsTrue")
    public void setIsTrue(String isTrue) {
        this.isTrue = isTrue;
    }
    
    @JsonProperty("IncludeSubMatches")
    public String getIncludeSubMatches() {
        return includeSubMatches;
    }

    @JsonProperty("IncludeSubMatches")
    public void setIncludeSubMatches(String includeSubMatches) {
        this.includeSubMatches = includeSubMatches;
    }

}
