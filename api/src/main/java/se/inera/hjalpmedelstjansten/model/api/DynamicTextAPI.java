package se.inera.hjalpmedelstjansten.model.api;


import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;


public class DynamicTextAPI implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = true)
    private String dynamicText;

    @Temporal(TemporalType.TIMESTAMP)
    private Date whenChanged;

    @Column(nullable = false)
    private String changedBy;


    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Long getUniqueId() {
        return uniqueId;
    }


    public void setDynamicText(String dynamicText) {
        this.dynamicText = dynamicText;
    }

    public String getDynamicText() {
        return dynamicText;
    }


    public void setWhenChanged(Date whenChanged) {
        this.whenChanged = whenChanged;
    }

    public Date getWhenChanged() {
        return whenChanged;
    }


    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy;
    }

    public String getChangedBy() {
        return changedBy;
    }

}
