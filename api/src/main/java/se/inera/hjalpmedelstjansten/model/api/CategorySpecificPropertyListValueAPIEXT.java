package se.inera.hjalpmedelstjansten.model.api;

import java.io.Serializable;


public class CategorySpecificPropertyListValueAPIEXT implements Serializable {

    private Long propertyId;
    private Long id;
    private String value;

    public Long getPropertyId() {
        return propertyId;
    }
    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
