package se.inera.hjalpmedelstjansten.model.api;

import se.inera.hjalpmedelstjansten.model.entity.PostAddress;

import jakarta.validation.constraints.Size;
import java.io.Serializable;


public class PostAddressAPI implements Serializable {

    private Long id;
    private PostAddress.AddressType addressType;
    @Size(max = 255, message = "{postAddress.streetAddress.wrongLength}")
    private String streetAddress;
    @Size(max = 255, message = "{postAddress.postCode.wrongLength}")
    private String postCode;
    @Size(max = 255, message = "{postAddress.city.wrongLength}")
    private String city;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PostAddress.AddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(PostAddress.AddressType addressType) {
        this.addressType = addressType;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}
