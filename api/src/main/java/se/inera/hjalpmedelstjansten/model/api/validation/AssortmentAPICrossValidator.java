package se.inera.hjalpmedelstjansten.model.api.validation;

import se.inera.hjalpmedelstjansten.model.api.AssortmentAPI;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;


public class AssortmentAPICrossValidator implements ConstraintValidator<ValidAssortmentAPICross, AssortmentAPI> {


    @Override
    public void initialize(ValidAssortmentAPICross constraintAnnotation) {

    }

    @Override
    public boolean isValid(AssortmentAPI assortmentAPI, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        boolean valid = true;
        if( assortmentAPI.getValidFrom() != null && assortmentAPI.getValidTo() != null ) {
            if( assortmentAPI.getValidFrom() >= assortmentAPI.getValidTo() ) {
                context.
                        buildConstraintViolationWithTemplate( "{assortment.validToBeforeValidFrom}" ).
                        addPropertyNode("validTo").
                        addConstraintViolation();
                valid = false;
            }
        }
        return valid;
    }



}
