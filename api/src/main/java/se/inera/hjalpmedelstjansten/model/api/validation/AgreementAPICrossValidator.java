package se.inera.hjalpmedelstjansten.model.api.validation;

import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;


public class AgreementAPICrossValidator implements ConstraintValidator<ValidAgreementAPICross, AgreementAPI> {


    @Override
    public void initialize(ValidAgreementAPICross constraintAnnotation) {

    }

    @Override
    public boolean isValid(AgreementAPI agreementAPI, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        boolean valid = true;
        if( agreementAPI.getValidFrom() != null && agreementAPI.getValidTo() != null ) {
            if( agreementAPI.getValidFrom() >= agreementAPI.getValidTo() ) {
                context.
                        buildConstraintViolationWithTemplate( "{agreement.validToBeforeValidFrom}" ).
                        addPropertyNode("validTo").
                        addConstraintViolation();
                valid = false;
            }
        }
        return valid;
    }



}
