package se.inera.hjalpmedelstjansten.model.api;

import java.io.Serializable;
import java.util.List;


public class CategorySpecificPropertyAPI implements Serializable {
    
    private Long id;
    private Integer orderIndex;
    private String name;
    private String description;
    private String type;
    private List<CategorySpecificPropertyListValueAPI> values;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getOrderIndex() { return orderIndex; }

    public void setOrderIndex(Integer orderIndex) { this.orderIndex = orderIndex; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<CategorySpecificPropertyListValueAPI> getValues() {
        return values;
    }

    public void setValues(List<CategorySpecificPropertyListValueAPI> values) {
        this.values = values;
    }

}
