package se.inera.hjalpmedelstjansten.model.api;

import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidGeneralPricelistAPICross;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 *
 */
@ValidGeneralPricelistAPICross
public class GeneralPricelistAPI {

    private Long id;

    @Size(max = 255, message = "{generalPricelist.name.wrongLength}")
    private String generalPricelistName;

    @NotNull(message = "{generalPricelist.number.notNull}")
    @Size(min = 1, max = 255, message = "{generalPricelist.number.wrongLength}")
    private String generalPricelistNumber;

    @NotNull(message = "{generalPricelist.validFrom.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{generalPricelist.validFrom.invalid}")
    private Long validFrom;

    @NotNull(message = "{generalPricelist.validTo.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{generalPricelist.validTo.invalid}")
    private Long validTo;

    private OrganizationAPI ownerOrganization;

    @NotNull(message = "{generalPricelist.deliveryTimeH.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{generalPricelist.deliveryTimeH.invalid}")
    private Integer deliveryTimeH;

    @NotNull(message = "{generalPricelist.deliveryTimeT.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{generalPricelist.deliveryTimeT.invalid}")
    private Integer deliveryTimeT;

    @NotNull(message = "{generalPricelist.deliveryTimeI.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{generalPricelist.deliveryTimeI.invalid}")
    private Integer deliveryTimeI;

    @NotNull(message = "{generalPricelist.deliveryTimeR.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{generalPricelist.deliveryTimeR.invalid}")
    private Integer deliveryTimeR;

    @NotNull(message = "{generalPricelist.deliveryTimeTJ.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{generalPricelist.deliveryTimeTJ.invalid}")
    private Integer deliveryTimeTJ;

    @DecimalMin(value = "1", inclusive = true, message = "{generalPricelist.warrantyQuantityH.invalid}")
    private Integer warrantyQuantityH;

    @DecimalMin(value = "1", inclusive = true, message = "{generalPricelist.warrantyQuantityT.invalid}")
    private Integer warrantyQuantityT;

    @DecimalMin(value = "1", inclusive = true, message = "{generalPricelist.warrantyQuantityI.invalid}")
    private Integer warrantyQuantityI;

    @DecimalMin(value = "1", inclusive = true, message = "{generalPricelist.warrantyQuantityR.invalid}")
    private Integer warrantyQuantityR;

    @DecimalMin(value = "1", inclusive = true, message = "{generalPricelist.warrantyQuantityTJ.invalid}")
    private Integer warrantyQuantityTJ;

    private CVGuaranteeUnitAPI warrantyQuantityHUnit;
    private CVGuaranteeUnitAPI warrantyQuantityTUnit;
    private CVGuaranteeUnitAPI warrantyQuantityIUnit;
    private CVGuaranteeUnitAPI warrantyQuantityRUnit;
    private CVGuaranteeUnitAPI warrantyQuantityTJUnit;

    private CVPreventiveMaintenanceAPI warrantyValidFromH;
    private CVPreventiveMaintenanceAPI warrantyValidFromT;
    private CVPreventiveMaintenanceAPI warrantyValidFromI;
    private CVPreventiveMaintenanceAPI warrantyValidFromR;
    private CVPreventiveMaintenanceAPI warrantyValidFromTJ;

    @Size(min = 0, max = 255, message = "{generalPricelist.warrantyTermsH.wrongLength}")
    private String warrantyTermsH;

    @Size(min = 0, max = 255, message = "{generalPricelist.warrantyTermsT.wrongLength}")
    private String warrantyTermsT;

    @Size(min = 0, max = 255, message = "{generalPricelist.warrantyTermsI.wrongLength}")
    private String warrantyTermsI;

    @Size(min = 0, max = 255, message = "{generalPricelist.warrantyTermsR.wrongLength}")
    private String warrantyTermsR;

    @Size(min = 0, max = 255, message = "{generalPricelist.warrantyTermsTJ.wrongLength}")
    private String warrantyTermsTJ;

    private GeneralPricelistPricelistAPI currentPricelist;

    private Boolean hasPricelistRows;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGeneralPricelistName() {
        return generalPricelistName;
    }

    public void setGeneralPricelistName(String generalPricelistName) {
        this.generalPricelistName = generalPricelistName;
    }

    public String getGeneralPricelistNumber() {
        return generalPricelistNumber;
    }

    public void setGeneralPricelistNumber(String generalPricelistNumber) {
        this.generalPricelistNumber = generalPricelistNumber;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Long getValidTo() {
        return validTo;
    }

    public void setValidTo(Long validTo) {
        this.validTo = validTo;
    }

    public OrganizationAPI getOwnerOrganization() {
        return ownerOrganization;
    }

    public void setOwnerOrganization(OrganizationAPI ownerOrganization) {
        this.ownerOrganization = ownerOrganization;
    }

    public Integer getDeliveryTimeH() {
        return deliveryTimeH;
    }

    public void setDeliveryTimeH(Integer deliveryTimeH) {
        this.deliveryTimeH = deliveryTimeH;
    }

    public Integer getDeliveryTimeT() {
        return deliveryTimeT;
    }

    public void setDeliveryTimeT(Integer deliveryTimeT) {
        this.deliveryTimeT = deliveryTimeT;
    }

    public Integer getDeliveryTimeI() {
        return deliveryTimeI;
    }

    public void setDeliveryTimeI(Integer deliveryTimeI) {
        this.deliveryTimeI = deliveryTimeI;
    }

    public Integer getDeliveryTimeR() {
        return deliveryTimeR;
    }

    public void setDeliveryTimeR(Integer deliveryTimeR) {
        this.deliveryTimeR = deliveryTimeR;
    }

    public Integer getDeliveryTimeTJ() {
        return deliveryTimeTJ;
    }

    public void setDeliveryTimeTJ(Integer deliveryTimeTJ) {
        this.deliveryTimeTJ = deliveryTimeTJ;
    }

    public Integer getWarrantyQuantityH() {
        return warrantyQuantityH;
    }

    public void setWarrantyQuantityH(Integer warrantyQuantityH) {
        this.warrantyQuantityH = warrantyQuantityH;
    }

    public Integer getWarrantyQuantityT() {
        return warrantyQuantityT;
    }

    public void setWarrantyQuantityT(Integer warrantyQuantityT) {
        this.warrantyQuantityT = warrantyQuantityT;
    }

    public Integer getWarrantyQuantityI() {
        return warrantyQuantityI;
    }

    public void setWarrantyQuantityI(Integer warrantyQuantityI) {
        this.warrantyQuantityI = warrantyQuantityI;
    }

    public Integer getWarrantyQuantityR() {
        return warrantyQuantityR;
    }

    public void setWarrantyQuantityR(Integer warrantyQuantityR) {
        this.warrantyQuantityR = warrantyQuantityR;
    }

    public Integer getWarrantyQuantityTJ() {
        return warrantyQuantityTJ;
    }

    public void setWarrantyQuantityTJ(Integer warrantyQuantityTJ) {
        this.warrantyQuantityTJ = warrantyQuantityTJ;
    }

    public CVGuaranteeUnitAPI getWarrantyQuantityHUnit() {
        return warrantyQuantityHUnit;
    }

    public void setWarrantyQuantityHUnit(CVGuaranteeUnitAPI warrantyQuantityHUnit) {
        this.warrantyQuantityHUnit = warrantyQuantityHUnit;
    }

    public CVGuaranteeUnitAPI getWarrantyQuantityTUnit() {
        return warrantyQuantityTUnit;
    }

    public void setWarrantyQuantityTUnit(CVGuaranteeUnitAPI warrantyQuantityTUnit) {
        this.warrantyQuantityTUnit = warrantyQuantityTUnit;
    }

    public CVGuaranteeUnitAPI getWarrantyQuantityIUnit() {
        return warrantyQuantityIUnit;
    }

    public void setWarrantyQuantityIUnit(CVGuaranteeUnitAPI warrantyQuantityIUnit) {
        this.warrantyQuantityIUnit = warrantyQuantityIUnit;
    }

    public CVGuaranteeUnitAPI getWarrantyQuantityRUnit() {
        return warrantyQuantityRUnit;
    }

    public void setWarrantyQuantityRUnit(CVGuaranteeUnitAPI warrantyQuantityRUnit) {
        this.warrantyQuantityRUnit = warrantyQuantityRUnit;
    }

    public CVGuaranteeUnitAPI getWarrantyQuantityTJUnit() {
        return warrantyQuantityTJUnit;
    }

    public void setWarrantyQuantityTJUnit(CVGuaranteeUnitAPI warrantyQuantityTJUnit) {
        this.warrantyQuantityTJUnit = warrantyQuantityTJUnit;
    }

    public CVPreventiveMaintenanceAPI getWarrantyValidFromH() {
        return warrantyValidFromH;
    }

    public void setWarrantyValidFromH(CVPreventiveMaintenanceAPI warrantyValidFromH) {
        this.warrantyValidFromH = warrantyValidFromH;
    }

    public CVPreventiveMaintenanceAPI getWarrantyValidFromT() {
        return warrantyValidFromT;
    }

    public void setWarrantyValidFromT(CVPreventiveMaintenanceAPI warrantyValidFromT) {
        this.warrantyValidFromT = warrantyValidFromT;
    }

    public CVPreventiveMaintenanceAPI getWarrantyValidFromI() {
        return warrantyValidFromI;
    }

    public void setWarrantyValidFromI(CVPreventiveMaintenanceAPI warrantyValidFromI) {
        this.warrantyValidFromI = warrantyValidFromI;
    }

    public CVPreventiveMaintenanceAPI getWarrantyValidFromR() {
        return warrantyValidFromR;
    }

    public void setWarrantyValidFromR(CVPreventiveMaintenanceAPI warrantyValidFromR) {
        this.warrantyValidFromR = warrantyValidFromR;
    }

    public CVPreventiveMaintenanceAPI getWarrantyValidFromTJ() {
        return warrantyValidFromTJ;
    }

    public void setWarrantyValidFromTJ(CVPreventiveMaintenanceAPI warrantyValidFromTJ) {
        this.warrantyValidFromTJ = warrantyValidFromTJ;
    }

    public String getWarrantyTermsH() {
        return warrantyTermsH;
    }

    public void setWarrantyTermsH(String warrantyTermsH) {
        this.warrantyTermsH = warrantyTermsH;
    }

    public String getWarrantyTermsT() {
        return warrantyTermsT;
    }

    public void setWarrantyTermsT(String warrantyTermsT) {
        this.warrantyTermsT = warrantyTermsT;
    }

    public String getWarrantyTermsI() {
        return warrantyTermsI;
    }

    public void setWarrantyTermsI(String warrantyTermsI) {
        this.warrantyTermsI = warrantyTermsI;
    }

    public String getWarrantyTermsR() {
        return warrantyTermsR;
    }

    public void setWarrantyTermsR(String warrantyTermsR) {
        this.warrantyTermsR = warrantyTermsR;
    }

    public String getWarrantyTermsTJ() {
        return warrantyTermsTJ;
    }

    public void setWarrantyTermsTJ(String warrantyTermsTJ) {
        this.warrantyTermsTJ = warrantyTermsTJ;
    }

    public GeneralPricelistPricelistAPI getCurrentPricelist() {
        return currentPricelist;
    }

    public void setCurrentPricelist(GeneralPricelistPricelistAPI currentPricelist) {
        this.currentPricelist = currentPricelist;
    }

    public Boolean isHasPricelistRows() {
        return hasPricelistRows;
    }

    public void setHasPricelistRows(Boolean hasPricelistRows) {
        this.hasPricelistRows = hasPricelistRows;
    }

}
