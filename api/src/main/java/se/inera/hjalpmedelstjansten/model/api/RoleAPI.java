package se.inera.hjalpmedelstjansten.model.api;

import java.io.Serializable;
import java.util.List;


public class RoleAPI implements Serializable {
    
    private Long id;    
    private String name;    
    private String description;    
    private List<PermissionAPI> permissions;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<PermissionAPI> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionAPI> permissions) {
        this.permissions = permissions;
    }

    
}
