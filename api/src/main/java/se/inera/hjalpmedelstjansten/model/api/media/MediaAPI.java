package se.inera.hjalpmedelstjansten.model.api.media;


import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public abstract class MediaAPI {
    private Long id;
    private Long productId;
    private String description;
    private String url;
    private String originalUrl;

    @NotNull(message = "{mainImage.altText.notNull}")
//    @Size(min = 1, max = 35, message = "{mainImage.altText.notNull}")
    private String alternativeText;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getOriginalUrl() { return originalUrl; }

    public void setOriginalUrl(String url) { this.originalUrl = url; }

    public Long getProductId() { return productId; }

    public void setProductId(Long productId) { this.productId = productId; }

    public String getAlternativeText() { return alternativeText; }

    public void setAlternativeText(String alternativeText) { this.alternativeText = alternativeText; }
}
