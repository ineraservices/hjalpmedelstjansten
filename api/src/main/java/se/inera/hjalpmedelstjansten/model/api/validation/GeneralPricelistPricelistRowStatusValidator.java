package se.inera.hjalpmedelstjansten.model.api.validation;

import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelistRow;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;


public class GeneralPricelistPricelistRowStatusValidator implements ConstraintValidator<ValidGeneralPricelistPricelistRowStatus, String> {


    @Override
    public void initialize(ValidGeneralPricelistPricelistRowStatus constraintAnnotation) {

    }

    @Override
    public boolean isValid(String status, ConstraintValidatorContext context) {
        if( status != null ) {
            try {
                GeneralPricelistPricelistRow.Status.valueOf(status);
            } catch( IllegalArgumentException ex ) {
                return false;
            }
        }
        return true;
    }

}
