package se.inera.hjalpmedelstjansten.model.api;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 *
 */
public class UserAPI implements Serializable {

    private Long id;

    @NotNull(message = "{user.firstname.notNull}")
    private String firstName;

    @NotNull(message = "{user.lastname.notNull}")
    private String lastName;

    private String title;

    @NotNull(message = "{user.username.notNull}")
    private String username;

    private Boolean previousLoginFound;

    @Valid
    @NotNull(message = "{user.electronicAddress.notNull}")
    private ElectronicAddressAPI electronicAddress;

    @Valid
    @NotNull(message = "{user.engagements.notNull}")
    @Size(min = 1, max = 1, message = "{user.engagements.wrongSize}")
    private List<UserEngagementAPI> userEngagements;

    private String loginType;
    private String loginUrl;

    private boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ElectronicAddressAPI getElectronicAddress() {
        return electronicAddress;
    }

    public void setElectronicAddress(ElectronicAddressAPI electronicAddress) {
        this.electronicAddress = electronicAddress;
    }

    public List<UserEngagementAPI> getUserEngagements() {
        return userEngagements;
    }

    public void setUserEngagementAPIs(List<UserEngagementAPI> userEngagements) {
        this.userEngagements = userEngagements;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Boolean getPreviousLoginFound() {
        return previousLoginFound;
    }

    public void setPreviousLoginFound(Boolean previousLoginFound) {
        this.previousLoginFound = previousLoginFound;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

}
