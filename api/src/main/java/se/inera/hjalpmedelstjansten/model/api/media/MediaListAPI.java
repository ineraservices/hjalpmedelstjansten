package se.inera.hjalpmedelstjansten.model.api.media;

import java.util.List;


public class MediaListAPI {
    
    private List<MediaImageAPI> images;
    private List<MediaVideoAPI> videos;
    private List<MediaDocumentAPI> documents;

    public List<MediaImageAPI> getImages() {
        return images;
    }

    public void setImages(List<MediaImageAPI> images) {
        this.images = images;
    }

    public List<MediaVideoAPI> getVideos() {
        return videos;
    }

    public void setVideos(List<MediaVideoAPI> videos) {
        this.videos = videos;
    }

    public List<MediaDocumentAPI> getDocuments() {
        return documents;
    }

    public void setDocuments(List<MediaDocumentAPI> documents) {
        this.documents = documents;
    }

}
