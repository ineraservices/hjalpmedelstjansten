package se.inera.hjalpmedelstjansten.model.api;

import jakarta.validation.constraints.NotNull;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 *
 */
public class LoginAPI {

    @NotNull(message = "{login.username.notNull}")
    private String username;

    @NotNull(message = "{login.password.notNull}")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
