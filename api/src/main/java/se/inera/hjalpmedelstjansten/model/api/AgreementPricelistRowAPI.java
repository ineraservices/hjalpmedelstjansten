package se.inera.hjalpmedelstjansten.model.api;

import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidAgreementPricelistRowStatus;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 *
 */
public class AgreementPricelistRowAPI implements HJMTJRow {

    private Long id;

    @NotNull(message = "{pricelistrow.status.notNull}")
    @ValidAgreementPricelistRowStatus(message = "{pricelistrow.status.invalid}")
    private String status;

    @DecimalMin(value = "0", inclusive = true, message = "{pricelistrow.validFrom.invalid}")
    private Long validFrom;

    private BigDecimal price;

    @NotNull(message = "{pricelistrow.leastOrderQuantity.notNull}")
    @DecimalMin(value = "1", inclusive = true, message = "{pricelistrow.leastOrderQuantity.invalid}")
    private Integer leastOrderQuantity;

    @DecimalMin(value = "0", inclusive = true, message = "{pricelistrow.deliveryTime.invalid}")
    private Integer deliveryTime;

    @DecimalMin(value = "1", inclusive = true, message = "{pricelistrow.warrantyQuantity.invalid}")
    private Integer warrantyQuantity;

    private CVGuaranteeUnitAPI warrantyQuantityUnit;

    private CVPreventiveMaintenanceAPI warrantyValidFrom;

    @Size(min = 0, max = 255, message = "{pricelistrow.warrantyTerms.wrongLength}")
    private String warrantyTerms;

    @Size(min = 0, max = 255, message = "{pricelistrow.comment.wrongLength}")
    private String comment;

    private AgreementPricelistAPI pricelist;

    @NotNull(message = "{pricelistrow.article.notNull}")
    private ArticleAPI article;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public AgreementPricelistAPI getPricelist() {
        return pricelist;
    }

    public void setPricelist(AgreementPricelistAPI pricelist) {
        this.pricelist = pricelist;
    }

    public ArticleAPI getArticle() {
        return article;
    }

    public void setArticle(ArticleAPI article) {
        this.article = article;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Integer getLeastOrderQuantity() {
        return leastOrderQuantity;
    }

    public void setLeastOrderQuantity(Integer leastOrderQuantity) {
        this.leastOrderQuantity = leastOrderQuantity;
    }

    public Integer getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Integer deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Integer getWarrantyQuantity() {
        return warrantyQuantity;
    }

    public void setWarrantyQuantity(Integer warrantyQuantity) {
        this.warrantyQuantity = warrantyQuantity;
    }

    public CVGuaranteeUnitAPI getWarrantyQuantityUnit() {
        return warrantyQuantityUnit;
    }

    public void setWarrantyQuantityUnit(CVGuaranteeUnitAPI warrantyQuantityUnit) {
        this.warrantyQuantityUnit = warrantyQuantityUnit;
    }

    public CVPreventiveMaintenanceAPI getWarrantyValidFrom() {
        return warrantyValidFrom;
    }

    public void setWarrantyValidFrom(CVPreventiveMaintenanceAPI warrantyValidFrom) {
        this.warrantyValidFrom = warrantyValidFrom;
    }

    public String getWarrantyTerms() {
        return warrantyTerms;
    }

    public void setWarrantyTerms(String warrantyTerms) {
        this.warrantyTerms = warrantyTerms;
    }

    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }

}
