package se.inera.hjalpmedelstjansten.model.api;

/**
 * API to return true or false
 * 
 */
public class BooleanAPI {
        
    private boolean isTrue;

    public boolean isIsTrue() {
        return isTrue;
    }

    public void setIsTrue(boolean isTrue) {
        this.isTrue = isTrue;
    }
    
}
