package se.inera.hjalpmedelstjansten.model.api;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.util.List;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 *
 */
public class AgreementPricelistAPI {

    private Long id;

    @NotNull(message = "{pricelist.number.notNull}")
    @Size(min = 1, max = 255, message = "{pricelist.number.wrongLength}")
    private String number;

    @NotNull(message = "{pricelist.validFrom.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{pricelist.validFrom.invalid}")
    //@ValidTodayOrFutureDate(message = "{pricelist.validFrom.notFuture}")
    private Long validFrom;

    private AgreementAPI agreement;

    private String status;

    private List<AgreementPricelistRowAPI> rows;

    private Boolean hasPricelistRows;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public AgreementAPI getAgreement() {
        return agreement;
    }

    public void setAgreement(AgreementAPI agreement) {
        this.agreement = agreement;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<AgreementPricelistRowAPI> getRows() {
        return rows;
    }

    public void setRows(List<AgreementPricelistRowAPI> rows) {
        this.rows = rows;
    }

    public Boolean getHasPricelistRows() {
        return hasPricelistRows;
    }

    public void setHasPricelistRows(Boolean hasPricelistRows) {
        this.hasPricelistRows = hasPricelistRows;
    }

}
