package se.inera.hjalpmedelstjansten.model.api;

import se.inera.hjalpmedelstjansten.model.api.cv.CVCEDirectiveAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEStandardAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVOrderUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPackageUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidArticleAPICross;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidArticleStatus;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidGlnOrGtin;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.util.List;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 *
 */
@ValidArticleAPICross
public class ArticleAPI {

    private Long id;

    @NotNull(message = "{article.name.notNull}")
    @Size(min = 1, max = 255, message = "{article.name.wrongLength}")
    private String articleName;

    @NotNull(message = "{article.number.notNull}")
    @Size(min = 1, max = 35, message = "{article.number.wrongLength}")
    private String articleNumber;

    @NotNull(message = "{article.status.notNull}")
    @ValidArticleStatus
    private String status;

    private String organizationName;
    private Long organizationId;

    @NotNull(message = "{article.category.notNull}")
    private CategoryAPI category;

    private List<CategoryAPI> extendedCategories;

    private ProductAPI basedOnProduct;
    private List<ProductAPI> fitsToProducts;
    private List<ArticleAPI> fitsToArticles;

    @ValidGlnOrGtin(message = "{article.gtin}")
    private String gtin;

    @Size(min = 0, max = 512, message = "{article.supplementedInformation.wrongLength}")
    private String supplementedInformation;

    private boolean ceMarked;
    private CVCEDirectiveAPI ceDirective;
    private CVCEStandardAPI ceStandard;

    //ARTIKELINFORMATION
    public boolean customerUniqueOverridden;

    //BESTÄLLNINGSINFORMATION
    private boolean orderInformationOverridden;

    //ARTIKELMÄRKNING
    private boolean ceDirectiveOverridden;
    private boolean ceMarkedOverridden;
    private boolean ceStandardOverridden;

    //FÖREBYGGANDE UNDERHÅLL
    private boolean preventiveMaintenanceDescriptionOverridden;
    private boolean preventiveMaintenanceNumberOfDaysOverridden;
    private boolean preventiveMaintenanceValidFromOverridden;

    //TILLVERKARE
    private boolean manufacturerArticleNumberOverridden;
    private boolean manufacturerElectronicAddressOverridden;
    private boolean manufacturerOverridden;
    private boolean trademarkOverridden;

    //KOMPLETTERANDE ARTIKELINFORMATION
    private boolean colorOverridden;
    private boolean supplementedInformationOverridden;

    //ARTIKELN HAR UTGÅTT
    private boolean statusOverridden;

    public boolean isCustomerUniqueOverridden() {
        return customerUniqueOverridden;
    }
    public void setCustomerUniqueOverridden(boolean customerUniqueOverridden) {
        this.customerUniqueOverridden = customerUniqueOverridden;
    }

    public boolean isOrderInformationOverridden() {
        return orderInformationOverridden;
    }
    public void setOrderInformationOverridden(boolean orderInformationOverridden) {
        this.orderInformationOverridden = orderInformationOverridden;
    }

    public boolean isCeDirectiveOverridden() {
        return ceDirectiveOverridden;
    }
    public void setCeDirectiveOverridden(boolean ceDirectiveOverridden) {
        this.ceDirectiveOverridden = ceDirectiveOverridden;
    }

    public boolean isCeMarkedOverridden() {
        return this.ceMarkedOverridden;
    }
    public void setCeMarkedOverridden(boolean ceMarkedOverridden) {
        this.ceMarkedOverridden = ceMarkedOverridden;
    }


    public boolean isCeStandardOverridden() {
        return ceStandardOverridden;
    }
    public void setCeStandardOverridden(boolean ceStandardOverridden) {
        this.ceStandardOverridden = ceStandardOverridden;
    }

    public boolean isPreventiveMaintenanceDescriptionOverridden() {
        return preventiveMaintenanceDescriptionOverridden;
    }
    public void setPreventiveMaintenanceDescriptionOverridden(boolean preventiveMaintenanceDescriptionOverridden) {
        this.preventiveMaintenanceDescriptionOverridden = preventiveMaintenanceDescriptionOverridden;
    }

    public boolean isPreventiveMaintenanceNumberOfDaysOverridden() {
        return preventiveMaintenanceNumberOfDaysOverridden;
    }
    public void setPreventiveMaintenanceNumberOfDaysOverridden(boolean preventiveMaintenanceNumberOfDaysOverridden) {
        this.preventiveMaintenanceNumberOfDaysOverridden = preventiveMaintenanceNumberOfDaysOverridden;
    }

    public boolean isPreventiveMaintenanceValidFromOverridden() {
        return preventiveMaintenanceValidFromOverridden;
    }
    public void setPreventiveMaintenanceValidFromOverridden(boolean preventiveMaintenanceValidFromOverridden) {
        this.preventiveMaintenanceValidFromOverridden = preventiveMaintenanceValidFromOverridden;
    }

    public boolean isManufacturerArticleNumberOverridden() {
        return manufacturerArticleNumberOverridden;
    }
    public void setManufacturerArticleNumberOverridden(boolean manufacturerArticleNumberOverridden) {
        this.manufacturerArticleNumberOverridden = manufacturerArticleNumberOverridden;
    }

    public boolean isManufacturerElectronicAddressOverridden() {
        return manufacturerElectronicAddressOverridden;
    }
    public void setManufacturerElectronicAddressOverridden(boolean manufacturerElectronicAddressOverridden) {
        this.manufacturerElectronicAddressOverridden = manufacturerElectronicAddressOverridden;
    }

    public boolean isManufacturerOverridden() {
        return manufacturerOverridden;
    }
    public void setManufacturerOverridden(boolean manufacturerOverridden) {
        this.manufacturerOverridden = manufacturerOverridden;
    }

    public boolean isTrademarkOverridden() {
        return trademarkOverridden;
    }
    public void setTrademarkOverridden(boolean trademarkOverridden) {
        this.trademarkOverridden = trademarkOverridden;
    }

    public boolean isColorOverridden() {
        return colorOverridden;
    }
    public void setColorOverridden(boolean colorOverridden) {
        this.colorOverridden = colorOverridden;
    }

    public boolean isSupplementedInformationOverridden() {
        return supplementedInformationOverridden;
    }
    public void setSupplementedInformationOverridden(boolean supplementedInformationOverridden) {
        this.supplementedInformationOverridden = supplementedInformationOverridden;
    }

    public boolean isStatusOverridden() {
        return statusOverridden;
    }
    public void setStatusOverridden(boolean statusOverridden) {
        this.statusOverridden = statusOverridden;
    }

    @Size(min = 0, max = 255, message = "{article.manufacturer.wrongLength}")
    private String manufacturer;

    @Size(min = 0, max = 255, message = "{article.manufacturerArticleNumber.wrongLength}")
    private String manufacturerArticleNumber;

    @Size(min = 0, max = 255, message = "{article.trademark.wrongLength}")
    private String trademark;

    private ElectronicAddressAPI manufacturerElectronicAddress;

    private boolean customerUnique;

    private CVPreventiveMaintenanceAPI preventiveMaintenanceValidFrom;

    private Integer preventiveMaintenanceNumberOfDays;

    private String preventiveMaintenanceDescription;

    private String color;

    @DecimalMin(value = "0", inclusive = true, message = "{article.replacementDate.invalid}")
    private Long replacementDate;

    private Boolean inactivateRowsOnReplacement;

    private List<ArticleAPI> replacedByArticles;

    @NotNull(message = "{article.orderUnit.notNull}")
    private CVOrderUnitAPI orderUnit;

    private Double articleQuantityInOuterPackage;
    private CVOrderUnitAPI articleQuantityInOuterPackageUnit;

    private Double packageContent;
    private CVPackageUnitAPI packageContentUnit;

    private Integer packageLevelBase;

    private Integer packageLevelMiddle;

    private Integer packageLevelTop;

    private List<ResourceSpecificPropertyAPI> categoryPropertys;

    private Long created;

    private Long updated;

    private boolean numberEditable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public String getArticleNumber() {
        return articleNumber;
    }

    public void setArticleNumber(String articleNumber) {
        this.articleNumber = articleNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CategoryAPI getCategory() {
        return category;
    }

    public void setCategory(CategoryAPI category) {
        this.category = category;
    }

    public ProductAPI getBasedOnProduct() {
        return basedOnProduct;
    }

    public void setBasedOnProduct(ProductAPI basedOnProduct) {
        this.basedOnProduct = basedOnProduct;
    }

    public List<ProductAPI> getFitsToProducts() {
        return fitsToProducts;
    }

    public void setFitsToProducts(List<ProductAPI> fitsToProducts) {
        this.fitsToProducts = fitsToProducts;
    }

    public List<ArticleAPI> getFitsToArticles() {
        return fitsToArticles;
    }

    public void setFitsToArticles(List<ArticleAPI> fitsToArticles) {
        this.fitsToArticles = fitsToArticles;
    }

    public String getGtin() {
        return gtin;
    }

    public void setGtin(String gtin) {
        this.gtin = gtin;
    }

    public String getSupplementedInformation() {
        return supplementedInformation;
    }

    public void setSupplementedInformation(String supplementedInformation) {
        this.supplementedInformation = supplementedInformation;
    }

    public CVCEDirectiveAPI getCeDirective() {
        return ceDirective;
    }

    public void setCeDirective(CVCEDirectiveAPI ceDirective) {
        this.ceDirective = ceDirective;
    }

    public CVCEStandardAPI getCeStandard() {
        return ceStandard;
    }

    public void setCeStandard(CVCEStandardAPI ceStandard) {
        this.ceStandard = ceStandard;
    }

    public boolean isCeMarked() {
        return ceMarked;
    }

    public void setCeMarked(boolean ceMarked) {
        this.ceMarked = ceMarked;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getManufacturerArticleNumber() {
        return manufacturerArticleNumber;
    }

    public void setManufacturerArticleNumber(String manufacturerArticleNumber) {
        this.manufacturerArticleNumber = manufacturerArticleNumber;
    }

    public String getTrademark() {
        return trademark;
    }

    public void setTrademark(String trademark) {
        this.trademark = trademark;
    }

    public ElectronicAddressAPI getManufacturerElectronicAddress() {
        return manufacturerElectronicAddress;
    }

    public void setManufacturerElectronicAddress(ElectronicAddressAPI manufacturerElectronicAddress) {
        this.manufacturerElectronicAddress = manufacturerElectronicAddress;
    }

    public boolean isCustomerUnique() {
        return customerUnique;
    }

    public void setCustomerUnique(boolean customerUnique) {
        this.customerUnique = customerUnique;
    }

    public CVPreventiveMaintenanceAPI getPreventiveMaintenanceValidFrom() {
        return preventiveMaintenanceValidFrom;
    }

    public void setPreventiveMaintenanceValidFrom(CVPreventiveMaintenanceAPI preventiveMaintenanceValidFrom) {
        this.preventiveMaintenanceValidFrom = preventiveMaintenanceValidFrom;
    }

    public Integer getPreventiveMaintenanceNumberOfDays() {
        return preventiveMaintenanceNumberOfDays;
    }

    public void setPreventiveMaintenanceNumberOfDays(Integer preventiveMaintenanceNumberOfDays) {
        this.preventiveMaintenanceNumberOfDays = preventiveMaintenanceNumberOfDays;
    }

    public String getPreventiveMaintenanceDescription() {
        return preventiveMaintenanceDescription;
    }

    public void setPreventiveMaintenanceDescription(String preventiveMaintenanceDescription) {
        this.preventiveMaintenanceDescription = preventiveMaintenanceDescription;
    }

    public List<CategoryAPI> getExtendedCategories() {
        return extendedCategories;
    }

    public void setExtendedCategories(List<CategoryAPI> extendedCategories) {
        this.extendedCategories = extendedCategories;
    }

    public Long getReplacementDate() {
        return replacementDate;
    }

    public void setReplacementDate(Long replacementDate) {
        this.replacementDate = replacementDate;
    }

    public Boolean getInactivateRowsOnReplacement() {
        return inactivateRowsOnReplacement;
    }

    public void setInactivateRowsOnReplacement(Boolean inactivateRowsOnReplacement) {
        this.inactivateRowsOnReplacement = inactivateRowsOnReplacement;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public List<ArticleAPI> getReplacedByArticles() {
        return replacedByArticles;
    }

    public void setReplacedByArticles(List<ArticleAPI> replacedByArticles) {
        this.replacedByArticles = replacedByArticles;
    }

    public CVOrderUnitAPI getOrderUnit() {
        return orderUnit;
    }

    public void setOrderUnit(CVOrderUnitAPI orderUnit) {
        this.orderUnit = orderUnit;
    }

    public Double getArticleQuantityInOuterPackage() {
        return articleQuantityInOuterPackage;
    }

    public void setArticleQuantityInOuterPackage(Double articleQuantityInOuterPackage) {
        this.articleQuantityInOuterPackage = articleQuantityInOuterPackage;
    }

    public CVOrderUnitAPI getArticleQuantityInOuterPackageUnit() {
        return articleQuantityInOuterPackageUnit;
    }

    public void setArticleQuantityInOuterPackageUnit(CVOrderUnitAPI articleQuantityInOuterPackageUnit) {
        this.articleQuantityInOuterPackageUnit = articleQuantityInOuterPackageUnit;
    }

    public Double getPackageContent() {
        return packageContent;
    }

    public void setPackageContent(Double packageContent) {
        this.packageContent = packageContent;
    }

    public CVPackageUnitAPI getPackageContentUnit() {
        return packageContentUnit;
    }

    public void setPackageContentUnit(CVPackageUnitAPI packageContentUnit) {
        this.packageContentUnit = packageContentUnit;
    }

    public Integer getPackageLevelBase() {
        return packageLevelBase;
    }

    public void setPackageLevelBase(Integer packageLevelBase) {
        this.packageLevelBase = packageLevelBase;
    }

    public Integer getPackageLevelMiddle() {
        return packageLevelMiddle;
    }

    public void setPackageLevelMiddle(Integer packageLevelMiddle) {
        this.packageLevelMiddle = packageLevelMiddle;
    }

    public Integer getPackageLevelTop() {
        return packageLevelTop;
    }

    public void setPackageLevelTop(Integer packageLevelTop) {
        this.packageLevelTop = packageLevelTop;
    }

    public List<ResourceSpecificPropertyAPI> getCategoryPropertys() {
        return categoryPropertys;
    }

    public void setCategoryPropertys(List<ResourceSpecificPropertyAPI> categoryPropertys) {
        this.categoryPropertys = categoryPropertys;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isNumberEditable() {
        return numberEditable;
    }

    public void setNumberEditable(boolean numberEditable) {
        this.numberEditable = numberEditable;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

}
