package se.inera.hjalpmedelstjansten.model.api.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Validates if a time (given in milliseconds) is a time during today or a future
 * time.
 *
 */
public class TodayOrFutureDateValidator implements ConstraintValidator<ValidTodayOrFutureDate, Long> {

    @Override
    public void initialize(ValidTodayOrFutureDate constraintAnnotation) {

    }

    @Override
    public boolean isValid(Long dateAsMillis, ConstraintValidatorContext context) {
        if( dateAsMillis == null || dateAsMillis < 0 ) {
            // these cases should be handled by other bean validations
            return true;
        }
        // valid from must be a future date
        Instant nowInstant = Instant.now();
        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime todayStartOfDay = ZonedDateTime.ofInstant(nowInstant, zoneId).toLocalDate().atStartOfDay(zoneId);
        Instant validFromInstant = Instant.ofEpochMilli(dateAsMillis);
        ZonedDateTime validFrom = ZonedDateTime.ofInstant(validFromInstant, zoneId);
        if( validFrom.isBefore(todayStartOfDay) ) {
            return false;
        }
        return true;
    }



}
