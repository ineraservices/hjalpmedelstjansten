package se.inera.hjalpmedelstjansten.model.api;

import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;

import java.math.BigDecimal;

/**
 * Top-level interface for the different types of rows in the application, making it a bit easier to implement more general methods from the API models.
 */

public interface HJMTJRow {

   Long getId();
   void setPrice(BigDecimal price);
   ArticleAPI getArticle();
   String getStatus();
   BigDecimal getPrice();
   Long getValidFrom();
   Integer getLeastOrderQuantity();
   Integer getDeliveryTime();
   CVGuaranteeUnitAPI getWarrantyQuantityUnit();
   Integer getWarrantyQuantity();
   CVPreventiveMaintenanceAPI getWarrantyValidFrom();
   String getWarrantyTerms();

}