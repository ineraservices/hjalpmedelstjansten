package se.inera.hjalpmedelstjansten.model.api;

import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidAgreementAPICross;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.util.List;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 *
 */
@ValidAgreementAPICross
public class AgreementAPI {

    public enum Include {
        ALL, MINE
    }

    private Long id;

    @Size(max = 255, message = "{agreement.name.wrongLength}")
    private String agreementName;

    @NotNull(message = "{agreement.number.notNull}")
    @Size(min = 1, max = 255, message = "{agreement.number.wrongLength}")
    private String agreementNumber;

    @NotNull(message = "{agreement.validFrom.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{agreement.validFrom.invalid}")
    private Long validFrom;

    @NotNull(message = "{agreement.validTo.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{agreement.validTo.invalid}")
    private Long validTo;

    @DecimalMin(value = "0", inclusive = true, message = "{agreement.extensionOptionTo.invalid}")
    private Long extensionOptionTo;

    @DecimalMin(value = "0", inclusive = true, message = "{agreement.sendReminderOn.invalid}")
    private Long sendReminderOn;

    @DecimalMin(value = "0", inclusive = true, message = "{agreement.endDate.invalid}")
    private Long endDate;

    private String status;

    @NotNull(message = "{agreement.supplierOrganization.notNull}")
    private OrganizationAPI supplierOrganization;

    private OrganizationAPI customerOrganization;

    private BusinessLevelAPI customerBusinessLevel;

    private List<UserAPI> supplierContactPersons;

    @NotNull(message = "{agreement.customerPricelistApprovers.notNull}")
    @Size(min = 1, message = "{agreement.customerPricelistApprovers.wrongLength}")
    private List<UserAPI> customerPricelistApprovers;

    private List<OrganizationAPI> sharedWithCustomers;
    private List<BusinessLevelAPI> sharedWithCustomersBusinessLevels;

    @NotNull(message = "{agreement.deliveryTimeH.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{agreement.deliveryTimeH.invalid}")
    private Integer deliveryTimeH;

    @NotNull(message = "{agreement.deliveryTimeT.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{agreement.deliveryTimeT.invalid}")
    private Integer deliveryTimeT;

    @NotNull(message = "{agreement.deliveryTimeI.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{agreement.deliveryTimeI.invalid}")
    private Integer deliveryTimeI;

    @NotNull(message = "{agreement.deliveryTimeR.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{agreement.deliveryTimeR.invalid}")
    private Integer deliveryTimeR;

    @NotNull(message = "{agreement.deliveryTimeTJ.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{agreement.deliveryTimeTJ.invalid}")
    private Integer deliveryTimeTJ;

    @DecimalMin(value = "1", inclusive = true, message = "{agreement.warrantyQuantityH.invalid}")
    private Integer warrantyQuantityH;

    @DecimalMin(value = "1", inclusive = true, message = "{agreement.warrantyQuantityT.invalid}")
    private Integer warrantyQuantityT;

    @DecimalMin(value = "1", inclusive = true, message = "{agreement.warrantyQuantityI.invalid}")
    private Integer warrantyQuantityI;

    @DecimalMin(value = "1", inclusive = true, message = "{agreement.warrantyQuantityR.invalid}")
    private Integer warrantyQuantityR;

    @DecimalMin(value = "1", inclusive = true, message = "{agreement.warrantyQuantityTJ.invalid}")
    private Integer warrantyQuantityTJ;

    private CVGuaranteeUnitAPI warrantyQuantityHUnit;
    private CVGuaranteeUnitAPI warrantyQuantityTUnit;
    private CVGuaranteeUnitAPI warrantyQuantityIUnit;
    private CVGuaranteeUnitAPI warrantyQuantityRUnit;
    private CVGuaranteeUnitAPI warrantyQuantityTJUnit;

    private CVPreventiveMaintenanceAPI warrantyValidFromH;
    private CVPreventiveMaintenanceAPI warrantyValidFromT;
    private CVPreventiveMaintenanceAPI warrantyValidFromI;
    private CVPreventiveMaintenanceAPI warrantyValidFromR;
    private CVPreventiveMaintenanceAPI warrantyValidFromTJ;

    @Size(min = 1, max = 255, message = "{agreement.warrantyTermsH.wrongLength}")
    private String warrantyTermsH;

    @Size(min = 1, max = 255, message = "{agreement.warrantyTermsT.wrongLength}")
    private String warrantyTermsT;

    @Size(min = 1, max = 255, message = "{agreement.warrantyTermsI.wrongLength}")
    private String warrantyTermsI;

    @Size(min = 1, max = 255, message = "{agreement.warrantyTermsR.wrongLength}")
    private String warrantyTermsR;

    @Size(min = 1, max = 255, message = "{agreement.warrantyTermsTJ.wrongLength}")
    private String warrantyTermsTJ;

    private List<AgreementPricelistAPI> pricelists;

    private Boolean articlesExistOnAgreement;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgreementName() {
        return agreementName;
    }

    public void setAgreementName(String agreementName) {
        this.agreementName = agreementName;
    }

    public String getAgreementNumber() {
        return agreementNumber;
    }

    public void setAgreementNumber(String agreementNumber) {
        this.agreementNumber = agreementNumber;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Long getValidTo() {
        return validTo;
    }

    public void setValidTo(Long validTo) {
        this.validTo = validTo;
    }

    public Long getExtensionOptionTo() {
        return extensionOptionTo;
    }

    public void setExtensionOptionTo(Long extensionOptionTo) {
        this.extensionOptionTo = extensionOptionTo;
    }

    public Long getSendReminderOn() {
        return sendReminderOn;
    }

    public void setSendReminderOn(Long sendReminderOn) {
        this.sendReminderOn = sendReminderOn;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public OrganizationAPI getSupplierOrganization() {
        return supplierOrganization;
    }

    public void setSupplierOrganization(OrganizationAPI supplierOrganization) {
        this.supplierOrganization = supplierOrganization;
    }

    public BusinessLevelAPI getCustomerBusinessLevel() {
        return customerBusinessLevel;
    }

    public void setCustomerBusinessLevel(BusinessLevelAPI customerBusinessLevel) {
        this.customerBusinessLevel = customerBusinessLevel;
    }

    public List<UserAPI> getSupplierContactPersons() {
        return supplierContactPersons;
    }

    public void setSupplierContactPersons(List<UserAPI> supplierContactPersons) {
        this.supplierContactPersons = supplierContactPersons;
    }

    public List<UserAPI> getCustomerPricelistApprovers() {
        return customerPricelistApprovers;
    }

    public void setCustomerPricelistApprovers(List<UserAPI> customerPricelistApprovers) {
        this.customerPricelistApprovers = customerPricelistApprovers;
    }

    public List<OrganizationAPI> getSharedWithCustomers() {
        return sharedWithCustomers;
    }

    public void setSharedWithCustomers(List<OrganizationAPI> sharedWithCustomers) {
        this.sharedWithCustomers = sharedWithCustomers;
    }

    public OrganizationAPI getCustomerOrganization() {
        return customerOrganization;
    }

    public void setCustomerOrganization(OrganizationAPI customerOrganization) {
        this.customerOrganization = customerOrganization;
    }

    public Integer getDeliveryTimeH() {
        return deliveryTimeH;
    }

    public void setDeliveryTimeH(Integer deliveryTimeH) {
        this.deliveryTimeH = deliveryTimeH;
    }

    public Integer getDeliveryTimeT() {
        return deliveryTimeT;
    }

    public void setDeliveryTimeT(Integer deliveryTimeT) {
        this.deliveryTimeT = deliveryTimeT;
    }

    public Integer getDeliveryTimeI() {
        return deliveryTimeI;
    }

    public void setDeliveryTimeI(Integer deliveryTimeI) {
        this.deliveryTimeI = deliveryTimeI;
    }

    public Integer getDeliveryTimeR() {
        return deliveryTimeR;
    }

    public void setDeliveryTimeR(Integer deliveryTimeR) {
        this.deliveryTimeR = deliveryTimeR;
    }

    public Integer getDeliveryTimeTJ() {
        return deliveryTimeTJ;
    }

    public void setDeliveryTimeTJ(Integer deliveryTimeTJ) {
        this.deliveryTimeTJ = deliveryTimeTJ;
    }

    public Integer getWarrantyQuantityH() {
        return warrantyQuantityH;
    }

    public void setWarrantyQuantityH(Integer warrantyQuantityH) {
        this.warrantyQuantityH = warrantyQuantityH;
    }

    public Integer getWarrantyQuantityT() {
        return warrantyQuantityT;
    }

    public void setWarrantyQuantityT(Integer warrantyQuantityT) {
        this.warrantyQuantityT = warrantyQuantityT;
    }

    public Integer getWarrantyQuantityI() {
        return warrantyQuantityI;
    }

    public void setWarrantyQuantityI(Integer warrantyQuantityI) {
        this.warrantyQuantityI = warrantyQuantityI;
    }

    public Integer getWarrantyQuantityR() {
        return warrantyQuantityR;
    }

    public void setWarrantyQuantityR(Integer warrantyQuantityR) {
        this.warrantyQuantityR = warrantyQuantityR;
    }

    public Integer getWarrantyQuantityTJ() {
        return warrantyQuantityTJ;
    }

    public void setWarrantyQuantityTJ(Integer warrantyQuantityTJ) {
        this.warrantyQuantityTJ = warrantyQuantityTJ;
    }

    public CVGuaranteeUnitAPI getWarrantyQuantityHUnit() {
        return warrantyQuantityHUnit;
    }

    public void setWarrantyQuantityHUnit(CVGuaranteeUnitAPI warrantyQuantityHUnit) {
        this.warrantyQuantityHUnit = warrantyQuantityHUnit;
    }

    public CVGuaranteeUnitAPI getWarrantyQuantityTUnit() {
        return warrantyQuantityTUnit;
    }

    public void setWarrantyQuantityTUnit(CVGuaranteeUnitAPI warrantyQuantityTUnit) {
        this.warrantyQuantityTUnit = warrantyQuantityTUnit;
    }

    public CVGuaranteeUnitAPI getWarrantyQuantityIUnit() {
        return warrantyQuantityIUnit;
    }

    public void setWarrantyQuantityIUnit(CVGuaranteeUnitAPI warrantyQuantityIUnit) {
        this.warrantyQuantityIUnit = warrantyQuantityIUnit;
    }

    public CVGuaranteeUnitAPI getWarrantyQuantityRUnit() {
        return warrantyQuantityRUnit;
    }

    public void setWarrantyQuantityRUnit(CVGuaranteeUnitAPI warrantyQuantityRUnit) {
        this.warrantyQuantityRUnit = warrantyQuantityRUnit;
    }

    public CVGuaranteeUnitAPI getWarrantyQuantityTJUnit() {
        return warrantyQuantityTJUnit;
    }

    public void setWarrantyQuantityTJUnit(CVGuaranteeUnitAPI warrantyQuantityTJUnit) {
        this.warrantyQuantityTJUnit = warrantyQuantityTJUnit;
    }

    public CVPreventiveMaintenanceAPI getWarrantyValidFromH() {
        return warrantyValidFromH;
    }

    public void setWarrantyValidFromH(CVPreventiveMaintenanceAPI warrantyValidFromH) {
        this.warrantyValidFromH = warrantyValidFromH;
    }

    public CVPreventiveMaintenanceAPI getWarrantyValidFromT() {
        return warrantyValidFromT;
    }

    public void setWarrantyValidFromT(CVPreventiveMaintenanceAPI warrantyValidFromT) {
        this.warrantyValidFromT = warrantyValidFromT;
    }

    public CVPreventiveMaintenanceAPI getWarrantyValidFromI() {
        return warrantyValidFromI;
    }

    public void setWarrantyValidFromI(CVPreventiveMaintenanceAPI warrantyValidFromI) {
        this.warrantyValidFromI = warrantyValidFromI;
    }

    public CVPreventiveMaintenanceAPI getWarrantyValidFromR() {
        return warrantyValidFromR;
    }

    public void setWarrantyValidFromR(CVPreventiveMaintenanceAPI warrantyValidFromR) {
        this.warrantyValidFromR = warrantyValidFromR;
    }

    public CVPreventiveMaintenanceAPI getWarrantyValidFromTJ() {
        return warrantyValidFromTJ;
    }

    public void setWarrantyValidFromTJ(CVPreventiveMaintenanceAPI warrantyValidFromTJ) {
        this.warrantyValidFromTJ = warrantyValidFromTJ;
    }

    public String getWarrantyTermsH() {
        return warrantyTermsH;
    }

    public void setWarrantyTermsH(String warrantyTermsH) {
        this.warrantyTermsH = warrantyTermsH;
    }

    public String getWarrantyTermsT() {
        return warrantyTermsT;
    }

    public void setWarrantyTermsT(String warrantyTermsT) {
        this.warrantyTermsT = warrantyTermsT;
    }

    public String getWarrantyTermsI() {
        return warrantyTermsI;
    }

    public void setWarrantyTermsI(String warrantyTermsI) {
        this.warrantyTermsI = warrantyTermsI;
    }

    public String getWarrantyTermsR() {
        return warrantyTermsR;
    }

    public void setWarrantyTermsR(String warrantyTermsR) {
        this.warrantyTermsR = warrantyTermsR;
    }

    public String getWarrantyTermsTJ() {
        return warrantyTermsTJ;
    }

    public void setWarrantyTermsTJ(String warrantyTermsTJ) {
        this.warrantyTermsTJ = warrantyTermsTJ;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<BusinessLevelAPI> getSharedWithCustomersBusinessLevels() {
        return sharedWithCustomersBusinessLevels;
    }

    public void setSharedWithCustomersBusinessLevels(List<BusinessLevelAPI> sharedWithCustomersBusinessLevels) {
        this.sharedWithCustomersBusinessLevels = sharedWithCustomersBusinessLevels;
    }

    public List<AgreementPricelistAPI> getPricelists() {
        return pricelists;
    }

    public void setPricelists(List<AgreementPricelistAPI> pricelists) {
        this.pricelists = pricelists;
    }

    public Boolean isArticlesExistOnAgreement() {
        return articlesExistOnAgreement;
    }

    public void setArticlesExistOnAgreement(Boolean articlesExistOnAgreement) {
        this.articlesExistOnAgreement = articlesExistOnAgreement;
    }

}
