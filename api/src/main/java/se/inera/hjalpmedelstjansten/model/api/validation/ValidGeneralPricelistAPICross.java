package se.inera.hjalpmedelstjansten.model.api.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Cross validation of general pricelists
 *
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {GeneralPricelistAPICrossValidator.class})
public @interface ValidGeneralPricelistAPICross {

    String message() default "Felaktigt API";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
