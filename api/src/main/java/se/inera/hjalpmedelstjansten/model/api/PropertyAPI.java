package se.inera.hjalpmedelstjansten.model.api;

import java.util.Map;


public class PropertyAPI {
    
    private Map<Object, Object> values;

    public Map<Object, Object> getValues() {
        return values;
    }

    public void setValues(Map<Object, Object> values) {
        this.values = values;
    }
    
}
