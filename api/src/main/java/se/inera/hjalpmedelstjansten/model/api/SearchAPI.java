package se.inera.hjalpmedelstjansten.model.api;

import java.util.List;


public class SearchAPI {
    
    private List<SearchCategorySpecificPropertyAPI> properties;

    public List<SearchCategorySpecificPropertyAPI> getProperties() {
        return properties;
    }

    public void setProperties(List<SearchCategorySpecificPropertyAPI> properties) {
        this.properties = properties;
    }

}
