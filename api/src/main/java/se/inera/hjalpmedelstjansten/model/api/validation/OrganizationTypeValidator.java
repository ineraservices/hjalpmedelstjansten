package se.inera.hjalpmedelstjansten.model.api.validation;

import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;


public class OrganizationTypeValidator implements ConstraintValidator<ValidOrganizationType, OrganizationAPI> {


    @Override
    public void initialize(ValidOrganizationType constraintAnnotation) {

    }

    @Override
    public boolean isValid(OrganizationAPI organizationAPI, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        if( organizationAPI.getOrganizationType() == null ) {
            context.
                    buildConstraintViolationWithTemplate( "{organization.organizationType.notNull}" ).
                    addPropertyNode("organizationType").
                    addConstraintViolation();
            return false;
        }
        return true;
    }



}
