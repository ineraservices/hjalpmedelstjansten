package se.inera.hjalpmedelstjansten.model.api;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.util.Date;

public class DeletedUserAPI {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column
    private Long oldUniqueId;

    @Column
    private Long deletedByUserId;

    @Column
    private String reasonCode;

    @Column
    private Long deletedUserOrg;

    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date dateDeleted;



    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setOldUniqueId(Long id) {
        this.oldUniqueId = id;
    }

    public Long getOldUniqueId() {
        return oldUniqueId;
    }

    public void setDeletedByUserId(Long id) {
        this.deletedByUserId = id;
    }

    public Long getDeletedByUserId() {
        return deletedByUserId;
    }

    public String getReasonCode() {return reasonCode ;}

    public void setReasonCode(String code) { this.reasonCode = code;}

    public Long getDeletedUserOrg(){
        return deletedUserOrg;
    }

    public void setDeletedUserOrg(Long organizationId){
        this.deletedUserOrg = organizationId;
    }

    public void setDeletedDate(Date dateDeleted) {
        this.dateDeleted = dateDeleted;
    }

    public Date getDeletedDate() {
        return dateDeleted;
    }
}
