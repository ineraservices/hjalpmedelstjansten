package se.inera.hjalpmedelstjansten.model.api.media;


public class MediaImageAPI extends MediaAPI {
    
    private String alternativeText;
    
    private boolean mainImage;
    
    private String fileName;
    
    public String getAlternativeText() {
        return alternativeText;
    }

    public void setAlternativeText(String alternativeText) {
        this.alternativeText = alternativeText;
    }

    public boolean isMainImage() {
        return mainImage;
    }

    public void setMainImage(boolean mainImage) {
        this.mainImage = mainImage;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

}
