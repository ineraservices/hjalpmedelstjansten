package se.inera.hjalpmedelstjansten.model.api;

import java.io.Serializable;
import java.util.List;

public class ChangeCategoryOnProductAPI implements Serializable {



    private CategoryAPI toCategory;
    private List<Long> products;




    public CategoryAPI getToCategory() {
        return toCategory;
    }
    public void setToCategory(CategoryAPI toCategory){this.toCategory = toCategory;}



    public List<Long> getProducts() {
        return products;
    }

    public void setProducts(List<Long> products){this.products = products;}





}
