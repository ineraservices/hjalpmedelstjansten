package se.inera.hjalpmedelstjansten.model.api;

import java.util.Set;


public class ErrorAPI {
    
    private Set<ErrorMessageAPI> errors;

    public Set<ErrorMessageAPI> getErrors() {
        return errors;
    }

    public void setErrors(Set<ErrorMessageAPI> errors) {
        this.errors = errors;
    }
    
}
