package se.inera.hjalpmedelstjansten.model.api.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {ArticleStatusValidator.class})
public @interface ValidArticleStatus {

    String message() default "{article.status.invalid}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
