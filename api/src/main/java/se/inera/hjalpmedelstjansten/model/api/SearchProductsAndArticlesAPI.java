package se.inera.hjalpmedelstjansten.model.api;

import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Product;

/**
 * Contains results for searches on product and articles
 *
 */
public class SearchProductsAndArticlesAPI {

    public enum Type {
        PRODUCT, ARTICLE
    }

    private Long id;
    private Type type;
    private Article.Type articleType;
    private String name;
    private String number;
    private Product.Status status;
    private String code;
    private ProductAPI basedOnProduct;
    private Boolean deleteable;
    private String organizationName;
    private Long organizationId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Article.Type getArticleType() {
        return articleType;
    }

    public void setArticleType(Article.Type articleType) {
        this.articleType = articleType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Product.Status getStatus() {
        return status;
    }

    public void setStatus(Product.Status status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ProductAPI getBasedOnProduct() {
        return basedOnProduct;
    }

    public void setBasedOnProduct(ProductAPI basedOnProduct) {
        this.basedOnProduct = basedOnProduct;
    }

    public Boolean getDeleteable() {
        return deleteable;
    }

    public void setDeleteable(Boolean deleteable) {
        this.deleteable = deleteable;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

}


