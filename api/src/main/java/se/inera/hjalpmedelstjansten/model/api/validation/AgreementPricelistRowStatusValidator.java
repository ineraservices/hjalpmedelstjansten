package se.inera.hjalpmedelstjansten.model.api.validation;

import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelistRow;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;


public class AgreementPricelistRowStatusValidator implements ConstraintValidator<ValidAgreementPricelistRowStatus, String> {


    @Override
    public void initialize(ValidAgreementPricelistRowStatus constraintAnnotation) {

    }

    @Override
    public boolean isValid(String status, ConstraintValidatorContext context) {
        if( status != null ) {
            try {
                AgreementPricelistRow.Status.valueOf(status);
            } catch( IllegalArgumentException ex ) {
                return false;
            }
        }
        return true;
    }

}
