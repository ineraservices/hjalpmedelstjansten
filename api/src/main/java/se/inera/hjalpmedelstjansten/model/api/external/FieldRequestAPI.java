package se.inera.hjalpmedelstjansten.model.api.external;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * API request object
 *
 */
public class FieldRequestAPI {
    
    public static final String CREDENTIALS_PROPERTY_NAME = "Credentials";
    public static final String NODE_ID_PROPERTY_NAME = "NodeId";
    public static final String ID_PATH_LIST_PROPERTY_NAME = "IdPathList";
    
    private String credentials;
    private String nodeId;
    private List<List<String>> idPathList;

    @JsonProperty(CREDENTIALS_PROPERTY_NAME)
    public String getCredentials() {
        return credentials;
    }

    @JsonProperty(CREDENTIALS_PROPERTY_NAME)
    public void setCredentials(String credentials) {
        this.credentials = credentials;
    }

    @JsonProperty(NODE_ID_PROPERTY_NAME)
    public String getNodeId() {
        return nodeId;
    }

    @JsonProperty(NODE_ID_PROPERTY_NAME)
    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    @JsonProperty(ID_PATH_LIST_PROPERTY_NAME)
    public List<List<String>> getIdPathList() {
        return idPathList;
    }

    @JsonProperty(ID_PATH_LIST_PROPERTY_NAME)
    public void setIdPathList(List<List<String>> idPathList) {
        this.idPathList = idPathList;
    }

}
