package se.inera.hjalpmedelstjansten.model.api.external;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


public class FieldResponseItemAPI {
    
    public enum ValueType {
        DECIMAL("decimal"),
        REFERENCE_TO("referenceto"), 
        STRING("string"),
        BOOLEAN("boolean"),
        DATETIME("datetime");
        
        private String actualName;
        
        public String getActualName() {
            return actualName;
        }
        
        private ValueType(String actualName) {
            this.actualName = actualName;
        }
    }
    
    public static final String ID_PATH_PROPERTY_NAME = "IdPath";
    public static final String VALUE_TYPE_PROPERTY_NAME = "ValueType";
    public static final String NAME_PROPERTY_NAME = "Name";
    
    private List<String> idPath;
    private String valueType;
    private String name;

    @JsonProperty(ID_PATH_PROPERTY_NAME)
    public List<String> getIdPath() {
        return idPath;
    }

    @JsonProperty(ID_PATH_PROPERTY_NAME)
    public void setIdPath(List<String> idPath) {
        this.idPath = idPath;
    }

    @JsonProperty(VALUE_TYPE_PROPERTY_NAME)
    public String getValueType() {
        return valueType;
    }

    @JsonProperty(VALUE_TYPE_PROPERTY_NAME)
    public void setValueType(String valueType) {
        this.valueType = valueType;
    }
    
    @JsonProperty(NAME_PROPERTY_NAME)
    public String getName() {
        return name;
    }

    @JsonProperty(NAME_PROPERTY_NAME)
    public void setName(String name) {
        this.name = name;
    }

}
