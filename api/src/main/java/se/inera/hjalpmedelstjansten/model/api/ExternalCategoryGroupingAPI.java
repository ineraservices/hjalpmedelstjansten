package se.inera.hjalpmedelstjansten.model.api;

import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.ExternalCategoryGrouping;

import java.io.Serializable;

public class ExternalCategoryGroupingAPI implements Serializable {

    private Long id;
    private String name;
    private String description;
    private Long parent;
    private Integer displayOrder;
    private Long category;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }


    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }


    public Long getCategory() {
        return category;
    }

    public void setCategory(Long category) {
        this.category = category;
    }
}


