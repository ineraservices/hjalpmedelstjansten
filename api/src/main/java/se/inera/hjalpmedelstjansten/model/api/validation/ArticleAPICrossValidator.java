package se.inera.hjalpmedelstjansten.model.api.validation;

import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * Cross validator for article. Validates CE and that article is either based
 * on product or fits to article or product
 *
 */
public class ArticleAPICrossValidator implements ConstraintValidator<ValidArticleAPICross, ArticleAPI> {


    @Override
    public void initialize(ValidArticleAPICross constraintAnnotation) {

    }

    @Override
    public boolean isValid(ArticleAPI articleAPI, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        boolean valid = true;
        if( !articleAPI.isCeMarked() && articleAPI.getCeDirective() != null ) {
            context.buildConstraintViolationWithTemplate( "{article.noCeButDirective}" ).addConstraintViolation();
            valid = false;
        }
        if( articleAPI.getBasedOnProduct() == null && (articleAPI.getFitsToProducts() == null || articleAPI.getFitsToProducts().isEmpty()) && (articleAPI.getFitsToArticles() == null || articleAPI.getFitsToArticles().isEmpty())) {
            context.buildConstraintViolationWithTemplate( "{article.productRelation.notNull}" ).addConstraintViolation();
            valid = false;
        }
        if( (articleAPI.getReplacementDate() != null && articleAPI.getInactivateRowsOnReplacement() == null) ||
                (articleAPI.getReplacementDate() == null && articleAPI.getInactivateRowsOnReplacement() != null)) {
            context.buildConstraintViolationWithTemplate( "{article.replacementDateInactivate.notNull}" ).addConstraintViolation();
            valid = false;
        }
        return valid;
    }

}
