package se.inera.hjalpmedelstjansten.model.api;

import se.inera.hjalpmedelstjansten.model.entity.Article;

import java.io.Serializable;
import java.util.List;


public class CategoryAPI implements Serializable {
    
    private Long id;
    private String name;
    private String code;
    private String description;
    private Article.Type articleType;
    private Integer level;
    private long parentId;
    private Long articleCount;
    private Long productCount;
    private List categorySpecificProperties;


    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
    
    public Long getId() {
        return id;
    }

    public Integer getLevel() {return level;}

    public void setLevel(Integer level) {this.level = level;}

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Article.Type getArticleType() {
        return articleType;
    }

    public void setArticleType(Article.Type articleType) {
        this.articleType = articleType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getArticleCount() { return articleCount; }

    public void setArticleCount(Long articleCount) { this.articleCount = articleCount; }

    public Long getProductCount() { return productCount; }

    public void setProductCount(Long productCount) { this.productCount = productCount; }

    public List getCategorySpecificProperties() { return categorySpecificProperties; }

    public void setCategorySpecificProperties(List<CategorySpecificPropertyAPI> categorySpecificProperties) {this.categorySpecificProperties = categorySpecificProperties; }


}
