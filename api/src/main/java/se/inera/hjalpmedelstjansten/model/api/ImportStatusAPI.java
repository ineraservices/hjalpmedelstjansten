package se.inera.hjalpmedelstjansten.model.api;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.util.Date;

public class ImportStatusAPI {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = true)
    private String fileName;

    @Column(nullable = false)
    private String changedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date importStartDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date importEndDate;

    private Integer readStatus;

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setChangedBy(String changedBy){this.changedBy = changedBy;}

    public String getChangedBy(){return this.changedBy;}

    public void setImportStartDate(Date importStartDate) {
        this.importStartDate = importStartDate;
    }

    public Date getImportStartDate() {
        return importStartDate;
    }

    public void setImportEndDate(Date importEndDate) {
        this.importEndDate = importEndDate;
    }

    public Date getImportEndDate() {
        return importEndDate;
    }

    public void setReadStatus(int readStatus){this.readStatus = readStatus;}

    public int getReadStatus(){
        return readStatus;
    }

}
