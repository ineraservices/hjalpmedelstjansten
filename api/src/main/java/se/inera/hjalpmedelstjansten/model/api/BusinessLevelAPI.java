package se.inera.hjalpmedelstjansten.model.api;

import java.io.Serializable;


public class BusinessLevelAPI implements Serializable {
    
    private Long id;
    private String name;    
    private String status;
    private OrganizationAPI organization;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public OrganizationAPI getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationAPI organization) {
        this.organization = organization;
    }
    
}
