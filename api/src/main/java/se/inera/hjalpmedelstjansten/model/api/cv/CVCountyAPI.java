package se.inera.hjalpmedelstjansten.model.api.cv;

import java.io.Serializable;
import java.util.List;


public class CVCountyAPI implements Serializable {
    
    private Long id;    
    private String name;
    private String code;
    private List<CVMunicipalityAPI> municipalities;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<CVMunicipalityAPI> getMunicipalities() {
        return municipalities;
    }

    public void setMunicipalities(List<CVMunicipalityAPI> municipalities) {
        this.municipalities = municipalities;
    }

}
