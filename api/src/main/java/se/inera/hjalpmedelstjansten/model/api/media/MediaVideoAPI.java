package se.inera.hjalpmedelstjansten.model.api.media;


public class MediaVideoAPI extends MediaAPI {
    
    private String alternativeText;
    
    public String getAlternativeText() {
        return alternativeText;
    }

    public void setAlternativeText(String alternativeText) {
        this.alternativeText = alternativeText;
    }
            
}
