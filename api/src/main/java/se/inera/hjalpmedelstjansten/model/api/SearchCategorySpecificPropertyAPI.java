package se.inera.hjalpmedelstjansten.model.api;

import java.util.Map;


public class SearchCategorySpecificPropertyAPI {
    
    private long property;
    private Map<String, Object> values;

    public long getProperty() {
        return property;
    }

    public void setProperty(long category) {
        this.property = category;
    }

    public Map<String, Object> getValues() {
        return values;
    }

    public void setValues(Map<String, Object> values) {
        this.values = values;
    }
        
}
