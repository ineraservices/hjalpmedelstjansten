package se.inera.hjalpmedelstjansten.model.api;

import se.inera.hjalpmedelstjansten.model.entity.Category;

import java.io.Serializable;


public class CategoryExtAPI implements Serializable {


    private Integer level;
    private Category category;
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category){this.category = category;}
    public Integer getLevel(){return level;}
    public void setLevel(Integer level) {this.level = level;}
}
