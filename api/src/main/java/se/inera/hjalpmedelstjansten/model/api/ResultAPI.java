package se.inera.hjalpmedelstjansten.model.api;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class ResultAPI<T> {

    private List<T> elements = new LinkedList<>();

    private Set<ErrorMessageAPI> validationMessages = new HashSet<>();

    public void add(T element) {
        elements.add(element);
    }

    public void add(Set<ErrorMessageAPI> validationMessages) {
        this.validationMessages.addAll(validationMessages);
    }

    public List<T> getElements() {
        return elements;
    }

    public Set<ErrorMessageAPI> getValidationMessages() {
        return validationMessages;
    }
}
