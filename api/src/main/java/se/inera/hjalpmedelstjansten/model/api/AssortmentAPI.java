package se.inera.hjalpmedelstjansten.model.api;

import se.inera.hjalpmedelstjansten.model.api.cv.CVCountyAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVMunicipalityAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidAssortmentAPICross;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.util.List;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 *
 */
@ValidAssortmentAPICross
public class AssortmentAPI {

    public enum Include {
        ALL, MINE
    }

    private Long id;

    @NotNull(message = "{assortment.name.notNull}")
    @Size(min = 3, max = 255, message = "{assortment.name.wrongLength}")
    private String name;

    @NotNull(message = "{assortment.validFrom.notNull}")
    @DecimalMin(value = "0", message = "{assortment.validFrom.invalid}")
    private Long validFrom;

    @DecimalMin(value = "0", message = "{assortment.validTo.invalid}")
    private Long validTo;

    private CVCountyAPI county;

    private List<CVMunicipalityAPI> municipalities;

    private OrganizationAPI customer;

    private List<UserAPI> managers;

    private List<ExternalCategoryGroupingAPI> externalCategoriesGroupings;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OrganizationAPI getCustomer() {
        return customer;
    }

    public void setCustomer(OrganizationAPI customer) {
        this.customer = customer;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Long getValidTo() {
        return validTo;
    }

    public void setValidTo(Long validTo) {
        this.validTo = validTo;
    }

    public CVCountyAPI getCounty() {
        return county;
    }

    public void setCounty(CVCountyAPI county) {
        this.county = county;
    }

    public List<CVMunicipalityAPI> getMunicipalities() {
        return municipalities;
    }

    public void setMunicipalities(List<CVMunicipalityAPI> municipalities) {
        this.municipalities = municipalities;
    }

    public List<UserAPI> getManagers() {
        return managers;
    }

    public void setManagers(List<UserAPI> managers) {
        this.managers = managers;
    }

    public List<ExternalCategoryGroupingAPI> getExternalCategoriesGroupings() {
        return externalCategoriesGroupings;
    }

    public void setExternalCategoriesGroupings(List<ExternalCategoryGroupingAPI> externalCategoriesGroupings) {
        this.externalCategoriesGroupings = externalCategoriesGroupings;
    }

}
