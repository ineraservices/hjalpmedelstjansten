package se.inera.hjalpmedelstjansten.model.api;

import se.inera.hjalpmedelstjansten.model.api.validation.ValidTodayOrFutureDate;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 *
 */
public class GeneralPricelistPricelistAPI {

    private Long id;

    @NotNull(message = "{pricelist.number.notNull}")
    @Size(min = 1, max = 255, message = "{pricelist.number.wrongLength}")
    private String number;

    @NotNull(message = "{pricelist.validFrom.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{pricelist.validFrom.invalid}")
    @ValidTodayOrFutureDate(message = "{pricelist.validFrom.notFuture}")
    private Long validFrom;

    private GeneralPricelistAPI generalPricelist;

    private String status;

    private Boolean hasPricelistRows;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public GeneralPricelistAPI getGeneralPricelist() {
        return generalPricelist;
    }

    public void setGeneralPricelist(GeneralPricelistAPI generalPricelist) {
        this.generalPricelist = generalPricelist;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getHasPricelistRows() {
        return hasPricelistRows;
    }

    public void setHasPricelistRows(Boolean hasPricelistRows) {
        this.hasPricelistRows = hasPricelistRows;
    }

}
