package se.inera.hjalpmedelstjansten.model.api.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * Validates GLN and GTIN-13 by multiplying every other digit with 1 and the others
 * with 3 except the checkdigit which is calculated by adding up the multiplied values
 * and then subtracting the sum from the nearest equal or higher multiple of ten.
 * e.g. if the sum was 143, the nearest equal or higher multiple of ten is 150 so
 * we do 150-143 which is 7 and that is also the check digit.
 *
 */
public class GlnAndGtinValidator implements ConstraintValidator<ValidGlnOrGtin, String> {


    @Override
    public void initialize(ValidGlnOrGtin constraintAnnotation) {

    }

    @Override
    public boolean isValid(String gln, ConstraintValidatorContext context) {
        if( gln == null ) {
            // null check must be separate since it differs
            return true;
        }
        // since we are sharing this validator between gln and gtin, but we want the
        // message to be appropriate we build the message based on the default message
        // template and prefix. the default message comes as "{product.gtin}", but
        // we want "product.gtin" + suffix so we remove curly braces
        String defaultConstraintMessageTemplate = context.getDefaultConstraintMessageTemplate();
        defaultConstraintMessageTemplate = defaultConstraintMessageTemplate.substring(1, defaultConstraintMessageTemplate.length()-1 );
        context.disableDefaultConstraintViolation();
        // gln must be 8, 12-14, 17-18 digits long, gtin must be 13 digits long or empty string
        if (defaultConstraintMessageTemplate.equals("article.gtin") && gln.length() != 13 && gln.equals("")) {
            return true;
        } else if(defaultConstraintMessageTemplate.equals("article.gtin") && gln.length() != 13 && gln.equalsIgnoreCase("*radera")) {
            return true;
        } else if( gln.length() != 13 ) {
            context.buildConstraintViolationWithTemplate( "{" + defaultConstraintMessageTemplate + ".notValid.wrongLength}"  ).addConstraintViolation();
            return false;
        }  else {
            char[] glnArray = gln.toCharArray();
            int checkDigit = 0;
            // make sure all chars are digits
            for( int i=0; i<glnArray.length; i++ ) {
                char glnChar = glnArray[i];
                if( !Character.isDigit(glnChar) ) {
                    context.buildConstraintViolationWithTemplate( "{" + defaultConstraintMessageTemplate + ".notValid.notAllDigits}"  ).addConstraintViolation();
                    return false;
                }
                if( i == (glnArray.length - 1) ) {
                    checkDigit = Character.getNumericValue(glnChar);
                }
            }
            // validate check digit
            int total = 0;
            for( int i=0; i<(glnArray.length - 1); i++ ) {
                int multiplier = 0;
                if( (i % 2) == 0 ) {
                    // even, multiply by 1
                    multiplier = 1;
                } else {
                    // uneven, multiply by 3
                    multiplier = 3;
                }
                total += (Character.getNumericValue(glnArray[i]) * multiplier);
            }
            int calculatedCheckDigit = 10 - (total % 10);
            if( calculatedCheckDigit == 10 ) {
                calculatedCheckDigit = 0;
            }
            if( checkDigit != calculatedCheckDigit ) {
                context.buildConstraintViolationWithTemplate( "{" + defaultConstraintMessageTemplate + ".notValid.checkSum}"  ).addConstraintViolation();
                return false;
            }
        }
        return true;
    }



}
