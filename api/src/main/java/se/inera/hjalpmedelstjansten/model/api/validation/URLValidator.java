package se.inera.hjalpmedelstjansten.model.api.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * For now, we make this really, really simple validation.
 *
 */
public class URLValidator implements ConstraintValidator<ValidURL, String> {


    @Override
    public void initialize(ValidURL constraintAnnotation) {

    }

    @Override
    public boolean isValid(String url, ConstraintValidatorContext context) {
        if( url == null ) {
            return true;
        }
        if( !url.startsWith("http://") && !url.startsWith("https://") ) {
            return false;
        }
        return true;
    }



}
