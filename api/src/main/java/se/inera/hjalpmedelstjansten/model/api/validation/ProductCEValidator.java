package se.inera.hjalpmedelstjansten.model.api.validation;

import se.inera.hjalpmedelstjansten.model.api.ProductAPI;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;


public class ProductCEValidator implements ConstraintValidator<ValidProductCE, ProductAPI> {


    @Override
    public void initialize(ValidProductCE constraintAnnotation) {

    }

    @Override
    public boolean isValid(ProductAPI productAPI, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        if( !productAPI.isCeMarked() && productAPI.getCeDirective() != null ) {
            context.
                    buildConstraintViolationWithTemplate( "{product.noCeButDirective}" ).
                    addPropertyNode("ceMarked").
                    addConstraintViolation();
            return false;
        }
        if( (productAPI.getReplacementDate() != null && productAPI.getInactivateRowsOnReplacement() == null) ||
                (productAPI.getReplacementDate() == null && productAPI.getInactivateRowsOnReplacement() != null)) {
            context.buildConstraintViolationWithTemplate( "{product.replacementDateInactivate.notNull}" ).addConstraintViolation();
            return false;
        }
        return true;
    }



}
