package se.inera.hjalpmedelstjansten.model.api;

import jakarta.validation.constraints.NotNull;
import java.util.List;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 *
 */
public class ExportSettingsAPI {

    private Long id;

    @NotNull(message = "{exportsettings.organization.notNull}")
    private OrganizationAPI organization;

    private BusinessLevelAPI businessLevel;

    @NotNull(message = "{exportsettings.filename.notNull}")
    private String filename;

    private int numberOfExports;

    private List<GeneralPricelistAPI> generalPricelists;

    private Long lastExported;

    private boolean enabled;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OrganizationAPI getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationAPI organization) {
        this.organization = organization;
    }

    public BusinessLevelAPI getBusinessLevel() {
        return businessLevel;
    }

    public void setBusinessLevel(BusinessLevelAPI businessLevel) {
        this.businessLevel = businessLevel;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getNumberOfExports() {
        return numberOfExports;
    }

    public void setNumberOfExports(int numberOfExports) {
        this.numberOfExports = numberOfExports;
    }

    public Long getLastExported() {
        return lastExported;
    }

    public void setLastExported(Long lastExported) {
        this.lastExported = lastExported;
    }

    public List<GeneralPricelistAPI> getGeneralPricelists() {
        return generalPricelists;
    }

    public void setGeneralPricelists(List<GeneralPricelistAPI> generalPricelists) {
        this.generalPricelists = generalPricelists;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
