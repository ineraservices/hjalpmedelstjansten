package se.inera.hjalpmedelstjansten.model.api.external;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


public class OrderByAPI {
    
    private List<String> idPath;
    private String Ascending;

    @JsonProperty("IdPath")
    public List<String> getIdPath() {
        return idPath;
    }

    @JsonProperty("IdPath")
    public void setIdPath(List<String> idPath) {
        this.idPath = idPath;
    }

    @JsonProperty("Ascending")
    public String getAscending() {
        return Ascending;
    }

    @JsonProperty("Ascending")
    public void setAscending(String Ascending) {
        this.Ascending = Ascending;
    }

}
