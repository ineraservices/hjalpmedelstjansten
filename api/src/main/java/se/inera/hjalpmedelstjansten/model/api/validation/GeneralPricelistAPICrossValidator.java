package se.inera.hjalpmedelstjansten.model.api.validation;

import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistAPI;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * Cross validation for general pricelists
 *
 */
public class GeneralPricelistAPICrossValidator implements ConstraintValidator<ValidGeneralPricelistAPICross, GeneralPricelistAPI> {


    @Override
    public void initialize(ValidGeneralPricelistAPICross constraintAnnotation) {

    }

    @Override
    public boolean isValid(GeneralPricelistAPI generalPricelistAPI, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        boolean valid = true;
        if( generalPricelistAPI.getValidFrom() != null && generalPricelistAPI.getValidTo() != null ) {
            if( generalPricelistAPI.getValidFrom() >= generalPricelistAPI.getValidTo() ) {
                context.
                        buildConstraintViolationWithTemplate( "{generalPricelist.validToBeforeValidFrom}" ).
                        addPropertyNode("validTo").
                        addConstraintViolation();
                valid = false;
            }
        }
        return valid;
    }



}
