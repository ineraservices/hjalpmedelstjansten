package se.inera.hjalpmedelstjansten.model.api;

import se.inera.hjalpmedelstjansten.model.api.cv.CVCEDirectiveAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEStandardAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVOrderUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPackageUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidProductCE;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidProductStatus;

import jakarta.persistence.Column;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.util.List;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 *
 */
@ValidProductCE
public class ProductAPI {

    private Long id;

    @NotNull(message = "{product.name.notNull}")
    @Size(min = 1, max = 255, message = "{product.name.wrongLength}")
    private String productName;

    @NotNull(message = "{product.number.notNull}")
    @Size(min = 1, max = 35, message = "{product.number.wrongLength}")
    private String productNumber;

    @NotNull(message = "{product.status.notNull}")
    @ValidProductStatus
    private String status;

    private String organizationName;
    private Long organizationId;

    @NotNull(message = "{product.category.notNull}")
    private CategoryAPI category;

    private List<CategoryAPI> extendedCategories;

    private CVPreventiveMaintenanceAPI preventiveMaintenanceValidFrom;

    private Integer preventiveMaintenanceNumberOfDays;

    private String preventiveMaintenanceDescription;

    private String color;

    @Size(min = 0, max = 255, message = "{product.manufacturer.wrongLength}")
    private String manufacturer;

    @Size(min = 0, max = 255, message = "{product.manufacturerProductNumber.wrongLength}")
    private String manufacturerProductNumber;

    @Size(min = 0, max = 255, message = "{product.trademark.wrongLength}")
    private String trademark;

    private ElectronicAddressAPI manufacturerElectronicAddress;

    private boolean ceMarked;
    private CVCEDirectiveAPI ceDirective;
    private CVCEStandardAPI ceStandard;

    private boolean customerUnique;

    @Size(min = 0, max = 512, message = "{product.supplementedInformation.wrongLength}")
    private String supplementedInformation;

    @Size(min = 0, max = 500, message = "{product.descriptionForElvasjuttiosju.wrongLength}")
    private String descriptionForElvasjuttiosju;

    @NotNull(message = "{mainImage.uploadLink.notNull}")
    @Size(min = 1, message = "{mainImage.uploadLink.notNull}")
    private String mainImageUrl;

    @Size(min = 0, max = 255, message = "{mainImage.bildText.wrongLength}")
    private String mainImageDescription;

    @NotNull(message = "{mainImage.altText.notNull}")
    @Size(min = 1, max = 255, message = "{mainImage.altText.wrongLength}")
    private String mainImageAltText;

    private String mainImageOriginalUrl;

    @DecimalMin(value = "0", inclusive = true, message = "{product.replacementDate.invalid}")
    private Long replacementDate;

    private Boolean inactivateRowsOnReplacement;

    private List<ProductAPI> replacedByProducts;

    @NotNull(message = "{product.orderUnit.notNull}")
    private CVOrderUnitAPI orderUnit;

    private Double articleQuantityInOuterPackage;
    private CVOrderUnitAPI articleQuantityInOuterPackageUnit;

    private Double packageContent;
    private CVPackageUnitAPI packageContentUnit;

    private Integer packageLevelBase;

    private Integer packageLevelMiddle;

    private Integer packageLevelTop;

    private List<ResourceSpecificPropertyAPI> categoryPropertys;

    private Long created;

    private Long updated;

    private boolean numberEditable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public CategoryAPI getCategory() {
        return category;
    }

    public void setCategory(CategoryAPI category) {
        this.category = category;
    }

    public List<CategoryAPI> getExtendedCategories() {
        return extendedCategories;
    }

    public void setExtendedCategories(List<CategoryAPI> extendedCategories) {
        this.extendedCategories = extendedCategories;
    }

    public CVPreventiveMaintenanceAPI getPreventiveMaintenanceValidFrom() {
        return preventiveMaintenanceValidFrom;
    }

    public void setPreventiveMaintenanceValidFrom(CVPreventiveMaintenanceAPI preventiveMaintenanceValidFrom) {
        this.preventiveMaintenanceValidFrom = preventiveMaintenanceValidFrom;
    }

    public Integer getPreventiveMaintenanceNumberOfDays() {
        return preventiveMaintenanceNumberOfDays;
    }

    public void setPreventiveMaintenanceNumberOfDays(Integer preventiveMaintenanceNumberOfDays) {
        this.preventiveMaintenanceNumberOfDays = preventiveMaintenanceNumberOfDays;
    }

    public String getPreventiveMaintenanceDescription() {
        return preventiveMaintenanceDescription;
    }

    public void setPreventiveMaintenanceDescription(String preventiveMaintenanceDescription) {
        this.preventiveMaintenanceDescription = preventiveMaintenanceDescription;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getManufacturerProductNumber() {
        return manufacturerProductNumber;
    }

    public void setManufacturerProductNumber(String manufacturerProductNumber) {
        this.manufacturerProductNumber = manufacturerProductNumber;
    }

    public String getTrademark() {
        return trademark;
    }

    public void setTrademark(String trademark) {
        this.trademark = trademark;
    }

    public ElectronicAddressAPI getManufacturerElectronicAddress() {
        return manufacturerElectronicAddress;
    }

    public void setManufacturerElectronicAddress(ElectronicAddressAPI manufacturerElectronicAddress) {
        this.manufacturerElectronicAddress = manufacturerElectronicAddress;
    }

    public boolean isCeMarked() {
        return ceMarked;
    }

    public void setCeMarked(boolean ceMarked) {
        this.ceMarked = ceMarked;
    }

    public boolean isCustomerUnique() {
        return customerUnique;
    }

    public void setCustomerUnique(boolean customerUnique) {
        this.customerUnique = customerUnique;
    }

    public CVCEDirectiveAPI getCeDirective() {
        return ceDirective;
    }

    public void setCeDirective(CVCEDirectiveAPI ceDirective) {
        this.ceDirective = ceDirective;
    }

    public CVCEStandardAPI getCeStandard() {
        return ceStandard;
    }

    public void setCeStandard(CVCEStandardAPI ceStandard) {
        this.ceStandard = ceStandard;
    }

    public String getSupplementedInformation() {
        return supplementedInformation;
    }

    public void setSupplementedInformation(String supplementedInformation) {
        this.supplementedInformation = supplementedInformation;
    }

    public String getDescriptionForElvasjuttiosju() {
        return descriptionForElvasjuttiosju;
    }

    public void setDescriptionForElvasjuttiosju(String descriptionForElvasjuttiosju) {
        this.descriptionForElvasjuttiosju = descriptionForElvasjuttiosju;
    }

    public String getMainImageUrl() {
        return mainImageUrl;
    }

    public void setMainImageUrl(String mainImageUrl) {
        this.mainImageUrl = mainImageUrl;
    }

    public String getMainImageDescription() {
        return mainImageDescription;
    }

    public void setMainImageDescription(String mainImageDescription) {
        this.mainImageDescription = mainImageDescription;
    }

    public String getMainImageOriginalUrl() {
        return mainImageOriginalUrl;
    }

    public void setMainImageOriginalUrl(String mainImageOriginalUrl) {
        this.mainImageOriginalUrl = mainImageOriginalUrl;
    }


    public String getMainImageAltText() {
        return mainImageAltText;
    }

    public void setMainImageAltText(String mainImageAltText) {
        this.mainImageAltText = mainImageAltText;
    }

    public Long getReplacementDate() {
        return replacementDate;
    }

    public void setReplacementDate(Long replacementDate) {
        this.replacementDate = replacementDate;
    }

    public Boolean getInactivateRowsOnReplacement() {
        return inactivateRowsOnReplacement;
    }

    public void setInactivateRowsOnReplacement(Boolean inactivateRowsOnReplacement) {
        this.inactivateRowsOnReplacement = inactivateRowsOnReplacement;
    }

    public List<ProductAPI> getReplacedByProducts() {
        return replacedByProducts;
    }

    public void setReplacedByProducts(List<ProductAPI> replacedByProducts) {
        this.replacedByProducts = replacedByProducts;
    }

    public CVOrderUnitAPI getOrderUnit() {
        return orderUnit;
    }

    public void setOrderUnit(CVOrderUnitAPI orderUnit) {
        this.orderUnit = orderUnit;
    }

    public Double getArticleQuantityInOuterPackage() {
        return articleQuantityInOuterPackage;
    }

    public void setArticleQuantityInOuterPackage(Double articleQuantityInOuterPackage) {
        this.articleQuantityInOuterPackage = articleQuantityInOuterPackage;
    }

    public CVOrderUnitAPI getArticleQuantityInOuterPackageUnit() {
        return articleQuantityInOuterPackageUnit;
    }

    public void setArticleQuantityInOuterPackageUnit(CVOrderUnitAPI articleQuantityInOuterPackageUnit) {
        this.articleQuantityInOuterPackageUnit = articleQuantityInOuterPackageUnit;
    }

    public Double getPackageContent() {
        return packageContent;
    }

    public void setPackageContent(Double packageContent) {
        this.packageContent = packageContent;
    }

    public CVPackageUnitAPI getPackageContentUnit() {
        return packageContentUnit;
    }

    public void setPackageContentUnit(CVPackageUnitAPI packageContentUnit) {
        this.packageContentUnit = packageContentUnit;
    }

    public Integer getPackageLevelBase() {
        return packageLevelBase;
    }

    public void setPackageLevelBase(Integer packageLevelBase) {
        this.packageLevelBase = packageLevelBase;
    }

    public Integer getPackageLevelMiddle() {
        return packageLevelMiddle;
    }

    public void setPackageLevelMiddle(Integer packageLevelMiddle) {
        this.packageLevelMiddle = packageLevelMiddle;
    }

    public Integer getPackageLevelTop() {
        return packageLevelTop;
    }

    public void setPackageLevelTop(Integer packageLevelTop) {
        this.packageLevelTop = packageLevelTop;
    }

    public List<ResourceSpecificPropertyAPI> getCategoryPropertys() {
        return categoryPropertys;
    }

    public void setCategoryPropertys(List<ResourceSpecificPropertyAPI> categoryPropertys) {
        this.categoryPropertys = categoryPropertys;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isNumberEditable() {
        return numberEditable;
    }

    public void setNumberEditable(boolean numberEditable) {
        this.numberEditable = numberEditable;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }


}
