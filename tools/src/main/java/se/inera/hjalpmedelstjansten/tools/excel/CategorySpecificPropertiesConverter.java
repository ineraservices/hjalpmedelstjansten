package se.inera.hjalpmedelstjansten.tools.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

/**
 * Contains functionality for converting category specific properties excel file
 * to system readable csv file
 *
 */
public class CategorySpecificPropertiesConverter {


    public static void main(String[] args) throws FileNotFoundException, IOException {
        if (args.length < 2) {
            System.out.println("<inputFileName> <outputFileName>");
            System.exit(1);
        }
        String inputFileName = args[0];
        Objects.requireNonNull(inputFileName, "inputFileName must be non-null");

        String outputFileName = args[1];
        Objects.requireNonNull(outputFileName, "outputFileName must be non-null");

        try (PrintWriter printWriter = new PrintWriter(outputFileName)) {
            FileInputStream fileInputStream = new FileInputStream(new File(inputFileName));
            Workbook workbook = new XSSFWorkbook(fileInputStream);
            Sheet sheet = workbook.getSheetAt(0);
            printWriter.println("CODE;FIELD_NAME;DESCRIPTION;TYPE;LIST");
            boolean checkNextLineForMin = false;
            System.out.println("Last row: " + sheet.getLastRowNum());
            for (int i = 0; i < sheet.getLastRowNum(); i++) {
                if (i > 1) {
                    Row row = sheet.getRow(i);
                    Cell categoryCell = row.getCell(0);
                    if (categoryCell == null) {
                        // we must have reached the last line
                        break;
                    }
                    String categoryString = categoryCell.getStringCellValue();
                    String category;
                    if (Character.isDigit(categoryString.charAt(7))) {
                        category = categoryString.substring(0, 9);
                    } else {
                        category = categoryString.substring(0, 6);
                    }
                    Cell typeCell = row.getCell(4);
                    if (typeCell == null) {
                        System.err.println("Row " + (i + 1) + " is missing type ");
                        continue;
                    }
                    String typeNameSv = typeCell.getStringCellValue()
                                                .trim();
                    String typeName = null;
                    if (typeNameSv.equals("Decimaltal")) {
                        typeName = "DECIMAL";
                    } else if (typeNameSv.equals("Fritextfält")) {
                        typeName = "TEXTFIELD";
                    } else if (typeNameSv.equals("Flervalsvärdelista")) {
                        typeName = "VALUELIST_MULTIPLE";
                    } else if (typeNameSv.equals("Värdelista")) {
                        typeName = "VALUELIST_SINGLE";
                    } else if (typeNameSv.equals("Rubrik")) {
                        typeName = "INTERVAL";
                    }
                    if (typeName == null) {
                        System.err.println("Unknown typeNameSv: " + typeNameSv + " on line: " + (i + 1));
                        continue;
                    }

                    Cell fieldNameCell = row.getCell(1);
                    String fieldName = fieldNameCell.getStringCellValue()
                                                    .trim();

                    Cell descriptionCell = row.getCell(5);
                    String description = "";
                    if (descriptionCell != null) {
                        description = descriptionCell.getStringCellValue()
                                                     .trim();
                        if (description.contains("\n")) {
                            System.err.println("Newline in description on row: " + (i + 1));
                        }
                    }

                    String valueList = "";
                    if (typeName.equals("VALUELIST_SINGLE") || typeName.equals("VALUELIST_MULTIPLE")) {
                        Cell valueListCell = row.getCell(2);
                        if (valueListCell != null) {
                            String valueListString = valueListCell.getStringCellValue();
                            String[] values = valueListString.split("\n");
                            for (String value : values) {
                                if (value.contains(":")) {
                                    System.err.println("Found illegal character comma in value on line:" + (i + 1));
                                    continue;
                                }
                            }
                            valueList = String.join(":", values);
                        } else {
                            System.err.println("Empty valuelist for valuelist type on row: " + (i + 1));
                            continue;
                        }
                    }
                    if (typeName.equals("INTERVAL")) {
                        i += 2;
                    }
                    printWriter.println(category + ";" + fieldName + ";" + description + ";" + typeName + ";" + valueList);

                }
            }
        }
    }

}
