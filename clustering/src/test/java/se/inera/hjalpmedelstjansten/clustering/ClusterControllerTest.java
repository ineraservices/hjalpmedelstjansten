package se.inera.hjalpmedelstjansten.clustering;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;

@ExtendWith(MockitoExtension.class)
class ClusterControllerTest {

    private static final String REDIS_KEY = "key";
    private static final String REDIS_LOCK_VALUE = "lock";
    private static final long SUCCESSFUL = 1L;
    private static final long FAILED = -1L;
    private static final long EXPIRES_AFTER = 90;

    @Mock
    private JedisPool jedisPool;

    @Mock
    private JedisCluster jedisCluster;

    @Mock
    private HjmtLogger hjmtLogger;

    @InjectMocks
    private ClusterController clusterController;

    @Nested
    class TestRedisCluster {

        @BeforeEach
        void setUp() {
            runAsCluster(true);
        }

        @Test
        void shallReturnTrueIfLockIsSuccessful() {
            doReturn(SUCCESSFUL).when(jedisCluster).setnx(REDIS_KEY, REDIS_LOCK_VALUE);

            final var result = clusterController.getLock(REDIS_KEY, EXPIRES_AFTER);

            assertTrue(result, "Expect a successful lock");
        }

        @Test
        void shallSetExpirationOnLockIfSuccessfull() {
            doReturn(SUCCESSFUL).when(jedisCluster).setnx(REDIS_KEY, REDIS_LOCK_VALUE);

            clusterController.getLock(REDIS_KEY, EXPIRES_AFTER);

            final var expirationCaptor = ArgumentCaptor.forClass(Long.class);
            verify(jedisCluster).expire(eq(REDIS_KEY), expirationCaptor.capture());

            assertEquals(EXPIRES_AFTER, expirationCaptor.getValue().intValue());
        }

        @Test
        void shallReturnFalseIfLockIsFailed() {
            doReturn(FAILED).when(jedisCluster).setnx(REDIS_KEY, REDIS_LOCK_VALUE);

            final var result = clusterController.getLock(REDIS_KEY, EXPIRES_AFTER);

            assertFalse(result, "Expect a failed lock");
        }

        @Test
        void shallNotSetExpirationOnLockIfFailed() {
            doReturn(FAILED).when(jedisCluster).setnx(REDIS_KEY, REDIS_LOCK_VALUE);

            clusterController.getLock(REDIS_KEY, EXPIRES_AFTER);

            verify(jedisCluster, never()).expire(any(String.class), any(Long.class));
        }

        @Test
        void shallReturnFalseIfRedisClusterThrowsException() {
            doThrow(new IllegalStateException("error!")).when(jedisCluster).setnx(REDIS_KEY, REDIS_LOCK_VALUE);

            final var result = clusterController.getLock(REDIS_KEY, EXPIRES_AFTER);

            assertFalse(result, "Expect a failed lock");
        }

        @Test
        void shallReturnKeysMatchingPrefix() {
            final List<String> expectedKeys = List.of("1", "2", "3");
            final String prefix = "prefix";

            final var jedisPoolMap = Map.of("jedisPool", jedisPool);
            doReturn(jedisPoolMap).when(jedisCluster).getClusterNodes();
            final var jedis = mock(Jedis.class);
            doReturn(jedis).when(jedisPool).getResource();
            final var scanResult = mock(ScanResult.class);
            doReturn(scanResult).when(jedis).scan(anyString(), any(ScanParams.class));
            doReturn(expectedKeys).when(scanResult).getResult();
            doReturn("0").when(scanResult).getCursor();

            final var actualKeys = clusterController.getKeys(prefix);

            assertAll(
                ()-> assertTrue(actualKeys.contains(expectedKeys.get(0)), () -> "Expected key " + expectedKeys.get(0) + " but got " + actualKeys),
                ()-> assertTrue(actualKeys.contains(expectedKeys.get(1)), () -> "Expected key " + expectedKeys.get(1) + " but got " + actualKeys),
                ()-> assertTrue(actualKeys.contains(expectedKeys.get(2)), () -> "Expected key " + expectedKeys.get(2) + " but got " + actualKeys)
            );
        }

        @Test
        void shallCloseRedisConnectionAfterGettingKeys() {
            final String prefix = "prefix";

            final var jedisPoolMap = Map.of("jedisPool", jedisPool);
            doReturn(jedisPoolMap).when(jedisCluster).getClusterNodes();
            final var jedis = mock(Jedis.class);
            doReturn(jedis).when(jedisPool).getResource();
            final var scanResult = mock(ScanResult.class);
            doReturn(scanResult).when(jedis).scan(anyString(), any(ScanParams.class));
            doReturn("0").when(scanResult).getCursor();

            clusterController.getKeys(prefix);

            verify(jedis).close();
        }
    }

    private void runAsCluster(boolean enable) {
        try {
            final var mockServiceClass = Class.forName("se.inera.hjalpmedelstjansten.clustering.ClusterController");
            final var mockService = (ClusterController) mockServiceClass.cast(clusterController);
            final var field = mockServiceClass.getDeclaredField("runAsCluster");
            field.setAccessible(true);
            field.set(mockService, enable);
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }
}
