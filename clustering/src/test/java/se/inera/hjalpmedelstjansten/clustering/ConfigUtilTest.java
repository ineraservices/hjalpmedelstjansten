package se.inera.hjalpmedelstjansten.clustering;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Map;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class ConfigUtilTest {

    private static final String EXISTS_KEY = "EXISTS";
    private static final String DONT_EXISTS_KEY = "DONT_EXISTS";

    @Test
    void shallReturnDefaultValueIfNoEnvironmentVariableExists() {
        final var expectedValue = 10;
        final var actualValue = ConfigUtil.getConfig(DONT_EXISTS_KEY, expectedValue, this::convert);
        assertEquals(expectedValue, actualValue);
    }

    @Test
    @Disabled("The way this test uses java internals to modify environment variables is not allowed from java 17+. Test needs to be rewritten.")
    void shallReturnValueIfEnvironmentVariableExists() {
        final var expectedValue = 20;
        addEnvironmentVariable(EXISTS_KEY, Integer.toString(expectedValue));
        final var actualValue = ConfigUtil.getConfig(EXISTS_KEY, 10, this::convert);
        assertEquals(expectedValue, actualValue);
    }

    private void addEnvironmentVariable(final String key, final String value) {
        try {
            final var classOfMap = System.getenv().getClass();
            final var field = classOfMap.getDeclaredField("m");
            field.setAccessible(true);
            final var writeableEnvironmentVariables = (Map<String, String>) field.get(System.getenv());
            writeableEnvironmentVariables.put(key, value);
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }

    private Integer convert(String value) {
        return Integer.parseInt(value);
    }
}
