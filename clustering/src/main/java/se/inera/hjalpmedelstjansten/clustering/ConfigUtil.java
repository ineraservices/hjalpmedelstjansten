package se.inera.hjalpmedelstjansten.clustering;

import java.util.function.Function;

public class ConfigUtil {

    private ConfigUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Get configuration from Environment Variables.
     * Will return default value if no environment variable found.
     * Will return converted value if environment variable found.
     * @param key   key of environment variable
     * @param defaultValue  default value if no environment variable found.
     * @param converter function to convert environment variable value (string)
     * @return  configuration value
     * @param <T>   type of the configuration value
     */
    public static <T> T getConfig(String key, T defaultValue, Function<String, T> converter) {
        final var valueStr = System.getenv(key);
        return valueStr == null ? defaultValue : converter.apply(valueStr);
    }

}
