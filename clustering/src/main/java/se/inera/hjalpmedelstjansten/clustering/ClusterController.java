package se.inera.hjalpmedelstjansten.clustering;

import static jakarta.ejb.ConcurrencyManagementType.BEAN;
import static se.inera.hjalpmedelstjansten.clustering.ConfigUtil.getConfig;

import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import jakarta.annotation.PostConstruct;
import jakarta.ejb.ConcurrencyManagement;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.inject.Inject;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;

/**
 * This class contains methods for handling cluster issues, like locks and
 * global sessions
 *
 * Reads two environment variables:
 * - REDIS_MASTER_NAME
 * - REDIS_HOST
 * if the environment variable REDIS_MASTER_NAME is set an attempt to start
 * a JedisCluster is made. The REDIS_HOST environment variable is
 * also assumed to be a comma separated string of host names
 * This happens in stage and prod.
 *
 * if the environment variable REDIS_MASTER_NAME is NOT set an attempt to start
 * a JedisPool is made. The REDIS_HOST environment variable can in this case NOT
 * be a comma separated string of host name, but instead a single hostname
 * This happens in local and dev environment.
 */
@Singleton
@Startup
@ConcurrencyManagement(BEAN)
public class ClusterController {

    @Inject
    HjmtLogger LOG;

    private JedisPool jedisPool;
    private JedisCluster jedisCluster;
    private String password;
    private Boolean runAsCluster = false;

    /**
     * Attempts to get a lock in Redis using the setnx (set if not exists) method.
     * If lock is received, the lock is maintained for <code>expiresAfter</code>
     * seconds.
     *
     * @param key the key of the lock
     * @param expiresAfter number of seconds to maintain the lock
     * @return true if lock was received, otherwise false
     */
    public boolean getLock(String key, long expiresAfter) {
        boolean lockReceived = false;
        if (Boolean.TRUE.equals(runAsCluster)) {
            try {
                final var result = jedisCluster.setnx(key, "lock");
                if (result == 1L) {
                    LOG.log(Level.INFO, String.format("Lock set successfully for key '%s' with expires '%s'", key, expiresAfter));
                    lockReceived = true;
                    jedisCluster.expire(key, expiresAfter);
                }
            } catch (Exception ex) {
                LOG.log(Level.WARNING, String.format("Error when trying to set lock for key '%s' with expires '%s'", key, expiresAfter), ex);
            }
        } else {
            try (Jedis jedis = getJedis()) {
                Long result = jedis.setnx(key, "lock");
                if (result == 1L) {
                    lockReceived = true;
                    jedis.expire(key, expiresAfter);
                }
            }
        }
        return lockReceived;
    }

    public void releaseLock(String key) {
        if ((key != null && !key.isEmpty()) && runAsCluster) {
            jedisCluster.del(key);
            LOG.log(Level.FINEST, "released lock: {0}", new Object[]{key});
        }
    }

    /**
     * Get a value with the given prefix and key.
     */
    public String getValue(String prefix, String key) {
        if (runAsCluster) {
            return jedisCluster.get(prefix + key);
        } else {
            try (Jedis jedis = getJedis()) {
                return jedis.get(prefix + key);
            }
        }
    }

    /**
     * Set a value with the given prefix and key.
     */
    public void setValue(String prefix, String key, String value) {
        if (runAsCluster) {
            jedisCluster.set(prefix + key, value);
        } else {
            try (Jedis jedis = getJedis()) {
                jedis.set(prefix + key, value);
            }
        }
    }

    /**
     * Set that a value with given key (and prefix) expires after the given
     * amount of seconds.
     *
     * @param expiresSeconds the number of seconds to expire value
     */
    public void valueExpires(String prefix, String key, long expiresSeconds) {
        if (runAsCluster) {
            jedisCluster.expire(prefix + key, expiresSeconds);
        } else {
            try (Jedis jedis = getJedis()) {
                jedis.expire(prefix + key, expiresSeconds);
            }
        }
    }

    /**
     * Remove a value with the given key (and prefix)
     */
    public void removeValue(String prefix, String key) {
        if (runAsCluster) {
            jedisCluster.del(prefix + key);
        } else {
            try (Jedis jedis = getJedis()) {
                jedis.del(prefix + key);
            }
        }
    }

    /**
     * Get all keys with the given prefix, used for Shiros session validation.
     */
    public Set<String> getKeys(String prefix) {
        if (runAsCluster) {
            final Collection<JedisPool> clusterNodes = jedisCluster.getClusterNodes().values();
            final Set<String> allKeys = new HashSet<>();

            for (JedisPool node : clusterNodes) {
                allKeys.addAll(
                    getKeys(prefix, node)
                );
            }

            if (allKeys.isEmpty()) {
                return Collections.emptySet();
            }

            return allKeys;
        } else {
            try (Jedis jedis = getJedis()) {
                return jedis.keys(prefix + "*");
            }
        }
    }

    private static Set<String> getKeys(String prefix, JedisPool node) {
        final Set<String> allKeys = new HashSet<>();
        final ScanParams scanParams = new ScanParams().match(prefix + "*");
        try (Jedis jedis = node.getResource()) {
            String cursor = ScanParams.SCAN_POINTER_START;
            do {
                final ScanResult<String> scanResult = jedis.scan(cursor, scanParams);
                final List<String> keys = scanResult.getResult();
                if (keys != null && !keys.isEmpty()) {
                    allKeys.addAll(keys);
                }
                cursor = scanResult.getCursor();
            } while (!cursor.equals(ScanParams.SCAN_POINTER_START));
        }
        return allKeys;
    }

    /**
     * Add a long to a cluster list. Uses Redis LPUSH
     *
     * @param queueName the name of the list (queue)
     * @param value long value, usually an id of some sort
     */
    public void addLongToQueue(String queueName, Long value) {

        if (runAsCluster) {
            LOG.log(Level.INFO, "addLongToQueue( queueName: {0}, value.toString(): {1} )", new Object[]{queueName, value.toString()});
            jedisCluster.lpush(queueName, value.toString());
        } else {
            try (Jedis jedis = getJedis()) {
                jedis.lpush(queueName, value.toString());
            }
        }

    }

    /**
     * Get next long from a cluster list. Uses Redis RPOP
     *
     * @param queueName the name of the list (queue)
     * @return next long value, null if there is none
     */
    public Long getNextLongFromQueue(String queueName) {

        if (runAsCluster) {
            String value = jedisCluster.rpop(queueName);
            return value == null ? null : Long.parseLong(value);
        } else {
            try (Jedis jedis = getJedis()) {
                String value = jedis.rpop(queueName);
                return value == null ? null : Long.parseLong(value);
            }
        }

    }

    private Jedis getJedis() {
        Jedis jedis = null;
        if (jedisPool != null) {
            jedis = jedisPool.getResource();

        }
        if (password != null) {
            assert jedis != null;
            jedis.auth(password);
        }
        return jedis;
    }

    @PostConstruct
    private void initialize() {
        LOG.log(Level.FINEST, "initialize()");
        String redisMasterNameEnv = System.getenv("REDIS_MASTER_NAME");
        String redisHostEnv = System.getenv("REDIS_HOST");

        if (System.getenv("REDIS_CLUSTER_AUTH") != null && !System.getenv("REDIS_CLUSTER_AUTH").isEmpty()) {
            password = System.getenv("REDIS_CLUSTER_AUTH");
        }
        if (redisMasterNameEnv != null && !redisMasterNameEnv.equals("")) {

            runAsCluster = true;

            LOG.log(Level.FINEST, "Found REDIS_MASTER_NAME environment variable. Starting JedisCluster");
            final var redisHosts = redisHostEnv.split(",");
            final var nodes = new HashSet<HostAndPort>();

            for (String redisHost : redisHosts) {
                final var hostAndPort = new HostAndPort(redisHost.split(":")[0], Integer.parseInt(redisHost.split(":")[1]));
                nodes.add(hostAndPort);
            }

            final var blockWhenExhausted = getConfig("REDIS_CLUSTER_BLOCK_WHEN_EXHAUSTED", true, Boolean::parseBoolean);
            final var maxWait = getConfig("REDIS_CLUSTER_MAX_WAIT", Duration.ofSeconds(10), valueStr -> Duration.ofSeconds(Long.parseLong(valueStr)));
            final var minEvictableIdleTime = getConfig("REDIS_CLUSTER_MIN_EVICTABLE_IDLE_TIME", Duration.ofSeconds(300), valueStr -> Duration.ofSeconds(Long.parseLong(valueStr)));
            final var minIdle = getConfig("REDIS_CLUSTER_MIN_IDLE", 10, Integer::parseInt);
            final var maxIdle = getConfig("REDIS_CLUSTER_MAX_IDLE", 100, Integer::parseInt);
            final var maxTotal = getConfig("REDIS_CLUSTER_MAX_TOTAL", 100, Integer::parseInt);

            final var jedisPoolConfig = new JedisPoolConfig();
            jedisPoolConfig.setBlockWhenExhausted(blockWhenExhausted);
            jedisPoolConfig.setMaxWait(maxWait);
            jedisPoolConfig.setMinEvictableIdleTime(minEvictableIdleTime);
            jedisPoolConfig.setMinIdle(minIdle);
            jedisPoolConfig.setMaxIdle(maxIdle);
            jedisPoolConfig.setMaxTotal(maxTotal);

            jedisCluster = new JedisCluster(nodes, 5000, 1000, 5, password, jedisPoolConfig);
        } else {
            LOG.log(Level.FINEST, "Did not find REDIS_MASTER_NAME environment variable. Starting JedisPool");
            jedisPool = new JedisPool(redisHostEnv);
        }
    }
}
