Detta projekt är under utveckling.  

# Köra lokalt

För att köra Hjälpmedelstjänsten lokalt:

1. Klona repository

2. Byt branch till develop (git checkout develop)

3. mvn clean install

4. cd src/main/frontend/html

5. npm install

6. npm run-script build / node --max_old_space_size=4096 node_modules/@angular/cli/bin/ng build --prod


7. ../root

8. docker-compose build

9. Tilldela docker mer än standardminne. Detta görs under Preferences->Advanced. 4GiB brukar räcka.

10. docker-compose up


# Licensinformation för Hjälpmedelstjänsten

Copyright (C) 2018 Inera AB (http://www.inera.se)

This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).

Hjalpmedelstjansten is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Hjalpmedelstjansten is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

# SFTP
For SFTP functionality Jsch by JCraft is used. 
Project page: http://www.jcraft.com/jsch/
License: http://www.jcraft.com/jsch/LICENSE.txt