package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Entity
public class DeletedUser implements Serializable{
/*    public static final String FIND_ALL = "HJMT_IMPORTSTATUS_FIND_ALL";
    public static final String UPDATE_IMPORTSTATUS = "HJMT_IMPORTSTATUS_UPDATE_IMPORTSTATUS";*/
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = true)
    private Long oldUniqueId;

    @Column(nullable = true)
    private Long deletedByUserId;

    @Column
    private Long deletedUserOrg;

    @Column
    private String reasonCode;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date dateDeleted;


    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Long getUniqueId() {
        return uniqueId;
    }


    public void setOldUniqueId(Long id) {
        this.oldUniqueId = id;
    }

    public Long getOldUniqueId() {
        return oldUniqueId;
    }

    public void setDeletedByUserId(Long id) {
        this.deletedByUserId = id;
    }

    public Long getDeletedByUserId() {
        return deletedByUserId;
    }

    public String getReasonCode() {return reasonCode ;}

    public void setReasonCode(String code) { this.reasonCode = code;}

    public Long getDeletedUserOrg(){
        return deletedUserOrg;
    }

    public void setDeletedUserOrg(Long organizationId){
        this.deletedUserOrg = organizationId;
    }

    public void setDeletedDate(Date dateDeleted) {
        this.dateDeleted = dateDeleted;
    }

    public Date getDeletedDate() {
        return dateDeleted;
    }



}
