package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;


@Entity
@NamedQueries({
        @NamedQuery(name = ImportStatus.FIND_ALL, query = "SELECT s FROM ImportStatus s  WHERE s.changedBy =:changedBy  ORDER BY s.importStartDate DESC "),
        @NamedQuery(name = ImportStatus.UPDATE_IMPORTSTATUS, query = "UPDATE ImportStatus SET importStatus = :status,importEndDate = :endDate WHERE uniqueId=:uniqueId")
})
public class ImportStatus implements Serializable{
    public static final String FIND_ALL = "HJMT_IMPORTSTATUS_FIND_ALL";
    public static final String UPDATE_IMPORTSTATUS = "HJMT_IMPORTSTATUS_UPDATE_IMPORTSTATUS";
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = false)
    private String fileName;

    @Column(nullable = false)
    private Integer importStatus;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date importStartDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date importEndDate;

    @Column(nullable = false)
    private String changedBy;

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Long getUniqueId() {
        return uniqueId;
    }


    public void setFileName(String dynamicText) {
        this.fileName = dynamicText;
    }

    public String getFileName() {
        return fileName;
    }

    public void setReadStatus(Integer importStatus) {
        this.importStatus = importStatus;
    }

    public Integer getReadStatus() {
        return importStatus;
    }


    public void setImportStartDate(Date importStartDate) {
        this.importStartDate = importStartDate;
    }

    public Date getImportStartDate() {
        return importStartDate;
    }

    public void setImportEndDate(Date importEndDate) {
        this.importEndDate = importEndDate;
    }

    public Date getImportEndDate() {
        return importEndDate;
    }


    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy;
    }

    public String getChangedBy() {
        return changedBy;
    }


}
