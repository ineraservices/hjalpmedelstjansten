package se.inera.hjalpmedelstjansten.model.entity.media;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;

/**
 * Media of type image
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = MediaImage.FIND_BY_PRODUCT, query = "SELECT m FROM MediaImage m WHERE m.product.uniqueId = :productUniqueId ORDER BY m.created DESC"),
    @NamedQuery(name = MediaImage.FIND_BY_PRODUCT_AND_MAIN, query = "SELECT m FROM MediaImage m WHERE m.product.uniqueId = :productUniqueId AND m.mainImage IS TRUE")
})
public class MediaImage extends Media {

    public static final String FIND_BY_PRODUCT = "HJMT_MEDIAIMAGE_FIND_BY_PRODUCT";
    public static final String FIND_BY_PRODUCT_AND_MAIN = "HJMT_MEDIAIMAGE_FIND_BY_PRODUCT_AND_MAIN";

    @Column(nullable = true)
    private String alternativeText;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean mainImage;

    @Column(nullable = false)
    private String fileName;

    @Column(nullable = true)
    private String originalUrl;

    @Column(nullable = false)
    private String externalKey;

    public String getAlternativeText() {
        return alternativeText;
    }

    public void setAlternativeText(String alternativeText) {
        this.alternativeText = alternativeText;
    }

    public boolean isMainImage() {
        return mainImage;
    }

    public void setMainImage(boolean mainImage) {
        this.mainImage = mainImage;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public String getExternalKey() {
        return externalKey;
    }

    public void setExternalKey(String externalKey) {
        this.externalKey = externalKey;
    }

}
