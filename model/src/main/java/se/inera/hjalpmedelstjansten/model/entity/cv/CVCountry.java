package se.inera.hjalpmedelstjansten.model.entity.cv;

import java.io.Serializable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;

/**
 * Countries based on ISO 3166-1
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = CVCountry.FIND_ALL, query = "SELECT c FROM CVCountry c ORDER BY c.name ASC"),
    @NamedQuery(name = CVCountry.FIND_BY_CODE, query = "SELECT c FROM CVCountry c WHERE c.code = :code")
})
public class CVCountry implements Serializable {

    public static final String FIND_ALL = "HJMT_COUNTRY_FIND_ALL";
    public static final String FIND_BY_CODE = "HJMT_COUNTRY_FIND_BY_CODE";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(unique = true)
    private String name;

    @Column(unique = true)
    private String code;

    public Long getUniqueId() {
        return uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
