package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class ArticleExtendedCategory implements Serializable {

    @Id
    @Column(nullable = false)
    private long articleId;

    @Id
    @Column(nullable = false)
    private long categoryId;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ArticleExtendedCategory that = (ArticleExtendedCategory) o;
        return articleId == that.articleId && categoryId == that.categoryId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(articleId, categoryId);
    }
}
