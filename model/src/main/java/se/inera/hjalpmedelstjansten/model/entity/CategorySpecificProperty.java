package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import java.io.Serializable;
import java.util.List;

/**
 * Property specific of a <code>Category</code>
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = CategorySpecificProperty.FIND_BY_CATEGORY, query = "SELECT c FROM CategorySpecificProperty c WHERE c.category.uniqueId = :categoryUniqueId ORDER BY c.name ASC"),
        @NamedQuery(name = CategorySpecificProperty.SET_ORDER_INDEX, query = "UPDATE CategorySpecificProperty SET orderIndex = :orderIndex WHERE uniqueId=:uniqueId")
})
public class CategorySpecificProperty implements Serializable {

    public static final String FIND_BY_CATEGORY = "HJMT_CATEGORY_SPECIFIC_PROPERTY_FIND_BY_CATEGORY";
    public static final String SET_ORDER_INDEX = "HJMT_CATEGORY_SPECIFIC_PROPERTY_SET_ORDER_INDEX";


    public enum Type {
        TEXTFIELD,
        DECIMAL,
        VALUELIST_SINGLE,
        VALUELIST_MULTIPLE,
        INTERVAL
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = true)
    private Integer orderIndex;

    @Column(nullable = false)
    private String name;

    @Column(nullable = true, length = 500)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "categoryId", nullable = false)
    private Category category;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Type type;

    @OneToMany(
            mappedBy = "categorySpecificProperty",
            cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE},
            fetch = FetchType.EAGER,
            orphanRemoval = true
    )
    private List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Integer getOrderIndex() { return orderIndex; }

    public void setOrderIndex(Integer orderIndex) { this.orderIndex = orderIndex; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CategorySpecificPropertyListValue> getCategorySpecificPropertyListValues() {
        return categorySpecificPropertyListValues;
    }

    public void setCategorySpecificPropertyListValues(List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues) {
        this.categorySpecificPropertyListValues = categorySpecificPropertyListValues;
    }

}
