package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Entity
@NamedQueries({
    @NamedQuery(name = UserEngagement.FIND_BY_EMAIL, query = "SELECT ue FROM UserEngagement ue WHERE ue.userAccount.electronicAddress.email = :email"),
    @NamedQuery(name = UserEngagement.FIND_BY_USERNAME, query = "SELECT ue FROM UserEngagement ue WHERE ue.userAccount.username = :username"),
    @NamedQuery(name = UserEngagement.COUNT_BY_ORGANIZATION, query = "SELECT COUNT(ue) FROM UserEngagement ue WHERE ue.organization.uniqueId = :organizationUniqueId"),
    @NamedQuery(name = UserEngagement.GET_BY_ORGANIZATION_AND_ROLES, query = "SELECT DISTINCT ue FROM UserEngagement ue JOIN ue.roles uer WHERE ue.organization.uniqueId = :organizationUniqueId AND uer.name IN :roleNames"),
    @NamedQuery(name = UserEngagement.GET_BY_ORGANIZATION, query = "SELECT ue FROM UserEngagement ue WHERE ue.organization.uniqueId = :organizationUniqueId"),
    @NamedQuery(name = UserEngagement.SEARCH_BY_ORGANIZATION_AND_BUSINESS_LEVELS, query = "SELECT ue FROM UserEngagement ue JOIN ue.businessLevels ubl WHERE ue.organization.uniqueId = :organizationUniqueId AND ubl.uniqueId IN :businessLevelsIds AND (ue.userAccount.firstName LIKE :query OR ue.userAccount.lastName LIKE :query OR ue.userAccount.username LIKE :query OR ue.userAccount.electronicAddress.email LIKE :query) ORDER BY ue.userAccount.lastName ASC"),
    @NamedQuery(name = UserEngagement.COUNT_BY_ORGANIZATION_AND_BUSINESS_LEVELS, query = "SELECT COUNT(ue) FROM UserEngagement ue JOIN ue.businessLevels ubl WHERE ue.organization.uniqueId = :organizationUniqueId AND ubl.uniqueId IN :businessLevelsIds AND (ue.userAccount.firstName LIKE :query OR ue.userAccount.lastName LIKE :query OR ue.userAccount.username LIKE :query OR ue.userAccount.electronicAddress.email LIKE :query)"),
    @NamedQuery(name = UserEngagement.SEARCH_BY_ORGANIZATION_AND_NO_BUSINESS_LEVELS, query = "SELECT ue FROM UserEngagement ue WHERE ue.organization.uniqueId = :organizationUniqueId AND ue.businessLevels IS EMPTY AND (ue.userAccount.firstName LIKE :query OR ue.userAccount.lastName LIKE :query OR ue.userAccount.username LIKE :query OR ue.userAccount.electronicAddress.email LIKE :query) ORDER BY ue.userAccount.lastName ASC"),
    @NamedQuery(name = UserEngagement.COUNT_BY_ORGANIZATION_AND_NO_BUSINESS_LEVELS, query = "SELECT COUNT(ue) FROM UserEngagement ue WHERE ue.organization.uniqueId = :organizationUniqueId AND ue.businessLevels IS EMPTY AND (ue.userAccount.firstName LIKE :query OR ue.userAccount.lastName LIKE :query OR ue.userAccount.username LIKE :query OR ue.userAccount.electronicAddress.email LIKE :query)"),
    @NamedQuery(name = UserEngagement.FIND_BY_BUSINESS_LEVEL, query = "SELECT ue FROM UserEngagement ue JOIN ue.businessLevels ubl WHERE ubl.uniqueId = :businessLevelUniqueId"),
    @NamedQuery(name = UserEngagement.FIND_BY_VALID_TO_PASSED, query = "SELECT ue FROM UserEngagement ue WHERE ue.validTo < :thisDate")
})
public class UserEngagement implements Serializable {

    public static final String GET_BY_ORGANIZATION_AND_ROLES = "HJMT_USERENGAGEMENT_GET_BY_ORGANIZATION_AND_ROLES";
    public static final String GET_BY_ORGANIZATION = "HJMT_USERENGAGEMENT_GET_BY_ORGANIZATION";
    public static final String SEARCH_BY_ORGANIZATION_AND_BUSINESS_LEVELS = "HJMT_USERENGAGEMENT_SEARCH_BY_ORGANIZATION_AND_BUSINESS_LEVELS";
    public static final String COUNT_BY_ORGANIZATION_AND_BUSINESS_LEVELS = "HJMT_USERENGAGEMENT_COUNT_BY_ORGANIZATION_AND_BUSINESS_LEVELS";
    public static final String SEARCH_BY_ORGANIZATION_AND_NO_BUSINESS_LEVELS = "HJMT_USERENGAGEMENT_SEARCH_BY_ORGANIZATION_AND_NO_BUSINESS_LEVELS";
    public static final String COUNT_BY_ORGANIZATION_AND_NO_BUSINESS_LEVELS = "HJMT_USERENGAGEMENT_COUNT_BY_ORGANIZATION_AND_NO_BUSINESS_LEVELS";
    public static final String COUNT_BY_ORGANIZATION = "HJMT_USERENGAGEMENT_COUNT_BY_ORGANIZATION";
    public static final String FIND_BY_EMAIL = "HJMT_USERENGAGEMENT_FIND_BY_EMAIL";
    public static final String FIND_BY_USERNAME = "HJMT_USERENGAGEMENT_FIND_BY_USERNAME";
    public static final String FIND_BY_BUSINESS_LEVEL = "HJMT_USERENGAGEMENT_FIND_BY_BUSINESS_LEVEL";
    public static final String FIND_BY_VALID_TO_PASSED = "HJMT_USERENGAGEMENT_FIND_BY_VALID_TO_PASSED";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "userId", referencedColumnName = "uniqueId")
    private UserAccount userAccount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orgId")
    private Organization organization;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date validFrom;

    @Temporal(TemporalType.TIMESTAMP)
    @Column()
    private Date validTo;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name="UserEngagementBusinessLevel",
        joinColumns={@JoinColumn(name="userEngagementId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="businessLevelId", referencedColumnName="uniqueId")})
    private List<BusinessLevel> businessLevels;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name="UserEngagementUserRole",
        joinColumns={@JoinColumn(name="userEngagementId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="roleId", referencedColumnName="uniqueId")})
    private List<UserRole> roles;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column()
    private Date lastUpdated;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount user) {
        this.userAccount = user;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public List<BusinessLevel> getBusinessLevels() {
        return businessLevels;
    }

    public void setBusinessLevels(List<BusinessLevel> businessLevels) {
        this.businessLevels = businessLevels;
    }

    public List<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(List<UserRole> roles) {
        this.roles = roles;
    }

    @PrePersist
    private void prePersist() {
        this.created = new Date();
    }

    @PreUpdate
    private void preUpdate() {
        this.lastUpdated = new Date();
    }

}
