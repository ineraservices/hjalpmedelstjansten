package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.ManyToOne;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * JPA Entity class for agreement pricelist row
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = AgreementPricelistRow.COUNT_SEARCH, query = "SELECT COUNT(p.uniqueId) FROM AgreementPricelistRow p LEFT JOIN p.article.category ac WHERE p.pricelist.uniqueId = :pricelistUniqueId AND p.status IN :statuses AND (p.article.articleName LIKE :query OR p.article.articleNumber LIKE :query)"),
    @NamedQuery(name = AgreementPricelistRow.SEARCH, query = "SELECT p FROM AgreementPricelistRow p LEFT JOIN p.article.category ac WHERE p.pricelist.uniqueId = :pricelistUniqueId AND p.status IN :statuses AND (p.article.articleName LIKE :query OR p.article.articleNumber LIKE :query) ORDER BY p.article.articleName ASC"),

    //@NamedQuery(name = AgreementPricelistRow.SEARCH_X, query = "SELECT p, aa.deliveryTime AS aDeliveryTime, aa.leastOrderQuantity AS aLeastOrderQuantity, aa.price AS aPrice, aa.validFrom AS aValidFrom, aa.warrantyQuantity AS aWarrantyQuantity, aa.warrantyQuantityUnit AS aWarrantyQuantityUnit, aa.warrantyTerms AS aWarrantyTerms, aa.warrantyValidFrom AS aWarrantyValidFrom FROM AgreementPricelistRow p LEFT JOIN p.article.category ac LEFT JOIN ApprovedAgreementPricelistRow aa WHERE p.uniqueId = aa.uniqueId AND p.pricelist.uniqueId = :pricelistUniqueId AND p.status IN :statuses AND (p.article.articleName LIKE :query OR p.article.articleNumber LIKE :query) ORDER BY p.article.articleName ASC"),

    @NamedQuery(name = AgreementPricelistRow.FIND_BY_ARTICLE, query = "SELECT p FROM AgreementPricelistRow p WHERE p.article.uniqueId = :articleUniqueId"),
    @NamedQuery(name = AgreementPricelistRow.COUNT_BY_ARTICLES, query = "SELECT COUNT(p.uniqueId) FROM AgreementPricelistRow p WHERE p.article.uniqueId IN :articleUniqueIds"),
    @NamedQuery(name = AgreementPricelistRow.FIND_BY_CUSTOMER_AND_ARTICLE, query = "SELECT p FROM AgreementPricelistRow p LEFT JOIN p.pricelist.agreement.sharedWithCustomers swc LEFT JOIN p.pricelist.agreement.sharedWithCustomerBusinessLevels swcbl WHERE (p.pricelist.agreement.customer.uniqueId = :organizationUniqueId OR swc.uniqueId = :organizationUniqueId OR swcbl.organization.uniqueId = :organizationUniqueId) AND p.article.uniqueId = :articleUniqueId AND p.status IN :statuses AND p.price IS NOT NULL ORDER by p.pricelist.agreement.agreementNumber"),
    @NamedQuery(name = AgreementPricelistRow.FIND_BY_CUSTOMER_BUSINESS_LEVELS_AND_ARTICLE, query = "SELECT p FROM AgreementPricelistRow p LEFT JOIN p.pricelist.agreement.sharedWithCustomerBusinessLevels swcbl LEFT JOIN p.pricelist.agreement.customerBusinessLevel cbl WHERE (swcbl.uniqueId IN :businessLevelIds OR cbl.uniqueId IN :businessLevelIds) AND p.article.uniqueId = :articleUniqueId AND p.status IN :statuses AND p.price IS NOT NULL ORDER by p.pricelist.agreement.agreementNumber"),
    //    @NamedQuery(name = AgreementPricelistRow.FIND_BY_CUSTOMER_BUSINESS_LEVELS_AND_ARTICLE, query = "SELECT p FROM AgreementPricelistRow p LEFT JOIN p.pricelist.agreement.customerBusinessLevel swcbl WHERE swcbl.uniqueId IN :businessLevelIds AND p.article.uniqueId = :articleUniqueId AND p.status IN :statuses AND p.price IS NOT NULL ORDER by p.pricelist.agreement.agreementNumber"),
    @NamedQuery(name = AgreementPricelistRow.FIND_BY_SUPPLIER_AND_ARTICLE, query = "SELECT p FROM AgreementPricelistRow p WHERE p.pricelist.agreement.supplier.uniqueId = :organizationUniqueId AND p.article.uniqueId = :articleUniqueId ORDER by p.pricelist.agreement.agreementNumber"),
    @NamedQuery(name = AgreementPricelistRow.COUNT_BY_PRICELIST, query = "SELECT COUNT(p.uniqueId) FROM AgreementPricelistRow p WHERE p.pricelist.uniqueId = :pricelistUniqueId"),
    @NamedQuery(name = AgreementPricelistRow.COUNT_BY_AGREEMENT, query = "SELECT COUNT(p.uniqueId) FROM AgreementPricelistRow p WHERE p.pricelist.agreement.uniqueId = :agreementUniqueId"),
    @NamedQuery(name = AgreementPricelistRow.FIND_BY_PRICELIST_AND_ARTICLE, query = "SELECT p FROM AgreementPricelistRow p WHERE p.pricelist.uniqueId = :pricelistUniqueId AND p.article.uniqueId = :articleUniqueId"),
    @NamedQuery(name = AgreementPricelistRow.GET_PRICELISTROW_UNIQUE_ID_BY_PRICELIST_AND_ARTICLE, query = "SELECT p.uniqueId FROM AgreementPricelistRow p WHERE p.pricelist.uniqueId = :pricelistUniqueId AND p.article.uniqueId = :articleUniqueId"),
    @NamedQuery(name = AgreementPricelistRow.FIND_BY_PRICELIST_AND_STATUSES, query = "SELECT p FROM AgreementPricelistRow p WHERE p.pricelist.uniqueId = :pricelistUniqueId AND p.status IN :statuses"),
    @NamedQuery(name = AgreementPricelistRow.FIND_BY_PRICELIST_AND_STATUSES_AND_PUBLISHED_ARTICLE, query = "SELECT p FROM AgreementPricelistRow p WHERE p.pricelist.uniqueId = :pricelistUniqueId AND p.status IN :statuses AND p.article.status = 'PUBLISHED' ORDER BY p.article.articleNumber ASC"),
    @NamedQuery(name = AgreementPricelistRow.FIND_BY_PRICELIST_AND_STATUSES_WITH_PRICE, query = "SELECT p FROM AgreementPricelistRow p WHERE p.pricelist.uniqueId = :pricelistUniqueId AND p.status IN :statuses AND p.price IS NOT NULL"),
    @NamedQuery(name = AgreementPricelistRow.FIND_BY_PRICELIST, query = "SELECT p FROM AgreementPricelistRow p WHERE p.pricelist.uniqueId = :pricelistUniqueId"),
    @NamedQuery(name = AgreementPricelistRow.GET_ARTICLE_UNIQUE_IDS, query = "SELECT apr.article.uniqueId FROM AgreementPricelistRow apr WHERE apr.pricelist.uniqueId = :pricelistUniqueId"),
    @NamedQuery(name = AgreementPricelistRow.GET_ARTICLE_UNIQUE_NUMBERS, query = "SELECT apr.article.articleNumber FROM AgreementPricelistRow apr WHERE apr.pricelist.uniqueId = :pricelistUniqueId")
})
public class AgreementPricelistRow implements Serializable {

    //public static final String SEARCH_X = "HJMT_AGREEMENT_PRICELISTROW_SEARCH_X";
    public static final String SEARCH = "HJMT_AGREEMENT_PRICELISTROW_SEARCH";
    public static final String COUNT_SEARCH = "HJMT_AGREEMENT_PRICELISTROW_COUNT_SEARCH";
    public static final String FIND_BY_ARTICLE = "HJMT_AGREEMENT_PRICELISTROW_FIND_BY_ARTICLE";
    public static final String COUNT_BY_ARTICLES = "HJMT_AGREEMENT_PRICELISTROW_COUNT_BY_ARTICLES";
    public static final String FIND_BY_CUSTOMER_AND_ARTICLE = "HJMT_AGREEMENT_PRICELISTROW_FIND_BY_CUSTOMER_AND_ARTICLE";
    public static final String FIND_BY_CUSTOMER_BUSINESS_LEVELS_AND_ARTICLE = "HJMT_AGREEMENT_PRICELISTROW_FIND_BY_CUSTOMER_BUSINESS_LEVELS_AND_ARTICLE";
    public static final String FIND_BY_SUPPLIER_AND_ARTICLE = "HJMT_AGREEMENT_PRICELISTROW_FIND_BY_SUPPLIER_AND_ARTICLE";
    public static final String COUNT_BY_PRICELIST = "HJMT_AGREEMENT_PRICELISTROW_COUNT_BY_PRICELIST";
    public static final String COUNT_BY_AGREEMENT = "HJMT_AGREEMENT_PRICELISTROW_COUNT_BY_AGREEMENT";
    public static final String FIND_BY_PRICELIST_AND_ARTICLE = "HJMT_AGREEMENT_PRICELISTROW_FIND_BY_PRICELIST_AND_ARTICLE";
    public static final String FIND_BY_PRICELIST_AND_STATUSES = "HJMT_AGREEMENT_PRICELISTROW_FIND_BY_PRICELIST_AND_STATUSES";
    public static final String FIND_BY_PRICELIST_AND_STATUSES_AND_PUBLISHED_ARTICLE = "HJMT_AGREEMENT_PRICELISTROW_FIND_BY_PRICELIST_AND_STATUSES_AND_PUBLISHED_ARTICLE";
    public static final String FIND_BY_PRICELIST_AND_STATUSES_WITH_PRICE = "HJMT_AGREEMENT_PRICELISTROW_FIND_BY_PRICELIST_AND_STATUSES_WITH_PRICE";
    public static final String FIND_BY_PRICELIST = "HJMT_AGREEMENT_PRICELISTROW_FIND_BY_PRICELIST";
    public static final String GET_ARTICLE_UNIQUE_IDS = "HJMT_AGREEMENT_PRICELISTROW_GET_ARTICLE_UNIQUE_IDS";
    public static final String GET_ARTICLE_UNIQUE_NUMBERS = "HJMT_AGREEMENT_PRICELISTROW_GET_ARTICLE_UNIQUE_NUMBERS";
    public static final String GET_PRICELISTROW_UNIQUE_ID_BY_PRICELIST_AND_ARTICLE = "HJMT_AGREEMENT_PRICELISTROW_GET_PRICELISTROW_UNIQUE_ID_BY_PRICELIST_AND_ARTICLE";

    public enum Status {
        CREATED,
        PENDING_APPROVAL,
        PENDING_APPROVAL_SHOW_MORE,
        DECLINED,
        ACTIVE,
        PENDING_INACTIVATION,
        INACTIVE,
        CHANGED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = true)
    private BigDecimal price;

    @Column(nullable = true)
    @Enumerated(EnumType.STRING)
    private Status status;

    @Temporal(TemporalType.DATE)
    @Column(nullable = true)
    private Date validFrom;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean validFromOverridden;

    @Column(nullable = false)
    private int leastOrderQuantity;

    @Column(nullable = true)
    private Integer deliveryTime;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean deliveryTimeOverridden;

    @Column(nullable = true)
    private Integer warrantyQuantity;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean warrantyQuantityOverridden;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyQuantityUnitId", nullable = true)
    private CVGuaranteeUnit warrantyQuantityUnit;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean warrantyQuantityUnitOverridden;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyValidFromId", nullable = true)
    private CVPreventiveMaintenance warrantyValidFrom;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean warrantyValidFromOverridden;

    @Column(nullable = true)
    private String warrantyTerms;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean warrantyTermsOverridden;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agreementPricelistId", nullable = false)
    private AgreementPricelist pricelist;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "articleId", nullable = false)
    private Article article;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uniqueId", nullable = true)
    private ApprovedAgreementPricelistRow approvedAgreementPricelistRow;

    @Column(nullable = true)
    private String comment;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date lastUpdated;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public AgreementPricelist getPricelist() {
        return pricelist;
    }

    public void setPricelist(AgreementPricelist pricelist) {
        this.pricelist = pricelist;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public ApprovedAgreementPricelistRow getApprovedAgreementPricelistRow() {
        return approvedAgreementPricelistRow;
    }

    public void setApprovedAgreementPricelistRow(ApprovedAgreementPricelistRow approvedAgreementPricelistRow) {
        this.approvedAgreementPricelistRow = approvedAgreementPricelistRow;
    }

    public int getLeastOrderQuantity() {
        return leastOrderQuantity;
    }

    public void setLeastOrderQuantity(int leastOrderQuantity) {
        this.leastOrderQuantity = leastOrderQuantity;
    }

    public Integer getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Integer deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public boolean isDeliveryTimeOverridden() {
        return deliveryTimeOverridden;
    }

    public void setDeliveryTimeOverridden(boolean deliveryTimeOverridden) {
        this.deliveryTimeOverridden = deliveryTimeOverridden;
    }

    public Integer getWarrantyQuantity() {
        return warrantyQuantity;
    }

    public void setWarrantyQuantity(Integer warrantyQuantity) {
        this.warrantyQuantity = warrantyQuantity;
    }

    public boolean isWarrantyQuantityOverridden() {
        return warrantyQuantityOverridden;
    }

    public void setWarrantyQuantityOverridden(boolean warrantyQuantityOverridden) {
        this.warrantyQuantityOverridden = warrantyQuantityOverridden;
    }

    public CVGuaranteeUnit getWarrantyQuantityUnit() {
        return warrantyQuantityUnit;
    }

    public void setWarrantyQuantityUnit(CVGuaranteeUnit warrantyQuantityUnit) {
        this.warrantyQuantityUnit = warrantyQuantityUnit;
    }

    public boolean isWarrantyQuantityUnitOverridden() {
        return warrantyQuantityUnitOverridden;
    }

    public void setWarrantyQuantityUnitOverridden(boolean warrantyQuantityUnitOverridden) {
        this.warrantyQuantityUnitOverridden = warrantyQuantityUnitOverridden;
    }

    public CVPreventiveMaintenance getWarrantyValidFrom() {
        return warrantyValidFrom;
    }

    public void setWarrantyValidFrom(CVPreventiveMaintenance warrantyValidFrom) {
        this.warrantyValidFrom = warrantyValidFrom;
    }

    public boolean isWarrantyValidFromOverridden() {
        return warrantyValidFromOverridden;
    }

    public void setWarrantyValidFromOverridden(boolean warrantyValidFromOverridden) {
        this.warrantyValidFromOverridden = warrantyValidFromOverridden;
    }

    public String getWarrantyTerms() {
        return warrantyTerms;
    }

    public void setWarrantyTerms(String warrantyTerms) {
        this.warrantyTerms = warrantyTerms;
    }

    public boolean isWarrantyTermsOverridden() {
        return warrantyTermsOverridden;
    }

    public void setWarrantyTermsOverridden(boolean warrantyTermsOverridden) {
        this.warrantyTermsOverridden = warrantyTermsOverridden;
    }

    public boolean isValidFromOverridden() {
        return validFromOverridden;
    }

    public void setValidFromOverridden(boolean validFromOverridden) {
        this.validFromOverridden = validFromOverridden;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }

    @PrePersist
    private void prePersist() {
        this.created = new Date();
    }

    @PreUpdate
    private void preUpdate() {
        this.lastUpdated = new Date();
    }

}
