package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

/**
 * Answer to interval type.
 *
 */
@Entity
@DiscriminatorValue(value = "INTERVAL")
public class ResourceSpecificPropertyValueInterval extends ResourceSpecificPropertyValue {

    @Column(nullable = true)
    private Double intervalFromValue;

    @Column(nullable = true)
    private Double intervalToValue;

    public Double getFromValue() {
        return intervalFromValue;
    }

    public void setFromValue(Double fromValue) {
        this.intervalFromValue = fromValue;
    }

    public Double getToValue() {
        return intervalToValue;
    }

    public void setToValue(Double toValue) {
        this.intervalToValue = toValue;
    }

}
