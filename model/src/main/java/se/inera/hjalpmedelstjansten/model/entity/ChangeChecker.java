package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import java.io.Serializable;

/**
 * JPA Entity class for changed agreement
 *
 */
@Entity
public class ChangeChecker implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column
    private String type;

    @Column
    private Long changeId;

    @Column
    private Long userId;

    @Column
    private Long assortmentId;

    public Long getUniqueId() {
        return uniqueId;
    }
    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public Long getChangeId() {
        return changeId;
    }
    public void setChangeId(Long changeId) {
        this.changeId = changeId;
    }

    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getAssortmentId() {
        return assortmentId;
    }
    public void setAssortmentId(Long assortmentId) {
        this.assortmentId = assortmentId;
    }
}
