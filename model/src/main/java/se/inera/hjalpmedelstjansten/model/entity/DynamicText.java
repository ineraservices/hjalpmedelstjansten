package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;


@Entity
public class DynamicText implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = true, length = 10000)
    private String dynamicText;

    @Temporal(TemporalType.TIMESTAMP)
    private Date whenChanged;

    @Column(nullable = false)
    private String changedBy;

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Long getUniqueId() {
        return uniqueId;
    }


    public void setDynamicText(String dynamicText) {
        this.dynamicText = dynamicText;
    }

    public String getDynamicText() {
        return dynamicText;
    }


    public void setWhenChanged(Date whenChanged) {
        this.whenChanged = whenChanged;
    }

    public Date getWhenChanged() {
        return whenChanged;
    }


    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy;
    }

    public String getChangedBy() {
        return changedBy;
    }


}
