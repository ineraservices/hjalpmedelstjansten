package se.inera.hjalpmedelstjansten.model.entity;

import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Transient;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity
@Setter
@Getter
public class UserNotification implements Serializable {

    @Transient
    private static final long serialVersionUID = -4605279777503800785L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long uniqueId;

    @OneToOne(fetch = FetchType.LAZY)
    private UserAccount userAccount;

    @ManyToMany(
        targetEntity = Notification.class,
        fetch = FetchType.LAZY,
        cascade = {
            CascadeType.PERSIST,
            CascadeType.REFRESH,
            CascadeType.MERGE
        })
    @JoinTable(name = "UserAccountNotification",
            joinColumns = @JoinColumn(name = "userId", referencedColumnName = "userAccount_uniqueId"),
            inverseJoinColumns = @JoinColumn(name = "notificationId", referencedColumnName = "uniqueId"))
    private Set<Notification> notifications = new HashSet<>();

}


