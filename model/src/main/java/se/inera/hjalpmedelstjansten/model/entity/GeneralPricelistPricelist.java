package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * JPA Entity class for Pricelist for a General Pricelist. Status is not saved per
 * pricelist, instead it is calculated when needed based on the validFrom, this
 * in order to avoid writing a batch job to update statuses daily.
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = GeneralPricelistPricelist.GET_ALL_BY_GENERAL_PRICELIST, query = "SELECT p FROM GeneralPricelistPricelist p WHERE p.generalPricelist.uniqueId = :generalPricelistUniqueId ORDER BY p.validFrom ASC"),
    @NamedQuery(name = GeneralPricelistPricelist.FIND_BY_VALID_FROM_AND_GENERAL_PRICELIST, query = "SELECT p FROM GeneralPricelistPricelist p WHERE p.generalPricelist.uniqueId = :generalPricelistUniqueId AND p.validFrom = :validFrom"),
    @NamedQuery(name = GeneralPricelistPricelist.FIND_BY_NUMBER_AND_GENERAL_PRICELIST, query = "SELECT p FROM GeneralPricelistPricelist p WHERE p.generalPricelist.uniqueId = :generalPricelistUniqueId AND p.number = :number"),
    @NamedQuery(name = GeneralPricelistPricelist.FIND_BY_PASSED_VALID_FROM_AND_ORGANIZATION, query = "SELECT p FROM GeneralPricelistPricelist p WHERE p.generalPricelist.ownerOrganization.uniqueId = :organizationUniqueId AND p.validFrom <= :validFrom ORDER BY p.validFrom DESC"),
    @NamedQuery(name = GeneralPricelistPricelist.FIND_BY_PASSED_VALID_FROM_AND_GENERAL_PRICELIST, query = "SELECT p FROM GeneralPricelistPricelist p WHERE p.generalPricelist.uniqueId = :generalPricelistUniqueId AND p.validFrom <= :validFrom ORDER BY p.validFrom DESC"),
        @NamedQuery(name = GeneralPricelistPricelist.FIND_BY_UNIQUE_ID, query = "SELECT p FROM GeneralPricelistPricelist p WHERE p.uniqueId = :generalPricelistUniqueId")
})
public class GeneralPricelistPricelist implements Serializable {

    public static final String GET_ALL_BY_GENERAL_PRICELIST = "HJMT_PRICELIST_GET_ALL_BY_GENERAL_PRICELIST";
    public static final String FIND_BY_VALID_FROM_AND_GENERAL_PRICELIST = "HJMT_PRICELIST_FIND_BY_VALID_FROM_AND_GENERAL_PRICELIST";
    public static final String FIND_BY_NUMBER_AND_GENERAL_PRICELIST = "HJMT_PRICELIST_FIND_BY_NUMBER_AND_GENERAL_PRICELIST";
    public static final String FIND_BY_PASSED_VALID_FROM_AND_ORGANIZATION = "HJMT_PRICELIST_FIND_BY_PASSED_VALID_FROM_AND_ORGANIZATION";
    public static final String FIND_BY_PASSED_VALID_FROM_AND_GENERAL_PRICELIST = "HJMT_PRICELIST_FIND_BY_PASSED_VALID_FROM_AND_GENERAL_PRICELIST";
    public static final String FIND_BY_UNIQUE_ID = "HJMT_PRICELIST_FIND_BY_UNIQUE_ID";

    public enum Status {
        PAST, CURRENT, FUTURE
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = false)
    private String number;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "generalPricelistId", nullable = false)
    private GeneralPricelist generalPricelist;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date validFrom;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date lastUpdated;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public GeneralPricelist getGeneralPricelist() {
        return generalPricelist;
    }

    public void setGeneralPricelist(GeneralPricelist generalPricelist) {
        this.generalPricelist = generalPricelist;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @PrePersist
    private void prePersist() {
        this.created = new Date();
    }

    @PreUpdate
    private void preUpdate() {
        this.lastUpdated = new Date();
    }

}
