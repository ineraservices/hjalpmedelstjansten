package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.ManyToOne;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * JPA Entity class for General Pricelist. Only one general pricelist per
 * organization (supplier) is allowed.
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = GeneralPricelist.FIND_BY_ORGANIZATION, query = "SELECT gp FROM GeneralPricelist gp WHERE gp.ownerOrganization.uniqueId = :organizationUniqueId")
})
public class GeneralPricelist implements Serializable {

    public static final String FIND_BY_ORGANIZATION = "HJMT_GENERALPRICELIST_FIND_BY_ORGANIZATION";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column()
    private String generalPricelistName;

    @Column(nullable = false)
    private String generalPricelistNumber;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ownerOrganizationId", nullable = false, unique = true)
    private Organization ownerOrganization;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date validFrom;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date validTo;

    @Column()
    private Integer deliveryTimeH;

    @Column()
    private Integer deliveryTimeT;

    @Column()
    private Integer deliveryTimeI;

    @Column()
    private Integer deliveryTimeR;

    @Column()
    private Integer deliveryTimeTJ;

    @Column()
    private Integer warrantyQuantityH;

    @Column()
    private Integer warrantyQuantityT;

    @Column()
    private Integer warrantyQuantityI;

    @Column()
    private Integer warrantyQuantityR;

    @Column()
    private Integer warrantyQuantityTJ;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "warrantyQuantityHUnitId")
    private CVGuaranteeUnit warrantyQuantityHUnit;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "warrantyQuantityTUnitId")
    private CVGuaranteeUnit warrantyQuantityTUnit;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "warrantyQuantityIUnitId")
    private CVGuaranteeUnit warrantyQuantityIUnit;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "warrantyQuantityRUnitId")
    private CVGuaranteeUnit warrantyQuantityRUnit;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "warrantyQuantityTJUnitId")
    private CVGuaranteeUnit warrantyQuantityTJUnit;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "warrantyValidFromHId")
    private CVPreventiveMaintenance warrantyValidFromH;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "warrantyValidFromTId")
    private CVPreventiveMaintenance warrantyValidFromT;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "warrantyValidFromIId")
    private CVPreventiveMaintenance warrantyValidFromI;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "warrantyValidFromRId")
    private CVPreventiveMaintenance warrantyValidFromR;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "warrantyValidFromTJId")
    private CVPreventiveMaintenance warrantyValidFromTJ;

    @Column()
    private String warrantyTermsH;

    @Column()
    private String warrantyTermsT;

    @Column()
    private String warrantyTermsI;

    @Column()
    private String warrantyTermsR;

    @Column()
    private String warrantyTermsTJ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column()
    private Date lastUpdated;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getGeneralPricelistName() {
        return generalPricelistName;
    }

    public void setGeneralPricelistName(String generalPricelistName) {
        this.generalPricelistName = generalPricelistName;
    }

    public String getGeneralPricelistNumber() {
        return generalPricelistNumber;
    }

    public void setGeneralPricelistNumber(String generalPricelistNumber) {
        this.generalPricelistNumber = generalPricelistNumber;
    }

    public Organization getOwnerOrganization() {
        return ownerOrganization;
    }

    public void setOwnerOrganization(Organization ownerOrganization) {
        this.ownerOrganization = ownerOrganization;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public Integer getDeliveryTimeH() {
        return deliveryTimeH;
    }

    public void setDeliveryTimeH(Integer deliveryTimeH) {
        this.deliveryTimeH = deliveryTimeH;
    }

    public Integer getDeliveryTimeT() {
        return deliveryTimeT;
    }

    public void setDeliveryTimeT(Integer deliveryTimeT) {
        this.deliveryTimeT = deliveryTimeT;
    }

    public Integer getDeliveryTimeI() {
        return deliveryTimeI;
    }

    public void setDeliveryTimeI(Integer deliveryTimeI) {
        this.deliveryTimeI = deliveryTimeI;
    }

    public Integer getDeliveryTimeR() {
        return deliveryTimeR;
    }

    public void setDeliveryTimeR(Integer deliveryTimeR) {
        this.deliveryTimeR = deliveryTimeR;
    }

    public Integer getDeliveryTimeTJ() {
        return deliveryTimeTJ;
    }

    public void setDeliveryTimeTJ(Integer deliveryTimeTJ) {
        this.deliveryTimeTJ = deliveryTimeTJ;
    }

    public Integer getWarrantyQuantityH() {
        return warrantyQuantityH;
    }

    public void setWarrantyQuantityH(Integer warrantyQuantityH) {
        this.warrantyQuantityH = warrantyQuantityH;
    }

    public Integer getWarrantyQuantityT() {
        return warrantyQuantityT;
    }

    public void setWarrantyQuantityT(Integer warrantyQuantityT) {
        this.warrantyQuantityT = warrantyQuantityT;
    }

    public Integer getWarrantyQuantityI() {
        return warrantyQuantityI;
    }

    public void setWarrantyQuantityI(Integer warrantyQuantityI) {
        this.warrantyQuantityI = warrantyQuantityI;
    }

    public Integer getWarrantyQuantityR() {
        return warrantyQuantityR;
    }

    public void setWarrantyQuantityR(Integer warrantyQuantityR) {
        this.warrantyQuantityR = warrantyQuantityR;
    }

    public Integer getWarrantyQuantityTJ() {
        return warrantyQuantityTJ;
    }

    public void setWarrantyQuantityTJ(Integer warrantyQuantityTJ) {
        this.warrantyQuantityTJ = warrantyQuantityTJ;
    }

    public CVGuaranteeUnit getWarrantyQuantityHUnit() {
        return warrantyQuantityHUnit;
    }

    public void setWarrantyQuantityHUnit(CVGuaranteeUnit warrantyQuantityHUnit) {
        this.warrantyQuantityHUnit = warrantyQuantityHUnit;
    }

    public CVGuaranteeUnit getWarrantyQuantityTUnit() {
        return warrantyQuantityTUnit;
    }

    public void setWarrantyQuantityTUnit(CVGuaranteeUnit warrantyQuantityTUnit) {
        this.warrantyQuantityTUnit = warrantyQuantityTUnit;
    }

    public CVGuaranteeUnit getWarrantyQuantityIUnit() {
        return warrantyQuantityIUnit;
    }

    public void setWarrantyQuantityIUnit(CVGuaranteeUnit warrantyQuantityIUnit) {
        this.warrantyQuantityIUnit = warrantyQuantityIUnit;
    }

    public CVGuaranteeUnit getWarrantyQuantityRUnit() {
        return warrantyQuantityRUnit;
    }

    public void setWarrantyQuantityRUnit(CVGuaranteeUnit warrantyQuantityRUnit) {
        this.warrantyQuantityRUnit = warrantyQuantityRUnit;
    }

    public CVGuaranteeUnit getWarrantyQuantityTJUnit() {
        return warrantyQuantityTJUnit;
    }

    public void setWarrantyQuantityTJUnit(CVGuaranteeUnit warrantyQuantityTJUnit) {
        this.warrantyQuantityTJUnit = warrantyQuantityTJUnit;
    }

    public CVPreventiveMaintenance getWarrantyValidFromH() {
        return warrantyValidFromH;
    }

    public void setWarrantyValidFromH(CVPreventiveMaintenance warrantyValidFromH) {
        this.warrantyValidFromH = warrantyValidFromH;
    }

    public CVPreventiveMaintenance getWarrantyValidFromT() {
        return warrantyValidFromT;
    }

    public void setWarrantyValidFromT(CVPreventiveMaintenance warrantyValidFromT) {
        this.warrantyValidFromT = warrantyValidFromT;
    }

    public CVPreventiveMaintenance getWarrantyValidFromI() {
        return warrantyValidFromI;
    }

    public void setWarrantyValidFromI(CVPreventiveMaintenance warrantyValidFromI) {
        this.warrantyValidFromI = warrantyValidFromI;
    }

    public CVPreventiveMaintenance getWarrantyValidFromR() {
        return warrantyValidFromR;
    }

    public void setWarrantyValidFromR(CVPreventiveMaintenance warrantyValidFromR) {
        this.warrantyValidFromR = warrantyValidFromR;
    }

    public CVPreventiveMaintenance getWarrantyValidFromTJ() {
        return warrantyValidFromTJ;
    }

    public void setWarrantyValidFromTJ(CVPreventiveMaintenance warrantyValidFromTJ) {
        this.warrantyValidFromTJ = warrantyValidFromTJ;
    }

    public String getWarrantyTermsH() {
        return warrantyTermsH;
    }

    public void setWarrantyTermsH(String warrantyTermsH) {
        this.warrantyTermsH = warrantyTermsH;
    }

    public String getWarrantyTermsT() {
        return warrantyTermsT;
    }

    public void setWarrantyTermsT(String warrantyTermsT) {
        this.warrantyTermsT = warrantyTermsT;
    }

    public String getWarrantyTermsI() {
        return warrantyTermsI;
    }

    public void setWarrantyTermsI(String warrantyTermsI) {
        this.warrantyTermsI = warrantyTermsI;
    }

    public String getWarrantyTermsR() {
        return warrantyTermsR;
    }

    public void setWarrantyTermsR(String warrantyTermsR) {
        this.warrantyTermsR = warrantyTermsR;
    }

    public String getWarrantyTermsTJ() {
        return warrantyTermsTJ;
    }

    public void setWarrantyTermsTJ(String warrantyTermsTJ) {
        this.warrantyTermsTJ = warrantyTermsTJ;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @PrePersist
    private void prePersist() {
        this.created = new Date();
    }

    @PreUpdate
    private void preUpdate() {
        this.lastUpdated = new Date();
    }

}
