package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;


@Entity
@NamedQueries({
    @NamedQuery(name = UserAccountToken.FIND_BY_EXPIRED_TOKENS, query = "SELECT uat FROM UserAccountToken uat WHERE uat.tokenExpires < :date"),
    @NamedQuery(name = UserAccountToken.FIND_BY_USERACCOUNT, query = "SELECT uat FROM UserAccountToken uat WHERE uat.userAccount.uniqueId = :userAccountUniqueId")
})
public class UserAccountToken implements Serializable {

    public static final String FIND_BY_EXPIRED_TOKENS = "HJMTJ_USERACCOUNTTOKEN_FIND_BY_EXPIRED_TOKENS";
    public static final String FIND_BY_USERACCOUNT = "HJMTJ_USERACCOUNTTOKEN_FIND_BY_USERACCOUNT";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = false)
    private String token;

    @Column(nullable = true)
    private String salt;

    @Column(nullable = true)
    private int iterations;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date tokenExpires;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userAccountId", nullable = false)
    private UserAccount userAccount;

    public Long getUniqueId() {
        return uniqueId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public Date getTokenExpires() {
        return tokenExpires;
    }

    public void setTokenExpires(Date tokenExpires) {
        this.tokenExpires = tokenExpires;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

}
