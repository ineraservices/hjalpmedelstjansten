package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import java.io.Serializable;
import java.util.List;


@Entity
@NamedQueries({
    @NamedQuery(name = UserRole.FIND_BY_NAMES, query = "SELECT u FROM UserRole u WHERE u.name IN :names")
})
public class UserRole implements Serializable {

    public enum RoleName {
        Baseuser,
        BaseTokenuser,
        Superadmin,
        Supplieradmin,
        SupplierAgreementViewer,
        SupplierAgreementManager,
        SupplierProductAndArticleHandler,
        Customeradmin,
        CustomerAgreementViewer,
        CustomerAgreementManager,
        CustomerAssortmentManager,
        CustomerAssignedAssortmentManager
    }

    public static final String FIND_BY_NAMES =  "HJMT_USERROLE_FIND_BY_NAMES";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(unique = true, nullable = false)
    @Enumerated(EnumType.STRING)
    private RoleName name;

    @Column(nullable = false)
    private String description;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name="UserRoleUserPermission",
        joinColumns={@JoinColumn(name="userRoleId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="userPermissionId", referencedColumnName="uniqueId")})
    private List<UserPermission> permissions;

    public Long getUniqueId() {
        return uniqueId;
    }

    public RoleName getName() {
        return name;
    }

    public void setName(RoleName name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<UserPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<UserPermission> permissions) {
        this.permissions = permissions;
    }

}
