package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.ManyToOne;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * JPA Entity class for General pricelist pricelist row
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = GeneralPricelistPricelistRow.COUNT_SEARCH, query = "SELECT COUNT(p.uniqueId) FROM GeneralPricelistPricelistRow p LEFT JOIN p.article.category ac WHERE p.pricelist.uniqueId = :pricelistUniqueId AND (p.article.articleName LIKE :query OR p.article.articleNumber LIKE :query)"),
    @NamedQuery(name = GeneralPricelistPricelistRow.SEARCH, query = "SELECT p FROM GeneralPricelistPricelistRow p LEFT JOIN p.article.category ac WHERE p.pricelist.uniqueId = :pricelistUniqueId AND (p.article.articleName LIKE :query OR p.article.articleNumber LIKE :query) ORDER BY p.article.articleName ASC"),
    @NamedQuery(name = GeneralPricelistPricelistRow.COUNT_BY_GENERAL_PRICELIST, query = "SELECT COUNT(p.uniqueId) FROM GeneralPricelistPricelistRow p WHERE p.pricelist.generalPricelist.uniqueId = :generalPricelistId"),
    @NamedQuery(name = GeneralPricelistPricelistRow.FIND_BY_GENERAL_PRICELIST_PRICELIST, query = "SELECT p FROM GeneralPricelistPricelistRow p WHERE p.pricelist.uniqueId = :generalPricelistPricelistId  ORDER BY p.article.articleNumber"),
    @NamedQuery(name = GeneralPricelistPricelistRow.FIND_BY_GENERAL_PRICELIST_PRICELIST_AND_STATUS, query = "SELECT p FROM GeneralPricelistPricelistRow p WHERE p.pricelist.uniqueId = :generalPricelistPricelistId AND p.status IN :statuses"),
    @NamedQuery(name = GeneralPricelistPricelistRow.FIND_BY_GENERAL_PRICELIST_PRICELIST_AND_DISCONTINUED, query = "SELECT p FROM GeneralPricelistPricelistRow p WHERE p.pricelist.uniqueId = :generalPricelistPricelistId AND p.article.status = 'DISCONTINUED' ORDER BY p.article.articleNumber"),
    @NamedQuery(name = GeneralPricelistPricelistRow.FIND_BY_GENERAL_PRICELIST_PRICELIST_AND_NOT_DISCONTINUED, query = "SELECT p FROM GeneralPricelistPricelistRow p WHERE p.pricelist.uniqueId = :generalPricelistPricelistId AND p.article.status <> 'DISCONTINUED'  ORDER BY p.article.articleNumber"),
    @NamedQuery(name = GeneralPricelistPricelistRow.COUNT_BY_PRICELIST, query = "SELECT COUNT(p.uniqueId) FROM GeneralPricelistPricelistRow p WHERE p.pricelist.uniqueId = :pricelistUniqueId"),
    @NamedQuery(name = GeneralPricelistPricelistRow.FIND_BY_GENERAL_PRICELIST_AND_ARTICLE, query = "SELECT p FROM GeneralPricelistPricelistRow p WHERE p.pricelist.uniqueId = :pricelistUniqueId AND p.article.uniqueId = :articleUniqueId"),
    @NamedQuery(name = GeneralPricelistPricelistRow.FIND_BY_ARTICLE, query = "SELECT p FROM GeneralPricelistPricelistRow p WHERE p.article.uniqueId = :articleUniqueId"),
    @NamedQuery(name = GeneralPricelistPricelistRow.COUNT_BY_ARTICLES, query = "SELECT COUNT(p.uniqueId) FROM GeneralPricelistPricelistRow p WHERE p.article.uniqueId IN :articleUniqueIds"),
    @NamedQuery(name = GeneralPricelistPricelistRow.FIND_BY_ARTICLE_AND_OWNER, query = "SELECT p FROM GeneralPricelistPricelistRow p WHERE p.article.uniqueId = :articleUniqueId AND p.pricelist.generalPricelist.ownerOrganization.uniqueId = :organizationUniqueId"),
    @NamedQuery(name = GeneralPricelistPricelistRow.FIND_BY_ARTICLE_WITH_PRICE, query = "SELECT p FROM GeneralPricelistPricelistRow p WHERE p.article.uniqueId = :articleUniqueId AND p.price IS NOT NULL"),
    @NamedQuery(name = GeneralPricelistPricelistRow.GET_ARTICLE_UNIQUE_NUMBERS, query = "SELECT gpr.article.articleNumber FROM GeneralPricelistPricelistRow gpr WHERE gpr.pricelist.uniqueId = :pricelistUniqueId"),
    @NamedQuery(name = GeneralPricelistPricelistRow.GET_ARTICLE_UNIQUE_IDS, query = "SELECT gpr.article.uniqueId FROM GeneralPricelistPricelistRow gpr WHERE gpr.pricelist.uniqueId = :pricelistUniqueId"),
    @NamedQuery(name = GeneralPricelistPricelistRow.GET_PRICELISTROW_UNIQUE_ID_BY_PRICELIST_AND_ARTICLE, query = "SELECT p.uniqueId FROM GeneralPricelistPricelistRow p WHERE p.pricelist.uniqueId = :pricelistUniqueId AND p.article.uniqueId = :articleUniqueId")
})
public class GeneralPricelistPricelistRow implements Serializable {

    public static final String SEARCH = "HJMT_GENERALPRICELIST_PRICELISTROW_SEARCH";
    public static final String COUNT_SEARCH = "HJMT_GENERALPRICELIST_PRICELISTROW_COUNT_SEARCH";
    public static final String COUNT_BY_GENERAL_PRICELIST = "HJMT_GENERALPRICELIST_PRICELISTROW_COUNT_BY_GENERAL_PRICELIST";
    public static final String FIND_BY_GENERAL_PRICELIST_PRICELIST = "HJMT_GENERALPRICELIST_PRICELISTROW_FIND_BY_GENERAL_PRICELIST_PRICELIST";
    public static final String FIND_BY_GENERAL_PRICELIST_PRICELIST_AND_STATUS = "HJMT_GENERALPRICELIST_PRICELISTROW_FIND_BY_GENERAL_PRICELIST_PRICELIST_AND_STATUS";
    public static final String FIND_BY_GENERAL_PRICELIST_PRICELIST_AND_DISCONTINUED = "HJMT_GENERALPRICELIST_PRICELISTROW_FIND_BY_GENERAL_PRICELIST_PRICELIST_AND_DISCONTINUED";
    public static final String FIND_BY_GENERAL_PRICELIST_PRICELIST_AND_NOT_DISCONTINUED = "HJMT_GENERALPRICELIST_PRICELISTROW_FIND_BY_GENERAL_PRICELIST_PRICELIST_AND_NOT_DISCONTINUED";
    public static final String COUNT_BY_PRICELIST = "HJMT_GENERALPRICELIST_PRICELISTROW_COUNT_BY_PRICELIST";
    public static final String FIND_BY_GENERAL_PRICELIST_AND_ARTICLE = "HJMT_GENERALPRICELIST_PRICELISTROW_FIND_BY_GENERAL_PRICELIST_AND_ARTICLE";
    public static final String FIND_BY_ARTICLE = "HJMT_GENERALPRICELIST_PRICELISTROW_FIND_BY_ARTICLE";
    public static final String COUNT_BY_ARTICLES = "HJMT_GENERALPRICELIST_PRICELISTROW_COUNT_BY_ARTICLES";
    public static final String FIND_BY_ARTICLE_WITH_PRICE = "HJMT_GENERALPRICELIST_PRICELISTROW_FIND_BY_ARTICLE_WITH_PRICE";
    public static final String FIND_BY_ARTICLE_AND_OWNER = "HJMT_GENERALPRICELIST_PRICELISTROW_FIND_BY_ARTICLE_AND_OWNER";
    public static final String GET_ARTICLE_UNIQUE_NUMBERS = "HJMT_GENERALPRICELIST_PRICELISTROW_GET_ARTICLE_UNIQUE_NUMBERS";
    public static final String GET_ARTICLE_UNIQUE_IDS = "HJMT_GENERALPRICELIST_PRICELISTROW_GET_ARTICLE_UNIQUE_IDS";
    public static final String GET_PRICELISTROW_UNIQUE_ID_BY_PRICELIST_AND_ARTICLE = "HJMT_GENERALPRICELIST_PRICELISTROW_GET_PRICELISTROW_UNIQUE_ID_BY_PRICELIST_AND_ARTICLE";

    public enum Status {
        ACTIVE,
        INACTIVE
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = true)
    private BigDecimal price;

    @Temporal(TemporalType.DATE)
    @Column(nullable = true)
    private Date validFrom;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean validFromOverridden;

    @Column(nullable = true)
    private Integer leastOrderQuantity;

    @Column(nullable = true)
    private Integer deliveryTime;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean deliveryTimeOverridden;

    @Column(nullable = true)
    private Integer warrantyQuantity;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean warrantyQuantityOverridden;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyQuantityUnitId", nullable = true)
    private CVGuaranteeUnit warrantyQuantityUnit;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean warrantyQuantityUnitOverridden;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyValidFromId", nullable = true)
    private CVPreventiveMaintenance warrantyValidFrom;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean warrantyValidFromOverridden;

    @Column(nullable = true)
    private String warrantyTerms;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean warrantyTermsOverridden;

    @Column(nullable = true)
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "generalPricelistPricelistId", nullable = false)
    private GeneralPricelistPricelist pricelist;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "articleId", nullable = false)
    private Article article;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date lastUpdated;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public GeneralPricelistPricelist getPricelist() {
        return pricelist;
    }

    public void setPricelist(GeneralPricelistPricelist pricelist) {
        this.pricelist = pricelist;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Integer getLeastOrderQuantity() {
        return leastOrderQuantity;
    }

    public void setLeastOrderQuantity(Integer leastOrderQuantity) {
        this.leastOrderQuantity = leastOrderQuantity;
    }

    public Integer getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Integer deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public boolean isDeliveryTimeOverridden() {
        return deliveryTimeOverridden;
    }

    public void setDeliveryTimeOverridden(boolean deliveryTimeOverridden) {
        this.deliveryTimeOverridden = deliveryTimeOverridden;
    }

    public Integer getWarrantyQuantity() {
        return warrantyQuantity;
    }

    public void setWarrantyQuantity(Integer warrantyQuantity) {
        this.warrantyQuantity = warrantyQuantity;
    }

    public boolean isWarrantyQuantityOverridden() {
        return warrantyQuantityOverridden;
    }

    public void setWarrantyQuantityOverridden(boolean warrantyQuantityOverridden) {
        this.warrantyQuantityOverridden = warrantyQuantityOverridden;
    }

    public CVGuaranteeUnit getWarrantyQuantityUnit() {
        return warrantyQuantityUnit;
    }

    public void setWarrantyQuantityUnit(CVGuaranteeUnit warrantyQuantityUnit) {
        this.warrantyQuantityUnit = warrantyQuantityUnit;
    }

    public boolean isWarrantyQuantityUnitOverridden() {
        return warrantyQuantityUnitOverridden;
    }

    public void setWarrantyQuantityUnitOverridden(boolean warrantyQuantityUnitOverridden) {
        this.warrantyQuantityUnitOverridden = warrantyQuantityUnitOverridden;
    }

    public CVPreventiveMaintenance getWarrantyValidFrom() {
        return warrantyValidFrom;
    }

    public void setWarrantyValidFrom(CVPreventiveMaintenance warrantyValidFrom) {
        this.warrantyValidFrom = warrantyValidFrom;
    }

    public boolean isWarrantyValidFromOverridden() {
        return warrantyValidFromOverridden;
    }

    public void setWarrantyValidFromOverridden(boolean warrantyValidFromOverridden) {
        this.warrantyValidFromOverridden = warrantyValidFromOverridden;
    }

    public String getWarrantyTerms() {
        return warrantyTerms;
    }

    public void setWarrantyTerms(String warrantyTerms) {
        this.warrantyTerms = warrantyTerms;
    }

    public boolean isWarrantyTermsOverridden() {
        return warrantyTermsOverridden;
    }

    public void setWarrantyTermsOverridden(boolean warrantyTermsOverridden) {
        this.warrantyTermsOverridden = warrantyTermsOverridden;
    }

    public boolean isValidFromOverridden() {
        return validFromOverridden;
    }

    public void setValidFromOverridden(boolean validFromOverridden) {
        this.validFromOverridden = validFromOverridden;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @PrePersist
    private void prePersist() {
        this.created = new Date();
    }

    @PreUpdate
    private void preUpdate() {
        this.lastUpdated = new Date();
    }

}
