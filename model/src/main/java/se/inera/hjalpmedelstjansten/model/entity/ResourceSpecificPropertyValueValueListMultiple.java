package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import java.util.List;

/**
 * Answer/s to list of multiple type. Name of join table abbreviated because it
 * was too long for the database.
 *
 */
@Entity
@DiscriminatorValue(value = "LISTMULTIPLE")
@NamedQueries({
        @NamedQuery(name = ResourceSpecificPropertyValueValueListMultiple.FIND_RESOURCES_BY_PROPERTYID, query = "SELECT c FROM ResourceSpecificPropertyValueValueListMultiple c WHERE c.uniqueId = :propertyId"),
})
public class ResourceSpecificPropertyValueValueListMultiple extends ResourceSpecificPropertyValue {

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name="RSPValueValueListMultipleCSPListValue",
        joinColumns={@JoinColumn(name="resourceSpecificPropertyValueValueListMultipleId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="categorySpecificPropertyListValueId", referencedColumnName="uniqueId")})
    private List<CategorySpecificPropertyListValue> values;

    public static final String FIND_RESOURCES_BY_PROPERTYID = "HJMT_CATEGORY_SPECIFIC_RESOURCEVALUES_FIND_BY_PROPERTYID";
    public List<CategorySpecificPropertyListValue> getValues() {
        return values;
    }

    public void setValues(List<CategorySpecificPropertyListValue> values) {
        this.values = values;
    }

}
