package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.*;
import java.io.Serializable;

/**
 * Grouping of categories as defined by external api
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = ExternalCategoryGrouping.FIND_ALL, query = "SELECT ecg FROM ExternalCategoryGrouping ecg ORDER BY ecg.uniqueId ASC"),
    @NamedQuery(name = ExternalCategoryGrouping.FIND_ALL_NON_CATEGORY, query = "SELECT ecg FROM ExternalCategoryGrouping ecg WHERE ecg.category IS NULL ORDER BY ecg.uniqueId ASC"),
    @NamedQuery(name = ExternalCategoryGrouping.FIND_BY_CATEGORYID, query = "SELECT ecg FROM ExternalCategoryGrouping ecg WHERE ecg.category.uniqueId = :categoryId"),
    @NamedQuery(name = ExternalCategoryGrouping.FIND_BY_ID, query = "SELECT ecg FROM ExternalCategoryGrouping ecg WHERE ecg.uniqueId = :uniqueId")
})
public class ExternalCategoryGrouping implements Serializable {

    public static final String FIND_ALL = "HJMT_EXTERNALCATEGORYGROUPING_FIND_ALL";
    public static final String FIND_ALL_NON_CATEGORY = "HJMT_EXTERNALCATEGORYGROUPING_FIND_ALL_NON_CATEGORY";
    public static final String FIND_BY_CATEGORYID = "HJMT_EXTERNALCATEGORYGROUPING_FIND_BY_CATEGORYID";
    public static final String FIND_BY_ID = "HJMT_EXTERNALCATEGORYGROUPING_FIND_BY_ID";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(length = 500)
    private String description;

    @Column()
    private Integer displayOrder;
    @Column(nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="categoryId", referencedColumnName = "uniqueId")
    private Category category;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parentId", referencedColumnName = "uniqueId")
    private ExternalCategoryGrouping parent;



    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public ExternalCategoryGrouping getParent() {
        return parent;
    }

    public void setParent(ExternalCategoryGrouping parent) {
        this.parent = parent;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

}
