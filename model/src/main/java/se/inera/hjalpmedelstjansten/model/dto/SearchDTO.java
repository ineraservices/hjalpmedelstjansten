package se.inera.hjalpmedelstjansten.model.dto;

import java.util.List;


public class SearchDTO {
    
    private Long count;
    private List<?> items;

    public SearchDTO(Long count, List<?> items) {
        this.count = count;
        this.items = items;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public List<?> getItems() {
        return items;
    }

    public void setItems(List<?> items) {
        this.items = items;
    }
    
    
}
