package se.inera.hjalpmedelstjansten.model.entity.cv;

import java.io.Serializable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;

/**
 * Unit for <code>Product</code> and <code>Article</code>
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = CVOrderUnit.FIND_ALL, query = "SELECT u FROM CVOrderUnit u ORDER BY u.displayOrder ASC"),
    @NamedQuery(name = CVOrderUnit.FIND_BY_CODE, query = "SELECT u FROM CVOrderUnit u WHERE u.code = :code"),
    @NamedQuery(name = CVOrderUnit.FIND_BY_NAME, query = "SELECT u FROM CVOrderUnit u WHERE u.name = :name")
})
public class CVOrderUnit implements Serializable {

    public static final String FIND_ALL = "HJMT_CV_ORDER_UNIT_FIND_ALL";
    public static final String FIND_BY_CODE = "HJMT_CV_ORDER_UNIT_FIND_BY_CODE";
    public static final String FIND_BY_NAME = "HJMT_CV_ORDER_UNIT_FIND_BY_NAME";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = true, unique = true)
    private String code;

    @Column(nullable = false)
    private int displayOrder;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

}
