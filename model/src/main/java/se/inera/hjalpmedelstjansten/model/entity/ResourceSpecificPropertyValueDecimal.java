package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

/**
 * Answer to decimal type.
 *
 */
@Entity
@DiscriminatorValue(value = "DECIMAL")
public class ResourceSpecificPropertyValueDecimal extends ResourceSpecificPropertyValue {

    @Column(nullable = true)
    private Double decimalValue;

    public Double getValue() {
        return decimalValue;
    }

    public void setValue(Double value) {
        this.decimalValue = value;
    }

}
