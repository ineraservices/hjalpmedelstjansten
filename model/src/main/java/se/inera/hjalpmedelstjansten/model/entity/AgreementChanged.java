package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * JPA Entity class for changed agreement
 *
 */
@Entity
public class AgreementChanged implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = false)
    private String agreementName;

    @Column(nullable = false)
    private String agreementNumber;

    @Column(nullable = false)
    private Long agreementId;

    @Column(nullable = false)
    private String customerOrganizationName;

    @Column(nullable = true)
    private String customerBusinessLevelName;

    @Column(nullable = true)
    private String changeString;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date whenChanged;

    public Long getUniqueId() {
        return uniqueId;
    }
    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getAgreementName() { return agreementName; }
    public void setAgreementName(String agreementName) {this.agreementName = agreementName;}

    public String getAgreementNumber() { return agreementNumber; }
    public void setAgreementNumber(String agreementNumber) {this.agreementNumber = agreementNumber;}

    public Long getAgreementId() {return agreementId; }
    public void setAgreementId(Long agreementId) {this.agreementId = agreementId; }

    public String getCustomerOrganizationName() { return customerOrganizationName; }
    public void setCustomerOrganizationName(String customerOrganizationName) {this.customerOrganizationName = customerOrganizationName;}

    public String getCustomerBusinessLevelName() { return customerBusinessLevelName; }
    public void setCustomerBusinessLevelName(String customerBusinessLevelName) {this.customerBusinessLevelName = customerBusinessLevelName;}

    public String getChanges() { return changeString; }
    public void setChanges(String changeString) {this.changeString = changeString;}

    public void setWhenChanged(Date date) {
        this.whenChanged = date;
    }
    public Date getWhenChanged() {
        return whenChanged;
    }
}
