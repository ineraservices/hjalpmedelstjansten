package se.inera.hjalpmedelstjansten.model.entity;

import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import java.io.Serializable;

@Entity
@Setter
@Getter
public class Notification implements Serializable {

    public enum NotificationType {

        ARTICLES_EXPIRED,
        ARTICLES_CHANGED,
        AGREEMENT_SHARED_CHANGED,
        AGREEMENT_NEW,
        AGREEMENT_CHANGED,
        GENERAL_PRICELIST_CHANGED,
        AGREEMENT_PRICELISTROW_APPROVE,
        AGREEMENT_PRICELISTROW_APPROVE_BASED_ON,
        AGREEMENT_NEW_PRICELIST,
        AGREEMENT_PRICELISTEROW_CUSTOMER_APPROVED,
        AGREEMENT_PRICELISTEROW_CUSTOMER_DECLINED,
        AGREEMENT_PRICELISTEROW_CUSTOMER_APPROVED_INACTIVATION,
        AGREEMENT_PRICELISTEROW_CUSTOMER_DECLINED_INACTIVATION;

    }

    private static final long serialVersionUID = -626353006554632656L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long uniqueId;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private NotificationType type;

    @Enumerated(EnumType.STRING)
    @Column(name = "recipient")
    private Organization.OrganizationType recipient;

    @Enumerated(EnumType.STRING)
    @Column(name = "permission")
    private UserRole.RoleName permission;

    @Column(name = "description")
    private String description;

}
