package se.inera.hjalpmedelstjansten.model.entity.media;

import jakarta.persistence.ManyToOne;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVDocumentType;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;

/**
 * Media of type document
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = MediaDocument.FIND_BY_PRODUCT, query = "SELECT m FROM MediaDocument m WHERE m.product.uniqueId = :productUniqueId ORDER BY m.created DESC")
})
public class MediaDocument extends Media {

    public static final String FIND_BY_PRODUCT = "HJMT_MEDIADOCUMENT_FIND_BY_PRODUCT";

    public enum FileType {
        PDF, WORD, EXCEL
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "documentTypeId")
    private CVDocumentType documentType;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private FileType fileType;

    @Column(nullable = false)
    private String fileName;

    @Column(nullable = false)
    private String externalKey;

    @Column(nullable = true)
    private String originalUrl;

    public CVDocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(CVDocumentType documentType) {
        this.documentType = documentType;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public String getExternalKey() {
        return externalKey;
    }

    public void setExternalKey(String externalKey) {
        this.externalKey = externalKey;
    }

}
