package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * JPA Entity class for ArticleChanged.
 *
 */
@Entity
public class ArticleChanged implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date dateChanged;

    @Column(nullable = false)
    private Long articleId;

    @Column(nullable = false)
    private String articleNumber;

    @Column
    private String newArticleName;

    @Column
    private String oldArticleName;

    @Column
    private String newOrderUnit;

    @Column
    private String oldOrderUnit;

    @Column
    private Long supplierId;

    @Column
    private String supplierName;

    @Column
    private String newCEmarked;

    @Column
    private String oldCEmarked;

    @Column
    private String newCVCEdirective;

    @Column
    private String oldCVCEdirective;

    @Column
    private String newCVCEstandard;

    @Column
    private String oldCVCEstandard;

    @Column
    private String oldIsoCode;

    @Column
    private String newIsoCode;

    @Column
    private String oldBasedOnProductNumber;

    @Column
    private String newBasedOnProductNumber;

    @Column
    private Long newReplacementDate;

    @Column
    private Date oldReplacementDate;

    @Column
    private String replacementArticles;





    public Long getUniqueId() {return uniqueId;}

    public void setUniqueId(Long uniqueId) {this.uniqueId = uniqueId;}

    public Date getDateChanged() {return dateChanged;}

    public void setDateChanged(Date dateChanged) {this.dateChanged = dateChanged;}

    public Long getArticleId() {return articleId;}

    public void setArticleId(Long articleId) {this.articleId = articleId;}

    public String getNewArticleName() {return newArticleName;}

    public void setNewArticleName(String newArticleName) {this.newArticleName = newArticleName;}

    public String getOldArticleName() {return oldArticleName;}

    public void setOldArticleName(String oldArticleName) {this.oldArticleName = oldArticleName;}

    public String getArticleNumber() {return articleNumber;}

    public void setArticleNumber(String articleNumber) {this.articleNumber = articleNumber;}

    public String getNewOrderUnit() {return newOrderUnit;}

    public void setNewOrderUnit(String newOrderUnit) {this.newOrderUnit = newOrderUnit;}

    public String getOldOrderUnit() {return oldOrderUnit;}

    public void setOldOrderUnit(String oldOrderUnit) {this.oldOrderUnit = oldOrderUnit;}

    public Long getSupplierId() {return supplierId;}

    public void setSupplierId(Long supplierId) {this.supplierId = supplierId;}

    public String getSupplierName() {return supplierName;}

    public void setSupplierName(String supplierName) {this.supplierName = supplierName;}

    public String getNewCEmarked() {return newCEmarked;}

    public void setNewCEmarked(String newCEmarked) {this.newCEmarked = newCEmarked;}

    public String getOldCEmarked() {return oldCEmarked;}

    public void setOldCEmarked(String oldCEmarked) {this.oldCEmarked = oldCEmarked;}

    public String getNewCVCEdirective() {return newCVCEdirective;}

    public void setNewCVCEdirective(String newCVCEdirective) {this.newCVCEdirective = newCVCEdirective;}

    public String getOldCVCEdirective() {return oldCVCEdirective;}

    public void setOldCVCEdirective(String oldCVCEdirective) {this.oldCVCEdirective = oldCVCEdirective;}

    public String getNewCVCEstandard() {return newCVCEstandard;}

    public void setNewCVCEstandard(String newCVCEstandard) {this.newCVCEstandard = newCVCEstandard;}

    public String getOldCVCEstandard() {return oldCVCEstandard;}

    public void setOldCVCEstandard(String oldCVCEstandard) {this.oldCVCEstandard = oldCVCEstandard;}

    public String getNewIsoCode() {return newIsoCode;}

    public void setNewIsoCode(String newIsoCode) {this.newIsoCode = newIsoCode;}

    public String getOldIsoCode() {return oldIsoCode;}

    public void setOldIsoCode(String oldIsoCode) {this.oldIsoCode = oldIsoCode;}

    public String getNewBasedOnProductNumber() {return newBasedOnProductNumber;}

    public void setNewBasedOnProductNumber(String newBasedOnProductNumber) {this.newBasedOnProductNumber = newBasedOnProductNumber;}

    public String getOldBasedOnProductNumber() {return oldBasedOnProductNumber;}

    public void setOldBasedOnProductNumber(String oldBasedOnProductNumber) {this.oldBasedOnProductNumber = oldBasedOnProductNumber;}

    public Long getNewReplacementDate() {return newReplacementDate;}

    public void setNewReplacementDate(Long newReplacementDate) {this.newReplacementDate = newReplacementDate;}

    public Date getOldReplacementDate() {return  oldReplacementDate;}

    public void setOldReplacementDate(Date oldReplacementDate) {this.oldReplacementDate = oldReplacementDate;}

    public String getReplacementArticles() {return replacementArticles;}

    public void setReplacementArticles(String replacementArticles) {this.replacementArticles = replacementArticles;}



}
