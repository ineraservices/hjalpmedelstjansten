package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.Date;

/**
 * JPA Entity class for Pricelist for agreements. Status is not saved per pricelist,
 * instead it is calculated when needed based on the validFrom, this in order to
 * avoid writing a batch job to update statuses daily.
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = AgreementPricelist.GET_ALL_BY_AGREEMENT, query = "SELECT p FROM AgreementPricelist p WHERE p.agreement.uniqueId = :agreementUniqueId ORDER BY p.validFrom ASC"),
    @NamedQuery(name = AgreementPricelist.FIND_BY_VALID_FROM_AND_AGREEMENT, query = "SELECT p FROM AgreementPricelist p WHERE p.agreement.uniqueId = :agreementUniqueId AND p.validFrom = :validFrom"),
    @NamedQuery(name = AgreementPricelist.FIND_BY_NUMBER_AND_AGREEMENT, query = "SELECT p FROM AgreementPricelist p WHERE p.agreement.uniqueId = :agreementUniqueId AND p.number = :number"),
    @NamedQuery(name = AgreementPricelist.FIND_BY_PASSED_VALID_FROM_AND_AGREEMENT, query = "SELECT p FROM AgreementPricelist p WHERE p.agreement.uniqueId = :agreementUniqueId AND p.validFrom <= :validFrom ORDER BY p.validFrom DESC"),
    @NamedQuery(name = AgreementPricelist.FIND_BY_NOT_PASSED_VALID_FROM_AND_AGREEMENT, query = "SELECT p FROM AgreementPricelist p WHERE p.agreement.uniqueId = :agreementUniqueId AND p.validFrom > :validFrom ORDER BY p.validFrom DESC"),
        @NamedQuery(name = AgreementPricelist.FIND_BY_UNIQUE_ID, query = "SELECT p FROM AgreementPricelist p WHERE p.uniqueId = :pricelistUniqueId")
})
@Table(
    uniqueConstraints = {
        @UniqueConstraint(
            columnNames = {"number", "agreementId"},
            name = "numberIsUniquePerAgreement"
        ),
        @UniqueConstraint(
            columnNames = {"validFrom", "agreementId"},
            name = "validFromIsUniquePerAgreement"
        )
    }
)
public class AgreementPricelist implements Serializable {

    public static final String GET_ALL_BY_AGREEMENT = "HJMT_PRICELIST_GET_ALL_BY_AGREEMENT";
    public static final String FIND_BY_VALID_FROM_AND_AGREEMENT = "HJMT_PRICELIST_FIND_BY_VALID_FROM_AND_AGREEMENT";
    public static final String FIND_BY_NUMBER_AND_AGREEMENT = "HJMT_PRICELIST_FIND_BY_NUMBER_AND_AGREEMENT";
    public static final String FIND_BY_PASSED_VALID_FROM_AND_AGREEMENT = "HJMT_PRICELIST_FIND_BY_PASSED_VALID_FROM_AND_AGREEMENT";
    public static final String FIND_BY_NOT_PASSED_VALID_FROM_AND_AGREEMENT = "HJMT_PRICELIST_FIND_BY_NOT_PASSED_VALID_FROM_AND_AGREEMENT";
    public static final String FIND_BY_UNIQUE_ID = "HJMT-PRICELIST_FIND_BY_UNIQUE_ID";

    public enum Status {
        PAST, CURRENT, FUTURE
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = false)
    private String number;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agreementId", nullable = false)
    private Agreement agreement;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date validFrom;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date lastUpdated;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Agreement getAgreement() {
        return agreement;
    }

    public void setAgreement(Agreement agreement) {
        this.agreement = agreement;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @PrePersist
    private void prePersist() {
        this.created = new Date();
    }

    @PreUpdate
    private void preUpdate() {
        this.lastUpdated = new Date();
    }

}
