package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;


@Entity
public class InternalAudit implements Serializable {

    public enum EntityType {
        ORGANIZATION,
        USER,
        PRODUCT,
        ARTICLE,
        AGREEMENT,
        AGREEMENT_PRICELIST,
        AGREEMENT_PRICELIST_ROW,
        GENERAL_PRICELIST,
        GENERAL_PRICELIST_PRICELIST,
        GENERAL_PRICELIST_PRICELIST_ROW,
        ASSORTMENT,
        MEDIA,
        EXPORTFILE
    }

    public enum ActionType {
        CREATE, UPDATE, DELETE, LOGIN, LOGOUT, VIEW
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "InternalAuditIdSequence" )
    @SequenceGenerator(name = "InternalAuditIdSequence", sequenceName = "InternalAuditIdSequence")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date auditTime;

    @Enumerated(EnumType.STRING)
    private EntityType entityType;

    @Enumerated(EnumType.STRING)
    private ActionType actionType;

    @Column(nullable = true)
    private Long userEngagementId;

    @Column(nullable = true)
    private Long entityId;

    @Column(nullable = true)
    private String sessionId;

    @Column(nullable = true)
    private String requestIp;

    public Long getId() {
        return id;
    }

    public Date getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public Long getUserEngagementId() {
        return userEngagementId;
    }

    public void setUserEngagementId(Long userEngagementId) {
        this.userEngagementId = userEngagementId;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getRequestIp() {
        return requestIp;
    }

    public void setRequestIp(String requestIp) {
        this.requestIp = requestIp;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

}
