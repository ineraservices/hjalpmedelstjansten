package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;

/**
 * Answer to list of single type.
 *
 */
@Entity
@DiscriminatorValue(value = "LISTSINGLE")
public class ResourceSpecificPropertyValueValueListSingle extends ResourceSpecificPropertyValue {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "singleCategorySpecificPropertyListValueId", nullable = true)
    private CategorySpecificPropertyListValue value;

    public CategorySpecificPropertyListValue getValue() {
        return value;
    }

    public void setValue(CategorySpecificPropertyListValue value) {
        this.value = value;
    }

}
