package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

/**
 * Answer to textfield
 *
 */
@Entity
@DiscriminatorValue(value = "TEXTFIELD")
public class ResourceSpecificPropertyValueTextField extends ResourceSpecificPropertyValue {

    @Column(nullable = true)
    private String textValue;

    public String getValue() {
        return textValue;
    }

    public void setValue(String value) {
        this.textValue = value;
    }

}
