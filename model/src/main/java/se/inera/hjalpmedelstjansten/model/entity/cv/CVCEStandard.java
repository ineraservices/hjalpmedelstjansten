package se.inera.hjalpmedelstjansten.model.entity.cv;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import java.io.Serializable;

/**
 * JPA Entity class for CE Standard
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = CVCEStandard.FIND_ALL, query = "SELECT c FROM CVCEStandard c ORDER BY c.displayOrder ASC"),
    @NamedQuery(name = CVCEStandard.FIND_BY_CODE, query = "SELECT c FROM CVCEStandard c WHERE c.code = :code"),
    @NamedQuery(name = CVCEStandard.FIND_BY_NAME, query = "SELECT c FROM CVCEStandard c WHERE c.name = :name")
})
public class CVCEStandard implements Serializable {

    public static final String FIND_ALL = "HJMT_CVCESTANDARD_FIND_ALL";
    public static final String FIND_BY_CODE = "HJMT_CVCESTANDARD_FIND_BY_CODE";
    public static final String FIND_BY_NAME = "HJMT_CVCESTANDARD_FIND_BY_NAME";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false, unique = true)
    private String code;

    @Column(nullable = false)
    private int displayOrder;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

}
