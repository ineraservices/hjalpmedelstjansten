package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;
import java.io.Serializable;

/**
 * List value of a <code>CategorySpecificProperty</code>
 *
 */

@Entity
@NamedQueries({
        @NamedQuery(name = CategorySpecificPropertyListValue.FIND_BY_PROPERTYID, query = "SELECT p FROM CategorySpecificPropertyListValue p WHERE p.categorySpecificProperty.uniqueId = :propertyId ORDER BY p.value ASC")
})
public class CategorySpecificPropertyListValue implements Serializable {

    public static final String FIND_BY_PROPERTYID = "HJMT_CATEGORY_SPECIFIC_PROPERTYLISTVALUE_FIND_BY_PROPERTYID";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = false)
    private String value;

    @Column(nullable = false)
    private String code;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "categorySpecificPropertyId", nullable = false)
    private CategorySpecificProperty categorySpecificProperty;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CategorySpecificProperty getCategorySpecificProperty() {
        return categorySpecificProperty;
    }

    public void setCategorySpecificProperty(CategorySpecificProperty categorySpecificProperty) {
        this.categorySpecificProperty = categorySpecificProperty;
    }

}
