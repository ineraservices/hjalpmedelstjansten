package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;
import java.io.Serializable;

/**
 * Answer parent class. Use of single table inheritance for performance reasons.
 *
 */
@Entity

@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "propertyType")
@NamedQueries({
        @NamedQuery(name = ResourceSpecificPropertyValue.FIND_RESOURCE_BY_PROPERTYID, query = "SELECT c FROM ResourceSpecificPropertyValue c WHERE c.categorySpecificProperty.uniqueId = :propertyId"),
        @NamedQuery(name = ResourceSpecificPropertyValue.FIND_RESOURCES_BY_ARTICLEID, query = "SELECT c FROM ResourceSpecificPropertyValue c WHERE c.article.uniqueId = :articleId"),
        @NamedQuery(name = ResourceSpecificPropertyValue.FIND_RESOURCES_BY_PRODUCTID, query = "SELECT c FROM ResourceSpecificPropertyValue c WHERE c.product.uniqueId = :productId")
})
public abstract class ResourceSpecificPropertyValue implements Serializable {



    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "categorySpecificPropertyId", nullable = false)
    private CategorySpecificProperty categorySpecificProperty;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "productId", nullable = true)
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "articleId", nullable = true)
    private Article article;



    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public CategorySpecificProperty getCategorySpecificProperty() {
        return categorySpecificProperty;
    }

    public void setCategorySpecificProperty(CategorySpecificProperty categorySpecificProperty) {
        this.categorySpecificProperty = categorySpecificProperty;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public static final String FIND_RESOURCE_BY_PROPERTYID = "HJMT_CATEGORY_SPECIFIC_RESOURCEVALUE_FIND_BY_PROPERTYID";
    public static final String FIND_RESOURCES_BY_ARTICLEID = "HJMT_CATEGORY_SPECIFIC_RESOURCEVALUE_FIND_BY_ARTICLEID";
    public static final String FIND_RESOURCES_BY_PRODUCTID = "HJMT_CATEGORY_SPECIFIC_RESOURCEVALUE_FIND_BY_PRODUCTID";

}
