package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;
import java.io.Serializable;

/**
 * Category of <code>Product</code> and <code>Category</code>
 *
 */
@Entity
@NamedQueries({
        @NamedQuery(name = Category.FIND_LEAF_NODES, query = "SELECT c FROM Category c WHERE c.articleType IS NOT NULL ORDER BY c.code ASC"),
        @NamedQuery(name = Category.FIND_BY_CODE, query = "SELECT c FROM Category c WHERE c.code = :code"),
        @NamedQuery(name = Category.FIND_BY_CODE_AND_LEAFNODE, query = "SELECT c FROM Category c WHERE c.code = :code AND c.articleType IS NOT NULL ORDER BY c.code ASC"),
        @NamedQuery(name = Category.FIND_BY_ID_AND_LEAFNODE, query = "SELECT c FROM Category c WHERE c.uniqueId = :uniqueId AND c.articleType IS NOT NULL"),
        @NamedQuery(name = Category.FIND_FIRST_LEVEL_CATEGORIES, query = "SELECT c FROM Category c WHERE c.parent IS NULL ORDER BY c.code ASC"),
        @NamedQuery(name = Category.SEARCH_CATEGORIES, query = "SELECT c FROM Category c WHERE (c.name LIKE :query OR c.code LIKE :query) AND c.articleType IS NOT NULL ORDER BY c.code ASC"),
        @NamedQuery(name = Category.SEARCH_CATEGORIES_BY_PARENT, query = "SELECT c FROM Category c WHERE c.parent.uniqueId = :uniqueId ORDER BY c.code ASC"),
        @NamedQuery(name = Category.FIND_BY_ARTICLE_TYPE_EMPTY_CODE, query = "SELECT c FROM Category c WHERE c.articleType = :articleType AND c.code IS NULL"),
        @NamedQuery(name = Category.FIND_BY_UNIQUEID, query = "SELECT c FROM Category c WHERE c.uniqueId = :uniqueId"),
        //@NamedQuery(name = Category.FIND_ALL_CATEGORIES, query = "SELECT c FROM Category c WHERE c.code IS NOT NULL ORDER BY c.code"), //this should fix the order in dropdowns
        @NamedQuery(name = Category.FIND_ALL_CATEGORIES, query = "SELECT c FROM Category c WHERE c.code IS NOT NULL"),
        @NamedQuery(name = Category.FIND_ARTICLE_COUNT, query = "SELECT COUNT(a) FROM Article a WHERE a.category.uniqueId = :uniqueId" ),
        @NamedQuery(name = Category.FIND_PRODUCT_COUNT, query = "SELECT COUNT(p) FROM Product p WHERE p.category.uniqueId = :uniqueId" ),


})
public class Category implements Serializable {

    public static final String FIND_LEAF_NODES = "HJMT_CATEGORY_FIND_ALL";
    public static final String FIND_BY_CODE = "HJMT_CATEGORY_FIND_BY_CODE";
    public static final String FIND_FIRST_LEVEL_CATEGORIES = "HJMT_CATEGORY_FIND_FIRST_LEVEL_CATEGORIES";
    public static final String SEARCH_CATEGORIES = "HJMT_CATEGORY_SEARCH_CATEGORIES";
    public static final String SEARCH_CATEGORIES_BY_PARENT = "HJMT_CATEGORY_SEARCH_CATEGORIES_BY_CODE";
    public static final String FIND_BY_ID_AND_LEAFNODE = "HJMT_CATEGORY_FIND_BY_ID_CHILDLESS";
    public static final String FIND_BY_CODE_AND_LEAFNODE = "HJMT_CATEGORY_FIND_BY_CODE_AND_LEAFNODE";
    public static final String FIND_BY_ARTICLE_TYPE_EMPTY_CODE = "HJMT_CATEGORY_FIND_BY_ARTICLE_TYPE_EMPTY_CODE";

    public static final String FIND_BY_UNIQUEID = "FIND_BY_UNIQUEID";

    public static final String FIND_ALL_CATEGORIES = "FIND_ALL_CATEGORIES";

    public static final String FIND_ARTICLE_COUNT = "FIND_ARTICLE_COUNT";
    public static final String FIND_PRODUCT_COUNT = "FIND_PRODUCT_COUNT";



    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = false)
    private String name;

    @Column(nullable = true, unique = true)
    private String code;

    @Column(nullable = true, length = 1000)
    private String description;

    @Column(nullable = true)
    @Enumerated(EnumType.STRING)
    private Article.Type articleType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parentCategoryId", nullable = true)
    private Category parent;


    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Article.Type getArticleType() {
        return articleType;
    }

    public void setArticleType(Article.Type articleType) {
        this.articleType = articleType;
    }

    public Category getParent() {
        return parent;
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



}
