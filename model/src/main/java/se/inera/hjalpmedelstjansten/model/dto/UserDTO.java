package se.inera.hjalpmedelstjansten.model.dto;

import lombok.Data;
import se.inera.hjalpmedelstjansten.model.entity.ElectronicAddress;
import se.inera.hjalpmedelstjansten.model.entity.UserAccount;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;

import java.util.Date;
import java.util.List;

@Data
public class UserDTO {

    private Long uniqueId;
    private UserAccount user;
    private String firstName;
    private String lastName;
    private String title;
    private String username;
    private String password;
    private String token;
    private String salt;
    private int iterations;
    private ElectronicAddress electronicAddress;
    private UserAccount.LoginType loginType;
    private List<UserEngagement> userEngagements;
    private boolean hasLoggedIn;
    private boolean previousLoginFound;
    private Date created;
    private Date lastUpdated;
    private String loginTypeString;
    private String loginUrl;
    private boolean active;

}
