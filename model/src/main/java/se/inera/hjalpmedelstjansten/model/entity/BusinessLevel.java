package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import java.io.Serializable;


@Entity
@NamedQueries({
        @NamedQuery(name = BusinessLevel.FIND_BY_ORGANIZATION_AND_STATUSES, query = "SELECT b FROM BusinessLevel b WHERE b.organization.uniqueId = :organizationUniqueId AND b.status IN :statuses ORDER BY b.name ASC")
})
public class BusinessLevel implements Serializable {

    public enum Status {
        ACTIVE, INACTIVE
    }

    public static final String FIND_BY_ORGANIZATION_AND_STATUSES = "HJMT_BUSINESSLEVEL_FIND_BY_ORGANIZATION_AND_STATUSES";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Status status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organizationId", nullable = false)
    private Organization organization;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
