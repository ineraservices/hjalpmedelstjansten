package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.ManyToOne;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * JPA Entity class for approved agreement pricelist row
 *
 */
@Entity
@NamedQueries({
        @NamedQuery(name = ApprovedAgreementPricelistRow.FIND_BY_USERID, query = "SELECT ar FROM ApprovedAgreementPricelistRow ar WHERE ar.approvedBy = :approvedBy"),
})
public class ApprovedAgreementPricelistRow implements Serializable {
    public static final String FIND_BY_USERID = "HJMT_APPROVEDAGREEMENTPRICELISTROW_FIND_BY_USERID";

    @Id
    @Column(nullable = false)
    private Long uniqueId;

    @Column(nullable = true)
    private BigDecimal price;

    @Temporal(TemporalType.DATE)
    @Column(nullable = true)
    private Date validFrom;

    @Column(nullable = false)
    private int leastOrderQuantity;

    @Column(nullable = true)
    private Integer deliveryTime;

    @Column(nullable = true)
    private Integer warrantyQuantity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyQuantityUnitId", nullable = true)
    private CVGuaranteeUnit warrantyQuantityUnit;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyValidFromId", nullable = true)
    private CVPreventiveMaintenance warrantyValidFrom;

    @Column(nullable = true)
    private String warrantyTerms;

    @Column(nullable = false)
    private String approvedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date approvedDate;

    @Column(nullable = true)
    private Long approvementId;

    @Column()
    private Long categoryId;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }


    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }


    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }


    public int getLeastOrderQuantity() {
        return leastOrderQuantity;
    }

    public void setLeastOrderQuantity(int leastOrderQuantity) {
        this.leastOrderQuantity = leastOrderQuantity;
    }


    public Integer getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Integer deliveryTime) {
        this.deliveryTime = deliveryTime;
    }


    public Integer getWarrantyQuantity() {
        return warrantyQuantity;
    }

    public void setWarrantyQuantity(Integer warrantyQuantity) {
        this.warrantyQuantity = warrantyQuantity;
    }


    public CVGuaranteeUnit getWarrantyQuantityUnit() {
        return warrantyQuantityUnit;
    }

    public void setWarrantyQuantityUnit(CVGuaranteeUnit warrantyQuantityUnit) {
        this.warrantyQuantityUnit = warrantyQuantityUnit;
    }


    public CVPreventiveMaintenance getWarrantyValidFrom() {
        return warrantyValidFrom;
    }

    public void setWarrantyValidFrom(CVPreventiveMaintenance warrantyValidFrom) {
        this.warrantyValidFrom = warrantyValidFrom;
    }


    public String getWarrantyTerms() {
        return warrantyTerms;
    }

    public void setWarrantyTerms(String warrantyTerms) {
        this.warrantyTerms = warrantyTerms;
    }


    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public Date getApprovedDate() {return approvedDate;}

    @PrePersist
    private void prePersist() {
        this.approvedDate = new Date();
    }

    @PreUpdate
    private void preUpdate() {
        this.approvedDate = new Date();
    }

    public Long getApprovementId() {
        return approvementId;
    }
    public void setApprovementId(Long approvementId) {
        this.approvementId = approvementId;
    }

    public Long getCategoryId() {
        return categoryId;
    }
    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

}
