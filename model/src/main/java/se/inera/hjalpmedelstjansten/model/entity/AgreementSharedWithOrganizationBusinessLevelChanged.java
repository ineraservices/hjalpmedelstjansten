package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import java.io.Serializable;

/**
 * JPA Entity class for changed agreement
 *
 */
@Entity
public class AgreementSharedWithOrganizationBusinessLevelChanged implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = false)
    private Long changeId;

    @Column(nullable = false)
    private Long agreementId;

    @Column()
    private Long oldBusinessLevelId;

    @Column()
    private Long newBusinessLevelId;

    public Long getUniqueId() {
        return uniqueId;
    }
    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Long getChangeId() {
        return changeId;
    }
    public void setChangeId(Long changeId) {
        this.changeId = changeId;
    }

    public Long getAgreementId() {
        return agreementId;
    }
    public void setAgreementId(Long agreementId) {
        this.agreementId = agreementId;
    }

    public Long getOldBusinessLevelId() {
        return oldBusinessLevelId;
    }
    public void setOldBusinessLevelId(Long oldBusinessLevelId) {
        this.oldBusinessLevelId = oldBusinessLevelId;
    }

    public Long getNewBusinessLevelId() {
        return newBusinessLevelId;
    }
    public void setNewBusinessLevelId(Long newBusinessLevelId) {
        this.newBusinessLevelId = newBusinessLevelId;
    }
}
