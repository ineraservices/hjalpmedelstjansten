package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.ManyToOne;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * JPA Entity class for Agreement
 *
 */
@Entity
@NamedQueries({
        @NamedQuery(name = Agreement.FIND_TO_DISCONTINUE, query = "SELECT a FROM Agreement a WHERE a.status != 'DISCONTINUED' AND (a.validTo < :date OR a.endDate < :date)"),
        @NamedQuery(name = Agreement.FIND_TO_ACTIVATE, query = "SELECT a FROM Agreement a WHERE a.status != 'CURRENT' AND (a.validFrom < :date AND a.validTo > :date)"),
        @NamedQuery(name = Agreement.FIND_BY_CUSTOMER_AND_STATUS, query = "SELECT a FROM Agreement a JOIN a.sharedWithCustomers swc WHERE (a.customer.uniqueId = :customerUniqueId OR swc.uniqueId = :customerUniqueId) AND a.status = :status"),
        @NamedQuery(name = Agreement.FIND_BY_SHARED_WITH_CUSTOMER_AND_STATUS, query = "SELECT a FROM Agreement a JOIN a.sharedWithCustomers swc WHERE swc.uniqueId = :customerUniqueId AND a.status IN :status"),
        @NamedQuery(name = Agreement.FIND_BY_BUSINESS_LEVEL, query = "SELECT a FROM Agreement a JOIN a.sharedWithCustomerBusinessLevels swcbl WHERE a.customerBusinessLevel.uniqueId = :businessLevelUniqueId OR swcbl.uniqueId = :businessLevelUniqueId"),
        @NamedQuery(name = Agreement.FIND_BY_USER_ACCOUNT, query = "SELECT a FROM Agreement a JOIN a.customerPricelistApprovers cpa WHERE cpa.userAccount = :userAccount")
})
public class Agreement implements Serializable {

    public static final String FIND_TO_DISCONTINUE = "HJMT_AGREEMENT_FIND_TO_DISCONTINUE";
    public static final String FIND_TO_ACTIVATE = "HJMT_AGREEMENT_FIND_TO_ACTIVATE";
    public static final String FIND_BY_CUSTOMER_AND_STATUS = "HJMT_AGREEMENT_FIND_BY_CUSTOMER_AND_STATUS";
    public static final String FIND_BY_SHARED_WITH_CUSTOMER_AND_STATUS = "HJMT_AGREEMENT_FIND_BY_SHARED_WITH_CUSTOMER_AND_STATUS";
    public static final String FIND_BY_BUSINESS_LEVEL = "HJMT_AGREEMENT_FIND_BY_BUSINESS_LEVEL";
    public static final String FIND_BY_USER_ACCOUNT = "HJMT_AGREEMENT_FIND_BY_USER_ACCOUNT";

    public enum Status {
        FUTURE, CURRENT, DISCONTINUED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = true)
    private String agreementName;

    @Column(nullable = false)
    private String agreementNumber;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "supplierOrganizationId", nullable = false)
    private Organization supplier;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name="AgreementUserEngagementCustomer",
            joinColumns={@JoinColumn(name="agreementId", referencedColumnName="uniqueId")},
            inverseJoinColumns={@JoinColumn(name="userEngagementId", referencedColumnName="uniqueId")})
    private List<UserEngagement> customerPricelistApprovers;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name="AgreementSharedWithOrganization",
            joinColumns={@JoinColumn(name="agreementId", referencedColumnName="uniqueId")},
            inverseJoinColumns={@JoinColumn(name="organizationId", referencedColumnName="uniqueId")})
    private List<Organization> sharedWithCustomers;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name="AgreementSharedWithOrganizationBusinessLevel",
            joinColumns={@JoinColumn(name="agreementId", referencedColumnName="uniqueId")},
            inverseJoinColumns={@JoinColumn(name="businessLevelId", referencedColumnName="uniqueId")})
    private List<BusinessLevel> sharedWithCustomerBusinessLevels;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customerOrganizationId", nullable = false)
    private Organization customer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customerBusinessLevelId", nullable = true)
    private BusinessLevel customerBusinessLevel;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date validFrom;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date validTo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date extensionOptionTo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date sendReminderOn;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date endDate;

    @Column(nullable = true)
    private Integer deliveryTimeH;

    @Column(nullable = true)
    private Integer deliveryTimeT;

    @Column(nullable = true)
    private Integer deliveryTimeI;

    @Column(nullable = true)
    private Integer deliveryTimeR;

    @Column(nullable = true)
    private Integer deliveryTimeTJ;

    @Column(nullable = true)
    private Integer warrantyQuantityH;

    @Column(nullable = true)
    private Integer warrantyQuantityT;

    @Column(nullable = true)
    private Integer warrantyQuantityI;

    @Column(nullable = true)
    private Integer warrantyQuantityR;

    @Column(nullable = true)
    private Integer warrantyQuantityTJ;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyQuantityHUnitId", nullable = true)
    private CVGuaranteeUnit warrantyQuantityHUnit;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyQuantityTUnitId", nullable = true)
    private CVGuaranteeUnit warrantyQuantityTUnit;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyQuantityIUnitId", nullable = true)
    private CVGuaranteeUnit warrantyQuantityIUnit;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyQuantityRUnitId", nullable = true)
    private CVGuaranteeUnit warrantyQuantityRUnit;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyQuantityTJUnitId", nullable = true)
    private CVGuaranteeUnit warrantyQuantityTJUnit;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyValidFromHId", nullable = true)
    private CVPreventiveMaintenance warrantyValidFromH;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyValidFromTId", nullable = true)
    private CVPreventiveMaintenance warrantyValidFromT;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyValidFromIId", nullable = true)
    private CVPreventiveMaintenance warrantyValidFromI;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyValidFromRId", nullable = true)
    private CVPreventiveMaintenance warrantyValidFromR;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyValidFromTJId", nullable = true)
    private CVPreventiveMaintenance warrantyValidFromTJ;

    @Column(nullable = true)
    private String warrantyTermsH;

    @Column(nullable = true)
    private String warrantyTermsT;

    @Column(nullable = true)
    private String warrantyTermsI;

    @Column(nullable = true)
    private String warrantyTermsR;

    @Column(nullable = true)
    private String warrantyTermsTJ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date lastUpdated;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getAgreementName() {
        return agreementName;
    }

    public void setAgreementName(String agreementName) {
        this.agreementName = agreementName;
    }

    public String getAgreementNumber() {
        return agreementNumber;
    }

    public void setAgreementNumber(String agreementNumber) {
        this.agreementNumber = agreementNumber;
    }

    public Organization getSupplier() {
        return supplier;
    }

    public void setSupplier(Organization supplier) {
        this.supplier = supplier;
    }

    public Organization getCustomer() {
        return customer;
    }

    public void setCustomer(Organization customer) {
        this.customer = customer;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public Date getExtensionOptionTo() {
        return extensionOptionTo;
    }

    public void setExtensionOptionTo(Date extensionOptionTo) {
        this.extensionOptionTo = extensionOptionTo;
    }

    public Date getSendReminderOn() {
        return sendReminderOn;
    }

    public void setSendReminderOn(Date sendReminderOn) {
        this.sendReminderOn = sendReminderOn;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BusinessLevel getCustomerBusinessLevel() {
        return customerBusinessLevel;
    }

    public void setCustomerBusinessLevel(BusinessLevel customerBusinessLevel) {
        this.customerBusinessLevel = customerBusinessLevel;
    }

    public List<UserEngagement> getCustomerPricelistApprovers() {
        return customerPricelistApprovers;
    }

    public void setCustomerPricelistApprovers(List<UserEngagement> customerPricelistApprovers) {
        this.customerPricelistApprovers = customerPricelistApprovers;
    }

    public List<Organization> getSharedWithCustomers() {
        return sharedWithCustomers;
    }

    public void setSharedWithCustomers(List<Organization> sharedWithCustomers) {
        this.sharedWithCustomers = sharedWithCustomers;
    }

    public Integer getDeliveryTimeH() {
        return deliveryTimeH;
    }

    public void setDeliveryTimeH(Integer deliveryTimeH) {
        this.deliveryTimeH = deliveryTimeH;
    }

    public Integer getDeliveryTimeT() {
        return deliveryTimeT;
    }

    public void setDeliveryTimeT(Integer deliveryTimeT) {
        this.deliveryTimeT = deliveryTimeT;
    }

    public Integer getDeliveryTimeI() {
        return deliveryTimeI;
    }

    public void setDeliveryTimeI(Integer deliveryTimeI) {
        this.deliveryTimeI = deliveryTimeI;
    }

    public Integer getDeliveryTimeR() {
        return deliveryTimeR;
    }

    public void setDeliveryTimeR(Integer deliveryTimeR) {
        this.deliveryTimeR = deliveryTimeR;
    }

    public Integer getDeliveryTimeTJ() {
        return deliveryTimeTJ;
    }

    public void setDeliveryTimeTJ(Integer deliveryTimeTJ) {
        this.deliveryTimeTJ = deliveryTimeTJ;
    }

    public Integer getWarrantyQuantityH() {
        return warrantyQuantityH;
    }

    public void setWarrantyQuantityH(Integer warrantyQuantityH) {
        this.warrantyQuantityH = warrantyQuantityH;
    }

    public Integer getWarrantyQuantityT() {
        return warrantyQuantityT;
    }

    public void setWarrantyQuantityT(Integer warrantyQuantityT) {
        this.warrantyQuantityT = warrantyQuantityT;
    }

    public Integer getWarrantyQuantityI() {
        return warrantyQuantityI;
    }

    public void setWarrantyQuantityI(Integer warrantyQuantityI) {
        this.warrantyQuantityI = warrantyQuantityI;
    }

    public Integer getWarrantyQuantityR() {
        return warrantyQuantityR;
    }

    public void setWarrantyQuantityR(Integer warrantyQuantityR) {
        this.warrantyQuantityR = warrantyQuantityR;
    }

    public Integer getWarrantyQuantityTJ() {
        return warrantyQuantityTJ;
    }

    public void setWarrantyQuantityTJ(Integer warrantyQuantityTJ) {
        this.warrantyQuantityTJ = warrantyQuantityTJ;
    }

    public CVGuaranteeUnit getWarrantyQuantityHUnit() {
        return warrantyQuantityHUnit;
    }

    public void setWarrantyQuantityHUnit(CVGuaranteeUnit warrantyQuantityHUnit) {
        this.warrantyQuantityHUnit = warrantyQuantityHUnit;
    }

    public CVGuaranteeUnit getWarrantyQuantityTUnit() {
        return warrantyQuantityTUnit;
    }

    public void setWarrantyQuantityTUnit(CVGuaranteeUnit warrantyQuantityTUnit) {
        this.warrantyQuantityTUnit = warrantyQuantityTUnit;
    }

    public CVGuaranteeUnit getWarrantyQuantityIUnit() {
        return warrantyQuantityIUnit;
    }

    public void setWarrantyQuantityIUnit(CVGuaranteeUnit warrantyQuantityIUnit) {
        this.warrantyQuantityIUnit = warrantyQuantityIUnit;
    }

    public CVGuaranteeUnit getWarrantyQuantityRUnit() {
        return warrantyQuantityRUnit;
    }

    public void setWarrantyQuantityRUnit(CVGuaranteeUnit warrantyQuantityRUnit) {
        this.warrantyQuantityRUnit = warrantyQuantityRUnit;
    }

    public CVGuaranteeUnit getWarrantyQuantityTJUnit() {
        return warrantyQuantityTJUnit;
    }

    public void setWarrantyQuantityTJUnit(CVGuaranteeUnit warrantyQuantityTJUnit) {
        this.warrantyQuantityTJUnit = warrantyQuantityTJUnit;
    }

    public CVPreventiveMaintenance getWarrantyValidFromH() {
        return warrantyValidFromH;
    }

    public void setWarrantyValidFromH(CVPreventiveMaintenance warrantyValidFromH) {
        this.warrantyValidFromH = warrantyValidFromH;
    }

    public CVPreventiveMaintenance getWarrantyValidFromT() {
        return warrantyValidFromT;
    }

    public void setWarrantyValidFromT(CVPreventiveMaintenance warrantyValidFromT) {
        this.warrantyValidFromT = warrantyValidFromT;
    }

    public CVPreventiveMaintenance getWarrantyValidFromI() {
        return warrantyValidFromI;
    }

    public void setWarrantyValidFromI(CVPreventiveMaintenance warrantyValidFromI) {
        this.warrantyValidFromI = warrantyValidFromI;
    }

    public CVPreventiveMaintenance getWarrantyValidFromR() {
        return warrantyValidFromR;
    }

    public void setWarrantyValidFromR(CVPreventiveMaintenance warrantyValidFromR) {
        this.warrantyValidFromR = warrantyValidFromR;
    }

    public CVPreventiveMaintenance getWarrantyValidFromTJ() {
        return warrantyValidFromTJ;
    }

    public void setWarrantyValidFromTJ(CVPreventiveMaintenance warrantyValidFromTJ) {
        this.warrantyValidFromTJ = warrantyValidFromTJ;
    }

    public String getWarrantyTermsH() {
        return warrantyTermsH;
    }

    public void setWarrantyTermsH(String warrantyTermsH) {
        this.warrantyTermsH = warrantyTermsH;
    }

    public String getWarrantyTermsT() {
        return warrantyTermsT;
    }

    public void setWarrantyTermsT(String warrantyTermsT) {
        this.warrantyTermsT = warrantyTermsT;
    }

    public String getWarrantyTermsI() {
        return warrantyTermsI;
    }

    public void setWarrantyTermsI(String warrantyTermsI) {
        this.warrantyTermsI = warrantyTermsI;
    }

    public String getWarrantyTermsR() {
        return warrantyTermsR;
    }

    public void setWarrantyTermsR(String warrantyTermsR) {
        this.warrantyTermsR = warrantyTermsR;
    }

    public String getWarrantyTermsTJ() {
        return warrantyTermsTJ;
    }

    public void setWarrantyTermsTJ(String warrantyTermsTJ) {
        this.warrantyTermsTJ = warrantyTermsTJ;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<BusinessLevel> getSharedWithCustomerBusinessLevels() {
        return sharedWithCustomerBusinessLevels;
    }

    public void setSharedWithCustomerBusinessLevels(List<BusinessLevel> sharedWithCustomerBusinessLevels) {
        this.sharedWithCustomerBusinessLevels = sharedWithCustomerBusinessLevels;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @PrePersist
    private void prePersist() {
        this.created = new Date();
    }

    @PreUpdate
    private void preUpdate() {
        this.lastUpdated = new Date();
    }

}
