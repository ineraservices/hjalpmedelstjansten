package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import java.io.Serializable;

@Entity
public class AgreementPricelistRowApprovement implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = true)
    private String comment;

    public Long getUniqueId() {
        return uniqueId;
    }
    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
}
