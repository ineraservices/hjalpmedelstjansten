package se.inera.hjalpmedelstjansten.model.entity.cv;

import jakarta.persistence.*;
import java.io.Serializable;

/**
 * JPA Entity class for Municipalities
 *
 */
@Entity
public class CVMunicipality implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false, unique = true)
    private String code;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "countyId", nullable = false)
    private CVCounty county;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CVCounty getCounty() {
        return county;
    }

    public void setCounty(CVCounty county) {
        this.county = county;
    }

}
