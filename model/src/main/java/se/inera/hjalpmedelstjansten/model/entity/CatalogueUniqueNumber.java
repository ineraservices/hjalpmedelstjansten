package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import java.io.Serializable;

/**
 * JPA Entity class for Product
 *
 */
@Entity
public class CatalogueUniqueNumber implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CatalogueUniqueNumberIdSequence")
    @SequenceGenerator(name = "CatalogueUniqueNumberIdSequence", sequenceName = "CatalogueUniqueNumberIdSequence")
    private Long uniqueId;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

}
