package se.inera.hjalpmedelstjansten.model.dto;

import lombok.Data;
import se.inera.hjalpmedelstjansten.model.entity.Notification;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.UserAccount;
import se.inera.hjalpmedelstjansten.model.entity.UserNotification;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
public class NotificationDTO {


    private long userId;
    private long organizationId;
    private Organization.OrganizationType organizationType;

    private UserAccount user;
    private UserNotification userNotification;

    private Set<String> userNotificationsStringSet = new HashSet<>();
    private Map<String, String> allNotificationsMap = new HashMap<>();
    private Set<Notification> userNotificationTypeSet = new HashSet<>();
    private Set<Notification> allNotificationTypeSet = new HashSet<>();

    private List<UserRole> userRoles = new ArrayList<>();
    private Map<String, String> error = new HashMap<>();

}
