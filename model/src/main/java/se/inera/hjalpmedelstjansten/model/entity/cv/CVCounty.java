package se.inera.hjalpmedelstjansten.model.entity.cv;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * JPA Entity class for Counties
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = CVCounty.FIND_ALL, query = "SELECT c FROM CVCounty c ORDER BY c.name ASC")
})
public class CVCounty implements Serializable {

    public static final String FIND_ALL = "HJMT_CVCOUNTY_FIND_ALL";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false, unique = true)
    private String code;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean showMunicipalities;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "county")
    @OrderBy("name ASC")
    private List<CVMunicipality> municipalities;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean getShowMunicipalities() {
        return showMunicipalities;
    }

    public void setShowMunicipalities(boolean showMunicipalities) {
        this.showMunicipalities = showMunicipalities;
    }

    public List<CVMunicipality> getMunicipalities() {
        return municipalities;
    }

    public void setMunicipalities(List<CVMunicipality> municipalities) {
        this.municipalities = municipalities;
    }

}
