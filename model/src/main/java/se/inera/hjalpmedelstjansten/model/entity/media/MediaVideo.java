package se.inera.hjalpmedelstjansten.model.entity.media;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;

/**
 * Media of type video
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = MediaVideo.FIND_BY_PRODUCT, query = "SELECT m FROM MediaVideo m WHERE m.product.uniqueId = :productUniqueId ORDER BY m.created DESC")
})
public class MediaVideo extends Media {

    public static final String FIND_BY_PRODUCT = "HJMT_MEDIAVIDEO_FIND_BY_PRODUCT";

    @Column(nullable = true)
    private String alternativeText;

    public String getAlternativeText() {
        return alternativeText;
    }

    public void setAlternativeText(String alternativeText) {
        this.alternativeText = alternativeText;
    }

}
