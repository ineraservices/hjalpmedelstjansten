package se.inera.hjalpmedelstjansten.model.entity.media;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;


@Entity
@NamedQueries({
    @NamedQuery(name = ArticleMediaVideo.FIND_BY_ARTICLE, query = "SELECT a FROM ArticleMediaVideo a WHERE a.article.uniqueId = :articleUniqueId ORDER BY a.created DESC"),
    @NamedQuery(name = ArticleMediaVideo.FIND_BY_MEDIAVIDEO, query = "SELECT a FROM ArticleMediaVideo a WHERE a.mediaVideo.uniqueId = :mediaUniqueId ORDER BY a.created DESC")
})
public class ArticleMediaVideo extends ArticleMedia {

    public static final String FIND_BY_ARTICLE = "HJMT_ARTICLEMEDIAVIDEO_FIND_BY_ARTICLE";
    public static final String FIND_BY_MEDIAVIDEO = "HJMT_ARTICLEMEDIAVIDEO_FIND_BY_MEDIAVIDEO";

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mediaVideoId", nullable = false)
    private MediaVideo mediaVideo;

    public MediaVideo getMediaVideo() {
        return mediaVideo;
    }

    public void setMediaVideo(MediaVideo mediaVideo) {
        this.mediaVideo = mediaVideo;
    }

}
