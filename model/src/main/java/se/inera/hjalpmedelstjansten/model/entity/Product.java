package se.inera.hjalpmedelstjansten.model.entity;

import jakarta.persistence.ManyToOne;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEDirective;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEStandard;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * JPA Entity class for Product
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = Product.FIND_ALL, query = "SELECT p FROM Product p ORDER BY p.uniqueId ASC"),
    @NamedQuery(name = Product.GET_BY_ORGANIZATION, query = "SELECT p FROM Product p WHERE p.organization.uniqueId = :organizationUniqueId AND p.status IN :statuses AND p.category.articleType IN :articleTypes ORDER BY p.productName ASC"),
    @NamedQuery(name = Product.COUNT_SEARCH_BY_ORGANIZATION, query = "SELECT COUNT(p) FROM Product p WHERE p.organization.uniqueId = :organizationUniqueId AND p.status IN :statuses AND p.category.articleType IN :articleTypes AND p.productName LIKE :query"),
    @NamedQuery(name = Product.FIND_BY_STATUS_AND_REPLACEMENT_DATE_PASSED, query = "SELECT p FROM Product p WHERE p.status = :status AND p.replacementDate < :replacementDate"),
    @NamedQuery(name = Product.FIND_BY_PRODUCT_NUMBER_AND_ORGANIZATION, query = "SELECT p FROM Product p WHERE p.productNumber = :productNumber AND p.organization.uniqueId = :organizationUniqueId"),
    @NamedQuery(name = Product.FIND_BY_ORGANIZATION_AND_CATEGORY, query = "SELECT p FROM Product p WHERE p.organization.uniqueId = :organizationUniqueId AND p.category.uniqueId = :categoryUniqueId"),
        @NamedQuery(name = Product.FIND_BY_CATEGORY, query = "SELECT p FROM Product p WHERE p.category.uniqueId = :categoryUniqueId"),
        @NamedQuery(name = Product.FIND_BY_UNIQUE_ID, query = "SELECT p FROM Product p WHERE p.uniqueId = :productUniqueId")
})
public class Product implements Serializable {


    public enum Status {
        PUBLISHED,
        DISCONTINUED
    }

    public static final String FIND_ALL = "HJMT_PRODUCT_FIND_ALL";
    public static final String COUNT_SEARCH_BY_ORGANIZATION = "HJMT_PRODUCT_COUNT_SEARCH_BY_ORGANIZATION";
    public static final String GET_BY_ORGANIZATION = "HJMT_PRODUCT_GET_BY_ORGANIZATION";
    public static final String FIND_BY_STATUS_AND_REPLACEMENT_DATE_PASSED = "HJMT_PRODUCT_FIND_BY_STATUS_AND_REPLACEMENT_DATE_PASSED";
    public static final String FIND_BY_PRODUCT_NUMBER_AND_ORGANIZATION = "HJMT_PRODUCT_FIND_BY_ARTICLE_NUMBER_AND_ORGANIZATION";
    public static final String FIND_BY_ORGANIZATION_AND_CATEGORY = "HJMT_PRODUCT_FIND_BY_ORGANIZATION_AND_CATEGORY";

    public static final String FIND_BY_CATEGORY = "HJMT_PRODUCT_FIND_BY_CATEGORY";
    public static final String FIND_BY_UNIQUE_ID = "HJMT_PRODUCT_FIND_BY_ID";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = false)
    private String productName;

    @Column(nullable = false, length = 35)
    private String productNumber;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organizationId", nullable = false)
    private Organization organization;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "categoryId", nullable = false)
    private Category category;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name="ProductExtendedCategory",
        joinColumns={@JoinColumn(name="productId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="categoryId", referencedColumnName="uniqueId")})
    private List<Category> extendedCategories;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "preventiveMaintenanceValidFromId", nullable = true)
    private CVPreventiveMaintenance preventiveMaintenanceValidFrom;

    @Column(nullable = true)
    private Integer preventiveMaintenanceNumberOfDays;

    @Column(nullable = true)
    private String preventiveMaintenanceDescription;

    @Column(nullable = true)
    private String manufacturer;

    @Column(nullable = true)
    private String manufacturerProductNumber;

    @Column(nullable = true)
    private String trademark;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "manufacturerElectronicAddressId", nullable = true)
    private ElectronicAddress manufacturerElectronicAddress;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean ceMarked;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean customerUnique;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ceDirectiveId", nullable = true)
    private CVCEDirective ceDirective;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ceStandardId", nullable = true)
    private CVCEStandard ceStandard;

    @Column(nullable = true, length = 512)
    private String supplementedInformation;

    @Column(nullable = true, length = 500)
    private String descriptionForElvasjuttiosju;

    @Column(nullable = false)
    private String mainImageUrl;

    @Column(length = 255)
    private String mainImageDescription;

    @Column(nullable = false, length = 255)
    private String mainImageAltText;

    @Column()
    private String mainImageOriginalUrl;

    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "catalogUniqueNumberId", nullable = true)
    private CatalogueUniqueNumber catalogUniqueNumber;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
        name="ProductReplacedByProduct",
        joinColumns={@JoinColumn(name="productId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="replacedByProductId", referencedColumnName="uniqueId")})
    private List<Product> replacedByProducts;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date replacementDate;

    @Column(nullable = true, columnDefinition = "TINYINT(1)")
    private Boolean inactivateRowsOnReplacement;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orderUnitId", nullable = true)
    private CVOrderUnit orderUnit;

    private Double articleQuantityInOuterPackage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "articleQuantityInOuterPackageUnitId", nullable = true)
    private CVOrderUnit articleQuantityInOuterPackageUnit;

    private Double packageContent;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "packageContentId", nullable = true)
    private CVPackageUnit packageContentUnit;

    @Column(nullable = true)
    private Integer packageLevelBase;

    @Column(nullable = true)
    private Integer packageLevelMiddle;

    @Column(nullable = true)
    private Integer packageLevelTop;

    @Column(nullable = true)
    private String color;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date lastUpdated;

    @OneToMany(
            mappedBy = "product",
            cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE},
            fetch = FetchType.LAZY,
            orphanRemoval = true
    )
    private List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues;

    @Column(nullable = false, unique = true)
    private String mediaFolderName;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Category> getExtendedCategories() {
        return extendedCategories;
    }

    public void setExtendedCategories(List<Category> extendedCategories) {
        this.extendedCategories = extendedCategories;
    }

    public CVPreventiveMaintenance getPreventiveMaintenanceValidFrom() {
        return preventiveMaintenanceValidFrom;
    }

    public void setPreventiveMaintenanceValidFrom(CVPreventiveMaintenance preventiveMaintenanceValidFrom) {
        this.preventiveMaintenanceValidFrom = preventiveMaintenanceValidFrom;
    }

    public Integer getPreventiveMaintenanceNumberOfDays() {
        return preventiveMaintenanceNumberOfDays;
    }

    public void setPreventiveMaintenanceNumberOfDays(Integer preventiveMaintenanceNumberOfDays) {
        this.preventiveMaintenanceNumberOfDays = preventiveMaintenanceNumberOfDays;
    }

    public String getPreventiveMaintenanceDescription() {
        return preventiveMaintenanceDescription;
    }

    public void setPreventiveMaintenanceDescription(String preventiveMaintenanceDescription) {
        this.preventiveMaintenanceDescription = preventiveMaintenanceDescription;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getManufacturerProductNumber() {
        return manufacturerProductNumber;
    }

    public void setManufacturerProductNumber(String manufacturerProductNumber) {
        this.manufacturerProductNumber = manufacturerProductNumber;
    }

    public String getTrademark() {
        return trademark;
    }

    public void setTrademark(String trademark) {
        this.trademark = trademark;
    }

    public ElectronicAddress getManufacturerElectronicAddress() {
        return manufacturerElectronicAddress;
    }

    public void setManufacturerElectronicAddress(ElectronicAddress manufacturerElectronicAddress) {
        this.manufacturerElectronicAddress = manufacturerElectronicAddress;
    }

    public boolean isCeMarked() {
        return ceMarked;
    }

    public void setCeMarked(boolean ceMarked) {
        this.ceMarked = ceMarked;
    }

    public boolean isCustomerUnique() {
        return customerUnique;
    }

    public void setCustomerUnique(boolean customerUnique) {
        this.customerUnique = customerUnique;
    }

    public CVCEDirective getCeDirective() {
        return ceDirective;
    }

    public void setCeDirective(CVCEDirective ceDirective) {
        this.ceDirective = ceDirective;
    }

    public CVCEStandard getCeStandard() {
        return ceStandard;
    }

    public void setCeStandard(CVCEStandard ceStandard) {
        this.ceStandard = ceStandard;
    }

    public String getSupplementedInformation() {
        return supplementedInformation;
    }

    public void setSupplementedInformation(String supplementedInformation) {
        this.supplementedInformation = supplementedInformation;
    }

    public String getDescriptionForElvasjuttiosju() {
        return descriptionForElvasjuttiosju;
    }

    public void setDescriptionForElvasjuttiosju(String descriptionForElvasjuttiosju) {
        this.descriptionForElvasjuttiosju = descriptionForElvasjuttiosju;
    }

    public String getMainImageUrl() {
        return mainImageUrl;
    }

    public void setMainImageUrl(String mainImageUrl) {
        this.mainImageUrl = mainImageUrl;
    }

    public String getMainImageDescription() {
        return mainImageDescription;
    }

    public void setMainImageDescription(String mainImageDescription) {
        this.mainImageDescription = mainImageDescription;
    }

    public String getMainImageOriginalUrl() {
        return mainImageOriginalUrl;
    }

    public void setMainImageOriginalUrl(String mainImageOriginalUrl) {
        this.mainImageOriginalUrl = mainImageOriginalUrl;
    }

    public String getMainImageAltText() {
        return mainImageAltText;
    }

    public void setMainImageAltText(String mainImageAltText) {
        this.mainImageAltText = mainImageAltText;
    }

    public CatalogueUniqueNumber getCatalogUniqueNumber() {
        return catalogUniqueNumber;
    }

    public void setCatalogUniqueNumber(CatalogueUniqueNumber catalogUniqueNumber) {
        this.catalogUniqueNumber = catalogUniqueNumber;
    }

    public List<Product> getReplacedByProducts() {
        return replacedByProducts;
    }

    public void setReplacedByProducts(List<Product> replacedByProducts) {
        this.replacedByProducts = replacedByProducts;
    }

    public Date getReplacementDate() {
        return replacementDate;
    }

    public void setReplacementDate(Date replacementDate) {
        this.replacementDate = replacementDate;
    }

    public Boolean isInactivateRowsOnReplacement() {
        return inactivateRowsOnReplacement;
    }

    public void setInactivateRowsOnReplacement(Boolean inactivateRowsOnReplacement) {
        this.inactivateRowsOnReplacement = inactivateRowsOnReplacement;
    }

    public CVOrderUnit getOrderUnit() {
        return orderUnit;
    }

    public void setOrderUnit(CVOrderUnit orderUnit) {
        this.orderUnit = orderUnit;
    }

    public Double getArticleQuantityInOuterPackage() {
        return articleQuantityInOuterPackage;
    }

    public void setArticleQuantityInOuterPackage(Double articleQuantityInOuterPackage) {
        this.articleQuantityInOuterPackage = articleQuantityInOuterPackage;
    }

    public CVOrderUnit getArticleQuantityInOuterPackageUnit() {
        return articleQuantityInOuterPackageUnit;
    }

    public void setArticleQuantityInOuterPackageUnit(CVOrderUnit articleQuantityInOuterPackageUnit) {
        this.articleQuantityInOuterPackageUnit = articleQuantityInOuterPackageUnit;
    }

    public Double getPackageContent() {
        return packageContent;
    }

    public void setPackageContent(Double packageContent) {
        this.packageContent = packageContent;
    }

    public CVPackageUnit getPackageContentUnit() {
        return packageContentUnit;
    }

    public void setPackageContentUnit(CVPackageUnit packageContentUnit) {
        this.packageContentUnit = packageContentUnit;
    }

    public Integer getPackageLevelBase() {
        return packageLevelBase;
    }

    public void setPackageLevelBase(Integer packageLevelBase) {
        this.packageLevelBase = packageLevelBase;
    }

    public Integer getPackageLevelMiddle() {
        return packageLevelMiddle;
    }

    public void setPackageLevelMiddle(Integer packageLevelMiddle) {
        this.packageLevelMiddle = packageLevelMiddle;
    }

    public Integer getPackageLevelTop() {
        return packageLevelTop;
    }

    public void setPackageLevelTop(Integer packageLevelTop) {
        this.packageLevelTop = packageLevelTop;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public List<ResourceSpecificPropertyValue> getResourceSpecificPropertyValues() {
        return resourceSpecificPropertyValues;
    }

    public void setResourceSpecificPropertyValues(List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues) {
        this.resourceSpecificPropertyValues = resourceSpecificPropertyValues;
    }

    public String getMediaFolderName() {
        return mediaFolderName;
    }

    public void setMediaFolderName(String mediaFolderName) {
        this.mediaFolderName = mediaFolderName;
    }

    @PrePersist
    private void prePersist() {
        this.created = new Date();
        this.catalogUniqueNumber = new CatalogueUniqueNumber();
    }

    @PreUpdate
    private void preUpdate() {
        this.lastUpdated = new Date();
    }

}
