package se.inera.hjalpmedelstjansten.model.entity.cv;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import java.io.Serializable;

/**
 * JPA Entity class for CE Directive
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = CVDocumentType.FIND_ALL, query = "SELECT c FROM CVDocumentType c ORDER BY c.displayOrder ASC"),
    @NamedQuery(name = CVDocumentType.FIND_BY_VALUE, query = "SELECT c FROM CVDocumentType c WHERE c.value = :value")
})
public class CVDocumentType implements Serializable {

    public static final String FIND_ALL = "HJMT_CVDOCUMENTTYPE_FIND_ALL";
    public static final String FIND_BY_VALUE = "HJMT_CVDOCUMENTTYPE_FIND_BY_VALUE";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = false, unique = true)
    private String code;

    @Column(nullable = false, unique = true)
    private String value;

    @Column(nullable = false, unique = true)
    private int displayOrder;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

}
