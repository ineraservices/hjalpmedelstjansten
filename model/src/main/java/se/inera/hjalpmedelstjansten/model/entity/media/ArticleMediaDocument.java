package se.inera.hjalpmedelstjansten.model.entity.media;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;


@Entity
@NamedQueries({
    @NamedQuery(name = ArticleMediaDocument.FIND_BY_ARTICLE, query = "SELECT a FROM ArticleMediaDocument a WHERE a.article.uniqueId = :articleUniqueId ORDER BY a.created DESC"),
    @NamedQuery(name = ArticleMediaDocument.FIND_BY_MEDIADOCUMENT, query = "SELECT a FROM ArticleMediaDocument a WHERE a.mediaDocument.uniqueId = :mediaUniqueId ORDER BY a.created DESC")
})
public class ArticleMediaDocument extends ArticleMedia {

    public static final String FIND_BY_ARTICLE = "HJMT_ARTICLEMEDIADOCUMENT_FIND_BY_ARTICLE";
    public static final String FIND_BY_MEDIADOCUMENT = "HJMT_ARTICLEMEDIADOCUMENT_FIND_BY_MEDIADOCUMENT";

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mediaDocumentId", nullable = false)
    private MediaDocument mediaDocument;

    public MediaDocument getMediaDocument() {
        return mediaDocument;
    }

    public void setMediaDocument(MediaDocument mediaDocument) {
        this.mediaDocument = mediaDocument;
    }

}
