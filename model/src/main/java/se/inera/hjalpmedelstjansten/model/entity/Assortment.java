package se.inera.hjalpmedelstjansten.model.entity;

import se.inera.hjalpmedelstjansten.model.entity.cv.CVCounty;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVMunicipality;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * JPA Entity class for Assortment
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = Assortment.GET_ARTICLE_UNIQUE_IDS, query = "SELECT ar.uniqueId FROM Assortment a JOIN a.articles ar WHERE a.uniqueId = :assortmentUniqueId"),
    @NamedQuery(name = Assortment.FIND_BY_ARTICLE, query = "SELECT a FROM Assortment a JOIN a.articles ar WHERE ar.uniqueId = :articleUniqueId")
})
public class Assortment implements Serializable {

    public enum Status {
        FUTURE, CURRENT, DISCONTINUED
    }

    public static final String GET_ARTICLE_UNIQUE_IDS = "HJMT_ASSORTMENT_GET_ARTICLE_UNIQUE_IDS";
    public static final String FIND_BY_ARTICLE = "HJMT_ASSORTMENT_FIND_BY_ARTICLE";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long uniqueId;

    @Column(nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customerOrganizationId", nullable = false)
    private Organization customer;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date validFrom;

    @Temporal(TemporalType.TIMESTAMP)
    @Column()
    private Date validTo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "countyId")
    private CVCounty county;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name="AssortmentExternalCategoryGrouping",
        joinColumns={@JoinColumn(name="assortmentId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="externalCategoryGroupingId", referencedColumnName="uniqueId")})
    private List<ExternalCategoryGrouping> externalCategoryGroupings;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name="AssortmentCVMunicipality",
        joinColumns={@JoinColumn(name="assortmentId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="municipalityId", referencedColumnName="uniqueId")})
    private List<CVMunicipality> municipalities;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name="AssortmentUserEngagement",
        joinColumns={@JoinColumn(name="assortmentId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="userEngagementId", referencedColumnName="uniqueId")})
    private List<UserEngagement> managers;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name="AssortmentArticle",
        joinColumns={@JoinColumn(name="assortmentId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="articleId", referencedColumnName="uniqueId")})
    private List<Article> articles;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column()
    private Date lastUpdated;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Organization getCustomer() {
        return customer;
    }

    public void setCustomer(Organization customer) {
        this.customer = customer;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public CVCounty getCounty() {
        return county;
    }

    public void setCounty(CVCounty county) {
        this.county = county;
    }

    public List<CVMunicipality> getMunicipalities() {
        return municipalities;
    }

    public void setMunicipalities(List<CVMunicipality> municipalities) {
        this.municipalities = municipalities;
    }

    public List<ExternalCategoryGrouping> getExternalCategoryGroupings() {
        return externalCategoryGroupings;
    }

    public void setExternalCategoryGroupings(List<ExternalCategoryGrouping> externalCategoryGroupings) {
       this.externalCategoryGroupings = externalCategoryGroupings;
    }

    public List<UserEngagement> getManagers() {
        return managers;
    }

    public void setManagers(List<UserEngagement> managers) {
        this.managers = managers;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @PrePersist
    private void prePersist() {
        this.created = new Date();
    }

    @PreUpdate
    private void preUpdate() {
        this.lastUpdated = new Date();
    }

}
