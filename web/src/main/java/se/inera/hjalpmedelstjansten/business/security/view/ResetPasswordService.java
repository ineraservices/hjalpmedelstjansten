package se.inera.hjalpmedelstjansten.business.security.view;

import java.util.logging.Level;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.MdcToolkit;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.ChangeUserPasswordAPI;
import se.inera.hjalpmedelstjansten.model.api.RequestChangeUserPasswordAPI;

/**
 * REST API methods needed for resetting user password
 *
 */
@Stateless
@Path("reset")
@Interceptors({ PerformanceLogInterceptor.class })
public class ResetPasswordService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    private UserController userController;

    /**
     * Request to change a user's password based on a supplied user information
     *
     * @param requestChangeUserPasswordAPI
     * @return HTTP 200 if request was validated, otherwise 400
     * @throws HjalpmedelstjanstenValidationException  if validation of supplied user information fails
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException if a technical issue, like
     * not being able to send email occurs
     */
    @POST
    @Path("request")
    public Response requestChangePassword(RequestChangeUserPasswordAPI requestChangeUserPasswordAPI) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "requestChangePassword(...)");
        userController.requestChangePassword(requestChangeUserPasswordAPI);
        return Response.ok().build();
    }

    /**
     * Change a user's password based on a token, e.g. received in an email
     *
     * @param changeUserPasswordAPI
     * @return HTTP 200 if password was reset, otherwise 403
     * @throws HjalpmedelstjanstenValidationException
     */
    @POST
    @Path("change")
    public Response changePasswordWithEmailToken(ChangeUserPasswordAPI changeUserPasswordAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "changePasswordWithEmailToken(...)");
        boolean success = userController.changePasswordWithEmailToken(changeUserPasswordAPI);
        if( success ) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
    }

    /**
     * Validate a token, e.g. received in an email.
     *
     * @param tokenAPI
     * @return HTTP 200 if token validates, otherwise 403
     */
    @POST
    @Path("validate")
    public Response validateEmailToken(ChangeUserPasswordAPI tokenAPI) {
        LOG.log(Level.FINEST, "validateEmailToken(...)");
        boolean success = userController.validateEmailToken(tokenAPI);
        if( success ) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
    }

    /**
     * Send welcome mail to all new users. Only super admin should have this
     * capability.
     *
     * @return
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
     */
    @POST
    @Path("welcomeall")
    @SecuredService(permissions = {"user:update"})
    public Response welcomeNewUsers() throws HjalpmedelstjanstenException, HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "welcomeNewUsers( ... )" );
        userController.sendWelcomeAllNew(MdcToolkit.copyOfMdc());
        return Response.ok().build();
    }

}
