package se.inera.hjalpmedelstjansten.business.media.controller;

import lombok.Setter;
import org.apache.commons.io.IOUtils;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.mime.MimeTypeException;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.HeadObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.*;
import jakarta.inject.Inject;
import javax.net.ssl.SSLParameters;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Set;
import java.util.logging.Level;

/**
 * This class uses Amazon S3 to store media files (documents and images).
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class MediaUploadAmazonS3Controller {

    @Inject
    HjmtLogger LOG;

    @Inject
    ValidationMessageService validationMessageService;

    private S3Configuration s3Config;

    @Setter
    private S3Client s3Client;

    @Setter
    private HttpClient httpClient;

    /**
     * @param objectKey object key for the file
     * @param url desired remote file to upload to s3
     * @return HTTP Path to uploaded file.
     * @throws HjalpmedelstjanstenValidationException
     */
    public String uploadFileFromUrl(String objectKey, String url) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, String.format("uploadFileFromUrl, ObjectKey: %s. UrlString: %s", objectKey, url));

        HttpResponse<InputStream> response = null;

        try {
            response = getMediaStream(url);
            final var contentType = response.headers()
                .firstValue("Content-Type")
                .orElseThrow(() -> new IllegalArgumentException("Missing Content-Type"));

            LOG.log(Level.FINEST, "uploadFileFromUrl: Uploading file");
            return uploadFile(correctFilename(contentType, objectKey), response.body(), contentType);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, String.format("Failed to handle media upload using url: %s for objectKey: %s", url, objectKey) , e);
            throw validationMessageService.generateValidationException("file", "media.url.fetchfail");
        } finally {
            try {
                if (response != null && response.body() != null) {
                    response.body().close();
                }
            } catch (Exception ex) {
                LOG.log(Level.SEVERE, "Failed to close inputstream on upload", ex);
            }
        }
    }

    /**
     * Uploads a file to the configured AWS S3 bucket.
     *
     * @param objectKey      The key under which to store the new object.
     * @param inputStream    The input stream containing the file content.
     * @param contentType    The IANA media type of the file.
     * @return The URL of the uploaded file.
     * @throws HjalpmedelstjanstenValidationException If an error occurs during the upload process.
     */
    public String uploadFile(String objectKey, InputStream inputStream, String contentType) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, String.format("uploadFile( objectKey: %s )", objectKey));

        validateSupportedContentType(contentType);

        try {
            final var bytes = IOUtils.toByteArray(inputStream);
            final var request = PutObjectRequest.builder()
                .contentLength((long) bytes.length)
                .contentType(contentType)
                .bucket(s3Config.getBucketName())
                .key(objectKey)
                .build();

            final var requestBody = RequestBody.fromInputStream(new ByteArrayInputStream(bytes), bytes.length);
            s3Client.putObject(request, requestBody);
        } catch (Exception e) {
            LOG.log(Level.WARNING, String.format("Failed to upload file to bucket: %s for objectKey: %s of contentType: %s.", s3Config.getBucketName(), objectKey, contentType), e);
            throw validationMessageService.generateValidationException("file", "media.file.uploadfail");
        }

        return s3Config.getMediaBaseUrl() + (s3Config.getMediaBaseUrl().endsWith("/") ? "" : "/") + objectKey;
    }

    /**
     * Deletes a file from the configured AWS S3 bucket based on the provided object key.
     *
     * @param objectKey The key of the object to be deleted.
     * @throws HjalpmedelstjanstenValidationException If an error occurs during the deletion process.
     */
    public void deleteFile(String objectKey) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteFile( objectKey: {0} )", new Object[]{objectKey});

        if (!objectExists(objectKey)) {
            LOG.log(Level.WARNING, "No object with key exists in bucket when trying to delete");
            return;
        }

        try {
            s3Client.deleteObject(DeleteObjectRequest.builder()
                .bucket(s3Config.getBucketName())
                .key(objectKey)
                .build());
        } catch (Exception e) {
            LOG.log(Level.SEVERE, String.format("Failed to delete file from bucket %s for objectKey: %s.", s3Config.getBucketName(), objectKey), e);
            throw validationMessageService.generateValidationException("file", "media.file.deletefail");
        }
    }

    /**
     * Checks if an object with the specified key exists in the configured AWS S3 bucket.
     *
     * @param objectKey The key of the object to check for existence.
     * @return {@code true} if the object exists, {@code false} otherwise.
     */
    private boolean objectExists(String objectKey) {
        try {
            s3Client.headObject(HeadObjectRequest.builder()
                .bucket(s3Config.getBucketName())
                .key(objectKey)
                .build());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * @param url desired remote file to upload to s3
     * @return returns the HTTPResponse
     * @throws IOException potential error
     */
    private HttpResponse<InputStream> getMediaStream(String url) throws IOException {
        try {
            final var request = HttpRequest.newBuilder().uri(URI.create(url)).GET().build();
            return httpClient.send(request, HttpResponse.BodyHandlers.ofInputStream());
        } catch (Exception e) {
            LOG.log(Level.SEVERE, String.format("Interrupted while fetching media with url: %s", url), e);
            Thread.currentThread().interrupt();
            throw new IOException("Interrupted while fetching media", e);
        }
    }

    /**
     * @param contentType IANA media type
     * @return file extension. example ".pdf"
     * @throws MimeTypeException exception thrown when type is unknown
     */
    private String getFileExtensionFromContentType(String contentType) throws MimeTypeException {
        assert contentType != null;
        final var allTypes = TikaConfig.getDefaultConfig().getMimeRepository();
        return allTypes.forName(contentType).getExtension();
    }

    /**
     * @param contentType IANA media type
     * @param fileName    filename to be corrected
     * @return filename with correct file extension.
     * @throws MimeTypeException
     */
    private String correctFilename(String contentType, String fileName) throws MimeTypeException {
        return fileName.replace(fileName.substring(fileName.lastIndexOf('.')), getFileExtensionFromContentType(contentType));
    }

    /**
     * @param contentType IANA media type
     * @throws HjalpmedelstjanstenValidationException validation error
     */
    private void validateSupportedContentType(String contentType) throws HjalpmedelstjanstenValidationException {
        final var mainType = contentType.substring(0, contentType.lastIndexOf('/')).toLowerCase();
        final var subType = contentType.substring(contentType.lastIndexOf('/') + 1).toLowerCase();

        if (!Set.of("image", "application", "binary").contains(mainType)) {
            LOG.log(Level.WARNING, String.format("Only accepts image, application and one binary as a main content-type, type: %s is not supported", contentType));
            throw new HjalpmedelstjanstenValidationException("Non supported content-type");
        }
        if (mainType.equals("binary") && !subType.equals("octet-stream")) {
            LOG.log(Level.WARNING, String.format("Only accepts octet-stream as binary type: %s is not supported", subType));
            throw new HjalpmedelstjanstenValidationException("Non supported content-type");
        }
    }

    @PostConstruct
    private void initialize() {
        final var sslParameters = new SSLParameters();
        sslParameters.setProtocols(new String[] {"TLSv1.2"});
        httpClient = HttpClient.newBuilder()
            .followRedirects(HttpClient.Redirect.ALWAYS)
            .sslParameters(sslParameters)
            .build();

        s3Config = new S3Configuration();

        if (!s3Config.isValid()) {
            LOG.log(Level.INFO, "Local S3 is being configured");
            StaticCredentialsProvider credentialsProvider = StaticCredentialsProvider.create(
                AwsBasicCredentials.create("test", "testtest"));

            s3Client = S3Client.builder()
                .region(Region.AWS_GLOBAL)
                .forcePathStyle(true)
                .credentialsProvider(credentialsProvider)
                .endpointOverride(URI.create("http://minio:9000"))
                .build();
            this.s3Config
                .setBucketName("test")
                .setMediaBaseUrl("http://localhost:9000/test/");
        } else {
            s3Client = S3Client.builder()
                .credentialsProvider(EnvironmentVariableCredentialsProvider.create())
                .build();
        }

        LOG.log(Level.INFO, String.format("AWS BUCKET = %s)", s3Config.getBucketName()));
        LOG.log(Level.INFO, String.format("MEDIA_BASE_URL = %s", s3Config.getMediaBaseUrl()));
    }

}
