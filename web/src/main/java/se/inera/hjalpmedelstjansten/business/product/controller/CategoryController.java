package se.inera.hjalpmedelstjansten.business.product.controller;

import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.helpers.ExportHelper;
import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyApiEXT;
import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyListValueAPIEXT;
import se.inera.hjalpmedelstjansten.model.api.ChangeCategoryOnProductAPI;
import se.inera.hjalpmedelstjansten.model.api.ExternalCategoryGroupingAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.*;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;


@Stateless
public class CategoryController {


    @Inject
    HjmtLogger LOG;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    ElasticSearchController elasticSearchController;

    @Inject
    ProductController productController;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    ExportHelper exportHelper;

    static final String CATEGORIES_SHEET_NAME = "Kategorier";
    static final String CLEAR_VALUE_STRING = "*radera*";
    static final String VALUELIST_SHEET_NAME = "Värdelistor";



    /**
     * Search for categories based on user input. If no query and no code is requested
     * returns all first level codes (without parents)
     *
     * @param queryString user query
     * @param code code to search for
     * @param articleType if this is not null, only one category is returned, which is
     * the one without a code for this article type
     * @param excludeCodeLessCategories
     * @param excludeCodeCategories
     * @return a list of <code>CategoryAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if articleType is H
     */



    public List<CategoryAPI> searchCategories(String queryString,
            String code,
            Article.Type articleType,
            boolean excludeCodeLessCategories,
            boolean excludeCodeCategories) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "searchCategories(...)");
        if( articleType != null ) {
            if( articleType == Article.Type.H ) {
                LOG.log( Level.WARNING, "Attempt to filter categories on articletype H which is not allowed");
                throw validationMessageService.generateValidationException("type", "category.selectHNotAllowed");
            }
            List<Category> categorys = em.createNamedQuery(Category.FIND_BY_ARTICLE_TYPE_EMPTY_CODE).
                        setParameter("articleType", articleType).
                        getResultList();
            if( categorys == null || categorys.isEmpty() ) {
                LOG.log( Level.WARNING, "No category without code found for articleType: {0}", new Object[] {articleType});
                return null;
            } else if( categorys.size() > 1 ) {
                LOG.log( Level.WARNING, "Multiple categorys without code found for articleType: {0}", new Object[] {articleType});
                return null;
            } else {
                return CategoryMapper.map(categorys);
            }
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT c FROM Category c ");
            boolean orderByUniqueId = false;
            if( queryString != null && !queryString.isEmpty() ) {
                sb.append("WHERE (c.name LIKE :query OR c.code LIKE :query) AND c.articleType IS NOT NULL ");
            } else if( code != null && !code.isEmpty() ) {
                sb.append("WHERE c.parent.code = :code ");
            } else {
                sb.append("WHERE c.parent IS NULL ");
                //orderByUniqueId = true;
            }
            if( excludeCodeLessCategories ) {
                sb.append("AND c.code IS NOT NULL ");
            }
            if( excludeCodeCategories ) {
                sb.append("AND c.code IS NULL ");
            }
            // when looking for first level codes, we want to order by id because
            // there may be categories that do not have codes to sort on
            if( orderByUniqueId ) {
                sb.append( "ORDER BY c.uniqueId ASC" );
            } else {
                sb.append( "ORDER BY c.code ASC" );
            }
            String querySql = sb.toString();
            LOG.log(Level.FINEST, "querySql: {0}", new Object[] {querySql});
            Query query = em.createQuery(sb.toString());
            if( queryString != null && !queryString.isEmpty() ) {
                query.setParameter("query", "%" + queryString + "%");
            }
            if( code != null && !code.isEmpty() ) {
                query.setParameter("code", code);
            }
            return CategoryMapper.map(query.getResultList());
        }
    }




    public boolean DeleteExtendedCategorysOnProducts(long id)throws HjalpmedelstjanstenValidationException{


        try {
            StringBuilder sb = new StringBuilder();
            sb.append("DELETE FROM ArticleExtendedCategory WHERE categoryId = :id ");


            String querySql = sb.toString();
            Query query = em.createQuery(sb.toString());
            query.setParameter("id", id);
            LOG.log(Level.FINEST, "querySql: {0}", new Object[] {querySql});
            int posts = query.executeUpdate();
            //  Block of code to try
        }
        catch(Exception e) {
            //  Block of code to handle errors
            throw validationMessageService.generateValidationException("code", "Fel när kategorin skulle tas bort pga extendedcategoryproducts");
        }





        return true;
    }

    public boolean DeleteExtendedCategorysOnArticles(long id)throws HjalpmedelstjanstenValidationException{

        try {
            //  Block of code to try
            StringBuilder sb = new StringBuilder();
            sb.append("DELETE FROM ProductExtendedCategory WHERE categoryId = :id ");


            String querySql = sb.toString();
            Query query = em.createQuery(sb.toString());
            query.setParameter("id", id);
            LOG.log(Level.FINEST, "querySql: {0}", new Object[] {querySql});
            int posts = query.executeUpdate();
            return true;
        }
        catch(Exception e) {
            throw validationMessageService.generateValidationException("code", "Fel när kategorin skulle tas bort pga extendedcategoryproducts");
            //  Block of code to handle errors
        }

    }

    public boolean upDateChildCategoryArticleTypes(Article.Type type, long categoryUniqueId)
    {

        try {
            LOG.log( Level.INFO, "Update childcategorys when updating level 3 category");
            Category categoryEntity = em.find(Category.class, categoryUniqueId);
            categoryEntity.setArticleType(type);
            em.joinTransaction();
            em.merge(categoryEntity);
            return true;
        }
        catch(Exception e) {
            return false;
        }


    }



    public List<Category> getCategory(long categoryUniqueId ) {

       return em.createNamedQuery(Category.FIND_BY_UNIQUEID).setParameter("uniqueId",categoryUniqueId).getResultList();

    }

    public List<CategoryAPI> getAllCategories(){


        long startTime = System.nanoTime();
        List<Category> categories = em.createNamedQuery(Category.FIND_ALL_CATEGORIES).getResultList();
        LOG.log(Level.INFO, "em.createNamedQuery(Category.FIND_ALL_CATEGORIES).getResultList(): {0} nanosec",  new Object[]{(System.nanoTime() - startTime)});
        List<CategoryAPI> categoryAPIS = new ArrayList();
        startTime = System.nanoTime();
        categoryAPIS = CategoryMapper.map(categories);
        LOG.log(Level.INFO, "CategoryMapper.map(categories): {0} nanosec",  new Object[]{(System.nanoTime() - startTime)});

        return categoryAPIS;
    }


    public List<CategoryAPI> getAllCategoriesWithCSP(){
//        String sql = "SELECT c.* FROM ";
//        sql += "FROM hjmtj.Category c ";
//        sql += "LEFT JOIN hjmtj.CategorySpecificProperty csp ON c.uniqueId = csp.categoryId ";
//        sql += "WHERE csp.categoryId IS NOT NULL AND c.code IS NOT NULL ";
        String sql = "SELECT * FROM hjmtj.Category c WHERE c.uniqueId IN (SELECT distinct categoryId FROM hjmtj.CategorySpecificProperty) AND c.code IS NOT NULL";
        List<Category> categories = em.createNativeQuery(sql, Category.class).getResultList();
        List<CategoryAPI> categoriesAPI = CategoryMapper.map(categories);
        return categoriesAPI;
    }

    public List<ArticleAPI> getArticlesByCategory(long categoryUniqueId) {

        List<Article> articles = em.createNamedQuery(Article.FIND_BY_CATEGORY).setParameter("categoryUniqueId", categoryUniqueId).getResultList();
        if (articles.size() != 0) {
            List<ArticleAPI> articleAPIS = ArticleMapper.map(articles, true);

            for (ArticleAPI articleAPI : articleAPIS) {
                Long id = articleAPI.getId();
                List<Article> articleList = em.createNamedQuery(Article.FIND_BY_UNIQUE_ID).setParameter("articleUniqueId", id).getResultList();
                Article article = articleList.get(0);
                Date replacementDate = article.getReplacementDate();
                if (replacementDate != null) {
                    articleAPI.setReplacementDate(replacementDate.getTime());
                }
            }
            return articleAPIS;
        }
        else {
            return null;
        }

    }

    public List<ProductAPI> getProductsByCategory(long categoryUniqueId) {

        List<Product> products = em.createNamedQuery(Product.FIND_BY_CATEGORY).setParameter("categoryUniqueId", categoryUniqueId).getResultList();
        if (products.size() != 0) {
            List<ProductAPI> productAPIS = ProductMapper.map(products);

            for (ProductAPI productAPI : productAPIS) {
                Long id = productAPI.getId();
                List<Product> productList = em.createNamedQuery(Product.FIND_BY_UNIQUE_ID).setParameter("productUniqueId", id ).getResultList();
                Product product = productList.get(0);
                long createdDate = product.getCreated().getTime();
                productAPI.setCreated(createdDate);
                Date replacementDate = product.getReplacementDate();
                if (replacementDate != null) {
                    productAPI.setReplacementDate(replacementDate.getTime());
                }
            }
            return productAPIS;
        }
        else {
            return null;
        }

    }



    public List<Category> getCategoriesByCode(String code )
    {
        return em.createNamedQuery(Category.FIND_BY_CODE).setParameter("code",code).getResultList();
    }

    public List<CategoryAPI> getCategoryAPIbyId(long uniqueId) {
        String sql = "SELECT * FROM hjmtj.Category c WHERE c.uniqueId = " + uniqueId + ";";
        List<Category> category = em.createNativeQuery(sql, Category.class).getResultList();
        List<CategoryAPI> categoryAPI = CategoryMapper.map(category);
        return categoryAPI;
    }

    public void CheckIfCodeIsUnique(Category cat)
            throws HjalpmedelstjanstenValidationException{


        List<Category> catList = getCategoriesByCode(cat.getCode());

        if(cat.getUniqueId() != null){

            if(catList.stream().count() > 0)
            {
                for (Category catItem : catList)
                {
                    if(catItem.getUniqueId() != cat.getUniqueId())
                    {
                        LOG.log( Level.WARNING, "Exception while creating new category - code not unique");
                        throw validationMessageService.generateValidationException("code", "Det existerar redan en kategori med angiven kod");
                    }

                }

            }
            //entity update check
        }
        else
        {
            //create new entity check
            if(catList.stream().count() > 0)
            {
                LOG.log( Level.WARNING, "Exception while creating new category - code not unique");
                throw validationMessageService.generateValidationException("code", "Det existerar redan en kategori med angiven kod");
            }
        }
    }

    public void CheckIfChildrenDependsOnParent(Category cat)throws HjalpmedelstjanstenValidationException
    {
        List<Category> catList = em.createNamedQuery(Category.SEARCH_CATEGORIES_BY_PARENT).setParameter("uniqueId",cat.getUniqueId()).getResultList();
        if(catList.stream().count() > 0)
        {
            throw validationMessageService.generateValidationException("parent", "Det existerar underkategorier som är beroende av denna kod.");
        }
    }


    public Category createCategory(CategoryAPI categoryApi) throws HjalpmedelstjanstenValidationException {



        Category category = CategoryMapper.map(categoryApi);
            Integer level = categoryApi.getLevel();


            //if update
            if (category.getUniqueId() != null) {
                //uppdatera befintlig

                Category categoryEntity = em.find(Category.class, categoryApi.getId());
                categoryEntity.setName(category.getName());
                categoryEntity.setDescription(category.getDescription());


                CheckIfCodeIsUnique(category);

                String currentCode = categoryEntity.getCode();
                String newCode = categoryApi.getCode();
                if(!currentCode.equals(newCode))
                {
                    CheckIfChildrenDependsOnParent(category);
                    categoryEntity.setCode(category.getCode());
                }

                if (level == 3 || level == 4) {
                    categoryEntity.setArticleType(categoryApi.getArticleType());
                    List<Article> articleList = em.createNamedQuery(Article.FIND_BY_CATEGORY).setParameter("categoryUniqueId", categoryApi.getId()).getResultList();
                    List<Product> productList = em.createNamedQuery(Product.FIND_BY_CATEGORY).setParameter("categoryUniqueId", categoryApi.getId()).getResultList();
                    for (Article article : articleList) {
                        Long articleIndex = Long.valueOf(articleList.indexOf(article));
                        Long articleIndexPlusOne = articleIndex +1;
                        LOG.log( Level.INFO, "Article " + articleIndexPlusOne + "/" + articleList.size() + "updated");

                        Article articleEntity = em.find(Article.class, article.getUniqueId());
                        em.joinTransaction();
                        em.merge(articleEntity);
                        ArticleAPI merged = ArticleMapper.map(articleEntity, false, null);
                        elasticSearchController.updateIndexArticle(merged);
                    }
                    for (Product product : productList) {
                        Long productIndex = Long.valueOf(productList.indexOf(product));
                        Long productIndexPlusOne = productIndex +1;
                        LOG.log( Level.INFO, "Product " + productIndexPlusOne + "/" + productList.size() + " updated");

                        Product productEntity = em.find(Product.class, product.getUniqueId());
                        em.joinTransaction();
                        em.merge(productEntity);
                        ProductAPI merged = ProductMapper.map(productEntity, false, null);
                        elasticSearchController.updateIndexProduct(merged);
                    }
                }


                    //update childcategorys with same article type
/*                    if (categoryEntity.getArticleType() != categoryApi.getArticleType()) {
                        //updatera artikeltyp ifall den skiljer sig från tidigare
                        categoryEntity.setArticleType(category.getArticleType());
                        //uppdatera eventuella childnodes som skall ärva artikeltyp från nivå 3
                        List<Category> CategoryList = em.createNamedQuery(Category.SEARCH_CATEGORIES_BY_PARENT).
                                setParameter("uniqueId", category.getUniqueId()).
                                getResultList();

                        if (CategoryList != null) {
                            for (Category cat : CategoryList) {
                                upDateChildCategoryArticleTypes(category.getArticleType(), cat.getUniqueId());
                            }
                        }
                    }*/






                em.joinTransaction();
                em.merge(categoryEntity);
            }
            //else create new
            else {

                if(categoryApi.getParentId() == 0 && categoryApi.getLevel() > 1)
                {
                    throw validationMessageService.generateValidationException("parent", "Du måste välja en överliggande kategori.");
                }

                CheckIfCodeIsUnique(category);

                    if (level == 3 || level == 2) {
                        category.setParent(em.find(Category.class, categoryApi.getParentId()));
                    }
                    if (level == 4) {
                        Category parent = em.find(Category.class, categoryApi.getParentId());
                        category.setParent(parent);
                       /* category.setArticleType(parent.getArticleType());*/
                    }
                    //skapa ny entitet
                    em.persist(category);
                    LOG.log( Level.INFO, "Category " + category.getName() + "created");
                    return category;


                    /*LOG.log( Level.WARNING, "Exception while creating new category");*/

            }

            return category;
        }



    public boolean deleteCategory(long categoryUniqueId)
            throws HjalpmedelstjanstenValidationException{
        //check if category is parent to someone
        Category categoryEntity = em.find(Category.class, categoryUniqueId);


        List<CategoryAPI> childList = searchCategories(null,categoryEntity.getCode(),null,true,false);

        if(childList != null)
        {
            throw validationMessageService.generateValidationException("delete", "Kan ej ta bort kategori för att det finns underliggande kategorier.");
        }

        List<CategorySpecificPropertyAPI> categoryPropertyList = getCategoryPropertyAPIs(categoryUniqueId);

        if(categoryPropertyList != null){
            for(CategorySpecificPropertyAPI categoryProperty : categoryPropertyList){
                DeleteProperties(categoryProperty.getId());
            }
        }
        //måste tas bort
        List<ExternalCategoryGrouping> ecgList = em.createNamedQuery(ExternalCategoryGrouping.FIND_BY_CATEGORYID).setParameter("categoryId",categoryUniqueId).getResultList();
        for(ExternalCategoryGrouping ecg : ecgList){
            em.remove(ecg);
        }

/*        if(categoryPropertyList != null)
        {
            throw validationMessageService.generateValidationException("delete", "Kan ej ta bort kategori för att det finns kopplade kategorispecifika egenskaper kvar.");
        }*/

        List<Article> articleList = em.createNamedQuery(Article.FIND_BY_CATEGORY).setParameter("categoryUniqueId", categoryUniqueId).getResultList();
        if(articleList.size() > 0)
        {
            throw validationMessageService.generateValidationException("delete", "Kan ej ta bort kategori för att det finns kopplade artiklar till kategorin");
        }

        List<Product> productList = em.createNamedQuery(Product.FIND_BY_CATEGORY).setParameter("categoryUniqueId", categoryUniqueId).getResultList();
        if(productList.size() > 0)
        {
            throw validationMessageService.generateValidationException("delete", "Kan ej ta bort kategori för att det finns kopplade produkter till kategorin");
        }

        DeleteExtendedCategorysOnProducts(categoryUniqueId);
        DeleteExtendedCategorysOnArticles(categoryUniqueId);

        LOG.log( Level.INFO, "Deleting category with uniqueId: " + categoryUniqueId);
        em.joinTransaction();
        em.remove(categoryEntity);

        return true;
    }





    public List<Category> getChildlessById(long categoryUniqueId) {
        LOG.log(Level.FINEST, "getChildlessById(categoryUniqueId: {0})", new Object[] {categoryUniqueId});
        return em.createNamedQuery(Category.FIND_BY_ID_AND_LEAFNODE).
                setParameter("uniqueId", categoryUniqueId).
                getResultList();
    }

    /**
     * Get the <code>Category</code> with the given code. We are only
     * interested in categories without children, i.e. leaf nodes cause
     * these are the only ones that can be used.
     *
     * @param code unique code of the category
     * @return the corresponding <code>Category</code>
     */
    public Category getChildlessByCode(String code) {
        LOG.log(Level.FINEST, "getChildlessByCode(code: {0})", new Object[] {code});
        List<Category> categorys = em.createNamedQuery(Category.FIND_BY_CODE_AND_LEAFNODE).
                setParameter("code", code).
                getResultList();
        // category codes are unique so maximum one can be returned
        if( categorys != null && !categorys.isEmpty() ) {
            return categorys.get(0);
        }
        return null;
    }

    /**
     * Get the <code>Category</code> with the given id. We are only
     * interested in categories without children, i.e. leaf nodes cause
     * these are the only ones that can be added.
     *
     * @param categoryUniqueId unique id of the category
     * @return the corresponding list of <code>Category</code>, this should
     * never include more than 1 result
     */
    public Category getById(long categoryUniqueId) {
        LOG.log(Level.FINEST, "getById(categoryUniqueId: {0})", new Object[] {categoryUniqueId});
        return em.find(Category.class, categoryUniqueId);
    }

    /**
     * Get all category specific propertiy apis for the given category
     *
     * @param categoryUniqueId unique id of the category
     * @return a list of specific properties mapped to API, may be null or empty
     */
    public List<CategorySpecificPropertyAPI> getCategoryPropertyAPIs(long categoryUniqueId) {
        LOG.log(Level.FINEST, "getCategoryPropertyAPIs(categoryUniqueId: {0})", new Object[] {categoryUniqueId});
        List<CategorySpecificProperty> categorySpecificPropertys = em.createNamedQuery(CategorySpecificProperty.FIND_BY_CATEGORY).
                setParameter("categoryUniqueId", categoryUniqueId).
                getResultList();
        return CategorySpecificPropertyMapper.map(categorySpecificPropertys);
    }

    /**
     * Get all category specific properties for the given category
     *
     * @param categoryUniqueId unique id of the category
     * @return a list of specific properties, may be null or empty
     */
    public List<CategorySpecificProperty> getCategoryPropertys(Long categoryUniqueId) {
        // TODO: changed from long to Long to make test cases pass, testcases should be fixed instead
        LOG.log(Level.FINEST, "getCategoryPropertys(categoryUniqueId: {0})", new Object[] {categoryUniqueId});
        return em.createNamedQuery(CategorySpecificProperty.FIND_BY_CATEGORY).
                setParameter("categoryUniqueId", categoryUniqueId).
                getResultList();
    }
    public List<Category> getCategoryByArticleType(Article.Type articleType)
    {
        LOG.log(Level.FINEST, "getCategoryByArticleType(articleType: {0})", new Object[] {articleType});
        return em.createNamedQuery(Category.FIND_BY_ARTICLE_TYPE_EMPTY_CODE).
                setParameter("articleType", articleType).
                getResultList();
    }

    public CategorySpecificProperty getCategorySpecificProperty( long categorySpecificPropertyUniqueId ) {
        LOG.log(Level.FINEST, "getCategorySpecificProperty(categorySpecificPropertyUniqueId: {0})", new Object[] {categorySpecificPropertyUniqueId});
        return em.find(CategorySpecificProperty.class, categorySpecificPropertyUniqueId);
    }

    public CategorySpecificPropertyListValue getCategorySpecificPropertyListValue( long categorySpecificPropertyListValueId ) {
        LOG.log(Level.FINEST, "getCategorySpecificPropertyListValue(categorySpecificPropertyListValueId: {0})", new Object[] {categorySpecificPropertyListValueId});
        return em.find(CategorySpecificPropertyListValue.class, categorySpecificPropertyListValueId);
    }

    public boolean UpdateProductAndElasticIndex(ProductAPI productApi, CategoryAPI categoryApi)
    {
        Product productEntity = em.find(Product.class, productApi.getId());
        productEntity.setCategory(CategoryMapper.map(categoryApi));

        em.joinTransaction();
        em.merge(productEntity);

        ProductAPI merged = ProductMapper.map(productEntity,false,null);
        elasticSearchController.updateIndexProduct(merged);

        List<ResourceSpecificPropertyValue> rsValueList = em.createNamedQuery(ResourceSpecificPropertyValue.FIND_RESOURCES_BY_PRODUCTID).
                setParameter("productId",productEntity.getUniqueId()).
                getResultList();
        for(ResourceSpecificPropertyValue rsp : rsValueList){
            LOG.log( Level.INFO, "Deleting ResourceSpecificPropertyValue with productId: " + productEntity.getUniqueId());
            em.remove(rsp);
        }
        LOG.log( Level.INFO, "Reindexing of product: " + productEntity.getProductName() + " completed");
        return true ;
    }

    public boolean UpdateArticleAndElasticIndex(Article article, CategoryAPI categoryApi)
    {
        Article articleEntity = em.find(Article.class, article.getUniqueId());
        articleEntity.setCategory(CategoryMapper.map(categoryApi));

        em.joinTransaction();
        em.merge(articleEntity);

        ArticleAPI merged = ArticleMapper.map(articleEntity,false,null);
        elasticSearchController.updateIndexArticle(merged);

        List<ResourceSpecificPropertyValue> rsValueList = em.createNamedQuery(ResourceSpecificPropertyValue.FIND_RESOURCES_BY_ARTICLEID).
                setParameter("articleId",articleEntity.getUniqueId()).
                getResultList();
        for(ResourceSpecificPropertyValue rsp : rsValueList){
            LOG.log( Level.INFO, "Deleting ResourceSpecificPropertyValue with articleId: " + articleEntity.getUniqueId());
            em.remove(rsp);
        }
        LOG.log( Level.INFO, "Reindexing of article: " + articleEntity.getArticleName() + " completed");



        return true ;
    }

    public boolean ChangeCategorysOnProducts(ChangeCategoryOnProductAPI changeCategoryOnProductAPI) throws HjalpmedelstjanstenValidationException{

        LOG.log(Level.FINEST, "searchCategories(...)");

        if(changeCategoryOnProductAPI.getProducts() != null && changeCategoryOnProductAPI.getProducts().stream().count() > 0)
        {
            int articleCount = 0;
            for (Long item : changeCategoryOnProductAPI.getProducts())
            {
                try{
                    Product product =  em.find(Product.class, item);
                    //får flytta pub DISCONTINUED
                    if(product.getStatus().name().equals("PUBLISHED") || product.getStatus().name().equals("DISCONTINUED"))
                    {
                        UpdateProductAndElasticIndex(ProductMapper.map(product,false,null),changeCategoryOnProductAPI.getToCategory());
                    }
                    List<Article> articlesBasedOnProduct = productController.getArticlesBasedOnProduct(product.getOrganization().getUniqueId(),product.getUniqueId());
                    for(Article article : articlesBasedOnProduct){
                        UpdateArticleAndElasticIndex(article,changeCategoryOnProductAPI.getToCategory());
                        articleCount ++;
                    }


                }
                catch(Exception e)
                {
                throw validationMessageService.generateValidationException("error", "Något gick fel när produkterna skulle uppdateras");

                }

            }
            LOG.log( Level.INFO, "Reindexing of all products/articles is now complete. Reindexing has been done on " + changeCategoryOnProductAPI.getProducts().size() + " products and " + articleCount + "articles");
        }

        return true;
    }




    public boolean SaveProperty(CategorySpecificPropertyApiEXT categorySpecificPropertyApiEXT)throws HjalpmedelstjanstenValidationException{

        try{
            CategorySpecificProperty catProperty = CategorySpecificPropertyMapper.map(categorySpecificPropertyApiEXT);
            catProperty.setCategory(em.find(Category.class,categorySpecificPropertyApiEXT.getCategoryId()));

            List<CategorySpecificPropertyListValue> catproplist = CategorySpecificPropertyMapper.map(categorySpecificPropertyApiEXT.getValues(),catProperty);
            catProperty.setCategorySpecificPropertyListValues(catproplist);

            em.persist(catProperty);
        }
        catch(Exception e){

        }



        return true;
    }
    public boolean UpdateProperty(CategorySpecificPropertyApiEXT categorySpecificPropertyApiEXT)throws HjalpmedelstjanstenValidationException{
        try{
            CategorySpecificProperty catProperty = em.find(CategorySpecificProperty.class, categorySpecificPropertyApiEXT.getId());
            catProperty.setDescription(categorySpecificPropertyApiEXT.getDescription());
            catProperty.setName(categorySpecificPropertyApiEXT.getName());
            em.joinTransaction();
            em.merge(catProperty);

        }
        catch(Exception e){
            throw validationMessageService.generateValidationException("propertyupdate","Något gick fel.");
        }

        return true;
    }

    public boolean UpdatePropertyOrder(CategorySpecificPropertyApiEXT categorySpecificPropertyApiEXT)throws HjalpmedelstjanstenValidationException{

        try {
//            Query query = em.createNamedQuery(CategorySpecificProperty.SET_ORDER_INDEX).setParameter("uniqueId",1780).setParameter("orderIndex", categorySpecificPropertyApiEXT.getOrderIndex());
//            query.executeUpdate();


                    CategorySpecificProperty catProperty = em.find(CategorySpecificProperty.class, categorySpecificPropertyApiEXT.getId());
                    catProperty.setOrderIndex(categorySpecificPropertyApiEXT.getOrderIndex());
                    em.joinTransaction();
                    em.merge(catProperty);
        }
        catch(Exception e){
            throw validationMessageService.generateValidationException("propertyorderupdate","Något gick fel.");
        }

        return true;
    }



    public long SavePropertyListItem(CategorySpecificPropertyListValueAPIEXT categorySpecificPropertyListValueAPIEXT)throws HjalpmedelstjanstenValidationException
    {
        try
        {
            CategorySpecificPropertyListValue categorySpecificPropertyListValue = new CategorySpecificPropertyListValue();
            categorySpecificPropertyListValue.setCode(categorySpecificPropertyListValueAPIEXT.getValue());
            categorySpecificPropertyListValue.setValue(categorySpecificPropertyListValueAPIEXT.getValue());
            categorySpecificPropertyListValue.setCategorySpecificProperty(em.find(CategorySpecificProperty.class,categorySpecificPropertyListValueAPIEXT.getPropertyId()));
            em.persist((categorySpecificPropertyListValue));
            return categorySpecificPropertyListValue.getUniqueId();
        }
        catch(Exception e)
        {
            throw validationMessageService.generateValidationException("savepropertyItem","Något gick fel.");
        }
    }


    public boolean DeletePropertyItem(long propertyListItemId)throws HjalpmedelstjanstenValidationException
    {
        try
        {
            CategorySpecificPropertyListValue propertyItem = em.find(CategorySpecificPropertyListValue.class,propertyListItemId);
            StringBuilder queryBuilder = new StringBuilder("DELETE FROM RSPValueValueListMultipleCSPListValue WHERE categorySpecificPropertyListValueId = :propertyId");
            Query searchQuery = em.createNativeQuery(queryBuilder.toString());
            searchQuery.setParameter("propertyId", propertyListItemId);
            searchQuery.executeUpdate();

            StringBuilder queryBuilder2 = new StringBuilder("DELETE FROM ResourceSpecificPropertyValue WHERE singleCategorySpecificPropertyListValueId = :propertyId");
            Query searchQuery2 = em.createNativeQuery(queryBuilder2.toString());
            searchQuery2.setParameter("propertyId", propertyListItemId);
            searchQuery2.executeUpdate();

            em.joinTransaction();
            em.remove(propertyItem);
            return true;
        }
        catch(Exception e)
        {
            throw validationMessageService.generateValidationException("propertyItem","Något gick fel.");
        }

    }


    public boolean DeleteProperty(long propertyId)
    throws HjalpmedelstjanstenValidationException {
        try{

            CategorySpecificProperty property = em.find(CategorySpecificProperty.class,propertyId);
            List<ResourceSpecificPropertyValue> rsValueList = em.createNamedQuery(ResourceSpecificPropertyValue.FIND_RESOURCE_BY_PROPERTYID).
                    setParameter("propertyId",propertyId).
                    getResultList();

            if(rsValueList != null && rsValueList.size() > 0)
            {
                for(ResourceSpecificPropertyValue rsp : rsValueList){
                    em.remove(rsp);
                }
            }

            if(property != null)
            {
                em.joinTransaction();
                em.remove(property);
            }


            else{
                throw validationMessageService.generateValidationException("property","Hittar ingen egenskap med aktuellt id");
            }
        }
        catch(Exception e)
        {
            throw validationMessageService.generateValidationException("property","Något gick fel vid borttagande av kategorispecifik egenskap");
        }
        return true;
    }

    public boolean DeleteProperties(long propertyId)
            throws HjalpmedelstjanstenValidationException {
        try{

            CategorySpecificProperty property = em.find(CategorySpecificProperty.class,propertyId);
            List<ResourceSpecificPropertyValue> rsValueList = em.createNamedQuery(ResourceSpecificPropertyValue.FIND_RESOURCE_BY_PROPERTYID).
                    setParameter("propertyId",propertyId).
                    getResultList();

            if(rsValueList != null && rsValueList.size() > 0)
            {
                throw validationMessageService.generateValidationException("propertyerror","Finns värden som bygger på denna egenskap.");
            }

            if(property != null)
            {
                em.joinTransaction();
                em.remove(property);
            }


            else{
                throw validationMessageService.generateValidationException("property","Hittar ingen egenskap med aktuellt id");
            }
        }
        catch(Exception e)
        {
            throw validationMessageService.generateValidationException("property","Kan ej ta bort kategori för att det finns kopplade produkter till kategorin.");
        }
        return true;
    }

    public byte[] exportFileIsoCategory(UserAPI userAPI,
                                String sessionId, String uniqueId
    ) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "export( ... )");

        Session session = em.unwrap(Session.class);
        session.setHibernateFlushMode(FlushMode.MANUAL);

        DateTimeFormatter dateTimeFormatter =  DateTimeFormatter.ofPattern("yyyy-MM-dd");

        try (SXSSFWorkbook workbook = new SXSSFWorkbook()) {

            ZipSecureFile.setMinInflateRatio(0);

            workbook.getXSSFWorkbook().lockStructure();

            //sheet for the category information

            List<Category> category = this.getCategory(Long.valueOf(uniqueId));

            SXSSFSheet categorySheet = workbook.createSheet("Kategori " + category.get(0).getCode() );
            categorySheet.protectSheet(UUID.randomUUID().toString());
            categorySheet.lockFormatColumns(false);
            categorySheet.trackAllColumnsForAutoSizing();

            int categorySheetRowNum = 0;

            int categorySheetCellNumber = 0;

            // write headers for category-sheet
            SXSSFRow categoryHeaderRow = categorySheet.createRow(categorySheetRowNum++);

            categoryHeaderRow.createCell(categorySheetCellNumber++).setCellValue("Namn");
            categoryHeaderRow.createCell(categorySheetCellNumber++).setCellValue("Kod");
            categoryHeaderRow.createCell(categorySheetCellNumber++).setCellValue("Beskrivning");
            categoryHeaderRow.createCell(categorySheetCellNumber++).setCellValue("Artikeltyp");

            // add category to the categories sheet
            SXSSFRow categoryRow = categorySheet.createRow(categorySheetRowNum++);

            // fill the categorysheet with category information
            SXSSFCell categoryNameCell = categoryRow.createCell(0);
            categoryNameCell.setCellValue(category.get(0).getName());

            SXSSFCell categoryCodeCell = categoryRow.createCell(1);
            categoryCodeCell.setCellValue(category.get(0).getCode());

            SXSSFCell categoryDescriptionCell = categoryRow.createCell(2);
            categoryDescriptionCell.setCellValue(category.get(0).getDescription());

            SXSSFCell categoryArticleTypeCell = categoryRow.createCell(3);
            if (category.get(0).getArticleType() != null) {
                categoryArticleTypeCell.setCellValue(category.get(0).getArticleType().name());
            }

            // size columns
            for (int i = 0; i <= categoryHeaderRow.getLastCellNum(); i++) {
                categorySheet.autoSizeColumn(i);
            }
            categorySheet.trackAllColumnsForAutoSizing();

            //sheet for products

            List<ProductAPI> productsAPI = this.getProductsByCategory(Long.valueOf(uniqueId));

            if (productsAPI != null) {
                SXSSFSheet productSheet = workbook.createSheet("Produkter " + productsAPI.size() + " st");
                productSheet.protectSheet(UUID.randomUUID().toString());
                productSheet.lockFormatColumns(false);
                productSheet.trackAllColumnsForAutoSizing();

                int productSheetRowNum = 0;

                int productSheetCellNumber = 0;

                // write headers for product-sheet
                SXSSFRow productHeaderRow = productSheet.createRow(productSheetRowNum++);

                productHeaderRow.createCell(productSheetCellNumber++).setCellValue("Namn");
                productHeaderRow.createCell(productSheetCellNumber++).setCellValue("Produktnummer");
                productHeaderRow.createCell(productSheetCellNumber++).setCellValue("Skapad");
                productHeaderRow.createCell(productSheetCellNumber++).setCellValue("Utgången/ersatt");
                productHeaderRow.createCell(productSheetCellNumber++).setCellValue("Leverantör");
                productHeaderRow.createCell(productSheetCellNumber++).setCellValue("Status");

                for (ProductAPI product : productsAPI) {

                    // add product to the product sheet
                    SXSSFRow productRow = productSheet.createRow(productSheetRowNum++);

                    // fill the product sheet with product information
                    SXSSFCell productNameCell = productRow.createCell(0);
                    productNameCell.setCellValue(product.getProductName());

                    SXSSFCell productNumberCell = productRow.createCell(1);
                    productNumberCell.setCellValue(product.getProductNumber());

                    SXSSFCell productCreatedCell = productRow.createCell(2);
                    ZonedDateTime productCreatedZonedDateTime;
                    productCreatedZonedDateTime = Instant.ofEpochMilli(product.getCreated()).atZone(ZoneId.systemDefault());
                    String productCreated = dateTimeFormatter.format(productCreatedZonedDateTime);
                    productCreatedCell.setCellValue(productCreated);

                    SXSSFCell productReplacedCell = productRow.createCell(3);
                    if (product.getReplacementDate() != null) {
                        ZonedDateTime productReplacedZonedDateTime;
                        productReplacedZonedDateTime = Instant.ofEpochMilli(product.getReplacementDate()).atZone(ZoneId.systemDefault());
                        String productReplaced = dateTimeFormatter.format(productReplacedZonedDateTime);
                        productReplacedCell.setCellValue(productReplaced);
                    }

                    SXSSFCell productManufacturerCell = productRow.createCell(4);
                    productManufacturerCell.setCellValue(product.getOrganizationName());

                    SXSSFCell productStatusCell = productRow.createCell(5);
                    //productStatusCell.setCellValue(product.getStatus());
                    if (product.getStatus().equalsIgnoreCase("DISCONTINUED")) {
                        productStatusCell.setCellValue("Utgått");
                    } else if (product.getStatus().equalsIgnoreCase("PUBLISHED")) {
                        productStatusCell.setCellValue("Aktiv");
                    }


                    // size columns
                    for (int i = 0; i <= productHeaderRow.getLastCellNum(); i++) {
                        productSheet.autoSizeColumn(i);
                    }
                    productSheet.trackAllColumnsForAutoSizing();

                }

            }



            //sheet for articles

            List<ArticleAPI> articleAPIS = this.getArticlesByCategory(Long.valueOf(uniqueId));

            if (articleAPIS != null) {
                SXSSFSheet articleSheet = workbook.createSheet("Artiklar "+ articleAPIS.size() + " st");
                articleSheet.protectSheet(UUID.randomUUID().toString());
                articleSheet.lockFormatColumns(false);
                articleSheet.trackAllColumnsForAutoSizing();

                int articleSheetRowNum = 0;

                int articleSheetCellNumber = 0;

                // write headers for article-sheet
                SXSSFRow articleHeaderRow = articleSheet.createRow(articleSheetRowNum++);

                articleHeaderRow.createCell(articleSheetCellNumber++).setCellValue("Namn");
                articleHeaderRow.createCell(articleSheetCellNumber++).setCellValue("Artikelnummer");
                articleHeaderRow.createCell(articleSheetCellNumber++).setCellValue("Skapad");
                articleHeaderRow.createCell(articleSheetCellNumber++).setCellValue("Utgången/ersatt");
                articleHeaderRow.createCell(articleSheetCellNumber++).setCellValue("Leverantör");
                articleHeaderRow.createCell(articleSheetCellNumber++).setCellValue("Status");


                for (ArticleAPI article : articleAPIS) {

                    // add articles to the articles sheet
                    SXSSFRow articleRow = articleSheet.createRow(articleSheetRowNum++);

                    //fill the articles sheet with articles information
                    SXSSFCell articleNameCell = articleRow.createCell(0);
                    articleNameCell.setCellValue(article.getArticleName());

                    SXSSFCell articleNumberCell = articleRow.createCell(1);
                    articleNumberCell.setCellValue(article.getArticleNumber());

                    SXSSFCell articleCreatedCell = articleRow.createCell(2);
                    ZonedDateTime articleCreatedZonedDateTime;
                    articleCreatedZonedDateTime = Instant.ofEpochMilli(article.getCreated()).atZone(ZoneId.systemDefault());
                    String articleCreated = dateTimeFormatter.format(articleCreatedZonedDateTime);
                    articleCreatedCell.setCellValue(articleCreated);

                    SXSSFCell articleReplacedCell = articleRow.createCell(3);
                    if (article.getReplacementDate() != null) {
                        ZonedDateTime articleReplacedZonedDateTime;
                        articleReplacedZonedDateTime = Instant.ofEpochMilli(article.getCreated()).atZone(ZoneId.systemDefault());
                        String articleReplaced = dateTimeFormatter.format(articleReplacedZonedDateTime);
                        articleReplacedCell.setCellValue(articleReplaced);
                    }

                    SXSSFCell articleManufacturerCell = articleRow.createCell(4);
                    articleManufacturerCell.setCellValue(article.getOrganizationName());

                    SXSSFCell articleStatusCell = articleRow.createCell(5);
                    //articleStatusCell.setCellValue(article.getStatus());
                    if (article.getStatus().equalsIgnoreCase("DISCONTINUED")) {
                        articleStatusCell.setCellValue("Utgått");
                    } else if (article.getStatus().equalsIgnoreCase("PUBLISHED")) {
                        articleStatusCell.setCellValue("Aktiv");
                    }


                    // size columns
                    for (int i = 0; i <= articleHeaderRow.getLastCellNum(); i++) {
                        articleSheet.autoSizeColumn(i);
                    }
                    articleSheet.trackAllColumnsForAutoSizing();
                }

            }



            //sheet for categorySpecificProperties

            List<CategorySpecificPropertyAPI> categorySpecificPropertiesAPI = this.getCategoryPropertyAPIs(Long.valueOf(uniqueId));
            if (categorySpecificPropertiesAPI != null) {

                SXSSFSheet categorySpecificSheet = workbook.createSheet("Kategorispecifika egenskaper " + categorySpecificPropertiesAPI.size() + " st");
                categorySpecificSheet.protectSheet(UUID.randomUUID().toString());
                categorySpecificSheet.lockFormatColumns(false);
                categorySpecificSheet.trackAllColumnsForAutoSizing();

                int categorySpecificSheetRowNum = 0;

                int categorySpecificSheetcellNumber = 0;

                // write headers
                SXSSFRow categorySpecificHeaderRow = categorySpecificSheet.createRow(categorySpecificSheetRowNum++);

                categorySpecificHeaderRow.createCell(categorySpecificSheetcellNumber++).setCellValue("Namn");
                categorySpecificHeaderRow.createCell(categorySpecificSheetcellNumber++).setCellValue("Beskrivning");

                // sort the properties by orderIndex, and properties with orderIndex null should be placed last
                if (categorySpecificPropertiesAPI != null) {
                    Collections.sort(categorySpecificPropertiesAPI, (a, b) -> {
                        if (a.getOrderIndex() == null) {
                            return (b.getOrderIndex() == null) ? 0 : 1;
                        }
                        if (b.getOrderIndex() == null) {
                            return -1;
                        }
                        return a.getOrderIndex() - b.getOrderIndex();
                    });
                }

                for (CategorySpecificPropertyAPI property : categorySpecificPropertiesAPI) {

                    // add category to the categories sheet
                    SXSSFRow categorySpecificPropertyRow = categorySpecificSheet.createRow(categorySpecificSheetRowNum++);

                    // fill the categorysheet with category information
                    SXSSFCell categorySpecificPropertyNameCell = categorySpecificPropertyRow.createCell(0);
                    categorySpecificPropertyNameCell.setCellValue(property.getName());


                    SXSSFCell categorySpecificPropertyDescriptionCell = categorySpecificPropertyRow.createCell(1);
                    categorySpecificPropertyDescriptionCell.setCellValue(property.getDescription());

                    // size columns
                    for (int i = 0; i <= categorySpecificHeaderRow.getLastCellNum(); i++) {
                        categorySpecificSheet.autoSizeColumn(i);
                    }
                    categorySpecificSheet.trackAllColumnsForAutoSizing();
                }

            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            workbook.write(baos);


            return baos.toByteArray();
        } catch (IOException ex) {
            LOG.log(Level.WARNING, "Failed to export ISO-category to file", ex);

            throw validationMessageService.generateValidationException("export", "Kunde ej exportera ISO-kategori");
        }
    }

        public byte[] exportFileIso(UserAPI userAPI,
                                String sessionId, String selected
                               ) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "export( ... )");

        String select = selected;

        List<CategoryAPI> selectedList = new ArrayList<>();

        if (select.equals("all")) {
            selectedList = this.getAllCategories();
        } else if (select.equals("withCategorySpecificProperties")) {
            selectedList = this.getAllCategoriesWithCSP();
        } else {
            List<CategoryAPI> allCategories = this.getAllCategories();

            //ISO-koder med tillhörande information
            List<CategoryAPI> onlyIsoCategories = new ArrayList<>();

            //Nationella tillägg med tillhörande information
            List<CategoryAPI> onlyNationalAddons = new ArrayList<>();

            //sorting for onlyIsoCategories and onlyNationalAddons

            for (CategoryAPI cat : allCategories) {
                if (cat.getCode() != null) {
                    String code = cat.getCode();

                    if (cat.getCode().length() > 7) {
                        onlyNationalAddons.add(cat);
                    } else if (cat.getCode().startsWith("01") || cat.getCode().startsWith("02") || cat.getCode().startsWith("9")) {
                        onlyNationalAddons.add(cat);
                    } else {
                        if (code.length() > 2 && code.charAt(2) == '9') {
                            onlyNationalAddons.add(cat);

                        } else if (code.length() > 2 && code.charAt(2) == '0' && code.charAt(3) == '1') {
                            onlyNationalAddons.add(cat);

                        } else if (code.length() > 2 && code.charAt(2) == '0' && code.charAt(3) == '2') {
                            onlyNationalAddons.add(cat);

                        } else if (code.length() > 4 && code.charAt(4) == '9') {
                            onlyNationalAddons.add(cat);

                        } else if (code.length() > 4 && code.charAt(4) == '0' && code.charAt(5) == '1') {
                            onlyNationalAddons.add(cat);

                        } else if (code.length() > 4 && code.charAt(4) == '0' && code.charAt(5) == '2') {
                            onlyNationalAddons.add(cat);

                        } else {
                            onlyIsoCategories.add(cat);
                            }
                        }
                    }
                }
            if (select.equals("ISO")) {
                selectedList = onlyIsoCategories;
            } else if (select.equals("additions")) {
                selectedList = onlyNationalAddons;
            }
        }

        Session session = em.unwrap(Session.class);
        session.setHibernateFlushMode(FlushMode.MANUAL);

        try (SXSSFWorkbook workbook = new SXSSFWorkbook()) {

            ZipSecureFile.setMinInflateRatio(0);

            workbook.getXSSFWorkbook().lockStructure();

            SXSSFSheet sheet = workbook.createSheet(CATEGORIES_SHEET_NAME);
            sheet.protectSheet(UUID.randomUUID().toString());
            sheet.lockFormatColumns(false);
            sheet.trackAllColumnsForAutoSizing();

            int sheetRowNum = 0;

            int cellNumber = 0;

            // write headers
            SXSSFRow headerRow = sheet.createRow(sheetRowNum++);

            headerRow.createCell(cellNumber++).setCellValue("Namn");
            headerRow.createCell(cellNumber++).setCellValue("Kod");
            headerRow.createCell(cellNumber++).setCellValue("Beskrivning");

            headerRow.createCell(cellNumber++).setCellValue("Artikeltyp");

            for (CategoryAPI category : selectedList) {

                // add category to the categories sheet
                SXSSFRow categoryRow = sheet.createRow(sheetRowNum++);

                //fill the categorysheet with category information
                SXSSFCell categoryNameCell = categoryRow.createCell(0);
                categoryNameCell.setCellValue(category.getName());

                SXSSFCell categoryCodeCell = categoryRow.createCell(1);
                categoryCodeCell.setCellValue(category.getCode());

                SXSSFCell categoryDescriptionCell = categoryRow.createCell(2);
                categoryDescriptionCell.setCellValue(category.getDescription());


                if (category.getArticleType() != null) {
                    SXSSFCell categoryArticleTypeCell = categoryRow.createCell(3);
                    categoryArticleTypeCell.setCellValue(String.valueOf(category.getArticleType()));
                }


                // size columns
                for (int i = 0; i <= headerRow.getLastCellNum(); i++) {
                    sheet.autoSizeColumn(i);
                }
                sheet.trackAllColumnsForAutoSizing();
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            workbook.write(baos);

            LOG.log(Level.INFO, "Export categorys " + selected + " " + selectedList.size() );


            return baos.toByteArray();
        } catch (IOException ex) {
            LOG.log(Level.WARNING, "Failed to export ISO-categories to file", ex);

            throw validationMessageService.generateValidationException("export", "Kunde ej exportera ISO-kategorier");
        }
    }

    /**
     * Get external category groupings
     *
     * **/

    public List<ExternalCategoryGroupingAPI> getExternalCategories() {
        String sql = "SELECT * FROM hjmtj.ExternalCategoryGrouping WHERE parentId IS NULL;";
        List<ExternalCategoryGrouping> externalCategoryGrouping = em.createNativeQuery(sql, ExternalCategoryGrouping.class).getResultList();
        List<ExternalCategoryGroupingAPI> externalCategoryGroupingAPI = ExternalCategoryMapper.map(externalCategoryGrouping);
        return externalCategoryGroupingAPI;
    }

    public List<ExternalCategoryGroupingAPI> getExternalCategoryChildren(Long parentId) {
        String sql = "SELECT * FROM hjmtj.ExternalCategoryGrouping WHERE parentId =" + parentId;
        List<ExternalCategoryGrouping> externalCategoryGrouping = em.createNativeQuery(sql, ExternalCategoryGrouping.class).getResultList();
        List<ExternalCategoryGroupingAPI> externalCategoryGroupingAPI = ExternalCategoryMapper.map(externalCategoryGrouping);
        return externalCategoryGroupingAPI;
    }

    public SearchDTO getExternalCategoryToCategoryId(Long categoryId) {
        String sql = "SELECT * FROM hjmtj.ExternalCategoryGrouping ec WHERE ec.uniqueId IN (SELECT ec2.parentId FROM hjmtj.ExternalCategoryGrouping ec2 WHERE categoryId =" + categoryId + ")";

        List<ExternalCategoryGrouping> externalCategories = em.createNativeQuery(sql).getResultList();

        return new SearchDTO((long) externalCategories.size(), externalCategories);
    }

    public List<ExternalCategoryGroupingAPI> getExternalCategoriesForAssortment() {
        String sql = "SELECT * FROM hjmtj.ExternalCategoryGrouping WHERE parentId IS NOT NULL AND categoryId IS NULL";
        List<ExternalCategoryGrouping> externalCategoryGrouping = em.createNativeQuery(sql, ExternalCategoryGrouping.class).getResultList();
        List<ExternalCategoryGroupingAPI> externalCategoryGroupingAPI = ExternalCategoryMapper.map(externalCategoryGrouping);
        return externalCategoryGroupingAPI;
    }

    public ExternalCategoryGroupingAPI createExternalCategory(ExternalCategoryGroupingAPI ecgAPI) {

        ExternalCategoryGrouping ecg = new ExternalCategoryGrouping();

        //update entity if ExternalCategoryGroupingAPI has id
        if (ecgAPI.getId() != null) {
            String sql = "SELECT * FROM hjmtj.ExternalCategoryGrouping ecg WHERE ecg.uniqueId =" + ecgAPI.getId() + ";";
            List<ExternalCategoryGrouping> ecgList = em.createNativeQuery(sql, ExternalCategoryGrouping.class).getResultList();
            ecg = ecgList.get(0);

            ecg.setName(ecgAPI.getName());
            ecg.setDescription(ecgAPI.getDescription());
            if (ecgAPI.getParent() != null) {
                if (ecgAPI.getParent() != ecg.getParent().getUniqueId()) {
                    String sql1 = "SELECT * FROM hjmtj.ExternalCategoryGrouping ecg WHERE ecg.uniqueId =" + ecgAPI.getParent() + ";";
                    List<ExternalCategoryGrouping> parentList = em.createNativeQuery(sql1, ExternalCategoryGrouping.class).getResultList();
                    ecg.setParent(parentList.get(0));
                }
            }

            if (ecgAPI.getCategory() != null) {
                if (ecgAPI.getCategory() != ecg.getCategory().getUniqueId() && ecgAPI.getCategory() != null) {
                    String sql2 = "SELECT * FROM hjmtj.Category c WHERE c.uniqueId = " + ecgAPI.getCategory() + ";";
                    List<Category> categoryList = em.createNativeQuery(sql2, Category.class).getResultList();
                    ecg.setCategory(categoryList.get(0));
                }
            }

            em.joinTransaction();
            em.merge(ecg);
            return ExternalCategoryMapper.map(ecg);
        }
        //create new entity if ExternalCategoryGroupingAPI doesn't have id
        else {
            ecg.setName(ecgAPI.getName());
            ecg.setDisplayOrder(ecgAPI.getDisplayOrder());
            ecg.setDescription(ecgAPI.getDescription());
            if (ecgAPI.getParent() != null) {
                String sql1 = "SELECT * FROM hjmtj.ExternalCategoryGrouping ecg WHERE ecg.uniqueId =" + ecgAPI.getParent() + ";";
                List<ExternalCategoryGrouping> parentList = em.createNativeQuery(sql1, ExternalCategoryGrouping.class).getResultList();
                ecg.setParent(parentList.get(0));
            }
            if (ecgAPI.getCategory() != null) {
                String sql2 = "SELECT * FROM hjmtj.Category c WHERE c.uniqueId = " + ecgAPI.getCategory() + ";";
                List<Category> categoryList = em.createNativeQuery(sql2, Category.class).getResultList();
                ecg.setCategory(categoryList.get(0));
            }

            em.persist(ecg);
            LOG.log( Level.INFO, "ExternalCategory " + ecg.getName() + "created");
            return ExternalCategoryMapper.map(ecg);


        }

    }

    public boolean deleteExternalCategory(long uniqueId)
        throws HjalpmedelstjanstenValidationException{
        ExternalCategoryGrouping externalCategoryEntity = em.find(ExternalCategoryGrouping.class, uniqueId);

        //check if external category is parent to someone
        String sql = "SELECT * FROM hjmtj.ExternalCategoryGrouping ecg WHERE ecg.parentId = " + uniqueId + ";";
        List<ExternalCategoryGrouping> ecgList = em.createNativeQuery(sql, ExternalCategoryGrouping.class).getResultList();
        if(ecgList.size() > 0) {
            throw validationMessageService.generateValidationException("delete", "Kan ej ta bort 1177-kategori för att det finns underliggande kategorier.");
        }

        LOG.log( Level.INFO, "Deleting external category with uniqueId: " + uniqueId);
        em.joinTransaction();
        em.remove(externalCategoryEntity);

        return true;
    }

}
