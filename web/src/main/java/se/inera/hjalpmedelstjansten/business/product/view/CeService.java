package se.inera.hjalpmedelstjansten.business.product.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.product.controller.CeController;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEAPI;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;
import java.util.logging.Level;

/**
 * REST API for CE directives and standards
 *
 */
@Stateless
@Path("ce")
@Interceptors({ PerformanceLogInterceptor.class })
public class CeService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    private CeController ceController;

    /**
     * Get all CE directives and standards
     *
     * @return a <code>CeAPI</code> containing all standards and directives
     */
    @GET
    @SecuredService(permissions = {"ce:view"})
    public Response getAllCe() {
        LOG.log(Level.FINEST, "getAllCe()");
        CVCEAPI ceAPI = ceController.getAllCe();
        return Response.ok(ceAPI).build();
    }

}
