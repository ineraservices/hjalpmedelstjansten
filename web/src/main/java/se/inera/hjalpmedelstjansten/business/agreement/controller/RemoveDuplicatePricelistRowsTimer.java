package se.inera.hjalpmedelstjansten.business.agreement.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.clustering.ClusterController;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import jakarta.ejb.*;
import jakarta.inject.Inject;
import java.util.logging.Level;

/**
 * Timer for deleting duplicate pricelistrows (agreement and gp)
 * (Pricelistrows can accidentally be duplicated if user tries to import excel while another import is running...)
 *
 * @author Per Abrahamsson
 */
@Singleton
@Startup
@DependsOn("PropertyLoader")
public class RemoveDuplicatePricelistRowsTimer {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private boolean removeDuplicatePricelistRowsTimerEnabled;


    @Inject
    private String removeDuplicatePricelistRowsDateYear;

    @Inject
    private String removeDuplicatePricelistRowsDateMonth;

    @Inject
    private String removeDuplicatePricelistRowsDateDay;

    @Inject
    private String removeDuplicatePricelistRowsTimerHour;

    @Inject
    private String removeDuplicatePricelistRowsTimerMinute;

    @Inject
    private String removeDuplicatePricelistRowsTimerSecond;

    @Inject
    private AgreementPricelistRowController agreementPricelistRowControllerController;

    @Inject
    private ClusterController clusterController;

    @Resource
    private TimerService timerService;

    @Timeout
    private void schedule() {
        LOG.log( Level.FINEST, "schedule" );
        // attempt to get lock
        boolean lockReceived = clusterController.getLock(this.getClass().getName(), 30);
        if( lockReceived ) {
            LOG.log( Level.FINEST, "Received lock!" );
            long start = System.currentTimeMillis();
            agreementPricelistRowControllerController.removeDuplicatePricelistRows();
            long total = System.currentTimeMillis() - start;
            if( total > 60000 ) {
                LOG.log( Level.WARNING, "Scheduled job took: {0} ms which is more than 1 minute which is a long time, a developer should take a look at this.", new Object[] {total});
            } else {
                LOG.log( Level.INFO, "Scheduled job took: {0} ms.", new Object[] {total});
            }
            //release lock
            clusterController.releaseLock(this.getClass().getName());
        } else {
            LOG.log( Level.INFO, "Did not receive lock!" );
        }
    }

    @PostConstruct
    private void initialize() {
        LOG.log( Level.FINEST, "initialize()" );
        if( removeDuplicatePricelistRowsTimerEnabled ) {
            LOG.log( Level.FINEST, "Timer is ENABLED" );
            if (timerService.getTimers().isEmpty()) {
                String name = this.getClass().getName();
                TimerConfig configuration = new TimerConfig();
                configuration.setPersistent(false);
                configuration.setInfo(name);
                ScheduleExpression scheduleExpression = new ScheduleExpression();
                scheduleExpression.
                        year(removeDuplicatePricelistRowsDateYear).
                        month(removeDuplicatePricelistRowsDateMonth).
                        dayOfMonth(removeDuplicatePricelistRowsDateDay).
                        hour(removeDuplicatePricelistRowsTimerHour).
                        minute(removeDuplicatePricelistRowsTimerMinute).
                        second(removeDuplicatePricelistRowsTimerSecond);
                timerService.createCalendarTimer(scheduleExpression, configuration);
            }
        } else {
            LOG.log( Level.INFO, "Timer is NOT ENABLED" );
        }
    }
}
