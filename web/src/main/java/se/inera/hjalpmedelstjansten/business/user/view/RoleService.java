package se.inera.hjalpmedelstjansten.business.user.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.business.user.controller.RoleController;
import se.inera.hjalpmedelstjansten.model.api.RoleAPI;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Level;

/**
 * REST API for User functionality.
 *
 */
@Stateless
@Path("organizations/{organizationUniqueId}/roles")
@Interceptors({ PerformanceLogInterceptor.class })
public class RoleService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private RoleController roleController;

    /**
     * Get available roles for an organization
     *
     * @param organizationUniqueId the unique id of the organization
     * @return a list of <code>RoleAPI</code> suitable for organization
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
     */
    @GET
    @SecuredService(permissions = {"roles:view"})
    public Response getRolesForOrganization(
            @PathParam("organizationUniqueId") long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "getRolesForOrganization( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        List<RoleAPI> roleAPIs = roleController.getRolesForOrganization(organizationUniqueId);
        return Response.ok(roleAPIs).build();
    }

}
