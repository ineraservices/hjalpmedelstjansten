package se.inera.hjalpmedelstjansten.business.media.controller;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVDocumentTypeAPI;
import se.inera.hjalpmedelstjansten.model.api.media.*;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVDocumentType;
import se.inera.hjalpmedelstjansten.model.entity.media.*;

import jakarta.ejb.Stateless;
import jakarta.enterprise.event.Event;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;


@Stateless
public class MediaController {

    @Inject
    private HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    DocumentTypeController documentTypeController;

    @Inject
    ProductController productController;

    @Inject
    ArticleController articleController;

    @Inject
    FileUploadValidationController fileUploadValidationController;

    @Inject
    MediaUploadAmazonS3Controller uploadController;

    @Inject
    OrganizationController organizationController;

    @Inject
    Event<InternalAuditEvent> internalAuditEvent;

    @Inject
    String noMediaUploadConfiguredDefaultMediaUrl;

    private static final Map<String, String> CONTENT_TYPES = Map.of(
        "pdf", "application/pdf",
        "doc", "application/msword",
        "xls", "application/vnd.ms-excel",
        "docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

    public MediaAPI handleVideoUpload(long organizationUniqueId,
            Long productUniqueId,
            Long articleUniqueId,
            MultipartFormDataInput multipartFormDataInput,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "handleProductVideoUpload( organizationUniqueId: {0}, productUniqueId: {1} )", new Object[] {organizationUniqueId, productUniqueId} );
        Product product = null;
        Article article = null;
        if( productUniqueId != null ) {
            product = productController.getProduct(organizationUniqueId, productUniqueId, userAPI);
            if( product == null ) {
                throw validationMessageService.generateValidationException("product", "media.product.notExists");
            }
        } else {
            article = articleController.getArticle(organizationUniqueId, articleUniqueId, userAPI);
            if( article == null ) {
                throw validationMessageService.generateValidationException("article", "media.article.notExists");
            }
        }
        String description = getStringField(multipartFormDataInput.getFormDataMap(), "description");
        String alternativeText = getStringField(multipartFormDataInput.getFormDataMap(), "alternativeText");
        String url = getStringField(multipartFormDataInput.getFormDataMap(), "url");
        if( url == null || url.isEmpty() ) {
            throw validationMessageService.generateValidationException("url", "media.video.onlyurl");
        }
        MediaVideo mediaVideo = new MediaVideo();
        if( product != null ) {
            mediaVideo.setProduct(product);
        }
        mediaVideo.setDescription(description);
        mediaVideo.setAlternativeText(alternativeText);
        fileUploadValidationController.validateUrl(url);
        mediaVideo.setUrl(url);
        em.persist(mediaVideo);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, mediaVideo.getUniqueId(), requestIp));
        if( product != null ) {
            List<Article> articlesBasedOnProduct = productController.getArticlesBasedOnProduct(organizationUniqueId, productUniqueId);
            if( articlesBasedOnProduct != null && !articlesBasedOnProduct.isEmpty() ) {
                for( Article articleBasedOnProduct : articlesBasedOnProduct ) {
                    ArticleMediaVideo articleMediaVideo = new ArticleMediaVideo();
                    articleMediaVideo.setInherited(true);
                    articleMediaVideo.setMediaVideo(mediaVideo);
                    articleMediaVideo.setArticle(articleBasedOnProduct);
                    em.persist(articleMediaVideo);
                }
            }
            return MediaMapper.mapVideo(mediaVideo);
        } else {
            ArticleMediaVideo articleMediaVideo = new ArticleMediaVideo();
            articleMediaVideo.setInherited(false);
            articleMediaVideo.setMediaVideo(mediaVideo);
            articleMediaVideo.setArticle(article);
            em.persist(articleMediaVideo);
            return MediaMapper.mapArticleVideo(articleMediaVideo);
        }
    }

    public void handleMediaUpload(long organizationUniqueId,
            Long productId,
            Long articleId,
            MediaAPI mediaAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "handleMediaUpload( organizationUniqueId: {0}, productId: {1}, articleId: {2}, mediaAPI->id: {3} )", new Object[] {organizationUniqueId, productId, articleId, mediaAPI.getId()} );
        if( productId != null ) {
            Product product = productController.getProduct(organizationUniqueId, productId, userAPI);
            if( product == null ) {
                throw validationMessageService.generateValidationException("product", "media.product.notExists");
            }
            handleMediaUpload(product, null, mediaAPI, userAPI, sessionId, requestIp);
        } else {
            Article article = articleController.getArticle(organizationUniqueId, articleId, userAPI);
            if( article == null ) {
                throw validationMessageService.generateValidationException("article", "media.article.notExists");
            }
            handleMediaUpload(null, article, mediaAPI, userAPI, sessionId, requestIp);
        }
    }

    public void handleMediaUpload(Product product,
            Article article,
            MediaAPI mediaAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "handleMediaUpload( product->uniqueId: {0}, article->uniqueId: {1}, mediaAPI->id: {2} )", new Object[] {product == null ? null: product.getUniqueId(), article == null ? null: article.getUniqueId(), mediaAPI.getId()} );
        if( mediaAPI instanceof MediaImageAPI ) {
            MediaImageAPI mediaImageAPI = (MediaImageAPI) mediaAPI;
            if( product != null ) {
                handleProductImageUpload(product, mediaImageAPI, userAPI, sessionId, requestIp);
            } else {
                handleArticleImageUpload(article, mediaImageAPI, userAPI, sessionId, requestIp);
            }
        } else if( mediaAPI instanceof MediaDocumentAPI ) {
            MediaDocumentAPI mediaDocumentAPI = (MediaDocumentAPI) mediaAPI;
            if( product != null ) {
                handleProductDocumentUpload(product, mediaDocumentAPI, userAPI, sessionId, requestIp);
            } else {
                handleArticleDocumentUpload(article, mediaDocumentAPI, userAPI, sessionId, requestIp);
            }
        } else if( mediaAPI instanceof MediaVideoAPI ) {
            MediaVideoAPI mediaVideoAPI = (MediaVideoAPI) mediaAPI;
            if( product != null ) {
                handleProductVideoUpload(product, mediaVideoAPI, userAPI, sessionId, requestIp);
            } else {
                handleArticleVideoUpload(article, mediaVideoAPI, userAPI, sessionId, requestIp);
            }
        }
    }

    private void handleProductImageUpload(Product product,
            MediaImageAPI mediaImageAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "handleProductImageUpload( product->uniqueId: {0}, mediaImageAPI->uniqueId: {1} )", new Object[] {product.getUniqueId(), mediaImageAPI.getId()} );
        if( mediaImageAPI.getId() != null ) {
            // update
            MediaImage mediaImage = em.find(MediaImage.class, mediaImageAPI.getId());
            if(mediaImage != null) {
                if( !mediaImage.getUrl().equals(mediaImageAPI.getUrl()) ) {
                    LOG.log( Level.FINEST, "Image url has changed, must refetch." );
                    deleteProductMedia(product, mediaImage, userAPI, sessionId, requestIp);
                    addNewProductImage(product, mediaImageAPI, false, userAPI, sessionId, requestIp);
                } else {
                    mediaImage.setDescription(mediaImageAPI.getDescription());
                    mediaImage.setAlternativeText(mediaImageAPI.getAlternativeText());
                    internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, mediaImage.getUniqueId(), requestIp));
            }} else { addNewProductImage(product, mediaImageAPI, false, userAPI, sessionId, requestIp);}
        } else {
            addNewProductImage(product, mediaImageAPI, false, userAPI, sessionId, requestIp);
        }
    }

    private void handleArticleImageUpload(Article article,
            MediaImageAPI mediaImageAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "handleArticleImageUpload( article->uniqueId: {0}, mediaImageAPI->uniqueId: {1} )", new Object[] {article.getUniqueId(), mediaImageAPI.getId()} );
        if( mediaImageAPI.getId() != null ) {
            // update
            ArticleMediaImage articleMediaImage = em.find(ArticleMediaImage.class, mediaImageAPI.getId());

            if(articleMediaImage != null) {

            if( articleMediaImage.isInherited() ) {
                // remove inheritance
                deleteArticleMedia(article, articleMediaImage, userAPI, sessionId, requestIp);
                addNewArticleImage(article, mediaImageAPI, false, userAPI, sessionId, requestIp);
            } else {
                if( !articleMediaImage.getMediaImage().getUrl().equals(mediaImageAPI.getUrl()) ) {
                    LOG.log( Level.FINEST, "Main image url has changed, must refetch." );
                    deleteArticleMedia(article, articleMediaImage, userAPI, sessionId, requestIp);
                    addNewArticleImage(article, mediaImageAPI, false, userAPI, sessionId, requestIp);
                } else {
                    articleMediaImage.getMediaImage().setDescription(mediaImageAPI.getDescription());
                    articleMediaImage.getMediaImage().setAlternativeText(mediaImageAPI.getAlternativeText());
                    internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, articleMediaImage.getUniqueId(), requestIp));
                }
            }
            } else {addNewArticleImage(article, mediaImageAPI, false, userAPI, sessionId, requestIp); }

        } else {
            addNewArticleImage(article, mediaImageAPI, false, userAPI, sessionId, requestIp);
        }
    }

    private void handleProductDocumentUpload(Product product, MediaDocumentAPI mediaDocumentAPI, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "handleProductDocumentUpload( product->uniqueId: {0}, mediaDocumentAPI->uniqueId: {1} )", new Object[] {product.getUniqueId(), mediaDocumentAPI.getId()} );
        if( mediaDocumentAPI.getId() != null ) {
            // update
            MediaDocument mediaDocument = em.find(MediaDocument.class, mediaDocumentAPI.getId());
            if(mediaDocument != null) {
                if( !mediaDocument.getUrl().equals(mediaDocumentAPI.getUrl()) ) {
                    LOG.log( Level.FINEST, "Document url has changed, must refetch." );
                    deleteProductMedia(product, mediaDocument, userAPI, sessionId, requestIp);
                    addNewProductDocument(product, mediaDocumentAPI, userAPI, sessionId, requestIp);
                } else {
                    mediaDocument.setDescription(mediaDocumentAPI.getDescription());
                    if( mediaDocumentAPI.getDocumentType() != null ) {
                        CVDocumentType documentType = documentTypeController.findByValue(mediaDocumentAPI.getDocumentType().getValue());
                        mediaDocument.setDocumentType(documentType);
                    }
                    internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, mediaDocument.getUniqueId(), requestIp));
            } } else addNewProductDocument(product, mediaDocumentAPI, userAPI, sessionId, requestIp);

        } else {
            addNewProductDocument(product, mediaDocumentAPI, userAPI, sessionId, requestIp);
        }
    }

    private void handleArticleDocumentUpload(Article article,
            MediaDocumentAPI mediaDocumentAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "handleArticleDocumentUpload( article->uniqueId: {0}, mediaDocumentAPI->uniqueId: {1} )", new Object[] {article.getUniqueId(), mediaDocumentAPI.getId()} );
        if( mediaDocumentAPI.getId() != null ) {
            // update
            ArticleMediaDocument articleMediaDocument = em.find(ArticleMediaDocument.class, mediaDocumentAPI.getId());
            if(articleMediaDocument != null) {
                if( articleMediaDocument.isInherited() ) {
                    // remove inheritance
                    deleteArticleMedia(article, articleMediaDocument, userAPI, sessionId, requestIp);
                    addNewArticleDocument(article, mediaDocumentAPI, userAPI, sessionId, requestIp);
                } else {
                    if( !articleMediaDocument.getMediaDocument().getUrl().equals(mediaDocumentAPI.getUrl()) ) {
                        LOG.log( Level.FINEST, "Document url has changed, must refetch." );
                        deleteArticleMedia(article, articleMediaDocument, userAPI, sessionId, requestIp);
                        addNewArticleDocument(article, mediaDocumentAPI, userAPI, sessionId, requestIp);
                    } else {
                        articleMediaDocument.getMediaDocument().setDescription(mediaDocumentAPI.getDescription());
                        if( mediaDocumentAPI.getDocumentType() != null ) {
                            CVDocumentType documentType = documentTypeController.findByValue(mediaDocumentAPI.getDocumentType().getValue());
                            articleMediaDocument.getMediaDocument().setDocumentType(documentType);
                        }
                        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, articleMediaDocument.getUniqueId(), requestIp));
                    }
                } //else missing?
            } addNewArticleDocument(article, mediaDocumentAPI, userAPI, sessionId, requestIp);

        } else {
            addNewArticleDocument(article, mediaDocumentAPI, userAPI, sessionId, requestIp);
        }
    }

    private void handleProductVideoUpload(Product product, MediaVideoAPI mediaVideoAPI, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "handleProductVideoUpload( product->uniqueId: {0}, mediaVideoAPI->uniqueId: {1} )", new Object[] {product.getUniqueId(), mediaVideoAPI.getId()} );
        if( mediaVideoAPI.getId() != null ) {
            // update
            fileUploadValidationController.validateUrl(mediaVideoAPI.getUrl());
            MediaVideo mediaVideo = em.find(MediaVideo.class, mediaVideoAPI.getId());
            mediaVideo.setUrl(mediaVideoAPI.getUrl());
            mediaVideo.setDescription(mediaVideoAPI.getDescription());
            mediaVideo.setAlternativeText(mediaVideoAPI.getAlternativeText());
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, mediaVideo.getUniqueId(), requestIp));
        } else {
            addNewProductVideo(product, mediaVideoAPI, userAPI, sessionId, requestIp);
        }
    }

    private void handleArticleVideoUpload(Article article,
            MediaVideoAPI mediaVideoAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "handleArticleVideoUpload( article->uniqueId: {0}, mediaVideoAPI->uniqueId: {1} )", new Object[] {article.getUniqueId(), mediaVideoAPI.getId()} );
        if( mediaVideoAPI.getId() != null ) {
            // update
            ArticleMediaVideo articleMediaVideo = em.find(ArticleMediaVideo.class, mediaVideoAPI.getId());
            if( articleMediaVideo.isInherited() ) {
                // remove inheritance
                deleteArticleMedia(article, articleMediaVideo, userAPI, sessionId, requestIp);
                addNewArticleVideo(article, mediaVideoAPI, userAPI, sessionId, requestIp);
            } else {
                fileUploadValidationController.validateUrl(mediaVideoAPI.getUrl());
                articleMediaVideo.getMediaVideo().setUrl(mediaVideoAPI.getUrl());
                articleMediaVideo.getMediaVideo().setDescription(mediaVideoAPI.getDescription());
                articleMediaVideo.getMediaVideo().setAlternativeText(mediaVideoAPI.getAlternativeText());
                internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, articleMediaVideo.getUniqueId(), requestIp));
            }
        } else {
            addNewArticleVideo(article, mediaVideoAPI, userAPI, sessionId, requestIp);
        }
    }

    public MediaImageAPI handleMainImageUpload(Product product,
            MediaImageAPI mediaImageAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp ) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "handleMainImageUpload( product->uniqueId: {0} )", new Object[] {product.getUniqueId()} );
        if( mediaImageAPI.getId() != null ) {
            // update
            MediaImage mediaImage = em.find(MediaImage.class, mediaImageAPI.getId());
            if(mediaImage != null) {
                if( !mediaImage.getUrl().equals(mediaImageAPI.getUrl()) ) {
                    LOG.log( Level.FINEST, "Main image url has changed, must refetch." );
                    deleteProductMedia(product, mediaImage, userAPI, sessionId, requestIp);
                    return addNewProductImage(product, mediaImageAPI, true, userAPI, sessionId, requestIp);
                } else {
                    mediaImage.setDescription(mediaImageAPI.getDescription());
                    mediaImage.setAlternativeText(mediaImageAPI.getAlternativeText());
                    mediaImage.setUrl(mediaImageAPI.getUrl());
                    mediaImage.setOriginalUrl(mediaImageAPI.getOriginalUrl());
                    internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, mediaImage.getUniqueId(), requestIp));
                    return MediaMapper.mapImage(mediaImage);
                }
            } else {
                return addNewProductImage(product, mediaImageAPI, true, userAPI, sessionId, requestIp);
            }
        } else {
            return addNewProductImage(product, mediaImageAPI, true, userAPI, sessionId, requestIp);
        }
    }

    private MediaImageAPI addNewProductImage(Product product,
            MediaImageAPI mediaImageAPI,
            boolean mainImage,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "addNewProductImage( product->uniqueId: {0} )", new Object[] {product.getUniqueId()} );
        MediaImage mediaImage = new MediaImage();
        mediaImage.setProduct(product);
        mediaImage.setDescription(mediaImageAPI.getDescription());
        mediaImage.setAlternativeText(mediaImageAPI.getAlternativeText());
        mediaImage.setMainImage(mainImage);
        URL url = fileUploadValidationController.validateUrl(mediaImageAPI.getUrl());
        String fileName = fileUploadValidationController.getFileName(url);
        String fileEnding = fileUploadValidationController.getFileEnding(fileName);
        fileUploadValidationController.validateImageFileEnding(fileEnding, "url");
        String filePath = generateFilePath(product.getOrganization(), product, fileName);
        LOG.log( Level.FINEST, "filePath: {0}", new Object[] {filePath});
        String externalUrl = uploadController.uploadFileFromUrl(filePath, mediaImageAPI.getUrl());

        if( externalUrl != null && !externalUrl.isEmpty() ) {
            mediaImage.setUrl(externalUrl);
        } else {
            // for local development, configuring media upload should not be required in order to test functionality
            // therefore we allow developer to configure a default URL to set on all media
            if( noMediaUploadConfiguredDefaultMediaUrl != null && !noMediaUploadConfiguredDefaultMediaUrl.isEmpty() ) {
                mediaImage.setUrl(noMediaUploadConfiguredDefaultMediaUrl);
            }
        }
        mediaImage.setExternalKey(filePath);
        mediaImage.setOriginalUrl(mediaImageAPI.getUrl());
        mediaImage.setFileName(fileName);
        em.persist(mediaImage);
        inheritImageToArticles(product.getOrganization().getUniqueId(), product.getUniqueId(), mediaImage);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, mediaImage.getUniqueId(), requestIp));
        return MediaMapper.mapImage(mediaImage);
    }

    private MediaDocumentAPI addNewProductDocument(Product product,
            MediaDocumentAPI mediaDocumentAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "addNewProductDocument( product->uniqueId: {0} )", new Object[] {product.getUniqueId()} );
        MediaDocument mediaDocument = new MediaDocument();
        mediaDocument.setProduct(product);
        mediaDocument.setDescription(mediaDocumentAPI.getDescription());

        CVDocumentTypeAPI documentTypeAPI = mediaDocumentAPI.getDocumentType();
        CVDocumentType documentType = null;

        if(documentTypeAPI != null) {
             documentType = documentTypeController.findByValue(mediaDocumentAPI.getDocumentType().getValue());
        }

        mediaDocument.setDocumentType(documentType);
        URL url = fileUploadValidationController.validateUrl(mediaDocumentAPI.getUrl());
        String fileName = fileUploadValidationController.getFileName(url);
        String fileEnding = fileUploadValidationController.getFileEnding(fileName);
        fileUploadValidationController.validateDocumentFileEnding(fileEnding, "url");
        String filePath = generateFilePath(product.getOrganization(), product, fileName);
        LOG.log( Level.FINEST, "filePath: {0}", new Object[] {filePath});
        String externalUrl = uploadController.uploadFileFromUrl(filePath, mediaDocumentAPI.getUrl());
        if( externalUrl != null && !externalUrl.isEmpty() ) {
            mediaDocument.setUrl(externalUrl);
        } else {
            // for local development, configuring media upload should not be required in order to test functionality
            // therefore we allow developer to configure a default URL to set on all media
            if( noMediaUploadConfiguredDefaultMediaUrl != null && !noMediaUploadConfiguredDefaultMediaUrl.isEmpty() ) {
                mediaDocument.setUrl(noMediaUploadConfiguredDefaultMediaUrl);
            }
        }
        mediaDocument.setFileType(fileUploadValidationController.getDocumentFileType(fileEnding));
        mediaDocument.setExternalKey(filePath);
        mediaDocument.setOriginalUrl(mediaDocumentAPI.getUrl());
        mediaDocument.setFileName(fileName);
        em.persist(mediaDocument);
        inheritDocumentToArticles(product.getOrganization().getUniqueId(), product.getUniqueId(), mediaDocument);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, mediaDocument.getUniqueId(), requestIp));
        return MediaMapper.mapDocument(mediaDocument);
    }

    private MediaVideoAPI addNewProductVideo(Product product,
            MediaVideoAPI mediaVideoAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "addNewProductVideo( product->uniqueId: {0} )", new Object[] {product.getUniqueId()} );
        MediaVideo mediaVideo = new MediaVideo();
        mediaVideo.setProduct(product);
        mediaVideo.setDescription(mediaVideoAPI.getDescription());
        mediaVideo.setAlternativeText(mediaVideoAPI.getAlternativeText());
        fileUploadValidationController.validateUrl(mediaVideoAPI.getUrl());
        mediaVideo.setUrl(mediaVideoAPI.getUrl());
        em.persist(mediaVideo);
        inheritVideoToArticles(product.getOrganization().getUniqueId(), product.getUniqueId(), mediaVideo);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, mediaVideo.getUniqueId(), requestIp));
        return MediaMapper.mapVideo(mediaVideo);
    }

    public MediaImageAPI handleMainImageUpload(Article article,
            MediaImageAPI mediaImageAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "handleMainImageUpload( article->uniqueId: {0} )", new Object[] {article.getUniqueId()} );
        if( mediaImageAPI.getId() != null ) {
            // update
            ArticleMediaImage articleMediaImage = em.find(ArticleMediaImage.class, mediaImageAPI.getId());
            if(articleMediaImage != null) {
                if( articleMediaImage.isInherited() ) {
                    // remove inheritance
                    deleteArticleMedia(article, articleMediaImage, userAPI, sessionId, requestIp);
                    return addNewArticleImage(article, mediaImageAPI, true, userAPI, sessionId, requestIp);
                } else {
                    if( !articleMediaImage.getMediaImage().getUrl().equals(mediaImageAPI.getUrl()) ) {
                        LOG.log( Level.FINEST, "Main image url has changed, must refetch." );
                        deleteArticleMedia(article, articleMediaImage, userAPI, sessionId, requestIp);
                        return addNewArticleImage(article, mediaImageAPI, true, userAPI, sessionId, requestIp);
                    } else {
                        articleMediaImage.getMediaImage().setDescription(mediaImageAPI.getDescription());
                        articleMediaImage.getMediaImage().setAlternativeText(mediaImageAPI.getAlternativeText());
                        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, articleMediaImage.getUniqueId(), requestIp));
                        return MediaMapper.mapArticleImage(articleMediaImage);
                    }
                }
            } else {
                return addNewArticleImage(article, mediaImageAPI, true, userAPI, sessionId, requestIp);
            }
        } else {
            return addNewArticleImage(article, mediaImageAPI, true, userAPI, sessionId, requestIp);
        }
    }

    private MediaImageAPI addNewArticleImage(Article article,
            MediaImageAPI mediaImageAPI,
            boolean mainImage,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "addNewArticleImage( article->uniqueId: {0} )", new Object[] {article.getUniqueId()} );
        MediaImage mediaImage = new MediaImage();
        mediaImage.setDescription(mediaImageAPI.getDescription());
        mediaImage.setAlternativeText(mediaImageAPI.getAlternativeText());
        mediaImage.setMainImage(mainImage);
        URL url = fileUploadValidationController.validateUrl(mediaImageAPI.getUrl());
        String fileName = fileUploadValidationController.getFileName(url);
        String fileEnding = fileUploadValidationController.getFileEnding(fileName);
        fileUploadValidationController.validateImageFileEnding(fileEnding, "url");
        String filePath = generateFilePath(article.getOrganization(), article, fileName);
        LOG.log( Level.FINEST, "filePath: {0}", new Object[] {filePath});
        String externalUrl = uploadController.uploadFileFromUrl(filePath, mediaImageAPI.getUrl());
        if( externalUrl != null && !externalUrl.isEmpty() ) {
            mediaImage.setUrl(externalUrl);
        } else {
            // for local development, configuring media upload should not be required in order to test functionality
            // therefore we allow developer to configure a default URL to set on all media
            if( noMediaUploadConfiguredDefaultMediaUrl != null && !noMediaUploadConfiguredDefaultMediaUrl.isEmpty() ) {
                mediaImage.setUrl(noMediaUploadConfiguredDefaultMediaUrl);
            }
        }
        mediaImage.setExternalKey(filePath);
        mediaImage.setOriginalUrl(mediaImageAPI.getUrl());
        mediaImage.setFileName(fileName);
        em.persist(mediaImage);
        ArticleMediaImage articleMediaImage = new ArticleMediaImage();
        articleMediaImage.setInherited(false);
        articleMediaImage.setMediaImage(mediaImage);
        articleMediaImage.setArticle(article);
        em.persist(articleMediaImage);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, articleMediaImage.getUniqueId(), requestIp));
        return MediaMapper.mapArticleImage(articleMediaImage);
    }

    private MediaDocumentAPI addNewArticleDocument(Article article,
            MediaDocumentAPI mediaDocumentAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "addNewArticleDocument( article->uniqueId: {0} )", new Object[] {article.getUniqueId()} );
        MediaDocument mediaDocument = new MediaDocument();
        mediaDocument.setDescription(mediaDocumentAPI.getDescription());

        CVDocumentTypeAPI documentTypeAPI = mediaDocumentAPI.getDocumentType();
        CVDocumentType documentType = null;

        if(documentTypeAPI != null) {
            documentType = documentTypeController.findByValue(documentTypeAPI.getValue());
        }

        mediaDocument.setDocumentType(documentType);
        URL url = fileUploadValidationController.validateUrl(mediaDocumentAPI.getUrl());
        String fileName = fileUploadValidationController.getFileName(url);
        String fileEnding = fileUploadValidationController.getFileEnding(fileName);
        fileUploadValidationController.validateDocumentFileEnding(fileEnding, "url");
        String filePath = generateFilePath(article.getOrganization(), article, fileName);
        LOG.log( Level.FINEST, "filePath: {0}", new Object[] {filePath});
        String externalUrl = uploadController.uploadFileFromUrl(filePath, mediaDocumentAPI.getUrl());
        if( externalUrl != null && !externalUrl.isEmpty() ) {
            mediaDocument.setUrl(externalUrl);
        } else {
            // for local development, configuring media upload should not be required in order to test functionality
            // therefore we allow developer to configure a default URL to set on all media
            if( noMediaUploadConfiguredDefaultMediaUrl != null && !noMediaUploadConfiguredDefaultMediaUrl.isEmpty() ) {
                mediaDocument.setUrl(noMediaUploadConfiguredDefaultMediaUrl);
            }
        }
        mediaDocument.setFileType(fileUploadValidationController.getDocumentFileType(fileEnding));
        mediaDocument.setExternalKey(filePath);
        mediaDocument.setOriginalUrl(mediaDocumentAPI.getUrl());
        mediaDocument.setFileName(fileName);
        em.persist(mediaDocument);
        ArticleMediaDocument articleMediaDocument = new ArticleMediaDocument();
        articleMediaDocument.setInherited(false);
        articleMediaDocument.setMediaDocument(mediaDocument);
        articleMediaDocument.setArticle(article);
        em.persist(articleMediaDocument);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, articleMediaDocument.getUniqueId(), requestIp));
        return MediaMapper.mapArticleDocument(articleMediaDocument);
    }

    private MediaVideoAPI addNewArticleVideo(Article article,
            MediaVideoAPI mediaVideoAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "addNewArticleVideo( article->uniqueId: {0} )", new Object[] {article.getUniqueId()} );
        fileUploadValidationController.validateUrl(mediaVideoAPI.getUrl());
        MediaVideo mediaVideo = new MediaVideo();
        mediaVideo.setDescription(mediaVideoAPI.getDescription());
        mediaVideo.setAlternativeText(mediaVideoAPI.getAlternativeText());
        mediaVideo.setUrl(mediaVideoAPI.getUrl());
        em.persist(mediaVideo);
        ArticleMediaVideo articleMediaVideo = new ArticleMediaVideo();
        articleMediaVideo.setInherited(false);
        articleMediaVideo.setMediaVideo(mediaVideo);
        articleMediaVideo.setArticle(article);
        em.persist(articleMediaVideo);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, articleMediaVideo.getUniqueId(), requestIp));
        return MediaMapper.mapArticleVideo(articleMediaVideo);
    }

    @Transactional
    public MediaAPI handleImageUpload(long organizationUniqueId, Long productUniqueId, Long articleUniqueId, MultipartFormDataInput multipartFormDataInput, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "handleProductImageUpload( organizationUniqueId: {0}, productUniqueId: {1}, articleUniqueId: {2} )", new Object[] {organizationUniqueId, productUniqueId, articleUniqueId} );
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        Product product = null;
        Article article = null;
        if( productUniqueId != null ) {
            product = productController.getProduct(organizationUniqueId, productUniqueId, userAPI);
            if( product == null ) {
                throw validationMessageService.generateValidationException("product", "media.product.notExists");
            }
        } else {
            article = articleController.getArticle(organizationUniqueId, articleUniqueId, userAPI);
            if( article == null ) {
                throw validationMessageService.generateValidationException("article", "media.article.notExists");
            }
        }
        String description = getStringField(multipartFormDataInput.getFormDataMap(), "description");
        String urlParam = getStringField(multipartFormDataInput.getFormDataMap(), "url");
        List<InputPart> inputParts = multipartFormDataInput.getFormDataMap().get("file");
        String alternativeText = getStringField(multipartFormDataInput.getFormDataMap(), "alternativeText");
        String mainImage = getStringField(multipartFormDataInput.getFormDataMap(), "mainimage");
        MediaImage mediaImage = new MediaImage();
        if( product != null ) {
            mediaImage.setProduct(product);
        }
        mediaImage.setDescription(description);
        mediaImage.setAlternativeText(alternativeText);
        if( mainImage != null && mainImage.equals("true") ) {
            if( product != null ) {
                MediaImage oldMediaImage = getProductMainImage(product.getUniqueId());
                if (oldMediaImage != null) {
                    if ((urlParam == null || urlParam.isEmpty()) && inputParts.isEmpty()) {
                        mediaImage = oldMediaImage;
                        mediaImage.setDescription(description);
                        mediaImage.setAlternativeText(alternativeText);

                        em.persist(mediaImage);
                        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, mediaImage.getUniqueId(), requestIp));
                        return MediaMapper.mapImage(mediaImage);
                    } else {
                        deleteProductMainImage(product, userAPI, sessionId, requestIp);
                    }
                }
            } else {
                if( getArticleMainImage(article.getUniqueId()) != null ) {
                    throw validationMessageService.generateValidationException("mainimage", "media.article.image.mainExists");
                }
            }
            mediaImage.setMainImage(true);
        } else {
            mediaImage.setMainImage(false);
        }
        if( product != null ) {
            if( mediaImage.isMainImage() && alternativeText == null ) {
                throw validationMessageService.generateValidationException("mainImageAltText", "mainImage.altText.notNull");
            }
            if( mediaImage.isMainImage() && urlParam == null && (multipartFormDataInput.getFormDataMap().get("file") == null)) {
                throw validationMessageService.generateValidationException("mainImageUploadLink", "mainImage.uploadLink.notNull");
            }
        }

        if( urlParam != null && !urlParam.isEmpty() ) {
            LOG.log( Level.FINEST, "Url: {0}", new Object[] {urlParam} );
            URL url = fileUploadValidationController.validateUrl(urlParam);
            String fileName = fileUploadValidationController.getFileName(url);
            String fileEnding = fileUploadValidationController.getFileEnding(fileName);
            fileUploadValidationController.validateImageFileEnding(fileEnding, "url");
            String filePath;
            if( product != null ) {
                filePath = generateFilePath(organization, product, fileName);
            } else {
                filePath = generateFilePath(organization, article, fileName);
            }
            LOG.log( Level.FINEST, "filePath: {0}", new Object[] {filePath});
            String externalUrl = uploadController.uploadFileFromUrl(filePath, urlParam);
            if( externalUrl != null && !externalUrl.isEmpty() ) {
                mediaImage.setUrl(externalUrl);
            } else {
                // for local development, configuring media upload should not be required in order to test functionality
                // therefore we allow developer to configure a default URL to set on all media
                if( noMediaUploadConfiguredDefaultMediaUrl != null && !noMediaUploadConfiguredDefaultMediaUrl.isEmpty() ) {
                    mediaImage.setUrl(noMediaUploadConfiguredDefaultMediaUrl);
                }
            }
            mediaImage.setExternalKey(filePath);
            mediaImage.setOriginalUrl(urlParam);
            mediaImage.setFileName(fileName);
        } else {
            InputStream inputStream = null;
            try {
                inputParts = multipartFormDataInput.getFormDataMap().get("file");
                String contentDispositionHeader = inputParts.get(0).getHeaders().getFirst("Content-Disposition");
                String fileName = fileUploadValidationController.getFileName(contentDispositionHeader);
                String fileEnding = fileUploadValidationController.getFileEnding(fileName);
                fileUploadValidationController.validateImageFileEnding(fileEnding, "file");
                inputStream = inputParts.get(0).getBody(InputStream.class,null);
                String filePath;
                if( product != null ) {
                    filePath = generateFilePath(organization, product, fileName);
                } else {
                    filePath = generateFilePath(organization, article, fileName);
                }
                LOG.log( Level.FINEST, "filePath: {0}", new Object[] {filePath});
                String externalUrl = uploadController.uploadFile(filePath, inputStream, URLConnection.guessContentTypeFromName(filePath));
                mediaImage.setExternalKey(filePath);
                if( externalUrl != null && !externalUrl.isEmpty() ) {
                    mediaImage.setUrl(externalUrl);
                } else {
                    // for local development, configuring media upload should not be required in order to test functionality
                    // therefore we allow developer to configure a default URL to set on all media
                    if( noMediaUploadConfiguredDefaultMediaUrl != null && !noMediaUploadConfiguredDefaultMediaUrl.isEmpty() ) {
                        mediaImage.setUrl(noMediaUploadConfiguredDefaultMediaUrl);
                    }
                }
                mediaImage.setFileName(fileName);
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Failed to handle upload", ex);
                throw validationMessageService.generateValidationException("file", "media.upload.fail");
            } finally {
                try {
                    if( inputStream != null ) {
                        inputStream.close();
                    }
                } catch (IOException ex) {
                    LOG.log(Level.SEVERE, "Failed to close inputstream on upload", ex);
                }
            }
        }
        em.persist(mediaImage);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, mediaImage.getUniqueId(), requestIp));

        if( product != null && mediaImage.isMainImage()) {
            product.setMainImageDescription(mediaImage.getDescription());
            product.setMainImageAltText(mediaImage.getAlternativeText());
            product.setMainImageUrl(mediaImage.getUrl());
            if (mediaImage.getOriginalUrl() != null && !mediaImage.getOriginalUrl().isEmpty()) {
                product.setMainImageOriginalUrl(mediaImage.getOriginalUrl());
            } else {
                product.setMainImageOriginalUrl("");
            }
            inheritImageToArticles(organizationUniqueId, productUniqueId, mediaImage);
            return MediaMapper.mapImage(mediaImage);
        } else if( product != null && !mediaImage.isMainImage()) {
            inheritImageToArticles(organizationUniqueId, productUniqueId, mediaImage);
            return MediaMapper.mapImage(mediaImage);
        } else {
            ArticleMediaImage articleMediaImage = new ArticleMediaImage();
            articleMediaImage.setInherited(false);
            articleMediaImage.setMediaImage(mediaImage);
            articleMediaImage.setArticle(article);
            em.persist(articleMediaImage);
            return MediaMapper.mapArticleImage(articleMediaImage);
        }
    }

    private void inheritImageToArticles(long organizationUniqueId, long productUniqueId, MediaImage mediaImage) {
        List<Article> articlesBasedOnProduct = productController.getArticlesBasedOnProduct(organizationUniqueId, productUniqueId);
        if( articlesBasedOnProduct != null && !articlesBasedOnProduct.isEmpty() ) {
            for( Article articleBasedOnProduct : articlesBasedOnProduct ) {
                LOG.log( Level.FINEST, "Article {0} is based on product: {1}", new Object[] {articleBasedOnProduct.getUniqueId(), productUniqueId});
                if( mediaImage.isMainImage() ) {
                    // must check if main image already exist on article, if so
                    // no inheritance
                    if( getArticleMainImage(articleBasedOnProduct.getUniqueId()) != null) {
                        LOG.log( Level.FINEST, "Article: {0} already has a main image, will not inherit new product image", new Object[] {articleBasedOnProduct.getUniqueId()} );
                        continue;
                    }
                }
                ArticleMediaImage articleMediaImage = new ArticleMediaImage();
                articleMediaImage.setInherited(true);
                articleMediaImage.setMediaImage(mediaImage);
                articleMediaImage.setArticle(articleBasedOnProduct);
                em.persist(articleMediaImage);
            }
        }
    }

    private void inheritDocumentToArticles(long organizationUniqueId, long productUniqueId, MediaDocument mediaDocument) {
        List<Article> articlesBasedOnProduct = productController.getArticlesBasedOnProduct(organizationUniqueId, productUniqueId);
        if( articlesBasedOnProduct != null && !articlesBasedOnProduct.isEmpty() ) {
            for( Article articleBasedOnProduct : articlesBasedOnProduct ) {
                LOG.log( Level.FINEST, "Article {0} is based on product: {1}", new Object[] {articleBasedOnProduct.getUniqueId(), productUniqueId});
                ArticleMediaDocument articleMediaDocument = new ArticleMediaDocument();
                articleMediaDocument.setInherited(true);
                articleMediaDocument.setMediaDocument(mediaDocument);
                articleMediaDocument.setArticle(articleBasedOnProduct);
                em.persist(articleMediaDocument);
            }
        }
    }

    private void inheritVideoToArticles(long organizationUniqueId, long productUniqueId, MediaVideo mediaVideo) {
        List<Article> articlesBasedOnProduct = productController.getArticlesBasedOnProduct(organizationUniqueId, productUniqueId);
        if( articlesBasedOnProduct != null && !articlesBasedOnProduct.isEmpty() ) {
            for( Article articleBasedOnProduct : articlesBasedOnProduct ) {
                LOG.log( Level.FINEST, "Article {0} is based on product: {1}", new Object[] {articleBasedOnProduct.getUniqueId(), productUniqueId});
                ArticleMediaVideo articleMediaVideo = new ArticleMediaVideo();
                articleMediaVideo.setInherited(true);
                articleMediaVideo.setMediaVideo(mediaVideo);
                articleMediaVideo.setArticle(articleBasedOnProduct);
                em.persist(articleMediaVideo);
            }
        }
    }

    /*public MediaAPI updateMediaDocument(long organizationUniqueId, Long mediaUniqueId, Long productUniqueId, Long articleUniqueId, MultipartFormDataInput multipartFormDataInput, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "updateMediaDocument( organizationUniqueId: {0}, productUniqueId: {1} )", new Object[] {organizationUniqueId, productUniqueId} );
        //Organization organization = organizationController.getOrganization(organizationUniqueId);
        String description = getStringField(multipartFormDataInput.getFormDataMap(), "description");
        Long documentTypeLong = getLongField(multipartFormDataInput.getFormDataMap(), "documenttype");

        new MediaDocument();
        MediaDocument mediaDocument;
        if (productUniqueId != null) {
            mediaDocument = getProductMediaDocument(organizationUniqueId, mediaUniqueId, userAPI);
        }
        else {
            ArticleMediaDocument articleMediaDocument = getArticleMediaDocument(organizationUniqueId, mediaUniqueId, userAPI);
            mediaDocument = articleMediaDocument.getMediaDocument();
        }
        mediaDocument.setDescription(description);
        if( documentTypeLong != null ) {
            CVDocumentType documentType = documentTypeController.getDocumentType(documentTypeLong);
            if( documentType == null ) {
                throw validationMessageService.generateValidationException("documenttype", "media.invaliddocumenttype");
            }
            mediaDocument.setDocumentType(documentType);
        }
        em.persist(mediaDocument);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, mediaDocument.getUniqueId(), requestIp));
        return MediaMapper.mapDocument(mediaDocument);
    }

     */

    public MediaAPI handleDocumentUpload(long organizationUniqueId, Long productUniqueId, Long articleUniqueId, MultipartFormDataInput multipartFormDataInput, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "handleProductDocumentUpload( organizationUniqueId: {0}, productUniqueId: {1}, articleUniqueId: {2} )", new Object[] {organizationUniqueId, productUniqueId, articleUniqueId} );
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        Product product = null;
        Article article = null;
        if( productUniqueId != null ) {
            product = productController.getProduct(organizationUniqueId, productUniqueId, userAPI);
            if( product == null ) {
                throw validationMessageService.generateValidationException("product", "media.product.notExists");
            }
        } else {
            article = articleController.getArticle(organizationUniqueId, articleUniqueId, userAPI);
            if( article == null ) {
                throw validationMessageService.generateValidationException("article", "media.article.notExists");
            }
        }
        MediaDocument mediaDocument = new MediaDocument();
        if( product != null ) {
            mediaDocument.setProduct(product);
        }
        String description = getStringField(multipartFormDataInput.getFormDataMap(), "description");
        String urlParam = getStringField(multipartFormDataInput.getFormDataMap(), "url");
        Long documentTypeLong = getLongField(multipartFormDataInput.getFormDataMap(), "documenttype");
        if( documentTypeLong != null ) {
            CVDocumentType documentType = documentTypeController.getDocumentType(documentTypeLong);
            if( documentType == null ) {
                throw validationMessageService.generateValidationException("documenttype", "media.invaliddocumenttype");
            }
            mediaDocument.setDocumentType(documentType);
        }
        mediaDocument.setDescription(description);
        if( urlParam != null && !urlParam.isEmpty() ) {
            URL url = fileUploadValidationController.validateUrl(urlParam);
            String fileName = fileUploadValidationController.getFileName(url);
            String fileEnding = fileUploadValidationController.getFileEnding(fileName);
            fileUploadValidationController.validateDocumentFileEnding(fileEnding, "url");
            mediaDocument.setFileType(fileUploadValidationController.getDocumentFileType(fileEnding));
            String filePath;
            if( product != null ) {
                filePath = generateFilePath(organization, product, fileName);
            } else {
                filePath = generateFilePath(organization, article, fileName);
            }
            LOG.log( Level.FINEST, "filePath: {0}", new Object[] {filePath});
            String externalUrl = uploadController.uploadFileFromUrl(filePath, urlParam);
            if( externalUrl != null && !externalUrl.isEmpty() ) {
                mediaDocument.setUrl(externalUrl);
            } else {
                // for local development, configuring media upload should not be required in order to test functionality
                // therefore we allow developer to configure a default URL to set on all media
                if( noMediaUploadConfiguredDefaultMediaUrl != null && !noMediaUploadConfiguredDefaultMediaUrl.isEmpty() ) {
                    mediaDocument.setUrl(noMediaUploadConfiguredDefaultMediaUrl);
                }
            }
            mediaDocument.setExternalKey(filePath);
            mediaDocument.setOriginalUrl(urlParam);
            mediaDocument.setFileName(fileName);
        } else {
            InputStream inputStream = null;
            try {
                List<InputPart> inputParts = multipartFormDataInput.getFormDataMap().get("file");
                String contentDispositionHeader = inputParts.get(0).getHeaders().getFirst("Content-Disposition");
                String fileName = fileUploadValidationController.getFileName(contentDispositionHeader);
                String fileEnding = fileUploadValidationController.getFileEnding(fileName);
                fileUploadValidationController.validateDocumentFileEnding(fileEnding, "file");
                inputStream = inputParts.get(0).getBody(InputStream.class,null);
                String filePath;
                if( product != null ) {
                    filePath = generateFilePath(organization, product, fileName);
                } else {
                    filePath = generateFilePath(organization, article, fileName);
                }
                LOG.log( Level.FINEST, "filePath: {0}", new Object[] {filePath});
                String externalUrl = uploadController.uploadFile(filePath, inputStream, CONTENT_TYPES.get(fileEnding));
                mediaDocument.setExternalKey(filePath);
                if( externalUrl != null && !externalUrl.isEmpty() ) {
                    mediaDocument.setUrl(externalUrl);
                } else {
                    // for local development, configuring media upload should not be required in order to test functionality
                    // therefore we allow developer to configure a default URL to set on all media
                    if( noMediaUploadConfiguredDefaultMediaUrl != null && !noMediaUploadConfiguredDefaultMediaUrl.isEmpty() ) {
                        mediaDocument.setUrl(noMediaUploadConfiguredDefaultMediaUrl);
                    }
                }
                mediaDocument.setFileType(fileUploadValidationController.getDocumentFileType(fileEnding));
                mediaDocument.setFileName(fileName);
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Failed to handle upload", ex);
                throw validationMessageService.generateValidationException("file", "media.upload.fail");
            } finally {
                try {
                    if( inputStream != null ) {
                        inputStream.close();
                    }
                } catch (IOException ex) {
                    LOG.log(Level.SEVERE, "Failed to close inputstream on upload", ex);
                }
            }
        }
        em.persist(mediaDocument);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, mediaDocument.getUniqueId(), requestIp));
        if( product != null ) {
            List<Article> articlesBasedOnProduct = productController.getArticlesBasedOnProduct(organizationUniqueId, productUniqueId);
            if( articlesBasedOnProduct != null && !articlesBasedOnProduct.isEmpty() ) {
                for( Article articleBasedOnProduct : articlesBasedOnProduct ) {
                    ArticleMediaDocument articleMediaDocument = new ArticleMediaDocument();
                    articleMediaDocument.setInherited(true);
                    articleMediaDocument.setMediaDocument(mediaDocument);
                    articleMediaDocument.setArticle(articleBasedOnProduct);
                    em.persist(articleMediaDocument);
                }
            }
            return MediaMapper.mapDocument(mediaDocument);
        } else {
            ArticleMediaDocument articleMediaDocument = new ArticleMediaDocument();
            articleMediaDocument.setInherited(false);
            articleMediaDocument.setMediaDocument(mediaDocument);
            articleMediaDocument.setArticle(article);
            em.persist(articleMediaDocument);
            return MediaMapper.mapArticleDocument(articleMediaDocument);
        }
    }

    public MediaListAPI getProductMedia(long organizationUniqueId, long productUniqueId, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "getProductMedia( organizationUniqueId: {0}, productUniqueId: {1} )", new Object[] {organizationUniqueId, productUniqueId} );
        ProductAPI productAPI = productController.getProductAPI(organizationUniqueId, productUniqueId, userAPI, sessionId, requestIp);
        if( productAPI == null ) {
            return null;
        }
        List<MediaDocument> mediaDocuments = getProductMediaDocuments(productAPI.getId());
        List<MediaImage> mediaImages = getProductMediaImages(productAPI.getId());
        List<MediaVideo> mediaVideos = getProductMediaVideos(productAPI.getId());
        MediaListAPI mediaListAPI = MediaMapper.mapProductMedias(mediaDocuments,mediaVideos, mediaImages);
        return mediaListAPI;
    }

    public List<MediaDocument> getProductMediaDocuments( long productUniqueId ) {
        LOG.log( Level.FINEST, "getProductMediaDocuments( productUniqueId: {0} )", new Object[] {productUniqueId} );
        return em.createNamedQuery(MediaDocument.FIND_BY_PRODUCT).
                setParameter("productUniqueId", productUniqueId).
                getResultList();
    }

    public List<MediaImage> getProductMediaImages( long productUniqueId ) {
        LOG.log( Level.FINEST, "getProductMediaImages( productUniqueId: {0} )", new Object[] {productUniqueId} );
        return em.createNamedQuery(MediaImage.FIND_BY_PRODUCT).
                setParameter("productUniqueId", productUniqueId).
                getResultList();
    }

    public List<MediaVideo> getProductMediaVideos( long productUniqueId ) {
        LOG.log( Level.FINEST, "getProductMediaVideos( productUniqueId: {0} )", new Object[] {productUniqueId} );
        return em.createNamedQuery(MediaVideo.FIND_BY_PRODUCT).
                setParameter("productUniqueId", productUniqueId).
                getResultList();
    }

    public List<ArticleMediaDocument> getArticleMediaDocuments( long articleUniqueId ) {
        LOG.log( Level.FINEST, "getProductMediaDocuments( articleUniqueId: {1} )", new Object[] {articleUniqueId} );
        return em.createNamedQuery(ArticleMediaDocument.FIND_BY_ARTICLE).
                setParameter("articleUniqueId", articleUniqueId).
                getResultList();
    }

    public List<ArticleMediaImage> getArticleMediaImages( long articleUniqueId ) {
        LOG.log( Level.FINEST, "getArticleMediaImages( articleUniqueId: {0} )", new Object[] {articleUniqueId} );
        return em.createNamedQuery(ArticleMediaImage.FIND_BY_ARTICLE).
                setParameter("articleUniqueId", articleUniqueId).
                getResultList();
    }

    public List<ArticleMediaVideo> getArticleMediaVideos( long articleUniqueId ) {
        LOG.log( Level.FINEST, "getArticleMediaVideos( articleUniqueId: {0} )", new Object[] {articleUniqueId} );
        return em.createNamedQuery(ArticleMediaVideo.FIND_BY_ARTICLE).
                setParameter("articleUniqueId", articleUniqueId).
                getResultList();
    }

    public MediaListAPI getArticleMedia(long organizationUniqueId, long articleUniqueId, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "getArticleMedia( organizationUniqueId: {0}, articleUniqueId: {1} )", new Object[] {organizationUniqueId, articleUniqueId} );
        ArticleAPI articleAPI = articleController.getArticleAPI(organizationUniqueId, articleUniqueId, userAPI, sessionId, requestIp);
        if( articleAPI == null ) {
            return null;
        }
        List<ArticleMediaDocument> articleMediaDocuments = em.createNamedQuery(ArticleMediaDocument.FIND_BY_ARTICLE).
                setParameter("articleUniqueId", articleUniqueId).
                getResultList();
        List<ArticleMediaImage> articleMediaImages = em.createNamedQuery(ArticleMediaImage.FIND_BY_ARTICLE).
                setParameter("articleUniqueId", articleUniqueId).
                getResultList();
        List<ArticleMediaVideo> articleMediaVideos = em.createNamedQuery(ArticleMediaVideo.FIND_BY_ARTICLE).
                setParameter("articleUniqueId", articleUniqueId).
                getResultList();
        MediaListAPI mediaListAPI = MediaMapper.mapArticleMedias(articleMediaDocuments, articleMediaVideos, articleMediaImages);
        return mediaListAPI;
    }

    public void deleteProductMedia(long organizationUniqueId,
            long productUniqueId,
            long mediaUniqueId,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "deleteProductMedia( organizationUniqueId: {0}, productUniqueId: {1}, mediaUniqueId: {2} )", new Object[] {organizationUniqueId, productUniqueId, mediaUniqueId} );
        Product product = productController.getProduct(organizationUniqueId, productUniqueId, userAPI);
        if( product == null ) {
            throw validationMessageService.generateValidationException("product", "media.product.notExists");
        }
        Media media = getMediaById(mediaUniqueId);
        if( media == null ) {
            throw validationMessageService.generateValidationException("product", "media.delete.notExists");
        }
        deleteProductMedia(product, media, userAPI, sessionId, requestIp);
    }

    public void deleteProductMedia(Product product, Media media, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "deleteProductMedia( product -> uniqueId: {0}, media -> uniqueId: {1} )", new Object[] {product.getUniqueId(), media.getUniqueId()} );
        if( media instanceof MediaVideo ) {
            MediaVideo mediaVideo = ( MediaVideo ) media;
            List<ArticleMediaVideo> articleMediaVideos = em.createNamedQuery(ArticleMediaVideo.FIND_BY_MEDIAVIDEO).
                    setParameter("mediaUniqueId", media.getUniqueId()).
                    getResultList();
            if( articleMediaVideos != null && !articleMediaVideos.isEmpty() ) {
                for( ArticleMediaVideo articleMediaVideo : articleMediaVideos ) {
                    em.remove(articleMediaVideo);
                }
            }
            em.remove(mediaVideo);
        } else if( media instanceof MediaImage ) {
            MediaImage mediaImage = ( MediaImage ) media;
            List<ArticleMediaImage> articleMediaImages = em.createNamedQuery(ArticleMediaImage.FIND_BY_MEDIAIMAGE).
                    setParameter("mediaUniqueId", media.getUniqueId()).
                    getResultList();
            if( articleMediaImages != null && !articleMediaImages.isEmpty() ) {
                for( ArticleMediaImage articleMediaImage : articleMediaImages ) {
                    em.remove(articleMediaImage);
                }
            }
            String externalKey = mediaImage.getExternalKey();
            em.remove(mediaImage);
            uploadController.deleteFile(externalKey);
        } else if( media instanceof MediaDocument ) {
            MediaDocument mediaDocument = ( MediaDocument ) media;
            List<ArticleMediaDocument> articleMediaDocuments = em.createNamedQuery(ArticleMediaDocument.FIND_BY_MEDIADOCUMENT).
                    setParameter("mediaUniqueId", media.getUniqueId()).
                    getResultList();
            if( articleMediaDocuments != null && !articleMediaDocuments.isEmpty() ) {
                for( ArticleMediaDocument articleMediaDocument : articleMediaDocuments ) {
                    em.remove(articleMediaDocument);
                }
            }
            String externalKey = mediaDocument.getExternalKey();
            em.remove(mediaDocument);
            uploadController.deleteFile(externalKey);
        }
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.DELETE, userAPI.getId(), sessionId, media.getUniqueId(), requestIp));
    }

    public ArticleMediaImage getArticleMainImage(long articleUniqueId) {
        List<ArticleMediaImage> articleMediaMainImages = em.createNamedQuery(ArticleMediaImage.FIND_BY_ARTICLE_AND_MAIN).
                setParameter("articleUniqueId", articleUniqueId).
                getResultList();
        if( articleMediaMainImages != null && !articleMediaMainImages.isEmpty() ) {
            // there shall be only one
            return articleMediaMainImages.get(0);
        }
        return null;
    }

    public ArticleMediaImage getArticleMediaImage(long organizationUniqueId, long mediaUniqueId, UserAPI userAPI) {
        LOG.log( Level.FINEST, "getArticleMediaImage( organizationUniqueId: {0}, mediaUniqueId: {1} )", new Object[] {organizationUniqueId, mediaUniqueId} );
        ArticleMediaImage articleMediaImage = em.find(ArticleMediaImage.class, mediaUniqueId);
        if( articleMediaImage == null ) {
            return null;
        }
        if( !articleMediaImage.getArticle().getOrganization().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log(Level.WARNING, "attempt by user: {0} to get article media image with code: {1} which not in organization", new Object[] {userAPI.getId(), mediaUniqueId});
            return null;
        }
        return articleMediaImage;
    }

    public MediaImage getProductMediaImage(long organizationUniqueId, long mediaUniqueId, UserAPI userAPI) {
        LOG.log( Level.FINEST, "getProductMediaImage( mediaUniqueId: {0} )", new Object[] {mediaUniqueId} );
        MediaImage mediaImage = em.find(MediaImage.class, mediaUniqueId);
        if( mediaImage == null ) {
            return null;
        }
        if( !mediaImage.getProduct().getOrganization().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log(Level.WARNING, "attempt by user: {0} to get article media image with code: {1} which not in organization", new Object[] {userAPI.getId(), mediaUniqueId});
            return null;
        }
        return mediaImage;
    }

    public ArticleMediaDocument getArticleMediaDocument(long organizationUniqueId, long mediaUniqueId, UserAPI userAPI) {
        LOG.log( Level.FINEST, "getArticleMediaDocument( mediaUniqueId: {0} )", new Object[] {mediaUniqueId} );
        ArticleMediaDocument articleMediaDocument = em.find(ArticleMediaDocument.class, mediaUniqueId);
        if( articleMediaDocument == null ) {
            return null;
        }
        if( !articleMediaDocument.getArticle().getOrganization().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log(Level.WARNING, "attempt by user: {0} to get article media document with code: {1} which not in organization", new Object[] {userAPI.getId(), mediaUniqueId});
            return null;
        }
        return articleMediaDocument;
    }

    public MediaDocument getProductMediaDocument(long organizationUniqueId, long mediaUniqueId, UserAPI userAPI) {
        LOG.log( Level.FINEST, "getProductMediaDocument( mediaUniqueId: {0} )", new Object[] {mediaUniqueId} );
        MediaDocument mediaDocument = em.find(MediaDocument.class, mediaUniqueId);
        if( mediaDocument == null ) {
            return null;
        }
        if( !mediaDocument.getProduct().getOrganization().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log(Level.WARNING, "attempt by user: {0} to get product media document with code: {1} which not in organization", new Object[] {userAPI.getId(), mediaUniqueId});
            return null;
        }
        return mediaDocument;
    }

    public ArticleMediaVideo getArticleMediaVideo(long organizationUniqueId, long mediaUniqueId, UserAPI userAPI) {
        LOG.log( Level.FINEST, "getArticleMediaVideo( mediaUniqueId: {0} )", new Object[] {mediaUniqueId} );
        ArticleMediaVideo articleMediaVideo = em.find(ArticleMediaVideo.class, mediaUniqueId);
        if( articleMediaVideo == null ) {
            return null;
        }
        if( !articleMediaVideo.getArticle().getOrganization().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log(Level.WARNING, "attempt by user: {0} to get article media video with code: {1} which not in organization", new Object[] {userAPI.getId(), mediaUniqueId});
            return null;
        }
        return articleMediaVideo;
    }

    public MediaVideo getProductMediaVideo(long organizationUniqueId, long mediaUniqueId, UserAPI userAPI) {
        LOG.log( Level.FINEST, "getProductMediaVideo( mediaUniqueId: {0} )", new Object[] {mediaUniqueId} );
        MediaVideo mediaVideo = em.find(MediaVideo.class, mediaUniqueId);
        if( mediaVideo == null ) {
            return null;
        }
        if( !mediaVideo.getProduct().getOrganization().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log(Level.WARNING, "attempt by user: {0} to get product media video with code: {1} which not in organization", new Object[] {userAPI.getId(), mediaUniqueId});
            return null;
        }
        return mediaVideo;
    }

    public MediaImage getProductMainImage(long productUniqueId) {
        List<MediaImage> mediaMainImages = em.createNamedQuery(MediaImage.FIND_BY_PRODUCT_AND_MAIN).
                setParameter("productUniqueId", productUniqueId).
                getResultList();
        if( mediaMainImages != null && !mediaMainImages.isEmpty() ) {
            // there shall be only one
            return mediaMainImages.get(0);
        }
        return null;
    }

    private Media getMediaById(long mediaUniqueId) {
        MediaImage mediaImage = em.find(MediaImage.class, mediaUniqueId);
        if( mediaImage != null ) {
            return mediaImage;
        }
        MediaDocument mediaDocument = em.find(MediaDocument.class, mediaUniqueId);
        if( mediaDocument != null ) {
            return mediaDocument;
        }
        MediaVideo mediaVideo = em.find(MediaVideo.class, mediaUniqueId);
        return mediaVideo;
    }

    public void deleteArticleMainImage(Article article, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        ArticleMediaImage articleMediaImage = getArticleMainImage(article.getUniqueId());
        deleteArticleMedia(article, articleMediaImage, userAPI, sessionId, requestIp);
    }

    public void deleteProductMainImage(Product product, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        MediaImage mediaImage = getProductMainImage(product.getUniqueId());
        deleteProductMedia(product, mediaImage, userAPI, sessionId, requestIp);
    }

    public void deleteArticleMedia(Article article, ArticleMedia articleMedia, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "deleteArticleMedia( article->uniqueId: {0}, media->uniqueId: {1} )", new Object[] {article.getUniqueId(), articleMedia.getUniqueId()} );
        if( articleMedia instanceof ArticleMediaVideo ) {
            ArticleMediaVideo articleMediaVideo = ( ArticleMediaVideo ) articleMedia;
            em.remove(articleMediaVideo);
            if( !articleMediaVideo.isInherited() ) {
                em.remove(articleMediaVideo.getMediaVideo());
            }
        } else if( articleMedia instanceof ArticleMediaImage ) {
            ArticleMediaImage articleMediaImage = ( ArticleMediaImage ) articleMedia;
            em.remove(articleMediaImage);
            if( !articleMediaImage.isInherited() ) {
                String externalKey = articleMediaImage.getMediaImage().getExternalKey();
                em.remove(articleMediaImage.getMediaImage());
                uploadController.deleteFile(externalKey);
            }
        } else if( articleMedia instanceof ArticleMediaDocument ) {
            ArticleMediaDocument articleMediaDocument = ( ArticleMediaDocument ) articleMedia;
            em.remove(articleMediaDocument);
            if( !articleMediaDocument.isInherited() ) {
                String externalKey = articleMediaDocument.getMediaDocument().getExternalKey();
                em.remove(articleMediaDocument.getMediaDocument());
                uploadController.deleteFile(externalKey);
            }
        }
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.DELETE, userAPI.getId(), sessionId, articleMedia.getUniqueId(), requestIp));
    }

    public void deleteArticleMedia(long organizationUniqueId, long articleUniqueId, long mediaUniqueId, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "deleteArticleMedia( organizationUniqueId: {0}, articleUniqueId: {1}, mediaUniqueId: {2} )", new Object[] {organizationUniqueId, articleUniqueId, mediaUniqueId} );
        Article article = articleController.getArticle(organizationUniqueId, articleUniqueId, userAPI);
        if( article == null ) {
            throw validationMessageService.generateValidationException("article", "media.article.notExists");
        }
        ArticleMedia articleMedia = getArticleMediaById(mediaUniqueId);
        if( articleMedia == null ) {
            throw validationMessageService.generateValidationException("article", "media.delete.notExists");
        }
        deleteArticleMedia(article, articleMedia, userAPI, sessionId, requestIp);
    }

    public void deleteAllArticleMedia(long organizationUniqueId, long articleUniqueId, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        List<ArticleMediaImage> images = getArticleMediaImages(articleUniqueId);
        List<ArticleMediaDocument> documents = getArticleMediaDocuments(articleUniqueId);
        List<ArticleMediaVideo> videos = getArticleMediaVideos(articleUniqueId);
        for (ArticleMediaImage media : images) {
            deleteArticleMedia(organizationUniqueId, articleUniqueId, media.getUniqueId(), userAPI, sessionId, requestIp);
        }
        for (ArticleMediaDocument media : documents) {
            deleteArticleMedia(organizationUniqueId, articleUniqueId, media.getUniqueId(), userAPI, sessionId, requestIp);
        }
        for (ArticleMediaVideo media : videos) {
            deleteArticleMedia(organizationUniqueId, articleUniqueId, media.getUniqueId(), userAPI, sessionId, requestIp);
        }
    }

    public void inheritMediaFromProduct( Article article, Product product ) {
        List<MediaDocument> productMediaDocuments = getProductMediaDocuments(product.getUniqueId());
        if( productMediaDocuments != null && !productMediaDocuments.isEmpty() ) {
            for( MediaDocument mediaDocument : productMediaDocuments ) {
                ArticleMediaDocument articleMediaDocument = new ArticleMediaDocument();
                articleMediaDocument.setInherited(true);
                articleMediaDocument.setMediaDocument(mediaDocument);
                articleMediaDocument.setArticle(article);
                em.persist(articleMediaDocument);
            }
        }
        List<MediaImage> productMediaImages = getProductMediaImages(product.getUniqueId());
        if( productMediaImages != null && !productMediaImages.isEmpty() ) {
            for( MediaImage mediaImage : productMediaImages ) {
                ArticleMediaImage articleMediaImage = new ArticleMediaImage();
                articleMediaImage.setInherited(true);
                articleMediaImage.setMediaImage(mediaImage);
                articleMediaImage.setArticle(article);
                em.persist(articleMediaImage);
            }
        }
        List<MediaVideo> productMediaVideos = getProductMediaVideos(product.getUniqueId());
        if( productMediaVideos != null && !productMediaVideos.isEmpty() ) {
            for( MediaVideo mediaVideo : productMediaVideos ) {
                ArticleMediaVideo articleMediaVideo = new ArticleMediaVideo();
                articleMediaVideo.setInherited(true);
                articleMediaVideo.setMediaVideo(mediaVideo);
                articleMediaVideo.setArticle(article);
                em.persist(articleMediaVideo);
            }
        }
    }

    private ArticleMedia getArticleMediaById(long mediaUniqueId) {
        ArticleMediaImage articleMediaImage = em.find(ArticleMediaImage.class, mediaUniqueId);
        if( articleMediaImage != null ) {
            return articleMediaImage;
        }
        ArticleMediaDocument articleMediaDocument = em.find(ArticleMediaDocument.class, mediaUniqueId);
        if( articleMediaDocument != null ) {
            return articleMediaDocument;
        }
        ArticleMediaVideo articleMediaVideo = em.find(ArticleMediaVideo.class, mediaUniqueId);
        return articleMediaVideo;
    }

    private String getStringField(Map<String, List<InputPart>> inputPartsMap, String field) throws HjalpmedelstjanstenValidationException {
        try {
            List<InputPart> inputParts = inputPartsMap.get(field);
            if( inputParts != null ) {
                byte[] textBytes = inputParts.get(0).getBody(byte[].class,null);
                return new String(textBytes, StandardCharsets.UTF_8);
            }
        } catch (IOException ex) {
            throw validationMessageService.generateValidationException("file", "media.upload.fail");
        }
        return null;
    }

    private Long getLongField(Map<String, List<InputPart>> inputPartsMap, String field) throws HjalpmedelstjanstenValidationException {
        try {
            List<InputPart> inputParts = inputPartsMap.get(field);
            if( inputParts == null || inputParts.isEmpty() ) {
                return null;
            } else {
                return Long.valueOf(inputParts.get(0).getBody(String.class,null));
            }
        } catch (IOException ex) {
            throw validationMessageService.generateValidationException("file", "media.upload.fail");
        }
    }

    private String generateFilePath(Organization organization, Product product, String filename ) {
        return organization.getMediaFolderName() + "/" + product.getMediaFolderName() + "/" + filename;
    }

    private String generateFilePath(Organization organization, Article article, String filename ) {
        return organization.getMediaFolderName() + "/" + article.getMediaFolderName() + "/" + filename;
    }

    /*public MediaAPI updateMediaImage(long organizationUniqueId, Long mediaUniqueID, Long productUniqueId, Long articleUniqueId,
                                          MultipartFormDataInput multipartFormDataInput, UserAPI userAPI, String sessionId,
                                          String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateMediaImage( organizationUniqueId: {0}, productUniqueId: {1} )",
            new Object[] {organizationUniqueId, productUniqueId});

        String description = getStringField(multipartFormDataInput.getFormDataMap(), "description");
        String alternativeText = getStringField(multipartFormDataInput.getFormDataMap(), "alternativeText");
        new MediaImage();
        MediaImage mediaImage;

        if( productUniqueId != null ) {
            mediaImage = getProductMediaImage(organizationUniqueId, mediaUniqueID, userAPI);
        }
        else{
            ArticleMediaImage articleMediaImage = getArticleMediaImage(organizationUniqueId, mediaUniqueID, userAPI);
            mediaImage = articleMediaImage.getMediaImage();
        }
        mediaImage.setDescription(description);
        mediaImage.setAlternativeText(alternativeText);

        em.persist(mediaImage);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, mediaImage.getUniqueId(), requestIp));
        return MediaMapper.mapImage(mediaImage);
    }

     */

    public MediaAPI updateMedia(long organizationUniqueId, Long mediaUniqueId, Long productUniqueId, Long articleUniqueId, MultipartFormDataInput multipartFormDataInput, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "updateMediaDocument( organizationUniqueId: {0}, productUniqueId: {1} )", new Object[] {organizationUniqueId, productUniqueId} );

        String description = getStringField(multipartFormDataInput.getFormDataMap(), "description");



        new MediaDocument();
        MediaDocument mediaDocument;

        new MediaImage();
        MediaImage mediaImage;

        if (productUniqueId != null) {
            mediaDocument = getProductMediaDocument(organizationUniqueId, mediaUniqueId, userAPI);
            mediaImage = getProductMediaImage(organizationUniqueId, mediaUniqueId, userAPI);
        }
        else {
            ArticleMediaDocument articleMediaDocument = getArticleMediaDocument(organizationUniqueId, mediaUniqueId, userAPI);
            mediaDocument = articleMediaDocument != null ? articleMediaDocument.getMediaDocument() : null;

            ArticleMediaImage articleMediaImage = getArticleMediaImage(organizationUniqueId, mediaUniqueId, userAPI);
            mediaImage = articleMediaImage != null ? articleMediaImage.getMediaImage() : null;
        }

        if (mediaDocument != null) {
            mediaDocument.setDescription(description);
            Long documentTypeLong = getLongField(multipartFormDataInput.getFormDataMap(), "documenttype");
            if (documentTypeLong != null) {
                CVDocumentType documentType = documentTypeController.getDocumentType(documentTypeLong);
                if (documentType == null) {
                    throw validationMessageService.generateValidationException("documenttype", "media.invaliddocumenttype");
                }
                mediaDocument.setDocumentType(documentType);
            }
            em.persist(mediaDocument);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, mediaDocument.getUniqueId(), requestIp));
            return MediaMapper.mapDocument(mediaDocument);
        }
        else if (mediaImage != null) {
            mediaImage.setDescription(description);
            String alternativeText = getStringField(multipartFormDataInput.getFormDataMap(), "alternativeText");
            mediaImage.setAlternativeText(alternativeText);
            em.persist(mediaImage);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.MEDIA, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, mediaImage.getUniqueId(), requestIp));
            return MediaMapper.mapImage(mediaImage);
        }
        return null;
    }

}
