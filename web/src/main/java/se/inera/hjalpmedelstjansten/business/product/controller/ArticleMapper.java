package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.business.organization.controller.ElectronicAddressMapper;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.ResourceSpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.RoleAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.UserEngagementAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValue;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class for mapping between API and Entity classes
 *
 */
public class ArticleMapper {

    public static final List<ArticleAPI> map(List<Article> articles) {
        return map(articles, false, null);
    }

    public static final List<ArticleAPI> map(List<Article> articles, boolean includeEverything) {
        return map(articles, includeEverything, null);
    }

    public static final List<ArticleAPI> map(List<Article> articles, boolean includeEverything, List<CategorySpecificProperty> categorySpecificPropertys) {
        return map(articles, includeEverything, categorySpecificPropertys, null);
    }

    public static final List<ArticleAPI> map(List<Article> articles, boolean includeEverything, List<CategorySpecificProperty> categorySpecificPropertys, UserAPI userAPI) {
        if( articles == null ) {
            return null;
        }
        List<ArticleAPI> articleAPIs = new ArrayList<>();
        for( Article article : articles ) {
            articleAPIs.add(map(article, includeEverything, categorySpecificPropertys, userAPI));
        }
        return articleAPIs;
    }

    public static final ArticleAPI map(Article article, boolean includeEverything, List<CategorySpecificProperty> categorySpecificPropertys ) {
        return map(article, includeEverything, categorySpecificPropertys, null);
    }

    //quick and dirty
    public static final ArticleAPI mapFromInactivateOrganization(Article article, boolean includeEverything, List<CategorySpecificProperty> categorySpecificPropertys, UserAPI userAPI) {
        if (article == null) {
            return null;
        }
        ArticleAPI articleAPI = new ArticleAPI();
        articleAPI.setId(article.getUniqueId());
        articleAPI.setArticleName(article.getArticleName());
        articleAPI.setArticleNumber(article.getArticleNumber());
        articleAPI.setStatus(article.getStatus().toString());
        articleAPI.setOrganizationName(article.getBasedOnProduct() == null ? article.getOrganization().getOrganizationName() : article.getBasedOnProduct().getOrganization().getOrganizationName());
        articleAPI.setOrganizationId(article.getOrganization().getUniqueId());
        mapOrderUnit(articleAPI, article);
        //if( article.getBasedOnProduct() == null || article.getStatus() == Product.Status.DISCONTINUED) { HJAL-1825 removed one term.
        if (article.getBasedOnProduct() == null) {
            articleAPI.setCategory(CategoryMapper.map(article.getCategory()));
        } else {
            articleAPI.setCategory(CategoryMapper.map(article.getBasedOnProduct().getCategory()));
        }
        if (includeEverything) {
            articleAPI.setExtendedCategories(CategoryMapper.map(article.getExtendedCategories()));
            //articleAPI.setBasedOnProduct(ProductMapper.map(article.getBasedOnProduct(), false, null));
            articleAPI.setBasedOnProduct(ProductMapper.map(article.getBasedOnProduct(), true, null));
            articleAPI.setGtin(article.getGtin());
            boolean customerUnique = article.isCustomerUniqueOverridden() ? article.isCustomerUnique() : article.getBasedOnProduct() == null ? article.isCustomerUnique() : article.getBasedOnProduct().isCustomerUnique();
            articleAPI.setCustomerUnique(customerUnique);
            articleAPI.setReplacementDate(article.getReplacementDate() == null ? null : article.getReplacementDate().getTime());
            if (article.getReplacedByArticles() != null) {
                articleAPI.setReplacedByArticles(map(article.getReplacedByArticles(), false));
            }
            mapSupplementedInformation(articleAPI, article);
            mapCE(articleAPI, article);
            mapManufacturer(articleAPI, article);
            mapPreventiveMaintenance(articleAPI, article);
            mapOrderInformation(articleAPI, article);
            articleAPI.setFitsToArticles(map(article.getFitsToArticles()));
            articleAPI.setFitsToProducts(ProductMapper.map(article.getFitsToProducts()));

            // overridden

            articleAPI.setCustomerUniqueOverridden(article.isCustomerUniqueOverridden());

            articleAPI.setOrderInformationOverridden(article.isOrderInformationOverridden());

            articleAPI.setCeDirectiveOverridden(article.isCeDirectiveOverridden());
            articleAPI.setCeMarkedOverridden(article.isCeMarkedOverridden());
            articleAPI.setCeStandardOverridden(article.isCeStandardOverridden());

            articleAPI.setPreventiveMaintenanceDescriptionOverridden(article.isPreventiveMaintenanceDescriptionOverridden());
            articleAPI.setPreventiveMaintenanceNumberOfDaysOverridden(article.isPreventiveMaintenanceNumberOfDaysOverridden());
            articleAPI.setPreventiveMaintenanceValidFromOverridden(article.isPreventiveMaintenanceValidFromOverridden());

            articleAPI.setManufacturerArticleNumberOverridden(article.isManufacturerArticleNumberOverridden());
            articleAPI.setManufacturerElectronicAddressOverridden(article.isManufacturerElectronicAddressOverridden());
            articleAPI.setManufacturerOverridden(article.isManufacturerOverridden());
            articleAPI.setTrademarkOverridden(article.isTrademarkOverridden());

            articleAPI.setColorOverridden(article.isColorOverridden());
            articleAPI.setSupplementedInformationOverridden(article.isSupplementedInformationOverridden());

            articleAPI.setStatusOverridden(article.isStatusOverridden());


            // category specific properties
            setCategoryProperties(article, articleAPI, categorySpecificPropertys);

            articleAPI.setCreated(article.getCreated() == null ? null : article.getCreated().getTime());
            articleAPI.setUpdated(article.getLastUpdated() == null ? null : article.getLastUpdated().getTime());

            // only the supplier on which the article belongs to can see inactivateRowsOnReplacement
            // även Superadmin måste få sätta inactivateRowsOnReplacement (vid inaktivering av organisation)
            boolean isSuperAdmin = false;
            if (userAPI != null) {
                List<UserEngagementAPI> userEngagementAPIs = userAPI.getUserEngagements();
                for (UserEngagementAPI userEngagementAPI : userEngagementAPIs) {
                    List<RoleAPI> roleAPIs = userEngagementAPI.getRoles();
                    for (RoleAPI roleAPI : roleAPIs) {
                        if (roleAPI.getName().equalsIgnoreCase(UserRole.RoleName.Superadmin.toString())) {
                            isSuperAdmin = true;
                            break;
                        }
                    }
                }
            }
            if (isSuperAdmin || (userAPI != null && UserController.getUserEngagementAPIByOrganizationId(article.getOrganization().getUniqueId(), userAPI.getUserEngagements()) != null)) {
                articleAPI.setInactivateRowsOnReplacement(article.isInactivateRowsOnReplacement());
            }
        }
        return articleAPI;
    }


    public static final ArticleAPI map(Article article, boolean includeEverything, List<CategorySpecificProperty> categorySpecificPropertys, UserAPI userAPI ) {
        if( article == null ) {
            return null;
        }
        ArticleAPI articleAPI = new ArticleAPI();
        articleAPI.setId(article.getUniqueId());
        articleAPI.setArticleName(article.getArticleName());
        articleAPI.setArticleNumber(article.getArticleNumber());
        articleAPI.setStatus(article.getStatus().toString());
        articleAPI.setOrganizationName(article.getBasedOnProduct() == null ? article.getOrganization().getOrganizationName(): article.getBasedOnProduct().getOrganization().getOrganizationName());
        articleAPI.setOrganizationId(article.getOrganization().getUniqueId());
        articleAPI.setBasedOnProduct(ProductMapper.map(article.getBasedOnProduct(), false, null));
        mapOrderUnit(articleAPI, article);
        //if( article.getBasedOnProduct() == null || article.getStatus() == Product.Status.DISCONTINUED) { HJAL-1825 removed one term.
        if( article.getBasedOnProduct() == null) {
            articleAPI.setCategory(CategoryMapper.map(article.getCategory()));
        } else {
            articleAPI.setCategory(CategoryMapper.map(article.getBasedOnProduct().getCategory()));
        }
        if( includeEverything ) {
            articleAPI.setExtendedCategories(CategoryMapper.map(article.getExtendedCategories()));
            //articleAPI.setBasedOnProduct(ProductMapper.map(article.getBasedOnProduct(), false, null));
            articleAPI.setGtin(article.getGtin());
            boolean customerUnique = article.isCustomerUniqueOverridden() ? article.isCustomerUnique(): article.getBasedOnProduct() == null ? article.isCustomerUnique(): article.getBasedOnProduct().isCustomerUnique();
            articleAPI.setCustomerUnique(customerUnique);
            articleAPI.setReplacementDate(article.getReplacementDate() == null ? null: article.getReplacementDate().getTime());
            if( article.getReplacedByArticles() != null ) {
                articleAPI.setReplacedByArticles(map(article.getReplacedByArticles(), false));
            }
            mapSupplementedInformation(articleAPI, article);
            mapCE(articleAPI, article);
            mapManufacturer(articleAPI, article);
            mapPreventiveMaintenance(articleAPI, article);
            mapOrderInformation(articleAPI, article);
            articleAPI.setFitsToArticles(map(article.getFitsToArticles(), true));
            articleAPI.setFitsToProducts(ProductMapper.map(article.getFitsToProducts(), true, null));

            // overridden

            articleAPI.setCustomerUniqueOverridden(article.isCustomerUniqueOverridden());

            articleAPI.setOrderInformationOverridden(article.isOrderInformationOverridden());

            articleAPI.setCeDirectiveOverridden(article.isCeDirectiveOverridden());
            articleAPI.setCeMarkedOverridden(article.isCeMarkedOverridden());
            articleAPI.setCeStandardOverridden(article.isCeStandardOverridden());

            articleAPI.setPreventiveMaintenanceDescriptionOverridden(article.isPreventiveMaintenanceDescriptionOverridden());
            articleAPI.setPreventiveMaintenanceNumberOfDaysOverridden(article.isPreventiveMaintenanceNumberOfDaysOverridden());
            articleAPI.setPreventiveMaintenanceValidFromOverridden(article.isPreventiveMaintenanceValidFromOverridden());

            articleAPI.setManufacturerArticleNumberOverridden(article.isManufacturerArticleNumberOverridden());
            articleAPI.setManufacturerElectronicAddressOverridden(article.isManufacturerElectronicAddressOverridden());
            articleAPI.setManufacturerOverridden(article.isManufacturerOverridden());
            articleAPI.setTrademarkOverridden(article.isTrademarkOverridden());

            articleAPI.setColorOverridden(article.isColorOverridden());
            articleAPI.setSupplementedInformationOverridden(article.isSupplementedInformationOverridden());

            articleAPI.setStatusOverridden(article.isStatusOverridden());


            // category specific properties
            setCategoryProperties( article, articleAPI, categorySpecificPropertys );

            articleAPI.setCreated(article.getCreated() == null ? null : article.getCreated().getTime());
            articleAPI.setUpdated(article.getLastUpdated() == null ? null : article.getLastUpdated().getTime());

            // only the supplier on which the article belongs to can see inactivateRowsOnReplacement
            // även Superadmin måste få sätta inactivateRowsOnReplacement (vid inaktivering av organisation)
            boolean isSuperAdmin = false;
            if( userAPI != null) {
                List<UserEngagementAPI> userEngagementAPIs = userAPI.getUserEngagements();
                for (UserEngagementAPI userEngagementAPI: userEngagementAPIs){
                    List<RoleAPI> roleAPIs = userEngagementAPI.getRoles();
                    for (RoleAPI roleAPI : roleAPIs){
                        if (roleAPI.getName().equalsIgnoreCase(UserRole.RoleName.Superadmin.toString())) {
                            isSuperAdmin = true;
                            break;
                        }
                    }
                }
            }
            if( isSuperAdmin || (userAPI != null && UserController.getUserEngagementAPIByOrganizationId( article.getOrganization().getUniqueId(), userAPI.getUserEngagements()) != null )) {
                articleAPI.setInactivateRowsOnReplacement(article.isInactivateRowsOnReplacement());
            }
        }
        return articleAPI;
    }

    public static final Article map(ArticleAPI articleAPI) {
        if( articleAPI == null ) {
            return null;
        }
        Article article = new Article();
        article.setArticleName(articleAPI.getArticleName());
        article.setArticleNumber(articleAPI.getArticleNumber().trim());
        article.setStatus(Product.Status.valueOf(articleAPI.getStatus()));
        article.setGtin(articleAPI.getGtin());
        return article;
    }

    public static void fix(ArticleAPI articleAPI) {
        if( articleAPI.getArticleNumber() != null ) {
            articleAPI.setArticleNumber(articleAPI.getArticleNumber().trim());
        }
    }

    private static void mapSupplementedInformation( ArticleAPI articleAPI, Article article ) {
        Product product = article.getBasedOnProduct();

        if (product != null && article.getStatus() != Product.Status.DISCONTINUED) {
            articleAPI.setSupplementedInformation(article.isSupplementedInformationOverridden() ?
                    article.getSupplementedInformation() : product.getSupplementedInformation());
            articleAPI.setColor(article.isColorOverridden() ? article.getColor(): product.getColor());
        } else {
            articleAPI.setSupplementedInformation(article.getSupplementedInformation());
            articleAPI.setColor(article.getColor());
        }
    }

    private static void mapCE(ArticleAPI articleAPI, Article article) {
        if( article.getBasedOnProduct() != null && article.getStatus() != Product.Status.DISCONTINUED) {
            articleAPI.setCeMarked( article.isCeMarkedOverridden() ? article.isCeMarked(): article.getBasedOnProduct().isCeMarked());
            articleAPI.setCeDirective( article.isCeDirectiveOverridden() ? ProductMapper.mapCEDirective(article.getCeDirective()): ProductMapper.mapCEDirective(article.getBasedOnProduct().getCeDirective()));
            articleAPI.setCeStandard( article.isCeStandardOverridden() ? ProductMapper.mapCEStandard(article.getCeStandard()): ProductMapper.mapCEStandard(article.getBasedOnProduct().getCeStandard()));
        } else {
            articleAPI.setCeMarked(article.isCeMarked());
            articleAPI.setCeDirective(ProductMapper.mapCEDirective(article.getCeDirective()));
            articleAPI.setCeStandard(ProductMapper.mapCEStandard(article.getCeStandard()));
        }

    }

    private static void mapManufacturer(ArticleAPI articleAPI, Article article) {
        if( article.getBasedOnProduct() != null && article.getStatus() != Product.Status.DISCONTINUED ) {
            articleAPI.setManufacturer(article.isManufacturerOverridden() ? article.getManufacturer(): article.getBasedOnProduct().getManufacturer());
            articleAPI.setManufacturerArticleNumber(article.isManufacturerArticleNumberOverridden() ? article.getManufacturerArticleNumber(): article.getBasedOnProduct().getManufacturerProductNumber());
            articleAPI.setTrademark(article.isTrademarkOverridden()? article.getTrademark(): article.getBasedOnProduct().getTrademark());
            articleAPI.setManufacturerElectronicAddress(article.isManufacturerElectronicAddressOverridden() ? ElectronicAddressMapper.map(article.getManufacturerElectronicAddress()): ElectronicAddressMapper.map(article.getBasedOnProduct().getManufacturerElectronicAddress()));
        } else {
            articleAPI.setManufacturer(article.getManufacturer());
            articleAPI.setManufacturerArticleNumber(article.getManufacturerArticleNumber());
            articleAPI.setTrademark(article.getTrademark());
            articleAPI.setManufacturerElectronicAddress(ElectronicAddressMapper.map(article.getManufacturerElectronicAddress()));
        }
    }

    private static void mapPreventiveMaintenance(ArticleAPI articleAPI, Article article) {
        if( article.getBasedOnProduct() != null && article.getStatus() != Product.Status.DISCONTINUED ) {
            articleAPI.setPreventiveMaintenanceValidFrom(article.isPreventiveMaintenanceValidFromOverridden() ? PreventiveMaintenanceMapper.map(article.getPreventiveMaintenanceValidFrom()): PreventiveMaintenanceMapper.map(article.getBasedOnProduct().getPreventiveMaintenanceValidFrom()));
            articleAPI.setPreventiveMaintenanceDescription(article.isPreventiveMaintenanceDescriptionOverridden() ? article.getPreventiveMaintenanceDescription(): article.getBasedOnProduct().getPreventiveMaintenanceDescription());
            articleAPI.setPreventiveMaintenanceNumberOfDays(article.isPreventiveMaintenanceNumberOfDaysOverridden() ? article.getPreventiveMaintenanceNumberOfDays(): article.getBasedOnProduct().getPreventiveMaintenanceNumberOfDays());
        } else {
            articleAPI.setPreventiveMaintenanceValidFrom(PreventiveMaintenanceMapper.map(article.getPreventiveMaintenanceValidFrom()));
            articleAPI.setPreventiveMaintenanceDescription(article.getPreventiveMaintenanceDescription());
            articleAPI.setPreventiveMaintenanceNumberOfDays(article.getPreventiveMaintenanceNumberOfDays());
        }
    }

    /**
     * Separated from mapOrderInformation since this information is need when mapping
     * pricelist rows on agreements
     *
     * @param articleAPI
     * @param article
     */
    private static void mapOrderUnit( ArticleAPI articleAPI, Article article ) {
        if( article.getBasedOnProduct() != null && !article.isOrderInformationOverridden() && article.getStatus() != Product.Status.DISCONTINUED) {
            articleAPI.setOrderUnit(OrderUnitMapper.map(article.getBasedOnProduct().getOrderUnit()));
        } else {
            articleAPI.setOrderUnit(OrderUnitMapper.map(article.getOrderUnit()));
        }
    }

    private static void mapOrderInformation( ArticleAPI articleAPI, Article article ) {
        if( article.getBasedOnProduct() != null && !article.isOrderInformationOverridden() && article.getStatus() != Product.Status.DISCONTINUED) {
            // use inhertited order information
            articleAPI.setArticleQuantityInOuterPackage(article.getBasedOnProduct().getArticleQuantityInOuterPackage());
            articleAPI.setArticleQuantityInOuterPackageUnit(OrderUnitMapper.map(article.getBasedOnProduct().getArticleQuantityInOuterPackageUnit()));
            articleAPI.setPackageContent(article.getBasedOnProduct().getPackageContent());
            articleAPI.setPackageContentUnit(PackageUnitMapper.map(article.getBasedOnProduct().getPackageContentUnit()));
            articleAPI.setPackageLevelBase(article.getBasedOnProduct().getPackageLevelBase());
            articleAPI.setPackageLevelMiddle(article.getBasedOnProduct().getPackageLevelMiddle());
            articleAPI.setPackageLevelTop(article.getBasedOnProduct().getPackageLevelTop());
        } else {
            // use article order information
            articleAPI.setArticleQuantityInOuterPackage(article.getArticleQuantityInOuterPackage());
            articleAPI.setArticleQuantityInOuterPackageUnit(OrderUnitMapper.map(article.getArticleQuantityInOuterPackageUnit()));
            articleAPI.setPackageContent(article.getPackageContent());
            articleAPI.setPackageContentUnit(PackageUnitMapper.map(article.getPackageContentUnit()));
            articleAPI.setPackageLevelBase(article.getPackageLevelBase());
            articleAPI.setPackageLevelMiddle(article.getPackageLevelMiddle());
            articleAPI.setPackageLevelTop(article.getPackageLevelTop());
        }
    }

    private static void setCategoryProperties(Article article, ArticleAPI articleAPI, List<CategorySpecificProperty> categorySpecificPropertys) {
        int categorySpecificPropertysSize = categorySpecificPropertys == null ? 0: categorySpecificPropertys.size();
        int resourceSpecificPropertyValuesSize = article.getResourceSpecificPropertyValues() == null ? 0: article.getResourceSpecificPropertyValues().size();
        if( article.getResourceSpecificPropertyValues() != null && !article.getResourceSpecificPropertyValues().isEmpty() ) {
            articleAPI.setCategoryPropertys(ResourceSpecificPropertyMapper.map(article.getResourceSpecificPropertyValues()));
        } else {
            articleAPI.setCategoryPropertys(new ArrayList<>());
        }
        if( categorySpecificPropertysSize != 0 && categorySpecificPropertysSize != resourceSpecificPropertyValuesSize ) {
            for( CategorySpecificProperty categorySpecificProperty : categorySpecificPropertys ) {
                ResourceSpecificPropertyValue articleResourceSpecificPropertyValue = getResourceSpecificProperty(categorySpecificProperty, article.getResourceSpecificPropertyValues());
                if( articleResourceSpecificPropertyValue == null ) {
                    // see if based on product has it
                    ResourceSpecificPropertyValue productResourceSpecificPropertyValue = null;
                    if (article.getBasedOnProduct() != null) {
                        productResourceSpecificPropertyValue = getResourceSpecificProperty(categorySpecificProperty, article.getBasedOnProduct().getResourceSpecificPropertyValues());
                    }
                    if( productResourceSpecificPropertyValue != null ) {
                        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = ResourceSpecificPropertyMapper.map(productResourceSpecificPropertyValue);
                        resourceSpecificPropertyAPI.setId(null);  // null to signal this is not the articles property
                        articleAPI.getCategoryPropertys().add(resourceSpecificPropertyAPI);
                    } else {
                        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
                        resourceSpecificPropertyAPI.setProperty(CategorySpecificPropertyMapper.map(categorySpecificProperty));
                        articleAPI.getCategoryPropertys().add(resourceSpecificPropertyAPI);
                    }
                }
            }
        }

        // must sort category properties
        Collections.sort(articleAPI.getCategoryPropertys(), (ResourceSpecificPropertyAPI o1, ResourceSpecificPropertyAPI o2) -> o1.getProperty().getName().compareTo(o2.getProperty().getName()));
    }

    private static ResourceSpecificPropertyValue getResourceSpecificProperty(CategorySpecificProperty categorySpecificProperty, List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues) {
        if( resourceSpecificPropertyValues != null && !resourceSpecificPropertyValues.isEmpty() ) {
            for( ResourceSpecificPropertyValue resourceSpecificPropertyValue : resourceSpecificPropertyValues ) {
                if( resourceSpecificPropertyValue.getCategorySpecificProperty().getUniqueId().equals(categorySpecificProperty.getUniqueId()) ) {
                    return resourceSpecificPropertyValue;
                }
            }
        }
        return null;
    }

}
