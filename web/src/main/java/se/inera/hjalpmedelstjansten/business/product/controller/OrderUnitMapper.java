package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.LoggerService;
import se.inera.hjalpmedelstjansten.model.api.cv.CVOrderUnitAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class OrderUnitMapper {
    
    private static final HjmtLogger LOG = LoggerService.getLogger(OrderUnitMapper.class.getName());
    
    public static final List<CVOrderUnitAPI> map(List<CVOrderUnit> orderUnits) {
        if( orderUnits == null ) {
            return null;
        }
        List<CVOrderUnitAPI> orderUnitAPIs = new ArrayList<>();
        for( CVOrderUnit orderUnit : orderUnits ) {
            orderUnitAPIs.add(map(orderUnit));
        }
        return orderUnitAPIs;
    }
    
    public static final CVOrderUnitAPI map(CVOrderUnit orderUnit) {
        LOG.log( Level.FINEST, "map(...)" );
        if( orderUnit == null ) {
            return null;
        }
        CVOrderUnitAPI orderUnitAPI = new CVOrderUnitAPI();
        orderUnitAPI.setId(orderUnit.getUniqueId());
        orderUnitAPI.setCode(orderUnit.getCode());
        orderUnitAPI.setName(orderUnit.getName());
        return orderUnitAPI;
    }
    
}
