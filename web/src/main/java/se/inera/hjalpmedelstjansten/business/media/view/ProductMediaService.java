package se.inera.hjalpmedelstjansten.business.media.view;

import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.media.controller.MediaController;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.media.MediaAPI;
import se.inera.hjalpmedelstjansten.model.api.media.MediaListAPI;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.logging.Level;

/**
 * REST API for articles.
 *
 */
@Stateless
@Path("organizations/{organizationUniqueId}/products/{productUniqueId}/media")
@Interceptors({ PerformanceLogInterceptor.class })
public class ProductMediaService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    private MediaController mediaController;

    @EJB
    private AuthHandler authHandler;

    /**
     * Get media on product
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to create article on
     * @param productUniqueId
     * @return the created <code>ArticleAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @GET
    @SecuredService(permissions = {"media:view"})
    public Response getProductMedia(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("productUniqueId") long productUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "getProductMedia( organizationUniqueId: {0}, productUniqueId: {1} )", new Object[] {organizationUniqueId, productUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        MediaListAPI mediaListAPI = mediaController.getProductMedia(organizationUniqueId, productUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( mediaListAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(mediaListAPI).build();
        }
    }

    @DELETE
    @Path("{mediaUniqueId}")
    @SecuredService(permissions = {"media:delete"})
    public Response deleteProductMedia(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("productUniqueId") long productUniqueId,
            @PathParam("mediaUniqueId") long mediaUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteProductMedia( organizationUniqueId: {0}, productUniqueId: {1}, mediaUniqueId: {2} )", new Object[] {organizationUniqueId, productUniqueId, mediaUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        mediaController.deleteProductMedia(organizationUniqueId, productUniqueId, mediaUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        return Response.ok().build();
    }

    /**
     * Create new document on product
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to create media on
     * @param productUniqueId unique id of the product to create media on
     * @param multipartFormDataInput user data, including file (if any)
     * @return the created <code>MediaAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("document")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @SecuredService(permissions = {"media:create"})
    public Response createProductDocument(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("productUniqueId") long productUniqueId,
            MultipartFormDataInput multipartFormDataInput) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createProductDocument( organizationUniqueId: {0}, productUniqueId: {1} )", new Object[] {organizationUniqueId, productUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        MediaAPI mediaAPI = mediaController.handleDocumentUpload(organizationUniqueId, productUniqueId, null, multipartFormDataInput, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        return Response.ok(mediaAPI).build();
    }

    /**
     * Create new image on product
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to create media on
     * @param productUniqueId unique id of the product to create media on
     * @param multipartFormDataInput user data, including file (if any)
     * @return the created <code>MediaAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("image")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @SecuredService(permissions = {"media:create"})
    public Response createProductImage(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("productUniqueId") long productUniqueId,
            MultipartFormDataInput multipartFormDataInput) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createProductImage( organizationUniqueId: {0}, productUniqueId: {1} )", new Object[] {organizationUniqueId, productUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        MediaAPI mediaAPI = mediaController.handleImageUpload(organizationUniqueId, productUniqueId, null, multipartFormDataInput, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        return Response.ok(mediaAPI).build();
    }

    /**
     * Create new video on product
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to create media on
     * @param productUniqueId unique id of the product to create media on
     * @param multipartFormDataInput user data, no file
     * @return the created <code>MediaAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("video")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @SecuredService(permissions = {"media:create"})
    public Response createProductVideo(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("productUniqueId") long productUniqueId,
            MultipartFormDataInput multipartFormDataInput) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createProductVideo( organizationUniqueId: {0}, productUniqueId: {1} )", new Object[] {organizationUniqueId, productUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        MediaAPI mediaAPI = mediaController.handleVideoUpload(organizationUniqueId, productUniqueId, null, multipartFormDataInput, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        return Response.ok(mediaAPI).build();
    }

    @PUT
    @Path("{mediaUniqueId}/update")
    @SecuredService(permissions = {"media:delete"})
    public Response updateProductMedia(@Context HttpServletRequest httpServletRequest,
                                       @PathParam("organizationUniqueId") long organizationUniqueId,
                                       @PathParam("mediaUniqueId") long mediaUniqueId,
                                       @PathParam("productUniqueId") long productUniqueId,
                                       MultipartFormDataInput multipartFormDataInput) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateProductMedia( organizationUniqueId: {0}, mediaUniqueId: {1} )", new Object[] {organizationUniqueId, mediaUniqueId});

        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        MediaAPI mediaAPI = mediaController.updateMedia(organizationUniqueId, mediaUniqueId, productUniqueId, null, multipartFormDataInput, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( mediaAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(mediaAPI).build();
        }
    }
}
