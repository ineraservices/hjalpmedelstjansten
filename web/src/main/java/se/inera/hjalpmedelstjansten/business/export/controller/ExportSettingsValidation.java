package se.inera.hjalpmedelstjansten.business.export.controller;

import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.ExportSettingsAPI;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.Set;
import java.util.logging.Level;

/**
 * Validation methods for export settings
 *
 */
@Stateless
public class ExportSettingsValidation {

    @Inject
    HjmtLogger LOG;

    @Inject
    ValidationMessageService validationMessageService;

    public void validateForCreate(ExportSettingsAPI exportSettingsAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForCreate(...)" );
        validateExportSettingsAPI(exportSettingsAPI);
    }

    public void validateForUpdate(ExportSettingsAPI exportSettingsAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForUpdate(...)" );
        validateExportSettingsAPI(exportSettingsAPI);
    }

    private void validateExportSettingsAPI(ExportSettingsAPI exportSettingsAPI) throws HjalpmedelstjanstenValidationException {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<ExportSettingsAPI>> constraintViolations = validator.validate(exportSettingsAPI, Default.class);
        if( constraintViolations != null && !constraintViolations.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            for( ConstraintViolation constraintViolation : constraintViolations ) {
                exception.addValidationMessage(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage());
            }
            throw exception;
        }
    }

}
