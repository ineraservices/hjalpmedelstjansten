package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Level;

@Stateless
public class PreventiveMaintenanceUnitController {

    @Inject
    private HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;

    public List<CVPreventiveMaintenanceAPI> getAllPreventiveMaintenanceAPIs() {
        LOG.log(Level.FINEST, "getAllPreventiveMaintenanceAPIs()");
        return PreventiveMaintenanceMapper.map(em.createNamedQuery(CVPreventiveMaintenance.FIND_ALL)
                .getResultList());
    }

    public List<CVPreventiveMaintenance> getAllPreventiveMaintenances() {
        LOG.log(Level.FINEST, "getAllPreventiveMaintenances()");
        return em.createNamedQuery(CVPreventiveMaintenance.FIND_ALL)
                .getResultList();
    }

    public CVPreventiveMaintenance getPreventiveMaintenance(long uniqueId) {
        LOG.log(Level.FINEST, "getPreventiveMaintenance( uniqueId: {0} )");
        return em.find(CVPreventiveMaintenance.class, uniqueId);
    }

    public CVPreventiveMaintenance findByCode(String code) {
        LOG.log(Level.FINEST, "findByCode( code: {0} )", new Object[] {code});
        List<CVPreventiveMaintenance> preventiveMaintenances = em.createNamedQuery(CVPreventiveMaintenance.FIND_BY_CODE).
                setParameter("code", code).
                getResultList();
        // there is a maximum of one
        if( preventiveMaintenances != null && !preventiveMaintenances.isEmpty() ) {
            return preventiveMaintenances.get(0);
        }
        return null;
    }

    public CVPreventiveMaintenance findByName(String name) {
        LOG.log(Level.FINEST, "findByName( name: {0} )", new Object[] {name});
        List<CVPreventiveMaintenance> preventiveMaintenances = em.createNamedQuery(CVPreventiveMaintenance.FIND_BY_NAME).
                setParameter("name", name).
                getResultList();
        // there is a maximum of one
        if( preventiveMaintenances != null && !preventiveMaintenances.isEmpty() ) {
            return preventiveMaintenances.get(0);
        }
        return null;
    }

}
