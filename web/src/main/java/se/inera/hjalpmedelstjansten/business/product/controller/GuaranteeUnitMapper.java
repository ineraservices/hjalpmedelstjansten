package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;

import java.util.ArrayList;
import java.util.List;

public class GuaranteeUnitMapper {
    
    public static final List<CVGuaranteeUnitAPI> map(List<CVGuaranteeUnit> guaranteeUnits) {
        if( guaranteeUnits == null ) {
            return null;
        }
        List<CVGuaranteeUnitAPI> guaranteeUnitAPIs = new ArrayList<>();
        for( CVGuaranteeUnit guaranteeUnit : guaranteeUnits ) {
            guaranteeUnitAPIs.add(map(guaranteeUnit));
        }
        return guaranteeUnitAPIs;
    }
    
    public static final CVGuaranteeUnitAPI map(CVGuaranteeUnit guaranteeUnit) {
        if( guaranteeUnit == null ) {
            return null;
        }
        CVGuaranteeUnitAPI guaranteeUnitAPI = new CVGuaranteeUnitAPI();
        guaranteeUnitAPI.setId(guaranteeUnit.getUniqueId());
        guaranteeUnitAPI.setCode(guaranteeUnit.getCode());
        guaranteeUnitAPI.setName(guaranteeUnit.getName());
        return guaranteeUnitAPI;
    }
    
}
