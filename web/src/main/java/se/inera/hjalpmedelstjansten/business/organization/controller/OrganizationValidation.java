package se.inera.hjalpmedelstjansten.business.organization.controller;

import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.entity.Organization;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;


@Stateless
public class OrganizationValidation {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private ValidationMessageService validationMessageService;

    @Inject
    private OrganizationController organizationController;

    @Inject
    private UserController userController;

    public void validateForCreate(OrganizationAPI organizationAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForCreate(...)" );
        validateOrganizationAPI(organizationAPI);
        HjalpmedelstjanstenValidationException exception = null;
        // make sure organizationNumber and gln is unique
        Organization.OrganizationType organizationType = Organization.OrganizationType.valueOf(organizationAPI.getOrganizationType());
        List<Organization> organizations = organizationController.findByGlnAndType(organizationAPI.getGln(), organizationType);
        if( organizations != null && !organizations.isEmpty() ) {
            exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessage("gln", validationMessageService.getMessage("organization.gln.notUnique"));
        }
        organizations = organizationController.findByOrganizationNumberAndType(organizationAPI.getOrganizationNumber(), organizationType );
        if( organizations != null && !organizations.isEmpty() ) {
            if( exception == null ) {
                exception = new HjalpmedelstjanstenValidationException("Validation failed");
            }
            exception.addValidationMessage("organizationNumber", validationMessageService.getMessage("organization.number.notUnique"));
        }
        if( exception != null ) {
            throw exception;
        }
    }

    public void validateForUpdate(OrganizationAPI organizationAPI, Organization currentOrganization) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForUpdate(...)" );
        validateOrganizationAPI(organizationAPI);
        // organizationNumber, country and type needs no check since they will not be changed
        List<Organization> organizations = organizationController.findByGlnAndType(organizationAPI.getGln(), currentOrganization.getOrganizationType());
        if( organizations != null && !organizations.isEmpty() ) {
            for( Organization organization : organizations ) {
                if( !organization.getUniqueId().equals(currentOrganization.getUniqueId()) ) {
                    // gln changed to another organizations
                    HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
                    exception.addValidationMessage("gln", validationMessageService.getMessage("organization.gln.notUnique"));
                    throw exception;
                }
            }
        }
    }

    public void validateForDelete(Organization currentOrganization) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForDelete(...)" );
        long numberOfUserEngagementsOnOrganization = userController.countUserEngagementsOnOrganization(currentOrganization.getUniqueId());
        if( numberOfUserEngagementsOnOrganization > 0 ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessage(validationMessageService.getMessage("organization.delete.usersOnOrganization"));
            throw exception;
        }

    }

    private void validateOrganizationAPI(OrganizationAPI organizationAPI) throws HjalpmedelstjanstenValidationException {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        if( constraintViolations != null && !constraintViolations.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            for( ConstraintViolation constraintViolation : constraintViolations ) {
                exception.addValidationMessage(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage());
            }
            throw exception;
        }
    }

}
