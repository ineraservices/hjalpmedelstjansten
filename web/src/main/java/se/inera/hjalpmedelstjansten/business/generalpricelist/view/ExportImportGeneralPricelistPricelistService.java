package se.inera.hjalpmedelstjansten.business.generalpricelist.view;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.jboss.ejb3.annotation.TransactionTimeout;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.exportimport.ExportPricelistController;
import se.inera.hjalpmedelstjansten.business.agreement.controller.exportimport.ImportGeneralPricelistPricelistController;
import se.inera.hjalpmedelstjansten.business.agreement.controller.exportimport.ImportAgreementPricelistController;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistController;
import se.inera.hjalpmedelstjansten.business.helpers.ExportHelper;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelist;


@Stateless
@Path("organizations/{organizationUniqueId}/generalpricelists/pricelists/{pricelistUniqueId}/exportimport")
@Interceptors({ PerformanceLogInterceptor.class })
public class ExportImportGeneralPricelistPricelistService extends BaseService {

    @Inject
    private HjmtLogger LOG;
    @Inject
    private GeneralPricelistController generalPricelistController;
    @Inject
    private ExportHelper exportHelper;

    @EJB
    private AuthHandler authHandler;

    @EJB
    private ExportPricelistController exportPricelistController;

    @EJB
    private ImportAgreementPricelistController importPricelistController;

    @EJB
    private ImportGeneralPricelistPricelistController importGeneralPricelistPricelistController;

    /**
     * Export general pricelist rows to Excel
     *
     * @param httpServletRequest
     * @param organizationUniqueId
     * @param pricelistUniqueId
     * @return
     * @throws HjalpmedelstjanstenValidationException
     */
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @Path("export/all")
    @SecuredService(permissions = {"generalpricelistpricelistexport:view"})
    public Response exportGeneralPricelistPricelistAll(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "exportGeneralPricelistPricelist( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[] {organizationUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
        GeneralPricelist generalPricelist = generalPricelistController.getGeneralPricelist(organizationUniqueId);

        String ownerOrganizationName = generalPricelist.getOwnerOrganization().getOrganizationName();
        //String agreementNumberNameAndOrgNameFiltered = (generalPricelist.getOwnerOrganization().getOrganizationName().substring(0, 10)+"_"+generalPricelist.getGeneralPricelistNumber() + "_" + generalPricelist.getGeneralPricelistName()).replaceAll(" ", "_").replaceAll(",.", "");
        String agreementNumberNameAndOrgNameFiltered = (ownerOrganizationName.substring(0, Math.min(10, ownerOrganizationName.length()))+"_"+generalPricelist.getGeneralPricelistNumber() + "_" + generalPricelist.getGeneralPricelistName()).replaceAll(" ", "_").replaceAll("[,.]", "");

        String filename = exportHelper.replaceSpecialCharacters(agreementNumberNameAndOrgNameFiltered) + "_GP_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";
        byte[] exportBytes = exportPricelistController.exportGeneralPricelistFile("all", organizationUniqueId, pricelistUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        LOG.log( Level.FINEST, "Content-Length: {0}", new Object[] {exportBytes.length});
        return Response.ok(
                exportBytes,
                jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM).
                header("Content-Disposition", "attachment; filename=" + filename).
                header("Content-Length", exportBytes.length).
                build();
    }

    /**
     * Export general pricelist rows to Excel
     *
     * @param httpServletRequest
     * @param organizationUniqueId
     * @param pricelistUniqueId
     * @return
     * @throws HjalpmedelstjanstenValidationException
     */
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @Path("export/active")
    @SecuredService(permissions = {"generalpricelistpricelistexport:view"})
    public Response exportGeneralPricelistPricelistActive(@Context HttpServletRequest httpServletRequest,
                                                    @PathParam("organizationUniqueId") long organizationUniqueId,
                                                    @PathParam("pricelistUniqueId") long pricelistUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "exportGeneralPricelistPricelist( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[] {organizationUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
        GeneralPricelist generalPricelist = generalPricelistController.getGeneralPricelist(organizationUniqueId);

        String ownerOrganizationName = generalPricelist.getOwnerOrganization().getOrganizationName();
        //String agreementNumberNameAndOrgNameFiltered = (generalPricelist.getOwnerOrganization().getOrganizationName().substring(0, 10)+"_"+generalPricelist.getGeneralPricelistNumber() + "_" + generalPricelist.getGeneralPricelistName()).replaceAll(" ", "_").replaceAll(",.", "");
        String agreementNumberNameAndOrgNameFiltered = (ownerOrganizationName.substring(0, Math.min(10, ownerOrganizationName.length()))+"_"+generalPricelist.getGeneralPricelistNumber() + "_" + generalPricelist.getGeneralPricelistName()).replaceAll(" ", "_").replaceAll("[,.]", "");

        String filename = exportHelper.replaceSpecialCharacters(agreementNumberNameAndOrgNameFiltered) + "_GP_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";
        byte[] exportBytes = exportPricelistController.exportGeneralPricelistFile("inProduction", organizationUniqueId, pricelistUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        LOG.log( Level.FINEST, "Content-Length: {0}", new Object[] {exportBytes.length});
        return Response.ok(
                exportBytes,
                jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM).
                header("Content-Disposition", "attachment; filename=" + filename).
                header("Content-Length", exportBytes.length).
                build();
    }

    /**
     * Export general pricelist rows to Excel
     *
     * @param httpServletRequest
     * @param organizationUniqueId
     * @param pricelistUniqueId
     * @return
     * @throws HjalpmedelstjanstenValidationException
     */
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @Path("export/inactive")
    @SecuredService(permissions = {"generalpricelistpricelistexport:view"})
    public Response exportGeneralPricelistPricelistInactive(@Context HttpServletRequest httpServletRequest,
                                                    @PathParam("organizationUniqueId") long organizationUniqueId,
                                                    @PathParam("pricelistUniqueId") long pricelistUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "exportGeneralPricelistPricelist( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[] {organizationUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
        GeneralPricelist generalPricelist = generalPricelistController.getGeneralPricelist(organizationUniqueId);

        String ownerOrganizationName = generalPricelist.getOwnerOrganization().getOrganizationName();
        //String agreementNumberNameAndOrgNameFiltered = (generalPricelist.getOwnerOrganization().getOrganizationName().substring(0, 10)+"_"+generalPricelist.getGeneralPricelistNumber() + "_" + generalPricelist.getGeneralPricelistName()).replaceAll(" ", "_").replaceAll(",.", "");
        String agreementNumberNameAndOrgNameFiltered = (ownerOrganizationName.substring(0, Math.min(10, ownerOrganizationName.length()))+"_"+generalPricelist.getGeneralPricelistNumber() + "_" + generalPricelist.getGeneralPricelistName()).replaceAll(" ", "_").replaceAll("[,.]", "");

        String filename = exportHelper.replaceSpecialCharacters(agreementNumberNameAndOrgNameFiltered) + "_GP_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";
        byte[] exportBytes = exportPricelistController.exportGeneralPricelistFile("discontinued", organizationUniqueId, pricelistUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        LOG.log( Level.FINEST, "Content-Length: {0}", new Object[] {exportBytes.length});
        return Response.ok(
                exportBytes,
                jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM).
                header("Content-Disposition", "attachment; filename=" + filename).
                header("Content-Length", exportBytes.length).
                build();
    }

    /**
     * Import general pricelist (rows) from Excel
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization
     * @param pricelistUniqueId unique id of the pricelist
     * @param multipartFormDataInput user uploaded file
     * @return a 200 response
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("import")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @SecuredService(permissions = {"productarticleimport:view"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response importGeneralPricelistPricelist(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            MultipartFormDataInput multipartFormDataInput) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "importGeneralPricelistPricelist( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[] {organizationUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        importGeneralPricelistPricelistController.importGeneralPricelistPricelistFile(organizationUniqueId,
                pricelistUniqueId,
                multipartFormDataInput,
                userAPI,
                authHandler.getSessionId(),
                getRequestIp(httpServletRequest));
        return Response.ok().build();
    }

}
