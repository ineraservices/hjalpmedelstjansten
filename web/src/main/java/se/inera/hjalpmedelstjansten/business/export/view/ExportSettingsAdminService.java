package se.inera.hjalpmedelstjansten.business.export.view;

import java.util.List;
import java.util.logging.Level;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Response;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.export.controller.ExportSettingsController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.ExportSettingsAPI;


@Stateless
@Path("exportsettings")
@Interceptors({ PerformanceLogInterceptor.class })
public class ExportSettingsAdminService {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private ExportSettingsController exportSettingsController;

    /**
     * Search all export settings
     *
     * @return a list of <code>ExportSettingsAPI</code>
     */
    @GET
    @SecuredService(permissions = {"exportsettings:view_all"})
    public Response searchExportSettings() {
        LOG.log(Level.FINEST, "searchExportSettings(...)");
        List<ExportSettingsAPI> exportSettingsAPIs = exportSettingsController.search();
        return Response.ok(exportSettingsAPIs).build();
    }

    @POST
    @Path("queue")
    @SecuredService(permissions = {"exportsettings:view_all"})
    public Response queueExport(ExportSettingsAPI exportSettingsAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "searchExportSettings(...)");
        Boolean success = exportSettingsController.queueExport(exportSettingsAPI.getId());
        return Response.ok(exportSettingsAPI).build();
    }

    /**
     * Get specific export settings
     *
     * @param exportSettingsId
     * @return an <code>ExportSettingsAPI</code>
     */
    @GET
    @Path("{exportSettingsId}")
    @SecuredService(permissions = {"exportsettings:view_all"})
    public Response getExportSettings(@PathParam("exportSettingsId") long exportSettingsId) {
        LOG.log(Level.FINEST, "getExportSettings(...)");
        ExportSettingsAPI exportSettingsAPI = exportSettingsController.find(exportSettingsId);
        if( exportSettingsAPI == null ) {
            return Response.noContent().build();
        } else {
            return Response.ok(exportSettingsAPI).build();
        }
    }

    /**
     * Create new export setting
     *
     * @param exportSettingsAPI user supplied values
     * @return the created <code>ExportSettingsAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @SecuredService(permissions = {"exportsettings:create_all"})
    public Response createExportSettings(ExportSettingsAPI exportSettingsAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createExportSettings(...)");
        exportSettingsAPI = exportSettingsController.create(exportSettingsAPI);
        return Response.ok(exportSettingsAPI).build();
    }

    /**
     * Update export setting
     *
     * @param exportSettingsId unique id of the export settings
     * @param exportSettingsAPI user supplied values
     * @return the update <code>ExportSettingsAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @PUT
    @Path("{exportSettingsId}")
    @SecuredService(permissions = {"exportsettings:update_all"})
    public Response updateExportSettings(@PathParam("exportSettingsId") long exportSettingsId,
            ExportSettingsAPI exportSettingsAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateExportSettings( exportSettingsId: {0} )", new Object[] {exportSettingsId});
        exportSettingsAPI = exportSettingsController.update(exportSettingsId, exportSettingsAPI);
        return Response.ok(exportSettingsAPI).build();
    }

}
