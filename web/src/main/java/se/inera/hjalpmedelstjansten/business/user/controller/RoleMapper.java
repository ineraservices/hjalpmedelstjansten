package se.inera.hjalpmedelstjansten.business.user.controller;

import se.inera.hjalpmedelstjansten.model.api.RoleAPI;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import java.util.ArrayList;
import java.util.List;


public class RoleMapper {

    public static List<RoleAPI> map(List<UserRole> userRoles) {
        if( userRoles == null ) {
            return null;
        }
        List<RoleAPI> roleAPIs = new ArrayList<>();
        for( UserRole userRole : userRoles ) {
            roleAPIs.add(map(userRole));
        }
        return roleAPIs;
    }

    public static RoleAPI map(UserRole userRole) {
        if( userRole == null ) {
            return null;
        }
        RoleAPI roleAPI = new RoleAPI();
        roleAPI.setId(userRole.getUniqueId());
        roleAPI.setName(userRole.getName().toString());
        roleAPI.setDescription(userRole.getDescription());
        return roleAPI;
    }
    
}
