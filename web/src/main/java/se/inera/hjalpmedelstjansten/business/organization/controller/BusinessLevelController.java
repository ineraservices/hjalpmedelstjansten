package se.inera.hjalpmedelstjansten.business.organization.controller;

import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Level;


@Stateless
public class BusinessLevelController {

    @Inject
    HjmtLogger LOG;

    @Inject
    OrganizationController organizationController;

    @Inject
    UserController userController;

    @Inject
    AgreementController agreementController;

    @Inject
    ValidationMessageService validationMessageService;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    public List<BusinessLevelAPI> getBusinessLevelsByOrganizationAndStatus( long organizationUniqueId, List<BusinessLevel.Status> statuses ) {
        LOG.log(Level.FINEST, "getAllActiveBusinessLevelsByOrganization( organizationUniqueId: {0}, statuses: {1} )", new Object[] {organizationUniqueId, statuses});
        return BusinessLevelMapper.map(em.createNamedQuery(BusinessLevel.FIND_BY_ORGANIZATION_AND_STATUSES).
                setParameter("organizationUniqueId", organizationUniqueId).
                setParameter("statuses", statuses).
                getResultList(), true);
    }

    public BusinessLevel getBusinessLevel( long organizationUniqueId, long businessLevelUniqueId ) {
        LOG.log(Level.FINEST, "getBusinessLevel( organizationUniqueId: {0}, businessLevelUniqueId: {1} )", new Object[] {organizationUniqueId, businessLevelUniqueId});
        BusinessLevel businessLevel = getBusinessLevel(businessLevelUniqueId);
        if( businessLevel == null ) {
            return null;
        }
        if( !businessLevel.getOrganization().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user to access business level: {0} not on given organization: {1}. Returning null.", new Object[] {businessLevel, organizationUniqueId});
            return null;
        }
        return businessLevel;
    }

    public BusinessLevel getBusinessLevel( long businessLevelUniqueId ) {
        LOG.log(Level.FINEST, "getBusinessLevel(businessLevelUniqueId: {0} )", new Object[] {businessLevelUniqueId});
        return em.find(BusinessLevel.class, businessLevelUniqueId );
    }

    public BusinessLevelAPI addBusinessLevelToOrganization(long organizationUniqueId, BusinessLevelAPI businessLevelAPI) {
        LOG.log(Level.FINEST, "getAllBusinessLevelsByOrganization( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        if( organization == null ) {
            return null;
        }
        BusinessLevel businessLevel = BusinessLevelMapper.map(businessLevelAPI, organization, BusinessLevel.Status.ACTIVE);
        em.persist(businessLevel);
        return BusinessLevelMapper.map(businessLevel, false);
    }

    public BusinessLevelAPI inactivateBusinessLevelOnOrganization(long organizationUniqueId, long businessLevelId) {
        LOG.log(Level.FINEST, "inactivateBusinessLevelOnOrganization( organizationUniqueId: {0}, businessLevelId: {1} )", new Object[] {organizationUniqueId, businessLevelId});
        BusinessLevel businessLevel = getBusinessLevel(organizationUniqueId, businessLevelId);
        if( businessLevel != null ) {
            businessLevel.setStatus(BusinessLevel.Status.INACTIVE);
            return BusinessLevelMapper.map(businessLevel, false);
        }
        return null;
    }

    public BusinessLevelAPI editBusinessLevelOnOrganization(long organizationUniqueId, long businessLevelId, BusinessLevelAPI businessLevelAPI) {
        LOG.log(Level.FINEST, "editBusinessLevelOnOrganization( organizationUniqueId: {0}, businessLevelId: {1} )", new Object[] {organizationUniqueId, businessLevelId});
        BusinessLevel businessLevel = getBusinessLevel(organizationUniqueId, businessLevelId);
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        if( organization == null ) {
            return null;
        }
        if( businessLevel != null ) {
            businessLevel.setName(businessLevelAPI.getName());
            return BusinessLevelMapper.map(businessLevel, false);
        }
        return null;
    }

    /**
     * Deletes the given business level from the organization. Only business levels
     * without any connections (except to the organization) can be removed so this is
     * more a feature if the user adds the business level by mistake.
     *
     * @param organizationUniqueId unique id of the organization
     * @param businessLevelId unique id of the business level
     * @return
     */
    public BusinessLevelAPI deleteBusinessLevelFromOrganization(long organizationUniqueId, long businessLevelId) throws HjalpmedelstjanstenValidationException {
        BusinessLevel businessLevel = getBusinessLevel(organizationUniqueId, businessLevelId);
        if( businessLevel != null ) {
            // no users can exist with this business level available
            List<UserEngagement> userEngagements = userController.findByBusinessLevel(businessLevelId);
            if( userEngagements != null && !userEngagements.isEmpty() ) {
                throw validationMessageService.generateValidationException("userEngagements", "businessLevel.delete.userEngagementsExist");
            }
            List<Agreement> agreements = agreementController.findByBusinessLevel(businessLevelId);
            if( agreements != null && !agreements.isEmpty() ) {
                throw validationMessageService.generateValidationException("agreements", "businessLevel.delete.agreementsExist");
            }
            em.remove(businessLevel);
        }
        return BusinessLevelMapper.map(businessLevel, false);
    }

}
