package se.inera.hjalpmedelstjansten.business.generalpricelist.controller;

import se.inera.hjalpmedelstjansten.business.BaseController;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.assortment.controller.AssortmentController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.product.controller.CVPreventiveMaintenanceController;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.*;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

import jakarta.ejb.Stateless;
import jakarta.enterprise.event.Event;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import java.util.*;
import java.util.logging.Level;

/**
 * Class for handling business logic of <code>GeneralPricelist</code>.
 * This includes talking to the database.
 *
 */
@Stateless
public class GeneralPricelistPricelistRowController extends BaseController {

    @Inject
    HjmtLogger LOG;

    @PersistenceContext(unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    GeneralPricelistPricelistController generalPricelistPricelistController;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    GeneralPricelistPricelistRowValidation pricelistRowValidation;

    @Inject
    ArticleController articleController;

    @Inject
    GuaranteeUnitController guaranteeUnitController;

    @Inject
    CVPreventiveMaintenanceController cVPreventiveMaintenanceController;

    @Inject
    OrganizationController organizationController;

    @Inject
    Event<InternalAuditEvent> internalAuditEvent;

    @Inject
    AssortmentController assortmentController;

    /**
     * Search pricelist rows for the user supplied query. Search is paginated so only
     * results valid for given offset and limit is returned.
     *
     * @param queryString           user supplied query
     * @param organizationUniqueId  unique id of the organization
     * @param pricelistUniqueId     unique id of the pricelist
     * @param userAPI               user information, needed for business levels validation
     * @param offset                start returning from this search result
     * @param limit                 maximum number of search results to return
     * @param statuses              list of statuses to include in search, if null or empty,
     *                              all statuses are included
     * @param articleTypes          list of article types to include in search, if null or empty,
     *                              all article types are included
     * @param showRowsWithPrices    include rows where price is not null, if neither this
     *                              not showRowsWithoutPrices is set, then all rows are included
     * @param showRowsWithoutPrices include rows where price is null, if neither this
     *                              not showRowsWithPrices is set, then all rows are included
     * @return a list of <code>GeneralPricelistPricelistRowAPI</code> matching the query parameters
     */
    public SearchDTO searchPricelistRows(String queryString,
                                         String sortOrder,
                                         String sortType,
                                         long organizationUniqueId,
                                         long pricelistUniqueId,
                                         long assortmentId,
                                         UserAPI userAPI,
                                         int offset,
                                         int limit,
                                         List<GeneralPricelistPricelistRow.Status> statuses,
                                         List<Article.Type> articleTypes,
                                         Boolean showRowsWithPrices,
                                         Boolean showRowsWithoutPrices,
                                         Long categoryId,
                                         List<Product.Status> articleStatuses) {
        LOG.log(Level.FINEST, "searchPricelistRows( organizationUniqueId: {0}, pricelistUniqueId: {1}, statuses: {2}, articleTypes: {3}, showRowsWithPrices: {4}, showRowsWithoutPrices: {5} )", new Object[]{organizationUniqueId, pricelistUniqueId, statuses, articleTypes, showRowsWithPrices, showRowsWithoutPrices});
        GeneralPricelistPricelist pricelist = generalPricelistPricelistController.getPricelist(organizationUniqueId, pricelistUniqueId, userAPI);
        if (pricelist == null) {
            LOG.log(Level.WARNING, "Attempt by user: {0} to search rows on pricelist: {1} which is is not available. Returning 404.", new Object[]{userAPI.getId(), pricelistUniqueId});
            return null;
        }

        boolean searchWithPriceLimitation = true;
        Organization loggedInUserOrganization = organizationController.getOrganizationFromLoggedInUser(userAPI);
        if (loggedInUserOrganization.getOrganizationType() == Organization.OrganizationType.CUSTOMER) {
            // customers can only see rows with prices
            showRowsWithPrices = true;
            showRowsWithoutPrices = false;
        } else {
            // if both showRows... is null, it means, show everything, i.e. do not include in search
            showRowsWithPrices = showRowsWithPrices == null ? false : showRowsWithPrices;
            showRowsWithoutPrices = showRowsWithoutPrices == null ? false : showRowsWithoutPrices;
            if ((showRowsWithPrices && showRowsWithoutPrices) || (!showRowsWithPrices && !showRowsWithoutPrices)) {
                searchWithPriceLimitation = false;
            }
        }

        // articleStatuses: if none or both articleStatuses are selected (PUBLISHED and DISCONTINUED) do not include this filter in search
        boolean searchWithArticleStatusLimitation = articleStatuses != null && articleStatuses.size() == 1;

        List<Long> articleIdsOnAssortment = new ArrayList<>();
        if (assortmentId != 0) {
            Assortment assortment = assortmentController.getAssortment(loggedInUserOrganization.getUniqueId(), assortmentId, userAPI);
            List<Article> articlesOnAssortment = assortment.getArticles();

            for (Article art : articlesOnAssortment){
                articleIdsOnAssortment.add(art.getUniqueId());
            }
        }

        // count
        StringBuilder countSqlBuilder = new StringBuilder();
        countSqlBuilder.append("SELECT COUNT(p.uniqueId) FROM GeneralPricelistPricelistRow p ");
        if ((articleTypes != null && !articleTypes.isEmpty()) || categoryId != null) {
            countSqlBuilder.append("LEFT JOIN p.article.category ac ");
            countSqlBuilder.append("LEFT JOIN p.article.basedOnProduct.category apc ");
        }
        countSqlBuilder.append("WHERE p.pricelist.uniqueId = :pricelistUniqueId ");
        if (statuses != null && !statuses.isEmpty()) {
            countSqlBuilder.append("AND p.status IN :statuses ");
        }
        if (articleIdsOnAssortment.size() != 0){
            countSqlBuilder.append("AND p.article.uniqueId NOT IN :articleIdsOnAssortment ");
        }
        if (queryString != null && !queryString.isEmpty()) {
            countSqlBuilder.append("AND (p.article.articleName LIKE :query OR p.article.articleNumber LIKE :query) ");
        }
        if( categoryId != null ) {
            countSqlBuilder.append("AND (ac.uniqueId = :categoryId OR apc.uniqueId = :categoryId) ");
        }
        if (assortmentId != 0) {
            countSqlBuilder.append("AND apc.code IS NOT NULL ");
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            countSqlBuilder.append("AND (");
            for (int i = 0; i < articleTypes.size(); i++) {
                if (i != 0) {
                    countSqlBuilder.append("OR ");
                }
                countSqlBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(" ) ");
            }
            countSqlBuilder.append(") ");
        }
        if (searchWithPriceLimitation) {
            // means one of showRows... is false and one is true
            if (showRowsWithPrices) {
                countSqlBuilder.append("AND p.price IS NOT NULL ");
            } else if (showRowsWithoutPrices) {
                countSqlBuilder.append("AND p.price IS NULL ");
            }
        }

        if (searchWithArticleStatusLimitation){
            countSqlBuilder.append("AND p.article.status IN :articleStatuses ");
        }

        String countQuerySql = countSqlBuilder.toString();
        LOG.log(Level.FINEST, "countQuerySql: {0}", new Object[]{countQuerySql});
        Query countQuery = em.createQuery(countQuerySql).
                setParameter("pricelistUniqueId", pricelistUniqueId);

        if (articleTypes != null && !articleTypes.isEmpty()) {
            for (int i = 0; i < articleTypes.size(); i++) {
                countQuery.setParameter("type" + i, articleTypes.get(i));
            }
        }
        if( categoryId != null ) {
            countQuery.setParameter("categoryId", categoryId);
        }
        if (statuses != null && !statuses.isEmpty()) {
            countQuery.setParameter("statuses", statuses);
        }
        if (articleIdsOnAssortment.size() != 0){
            countQuery.setParameter("articleIdsOnAssortment", articleIdsOnAssortment);
        }
        if(searchWithArticleStatusLimitation) {
            countQuery.setParameter("articleStatuses", articleStatuses);
        }
        if (queryString != null && !queryString.isEmpty()) {
            countQuery.setParameter("query", "%" + queryString + "%");
        }
        Long count = (Long) countQuery.getSingleResult();

        // search
        StringBuilder searchSqlBuilder = new StringBuilder();
        searchSqlBuilder.append("SELECT p FROM GeneralPricelistPricelistRow p ");
        if ((articleTypes != null && !articleTypes.isEmpty()) || categoryId != null) {
            searchSqlBuilder.append("LEFT JOIN p.article.category ac ");
            searchSqlBuilder.append("LEFT JOIN p.article.basedOnProduct.category apc ");
        }
        searchSqlBuilder.append("WHERE p.pricelist.uniqueId = :pricelistUniqueId ");
        if (articleIdsOnAssortment.size() != 0){
            searchSqlBuilder.append("AND p.article.uniqueId NOT IN :articleIdsOnAssortment ");
        }
        if (statuses != null && !statuses.isEmpty()) {
            searchSqlBuilder.append("AND p.status IN :statuses ");
        }
        if (queryString != null && !queryString.isEmpty()) {
            searchSqlBuilder.append("AND (p.article.articleName LIKE :query OR p.article.articleNumber LIKE :query) ");
        }
        if (categoryId != null) {
            searchSqlBuilder.append("AND (ac.uniqueId = :categoryId OR apc.uniqueId = :categoryId) ");
        }
        if (assortmentId != 0) {
            searchSqlBuilder.append("AND apc.code IS NOT NULL ");
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            searchSqlBuilder.append("AND (");
            for (int i = 0; i < articleTypes.size(); i++) {
                if (i != 0) {
                    searchSqlBuilder.append("OR ");
                }
                searchSqlBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(" ) ");
            }
            searchSqlBuilder.append(") ");
        }
        if (searchWithPriceLimitation) {
            // means one of showRows... is false and one is true
            if (showRowsWithPrices) {
                searchSqlBuilder.append("AND p.price IS NOT NULL ");
            } else if (showRowsWithoutPrices) {
                searchSqlBuilder.append("AND p.price IS NULL ");
            }
        }

        if (searchWithArticleStatusLimitation){
            searchSqlBuilder.append("AND p.article.status IN :articleStatuses ");
        }


        if (!Objects.equals(sortType, "")) {
            sortType = sortType.toUpperCase();
            sortOrder = sortOrder.toUpperCase();

            switch (sortType) {
                case "NAME":
                    searchSqlBuilder.append("ORDER BY p.article.articleName ");
                    break;
                case "NUMBER":
                    searchSqlBuilder.append("ORDER BY p.article.articleNumber ");
                    break;
                case "ORGANIZATIONNAME":
                    searchSqlBuilder.append("ORDER BY p.article.organization.organizationName ");
                    break;
                case "BASEDONPRODUCT":
                    searchSqlBuilder.append("ORDER BY p.article.basedOnProduct.productName ");
                    break;
                case "ARTICLETYPE":
                    searchSqlBuilder.append("ORDER BY p.article.basedOnProduct.category.articleType ");
                    break;
                case "CODE":
                    searchSqlBuilder.append("ORDER BY p.article.basedOnProduct.category.code ");
                    break;
                case "STATUS":
                    searchSqlBuilder.append("ORDER BY p.article.status ");
                    break;
            }

            if (sortOrder.equals("DESC"))
                searchSqlBuilder.append("DESC ");
            else
                searchSqlBuilder.append("ASC ");
        } else {
            searchSqlBuilder.append("ORDER BY p.article.articleNumber ASC");
        }

        String searchQuerySql = searchSqlBuilder.toString();
        LOG.log(Level.FINEST, "searchQuerySql: {0}", new Object[]{searchQuerySql});
        Query searchQuery = em.createQuery(searchQuerySql).
                setParameter("pricelistUniqueId", pricelistUniqueId);
        if (articleIdsOnAssortment.size() != 0){
            searchQuery.setParameter("articleIdsOnAssortment", articleIdsOnAssortment);
        }
        if (statuses != null && !statuses.isEmpty()) {
            searchQuery.setParameter("statuses", statuses);
        }
        if(searchWithArticleStatusLimitation) {
            searchQuery.setParameter("articleStatuses", articleStatuses);
        }
        if (queryString != null && !queryString.isEmpty()) {
            searchQuery.setParameter("query", "%" + queryString + "%");
        }
        if (categoryId != null) {
            searchQuery.setParameter("categoryId", categoryId);
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            for (int i = 0; i < articleTypes.size(); i++) {
                searchQuery.setParameter("type" + i, articleTypes.get(i));
            }
        }

        List<GeneralPricelistPricelistRowAPI> pricelistRowAPIs = GeneralPricelistPricelistRowMapper.map(searchQuery.
                setMaxResults(limit).
                setFirstResult(offset).
                getResultList(), false);

        SearchDTO searchDTO = new SearchDTO(count, pricelistRowAPIs);

        if (loggedInUserOrganization.getOrganizationType() == Organization.OrganizationType.CUSTOMER) {

            for (int i = 0; i < searchDTO.getItems().size(); i++) {

                GeneralPricelistPricelistRowAPI row = (GeneralPricelistPricelistRowAPI) searchDTO.getItems().get(i);

                if (row.getArticle().getStatus().equalsIgnoreCase("discontinued")) {
                    row.setPrice(null);
                }
            }
        }

        return new SearchDTO(count, pricelistRowAPIs);
    }

    /**
     * Get the <code>GeneralPricelistPricelistRow</code> with the given id on the
     * specified organization.
     *
     * @param organizationUniqueId unique id of the organization
     * @param pricelistUniqueId    unique id of the pricelist
     * @param pricelistRowUniqueId unique id of the general pricelist pricelist row
     * @param userAPI              user information
     * @return the corresponding <code>GeneralPricelistPricelistRow</code>
     */
    public GeneralPricelistPricelistRow getPricelistRow(long organizationUniqueId, long pricelistUniqueId, long pricelistRowUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getAgreement( organizationUniqueId: {0}, pricelistUniqueId: {1}, pricelistRowUniqueId: {2} )", new Object[]{organizationUniqueId, pricelistUniqueId, pricelistRowUniqueId});
        GeneralPricelistPricelistRow pricelistRow = em.find(GeneralPricelistPricelistRow.class, pricelistRowUniqueId);
        if (pricelistRow == null) {
            LOG.log(Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning 404.", new Object[]{userAPI.getId(), pricelistRowUniqueId});
            return null;
        }

        // make sure user can fetch agreement connected to pricelist
        GeneralPricelistPricelist pricelist = generalPricelistPricelistController.getPricelist(organizationUniqueId, pricelistUniqueId, userAPI);
        if (pricelist == null) {
            LOG.log(Level.WARNING, "Attempt by user: {0} to get pricelist row: {1} from pricelist: {2} which does not exist or is not available to user", new Object[]{userAPI.getId(), pricelistRowUniqueId, pricelistUniqueId});
            return null;
        }
        if (!pricelistRow.getPricelist().getUniqueId().equals(pricelist.getUniqueId())) {
            LOG.log(Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} not on given pricelist: {2}. Returning 404.", new Object[]{userAPI.getId(), pricelistRowUniqueId, pricelist.getUniqueId()});
            return null;
        }
        return pricelistRow;
    }

    /**
     * Create a pricelist row based on the supplied data.
     *
     * @param organizationUniqueId unique id of the organization
     * @param pricelistUniqueId    unique id of the pricelist
     * @param pricelistRowAPIs     user supplied data
     * @param userAPI              logged in user's session information
     * @param sessionId
     * @param requestIp
     * @return the created <code>GeneralPricelistPricelistRow</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public List<GeneralPricelistPricelistRowAPI> createPricelistRows(long organizationUniqueId, long pricelistUniqueId, List<GeneralPricelistPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createPricelistRows( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[]{organizationUniqueId, pricelistUniqueId});

        // pricelist
        GeneralPricelistPricelist pricelist = generalPricelistPricelistController.getPricelist(organizationUniqueId, pricelistUniqueId, userAPI);
        if (pricelist == null) {
            LOG.log(Level.WARNING, "Attempt by user: {0} to add articles to pricelist: {1} which does not exist or is not available to user", new Object[]{userAPI.getId(), pricelistUniqueId});
            throw validationMessageService.generateValidationException("pricelist", "pricelistrow.pricelist.notNull");
        }
        GeneralPricelistPricelist currentPricelist = generalPricelistPricelistController.getCurrentPricelist(organizationUniqueId);
        List<Long> currentArticleUniqueIdsInPricelist = getGeneralPricelistPricelistArticleUniqueIds(pricelistUniqueId);
        List<GeneralPricelistPricelistRowAPI> generalPricelistPricelistRowAPIs = new ArrayList<>();
        for (GeneralPricelistPricelistRowAPI pricelistRowAPI : pricelistRowAPIs) {
            GeneralPricelistPricelistRow pricelistPricelistRow = createPricelistRow(organizationUniqueId, pricelistRowAPI, pricelist, currentPricelist, currentArticleUniqueIdsInPricelist, userAPI, sessionId, requestIp);
            currentArticleUniqueIdsInPricelist.add(pricelistPricelistRow.getArticle().getUniqueId());
            generalPricelistPricelistRowAPIs.add(GeneralPricelistPricelistRowMapper.map(pricelistPricelistRow, true));
        }
        return generalPricelistPricelistRowAPIs;
    }

    public GeneralPricelistPricelistRow createPricelistRow(long organizationUniqueId,
                                                           GeneralPricelistPricelistRowAPI pricelistRowAPI,
                                                           GeneralPricelistPricelist pricelist,
                                                           GeneralPricelistPricelist currentPricelist,
                                                           List<Long> currentArticleUniqueIdsInPricelist,
                                                           UserAPI userAPI,
                                                           String sessionId,
                                                           String requestIp) throws HjalpmedelstjanstenValidationException {
        pricelistRowValidation.validateForCreate(pricelistRowAPI, pricelist, currentPricelist);
        Date validFrom = new Date();
        GeneralPricelistPricelist.Status pricelistStatus = GeneralPricelistPricelistMapper.getPricelistStatus(pricelist, currentPricelist);
        if (pricelistStatus == GeneralPricelistPricelist.Status.FUTURE) {
            validFrom = pricelist.getValidFrom();
        }
        GeneralPricelistPricelistRow pricelistRow = GeneralPricelistPricelistRowMapper.map(pricelistRowAPI, validFrom);
        pricelistRow.setPricelist(pricelist);

        // article
        Article article = articleController.getArticle(organizationUniqueId, pricelistRowAPI.getArticle().getId(), userAPI);
        if (article == null) {
            LOG.log(Level.WARNING, "Attempt by user: {0} to add article: {1} which doesn't exist to pricelist: {2}", new Object[]{userAPI.getId(), pricelistRowAPI.getArticle().getId(), pricelist.getUniqueId()});
            throw validationMessageService.generateValidationException("article", "pricelistrow.article.notExist");
        }
        /*
        HJAL-1774
        if( article.getStatus() == Product.Status.DISCONTINUED ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to add article: {1} which is discontinued to pricelist: {2}", new Object[] {userAPI.getId(), article.getUniqueId(), pricelist.getUniqueId()});
            throw validationMessageService.generateValidationException("article", "pricelistrow.article.discontinued");
        }*/
        if (currentArticleUniqueIdsInPricelist.contains(article.getUniqueId())) {
            throw validationMessageService.generateValidationException("article", "pricelistrow.article.alreadyExistsInPricelist", article.getArticleName());
        }
        pricelistRow.setArticle(article);

        Article.Type articleType = pricelistRow.getArticle().getBasedOnProduct() == null ? pricelistRow.getArticle().getCategory().getArticleType() : pricelistRow.getArticle().getBasedOnProduct().getCategory().getArticleType();
        GeneralPricelistPricelistRowMapper.mapUpdatableFields(pricelistRowAPI, pricelistRow, articleType, true);
        setOverrideWarrantyValidFrom(pricelistRowAPI, pricelistRow, articleType, userAPI, true);

        // set overridden warranty quantity unit (if any)
        setOverriddenWarrantyQuantityUnit(pricelistRow, pricelistRowAPI, true);

        em.persist(pricelistRow);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.GENERAL_PRICELIST_PRICELIST_ROW, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        return pricelistRow;
    }

    /**
     * Update a pricelist row based on the supplied data.
     *
     * @param organizationUniqueId unique id of the organization
     * @param pricelistUniqueId    unique id of the pricelist
     * @param pricelistRowUniqueId unique id of the row
     * @param pricelistRowAPI      user supplied data
     * @param userAPI              logged in user's session information
     * @return the updated <code>GeneralPricelistPricelistRow</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public GeneralPricelistPricelistRowAPI updatePricelistRow(long organizationUniqueId, long pricelistUniqueId, long pricelistRowUniqueId, GeneralPricelistPricelistRowAPI pricelistRowAPI, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updatePricelistRow( organizationUniqueId: {0}, pricelistUniqueId: {1}, pricelistRowUniqueId: {2} )", new Object[]{organizationUniqueId, pricelistUniqueId, pricelistRowUniqueId});
        // pricelist
        GeneralPricelistPricelist pricelist = generalPricelistPricelistController.getPricelist(organizationUniqueId, pricelistUniqueId, userAPI);
        if (pricelist == null) {
            LOG.log(Level.WARNING, "Attempt by user: {0} to update row: {1} on pricelist: {2} which does not exist or is not available to user", new Object[]{userAPI.getId(), pricelistRowUniqueId, pricelistUniqueId});
            throw validationMessageService.generateValidationException("pricelist", "pricelistrow.pricelist.notNull");
        }
        GeneralPricelistPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, pricelistUniqueId, pricelistRowUniqueId, userAPI);
        if (pricelistRow == null) {
            LOG.log(Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning 404.", new Object[]{userAPI.getId(), pricelistRowUniqueId});
            return null;
        }
        GeneralPricelistPricelist currentPricelist = generalPricelistPricelistController.getCurrentPricelist(organizationUniqueId);
        pricelistRowValidation.validateForUpdate(pricelistRowAPI, pricelistRow, currentPricelist);
        pricelistRow.setPrice(pricelistRowAPI.getPrice());
        pricelistRow.setLeastOrderQuantity(pricelistRowAPI.getLeastOrderQuantity());
        Article.Type articleType = pricelistRow.getArticle().getBasedOnProduct() == null ? pricelistRow.getArticle().getCategory().getArticleType() : pricelistRow.getArticle().getBasedOnProduct().getCategory().getArticleType();
        GeneralPricelistPricelistRowMapper.mapUpdatableFields(pricelistRowAPI, pricelistRow, articleType, false);
        setOverrideWarrantyValidFrom(pricelistRowAPI, pricelistRow, articleType, userAPI, false);

        // set overridden warranty quantity unit (if any)
        setOverriddenWarrantyQuantityUnit(pricelistRow, pricelistRowAPI, false);

        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.GENERAL_PRICELIST_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRowUniqueId, requestIp));

        return GeneralPricelistPricelistRowMapper.map(pricelistRow, true);
    }

    public GeneralPricelistPricelistRowAPI deleteRow(long organizationUniqueId, long pricelistUniqueId, long pricelistRowUniqueId, UserAPI userAPI, String sessionId, String requestIp) {
        LOG.log(Level.FINEST, "deleteRow( organizationUniqueId: {0}, pricelistUniqueId: {1} pricelistRowUniqueId: {2})", new Object[]{organizationUniqueId, pricelistUniqueId, pricelistRowUniqueId});
        GeneralPricelistPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, pricelistUniqueId, pricelistRowUniqueId, userAPI);

        em.remove(pricelistRow);
        LOG.log(Level.INFO, "Deleted...");

        return GeneralPricelistPricelistRowMapper.map(pricelistRow, true);
    }

    /**
     * Inactivate the specified pricelist rows. If row is inactivated, this is
     * not possible.
     *
     * @param organizationUniqueId unique id of the organization
     * @param pricelistUniqueId    unique id of the pricelist
     * @param pricelistRowAPIs     list of rows to inactivate
     * @param userAPI              user session information
     * @param sessionId
     * @param requestIp
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     *                                                a rows is inactivated
     */
    public List<GeneralPricelistPricelistRowAPI> inactivateRows(long organizationUniqueId,
                                                                long pricelistUniqueId,
                                                                List<GeneralPricelistPricelistRowAPI> pricelistRowAPIs,
                                                                UserAPI userAPI,
                                                                String sessionId,
                                                                String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "inactivateRows( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[]{organizationUniqueId, pricelistUniqueId});
        if (pricelistRowAPIs == null || pricelistRowAPIs.isEmpty()) {
            return null;
        }
        List<GeneralPricelistPricelistRow> pricelistRows = new ArrayList<>();
        for (GeneralPricelistPricelistRowAPI pricelistRowAPI : pricelistRowAPIs) {
            GeneralPricelistPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if (pricelistRow == null) {
                LOG.log(Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} not accessible. Returning 404.", new Object[]{userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if (pricelistRow.getStatus() != GeneralPricelistPricelistRow.Status.ACTIVE) {
                LOG.log(Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} on pricelist: {2}, but row is inactivated.", new Object[]{userAPI.getId(), pricelistRow.getUniqueId(), pricelistUniqueId});
                throw validationMessageService.generateValidationException("status", "pricelistrow.inactivateRows.wrongStatus");
            }
            pricelistRow.setStatus(GeneralPricelistPricelistRow.Status.INACTIVE);
            pricelistRows.add(pricelistRow);
        }
        pricelistRows.forEach((pricelistRow) -> {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.GENERAL_PRICELIST_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        });
        return GeneralPricelistPricelistRowMapper.map(pricelistRows, true);
    }

    private List<GeneralPricelistPricelistRow> findGeneralPricelistPricelistRowsWithArticle(Long generalPricelistPricelistUniqueId, Long articleUniqueId) {
        LOG.log(Level.FINEST, "findAgreementPricelistRowsWithArticle( generalPricelistPricelistUniqueId: {0}, articleUniqueId: {1} )", new Object[]{generalPricelistPricelistUniqueId, articleUniqueId});
        return em.createNamedQuery(GeneralPricelistPricelistRow.FIND_BY_GENERAL_PRICELIST_AND_ARTICLE).
                setParameter("pricelistUniqueId", generalPricelistPricelistUniqueId).
                setParameter("articleUniqueId", articleUniqueId).
                getResultList();
    }

    /**
     * When an article is discontinued, all rows on all agreements where the
     * given article is referred must be inactivated
     *
     * @param article the article to find rows to inactivate by
     */
    public void inactivateRowsByArticle(Article article) {
        LOG.log(Level.FINEST, "inactivateRowsByArticle( article->uniqueId: {0} )", new Object[]{article.getUniqueId()});
        List<GeneralPricelistPricelistRow> generalPricelistPricelistRows =
                em.createNamedQuery(GeneralPricelistPricelistRow.FIND_BY_ARTICLE).
                        setParameter("articleUniqueId", article.getUniqueId()).
                        getResultList();

        if (generalPricelistPricelistRows != null && !generalPricelistPricelistRows.isEmpty()) {
            generalPricelistPricelistRows.forEach((generalPricelistPricelistRow) -> {
                generalPricelistPricelistRow.setStatus(GeneralPricelistPricelistRow.Status.INACTIVE);
            });
        }
    }

    /**
     * When an article is changed in its "essence" (like switches based on product)
     * all references to that article must be removed.
     *
     * @param article the article to find rows for to remove
     */
    public void deleteRowsByArticle(Article article) {
        LOG.log(Level.FINEST, "deleteRowsByArticle( article->uniqueId: {0} )", new Object[]{article.getUniqueId()});
        List<GeneralPricelistPricelistRow> generalPricelistPricelistRows =
                em.createNamedQuery(GeneralPricelistPricelistRow.FIND_BY_ARTICLE).
                        setParameter("articleUniqueId", article.getUniqueId()).
                        getResultList();
        if (generalPricelistPricelistRows != null && !generalPricelistPricelistRows.isEmpty()) {
            Iterator<GeneralPricelistPricelistRow> it = generalPricelistPricelistRows.iterator();
            while (it.hasNext()) {
                em.remove(it.next());
            }
        }
    }

    public List<String> getGeneralPricelistPricelistArticleUniqueNumbers(Long pricelistUniqueId) {
        LOG.log(Level.FINEST, "getGeneralPricelistPricelistArticleUniqueNumbers( pricelistUniqueId: {0})", new Object[]{pricelistUniqueId});
        List<String> ids = em.createNamedQuery(GeneralPricelistPricelistRow.GET_ARTICLE_UNIQUE_NUMBERS).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                getResultList();
        return ids;
    }

    public List<Long> getGeneralPricelistPricelistArticleUniqueIds(Long pricelistUniqueId) {
        LOG.log(Level.FINEST, "getGeneralPricelistPricelistArticleUniqueIds( pricelistUniqueId: {0})", new Object[]{pricelistUniqueId});
        List<Long> ids = em.createNamedQuery(GeneralPricelistPricelistRow.GET_ARTICLE_UNIQUE_IDS).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                getResultList();
        return ids;
    }

    public List<Long> getGeneralPricelistPricelistRowUniqueId(Long articleUniqueId, Long pricelistUniqueId) {
        LOG.log(Level.FINEST, "getAgreementPricelistRowUniqueId( articleUniqueId: {0}, pricelistUniqueId: {1})", new Object[] {articleUniqueId, pricelistUniqueId});
        List<Long> id = em.createNamedQuery(GeneralPricelistPricelistRow.GET_PRICELISTROW_UNIQUE_ID_BY_PRICELIST_AND_ARTICLE).
                setParameter("articleUniqueId", articleUniqueId).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                getResultList();
        return id;
    }



    /**
     * Sets overridden warranty quantity unit if it is set by the user.
     *
     * @param pricelistRow    the row to update
     * @param pricelistRowAPI user supplied data
     * @throws HjalpmedelstjanstenValidationException if unit is set but does not exist
     */
    void setOverriddenWarrantyQuantityUnit(GeneralPricelistPricelistRow pricelistRow, GeneralPricelistPricelistRowAPI pricelistRowAPI, boolean isCreate) throws HjalpmedelstjanstenValidationException {
        if (isCreate && pricelistRowAPI.getWarrantyQuantityUnit() == null) {
            // do nothing, default to override
        } else {
            if (pricelistRow.isWarrantyQuantityUnitOverridden()) {
                if (pricelistRowAPI.getWarrantyQuantityUnit() != null) {
                    CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(pricelistRowAPI.getWarrantyQuantityUnit().getId());
                    if (unit == null) {
                        LOG.log(Level.FINEST, "cannot set warranty quantity unit: {0} for pricelist row since it does not exist", new Object[]{pricelistRowAPI.getWarrantyQuantityUnit().getId()});
                        throw validationMessageService.generateValidationException("warrantyQuantityUnit", "pricelistrow.warrantyQuantityUnit.notExist");
                    }
                    pricelistRow.setWarrantyQuantityUnit(unit);
                } else {
                    pricelistRow.setWarrantyQuantityUnit(null);
                }
            } else {
                Article article = pricelistRow.getArticle();
                Article.Type articleType = article.getBasedOnProduct() == null ? article.getCategory().getArticleType() : article.getBasedOnProduct().getCategory().getArticleType();
                boolean overridden = false;
                if (pricelistRowAPI.getWarrantyQuantityUnit() == null || pricelistRowAPI.getWarrantyQuantityUnit().getId() == null) {
                    // no unit sent in API, check whether it is different from agreement
                    if (articleType == Article.Type.H && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityHUnit() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.I && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityIUnit() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.R && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityRUnit() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.T && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityTUnit() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.Tj && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityTJUnit() != null) {
                        overridden = true;
                    }
                    if (overridden) {
                        pricelistRow.setWarrantyQuantityUnit(null);
                        pricelistRow.setWarrantyQuantityUnitOverridden(true);
                    }
                } else {
                    // unit sent in API
                    if (articleType == Article.Type.H) {
                        if (pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityHUnit() == null ||
                                !pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityHUnit().getUniqueId().equals(pricelistRowAPI.getWarrantyQuantityUnit().getId())) {
                            overridden = true;
                        }
                    } else if (articleType == Article.Type.R) {
                        if (pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityRUnit() == null ||
                                !pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityRUnit().getUniqueId().equals(pricelistRowAPI.getWarrantyQuantityUnit().getId())) {
                            overridden = true;
                        }
                    } else if (articleType == Article.Type.I) {
                        if (pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityIUnit() == null ||
                                !pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityIUnit().getUniqueId().equals(pricelistRowAPI.getWarrantyQuantityUnit().getId())) {
                            overridden = true;
                        }
                    } else if (articleType == Article.Type.T) {
                        if (pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityTUnit() == null ||
                                !pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityTUnit().getUniqueId().equals(pricelistRowAPI.getWarrantyQuantityUnit().getId())) {
                            overridden = true;
                        }
                    } else if (articleType == Article.Type.Tj) {
                        if (pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityTJUnit() == null ||
                                !pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityTJUnit().getUniqueId().equals(pricelistRowAPI.getWarrantyQuantityUnit().getId())) {
                            overridden = true;
                        }
                    }
                    if (overridden) {
                        CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(pricelistRowAPI.getWarrantyQuantityUnit().getId());
                        if (unit == null) {
                            LOG.log(Level.FINEST, "cannot set warranty quantity unit: {0} for pricelist row since it does not exist", new Object[]{pricelistRowAPI.getWarrantyQuantityUnit().getId()});
                            throw validationMessageService.generateValidationException("warrantyQuantityUnit", "pricelistrow.warrantyQuantityUnit.notExist");
                        }
                        pricelistRow.setWarrantyQuantityUnit(unit);
                        pricelistRow.setWarrantyQuantityUnitOverridden(true);
                    }
                }
            }
        }
    }


    public void setOverrideWarrantyValidFrom(GeneralPricelistPricelistRowAPI pricelistRowAPI, GeneralPricelistPricelistRow pricelistRow, Article.Type articleType, UserAPI userAPI, boolean isCreate) throws HjalpmedelstjanstenValidationException {
        if (isCreate && pricelistRowAPI.getWarrantyValidFrom() == null) {
            // do nothing, default to override
        } else {
            if (pricelistRow.isWarrantyValidFromOverridden()) {
                if (pricelistRowAPI.getWarrantyValidFrom() != null) {
                    CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(pricelistRowAPI.getWarrantyValidFrom().getCode());
                    if (preventiveMaintenance == null) {
                        LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[]{userAPI.getId(), pricelistRowAPI.getWarrantyValidFrom()});
                        throw validationMessageService.generateValidationException("warrantyValidFrom", "pricelistrow.warrantyValidFrom.notExist");
                    }
                    pricelistRow.setWarrantyValidFrom(preventiveMaintenance);
                } else {
                    pricelistRow.setWarrantyValidFrom(null);
                }
            } else {
                boolean overridden = false;
                if (pricelistRowAPI.getWarrantyValidFrom() != null) {
                    CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(pricelistRowAPI.getWarrantyValidFrom().getCode());
                    if (preventiveMaintenance == null) {
                        LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[]{userAPI.getId(), pricelistRowAPI.getWarrantyValidFrom()});
                        throw validationMessageService.generateValidationException("warrantyValidFrom", "pricelistrow.warrantyValidFrom.notExist");
                    }
                    if (articleType == Article.Type.H) {
                        if (pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromH() == null) {
                            overridden = true;
                        } else {
                            if (!preventiveMaintenance.getCode().equals(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromH().getCode())) {
                                overridden = true;
                            }
                        }
                    } else if (articleType == Article.Type.I) {
                        if (pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromI() == null) {
                            overridden = true;
                        } else {
                            if (!preventiveMaintenance.getCode().equals(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromI().getCode())) {
                                overridden = true;
                            }
                        }
                    } else if (articleType == Article.Type.R) {
                        if (pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromR() == null) {
                            overridden = true;
                        } else {
                            if (!preventiveMaintenance.getCode().equals(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromR().getCode())) {
                                overridden = true;
                            }
                        }
                    } else if (articleType == Article.Type.T) {
                        if (pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromT() == null) {
                            overridden = true;
                        } else {
                            if (!preventiveMaintenance.getCode().equals(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromT().getCode())) {
                                overridden = true;
                            }
                        }
                    } else if (articleType == Article.Type.Tj) {
                        if (pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromTJ() == null) {
                            overridden = true;
                        } else {
                            if (!preventiveMaintenance.getCode().equals(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromTJ().getCode())) {
                                overridden = true;
                            }
                        }
                    }
                    if (overridden) {
                        pricelistRow.setWarrantyValidFrom(preventiveMaintenance);
                        pricelistRow.setWarrantyValidFromOverridden(true);
                    }
                } else {
                    if (articleType == Article.Type.H && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromH() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.I && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromI() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.R && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromR() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.T && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromT() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.Tj && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromTJ() != null) {
                        overridden = true;
                    }
                    if (overridden) {
                        pricelistRow.setWarrantyValidFrom(null);
                        pricelistRow.setWarrantyValidFromOverridden(true);
                    }
                }
            }
        }
    }


    /**
     * @param organization
     * @param articleUniqueId
     * @return
     */
    public List<GeneralPricelistPricelistRow> getOrganizationGeneralPricelistRowsByArticle(Organization organization, long articleUniqueId, Boolean includeAllGP) {
        LOG.log(Level.FINEST, "getGeneralPricelistRowsByArticle( articleUniqueId: {0}, organization -> uniqueId: {1} )", new Object[]{articleUniqueId, organization.getUniqueId()});

        // customers and service owner can se any gp row the article appears in, suppliers can only see their own rows
        List<GeneralPricelistPricelistRow> generalPricelistPricelistRows = null;
        boolean isCustomer = false;

        if (organization.getOrganizationType() == Organization.OrganizationType.CUSTOMER) {
            isCustomer = true;
        }

        if (isCustomer) {
            generalPricelistPricelistRows = em.createNamedQuery(GeneralPricelistPricelistRow.FIND_BY_ARTICLE_WITH_PRICE).
                    setParameter("articleUniqueId", articleUniqueId).
                    getResultList();
        } else if (organization.getOrganizationType() == Organization.OrganizationType.SUPPLIER) {
            generalPricelistPricelistRows = em.createNamedQuery(GeneralPricelistPricelistRow.FIND_BY_ARTICLE_AND_OWNER).
                    setParameter("organizationUniqueId", organization.getUniqueId()).
                    setParameter("articleUniqueId", articleUniqueId).
                    getResultList();
        } else if (organization.getOrganizationType() == Organization.OrganizationType.SERVICE_OWNER) {
            generalPricelistPricelistRows = em.createNamedQuery(GeneralPricelistPricelistRow.FIND_BY_ARTICLE).
                    setParameter("articleUniqueId", articleUniqueId).
                    getResultList();
        }
        if (generalPricelistPricelistRows != null && !includeAllGP) {
            if (organization.getOrganizationType() != Organization.OrganizationType.SERVICE_OWNER) {
                Iterator<GeneralPricelistPricelistRow> generalPricelistPricelistRowIterator = generalPricelistPricelistRows.iterator();
                while (generalPricelistPricelistRowIterator.hasNext()) {
                    GeneralPricelistPricelistRow generalPricelistPricelistRow = generalPricelistPricelistRowIterator.next();
                    GeneralPricelistPricelist currentPricelistForGeneralPricelist = generalPricelistPricelistController.getCurrentPricelist(generalPricelistPricelistRow.getPricelist().getGeneralPricelist().getOwnerOrganization().getUniqueId());
                    if (currentPricelistForGeneralPricelist == null || !currentPricelistForGeneralPricelist.equals(generalPricelistPricelistRow.getPricelist())) {
                        generalPricelistPricelistRowIterator.remove();
                    }

                }
            }
        }

        return generalPricelistPricelistRows;
    }

    public boolean existRowsByArticle(List<Long> articleUniqueIds) {
        Long count = (Long) em.createNamedQuery(GeneralPricelistPricelistRow.COUNT_BY_ARTICLES).
                setParameter("articleUniqueIds", articleUniqueIds).
                getSingleResult();
        if (count != null && count > 0) {
            return true;
        }
        return false;
    }

}
