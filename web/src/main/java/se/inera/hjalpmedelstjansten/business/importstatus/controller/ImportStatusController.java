package se.inera.hjalpmedelstjansten.business.importstatus.controller;

import se.inera.hjalpmedelstjansten.business.BaseController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.model.api.ImportStatusAPI;
import se.inera.hjalpmedelstjansten.model.entity.ImportStatus;

import jakarta.ejb.Stateless;
import jakarta.enterprise.event.Event;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;

/**
 * Class for handling business logic of dynamic text. This includes talking to the
 * database. Only service owner can handle dynamic text.
 *
 */
@Stateless
public class ImportStatusController extends BaseController {

    @Inject
    HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    Event<InternalAuditEvent> internalAuditEvent;

    public List<ImportStatus> getAllImportsStatus(String userId) {
        LOG.log(Level.FINEST, "getImportStatus(...)");
//        Query query = em.createQuery(queryString);
//        List<Object[]> importStatuss = (List<Object[]>) query.getResultList();
        List importStatus = em.createNamedQuery(ImportStatus.FIND_ALL).setParameter("changedBy",userId).setMaxResults(10).getResultList();
//        L = (List<ImportStatus>) query.getResultList();

        return importStatus;
    }


    @Transactional
    public void updateImportStatus(ImportStatusAPI importStatusAPI)  {
        LOG.log(Level.FINEST, "updating( uniqueId: {0} )", new Object[] { importStatusAPI});
        Calendar calendar = Calendar.getInstance();
        importStatusAPI.setImportEndDate(calendar.getTime());
         Query query = em.createNamedQuery(ImportStatus.UPDATE_IMPORTSTATUS).setParameter("uniqueId",importStatusAPI.getUniqueId()).setParameter("status", importStatusAPI.getReadStatus()).setParameter("endDate",importStatusAPI.getImportEndDate());
         query.executeUpdate();


    }

    public ImportStatusAPI createImportStatusForFile( ImportStatusAPI importStatusAPI){
        if(importStatusAPI != null){
            ImportStatus status = new ImportStatus();
            status.setChangedBy(importStatusAPI.getChangedBy());
            status.setImportStartDate(importStatusAPI.getImportStartDate());
            status.setFileName(importStatusAPI.getFileName());
            status.setReadStatus(importStatusAPI.getReadStatus());
            em.persist(status);
            importStatusAPI.setUniqueId(status.getUniqueId());
            return importStatusAPI;
        }else{
            return null;
        }
        }
}

