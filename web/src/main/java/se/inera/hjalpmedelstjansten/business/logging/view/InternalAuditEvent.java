package se.inera.hjalpmedelstjansten.business.logging.view;

import java.util.Date;
import java.util.Map;
import lombok.Getter;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;


@Getter
public class InternalAuditEvent {

    private Date auditTime;
    private InternalAudit.EntityType entityType;
    private Long entityId;
    private InternalAudit.ActionType actionType;
    private Long userEngagementId;
    private String sessionId;
    private String requestIp;
    private final Map<String, String> mdc;

    private InternalAuditEvent() {
        this.mdc = MdcToolkit.copyOfMdc();
    }

    public InternalAuditEvent( Date auditTime, InternalAudit.EntityType entityType, InternalAudit.ActionType actionType, Long userEngagementId, String sessionId, Long entityId, String requestIp ) {
        this();
        this.auditTime = auditTime;
        this.entityType = entityType;
        this.actionType = actionType;
        this.userEngagementId = userEngagementId;
        this.sessionId = sessionId;
        this.entityId = entityId;
        this.requestIp = requestIp;
    }
}
