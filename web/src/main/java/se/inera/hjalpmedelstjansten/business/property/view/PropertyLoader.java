package se.inera.hjalpmedelstjansten.business.property.view;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.property.AppConfigurationService;
import se.inera.hjalpmedelstjansten.property.TextController;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.inject.Inject;
import java.util.ResourceBundle;
import java.util.logging.Level;


@Startup
@Singleton
public class PropertyLoader {

    @Inject
    HjmtLogger LOG;

    @Inject
    private AppConfigurationService appConfigurationService;

    @Inject
    private TextController textController;

    private ResourceBundle messageBundle;

    @PostConstruct
    private void loadProperties() {
        appConfigurationService.loadProperties("app.properties");
        textController.loadTexts("texts.properties");
        textController.loadMessages("message.properties");
        messageBundle = ResourceBundle.getBundle("message");
    }

    public String getMessage(String key) {
        if (messageBundle.containsKey(key) ) {
            return messageBundle.getString(key);
        } else {
            LOG.log(Level.WARNING, "Missing validation message: {0}", new Object[] {key});
            return key;
        }
    }
}
