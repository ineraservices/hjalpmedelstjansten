package se.inera.hjalpmedelstjansten.business.product.service;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Collectors;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.mail.MessagingException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.NoArgsConstructor;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.user.controller.EmailController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.entity.ChangeChecker;

@Stateless
@NoArgsConstructor
public class SendMailRegardingDiscontinuedArticlesOnAssortment {

    @Inject
    HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    EmailController emailController;

    @Inject
    UserController userController;

    public void send(List<Long> articleIds){
        LOG.log(Level.FINEST, "sendMailRegardingDiscontinuedArticlesOnAssortment");
        String articleIdStr = articleIds.stream()
            .map(Object::toString)
            .collect(Collectors.joining(","));

        String sql = "SELECT DISTINCT assortmentId FROM hjmtj.AssortmentArticle";
        sql += " WHERE articleId IN (" + articleIdStr + ")";

        List<BigInteger> assortmentIds = em.createNativeQuery(sql).getResultList();

        if (assortmentIds.isEmpty()){
            LOG.log(Level.FINEST, "Found no assortments with discontinued articles");
        } else {
            LOG.log(Level.FINEST, "Found " + assortmentIds.size() + " assortments with discontinued articles");
            StringBuilder assortmentIdSB = new StringBuilder();
            for (BigInteger assortmentId : assortmentIds){
                assortmentIdSB.append(assortmentId);
                if (assortmentIds.size() - 1 != assortmentIds.indexOf(assortmentId)) {
                    assortmentIdSB.append(",");
                }
            }
            String assortmentIdStr = assortmentIdSB.toString();

            //this one finds the CustomerAssignedAssortmentManager
            sql = "SELECT DISTINCT(ue.userId) FROM hjmtj.Assortment a \n" +
                "INNER JOIN hjmtj.AssortmentUserEngagement aue ON a.uniqueId = aue.assortmentId \n" +
                "INNER JOIN hjmtj.UserEngagement ue ON aue.userEngagementId = ue.uniqueId \n" +
                "WHERE a.uniqueId IN (" + assortmentIdStr + ")";
            List<BigInteger> userIds = em.createNativeQuery(sql).getResultList();

            //this one finds the CustomerAssignedManager
            sql = "SELECT DISTINCT(ue.userId) FROM hjmtj.Assortment a \n" +
                "INNER JOIN hjmtj.UserEngagement ue ON ue.orgId = a.customerOrganizationId \n" +
                "INNER JOIN hjmtj.UserEngagementUserRole ueur ON ueur.userEngagementId = ue.uniqueId\n" +
                "INNER JOIN hjmtj.UserRole ur ON ur.uniqueId = ueur.roleId\n" +
                "WHERE a.uniqueId IN (" + assortmentIdStr + ") AND ur.name = 'CustomerAssortmentManager'\n" +
                "AND (ue.validTo IS NULL OR ue.validTo >= CURDATE())";

            List<BigInteger> userIds2 = em.createNativeQuery(sql).getResultList();

            userIds.addAll(userIds2);

            //remove dupes
            Set<BigInteger> set = new HashSet<>(userIds);
            userIds.clear();
            userIds.addAll(set);

            LOG.log(Level.FINEST, "Found " + userIds.size() + " user to send mail to, and add a line in dashboard, regarding assortments with discontinued articles");

            for (BigInteger userId : userIds) {
                try {
                    if (userId != null) {
                        String emailAddress = userController.getUserById(userId.longValue()).getElectronicAddress().getEmail();
                        long userEngagementId = userController.getUserById(userId.longValue()).getUserEngagement().getUniqueId();

                        String mailBody = "<p>Följande utbud innehåller utgångna artiklar.</p> ";
                        StringBuilder body = new StringBuilder();

                        //find assortments with discontinued articles for the users organizations assortments
                        sql = "SELECT DISTINCT(aa.assortmentId) FROM hjmtj.AssortmentArticle aa";
                        sql += " INNER JOIN hjmtj.Assortment a ON a.uniqueId = aa.assortmentId";
                        sql += " INNER JOIN hjmtj.UserEngagement ue ON a.customerOrganizationId = ue.orgId";
                        sql += " WHERE aa.articleId IN (" + articleIdStr + ") AND ue.uniqueId = " + userEngagementId;
                        assortmentIds = em.createNativeQuery(sql).getResultList();

                        for (BigInteger assortmentId : assortmentIds) {
                            long assortmentIdLong = Long.parseLong(String.valueOf(assortmentId));

                            sql = "SELECT name FROM hjmtj.Assortment WHERE uniqueId = " + assortmentId;
                            String assortmentName = em.createNativeQuery(sql).getSingleResult().toString();
                            body.append("<p><b>Utbud:</b> ").append(assortmentName).append("<br><b>Utgångna artiklar:</b> ");
                            //find the articles
                            sql = "SELECT articleId FROM hjmtj.AssortmentArticle WHERE assortmentId = " + assortmentId;
                            sql += " AND articleId IN (" + articleIdStr + ")";
                            List<BigInteger> xarticleIds = em.createNativeQuery(sql).getResultList();


                            for (BigInteger articleId : xarticleIds) {
                                sql = "SELECT articleName FROM hjmtj.Article WHERE uniqueId = " + articleId;
                                String articleName = em.createNativeQuery(sql).getSingleResult().toString();
                                body.append(articleName);
                                if (xarticleIds.size() - 1 != xarticleIds.indexOf(articleId)) {
                                    body.append(", ");
                                }
                                //add toChangeChecker (stores userEngagementId, not userId...)
                                Long articleIdLong = Long.valueOf(String.valueOf(articleId));

                                ChangeChecker cc = new ChangeChecker();
                                cc.setType("ASSORTMENT_ARTICLE_DISCONTINUED");
                                cc.setChangeId(articleIdLong);
                                cc.setUserId(userEngagementId);
                                cc.setAssortmentId(assortmentIdLong);
                                em.persist(cc);
                            }
                            body.append("</p>");
                        }
                        mailBody = mailBody + body;
                        emailController.send(emailAddress, "Utgångna artiklar på utbud", mailBody);
                    }
                } catch (MessagingException ex) {
                    LOG.log(Level.SEVERE, "Failed to send notification message to customer about discontinued articles on assortments", ex);
                }
            }
        }
    }
}
