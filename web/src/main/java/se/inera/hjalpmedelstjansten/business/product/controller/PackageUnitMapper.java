package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.LoggerService;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPackageUnitAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class PackageUnitMapper {
    
    private static final HjmtLogger LOG = LoggerService.getLogger(PackageUnitMapper.class.getName());
    
    public static final List<CVPackageUnitAPI> map(List<CVPackageUnit> packageUnits) {
        if( packageUnits == null ) {
            return null;
        }
        List<CVPackageUnitAPI> packageUnitAPIs = new ArrayList<>();
        for( CVPackageUnit packageUnit : packageUnits ) {
            packageUnitAPIs.add(map(packageUnit));
        }
        return packageUnitAPIs;
    }
    
    public static final CVPackageUnitAPI map(CVPackageUnit packageUnit) {
        LOG.log( Level.FINEST, "map(...)" );
        if( packageUnit == null ) {
            return null;
        }
        CVPackageUnitAPI packageUnitAPI = new CVPackageUnitAPI();
        packageUnitAPI.setId(packageUnit.getUniqueId());
        packageUnitAPI.setCode(packageUnit.getCode());
        packageUnitAPI.setName(packageUnit.getName());
        return packageUnitAPI;
    }
    
}
