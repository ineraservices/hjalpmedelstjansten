package se.inera.hjalpmedelstjansten.business.product.controller.exportimport;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ooxml.POIXMLProperties;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.openxmlformats.schemas.officeDocument.x2006.customProperties.CTProperty;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.MdcToolkit;
import se.inera.hjalpmedelstjansten.business.media.controller.FileUploadValidationController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;

/**
 * Class for handling business logic of importing products and articles.
 *
 */
@Stateless
public class ImportProductsArticlesController {

    @Inject
    HjmtLogger LOG;

    @Inject
    FileUploadValidationController fileUploadValidationController;

    @Inject
    ImportProductsArticlesWriterAsynchController importProductsArticlesWriterAsynchController;

    @Inject
    ValidationMessageService validationMessageService;

    public void importFile(long organizationUniqueId, MultipartFormDataInput multipartFormDataInput, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "importFile( organizationUniqueId: {0} )", new Object[] {organizationUniqueId} );
        InputStream inputStream = null;
        try {
            List<InputPart> inputParts = multipartFormDataInput.getFormDataMap().get("file");
            String contentDispositionHeader = inputParts.get(0).getHeaders().getFirst("Content-Disposition");
            String fileName = fileUploadValidationController.getFileName(contentDispositionHeader);
            String fileEnding = fileUploadValidationController.getFileEnding(fileName);
            fileUploadValidationController.validateImportFileEnding(fileEnding);
            inputStream = inputParts.get(0).getBody(InputStream.class,null);
            XSSFWorkbook workbook = (XSSFWorkbook) WorkbookFactory.create(inputStream);

            // basic validation of the file itself
            validateWorkbook(workbook);

            importProductsArticlesWriterAsynchController.readAndSaveItems(organizationUniqueId, workbook, userAPI, sessionId, requestIp, fileName, MdcToolkit.copyOfMdc());
        } catch (IOException | EncryptedDocumentException ex) {
            LOG.log(Level.SEVERE, "Failed to handle upload", ex);
            throw validationMessageService.generateValidationException("file", "import.upload.fail");
        } finally {
            try {
                if( inputStream != null ) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Failed to close inputstream on upload", ex);
            }
        }
    }

    private void validateWorkbook(XSSFWorkbook workbook) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateWorkbook(...)");
        // workbook must have more than 1 sheet
        LOG.log( Level.FINEST, "Workbook contains {0} sheets", new Object[] {workbook.getNumberOfSheets()});

        POIXMLProperties properties = workbook.getProperties();
        POIXMLProperties.CustomProperties customProperties = properties.getCustomProperties();

        // validate type, this is to avoid users uploading pricelist rows to products upload
        // and vice versa
        CTProperty workbookExportTypeProperty = customProperties.getProperty(ExportImportProductsArticlesController.EXPORT_TYPE_NAME);
        if( workbookExportTypeProperty == null ) {
            throw validationMessageService.generateValidationException("file", "import.productarticle.invalidfile.missingtype");
        }
        String exportType = workbookExportTypeProperty.getLpwstr();
        if( exportType == null || !ExportImportProductsArticlesController.EXPORT_TYPE.equals(exportType) ) {
            throw validationMessageService.generateValidationException("file", "import.productarticle.invalidfile.invalidtype");
        }

        // validate version of excel file, version is an own concept that each time we make a change
        // that may break backwards compatibility we change version to avoid users to
        // use old excel files
        CTProperty workbookExportVersionProperty = customProperties.getProperty(ExportImportProductsArticlesController.EXPORT_VERSION_NAME);
        if( workbookExportVersionProperty == null ) {
            throw validationMessageService.generateValidationException("file", "import.productarticle.invalidfile.invalidversion");
        }
        String exportVersion = workbookExportVersionProperty.getLpwstr();
        if( exportVersion == null || !ExportImportProductsArticlesController.EXPORT_VERSION.equals(exportVersion) ) {
            throw validationMessageService.generateValidationException("file", "import.productarticle.invalidfile.invalidversion");
        }

        // validate document basics
        if( workbook.getNumberOfSheets() < 2 ) {
            throw validationMessageService.generateValidationException("file", "import.productarticle.invalidfile.numberofsheets");
        }
        if( workbook.getSheet(ExportImportProductsArticlesController.VALUELIST_SHEET_NAME) == null ) {
            throw validationMessageService.generateValidationException("file", "import.productarticle.invalidfile.novaluelistsheet");
        }
        if( workbook.getSheet(ExportImportProductsArticlesController.CATEGORIES_SHEET_NAME) == null ) {
            throw validationMessageService.generateValidationException("file", "import.productarticle.invalidfile.nocategoriessheet");
        }
    }

}
