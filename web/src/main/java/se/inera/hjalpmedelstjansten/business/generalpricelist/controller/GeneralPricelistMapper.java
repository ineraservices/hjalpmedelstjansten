package se.inera.hjalpmedelstjansten.business.generalpricelist.controller;

import se.inera.hjalpmedelstjansten.business.DateUtils;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.PreventiveMaintenanceMapper;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistAPI;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelist;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Class for mapping between API and Entity classes
 * 
 */
public class GeneralPricelistMapper {
    
    public static final GeneralPricelistAPI map(GeneralPricelist generalPricelist, boolean includeEverything) {
        return map(generalPricelist, includeEverything, null, null);
    }    
    
    public static final GeneralPricelistAPI map(GeneralPricelist generalPricelist, boolean includeEverything, GeneralPricelistPricelist currentPricelist, Long numberOfPricelistRows) {
        if( generalPricelist == null ) {
            return null;
        }
        GeneralPricelistAPI generalPricelistAPI = new GeneralPricelistAPI();
        generalPricelistAPI.setId(generalPricelist.getUniqueId());
        generalPricelistAPI.setGeneralPricelistName(generalPricelist.getGeneralPricelistName());
        generalPricelistAPI.setGeneralPricelistNumber(generalPricelist.getGeneralPricelistNumber());
        generalPricelistAPI.setValidFrom(generalPricelist.getValidFrom().getTime());
        generalPricelistAPI.setValidTo(generalPricelist.getValidTo().getTime());
        generalPricelistAPI.setOwnerOrganization(OrganizationMapper.map(generalPricelist.getOwnerOrganization(), false));
        if( includeEverything ) {
            // delivery times
            generalPricelistAPI.setDeliveryTimeH(generalPricelist.getDeliveryTimeH());
            generalPricelistAPI.setDeliveryTimeT(generalPricelist.getDeliveryTimeT());
            generalPricelistAPI.setDeliveryTimeI(generalPricelist.getDeliveryTimeI());
            generalPricelistAPI.setDeliveryTimeR(generalPricelist.getDeliveryTimeR());
            generalPricelistAPI.setDeliveryTimeTJ(generalPricelist.getDeliveryTimeTJ());

            // warranty quantity
            generalPricelistAPI.setWarrantyQuantityH(generalPricelist.getWarrantyQuantityH());
            generalPricelistAPI.setWarrantyQuantityT(generalPricelist.getWarrantyQuantityT());
            generalPricelistAPI.setWarrantyQuantityI(generalPricelist.getWarrantyQuantityI());
            generalPricelistAPI.setWarrantyQuantityR(generalPricelist.getWarrantyQuantityR());
            generalPricelistAPI.setWarrantyQuantityTJ(generalPricelist.getWarrantyQuantityTJ());

            // warranty valid from
            if( generalPricelist.getWarrantyValidFromH() != null ) {
                generalPricelistAPI.setWarrantyValidFromH(PreventiveMaintenanceMapper.map(generalPricelist.getWarrantyValidFromH()));
            }
            if( generalPricelist.getWarrantyValidFromT() != null ) {
                generalPricelistAPI.setWarrantyValidFromT(PreventiveMaintenanceMapper.map(generalPricelist.getWarrantyValidFromT()));
            }
            if( generalPricelist.getWarrantyValidFromI() != null ) {
                generalPricelistAPI.setWarrantyValidFromI(PreventiveMaintenanceMapper.map(generalPricelist.getWarrantyValidFromI()));
            }
            if( generalPricelist.getWarrantyValidFromR() != null ) {
                generalPricelistAPI.setWarrantyValidFromR(PreventiveMaintenanceMapper.map(generalPricelist.getWarrantyValidFromR()));
            }
            if( generalPricelist.getWarrantyValidFromTJ() != null ) {
                generalPricelistAPI.setWarrantyValidFromTJ(PreventiveMaintenanceMapper.map(generalPricelist.getWarrantyValidFromTJ()));
            }

            // warranty terms
            generalPricelistAPI.setWarrantyTermsH(generalPricelist.getWarrantyTermsH());
            generalPricelistAPI.setWarrantyTermsT(generalPricelist.getWarrantyTermsT());
            generalPricelistAPI.setWarrantyTermsI(generalPricelist.getWarrantyTermsI());
            generalPricelistAPI.setWarrantyTermsR(generalPricelist.getWarrantyTermsR());
            generalPricelistAPI.setWarrantyTermsTJ(generalPricelist.getWarrantyTermsTJ());

            // warranty quantity units
            generalPricelistAPI.setWarrantyQuantityHUnit(GuaranteeUnitMapper.map(generalPricelist.getWarrantyQuantityHUnit()));
            generalPricelistAPI.setWarrantyQuantityTUnit(GuaranteeUnitMapper.map(generalPricelist.getWarrantyQuantityTUnit()));
            generalPricelistAPI.setWarrantyQuantityIUnit(GuaranteeUnitMapper.map(generalPricelist.getWarrantyQuantityIUnit()));
            generalPricelistAPI.setWarrantyQuantityRUnit(GuaranteeUnitMapper.map(generalPricelist.getWarrantyQuantityRUnit()));
            generalPricelistAPI.setWarrantyQuantityTJUnit(GuaranteeUnitMapper.map(generalPricelist.getWarrantyQuantityTJUnit()));
            
            generalPricelistAPI.setHasPricelistRows(numberOfPricelistRows == null ? false: numberOfPricelistRows > 0);
            
        }
        if( currentPricelist != null ) {
            generalPricelistAPI.setCurrentPricelist(GeneralPricelistPricelistMapper.mapWithStatus(currentPricelist, false, GeneralPricelistPricelist.Status.CURRENT, null));
        }
        return generalPricelistAPI;
    }

    public static final GeneralPricelist map(GeneralPricelistAPI generalPricelistAPI) {
        if( generalPricelistAPI == null ) {
            return null;
        }
        GeneralPricelist generalPricelist = new GeneralPricelist();
        generalPricelist.setGeneralPricelistName(generalPricelistAPI.getGeneralPricelistName());
        generalPricelist.setGeneralPricelistNumber(generalPricelistAPI.getGeneralPricelistNumber());
        generalPricelist.setValidFrom(DateUtils.beginningOfDay(generalPricelistAPI.getValidFrom()));
        generalPricelist.setValidTo(DateUtils.endOfDay(generalPricelistAPI.getValidTo()));

        mapDeliveryAndWarrantyFields(generalPricelistAPI, generalPricelist);

        return generalPricelist;
    }
    
    public static void mapDeliveryAndWarrantyFields(GeneralPricelistAPI generalPricelistAPI, GeneralPricelist generalPricelist ) {
        // delivery times
        generalPricelist.setDeliveryTimeH(generalPricelistAPI.getDeliveryTimeH());
        generalPricelist.setDeliveryTimeT(generalPricelistAPI.getDeliveryTimeT());
        generalPricelist.setDeliveryTimeI(generalPricelistAPI.getDeliveryTimeI());
        generalPricelist.setDeliveryTimeR(generalPricelistAPI.getDeliveryTimeR());
        generalPricelist.setDeliveryTimeTJ(generalPricelistAPI.getDeliveryTimeTJ());
        
        // warranty quantity
        generalPricelist.setWarrantyQuantityH(generalPricelistAPI.getWarrantyQuantityH());
        generalPricelist.setWarrantyQuantityT(generalPricelistAPI.getWarrantyQuantityT());
        generalPricelist.setWarrantyQuantityI(generalPricelistAPI.getWarrantyQuantityI());
        generalPricelist.setWarrantyQuantityR(generalPricelistAPI.getWarrantyQuantityR());
        generalPricelist.setWarrantyQuantityTJ(generalPricelistAPI.getWarrantyQuantityTJ());
        
        // warranty terms
        generalPricelist.setWarrantyTermsH(generalPricelistAPI.getWarrantyTermsH());
        generalPricelist.setWarrantyTermsT(generalPricelistAPI.getWarrantyTermsT());
        generalPricelist.setWarrantyTermsI(generalPricelistAPI.getWarrantyTermsI());
        generalPricelist.setWarrantyTermsR(generalPricelistAPI.getWarrantyTermsR());
        generalPricelist.setWarrantyTermsTJ(generalPricelistAPI.getWarrantyTermsTJ());
    }

    public static Agreement.Status getAgreementStatus(Agreement agreement) {
        Instant nowInstant = Instant.now();
        ZoneId zoneId = ZoneId.systemDefault();
        if( agreement.getEndDate() != null ) {
            ZonedDateTime now = ZonedDateTime.ofInstant(nowInstant, zoneId).toLocalDate().atStartOfDay(zoneId);
            Instant endDateInstant = Instant.ofEpochMilli(agreement.getEndDate().getTime());
            ZonedDateTime endDate = ZonedDateTime.ofInstant(endDateInstant, zoneId);
            if( now.isAfter(endDate) || now.isEqual(endDate) ) {
                return Agreement.Status.DISCONTINUED;
            }
        }
        ZonedDateTime now = ZonedDateTime.ofInstant(nowInstant, zoneId).toLocalDate().atStartOfDay(zoneId);
        Instant validToInstant = Instant.ofEpochMilli(agreement.getValidTo().getTime());
        ZonedDateTime validTo = ZonedDateTime.ofInstant(validToInstant, zoneId);
        if( now.isAfter(validTo) || now.isEqual(validTo) ) {
            return Agreement.Status.DISCONTINUED;
        }
        ZonedDateTime tomorrowStartOfDay = ZonedDateTime.ofInstant(nowInstant, zoneId).toLocalDate().plusDays(1).atStartOfDay(zoneId);
        Instant validFromInstant = Instant.ofEpochMilli(agreement.getValidFrom().getTime());
        ZonedDateTime validFrom = ZonedDateTime.ofInstant(validFromInstant, zoneId);
        if( validFrom.isBefore(tomorrowStartOfDay) ) {
            return Agreement.Status.CURRENT;
        } else {
            return Agreement.Status.FUTURE;
        }
    }
    
}
