package se.inera.hjalpmedelstjansten.business.generalpricelist.controller;

import se.inera.hjalpmedelstjansten.business.product.controller.ArticleMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.PreventiveMaintenanceMapper;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelistRow;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Class for mapping between API and Entity classes
 * 
 */
public class GeneralPricelistPricelistRowMapper {
    
    public static final List<GeneralPricelistPricelistRowAPI> map(List<GeneralPricelistPricelistRow> pricelistRows, boolean includeEverything) {
        return map(pricelistRows, includeEverything, false);
    }
    
    public static final List<GeneralPricelistPricelistRowAPI> map(List<GeneralPricelistPricelistRow> pricelistRows, boolean includeEverything, boolean includeGeneralPricelist) {
        if( pricelistRows == null ) {
            return null;
        }
        List<GeneralPricelistPricelistRowAPI> pricelistRowAPIs = new ArrayList<>();
        for( GeneralPricelistPricelistRow  pricelistRow : pricelistRows ) {
            pricelistRowAPIs.add(map(pricelistRow, includeEverything, includeGeneralPricelist));
        }
        return pricelistRowAPIs;
    }
    
    public static final GeneralPricelistPricelistRowAPI map(GeneralPricelistPricelistRow pricelistRow, boolean includeEverything) {
        return map(pricelistRow, includeEverything, false);
    }
    
    public static final GeneralPricelistPricelistRowAPI map(GeneralPricelistPricelistRow pricelistRow, boolean includeEverything, boolean includeGeneralPricelist) {
        if( pricelistRow == null ) {
            return null;
        }
        GeneralPricelistPricelistRowAPI pricelistRowAPI = new GeneralPricelistPricelistRowAPI();
        pricelistRowAPI.setId(pricelistRow.getUniqueId());
        pricelistRowAPI.setArticle(ArticleMapper.map(pricelistRow.getArticle(), true, null));
        pricelistRowAPI.setPrice(pricelistRow.getPrice());
        pricelistRowAPI.setLeastOrderQuantity(pricelistRow.getLeastOrderQuantity());
        pricelistRowAPI.setStatus(pricelistRow.getStatus().toString());
        if( pricelistRow.getValidFrom() != null ) {
            pricelistRowAPI.setValidFrom(pricelistRow.getValidFrom().getTime());
        } else {
            pricelistRowAPI.setValidFrom(pricelistRow.getPricelist().getValidFrom().getTime());
        }
        
        // possible overridden fields
        Article.Type articleType = pricelistRow.getArticle().getBasedOnProduct() == null ? pricelistRow.getArticle().getCategory().getArticleType(): pricelistRow.getArticle().getBasedOnProduct().getCategory().getArticleType();        
        setDeliveryTime(pricelistRow, pricelistRowAPI, articleType);
        setWarrantyQuantity(pricelistRow, pricelistRowAPI, articleType);
        setWarrantyTerms(pricelistRow, pricelistRowAPI, articleType);
        setWarrantyQuantityUnit(pricelistRow, pricelistRowAPI, articleType);
        setWarrantyValidFrom(pricelistRow, pricelistRowAPI, articleType);
        
        if( includeEverything ) {
            pricelistRowAPI.setPricelist(GeneralPricelistPricelistMapper.mapWithStatus(pricelistRow.getPricelist(), includeGeneralPricelist, null, null));            
        }
        
        return pricelistRowAPI;
    }

    public static final GeneralPricelistPricelistRow map(GeneralPricelistPricelistRowAPI pricelistRowAPI, Date validFrom) {
        if( pricelistRowAPI == null ) {
            return null;
        }
        GeneralPricelistPricelistRow pricelistRow = new GeneralPricelistPricelistRow();
        pricelistRow.setPrice(pricelistRowAPI.getPrice());
        pricelistRow.setLeastOrderQuantity(pricelistRowAPI.getLeastOrderQuantity());
        pricelistRow.setValidFrom(validFrom);
        pricelistRow.setStatus(GeneralPricelistPricelistRow.Status.valueOf(pricelistRowAPI.getStatus()));
        return pricelistRow;
    }

    public static final void mapUpdatableFields(GeneralPricelistPricelistRowAPI pricelistRowAPI, GeneralPricelistPricelistRow pricelistRow, Article.Type articleType, boolean isCreate) {
        if( pricelistRow.isValidFromOverridden() ) {
            pricelistRow.setValidFrom(pricelistRowAPI.getValidFrom() == null ? null: new Date(pricelistRowAPI.getValidFrom()));
        } else {
            boolean overridden = false;
            if( pricelistRowAPI.getValidFrom() != null ) {
                if( pricelistRow.getPricelist().getValidFrom() == null ) {
                    overridden = true;
                } else {
                    if( !pricelistRowAPI.getValidFrom().equals(pricelistRow.getPricelist().getValidFrom().getTime()) ) {
                        overridden = true;
                    }
                }
            }
            if( overridden ) {
                pricelistRow.setValidFrom(new Date(pricelistRowAPI.getValidFrom()));
                pricelistRow.setValidFromOverridden(true);
            }
        }
        
        setOverrideDeliveryTime(pricelistRowAPI, pricelistRow, articleType, isCreate);
        setOverrideWarrantyQuantity(pricelistRowAPI, pricelistRow, articleType, isCreate);
        setOverrideWarrantyTerms(pricelistRowAPI, pricelistRow, articleType, isCreate);
    }

    private static void setDeliveryTime(GeneralPricelistPricelistRow pricelistRow, GeneralPricelistPricelistRowAPI pricelistRowAPI, Article.Type articleType) {
        if( pricelistRow.isDeliveryTimeOverridden() ) {
            pricelistRowAPI.setDeliveryTime(pricelistRow.getDeliveryTime());
        } else {
            if( articleType == Article.Type.H ) {
                pricelistRowAPI.setDeliveryTime(pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeH());
            } else if( articleType == Article.Type.I ) {
                pricelistRowAPI.setDeliveryTime(pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeI());
            } else if( articleType == Article.Type.R ) {
                pricelistRowAPI.setDeliveryTime(pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeR());
            } else if( articleType == Article.Type.T ) {
                pricelistRowAPI.setDeliveryTime(pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeT());
            } else if( articleType == Article.Type.Tj ) {
                pricelistRowAPI.setDeliveryTime(pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeTJ());
            }
        }
    }

    private static void setWarrantyQuantity(GeneralPricelistPricelistRow pricelistRow, GeneralPricelistPricelistRowAPI pricelistRowAPI, Article.Type articleType) {
        if( pricelistRow.isWarrantyQuantityOverridden() ) {
            pricelistRowAPI.setWarrantyQuantity(pricelistRow.getWarrantyQuantity());
        } else {
            if( articleType == Article.Type.H ) {
                pricelistRowAPI.setWarrantyQuantity(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityH());
            } else if( articleType == Article.Type.I ) {
                pricelistRowAPI.setWarrantyQuantity(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityI());
            } else if( articleType == Article.Type.R ) {
                pricelistRowAPI.setWarrantyQuantity(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityR());
            } else if( articleType == Article.Type.T ) {
                pricelistRowAPI.setWarrantyQuantity(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityT());
            } else if( articleType == Article.Type.Tj ) {
                pricelistRowAPI.setWarrantyQuantity(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityTJ());
            }
        }
    }

    private static void setWarrantyTerms(GeneralPricelistPricelistRow pricelistRow, GeneralPricelistPricelistRowAPI pricelistRowAPI, Article.Type articleType) {
        if( pricelistRow.isWarrantyTermsOverridden() ) {
            pricelistRowAPI.setWarrantyTerms(pricelistRow.getWarrantyTerms());
        } else {
            if( articleType == Article.Type.H ) {
                pricelistRowAPI.setWarrantyTerms(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsH());
            } else if( articleType == Article.Type.I ) {
                pricelistRowAPI.setWarrantyTerms(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsI());
            } else if( articleType == Article.Type.R ) {
                pricelistRowAPI.setWarrantyTerms(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsR());
            } else if( articleType == Article.Type.T ) {
                pricelistRowAPI.setWarrantyTerms(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsT());
            } else if( articleType == Article.Type.Tj ) {
                pricelistRowAPI.setWarrantyTerms(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsTJ());
            }
        }
    }

    private static void setWarrantyQuantityUnit(GeneralPricelistPricelistRow pricelistRow, GeneralPricelistPricelistRowAPI pricelistRowAPI, Article.Type articleType) {        
        if( pricelistRow.isWarrantyQuantityUnitOverridden() ) {
            pricelistRowAPI.setWarrantyQuantityUnit(GuaranteeUnitMapper.map(pricelistRow.getWarrantyQuantityUnit()));
        } else {
            if( articleType == Article.Type.H ) {
                pricelistRowAPI.setWarrantyQuantityUnit(GuaranteeUnitMapper.map(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityHUnit()));
            } else if( articleType == Article.Type.I ) {
                pricelistRowAPI.setWarrantyQuantityUnit(GuaranteeUnitMapper.map(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityIUnit()));
            } else if( articleType == Article.Type.R ) {
                pricelistRowAPI.setWarrantyQuantityUnit(GuaranteeUnitMapper.map(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityRUnit()));
            } else if( articleType == Article.Type.T ) {
                pricelistRowAPI.setWarrantyQuantityUnit(GuaranteeUnitMapper.map(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityTUnit()));
            } else if( articleType == Article.Type.Tj ) {
                pricelistRowAPI.setWarrantyQuantityUnit(GuaranteeUnitMapper.map(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityTJUnit()));
            }
        }
    }

    private static void setWarrantyValidFrom(GeneralPricelistPricelistRow pricelistRow, GeneralPricelistPricelistRowAPI pricelistRowAPI, Article.Type articleType) {
        if( pricelistRow.isWarrantyValidFromOverridden() ) {
            pricelistRowAPI.setWarrantyValidFrom(PreventiveMaintenanceMapper.map(pricelistRow.getWarrantyValidFrom() == null ? null: pricelistRow.getWarrantyValidFrom()));
        } else {
            if( articleType == Article.Type.H ) {
                if( pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromH() != null ) {
                    pricelistRowAPI.setWarrantyValidFrom(PreventiveMaintenanceMapper.map(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromH()));
                }
            } else if( articleType == Article.Type.I ) {
                if( pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromI() != null ) {
                    pricelistRowAPI.setWarrantyValidFrom(PreventiveMaintenanceMapper.map(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromI()));
                }
            } else if( articleType == Article.Type.R ) {
                if( pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromR() != null ) {
                    pricelistRowAPI.setWarrantyValidFrom(PreventiveMaintenanceMapper.map(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromR()));
                }
            } else if( articleType == Article.Type.T ) {
                if( pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromT() != null ) {
                    pricelistRowAPI.setWarrantyValidFrom(PreventiveMaintenanceMapper.map(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromT()));
                }
            } else if( articleType == Article.Type.Tj ) {
                if( pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromTJ() != null ) {
                    pricelistRowAPI.setWarrantyValidFrom(PreventiveMaintenanceMapper.map(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyValidFromTJ()));
                }
            }
        }
    }

    private static void setOverrideDeliveryTime(GeneralPricelistPricelistRowAPI pricelistRowAPI, GeneralPricelistPricelistRow pricelistRow, Article.Type articleType, boolean isCreate) {
        if( isCreate && pricelistRowAPI.getDeliveryTime() == null ) {
            // do nothing, default to override
        } else {
            if( pricelistRow.isDeliveryTimeOverridden() ) {
                pricelistRow.setDeliveryTime(pricelistRowAPI.getDeliveryTime());
            } else {
                boolean overridden = false;
                if( pricelistRowAPI.getDeliveryTime() != null ) {
                    if( articleType == Article.Type.H ) {
                        if( pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeH() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getDeliveryTime().equals(pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeH()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.I ) {
                        if( pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeI() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getDeliveryTime().equals(pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeI()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.R ) {
                        if( pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeR() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getDeliveryTime().equals(pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeR()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.T ) {
                        if( pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeT() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getDeliveryTime().equals(pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeT()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.Tj ) {
                        if( pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeTJ() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getDeliveryTime().equals(pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeTJ()) ) {
                                overridden = true;
                            }
                        }
                    }
                    if( overridden ) {
                        pricelistRow.setDeliveryTime(pricelistRowAPI.getDeliveryTime());
                        pricelistRow.setDeliveryTimeOverridden(true);
                    }
                } else {
                    if( articleType == Article.Type.H && pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeH() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.I && pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeI() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.R && pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeR() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.T && pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeT() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.Tj && pricelistRow.getPricelist().getGeneralPricelist().getDeliveryTimeTJ() != null ) {
                        overridden = true;
                    }
                    if( overridden ) {
                        pricelistRow.setDeliveryTime(null);
                        pricelistRow.setDeliveryTimeOverridden(true);
                    }
                }
            }
        }
    }

    private static void setOverrideWarrantyQuantity(GeneralPricelistPricelistRowAPI pricelistRowAPI, GeneralPricelistPricelistRow pricelistRow, Article.Type articleType, boolean isCreate) {
        if( isCreate && pricelistRowAPI.getWarrantyQuantity() == null ) {
            // do nothing, default to override
        } else {
            if( pricelistRow.isWarrantyQuantityOverridden() ) {
                pricelistRow.setWarrantyQuantity(pricelistRowAPI.getWarrantyQuantity());
            } else {
                boolean overridden = false;
                if( pricelistRowAPI.getWarrantyQuantity() != null ) {                
                    if( articleType == Article.Type.H ) {
                        if( pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityH() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyQuantity().equals(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityH()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.I ) {
                        if( pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityI() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyQuantity().equals(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityI()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.R ) {
                        if( pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityR() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyQuantity().equals(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityR()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.T ) {
                        if( pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityT() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyQuantity().equals(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityT()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.Tj ) {
                        if( pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityTJ() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyQuantity().equals(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityTJ()) ) {
                                overridden = true;
                            }
                        }
                    }
                    if( overridden ) {
                        pricelistRow.setWarrantyQuantity(pricelistRowAPI.getWarrantyQuantity());
                        pricelistRow.setWarrantyQuantityOverridden(true);
                    }
                } else {
                    if( articleType == Article.Type.H && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityH() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.I && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityI() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.R && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityR() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.T && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityT() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.Tj && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyQuantityTJ() != null ) {
                        overridden = true;
                    }
                    if( overridden ) {
                        pricelistRow.setWarrantyQuantity(null);
                        pricelistRow.setWarrantyQuantityOverridden(true);
                    }
                }
            }
        }
    }

    private static void setOverrideWarrantyTerms(GeneralPricelistPricelistRowAPI pricelistRowAPI, GeneralPricelistPricelistRow pricelistRow, Article.Type articleType, boolean isCreate) {
        if( isCreate && pricelistRowAPI.getWarrantyTerms() == null ) {
            // do nothing, default to override
        } else {
            if( pricelistRow.isWarrantyTermsOverridden() ) {
                pricelistRow.setWarrantyTerms(pricelistRowAPI.getWarrantyTerms());
            } else {
                boolean overridden = false;
                if( pricelistRowAPI.getWarrantyTerms() != null ) {                
                    if( articleType == Article.Type.H ) {
                        if( pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsH() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyTerms().equals(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsH()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.I ) {
                        if( pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsI() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyTerms().equals(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsI()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.R ) {
                        if( pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsR() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyTerms().equals(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsR()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.T ) {
                        if( pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsT() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyTerms().equals(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsT()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.Tj ) {
                        if( pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsTJ() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyTerms().equals(pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsTJ()) ) {
                                overridden = true;
                            }
                        }
                    }
                    if( overridden ) {
                        pricelistRow.setWarrantyTerms(pricelistRowAPI.getWarrantyTerms());
                        pricelistRow.setWarrantyTermsOverridden(true);
                    }
                } else {
                    if( articleType == Article.Type.H && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsH() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.I && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsI() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.R && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsR() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.T && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsT() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.Tj && pricelistRow.getPricelist().getGeneralPricelist().getWarrantyTermsTJ() != null ) {
                        overridden = true;
                    }
                    if( overridden ) {
                        pricelistRow.setWarrantyTerms(null);
                        pricelistRow.setWarrantyTermsOverridden(true);
                    }
                }
            }
        }
    }
    
}
