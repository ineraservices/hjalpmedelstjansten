package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.LoggerService;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.entity.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Class for mapping between API and Entity classes
 *
 */
public class CategoryMapper {
    
    private static final HjmtLogger LOG = LoggerService.getLogger(CategoryMapper.class.getName());
    
    public static final List<CategoryAPI> map(List<Category> categories) {
        LOG.log( Level.FINEST, "map(...)" );
        if( categories == null || categories.isEmpty() ) {
            return null;
        }
        List<CategoryAPI> categoryAPIs = new ArrayList<>();
        for( Category category : categories ) {
            categoryAPIs.add(map(category));
        }
        return categoryAPIs;
    }
    
    public static final CategoryAPI map(Category category) {
        LOG.log( Level.FINEST, "map(...)" );
        if( category == null ) {
            return null;
        }
        CategoryAPI categoryAPI = new CategoryAPI();        
        categoryAPI.setId(category.getUniqueId());
        categoryAPI.setName(category.getName());
        categoryAPI.setCode(category.getCode());
        categoryAPI.setDescription(category.getDescription());
        categoryAPI.setArticleType(category.getArticleType());
        return categoryAPI;
    }

    public static final Category map(CategoryAPI categoryApi) {
        LOG.log( Level.FINEST, "map(...)" );
        if( categoryApi == null ) {
            return null;
        }
        Category category = new Category();
        category.setUniqueId(categoryApi.getId());
        category.setName(categoryApi.getName());
        category.setCode(categoryApi.getCode());
        category.setDescription(categoryApi.getDescription());
        category.setArticleType(categoryApi.getArticleType());

        return category;
    }
    
}
