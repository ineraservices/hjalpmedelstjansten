package se.inera.hjalpmedelstjansten.business.product.controller;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import jakarta.ejb.DependsOn;
import jakarta.ejb.ScheduleExpression;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.ejb.Timeout;
import jakarta.ejb.TimerConfig;
import jakarta.ejb.TimerService;
import jakarta.inject.Inject;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.product.service.DiscontinueArticlesService;
import se.inera.hjalpmedelstjansten.clustering.ClusterController;

/**
 * Timer for discontinuing articles where the replacementDate is passed.
 *
 */
@Singleton
@Startup
@DependsOn("PropertyLoader")
public class ArticleDiscontinuedTimer {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private boolean articleDiscontinuedTimerEnabled;

    @Inject
    private String articleDiscontinuedTimerHour;

    @Inject
    private String articleDiscontinuedTimerMinute;

    @Inject
    private String articleDiscontinuedTimerSecond;

    @Inject
    private DiscontinueArticlesService discontinueArticlesService;

    @Inject
    private ClusterController clusterController;

    @Resource
    private TimerService timerService;

    @Timeout
    @TransactionTimeout(value=60, unit = TimeUnit.MINUTES)
    private void schedule() {
        LOG.log( Level.FINEST, "schedule" );
        // attempt to get lock
        boolean lockReceived = clusterController.getLock(this.getClass().getName(), 30);
        if( lockReceived ) {
            LOG.log( Level.FINEST, "Received lock!" );
            long start = System.currentTimeMillis();

            try {
                discontinueArticlesService.discontinue();
            } catch(Exception ex) {
                LOG.log(Level.SEVERE, "Job to discontinue articles failed!", ex);
            }

            long total = System.currentTimeMillis() - start;
            if( total > 60000 ) {
                LOG.log( Level.WARNING, "Scheduled job took: {0} ms which is more than 1 minute which is a long time, a developer should take a look at this.", new Object[] {total});
            } else {
                LOG.log( Level.INFO, "Scheduled job took: {0} ms.", new Object[] {total});
            }
            //release lock
            clusterController.releaseLock(this.getClass().getName());
        } else {
            LOG.log( Level.INFO, "Did not receive lock!" );
        }
    }

    @PostConstruct
    private void initialize() {
        LOG.log( Level.FINEST, "initialize()" );
        if( articleDiscontinuedTimerEnabled ) {
            LOG.log( Level.FINEST, "Timer is ENABLED" );
            if (timerService.getTimers().isEmpty()) {
                String name = this.getClass().getName();
                TimerConfig configuration = new TimerConfig();
                configuration.setPersistent(false);
                configuration.setInfo(name);
                ScheduleExpression scheduleExpression = new ScheduleExpression();
                scheduleExpression.hour(articleDiscontinuedTimerHour).minute(articleDiscontinuedTimerMinute).second(articleDiscontinuedTimerSecond);
                timerService.createCalendarTimer(scheduleExpression, configuration);
            }
        } else {
            LOG.log( Level.INFO, "Timer is NOT ENABLED" );
        }
    }

}
