package se.inera.hjalpmedelstjansten.business.media.controller;

import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaDocument;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaImage;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.DependsOn;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.inject.Inject;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

@Singleton
@Startup
@DependsOn("PropertyLoader")
public class FileUploadValidationController {

    @Inject
    private HjmtLogger LOG;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    private String validDocumentFileEndingsAndCorrespondingFileTypes;

    @Inject
    private String validImageFileEndings;

    @Inject
    private String validImportFileEndings;

    private final List<String> validDocumentFileEndings = new ArrayList<>();
    private final Map<String,MediaDocument.FileType> documentFileEndingsToFileTypes = new HashMap<>();
    private final List<String> validImageFileEndingsList = new ArrayList<>();
    private final List<String> validImportFileEndingsList = new ArrayList<>();

    public URL validateUrl( String urlString ) throws HjalpmedelstjanstenValidationException {

        try {
            URL url = new URL(urlString);
            if( url.getPath() == null || url.getPath().isEmpty()) {
                throw validationMessageService.generateValidationException("url", "media.url.invalid");
            }

            return url;
        } catch (MalformedURLException ex) {
            throw validationMessageService.generateValidationException("url", "media.url.invalid");
        }
    }

    public void validateMainImageAltText (MediaImage mediaImage) throws HjalpmedelstjanstenValidationException {
        HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
        if (mediaImage.isMainImage() && mediaImage.getAlternativeText() == null) {
//            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("mainImageAltText", validationMessageService.getMessage("mainImage.altText.notNull")));
            validationMessageService.generateValidationException("mainImageAltText", "mainImage.altText.notNull");
            HjalpmedelstjanstenValidationException.generateValidationMessage("mainImageAltText", validationMessageService.getMessage("mainImage.altText.notNull"));
            throw new HjalpmedelstjanstenValidationException(validationMessageService.getMessage("mainImage.altText.notNull"));
        }
        throw exception;
    }

    public void validateMainImageUploadLink (MediaImage mediaImage, String urlParam, boolean isFile) throws HjalpmedelstjanstenValidationException {
        if (mediaImage.isMainImage() && urlParam == null && isFile) {
//            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("mainImageAltText", validationMessageService.getMessage("mainImage.altText.notNull")));
            throw validationMessageService.generateValidationException("mainImageUploadLink", "mainImage.uploadLink.notNull");
        }
    }

    public void validateImageFileEnding( String fileEnding, String field ) throws HjalpmedelstjanstenValidationException {

        if( fileEnding == null || fileEnding.isEmpty() ) {
            throw validationMessageService.generateValidationException(field, "media.image.fileending.empty");
        }

        if( !validImageFileEndingsList.contains(fileEnding.toLowerCase()) ) {
            throw validationMessageService.generateValidationException(field, "media.image.fileending.invalid");
        }
    }

    public void validateDocumentFileEnding( String fileEnding, String field ) throws HjalpmedelstjanstenValidationException {
        if( fileEnding == null || fileEnding.isEmpty() ) {
            throw validationMessageService.generateValidationException(field, "media.document.fileending.empty");
        }
        if( !validDocumentFileEndings.contains(fileEnding.toLowerCase()) ) {
            throw validationMessageService.generateValidationException(field, "media.document.fileending.invalid");
        }
    }

    public MediaDocument.FileType getDocumentFileType(String fileEnding) {
        return documentFileEndingsToFileTypes.get(fileEnding);
    }

    public String getFileName(String contentDispositionHeader) {
        String[] contentDispositionHeaderSplit = contentDispositionHeader.split(";");
        for (String contentDispositionHeaderPart : contentDispositionHeaderSplit) {
            if ((contentDispositionHeaderPart.trim().startsWith("filename"))) {
                String[] filenameSplit = contentDispositionHeaderPart.split("=");
                String filename = filenameSplit[1].trim().replaceAll("\"", "");
                return filename.replaceAll("[^0-9a-zåäöA-ZÅÄÖ/!_.*'()-]", "");
            }
        }
        return null;
    }

    public String getFileNameMainImage(String filename) {
        return filename.replaceAll("[^0-9a-zåäöA-ZÅÄÖ/!_.*'()-]", "");
    }

    public String getFileName(URL url) {

        String fileName = url.getPath().substring(url.getPath().lastIndexOf("/")+1, url.getPath().length());
        return fileName.replaceAll("[^0-9a-zåäöA-ZÅÄÖ/!_.*'()-]", "");
    }

    public String getFileEnding(String fileName) {

        String fileEnding = null;

        if(fileName.lastIndexOf(".") != -1) {
            fileEnding = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length() );

        }

        return fileEnding;
    }

    public void validateImportFileEnding(String fileEnding) throws HjalpmedelstjanstenValidationException {
        if( fileEnding == null || fileEnding.isEmpty() ) {
            throw validationMessageService.generateValidationException("file", "import.fileending.empty");
        }
        if( !validImportFileEndingsList.contains(fileEnding.toLowerCase()) ) {
            throw validationMessageService.generateValidationException("file", "import.fileending.invalid");
        }
    }

    @PostConstruct
    private void initialize() {
        LOG.log( Level.FINEST, "initialize()" );

        // valid document file endings and their respective file type
        String[] validDocumentFileEndingsAndCorrespondingFileTypesArray = validDocumentFileEndingsAndCorrespondingFileTypes.split(",");
        for( String validDocumentFileEndingAndCorrespondingFileType : validDocumentFileEndingsAndCorrespondingFileTypesArray ) {
            String[] fileEndingAndFileType = validDocumentFileEndingAndCorrespondingFileType.split(":");
            String validFileEnding = fileEndingAndFileType[0];
            MediaDocument.FileType fileType = MediaDocument.FileType.valueOf(fileEndingAndFileType[1]);
            LOG.log( Level.FINEST, "Valid document file ending: {0}, fileType: {1}", new Object[] {validFileEnding, fileType});
            validDocumentFileEndings.add(validFileEnding);
            documentFileEndingsToFileTypes.put(validFileEnding, fileType);
        }

        // valid image file endings
        String[] validImageFileEndingsArray = validImageFileEndings.split(",");
        for( String validImageFileEnding : validImageFileEndingsArray ) {
            LOG.log( Level.FINEST, "Valid image file ending: {0}", new Object[] {validImageFileEnding});
            validImageFileEndingsList.add(validImageFileEnding);
        }

        // valid import file endings
        String[] validPImportFileEndingsArray = validImportFileEndings.split(",");
        for( String validPImportFileEnding : validPImportFileEndingsArray ) {
            LOG.log( Level.FINEST, "Valid import file ending: {0}", new Object[] {validPImportFileEnding});
            validImportFileEndingsList.add(validPImportFileEnding);
        }
    }

}
