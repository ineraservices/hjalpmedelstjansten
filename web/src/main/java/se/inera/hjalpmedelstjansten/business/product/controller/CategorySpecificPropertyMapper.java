package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyApiEXT;
import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyListValueAPI;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificPropertyListValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for mapping between API and Entity classes
 * 
 */
public class CategorySpecificPropertyMapper {
    
    public static final List<CategorySpecificPropertyAPI> map(List<CategorySpecificProperty> categorySpecificPropertys) {
        if( categorySpecificPropertys == null || categorySpecificPropertys.isEmpty() ) {
            return null;
        }
        List<CategorySpecificPropertyAPI> categorySpecificPropertyAPIs = new ArrayList<>();
        for( CategorySpecificProperty categorySpecificProperty : categorySpecificPropertys ) {
            categorySpecificPropertyAPIs.add(map(categorySpecificProperty));
        }
        return categorySpecificPropertyAPIs;
    }
    
    public static final CategorySpecificPropertyAPI map(CategorySpecificProperty categorySpecificProperty) {
        if( categorySpecificProperty == null ) {
            return null;
        }
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();        
        categorySpecificPropertyAPI.setId(categorySpecificProperty.getUniqueId());
        categorySpecificPropertyAPI.setName(categorySpecificProperty.getName());
        categorySpecificPropertyAPI.setOrderIndex(categorySpecificProperty.getOrderIndex());
        categorySpecificPropertyAPI.setDescription(categorySpecificProperty.getDescription());
        categorySpecificPropertyAPI.setType(categorySpecificProperty.getType().toString());
        if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_MULTIPLE ||
                categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_SINGLE ) {
            List<CategorySpecificPropertyListValueAPI> categorySpecificPropertyListValueAPIs = new ArrayList<>();
            for( CategorySpecificPropertyListValue categorySpecificPropertyListValue : categorySpecificProperty.getCategorySpecificPropertyListValues() ) {
                CategorySpecificPropertyListValueAPI categorySpecificPropertyListValueAPI = new CategorySpecificPropertyListValueAPI();
                categorySpecificPropertyListValueAPI.setId(categorySpecificPropertyListValue.getUniqueId());
                categorySpecificPropertyListValueAPI.setValue(categorySpecificPropertyListValue.getValue());
                categorySpecificPropertyListValueAPIs.add(categorySpecificPropertyListValueAPI);
            }
            if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_SINGLE ) {
                // must add an "empty" value so a selected value can be removed
                CategorySpecificPropertyListValueAPI categorySpecificPropertyListValueAPI = new CategorySpecificPropertyListValueAPI();
                categorySpecificPropertyListValueAPI.setValue("");
                categorySpecificPropertyListValueAPIs.add(categorySpecificPropertyListValueAPI);
            }
            categorySpecificPropertyAPI.setValues(categorySpecificPropertyListValueAPIs);
        }
        return categorySpecificPropertyAPI;
    }


    public static final CategorySpecificProperty map(CategorySpecificPropertyApiEXT categorySpecificPropertyAPI) {
        if( categorySpecificPropertyAPI == null ) {
            return null;
        }
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();

        categorySpecificProperty.setUniqueId(categorySpecificPropertyAPI.getId());
        categorySpecificProperty.setName(categorySpecificPropertyAPI.getName());
        categorySpecificProperty.setOrderIndex(categorySpecificPropertyAPI.getOrderIndex());
        categorySpecificProperty.setDescription(categorySpecificPropertyAPI.getDescription());
        categorySpecificProperty.setType(CategorySpecificProperty.Type.valueOf(categorySpecificPropertyAPI.getType()));
        List<CategorySpecificPropertyListValue> catproplistvalues = new ArrayList<>();
        categorySpecificProperty.setCategorySpecificPropertyListValues(catproplistvalues);
        return categorySpecificProperty;
    }


    public static final List<CategorySpecificPropertyListValue> map(List<CategorySpecificPropertyListValueAPI> categorySpecificPropertyListValueAPI,CategorySpecificProperty categorySpecificProperty) {

        if(categorySpecificPropertyListValueAPI == null || categorySpecificPropertyListValueAPI.stream().count() == 0)
        {return null;}
        List<CategorySpecificPropertyListValue> catproplistvalues = new ArrayList<>();

        for (CategorySpecificPropertyListValueAPI item : categorySpecificPropertyListValueAPI)
        {
            CategorySpecificPropertyListValue valueEntity = new  CategorySpecificPropertyListValue();
            valueEntity.setCode(item.getValue());
            valueEntity.setValue(item.getValue());
            valueEntity.setCategorySpecificProperty(categorySpecificProperty);
            catproplistvalues.add(valueEntity);
        }

        //for loopa alla apilistvalues och returnera en lista att fästa på propertyn

        return catproplistvalues;
    }
    
}
