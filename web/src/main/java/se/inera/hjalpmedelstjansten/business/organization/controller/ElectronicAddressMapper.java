package se.inera.hjalpmedelstjansten.business.organization.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.LoggerService;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.entity.ElectronicAddress;

import java.util.logging.Level;


public class ElectronicAddressMapper {
    
    private static final HjmtLogger LOG = LoggerService.getLogger(ElectronicAddressMapper.class.getName());
    
    public static ElectronicAddressAPI map(ElectronicAddress electronicAddress ) {
        LOG.log( Level.FINEST, "map(...)" );
        if( electronicAddress == null ) {
            return null;
        }
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setId(electronicAddress.getUniqueId());
        electronicAddressAPI.setEmail(electronicAddress.getEmail());
        electronicAddressAPI.setFax(electronicAddress.getFax());
        electronicAddressAPI.setMobile(electronicAddress.getMobile());
        electronicAddressAPI.setTelephone(electronicAddress.getTelephone());
        electronicAddressAPI.setWeb(electronicAddress.getWeb());
        return electronicAddressAPI;
    }
    
    public static ElectronicAddress map(ElectronicAddressAPI electronicAddressAPI ) {
        if( electronicAddressAPI == null ) {
            return null;
        }
        ElectronicAddress electronicAddress = new ElectronicAddress();
        electronicAddress.setEmail(electronicAddressAPI.getEmail());
        electronicAddress.setFax(electronicAddressAPI.getFax());
        electronicAddress.setMobile(electronicAddressAPI.getMobile());
        electronicAddress.setTelephone(electronicAddressAPI.getTelephone());
        electronicAddress.setWeb(electronicAddressAPI.getWeb());
        return electronicAddress;
    }
    
}
