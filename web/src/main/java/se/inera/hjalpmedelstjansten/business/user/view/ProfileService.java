package se.inera.hjalpmedelstjansten.business.user.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;
import java.util.logging.Level;

/**
 * REST API for User Profile functionality.
 *
 */
@Stateless
@Path("profile")
@Interceptors({ PerformanceLogInterceptor.class })
public class ProfileService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    private AuthHandler authHandler;

    @GET
    @SecuredService(permissions = {"profile:view"})
    public Response getUser() {
        LOG.log(Level.FINEST, "getUser()");
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( userAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(userAPI).build();
        }
    }

}
