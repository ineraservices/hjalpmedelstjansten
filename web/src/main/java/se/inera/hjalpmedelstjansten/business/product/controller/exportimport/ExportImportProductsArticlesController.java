package se.inera.hjalpmedelstjansten.business.product.controller.exportimport;

import se.inera.hjalpmedelstjansten.business.media.controller.DocumentTypeController;
import se.inera.hjalpmedelstjansten.business.media.controller.MediaController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.product.controller.CategoryController;
import se.inera.hjalpmedelstjansten.business.product.controller.CeController;
import se.inera.hjalpmedelstjansten.business.product.controller.OrderUnitController;
import se.inera.hjalpmedelstjansten.business.product.controller.PackageUnitController;
import se.inera.hjalpmedelstjansten.business.product.controller.PreventiveMaintenanceUnitController;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

/**
 * Base class for handling business logic of exporting and importing products and
 * articles.
 *
 * There is a notion of versions for excel export and import. Every times changes
 * are made that may cause old excel-files not to work, the version-field of this class
 * should be updated which causes a prompt to user using an old version of the excel
 * export to be forced to get a new one.
 *
 */
public abstract class ExportImportProductsArticlesController {

    public static final String EXPORT_VERSION_NAME = "ExportVersion";
    public static final String EXPORT_VERSION = "1.4";
    public static final String EXPORT_TYPE_NAME = "ExportType";
    public static final String EXPORT_TYPE = "Products";

    enum MediaType {
        IMAGE, DOCUMENT, VIDEO
    }

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    CategoryController categoryController;

    @Inject
    ProductController productController;

    @Inject
    ArticleController articleController;

    @Inject
    OrderUnitController orderUnitController;

    @Inject
    PackageUnitController packageUnitController;

    @Inject
    CeController ceController;

    @Inject
    DocumentTypeController documentTypeController;

    @Inject
    MediaController mediaController;

    @Inject
    PreventiveMaintenanceUnitController preventiveMaintenanceUnitController;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    static final String FIELD_TYPE_NAME = "type";
    static final String FIELD_NAME_NAME = "name";
    static final String FIELD_NUMBER_NAME = "number";
    static final String FIELD_MEDIA_URL_NAME = "mediaUrl";
    static final String FIELD_MEDIA_DESCRIPTION_NAME = "mediaDescription";
    static final String FIELD_MEDIA_ALTTEXT_NAME = "mediaAltText";
    static final String FIELD_MEDIA_DOCUMENTTYPE_NAME = "mediaDocumentType";
    static final String FIELD_BASEDONPRODUCT_NAME = "basedOnProduct";
    static final String FIELD_DISCONTINUED_NAME = "discontinued";
    static final String FIELD_INACTIVATE_ROWS_ON_DISCONTINUED_NAME = "inactivateRowsOnDiscontinued";
    static final String FIELD_REPLACEDBY_NAME = "replacedBy";
    static final String FIELD_EXTENDED_CATEGORIES_NAME = "extendedCategories";
    static final String FIELD_ORDERUNIT_NAME = "orderUnit";
    static final String FIELD_SUPPLEMENTARYINFORMATION_NAME = "supplementaryInformation";
    static final String FIELD_DESCRIPTION_FOR_ELVASJUTTIOSJU = "descriptionForElvasjuttiosju";
    static final String FIELD_MANUFACTURER_NAME = "manufacturer";
    static final String FIELD_TRADEMARK_NAME = "trademark";
    static final String FIELD_GTIN13_NAME = "gtin13";
    static final String FIELD_MANUFACTURERWEB_NAME = "manufacturerWeb";
    static final String FIELD_CUSTOMERUNIQUE_NAME = "customerUnique";
    static final String FIELD_ARTICLEQUANTITYINOUTERPACKAGE_NAME = "articleQuantityInOuterPackage";
    static final String FIELD_ARTICLEQUANTITYINOUTERPACKAGEUNIT_NAME = "articleQuantityInOuterPackageUnit";
    static final String FIELD_PACKAGELEVELBASE_NAME = "packageLevelBase";
    static final String FIELD_PACKAGELEVELMIDDLE_NAME = "packageLevelMiddle";
    static final String FIELD_PACKAGELEVELTOP_NAME = "packageLevelTop";
    static final String FIELD_MANUFACTURERNUMBER_NAME = "manufacturerNumber";
    static final String FIELD_COLOR_NAME = "color";
    static final String FIELD_CEMARKED_NAME = "ceMarked";
    static final String FIELD_DIRECTIVE_NAME = "directive";
    static final String FIELD_STANDARD_NAME = "standard";
    static final String FIELD_PREVENTIVEMAINTENANCE_VALIDFROM_NAME = "preventiveMaintenanceValidFrom";
    static final String FIELD_PREVENTIVEMAINTENANCE_INTERVAL_NAME = "preventiveMaintenanceInterval";
    static final String FIELD_CONNECTED_TO_NAME = "connectedTo";
    static final String FIELD_PACKAGECONTENT_NAME = "packageContent";
    static final String FIELD_PACKAGECONTENTUNIT_NAME = "packageContentUnit";

    static final String FIELD_CATEGORYSPECIFICPROPERTY_PREFIX = "csp_";

    static final String YES = "Ja";
    static final String NO = "Nej";

    static final String STYCK = "styck";
    static final String DYGN = "dygn";
    static final String TIMME = "timme";
    static final String GRAM = "gram";
    static final String KILOGRAM = "kilogram";
    static final String MILLIGRAM = "milligram";
    static final String METER = "meter";
    static final String LITER = "liter";
    static final String MILLILITER = "milliliter";
    static final String PAR = "par";
    static final String SATS = "sats";


    static final String UNKNOWN = "unknown";

    static final String IMAGES_SHEET_NAME = "Bilder";
    static final String DOCUMENTS_SHEET_NAME = "Dokument";
    static final String VIDEOS_SHEET_NAME = "Videor";
    static final String VALUELIST_SHEET_NAME = "Värdelistor";
    static final String CATEGORIES_SHEET_NAME = "Kategorier";

    static final String PRODUCT_TYPE_NAME = "Produkt";
    static final String ARTICLE_TYPE_NAME = "Artikel";

    public static final String DATE_PATTERN = "yyyy-MM-dd";
    static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    static final String CONNECTIONS_SEPARATOR = ";";

    static final String CLEAR_VALUE_STRING = "*radera*";
    static final Double CLEAR_VALUE_DOUBLE = -1d;

    /**
     * Sheetnames can be a maximum of 31 characters and not contain any of the
     * characters \, /, ?, *, [ or ]
     * @param categoryCode
     * @param categoryName
     * @return
     */
    static String getSheetName( String categoryCode, String categoryName ) {
        String sheetName = (categoryCode == null ? "": categoryCode + " " ) + categoryName;
        sheetName = sheetName.replaceAll("(\\\\|/|\\?|\\*|\\[|\\])", " ");
        if( sheetName.length() > 31 ) {
            sheetName = sheetName.substring(0, 31);
        }
        return sheetName;
    }

}
