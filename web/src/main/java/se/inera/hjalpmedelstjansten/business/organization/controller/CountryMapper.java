package se.inera.hjalpmedelstjansten.business.organization.controller;

import se.inera.hjalpmedelstjansten.model.api.cv.CVCountryAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCountry;

import java.util.ArrayList;
import java.util.List;


public class CountryMapper {
    
    public static final List<CVCountryAPI> map(List<CVCountry> countries) {
        if( countries == null ) {
            return null;
        }
        List<CVCountryAPI> countryAPIs = new ArrayList<>();
        for( CVCountry country : countries ) {
            countryAPIs.add(map(country));
        }
        return countryAPIs;
    }
    
    public static final CVCountryAPI map(CVCountry country) {
        if( country == null ) {
            return null;
        }
        CVCountryAPI countryAPI = new CVCountryAPI();
        countryAPI.setId(country.getUniqueId());
        countryAPI.setCode(country.getCode());
        countryAPI.setName(country.getName());
        return countryAPI;
    }
    
}
