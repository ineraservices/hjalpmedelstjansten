package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.api.cv.CVOrderUnitAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

@Stateless
public class OrderUnitController {

    @Inject
    private HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;

    public List<CVOrderUnitAPI> getAllUnits() {
        LOG.log(Level.FINEST, "getAllUnits()");
        return OrderUnitMapper.map(findAll());
    }

    public List<CVOrderUnit> findAll() {
        LOG.log(Level.FINEST, "findAll()");
        return em.createNamedQuery(CVOrderUnit.FIND_ALL).getResultList();
    }

    public CVOrderUnit getUnit(long uniqueId) {
        LOG.log(Level.FINEST, "getUnit( uniqueId: {0} )", new Object[] {uniqueId});
        return em.find(CVOrderUnit.class, uniqueId);
    }

    public CVOrderUnit findByName(String orderUnitName) {
        LOG.log(Level.FINEST, "findByName( orderUnitName: {0} )", new Object[] {orderUnitName});
        List<CVOrderUnit> orderUnits = em.createNamedQuery(CVOrderUnit.FIND_BY_NAME).
                setParameter("name", orderUnitName).
                getResultList();
        // order unit names are unique so either we get one or nothing
        if( orderUnits != null && !orderUnits.isEmpty() ) {
            return orderUnits.get(0);
        }
        return null;
    }

    public Map<Long, CVOrderUnit> findAllAsIdMap() {
        Map<Long, CVOrderUnit> orderUnitsIdMap = new HashMap<>();
        List<CVOrderUnit> allUnits = findAll();
        if( allUnits != null && !allUnits.isEmpty() ) {
            allUnits.forEach((orderUnit) -> {
                orderUnitsIdMap.put(orderUnit.getUniqueId(), orderUnit);
            });
        }
        return orderUnitsIdMap;
    }

    public Map<Long, CVOrderUnit> getAllAsIdMap(List<CVOrderUnit> allUnits) {
        Map<Long, CVOrderUnit> orderUnitsIdMap = new HashMap<>();
        if( allUnits != null && !allUnits.isEmpty() ) {
            allUnits.forEach((orderUnit) -> {
                orderUnitsIdMap.put(orderUnit.getUniqueId(), orderUnit);
            });
        }
        return orderUnitsIdMap;
    }

}
