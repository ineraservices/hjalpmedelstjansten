package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Level;

@Stateless
public class CVPreventiveMaintenanceController {

    @Inject
    private HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;

    public List<CVPreventiveMaintenance> getAllCVPreventiveMaintenances() {
        LOG.log(Level.FINEST, "getAllCVPreventiveMaintenances()");
        return em.createNamedQuery(CVPreventiveMaintenance.FIND_ALL).getResultList();
    }

    public CVPreventiveMaintenance findByCode(String code) {
        LOG.log(Level.FINEST, "findByCode( code: {0} )", new Object[] {code});
        List<CVPreventiveMaintenance> preventiveMaintenances = em.
                createNamedQuery(CVPreventiveMaintenance.FIND_BY_CODE).
                setParameter("code", code).
                getResultList();
        if( preventiveMaintenances == null || preventiveMaintenances.isEmpty() ) {
            return null;
        } else if( preventiveMaintenances.size() > 1 ) {
            LOG.log( Level.WARNING, "Multiple preventive maintenances found for code: {1}", new Object[] {code});
            return null;
        } else {
            return preventiveMaintenances.get(0);
        }
    }

}
