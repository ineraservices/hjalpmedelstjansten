package se.inera.hjalpmedelstjansten.business.agreement.view;

import org.jboss.ejb3.annotation.TransactionTimeout;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementController;
import se.inera.hjalpmedelstjansten.business.agreement.controller.exportimport.ExportPricelistController;
import se.inera.hjalpmedelstjansten.business.agreement.controller.exportimport.ImportAgreementPricelistController;
import se.inera.hjalpmedelstjansten.business.helpers.ExportHelper;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

@Stateless
@Path("organizations/{organizationUniqueId}/agreements/{agreementUniqueId}/pricelists/{pricelistUniqueId}/exportimport")
@Interceptors({PerformanceLogInterceptor.class})
public class ExportImportAgreementPricelistService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private AgreementController agreementController;

    @Inject
    ExportHelper exportHelper;

    @EJB
    private AuthHandler authHandler;

    @EJB
    private ExportPricelistController exportAgreementPricelistController;

    @EJB
    private ImportAgreementPricelistController importPricelistController;

    /**
     * Export agreement pricelist rows to Excel
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId    unique id of the agreement
     * @param pricelistUniqueId    unique id of the pricelist
     * @return
     * @throws HjalpmedelstjanstenValidationException
     */
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @Path("export")
    @SecuredService(permissions = {"agreementpricelistexport:view"})
    @TransactionTimeout(value = 20, unit = TimeUnit.MINUTES)
    public Response exportAgreementPricelist(@Context HttpServletRequest httpServletRequest,
                                             @PathParam("organizationUniqueId") long organizationUniqueId,
                                             @PathParam("agreementUniqueId") long agreementUniqueId,
                                             @PathParam("pricelistUniqueId") long pricelistUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "exportAgreementPricelist( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[]{organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if (!authorizeHandleOrganization(organizationUniqueId, userAPI)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");

        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);

        String supplierName = agreement.getSupplier().getOrganizationName();
        if(supplierName.length() > 10)
        {
            supplierName = supplierName.substring(0, 10);
        }

        String agreementNumberNameAndOrgNameFiltered = (supplierName + "_"+agreement.getAgreementNumber() + "_" + agreement.getAgreementName())
            .replaceAll(" ", "_")
            .replaceAll("[,.]", "");

        String filename = exportHelper.replaceSpecialCharacters(agreementNumberNameAndOrgNameFiltered) +"_Avt_"+ dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";
        byte[] exportBytes = exportAgreementPricelistController.exportAgreementPricelistFile(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        LOG.log(Level.FINEST, "Content-Length: {0}", new Object[]{exportBytes.length});
        return Response.ok(
                exportBytes,
                jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM).
                header("Content-Disposition", "attachment; filename=" + filename).
                header("Content-Length", exportBytes.length).
                build();
    }

    /**
     * Import agreement pricelist (rows) from Excel
     *
     * @param httpServletRequest
     * @param organizationUniqueId   unique id of the organization
     * @param agreementUniqueId      unique id of the agreement
     * @param pricelistUniqueId      unique id of the pricelist
     * @param multipartFormDataInput user uploaded file
     * @return a 200 response
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("import")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @SecuredService(permissions = {"agreementpricelistimport:view"})
    @TransactionTimeout(value = 20, unit = TimeUnit.MINUTES)
    public Response importAgreementPricelist(@Context HttpServletRequest httpServletRequest,
                                             @PathParam("organizationUniqueId") long organizationUniqueId,
                                             @PathParam("agreementUniqueId") long agreementUniqueId,
                                             @PathParam("pricelistUniqueId") long pricelistUniqueId,
                                             MultipartFormDataInput multipartFormDataInput) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "importAgreementPricelist( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[]{organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if (!authorizeHandleOrganization(organizationUniqueId, userAPI)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        importPricelistController.importAgreementPricelistFile(organizationUniqueId,
                agreementUniqueId,
                pricelistUniqueId,
                multipartFormDataInput,
                userAPI,
                authHandler.getSessionId(),
                getRequestIp(httpServletRequest));
        return Response.ok().build();
    }

}
