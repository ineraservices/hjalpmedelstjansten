package se.inera.hjalpmedelstjansten.business.agreement.controller;

import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelist;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

/**
 * Validation methods for agreement pricelist
 *
 */
@Stateless
public class AgreementPricelistValidation {

    @Inject
    HjmtLogger LOG;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    AgreementPricelistController pricelistController;

    /**
     * Validate user input for creation of pricelist
     *
     * @param pricelistAPI
     * @param agreement
     * @throws HjalpmedelstjanstenValidationException
     */
    public void validateForCreate(AgreementPricelistAPI pricelistAPI, Agreement agreement) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForCreate(...)" );
        HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
        if( agreement.getStatus() == Agreement.Status.DISCONTINUED ) {
            exception.addValidationMessage("status", validationMessageService.getMessage("agreement.status.discontinued"));
            // no need to continue checks
            throw exception;
        }
        validatePricelistAPI(pricelistAPI);

        // pricelist number is unique withing agreement
        validateNumberUnique(pricelistAPI.getNumber(), agreement.getUniqueId(), null, exception);

        // maximum one pricelist per date/per agreement
        validateValidFromUnique(pricelistAPI.getValidFrom(), agreement.getUniqueId(), null, exception);

        if( !exception.getValidationMessages().isEmpty() ) {
            throw exception;
        }

        // garbage collect help
        exception = null;
    }

    /**
     * Validate user input for update of pricelist.
     *
     * @param agreementPricelistAPI user supplied values
     * @param agreementPricelist pricelist to update
     * @param agreement agreement of the pricelist
     * @throws HjalpmedelstjanstenValidationException
     */
    public void validateForUpdate(AgreementPricelistAPI agreementPricelistAPI, AgreementPricelist agreementPricelist, Agreement agreement) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForUpdate(...)" );
        validatePricelistAPI(agreementPricelistAPI);

        HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");

        if( agreement.getStatus() == Agreement.Status.DISCONTINUED ) {
            exception.addValidationMessage("status", validationMessageService.getMessage("agreement.status.discontinued"));
            // no need to continue checks
            throw exception;
        }

        // pricelist number is unique withing agreement
        validateNumberUnique(agreementPricelistAPI.getNumber(), agreement.getUniqueId(), agreementPricelist.getUniqueId(), exception);

        // maximum one pricelist per date/per agreement
        validateValidFromUnique(agreementPricelistAPI.getValidFrom(), agreement.getUniqueId(), agreementPricelist.getUniqueId(), exception);

        if( !exception.getValidationMessages().isEmpty() ) {
            throw exception;
        }

        // garbage collect help
        exception = null;
    }

    /**
     * Make sure number of pricelist is unique within given agreement
     *
     * @param number the number to check
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist to update or null if this is create
     * @param exception
     */
    private void validateNumberUnique(String number, long agreementUniqueId, Long pricelistUniqueId, HjalpmedelstjanstenValidationException exception) {
        List<AgreementPricelist> pricelists = pricelistController.findByNumberAndAgreement(number, agreementUniqueId);
        if( pricelists != null && !pricelists.isEmpty() ) {
            if( pricelistUniqueId == null ) {
                // this is an attempt to create pricelist and there already is one with the number
                exception.addValidationMessage("number", validationMessageService.getMessage("pricelist.number.alreadyExist"));
            } else {
                // this is an attempt to update pricelist, no OTHER pricelist can have the same number
                for( AgreementPricelist pricelist : pricelists ) {
                    if( !pricelist.getUniqueId().equals(pricelistUniqueId) ) {
                        exception.addValidationMessage("number", validationMessageService.getMessage("pricelist.number.alreadyExist"));
                    }
                }
            }
        }
    }

    /**
     * Check that validfrom is unique within given agreement
     *
     * @param validFrom the valid from to check
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist to update or null if this is create
     * @param exception
     */
    private void validateValidFromUnique(long validFrom, long agreementUniqueId, Long pricelistUniqueId, HjalpmedelstjanstenValidationException exception) {
        List<AgreementPricelist> pricelists = pricelistController.findByValidFromAndAgreement(validFrom, agreementUniqueId);
        if( pricelists != null && !pricelists.isEmpty() ) {
            if( pricelistUniqueId == null ) {
                // this is an attempt to create pricelist and there already is one with the same valid from
                exception.addValidationMessage("validFrom", validationMessageService.getMessage("pricelist.validFrom.alreadyExist"));
            } else {
                // this is an attempt to update pricelist, no OTHER pricelist can have the same valid from
                for( AgreementPricelist pricelist : pricelists ) {
                    if( !pricelist.getUniqueId().equals(pricelistUniqueId) ) {
                        exception.addValidationMessage("validFrom", validationMessageService.getMessage("pricelist.validFrom.alreadyExist"));
                    }
                }
            }
        }
    }

    /**
     * Bean validation of pricelist API
     *
     * @param pricelistAPI
     * @throws HjalpmedelstjanstenValidationException
     */
    private void validatePricelistAPI(AgreementPricelistAPI pricelistAPI) throws HjalpmedelstjanstenValidationException {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<AgreementPricelistAPI>> constraintViolations = validator.validate(pricelistAPI, Default.class);
        if( constraintViolations != null && !constraintViolations.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            for( ConstraintViolation constraintViolation : constraintViolations ) {
                exception.addValidationMessage(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage());
            }
            throw exception;
        }
    }

}
