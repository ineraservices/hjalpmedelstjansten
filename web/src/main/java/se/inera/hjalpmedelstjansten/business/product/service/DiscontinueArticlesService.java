package se.inera.hjalpmedelstjansten.business.product.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.logging.Level;
import java.util.stream.Collectors;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import lombok.NoArgsConstructor;
import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.CategoryController;
import se.inera.hjalpmedelstjansten.business.product.repository.ArticleRepository;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.Product.Status;

@Stateless
@NoArgsConstructor
public class DiscontinueArticlesService {

    @Inject
    HjmtLogger LOG;

    @Inject
    ArticleRepository articleRepository;

    @Inject
    ElasticSearchController elasticSearchController;

    @Inject
    CategoryController categoryController;

    @Inject
    ArticleController articleController;

    @Inject
    SendMailRegardingDiscontinuedArticlesOnAssortment sendMailRegardingDiscontinuedArticlesOnAssortment;

    public void discontinue() {
        final var publishedArticlesWithReplacementDate = articleRepository.findByStatusAndReplacementDatePassed(Product.Status.PUBLISHED, new Date());

        LOG.log(Level.INFO,
            String.format("Found '%s' articles to discontinue!", publishedArticlesWithReplacementDate.size())
        );

        for(Article article : publishedArticlesWithReplacementDate) {
            articleController.handleArticleDiscontinued(article, article.isInactivateRowsOnReplacement());
            final var category = article.getBasedOnProduct() == null ? article.getCategory(): article.getBasedOnProduct().getCategory();
            final var categorySpecificPropertys = categoryController.getCategoryPropertys(category.getUniqueId());
            final var articleAPI = ArticleMapper.map(article, true, categorySpecificPropertys);
            elasticSearchController.bulkIndexArticle(articleAPI);
        }

        final var from = Date.from(LocalDate.now().atStartOfDay()
            .atZone(ZoneId.systemDefault())
            .toInstant());

        final var to = Date.from(LocalDate.now().atTime(LocalTime.MAX)
            .atZone(ZoneId.systemDefault())
            .toInstant());

        final var discontinuedArticles = articleRepository.findByStatusAndReplacementDateBetweenFromAndTo(Status.DISCONTINUED, from, to);

        LOG.log(Level.INFO,
            String.format("Found '%s' discontinued articles to send emails if part of assortments!", discontinuedArticles.size())
        );

        if (!discontinuedArticles.isEmpty()) {
            sendMailRegardingDiscontinuedArticlesOnAssortment.send(
                discontinuedArticles.stream()
                    .map(Article::getUniqueId)
                    .collect(Collectors.toList())
            );
        }
    }
}
