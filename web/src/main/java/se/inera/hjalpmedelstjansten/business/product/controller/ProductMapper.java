package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.LoggerService;
import se.inera.hjalpmedelstjansten.business.organization.controller.ElectronicAddressMapper;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.ResourceSpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.RoleAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.UserEngagementAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEDirectiveAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEStandardAPI;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValue;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEDirective;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEStandard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;


/**
 * Class for mapping between API and Entity classes
 *
 */
public class ProductMapper {

    private static final HjmtLogger LOG = LoggerService.getLogger(ProductMapper.class.getName());

    public static final List<ProductAPI> map(List<Product> products) {
        return map(products, false, null);
    }

    public static final List<ProductAPI> map(List<Product> products, boolean includeEverything, List<CategorySpecificProperty> categorySpecificPropertys) {
        return map(products, includeEverything, categorySpecificPropertys, null);
    }

    public static final List<ProductAPI> map(List<Product> products, boolean includeEverything, List<CategorySpecificProperty> categorySpecificPropertys, UserAPI userAPI) {
        if( products == null ) {
            return null;
        }
        List<ProductAPI> productAPIs = new ArrayList<>();
        for( Product product : products ) {
            productAPIs.add(map(product, includeEverything, categorySpecificPropertys, userAPI));
        }
        return productAPIs;
    }

    public static final ProductAPI map(Product product, boolean includeEverything, List<CategorySpecificProperty> categorySpecificPropertys ) {
        return map(product, includeEverything, categorySpecificPropertys, null);
    }

    public static final ProductAPI map(Product product, boolean includeEverything, List<CategorySpecificProperty> categorySpecificPropertys, UserAPI userAPI ) {
        LOG.log( Level.FINEST, "map(...)" );
        if( product == null ) {
            return null;
        }
        ProductAPI productAPI = new ProductAPI();
        productAPI.setId(product.getUniqueId());
        productAPI.setProductName(product.getProductName());
        productAPI.setProductNumber(product.getProductNumber());

        productAPI.setMainImageUrl(product.getMainImageUrl());
        productAPI.setMainImageAltText(product.getMainImageAltText());
        productAPI.setMainImageOriginalUrl(product.getMainImageOriginalUrl());
        productAPI.setStatus(product.getStatus().toString());
        productAPI.setOrganizationName(product.getOrganization().getOrganizationName());
        productAPI.setOrganizationId(product.getOrganization().getUniqueId());
        productAPI.setCategory(CategoryMapper.map(product.getCategory()));
        if( includeEverything ) {
            productAPI.setMainImageDescription(product.getMainImageDescription());
            productAPI.setExtendedCategories(CategoryMapper.map(product.getExtendedCategories()));
            productAPI.setPreventiveMaintenanceValidFrom(product.getPreventiveMaintenanceValidFrom() == null ? null: PreventiveMaintenanceMapper.map(product.getPreventiveMaintenanceValidFrom()));
            productAPI.setPreventiveMaintenanceDescription(product.getPreventiveMaintenanceDescription());
            productAPI.setPreventiveMaintenanceNumberOfDays(product.getPreventiveMaintenanceNumberOfDays());
            productAPI.setManufacturer(product.getManufacturer());
            productAPI.setManufacturerProductNumber(product.getManufacturerProductNumber());
            productAPI.setTrademark(product.getTrademark());
            productAPI.setManufacturerElectronicAddress(ElectronicAddressMapper.map(product.getManufacturerElectronicAddress()));
            productAPI.setCeMarked(product.isCeMarked());
            productAPI.setCustomerUnique(product.isCustomerUnique());
            productAPI.setCeDirective(mapCEDirective(product.getCeDirective()));
            productAPI.setCeStandard(mapCEStandard(product.getCeStandard()));
            productAPI.setSupplementedInformation(product.getSupplementedInformation());
            productAPI.setDescriptionForElvasjuttiosju(product.getDescriptionForElvasjuttiosju());
            productAPI.setReplacementDate(product.getReplacementDate() == null ? null: product.getReplacementDate().getTime());
            if( product.getReplacedByProducts() != null ) {
                productAPI.setReplacedByProducts(map(product.getReplacedByProducts(), false, null));
            }
            productAPI.setColor(product.getColor());

            // order information
            mapOrderInformation(product, productAPI);

            // category specific properties
            setCategoryProperties( product, productAPI, categorySpecificPropertys );

            productAPI.setCreated(product.getCreated() == null ? null: product.getCreated().getTime());
            productAPI.setUpdated(product.getLastUpdated() == null ? null: product.getLastUpdated().getTime());

            // only the supplier on which the article belongs to can see inactivateRowsOnReplacement
            // även Superadmin måste få sätta inactivateRowsOnReplacement (vid inaktivering av organisation)
            boolean isSuperAdmin = false;
            if( userAPI != null) {
                List<UserEngagementAPI> userEngagementAPIs = userAPI.getUserEngagements();
                for (UserEngagementAPI userEngagementAPI: userEngagementAPIs){
                    List<RoleAPI> roleAPIs = userEngagementAPI.getRoles();
                    for (RoleAPI roleAPI : roleAPIs){
                        if (roleAPI.getName().equalsIgnoreCase(UserRole.RoleName.Superadmin.toString())) {
                            isSuperAdmin = true;
                            break;
                        }
                    }
                }
            }

            if( isSuperAdmin || (userAPI != null && UserController.getUserEngagementAPIByOrganizationId( product.getOrganization().getUniqueId(), userAPI.getUserEngagements()) != null)) {
                productAPI.setInactivateRowsOnReplacement(product.isInactivateRowsOnReplacement());
            }
        }
        return productAPI;
    }

    public static final Product map(ProductAPI productAPI) {
        if( productAPI == null ) {
            return null;
        }
        Product product = new Product();
        product.setProductName(productAPI.getProductName());
        product.setProductNumber(productAPI.getProductNumber().trim());
        product.setStatus(Product.Status.valueOf(productAPI.getStatus()));
        product.setPreventiveMaintenanceDescription(productAPI.getPreventiveMaintenanceDescription());
        product.setPreventiveMaintenanceNumberOfDays(productAPI.getPreventiveMaintenanceNumberOfDays());
        product.setManufacturer(productAPI.getManufacturer());
        product.setManufacturerProductNumber(productAPI.getManufacturerProductNumber());
        product.setTrademark(productAPI.getTrademark());
        product.setManufacturerElectronicAddress(ElectronicAddressMapper.map(productAPI.getManufacturerElectronicAddress()));
        product.setCeMarked(productAPI.isCeMarked());
        product.setCustomerUnique(productAPI.isCustomerUnique());
        product.setSupplementedInformation(productAPI.getSupplementedInformation());
        product.setDescriptionForElvasjuttiosju(productAPI.getDescriptionForElvasjuttiosju());
        product.setColor(productAPI.getColor());

        product.setMainImageUrl(productAPI.getMainImageUrl());
        product.setMainImageDescription(productAPI.getMainImageDescription());
        product.setMainImageAltText(productAPI.getMainImageAltText());
        product.setMainImageOriginalUrl(productAPI.getMainImageOriginalUrl());

        return product;
    }

    public static final List<CVCEStandardAPI> mapCEStandards(List<CVCEStandard> cEStandards) {
        if( cEStandards == null ) {
            return null;
        }
        List<CVCEStandardAPI> cEStandardAPIs = new ArrayList<>();
        for( CVCEStandard cEStandard: cEStandards ) {
            cEStandardAPIs.add(mapCEStandard(cEStandard));
        }
        return cEStandardAPIs;
    }

    public static final List<CVCEDirectiveAPI> mapCEDirectives(List<CVCEDirective> cEDirectives) {
        if( cEDirectives == null ) {
            return null;
        }
        List<CVCEDirectiveAPI> cEDirectiveAPIs = new ArrayList<>();
        for( CVCEDirective cEDirective: cEDirectives ) {
            cEDirectiveAPIs.add(mapCEDirective(cEDirective));
        }
        return cEDirectiveAPIs;
    }

    public static final CVCEStandardAPI mapCEStandard(CVCEStandard cEStandard) {
        if( cEStandard == null ) {
            return null;
        }
        CVCEStandardAPI cEStandardAPI = new CVCEStandardAPI();
        cEStandardAPI.setId(cEStandard.getUniqueId());
        cEStandardAPI.setName(cEStandard.getName());
        return cEStandardAPI;
    }

    public static final CVCEDirectiveAPI mapCEDirective(CVCEDirective cEDirective) {
        if( cEDirective == null ) {
            return null;
        }
        CVCEDirectiveAPI cEDirectiveAPI = new CVCEDirectiveAPI();
        cEDirectiveAPI.setId(cEDirective.getUniqueId());
        cEDirectiveAPI.setName(cEDirective.getName());
        return cEDirectiveAPI;
    }

    public static void fix(ProductAPI productAPI) {
        if( productAPI.getProductNumber() != null ) {
            productAPI.setProductNumber(productAPI.getProductNumber().trim());
        }
    }

    private static void mapOrderInformation( Product product, ProductAPI productAPI ) {
        // order information
        productAPI.setOrderUnit(OrderUnitMapper.map(product.getOrderUnit()));
        productAPI.setArticleQuantityInOuterPackage(product.getArticleQuantityInOuterPackage());
        productAPI.setArticleQuantityInOuterPackageUnit(OrderUnitMapper.map(product.getArticleQuantityInOuterPackageUnit()));
        productAPI.setPackageContent(product.getPackageContent());
        productAPI.setPackageContentUnit(PackageUnitMapper.map(product.getPackageContentUnit()));
        productAPI.setPackageLevelBase(product.getPackageLevelBase());
        productAPI.setPackageLevelMiddle(product.getPackageLevelMiddle());
        productAPI.setPackageLevelTop(product.getPackageLevelTop());
    }

    private static void setCategoryProperties(Product product, ProductAPI productAPI, List<CategorySpecificProperty> categorySpecificPropertys) {
        LOG.log( Level.FINEST, "setCategoryProperties(...)" );
        int categorySpecificPropertysSize = categorySpecificPropertys == null ? 0: categorySpecificPropertys.size();
        int resourceSpecificPropertyValuesSize = product.getResourceSpecificPropertyValues() == null ? 0: product.getResourceSpecificPropertyValues().size();
        if( product.getResourceSpecificPropertyValues() != null && !product.getResourceSpecificPropertyValues().isEmpty() ) {
            productAPI.setCategoryPropertys(ResourceSpecificPropertyMapper.map(product.getResourceSpecificPropertyValues()));
        } else {
            productAPI.setCategoryPropertys(new ArrayList<>());
        }
        if( categorySpecificPropertysSize != resourceSpecificPropertyValuesSize ) {
            if (categorySpecificPropertys != null && !categorySpecificPropertys.isEmpty()) {
                for (CategorySpecificProperty categorySpecificProperty : categorySpecificPropertys) {
                    if (!resourceSpecificPropertysContains(categorySpecificProperty, product.getResourceSpecificPropertyValues())) {
                        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
                        resourceSpecificPropertyAPI.setProperty(CategorySpecificPropertyMapper.map(categorySpecificProperty));
                        productAPI.getCategoryPropertys().add(resourceSpecificPropertyAPI);
                    }
                }
            }
        }

        // must sort category properties
        Collections.sort(productAPI.getCategoryPropertys(), (ResourceSpecificPropertyAPI o1, ResourceSpecificPropertyAPI o2) -> o1.getProperty().getName().compareTo(o2.getProperty().getName()));
    }

    private static boolean resourceSpecificPropertysContains(CategorySpecificProperty categorySpecificProperty, List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues) {
        if( resourceSpecificPropertyValues != null && !resourceSpecificPropertyValues.isEmpty() ) {
            for( ResourceSpecificPropertyValue resourceSpecificPropertyValue : resourceSpecificPropertyValues ) {
                if( resourceSpecificPropertyValue.getCategorySpecificProperty().getUniqueId().equals(categorySpecificProperty.getUniqueId()) ) {
                    return true;
                }
            }
        }
        return false;
    }

}
