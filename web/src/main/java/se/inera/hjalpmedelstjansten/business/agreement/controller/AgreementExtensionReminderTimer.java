package se.inera.hjalpmedelstjansten.business.agreement.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.clustering.ClusterController;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import jakarta.ejb.DependsOn;
import jakarta.ejb.ScheduleExpression;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.ejb.Timeout;
import jakarta.ejb.TimerConfig;
import jakarta.ejb.TimerService;
import jakarta.inject.Inject;
import java.util.logging.Level;

/**
 * Timer for sending email-reminder for agreements where the senReminderOn is the current date
 *
 */
@Singleton
@Startup
@DependsOn("PropertyLoader")
public class AgreementExtensionReminderTimer {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private boolean agreementSendExtensionReminderTimerEnabled;

    @Inject
    private String agreementSendExtensionReminderTimerHour;

    @Inject
    private String agreementSendExtensionReminderTimerMinute;

    @Inject
    private String agreementSendExtensionReminderTimerSecond;

    @Inject
    private AgreementController agreementController;

    @Inject
    private ClusterController clusterController;

    @Resource
    private TimerService timerService;

    @Timeout
    private void schedule() {
        LOG.log(Level.FINEST, "schedule agreement reminder");
        //attempt to get lock
        boolean lockReceived = clusterController.getLock(this.getClass().getName(), 30);
        if (lockReceived) {
            LOG.log(Level.FINEST, "Received lock");
            long start = System.currentTimeMillis();
            agreementController.sendEmailReminderToExtendAgreement();
            long total = System.currentTimeMillis() - start;
            if( total > 60000) {
                LOG.log(Level.WARNING,"Scheduled job took: {0} ms which is more than 1 minute which is a long time, a developer should take a look at this.", new Object[] {total} );
            } else {
                LOG.log(Level.INFO, "Scheduled job took: {0} ms.", new Object[] {total});
            }
            //release lock
            clusterController.releaseLock(this.getClass().getName());
        } else {
            LOG.log(Level.INFO, "Did not receive lock");
        }
    }

    @PostConstruct
    private void initialize() {
        LOG.log(Level.FINEST, "initialize()");
        if(agreementSendExtensionReminderTimerEnabled) {
            LOG.log(Level.FINEST, "Timer is enabled");
            if(timerService.getTimers().isEmpty()) {
                String name = this.getClass().getName();
                TimerConfig configuration = new TimerConfig();
                configuration.setPersistent(false);
                configuration.setInfo(name);
                ScheduleExpression scheduleExpression = new ScheduleExpression();
                scheduleExpression.
                        hour(agreementSendExtensionReminderTimerHour).
                        minute(agreementSendExtensionReminderTimerMinute).
                        second(agreementSendExtensionReminderTimerSecond);
                timerService.createCalendarTimer(scheduleExpression, configuration);
            }
        } else {
            LOG.log(Level.INFO, "Timer is NOT enabled");
        }
    }


}
