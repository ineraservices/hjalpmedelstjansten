package se.inera.hjalpmedelstjansten.business.generalpricelist.controller;

import se.inera.hjalpmedelstjansten.business.BaseController;
import se.inera.hjalpmedelstjansten.business.DateUtils;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.CVPreventiveMaintenanceController;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

import jakarta.ejb.Stateless;
import jakarta.enterprise.event.Event;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

/**
 * Class for handling business logic of <code>GeneralPricelist</code>.
 * This includes talking to the database.
 *
 */
@Stateless
public class GeneralPricelistController extends BaseController {

    @Inject
    HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    GeneralPricelistValidation generalPricelistValidation;

    @Inject
    OrganizationController organizationController;

    @Inject
    GeneralPricelistPricelistController generalPricelistPricelistController;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    CVPreventiveMaintenanceController cVPreventiveMaintenanceController;

    @Inject
    GuaranteeUnitController guaranteeUnitController;

    @Inject
    UserController userController;

    @Inject
    Event<InternalAuditEvent> internalAuditEvent;

    public SearchDTO searchAllUnselectedPricelistAPIs(long exportSettingsUniqueId, String userSearchQuery, int limit, int offset) {

        StringBuilder countSql = new StringBuilder();
        countSql.append("SELECT COUNT(gp.uniqueId) FROM GeneralPricelist gp WHERE gp.uniqueId NOT IN (SELECT DISTINCT gp.uniqueId FROM ExportSettings es JOIN es.generalPricelists gp WHERE es.uniqueId = :exportSettingsUniqueId) ");
        if( userSearchQuery != null && !userSearchQuery.isEmpty() ) {
            countSql.append("AND (gp.ownerOrganization.organizationName LIKE :query OR gp.generalPricelistNumber LIKE :query) ");
        }
        String countString = countSql.toString();
        LOG.log( Level.FINEST, "countString: {0}", new Object[] {countString} );
        Query countQuery = em.createQuery(countString);
        countQuery.setParameter("exportSettingsUniqueId", exportSettingsUniqueId);
        if( userSearchQuery != null && !userSearchQuery.isEmpty() ) {
            countQuery.setParameter("query", "%" + userSearchQuery + "%");
        }
        Long count = (Long) countQuery.getSingleResult();



        StringBuilder querySql = new StringBuilder();
        querySql.append("SELECT gp FROM GeneralPricelist gp WHERE gp.uniqueId NOT IN (SELECT DISTINCT gp.uniqueId FROM ExportSettings es JOIN es.generalPricelists gp WHERE es.uniqueId = :exportSettingsUniqueId) ");
        if( userSearchQuery != null && !userSearchQuery.isEmpty() ) {
            querySql.append("AND (gp.ownerOrganization.organizationName LIKE :query OR gp.generalPricelistNumber LIKE :query) ");
        }
        querySql.append("ORDER BY gp.ownerOrganization.organizationName DESC");
        String queryString = querySql.toString();
        LOG.log( Level.FINEST, "queryString: {0}", new Object[] {queryString} );
        Query query = em.createQuery(queryString);
        query.setParameter("exportSettingsUniqueId", exportSettingsUniqueId);
        if( userSearchQuery != null && !userSearchQuery.isEmpty() ) {
            query.setParameter("query", "%" + userSearchQuery + "%");
        }


        List<GeneralPricelist> generalPricelists = query.
                setMaxResults(limit).
                setFirstResult(offset).
                getResultList();

        List<GeneralPricelistAPI> generalPricelistAPIs = new ArrayList<>();
        if( generalPricelists == null ) {
            return null;
        }

        for( GeneralPricelist generalPricelist : generalPricelists ) {
            GeneralPricelistPricelist currentPricelist = generalPricelistPricelistController.getCurrentPricelist(generalPricelist.getOwnerOrganization().getUniqueId());
            generalPricelistAPIs.add(GeneralPricelistMapper.map(generalPricelist, false, currentPricelist, null));
        }

        return new SearchDTO(count, generalPricelistAPIs);
    }

    public SearchDTO searchAllGeneralPricelistAPIs(String userSearchQuery, int limit, int offset, String sortType, String sortOrder) {
        LOG.log(Level.FINEST, "searchAllGeneralPricelistAPIs(...)");
        StringBuilder countSql = new StringBuilder();
        countSql.append("SELECT COUNT(gp.uniqueId) FROM GeneralPricelist gp ");
        if( userSearchQuery != null && !userSearchQuery.isEmpty() ) {
            countSql.append("WHERE (gp.ownerOrganization.organizationName LIKE :query OR gp.generalPricelistNumber LIKE :query) ");
        }
        String countString = countSql.toString();
        LOG.log( Level.FINEST, "countString: {0}", new Object[] {countString} );
        Query countQuery = em.createQuery(countString);
        if( userSearchQuery != null && !userSearchQuery.isEmpty() ) {
            countQuery.setParameter("query", "%" + userSearchQuery + "%");
        }
        Long count = (Long) countQuery.getSingleResult();

        StringBuilder querySql = new StringBuilder();
        querySql.append("SELECT gp FROM GeneralPricelist gp ");

        if( userSearchQuery != null && !userSearchQuery.isEmpty() ) {
            querySql.append("WHERE (gp.ownerOrganization.organizationName LIKE :query OR gp.generalPricelistNumber LIKE :query) ");
                    }
        if (!sortType.isEmpty()) {
            querySql.append("ORDER BY gp.").append(sortType);
            if (!sortOrder.isEmpty()) {
                querySql.append(" ").append(sortOrder.toUpperCase());
            }
        }
        String queryString = querySql.toString();
        LOG.log( Level.FINEST, "queryString: {0}", new Object[] {queryString} );
        Query query = em.createQuery(queryString);
        if( userSearchQuery != null && !userSearchQuery.isEmpty() ) {
            query.setParameter("query", "%" + userSearchQuery + "%");
        }
        List<GeneralPricelist> generalPricelists = query.
                setMaxResults(limit).
                setFirstResult(offset).
                getResultList();
        List<GeneralPricelistAPI> generalPricelistAPIs = new ArrayList<>();
        if( generalPricelists == null ) {
            return null;
        }
        for( GeneralPricelist generalPricelist : generalPricelists ) {
            GeneralPricelistPricelist currentPricelist = generalPricelistPricelistController.getCurrentPricelist(generalPricelist.getOwnerOrganization().getUniqueId());
            generalPricelistAPIs.add(GeneralPricelistMapper.map(generalPricelist, false, currentPricelist, null));
        }
        return new SearchDTO(count, generalPricelistAPIs);
    }

     /**
     * Get the <code>GeneralPricelistAPI</code> for the given organization. An
     * organization can have a maximum of one.
     *
     * @param organizationUniqueId unique id of the organization
     * @param userAPI
     * @param sessionId
     * @return the <code>GeneralPricelistAPI</code> for the given organization or
     * null if none exist
     */
    public GeneralPricelistAPI getGeneralPricelistAPI(long organizationUniqueId, UserAPI userAPI, String sessionId, String requestIp) {
        LOG.log(Level.FINEST, "getGeneralPricelistAPI( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        GeneralPricelist generalPricelist = getGeneralPricelist(organizationUniqueId);
        if( generalPricelist != null ) {
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.GENERAL_PRICELIST, InternalAudit.ActionType.VIEW, userAPI.getId(), sessionId, generalPricelist.getUniqueId(), requestIp));
            long totalNumberOfPricelistRows = getNumberOfRowsOnGeneralPricelist(generalPricelist.getUniqueId());
            return GeneralPricelistMapper.map( generalPricelist, true, null, totalNumberOfPricelistRows );
        }
        return null;
    }

    /**
     * Get the <code>GeneralPricelist</code> for the specified organization.
     *
     * @param organizationUniqueId unique id of the organization
     * @return the corresponding <code>GeneralPricelist</code>, null if none exist
     */
    public GeneralPricelist getGeneralPricelist(long organizationUniqueId) {
        LOG.log(Level.FINEST, "getGeneralPricelist( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        List<GeneralPricelist> generalPricelists = em.createNamedQuery(GeneralPricelist.FIND_BY_ORGANIZATION).
                setParameter("organizationUniqueId", organizationUniqueId).
                getResultList();
        if( generalPricelists == null || generalPricelists.isEmpty() ) {
            return null;
        } else {
            // the list should contain a maximum of one and this should be guaranteed by the database
            // since organization is labelled as unique
            return generalPricelists.get(0);
        }
    }

    /**
     * Create a general pricelist on the <code>Organization</code> with the given
     * id. A maximum of one pricelist per organization is allowed.
     *
     * @param organizationUniqueId unique id of the Organization
     * @param generalPricelistAPI user supplied values
     * @param userAPI logged in user information
     * @param sessionId
     * @param requestIp
     * @return the mapped <code>GeneralPricelistAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    public GeneralPricelistAPI createGeneralPricelist(long organizationUniqueId,
            GeneralPricelistAPI generalPricelistAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createGeneralPricelist( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});

        // make sure no general pricelist already exists
        GeneralPricelist existingGeneralPricelist = getGeneralPricelist(organizationUniqueId);
        if( existingGeneralPricelist != null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to create general pricelist when one already exists.", new Object[] {userAPI.getId()});
            throw validationMessageService.generateValidationException("generalpricelist", "generalpricelist.alreadyExists");
        }

        generalPricelistValidation.validateForCreate(generalPricelistAPI, organizationUniqueId);
        GeneralPricelist generalPricelist = GeneralPricelistMapper.map(generalPricelistAPI);

        // add owner organization
        setOwnerOrganization(generalPricelist, organizationUniqueId);

        // set warranty quantity units
        setWarrantyQuantityUnits(generalPricelist, generalPricelistAPI);

        // set warranty valid from
        setWarrantyValidFroms(generalPricelistAPI, generalPricelist, userAPI);

        // save it
        em.persist(generalPricelist);

        // a new agreement should have one first pricelist
        generalPricelistPricelistController.createFirstPricelistForGeneralPricelist(generalPricelist, userAPI, sessionId, requestIp);

        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.GENERAL_PRICELIST, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, generalPricelist.getUniqueId(), requestIp));

        long totalNumberOfPricelistRows = getNumberOfRowsOnGeneralPricelist(generalPricelist.getUniqueId());

        return GeneralPricelistMapper.map( generalPricelist, true, null, totalNumberOfPricelistRows );
    }

    /**
     * Update the <code>GeneralPricelist</code> for the organization with the
     * given id.
     *
     * @param organizationUniqueId unique id of the organization
     * @param generalPricelistAPI the user supplied values
     * @param userAPI logged in user information
     * @param sessionId
     * @param requestIp
     * @return the updated <code>AgreementAPI</code>
     * @throws HjalpmedelstjanstenValidationException in case validation fails
     */
    public GeneralPricelistAPI updateGeneralPricelist(long organizationUniqueId,
            GeneralPricelistAPI generalPricelistAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateGeneralPricelist( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        GeneralPricelist generalPricelist = getGeneralPricelist(organizationUniqueId);
        if( generalPricelist == null ) {
            return null;
        }

        // validate
        generalPricelistValidation.validateForUpdate(generalPricelistAPI, generalPricelist, organizationUniqueId);

        // update basic values
        setGeneralPricelistBasics(generalPricelist, generalPricelistAPI);

        // map all warranty fields
        GeneralPricelistMapper.mapDeliveryAndWarrantyFields(generalPricelistAPI, generalPricelist);

        // set warranty quantity units
        setWarrantyQuantityUnits(generalPricelist, generalPricelistAPI);

        // set warranty valid from
        setWarrantyValidFroms(generalPricelistAPI, generalPricelist, userAPI);

        // log
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.GENERAL_PRICELIST, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, generalPricelist.getUniqueId(), requestIp));

        long totalNumberOfPricelistRows = getNumberOfRowsOnGeneralPricelist(generalPricelist.getUniqueId());

        return GeneralPricelistMapper.map( generalPricelist, true, null, totalNumberOfPricelistRows );
    }

    /**
     * Sets the "basic" fields of the general pricelist from the user supplied values.
     * Valid from can never be updated. Value set on create is forever.
     *
     * @param generalPricelist the general pricelist to update
     * @param generalPricelistAPI the user supplied values
     */
    void setGeneralPricelistBasics(GeneralPricelist generalPricelist, GeneralPricelistAPI generalPricelistAPI) {
        LOG.log(Level.FINEST, "setGeneralPricelistBasics( generalPricelistUniqueId: {0} )", new Object[] {generalPricelist.getUniqueId()});
        generalPricelist.setGeneralPricelistName(generalPricelistAPI.getGeneralPricelistName());
        generalPricelist.setValidTo(DateUtils.endOfDay(generalPricelistAPI.getValidTo()));
    }

    /**
     * Set supplier organizatino on general pricelist
     *
     * @param generalPricelist the general pricelist to set owner organization on
     * @param organizationUniqueId unique id of the owner organization
     * @throws HjalpmedelstjanstenValidationException if the organization doesn't exist
     * or is not of type SUPPLIER
     */
    void setOwnerOrganization(GeneralPricelist generalPricelist, long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setOwnerOrganization( organizationId: {0} )", new Object[] {organizationUniqueId});
        Organization ownerOrganization = organizationController.getOrganization(organizationUniqueId);
        if( ownerOrganization == null ) {
            throw validationMessageService.generateValidationException("ownerOrganization", "generalpricelist.ownerOrganization.notExists");
        }
        if( ownerOrganization.getOrganizationType() != Organization.OrganizationType.SUPPLIER ) {
            throw validationMessageService.generateValidationException("ownerOrganization", "generalpricelist.ownerOrganization.invalidType");
        }
        generalPricelist.setOwnerOrganization(ownerOrganization);

    }

    /**
     * Set units for warrant quantity
     *
     * @param generalPricelist the general pricelist to set units on
     * @param generalPricelistAPI the user supplied values
     * @throws HjalpmedelstjanstenValidationException if a unit does not exist
     */
    void setWarrantyQuantityUnits(GeneralPricelist generalPricelist, GeneralPricelistAPI generalPricelistAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setWarrantyQuantityUnits(...)");
        // H
        if( generalPricelistAPI.getWarrantyQuantityHUnit() != null ) {
            CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(generalPricelistAPI.getWarrantyQuantityHUnit().getId());
            if( unit == null ) {
                LOG.log(Level.FINEST, "cannot set warranty quantity unit: {0} for article type H for general pricelist since it does not exist", new Object[] {generalPricelistAPI.getWarrantyQuantityHUnit().getId()});
                throw validationMessageService.generateValidationException("warrantyQuantityHUnit", "generalpricelist.warrantyQuantityHUnit.notExist");
            }
            generalPricelist.setWarrantyQuantityHUnit(unit);
        } else {
            generalPricelist.setWarrantyQuantityHUnit(null);
        }

        // T
        if( generalPricelistAPI.getWarrantyQuantityTUnit() != null ) {
            CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(generalPricelistAPI.getWarrantyQuantityTUnit().getId());
            if( unit == null ) {
                LOG.log(Level.FINEST, "cannot set warranty quantity unit: {0} for article type T for general pricelist since it does not exist", new Object[] {generalPricelistAPI.getWarrantyQuantityTUnit().getId()});
                throw validationMessageService.generateValidationException("warrantyQuantityTUnit", "generalpricelist.warrantyQuantityTUnit.notExist");
            }
            generalPricelist.setWarrantyQuantityTUnit(unit);
        } else {
            generalPricelist.setWarrantyQuantityTUnit(null);
        }

        // I
        if( generalPricelistAPI.getWarrantyQuantityIUnit() != null ) {
            CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(generalPricelistAPI.getWarrantyQuantityIUnit().getId());
            if( unit == null ) {
                LOG.log(Level.FINEST, "cannot set warranty quantity unit: {0} for article type I for general pricelist since it does not exist", new Object[] {generalPricelistAPI.getWarrantyQuantityIUnit().getId()});
                throw validationMessageService.generateValidationException("warrantyQuantityIUnit", "generalpricelist.warrantyQuantityIUnit.notExist");
            }
            generalPricelist.setWarrantyQuantityIUnit(unit);
        } else {
            generalPricelist.setWarrantyQuantityIUnit(null);
        }

        // R
        if( generalPricelistAPI.getWarrantyQuantityRUnit() != null ) {
            CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(generalPricelistAPI.getWarrantyQuantityRUnit().getId());
            if( unit == null ) {
                LOG.log(Level.FINEST, "cannot set warranty quantity unit: {0} for article type R for general pricelist since it does not exist", new Object[] {generalPricelistAPI.getWarrantyQuantityRUnit().getId()});
                throw validationMessageService.generateValidationException("warrantyQuantityRUnit", "generalpricelist.warrantyQuantityRUnit.notExist");
            }
            generalPricelist.setWarrantyQuantityRUnit(unit);
        } else {
            generalPricelist.setWarrantyQuantityRUnit(null);
        }

        // TJ
        if( generalPricelistAPI.getWarrantyQuantityTJUnit() != null ) {
            CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(generalPricelistAPI.getWarrantyQuantityTJUnit().getId());
            if( unit == null ) {
                LOG.log(Level.FINEST, "cannot set warranty quantity unit: {0} for article type TJ for general pricelist since it does not exist", new Object[] {generalPricelistAPI.getWarrantyQuantityTJUnit().getId()});
                throw validationMessageService.generateValidationException("warrantyQuantityTJUnit", "generalpricelist.warrantyQuantityTJUnit.notExist");
            }
            generalPricelist.setWarrantyQuantityTJUnit(unit);
        } else {
            generalPricelist.setWarrantyQuantityTJUnit(null);
        }
    }

    /**
     * Get the total number of rows in all pricelists in an general pricelist
     *
     * @param generalPricelistId the unique id of the general pricelist
     * @return the number of rows in the general pricelist
     */
    private long getNumberOfRowsOnGeneralPricelist(long generalPricelistId) {
        Long count = (Long) em.createNamedQuery(GeneralPricelistPricelistRow.COUNT_BY_GENERAL_PRICELIST).
                setParameter("generalPricelistId", generalPricelistId).
                getSingleResult();
        return count == null ? 0: count;
    }

    private void setWarrantyValidFroms(GeneralPricelistAPI generalPricelistAPI, GeneralPricelist generalPricelist, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        // warranty valid from
        if( generalPricelistAPI.getWarrantyValidFromH() != null ) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(generalPricelistAPI.getWarrantyValidFromH().getCode());
            if( preventiveMaintenance == null ) {
                LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[] {userAPI.getId(), generalPricelistAPI.getWarrantyValidFromH()});
                throw validationMessageService.generateValidationException("warrantyValidFrom", "generalPricelist.warrantyValidFromH.notExist");
            }
            generalPricelist.setWarrantyValidFromH(preventiveMaintenance);
        } else {
            generalPricelist.setWarrantyValidFromH(null);
        }
        if( generalPricelistAPI.getWarrantyValidFromT() != null ) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(generalPricelistAPI.getWarrantyValidFromT().getCode());
            if( preventiveMaintenance == null ) {
                LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[] {userAPI.getId(), generalPricelistAPI.getWarrantyValidFromT()});
                throw validationMessageService.generateValidationException("warrantyValidFrom", "generalPricelist.warrantyValidFromT.notExist");
            }
            generalPricelist.setWarrantyValidFromT(preventiveMaintenance);
        } else {
            generalPricelist.setWarrantyValidFromT(null);
        }
        if( generalPricelistAPI.getWarrantyValidFromI() != null ) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(generalPricelistAPI.getWarrantyValidFromI().getCode());
            if( preventiveMaintenance == null ) {
                LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[] {userAPI.getId(), generalPricelistAPI.getWarrantyValidFromI()});
                throw validationMessageService.generateValidationException("warrantyValidFrom", "generalPricelist.warrantyValidFromI.notExist");
            }
            generalPricelist.setWarrantyValidFromI(preventiveMaintenance);
        } else {
            generalPricelist.setWarrantyValidFromI(null);
        }
        if( generalPricelistAPI.getWarrantyValidFromR() != null ) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(generalPricelistAPI.getWarrantyValidFromR().getCode());
            if( preventiveMaintenance == null ) {
                LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[] {userAPI.getId(), generalPricelistAPI.getWarrantyValidFromR()});
                throw validationMessageService.generateValidationException("warrantyValidFrom", "generalPricelist.warrantyValidFromR.notExist");
            }
            generalPricelist.setWarrantyValidFromR(preventiveMaintenance);
        } else {
            generalPricelist.setWarrantyValidFromR(null);
        }
        if( generalPricelistAPI.getWarrantyValidFromTJ() != null ) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(generalPricelistAPI.getWarrantyValidFromTJ().getCode());
            if( preventiveMaintenance == null ) {
                LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[] {userAPI.getId(), generalPricelistAPI.getWarrantyValidFromTJ()});
                throw validationMessageService.generateValidationException("warrantyValidFrom", "generalPricelist.warrantyValidFromTJ.notExist");
            }
            generalPricelist.setWarrantyValidFromTJ(preventiveMaintenance);
        } else {
            generalPricelist.setWarrantyValidFromTJ(null);
        }

    }

}
