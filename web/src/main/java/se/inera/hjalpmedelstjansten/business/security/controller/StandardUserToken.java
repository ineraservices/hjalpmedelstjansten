package se.inera.hjalpmedelstjansten.business.security.controller;

import org.apache.shiro.authc.UsernamePasswordToken;


public class StandardUserToken extends UsernamePasswordToken {

    public StandardUserToken(String username, String password) {
        super(username, password);
    }
        
}
