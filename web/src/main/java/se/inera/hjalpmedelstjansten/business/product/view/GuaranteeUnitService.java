package se.inera.hjalpmedelstjansten.business.product.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitController;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import java.util.List;
import java.util.logging.Level;

@Stateless
@Path("guaranteeunits")
@Interceptors({ PerformanceLogInterceptor.class })
public class GuaranteeUnitService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    private GuaranteeUnitController guaranteeUnitController;

    /**
     * Return all guarantee units in the database.
     *
     * @return a list of <code>GuaranteeUnitAPI</code>
     */
    @GET
    @SecuredService(permissions = {"guaranteeunit:view"})
    public List<CVGuaranteeUnitAPI> getAllGuaranteeUnits() {
        LOG.log(Level.FINEST, "getAllGuaranteeUnits()");
        return guaranteeUnitController.getAllGuaranteeUnitAPIs();
    }

}
