package se.inera.hjalpmedelstjansten.business.user.domain.mapper;

import se.inera.hjalpmedelstjansten.business.user.domain.NotificationRequest;
import se.inera.hjalpmedelstjansten.business.user.domain.NotificationResponse;
import se.inera.hjalpmedelstjansten.model.dto.NotificationDTO;

public interface NotificationMapper {

    NotificationDTO toDTO(NotificationRequest request);

    NotificationResponse toResponse(NotificationDTO dto);
}
