package se.inera.hjalpmedelstjansten.business.organization.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.organization.controller.BusinessLevelController;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * REST API for BusinessLevel functionality.
 *
 */
@Stateless
@Path("organizations/{organizationUniqueId}/businesslevels")
@Interceptors({ PerformanceLogInterceptor.class })
public class BusinessLevelService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    private BusinessLevelController businessLevelController;

    /**
     * Return all business levels in the database for the given organization.
     *
     * @param organizationUniqueId
     * @param statuses
     * @return a list of <code>BusinessLevelAPI</code>
     */
    @GET
    @SecuredService(permissions = {"businesslevel:view"})
    public Response searchBusinessLevelsByOrganization(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @QueryParam("status") List<BusinessLevel.Status> statuses) {
        LOG.log(Level.FINEST, "searchBusinessLevelsByOrganization( organizationUniqueId: {0}, statuses: {1} )", new Object[] {organizationUniqueId, statuses});
        if( statuses == null || statuses.isEmpty() ) {
            statuses = new ArrayList<>();
            statuses.addAll(Arrays.asList(BusinessLevel.Status.values()));
        }
        List<BusinessLevelAPI> businessLevelAPIs = businessLevelController.getBusinessLevelsByOrganizationAndStatus(organizationUniqueId, statuses);
        return Response.
                ok(businessLevelAPIs).
                build();
    }

    /**
     * Add business level to the given organization.
     *
     * @param organizationUniqueId unique id of the organization
     * @param businessLevelAPI user supplied values
     * @return a list of <code>BusinessLevelAPI</code>
     */
    @POST
    @SecuredService(permissions = {"businesslevel:create"})
    public Response addBusinessLevelToOrganization(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            BusinessLevelAPI businessLevelAPI) {
        LOG.log(Level.FINEST, "addBusinessLevelToOrganization( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        businessLevelAPI = businessLevelController.addBusinessLevelToOrganization(organizationUniqueId, businessLevelAPI);
        if( businessLevelAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.
                ok(businessLevelAPI).
                build();
    }

    /**
     * Add business level to the given organization.
     *
     * @param organizationUniqueId unique id of the organization
     * @param businessLevelId unique id of the business level
     * @return the inactivated <code>BusinessLevelAPI</code>
     */
    @POST
    @Path("{businessLevelId}/inactivate")
    @SecuredService(permissions = {"businesslevel:inactivate"})
    public Response inactivateBusinessLevelOnOrganization(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("businessLevelId") long businessLevelId) {
        LOG.log(Level.FINEST, "addBusinessLevelToOrganization( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        BusinessLevelAPI businessLevelAPI = businessLevelController.inactivateBusinessLevelOnOrganization(organizationUniqueId, businessLevelId);
        if( businessLevelAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.
                ok(businessLevelAPI).
                build();
    }

    /**
     * Edit business level for the given organization.
     *
     * @param organizationUniqueId unique id of the organization
     * @param businessLevelId unique id of the business level
     * @return the inactivated <code>BusinessLevelAPI</code>
     */
    @POST
    @Path("{businessLevelId}/update")
    @SecuredService(permissions = {"businesslevel:inactivate"})   //?
    public Response editBusinessLevelOnOrganization(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("businessLevelId") long businessLevelId,
            BusinessLevelAPI businessLevelAPI) {
        LOG.log(Level.FINEST, "editBusinessLevelOnOrganization( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        businessLevelAPI = businessLevelController.editBusinessLevelOnOrganization(organizationUniqueId, businessLevelId, businessLevelAPI);
        if( businessLevelAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.
                ok(businessLevelAPI).
                build();
    }

    /**
     * Remove business level from the given organization.
     *
     * @param organizationUniqueId unique id of the organization
     * @param businessLevelId unique id of the business level
     * @return
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     * e.g. there are <code>UserEngagement</code>s connnected to the business level.
     */
    @DELETE
    @Path("{businessLevelId}")
    @SecuredService(permissions = {"businesslevel:delete"})
    public Response deleteBusinessLevelFromOrganization(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("businessLevelId") long businessLevelId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteBusinessLevelFromOrganization( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        BusinessLevelAPI businessLevelAPI = businessLevelController.deleteBusinessLevelFromOrganization(organizationUniqueId, businessLevelId);
        return Response.
                ok(businessLevelAPI).
                build();
    }

}
