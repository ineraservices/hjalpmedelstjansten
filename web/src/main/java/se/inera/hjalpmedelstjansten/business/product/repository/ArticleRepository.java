package se.inera.hjalpmedelstjansten.business.product.repository;

import java.util.Date;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.NoArgsConstructor;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Product.Status;

@Stateless
@NoArgsConstructor
public class ArticleRepository {

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    public List<Article> findByStatusAndReplacementDatePassed(Status status, Date replacementDate) {
        return em.createNamedQuery(Article.FIND_BY_STATUS_AND_REPLACEMENT_DATE_PASSED, Article.class)
            .setParameter("status", status)
            .setParameter("replacementDate", replacementDate)
            .getResultList();
    }

    public List<Article> findByStatusAndReplacementDateBetweenFromAndTo(Status status, Date from, Date to) {
        return em.createNamedQuery(Article.FIND_BY_STATUS_AND_REPLACEMENT_DATE, Article.class)
            .setParameter("status", status)
            .setParameter("replacementDateFrom", from)
            .setParameter("replacementDateTo", to)
            .getResultList();
    }
}
