package se.inera.hjalpmedelstjansten.business.product.view;

import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.product.controller.CategoryController;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyApiEXT;
import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyListValueAPIEXT;
import se.inera.hjalpmedelstjansten.model.api.ChangeCategoryOnProductAPI;
import se.inera.hjalpmedelstjansten.model.api.ExternalCategoryGroupingAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Category;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * REST API for product categories
 *
 */
@Stateless
@Path("categories")
@Interceptors({ PerformanceLogInterceptor.class })
public class CategoryService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    private CategoryController categoryController;

    @EJB
    private AuthHandler authHandler;

    /**
     * Search categories by user query
     *
     * @param query find all categories with name like this
     * @param code search categories children to this category with this code
     * @param articleType find category of this articletype without code, should only be one
     * @param excludeCodelessCategories whether to exclude categories without code
     * from search results
     * @param excludeCodeCategories
     * @return a list of <code>CategoryAPI</code> matching the query and a
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation of parameters fail
     */
    @GET
    @SecuredService(permissions = {"category:view"})
    public Response searchCategories(@DefaultValue(value = "") @QueryParam("query") String query,
            @DefaultValue(value = "") @QueryParam("code") String code,
            @QueryParam("type") Article.Type articleType,
            @DefaultValue(value = "false") @QueryParam("excludeCodelessCategories") boolean excludeCodelessCategories,
            @DefaultValue(value = "false") @QueryParam("excludeCodeCategories") boolean excludeCodeCategories) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "searchCategories( code: {0}, articleType: {1} )", new Object[] {code, articleType});
        List<CategoryAPI> categoryAPIs =  categoryController.searchCategories(query, code, articleType, excludeCodelessCategories, excludeCodeCategories);
        return Response.ok(categoryAPIs).build();
    }

    /**
     * Get properties for a specific query
     *
     * @param categoryUniqueId
     * @return a list of <code>CategorySpecificPropertyAPI</code> matching the category
     */
    @GET
    @Path("{categoryUniqueId}/properties")
    @SecuredService(permissions = {"category:view"})
    public Response getCategorySpecificProperties(@PathParam("categoryUniqueId") long categoryUniqueId) {
        LOG.log(Level.FINEST, "getCategorySpecificProperties( categoryUniqueId: {0} )", new Object[] {categoryUniqueId});
        List<CategorySpecificPropertyAPI> categoryPropertyAPIs =  categoryController.getCategoryPropertyAPIs(categoryUniqueId);
        return Response.ok(categoryPropertyAPIs).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @Path("/exportCategories/{selected}")
    @SecuredService(permissions = {"category:view"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportIsoCategories(@Context HttpServletRequest httpServletRequest, @PathParam("selected") String selected
) throws HjalpmedelstjanstenValidationException {
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);



        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
        String filename = "Export_ISO_kategorier" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";

        byte[] exportBytes = categoryController.exportFileIso(userAPI, authHandler.getSessionId(), selected);
        LOG.log( Level.FINEST, "Content-Length: {0}", new Object[] {exportBytes.length});
        LOG.log( Level.INFO, "Content-Length: {0}", new Object[] {exportBytes.length});
        return Response.ok(
                exportBytes,
                jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM).
                header("Content-Disposition", "attachment; filename=" + filename).
                header("Content-Length", exportBytes.length).
                build();

    }

    @POST
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @Path("/{code}/exportCategory")
    @SecuredService(permissions = {"category:view"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportIsoCategory(@Context HttpServletRequest httpServletRequest, @PathParam("code") String code
    ) throws HjalpmedelstjanstenValidationException {
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);



        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
        String filename = "Export_ISO_kategori" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";

        byte[] exportBytes = categoryController.exportFileIsoCategory(userAPI, authHandler.getSessionId(), code);
        LOG.log( Level.FINEST, "Content-Length: {0}", new Object[] {exportBytes.length});
        LOG.log( Level.INFO, "Content-Length: {0}", new Object[] {exportBytes.length});
        return Response.ok(
                        exportBytes,
                        jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM).
                header("Content-Disposition", "attachment; filename=" + filename).
                header("Content-Length", exportBytes.length).
                build();

    }

    @DELETE
    @SecuredService(permissions = {"organization:delete"})
    @Path("{categoryUniqueId}/delete")

    public Response deleteCategory(@PathParam("categoryUniqueId") long categoryUniqueId) throws HjalpmedelstjanstenValidationException
    {
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        LOG.log(Level.FINEST, "User {0} attempting to delete category {1}", new Object[] {userAPI.getUsername(),categoryUniqueId});
        boolean result = categoryController.deleteCategory(categoryUniqueId);
        return Response.ok(result).build();
    }

    @PUT
    @SecuredService(permissions = {"organization:delete"})
    @Path("/create")
    public Response createCategory(@Context HttpServletRequest httpServletRequest,
                                  CategoryAPI categoryAPI) throws HjalpmedelstjanstenValidationException {

        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        LOG.log(Level.FINEST, "User {0} attempting to create new category {1}", new Object[] {userAPI.getUsername(),categoryAPI.getName()});

        Category result  = categoryController.createCategory(categoryAPI);
        return Response.ok(result).build();


    }

    @GET
    @SecuredService(permissions = {"category:view"})
    @Path("getCategoryById/{uniqueId}")
    public Response getCategoryByUniqueId(@PathParam("uniqueId") long uniqueId){
        List<CategoryAPI> categoryAPI = categoryController.getCategoryAPIbyId(uniqueId);
        return Response.ok(categoryAPI).build();
    }

    @GET
    @SecuredService(permissions = {"organization:delete"})
    @Path("{categoryUniqueId}")
    public Response getCategory(@PathParam("categoryUniqueId") long categoryUniqueId){
        List <Category> category = categoryController.getCategory(categoryUniqueId);
        return Response.ok(category).build();
    }

    @GET
    @SecuredService(permissions = {"category:view"})
    @Path("/allcategories")
    public Response getAllCategories(){

        List<CategoryAPI> categoryApis = categoryController.getAllCategories();
        return Response.ok(categoryApis).build();

    }

    @GET
    @SecuredService(permissions = {"category:view"})
    @Path("/categoriesWithCSP")
    public Response getCategoriesWithCSP(){
        List<CategoryAPI> categoryApis = categoryController.getAllCategoriesWithCSP();

        return Response.ok(categoryApis).build();

    }

    @GET
    @SecuredService(permissions = {"category:view"})
    @Path("/allcategories/{categoryUniqueId}/articles")
    public Response getArticlesByCategory(@PathParam("categoryUniqueId") long categoryUniqueId) {

        List<ArticleAPI> articleAPIs = categoryController.getArticlesByCategory(categoryUniqueId);
        return Response.ok(articleAPIs).build();
    }

    @GET
    @SecuredService(permissions = {"category:view"})
    @Path("/allcategories/{categoryUniqueId}/products")
    public Response getProductsByCategory(@PathParam("categoryUniqueId") long categoryUniqueId) {

        List<ProductAPI> productAPIs = categoryController.getProductsByCategory(categoryUniqueId);
        return Response.ok(productAPIs).build();
    }


    @GET
    @SecuredService(permissions = {"organization:delete"})
    @Path("/getbycode/{code}")
    public Response getCategoriesByCode(@PathParam("code") String code){

        List <Category> category = categoryController.getCategoriesByCode(code);
        return Response.ok(category).build();
    }

    @PUT
    @SecuredService(permissions = {"organization:delete"})
    @Path("/changeProductCategory")
    public Response changeProductCategory(@Context HttpServletRequest httpServletRequest,ChangeCategoryOnProductAPI changeCategoryOnProductAPI) throws HjalpmedelstjanstenValidationException{

        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        LOG.log(Level.FINEST, "User {0} attempting to change products with id {1} to category {2}", new Object[] {userAPI.getUsername(), changeCategoryOnProductAPI.getProducts(), changeCategoryOnProductAPI.getToCategory()});

        boolean result = categoryController.ChangeCategorysOnProducts(changeCategoryOnProductAPI);
        return Response.ok(result).build();
    }


    @PUT
    @SecuredService(permissions = {"organization:delete"})
    @Path("/CreateProperty")
    public Response CreateProperty(@Context HttpServletRequest httpServletRequest, CategorySpecificPropertyApiEXT categorySpecificPropertyApiEXT) throws HjalpmedelstjanstenValidationException{

        boolean result = categoryController.SaveProperty(categorySpecificPropertyApiEXT);
        return Response.ok(result).build();
    }

    @PUT
    @SecuredService(permissions = {"organization:delete"})
    @Path("/CreatePropertyListItem")
    public Response CreatePropertyItem(@Context HttpServletRequest httpServletRequest, CategorySpecificPropertyListValueAPIEXT categorySpecificPropertyListValueApiEXT) throws HjalpmedelstjanstenValidationException{

        long result = categoryController.SavePropertyListItem(categorySpecificPropertyListValueApiEXT);
        return Response.ok(result).build();
    }

    @POST
    @SecuredService(permissions = {"organization:delete"})
    @Path("/DeleteProperty")
    public Response DeleteProperty(@Context HttpServletRequest httpServletRequest, long propertyId) throws HjalpmedelstjanstenValidationException{

        boolean result = categoryController.DeleteProperty(propertyId);
        return Response.ok(result).build();
    }
    @POST
    @SecuredService(permissions = {"organization:delete"})
    @Path("/DeletePropertyItem")
    public Response DeletePropertyItem(@Context HttpServletRequest httpServletRequest, long propertyId) throws HjalpmedelstjanstenValidationException{

        boolean result = categoryController.DeletePropertyItem(propertyId);
        return Response.ok(result).build();
    }

    @POST
    @SecuredService(permissions = {"organization:delete"})
    @Path("/UpdateProperty")
    public Response DeletePropertyItem(@Context HttpServletRequest httpServletRequest, CategorySpecificPropertyApiEXT categorySpecificPropertyApiEXT) throws HjalpmedelstjanstenValidationException{

        boolean result = categoryController.UpdateProperty(categorySpecificPropertyApiEXT);
        return Response.ok(result).build();
    }

    @POST
    @SecuredService(permissions = {"organization:delete"})
    @Path("/UpdatePropertyListOrder")
    public Response UpdatePropertySpecificItemOrder(@Context HttpServletRequest httpServletRequest, CategorySpecificPropertyApiEXT categorySpecificPropertyApiEXT) throws HjalpmedelstjanstenValidationException{

        boolean result = categoryController.UpdatePropertyOrder(categorySpecificPropertyApiEXT);
        return Response.ok(result).build();
    }

    /**
     * Get external categories
     *
     */

    @GET
    @SecuredService(permissions = {"category:view"})
    @Path("/getExternalCategories")
    public Response getExternalCategories() {
        //TODO!! Ändra till ExternalcategorygroupingAPI
        List<ExternalCategoryGroupingAPI> externalCategoryGroupingsAPI = categoryController.getExternalCategories();
        return Response.ok(externalCategoryGroupingsAPI).build();
    }

    @GET
    @SecuredService(permissions = {"category:view"})
    @Path("/getExternalCategoriesForAssortment")
    public Response getExternalCategoriesForAssortment() {
        List<ExternalCategoryGroupingAPI> externalCategoryGroupingsAPI = categoryController.getExternalCategoriesForAssortment();
        return Response.ok(externalCategoryGroupingsAPI).build();
    }

    @GET
    @SecuredService(permissions = {"category:view"})
    @Path("/getExternalCategories/{categoryId}/categories")
    public Response getExternalCategoryToCategoryId(@PathParam("categoryId") Long categoryId) {

        SearchDTO searchDTO = categoryController.getExternalCategoryToCategoryId(categoryId);

        if( searchDTO == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(searchDTO.getItems()).
                header("X-Total-Count", searchDTO.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                    build();
        }
    }

    @GET
    @SecuredService(permissions = {"category:view"})
    @Path("/getExternalCategories/{parentId}")
    public Response getExternalCategoryChildren(@PathParam("parentId") Long parentId) {
        List<ExternalCategoryGroupingAPI> externalCategoryGroupingsAPI = categoryController.getExternalCategoryChildren(parentId);
        return Response.ok(externalCategoryGroupingsAPI).build();
    }

    @PUT
    @SecuredService(permissions = {"organization:delete"})
    @Path("/createExternalCategory")
    public Response createExternalCategory(@Context HttpServletRequest httpServletRequest,
                                   ExternalCategoryGroupingAPI externalCategoryGroupingAPI) {

        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        LOG.log(Level.FINEST, "User {0} attempting to create new external category {1}", new Object[] {userAPI.getUsername(),externalCategoryGroupingAPI.getName()});

        ExternalCategoryGroupingAPI result  = categoryController.createExternalCategory(externalCategoryGroupingAPI);
        return Response.ok(result).build();


    }

    @DELETE
    @SecuredService(permissions = {"organization:delete"})
    @Path("/deleteExternalCategory/{uniqueId}")

    public Response deleteExternalCategory(@PathParam("uniqueId") long uniqueId) throws HjalpmedelstjanstenValidationException
    {
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        LOG.log(Level.FINEST, "User {0} attempting to delete external category {1}", new Object[] {userAPI.getUsername(),uniqueId});
        boolean result = categoryController.deleteExternalCategory(uniqueId);
        return Response.ok(result).build();
    }


}
