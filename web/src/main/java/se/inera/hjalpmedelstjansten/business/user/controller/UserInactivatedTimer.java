package se.inera.hjalpmedelstjansten.business.user.controller;

import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductController;
import se.inera.hjalpmedelstjansten.clustering.ClusterController;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.UserEngagementAPI;
import se.inera.hjalpmedelstjansten.model.entity.*;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import jakarta.ejb.*;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

/**
 * Timer for inactivated users where valid to has passed.
 *
 * @author Per Abrahamsson
 */
@Singleton
@Startup
@DependsOn("PropertyLoader")
public class UserInactivatedTimer {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private boolean userInactivatedTimerEnabled;

    @Inject
    private String userInactivatedTimerHour;

    @Inject
    private String userInactivatedTimerMinute;

    @Inject
    private String userInactivatedTimerSecond;

    @Inject
    private AgreementController agreementController;

    @Inject
    private UserController userController;

    @Inject
    private ClusterController clusterController;

    @Resource
    private TimerService timerService;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Timeout
    private void schedule() {
        LOG.log( Level.FINEST, "schedule" );
        // attempt to get lock
        boolean lockReceived = clusterController.getLock(this.getClass().getName(), 30);
        if( lockReceived ) {
            LOG.log( Level.FINEST, "Received lock!" );
            long start = System.currentTimeMillis();
            inactivateUsersByDate();
            long total = System.currentTimeMillis() - start;
            if( total > 60000 ) {
                LOG.log( Level.WARNING, "Scheduled job took: {0} ms which is more than 1 minute which is a long time, a developer should take a look at this.", new Object[] {total});
            } else {
                LOG.log( Level.INFO, "Scheduled job took: {0} ms.", new Object[] {total});
            }
            //release lock
            clusterController.releaseLock(this.getClass().getName());
        } else {
            LOG.log( Level.INFO, "Did not receive lock!" );
        }
    }

    /**
     * Inactivate all users with valid to set and has passed.
     * i.e. clear checkboxes for all different roles,
     * remove from agreements approvers list unless the last one on future and current agreements (customer),
     * remove from agreements contact list (supplier)
     */
    public void inactivateUsersByDate() {
        LOG.log(Level.FINEST, "inactivateUsersByDate()");
        List<UserEngagement> userEngagementsWithValidToPassed = em.createNamedQuery(UserEngagement.FIND_BY_VALID_TO_PASSED).
                setParameter("thisDate", new Date()).
                getResultList();
        if( userEngagementsWithValidToPassed != null && !userEngagementsWithValidToPassed.isEmpty() ) {
            LOG.log( Level.FINEST, "Found {0} user engagements to inactivate because of valid to passed", new Object[] {userEngagementsWithValidToPassed.size()});

            for( UserEngagement userEngagement : userEngagementsWithValidToPassed ) {
                userEngagement.setRoles(null); //clear checkboxes, remove supplier from contact list
                boolean isCustomer = userEngagement.getOrganization().getOrganizationType().equals(Organization.OrganizationType.CUSTOMER);
                // remove customer from approvers list unless the last one on future and current agreements
                if (isCustomer) {
                    LOG.log(Level.FINEST, "userEngagement: {0}", new Object[]{userEngagement});
                    List<Agreement> agreementList = agreementController.findByUserAccount(userEngagement.getUserAccount());
                    List<Agreement> handleAgreementList = new ArrayList<>();
                    for (Agreement agreement : agreementList) {
                        List<UserEngagement> approvers = agreement.getCustomerPricelistApprovers();
                        LOG.log(Level.FINEST, "number of approvers: {0}", new Object[]{approvers.size()} );
                        if (agreement.getStatus().equals(Agreement.Status.DISCONTINUED)) { //ok to remove all approvers
                            LOG.log(Level.INFO, "agreement is discontinued, ok to remove them all");
                            approvers.remove(userEngagement);
                        } else { //not discontinued
                            LOG.log(Level.FINEST, "agreement is NOT discontinued, check if last approver");
                            if (approvers.size() == 1) { //last approver, add agreement to list
                                LOG.log(Level.FINEST, "last approver, cannot remove, add agreement to list.");
                                handleAgreementList.add(agreement);
                            } else {
                                LOG.log(Level.FINEST, "atleast one more approver, ok to remove");
                                approvers.remove(userEngagement);
                            }
                        }
                    }
                    if (handleAgreementList.size() > 0) { //send email to local admin(s)
                        //find local admin
                        List<UserRole.RoleName> roleNames = new ArrayList<>();
                        roleNames.add(UserRole.RoleName.Customeradmin);

                        Long orgId = userEngagement.getOrganization().getUniqueId();
                        LOG.log(Level.FINEST, "orgName {0}, roleNames(0) {1}", new Object[]{userEngagement.getOrganization().getOrganizationName(), roleNames.get(0)});
                        List<UserAPI> userAPIs = userController.getUsersInRolesOnOrganization(orgId, roleNames);
                        LOG.log(Level.FINEST, "local admins: {0}", new Object[]{userAPIs.size()});
                        for (UserAPI userAPI : userAPIs) {
                            LOG.log(Level.FINEST, "send mail to {0} regarding roles of inactivated user {1}", new Object[]{userAPI.getElectronicAddress().getEmail(), userEngagement.getUserAccount().getUsername()});
                            agreementController.sendMailRegardingRolesOfInactivatedCustomer(handleAgreementList, userEngagement, userAPI.getElectronicAddress().getEmail());
                        }
                    }
                }
            }
        } else {
            LOG.log( Level.FINEST, "Found NO user engagements to inactivate because of valid to passed");
        }
    }

    @PostConstruct
    private void initialize() {
        LOG.log( Level.FINEST, "initialize()" );
        if( userInactivatedTimerEnabled ) {
            LOG.log( Level.FINEST, "Timer is ENABLED" );
            if (timerService.getTimers().isEmpty()) {
                String name = this.getClass().getName();
                TimerConfig configuration = new TimerConfig();
                configuration.setPersistent(false);
                configuration.setInfo(name);
                ScheduleExpression scheduleExpression = new ScheduleExpression();
                scheduleExpression.hour(userInactivatedTimerHour).minute(userInactivatedTimerMinute).second(userInactivatedTimerSecond);
                timerService.createCalendarTimer(scheduleExpression, configuration);
            }
        } else {
            LOG.log( Level.INFO, "Timer is NOT ENABLED" );
        }
    }


}
