package se.inera.hjalpmedelstjansten.business.indexing.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.clustering.ClusterController;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import jakarta.ejb.DependsOn;
import jakarta.ejb.ScheduleExpression;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.ejb.Timeout;
import jakarta.ejb.TimerConfig;
import jakarta.ejb.TimerService;
import jakarta.inject.Inject;
import java.util.logging.Level;

/**
 * Timer for reindexing products and articles
 *
 */
@Singleton
@Startup
@DependsOn("PropertyLoader")
public class ReIndexTimer {

    @Inject
    HjmtLogger LOG;

    @Inject
    private boolean reindexProductsAndArticlesTimerEnabled;

    @Inject
    private String reindexProductsAndArticlesTimerHour;

    @Inject
    private String reindexProductsAndArticlesTimerMinute;

    @Inject
    private String reindexProductsAndArticlesTimerSecond;

    @Inject
    private String reindexProductsAndArticlesDateYear;

    @Inject
    private String reindexProductsAndArticlesDateMonth;

    @Inject
    private String reindexProductsAndArticlesDateDay;

    @Inject
    private ReIndexController reIndexController;

    @Inject
    private ClusterController clusterController;

    @Resource
    private TimerService timerService;

    @Timeout
    protected void schedule() {
        LOG.log( Level.FINEST, "schedule" );
        boolean lockReceived = clusterController.getLock(this.getClass().getName(), 30);
        if( lockReceived ) {
            LOG.log( Level.FINEST, "Received lock!" );
            long start = System.currentTimeMillis();
            reIndexController.reIndexAllProductsAndArticles();
            long total = System.currentTimeMillis() - start;
            if( total > 60000 ) {
                LOG.log( Level.WARNING, "Scheduled job took: {0} ms which is more than 1 minute which is a long time, a developer should take a look at this.", new Object[] {total});
            } else {
                LOG.log( Level.INFO, "Scheduled job took: {0} ms.", new Object[] {total});
            }
            //release lock
            clusterController.releaseLock(this.getClass().getName());
        } else {
            LOG.log( Level.INFO, "Did not received lock!" );
        }
    }

    @PostConstruct
    private void initialize() {
        LOG.log( Level.FINEST, "initialize()" );
        if( reindexProductsAndArticlesTimerEnabled ) {
            LOG.log( Level.FINEST, "Timer is ENABLED" );
            if (timerService.getTimers().isEmpty()) {
                String name = this.getClass().getName();
                TimerConfig configuration = new TimerConfig();
                configuration.setPersistent(false);
                configuration.setInfo(name);
                ScheduleExpression scheduleExpression = new ScheduleExpression();

                if(reindexProductsAndArticlesDateYear != null && reindexProductsAndArticlesDateMonth != null && reindexProductsAndArticlesDateDay != null) {
                    scheduleExpression.year(reindexProductsAndArticlesDateYear)
                                      .month(reindexProductsAndArticlesDateMonth)
                                      .dayOfMonth(reindexProductsAndArticlesDateDay)
                                      .hour(reindexProductsAndArticlesTimerHour)
                                      .minute(reindexProductsAndArticlesTimerMinute)
                                      .second(reindexProductsAndArticlesTimerSecond);
                } else {
                    scheduleExpression.hour(reindexProductsAndArticlesTimerHour).minute(reindexProductsAndArticlesTimerMinute).second(reindexProductsAndArticlesTimerSecond);
                }

                timerService.createCalendarTimer(scheduleExpression, configuration);
            }
        } else {
            LOG.log( Level.INFO, "Timer is NOT ENABLED" );
        }
    }
}
