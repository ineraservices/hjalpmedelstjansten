package se.inera.hjalpmedelstjansten.business.product.controller.exportimport;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.stream.Collectors;
import org.apache.poi.ooxml.POIXMLProperties;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFName;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.helpers.ExportHelper;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.product.controller.SearchController;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.ResourceSpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.SearchProductsAndArticlesAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificPropertyListValue;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEDirective;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEStandard;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVDocumentType;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;
import se.inera.hjalpmedelstjansten.model.entity.media.ArticleMediaDocument;
import se.inera.hjalpmedelstjansten.model.entity.media.ArticleMediaImage;
import se.inera.hjalpmedelstjansten.model.entity.media.ArticleMediaVideo;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaDocument;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaImage;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaVideo;

/**
 * Class for handling business logic of exporting products and articles.
 *
 */
@Stateless
public class ExportProductsArticlesController extends ExportImportProductsArticlesController {

    @Inject
    HjmtLogger LOG;

    @Inject
    SearchController searchController;

    @Inject
    ExportHelper exportHelper;

    private CellStyle unlockedCellStyle;
    private CellStyle unlockedTextCellStyle;
    private CellStyle greyStyle;
    private CellStyle unlockedDateCellStyle;

    //Previously was instantiated as many times as a row was created. Fix for HJAL-1962.
    private CellStyle orangeStyle;
    private CellStyle goldStyle;

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);
    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DATE_PATTERN);

    public byte[] exportFile(long organizationUniqueId,
                             Set<Long> categoryIdList,
                             List<Long> productIds,
                             boolean includeProductsAndArticles,
                             boolean includeT,
                             boolean includeTj,
                             boolean includeI,
                             boolean includeR,
                             boolean pIncludeA,
                             boolean pIncludeT,
                             boolean pIncludeTj,
                             boolean pIncludeI,
                             boolean pIncludeR,
                             boolean includePublished,
                             boolean includeDiscontinued,
                             UserAPI userAPI,
                             String sessionId,
                             String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "export( ... )");
        LOG.log(Level.FINEST, "productIds {0}", new Object[]{productIds});
        //long startTime = System.nanoTime();
        boolean exportByProducts = false;
        if (productIds.size() == 0) {
            LOG.log(Level.FINEST, "export by categories");
        }
        else {
            exportByProducts = true;
            LOG.log(Level.FINEST, "export by products");
        }

        Session session = em.unwrap(Session.class);
        session.setHibernateFlushMode(FlushMode.MANUAL);

        try (SXSSFWorkbook workbook = new SXSSFWorkbook()) {

            ZipSecureFile.setMinInflateRatio(0);

            this.unlockedCellStyle = createUnlockedCellStyle(workbook);
            this.unlockedTextCellStyle = createUnlockedTextCellStyle(workbook);

            this.unlockedDateCellStyle = createUnlockedDateCellStyle(workbook);

            this.orangeStyle = workbook.createCellStyle();
            this.orangeStyle.setFillForegroundColor(IndexedColors.ORANGE.index);
            this.orangeStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            this.goldStyle = workbook.createCellStyle();
            this.goldStyle.setFillForegroundColor(IndexedColors.GOLD.index);
            this.goldStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            // greystyle cellstyle used for VALUELIST_MULTIPLE since they cannot be handled using Excel
            this.greyStyle = workbook.createCellStyle();
            this.greyStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.index);
            this.greyStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            workbook.getXSSFWorkbook().lockStructure();

            POIXMLProperties properties = workbook.getXSSFWorkbook().getProperties();
            POIXMLProperties.CustomProperties customProperties = properties.getCustomProperties();

            // set export type
            customProperties.addProperty(EXPORT_TYPE_NAME, EXPORT_TYPE);

            // set export version
            customProperties.addProperty(EXPORT_VERSION_NAME, EXPORT_VERSION);

            // find values for drop downs
            List<CVCEStandard> standards = ceController.findAllCEStandards();
            List<CVCEDirective> directives = ceController.findAllCEDirectives();
            List<CVOrderUnit> orderUnits = orderUnitController.findAll();
            List<CVPackageUnit> packageUnits = packageUnitController.findAll();
            List<CVDocumentType> documentTypes = documentTypeController.findAll();
            List<CVPreventiveMaintenance> preventiveMaintenances = preventiveMaintenanceUnitController.getAllPreventiveMaintenances();
            //HJAL-2115
            //set how many years back and forward dates will go. 16383 is the maximum number of columns in Excel (almost 45 years...) so yearsBack + yearsForward < 45
            List<String> dates = getDates(5, 5);

            List<SXSSFSheet> listOfSheetsThatWillHaveTheirHeaderRowsLocked = new ArrayList<>();

            SXSSFSheet imagesSheet = workbook.createSheet(IMAGES_SHEET_NAME);
            prepareMediaSheet(imagesSheet, unlockedTextCellStyle, MediaType.IMAGE);
            listOfSheetsThatWillHaveTheirHeaderRowsLocked.add(imagesSheet);
            imagesSheet.trackAllColumnsForAutoSizing();

            SXSSFSheet documentsSheet = workbook.createSheet(DOCUMENTS_SHEET_NAME);
            prepareMediaSheet(documentsSheet, unlockedTextCellStyle, MediaType.DOCUMENT);
            listOfSheetsThatWillHaveTheirHeaderRowsLocked.add(documentsSheet);
            documentsSheet.trackAllColumnsForAutoSizing();

            SXSSFSheet videosSheet = workbook.createSheet(VIDEOS_SHEET_NAME);
            prepareMediaSheet(videosSheet, unlockedTextCellStyle, MediaType.VIDEO);
            listOfSheetsThatWillHaveTheirHeaderRowsLocked.add(videosSheet);
            videosSheet.trackAllColumnsForAutoSizing();

            SXSSFSheet valueListSheet = workbook.createSheet(VALUELIST_SHEET_NAME);
            valueListSheet.protectSheet(UUID.randomUUID().toString());
            //valueListSheet.protectSheet("apa");
            valueListSheet.lockFormatColumns(false);
            valueListSheet.trackAllColumnsForAutoSizing();

            SXSSFSheet categoriesSheet = workbook.createSheet(CATEGORIES_SHEET_NAME);
            categoriesSheet.protectSheet(UUID.randomUUID().toString());
            //categoriesSheet.protectSheet("apa");
            categoriesSheet.lockFormatColumns(false);
            categoriesSheet.trackAllColumnsForAutoSizing();

            // fill tab value list with values
            int lastValueListRow = fillValueListSheet(valueListSheet,
                    standards,
                    directives,
                    orderUnits,
                    packageUnits,
                    preventiveMaintenances,
                    documentTypes,
                    dates);

            List<SXSSFSheet> categoriesSheets = new ArrayList<>();
            Map<Long, List<ProductAPI>> categoryToProductsMap = null;
            boolean pIncludeP = false;
            List<ProductAPI> products = new ArrayList<>();
            if (exportByProducts) {
                pIncludeP = true;
                categoryToProductsMap = new HashMap<>();
                //long startTime1 = System.nanoTime();
                for (Long productId : productIds) {
                    ProductAPI productAPI;
                    productAPI = productController.getProductAPI(organizationUniqueId, productId, userAPI, sessionId, requestIp);

                    if (includePublished && productAPI.getStatus().equals("PUBLISHED") || includeDiscontinued && productAPI.getStatus().equals("DISCONTINUED")) {
                        products.add(productAPI);
                        categoryIdList.add(productAPI.getCategory().getId());
                    }
                }
                //LOG.log(Level.INFO, "XXX productId loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime1)});
            }
            if (categoryIdList.size() > 100) {
                throw validationMessageService.generateValidationException("export", "export.productarticle.tooManyCategories");
            }

            int categoriesSheetRowNum = 0;

            int articleQuantityInOuterPackageUnitColumnNumber = 0;
            int packageContentUnitColumnNumber = 0;
            int orderUnitColumnNumber = 0;
            int preventiveMaintenanceValidFromColumnNumber = 0;
            int customerUniqueColumnNumber = 0;
            int ceMarkedColumnNumber = 0;
            int standardsColumnNumber = 0;
            int directivesColumnNumber = 0;
            int discontinuedColumnNumber = 0;
            int articleQuantityInOuterPackageColumnNumber = 0;
            int packageContentColumnNumber = 0;
            int packageLevelBaseColumnNumber = 0;
            int packageLevelMiddleColumnNumber = 0;
            int packageLevelTopColumnNumber = 0;
            int preventiveMaintenanceNumberOfDaysColumnNumber = 0;
            int inactivateRowsOnDiscontinuedColumnNumber = 0;


            if (includeI || pIncludeI) {
                List<Category> catList = categoryController.getCategoryByArticleType(Article.Type.I);
                //long startTime2 = System.nanoTime();
                for (Category category : catList) {
                    categoryIdList.add(category.getUniqueId());
                }
                //LOG.log(Level.INFO, "XXX I loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime2)});
            }
            if (includeT || pIncludeT) {
                List<Category> catList = categoryController.getCategoryByArticleType(Article.Type.T);
                //long startTime2 = System.nanoTime();
                for (Category category : catList) {
                    categoryIdList.add(category.getUniqueId());
                }
                //LOG.log(Level.INFO, "XXX T loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime2)});
            }
            if (includeTj || pIncludeTj) {
                List<Category> catList = categoryController.getCategoryByArticleType(Article.Type.Tj);
                //long startTime2 = System.nanoTime();
                for (Category category : catList) {
                    categoryIdList.add(category.getUniqueId());
                }
                //LOG.log(Level.INFO, "XXX Tj loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime2)});
            }
            if (includeR || pIncludeR) {
                List<Category> catList = categoryController.getCategoryByArticleType(Article.Type.R);
                //long startTime2 = System.nanoTime();
                for (Category category : catList) {
                    categoryIdList.add(category.getUniqueId());
                }
                //LOG.log(Level.INFO, "XXX R loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime2)});
            }

            List<Category> categories = new ArrayList<>();
            //long startTime3 = System.nanoTime();
            for (Long categoryUniqueId : categoryIdList) {
                Category category = categoryController.getById(categoryUniqueId);
                if (category == null) {
                    throw validationMessageService.generateValidationException("export", "export.productarticle.categoryNotExists");
                }
                categories.add(category);
            }
            //LOG.log(Level.INFO, "XXX categoryIdList loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime3)});


                //Collections.sort(categories, (a, b) -> (a.getArticleType() == Article.Type.H && b.getArticleType() != Article.Type.H) ? -1 : (a.getArticleType() == Article.Type.H && b.getArticleType() == Article.Type.H) ? 0 : 1);
                //Collections.sort(categories, (a, b) -> (a.getArticleType() == Article.Type.H && b.getArticleType() != Article.Type.H) ? -1 : (a.getArticleType() == Article.Type.H && b.getArticleType() == Article.Type.H) ? 0 : (a.getArticleType() == Article.Type.T && a.getCode() != null && b.getArticleType() != Article.Type.T && b.getCode()!= null) ? 1 : (a.getArticleType() == Article.Type.T && a.getCode()!= null && b.getArticleType() == Article.Type.T && b.getCode()!= null) ? 2 : 3);
                //Collections.sort(categories, (a, b) -> (a.getArticleType() == Article.Type.H && b.getArticleType() != Article.Type.H) ? -1 : ((a.getArticleType() == Article.Type.H && b.getArticleType() == Article.Type.H) ? 0 : ((a.getArticleType() == Article.Type.T && a.getCode() != null && b.getArticleType() != Article.Type.T && b.getCode()!= null) ? 1 : ((a.getArticleType() == Article.Type.T && a.getCode()!= null && b.getArticleType() == Article.Type.T && b.getCode()!= null) ? 2 : 3))));
                Collections.sort(categories, (a, b) -> (a.getCode() != null && b.getCode() == null) ? -1 : (a.getCode() != null && b.getCode() != null ? 0 : 1));


            //categories.sort()
                //List<Long> mainCategories = new ArrayList<>();
                //LOG.log(Level.FINEST, "products: {0})", new Object[]{categories});
                //long startTime4 = System.nanoTime();
                for (Category category : categories) {

                    //Since list is sorted the articles of type H always come first in the list.
                    /*if ((pIncludeP && pIncludeA) || includeProductsAndArticles) {
                        if (category.getArticleType() == Article.Type.H) {
                            mainCategories.add(category.getUniqueId());
                        }
                    }*/


                    // add category to the categories sheet
                    SXSSFRow categoryRow = categoriesSheet.createRow(categoriesSheetRowNum++);
                    SXSSFCell categoryIdCell = categoryRow.createCell(0);
                    categoryIdCell.setCellValue(category.getUniqueId());
                    categoriesSheet.getRow(0).setZeroHeight(true);


                    SXSSFCell categoryNameCell = categoryRow.createCell(1);
                    categoryNameCell.setCellValue(category.getName());

                    SXSSFCell categoryCodeCell = categoryRow.createCell(2);
                    categoryCodeCell.setCellValue(category.getCode());

                    String sheetName = getSheetName(category.getCode(), category.getName());
                    SXSSFCell sheetNameCell = categoryRow.createCell(3);
                    sheetNameCell.setCellValue(sheetName);

                    List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(category.getUniqueId());

                    // create sheet and lock it in order to avoid users to edit fields that shouldn't be
                    SXSSFSheet sheet = workbook.createSheet(sheetName);
                    sheet.protectSheet(UUID.randomUUID().toString());
                    //sheet.protectSheet("apa");
                    sheet.lockFormatColumns(false);
                    listOfSheetsThatWillHaveTheirHeaderRowsLocked.add(sheet);
                    sheet.trackAllColumnsForAutoSizing();


                    int rowNumber = 0;

                    // write field names (used when reading back file), could have used
                    // column numbers alone, but that would break if the Excel file
                    // was changed in a later release
                    SXSSFRow fieldNamesRow = sheet.createRow(rowNumber++);
                    fieldNamesRow.setZeroHeight(true);

                    // write headers
                    int cellNumber = 0;
                    SXSSFRow headerRow = sheet.createRow(rowNumber++);
                    headerRow.createCell(cellNumber++).setCellValue("Unikt Id");

                    // type (Product/Article)
                    cellNumber = this.cellUnlockedCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_TYPE_NAME, "Produkt/Artikel (*)");

                    // number
                    cellNumber = this.cellUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_NUMBER_NAME, "Produkt-/Artikelnummer (*)");

                    // name
                    cellNumber = this.cellWithAdditionalFieldUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_NAME_NAME, "Produkt-/Artikelbenämning (*)");

                    // based on product
                    // HJAL-2022, 2081
                    cellNumber = this.cellWithAdditionalFieldUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_BASEDONPRODUCT_NAME, "Baseras på produkt");
                    //headerRow.createCell(cellNumber++).setCellValue("Baseras på produkt");

                    String connectionsHeading = addConnectionHeading(category);
                    if (connectionsHeading != null) {
                        cellNumber = this.cellWithAdditionalFieldUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_CONNECTED_TO_NAME, connectionsHeading);
                        LOG.log(Level.FINEST, "connectionsHeading: {0}, cellNumber: {1}", new Object[]{connectionsHeading, cellNumber});
                    }

                    // category
                    headerRow.createCell(cellNumber++).setCellValue("Kategori");

                    // also in categories
                    cellNumber = this.cellWithAdditionalFieldUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_EXTENDED_CATEGORIES_NAME, "Visas även i följande kategorier");

                    // gtin 13
                    cellNumber = this.cellWithAdditionalFieldUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_GTIN13_NAME, "GTIN-13");

                    // customer unique
                    cellNumber = this.cellWithAdditionalFieldUnlockedCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_CUSTOMERUNIQUE_NAME, "Kundunik");
                    customerUniqueColumnNumber = cellNumber - 1;

                    // order unit
                    cellNumber = this.cellWithAdditionalFieldUnlockedCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_ORDERUNIT_NAME, "Beställningsenhet (*)");
                    orderUnitColumnNumber = cellNumber - 1;

                    // article quantity in outer package
                    cellNumber = this.cellWithAdditionalFieldUnlockedCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_ARTICLEQUANTITYINOUTERPACKAGE_NAME, "Antal förpackningar");
                    articleQuantityInOuterPackageColumnNumber = cellNumber - 1;

                    // article quantity in outer package unit
                    cellNumber = this.cellWithAdditionalFieldUnlockedCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_ARTICLEQUANTITYINOUTERPACKAGEUNIT_NAME, "Enhet för antal förpackningar");
                    articleQuantityInOuterPackageUnitColumnNumber = cellNumber - 1;

                    // package content
                    cellNumber = this.cellWithAdditionalFieldUnlockedCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_PACKAGECONTENT_NAME, "Varje förpackning innehåller");
                    packageContentColumnNumber = cellNumber - 1;

                    // package content unit
                    cellNumber = this.cellWithAdditionalFieldUnlockedCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_PACKAGECONTENTUNIT_NAME, "Enhet för varje förpackning innehåller");
                    packageContentUnitColumnNumber = cellNumber - 1;

                    // package level base
                    cellNumber = this.cellWithAdditionalFieldUnlockedCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_PACKAGELEVELBASE_NAME, "Antal artiklar bas");
                    packageLevelBaseColumnNumber = cellNumber - 1;

                    // package level middle
                    cellNumber = this.cellWithAdditionalFieldUnlockedCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_PACKAGELEVELMIDDLE_NAME, "Antal artiklar mellan");
                    packageLevelMiddleColumnNumber = cellNumber - 1;

                    // package level top
                    cellNumber = this.cellWithAdditionalFieldUnlockedCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_PACKAGELEVELTOP_NAME, "Antal artiklar pall");
                    packageLevelTopColumnNumber = cellNumber - 1;

                    // ce marked
                    cellNumber = this.cellWithAdditionalFieldUnlockedCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_CEMARKED_NAME, "Artikeln har CE-märkning");
                    ceMarkedColumnNumber = cellNumber - 1;

                    // directive
                    cellNumber = this.cellWithAdditionalFieldUnlockedCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_DIRECTIVE_NAME, "CE-märkning/Direktiv");
                    directivesColumnNumber = cellNumber - 1;

                    // standard
                    cellNumber = this.cellWithAdditionalFieldUnlockedCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_STANDARD_NAME, "CE-märkning/Standard");
                    standardsColumnNumber = cellNumber - 1;

                    // preventive maintenance valid from
                    cellNumber = this.cellWithAdditionalFieldUnlockedCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_PREVENTIVEMAINTENANCE_VALIDFROM_NAME, "Förebyggande underhåll/Gäller från");
                    preventiveMaintenanceValidFromColumnNumber = cellNumber - 1;

                    // preventive maintenance interval
                    cellNumber = this.cellWithAdditionalFieldUnlockedCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_PREVENTIVEMAINTENANCE_INTERVAL_NAME, "Förebyggande underhåll/Intervall");
                    preventiveMaintenanceNumberOfDaysColumnNumber = cellNumber - 1;

                    // manufacturer
                    cellNumber = this.cellWithAdditionalFieldUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_MANUFACTURER_NAME, "Tillverkarens namn");

                    // manufacturer number
                    cellNumber = this.cellWithAdditionalFieldUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_MANUFACTURERNUMBER_NAME, "Tillverkarens produktnummer/artikelnummer");

                    // manufacturer web page
                    cellNumber = this.cellWithAdditionalFieldUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_MANUFACTURERWEB_NAME, "Tillverkarens webbsida");

                    // trademark
                    cellNumber = this.cellWithAdditionalFieldUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_TRADEMARK_NAME, "Varumärke");

                    // color
                    cellNumber = this.cellWithAdditionalFieldUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_COLOR_NAME, "Färg");

                    // supplementaryInformation
                    cellNumber = this.cellWithAdditionalFieldUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_SUPPLEMENTARYINFORMATION_NAME, "Kompletterande information");

                    // description for 1177
                    cellNumber = this.cellWithAdditionalFieldUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_DESCRIPTION_FOR_ELVASJUTTIOSJU, "Beskrivning som visas på 1177");

                    // main image
                    cellNumber = this.cellWithAdditionalFieldUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_MEDIA_URL_NAME, "Huvudbild URL (*)");

                    // main image description
                    cellNumber = this.cellWithAdditionalFieldUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_MEDIA_DESCRIPTION_NAME, "Huvudbild beskrivning");

                    // main image alt text
                    cellNumber = this.cellWithAdditionalFieldUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_MEDIA_ALTTEXT_NAME, "Huvudbild alt-text (*)");

                    // discontinued
                    //HJAL-2115
                    //cellNumber = this.cellWithAdditionalFieldUnlockedDateCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_DISCONTINUED_NAME, "Utgår datum");
                    cellNumber = this.cellWithAdditionalFieldUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_DISCONTINUED_NAME, "Utgår datum");
                    discontinuedColumnNumber = cellNumber - 1;

                    // inactivate rows on discontinued
                    cellNumber = this.cellWithAdditionalFieldUnlockedCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_INACTIVATE_ROWS_ON_DISCONTINUED_NAME, "Inaktivera rader");
                    inactivateRowsOnDiscontinuedColumnNumber = cellNumber - 1;

                    // replaced by
                    cellNumber = this.cellWithAdditionalFieldUnlockedTextCellStyle(fieldNamesRow, headerRow, sheet, cellNumber, FIELD_REPLACEDBY_NAME, "Ersätts av");

                    //Created
                    headerRow.createCell(cellNumber++).setCellValue("Skapad");

                    //Last updated
                    headerRow.createCell(cellNumber++).setCellValue("Senast uppdaterad");


                    List<Integer> categorySpecificPropertyIntegerColumns = new ArrayList<>();
                    if (categorySpecificPropertys != null && !categorySpecificPropertys.isEmpty()) {
                        //long startTime5 = System.nanoTime();
                        for (CategorySpecificProperty categorySpecificProperty : categorySpecificPropertys) {
                            LOG.log(Level.FINEST, "categorySpecificProperty.getName: {0})", new Object[]{categorySpecificProperty.getName()});
                            LOG.log(Level.FINEST, "categorySpecificProperty.getType: {0})", new Object[]{categorySpecificProperty.getType()});
                            if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.INTERVAL) {

                                headerRow.createCell(cellNumber++).setCellValue(categorySpecificProperty.getName() + " (min)");
                                fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_CATEGORYSPECIFICPROPERTY_PREFIX + categorySpecificProperty.getUniqueId() + "-min");
                                categorySpecificPropertyIntegerColumns.add(cellNumber);

                                SXSSFCell minEditableCell = headerRow.createCell(cellNumber++);
                                minEditableCell.setCellValue(categorySpecificProperty.getName() + " (min)");

                                headerRow.createCell(cellNumber++).setCellValue(categorySpecificProperty.getName() + " (max)");
                                fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_CATEGORYSPECIFICPROPERTY_PREFIX + categorySpecificProperty.getUniqueId() + "-max");
                                categorySpecificPropertyIntegerColumns.add(cellNumber);

                                SXSSFCell maxEditableCell = headerRow.createCell(cellNumber++);
                                maxEditableCell.setCellValue(categorySpecificProperty.getName() + " (max)");
                                // make editable columns editable
                                sheet.setDefaultColumnStyle(minEditableCell.getColumnIndex(), unlockedCellStyle);
                                sheet.setDefaultColumnStyle(maxEditableCell.getColumnIndex(), unlockedCellStyle);

                            } else {
                                LOG.log(Level.FINEST, "NOT interval");
                                SXSSFCell valueUneditableCell = headerRow.createCell(cellNumber++);
                                valueUneditableCell.setCellValue(categorySpecificProperty.getName());

                                if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_SINGLE) {
                                    setCategorySpecificPropertyValidation(workbook, categorySpecificProperty, sheet, 1, 1000, cellNumber, valueListSheet, ++lastValueListRow);
                                }
                                fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_CATEGORYSPECIFICPROPERTY_PREFIX + categorySpecificProperty.getUniqueId());
                                if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.DECIMAL) {
                                    categorySpecificPropertyIntegerColumns.add(cellNumber);
                                }

                                SXSSFCell valueEditableCell = headerRow.createCell(cellNumber++);
                                valueEditableCell.setCellValue(categorySpecificProperty.getName());

                                // make editable columns editable, multiple lists are not editable at all
                                if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_MULTIPLE) {
                                    sheet.setDefaultColumnStyle(valueUneditableCell.getColumnIndex(), greyStyle);
                                    sheet.setDefaultColumnStyle(valueEditableCell.getColumnIndex(), greyStyle);

                                } else {
                                    if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.TEXTFIELD) {
                                        sheet.setDefaultColumnStyle(valueEditableCell.getColumnIndex(), unlockedTextCellStyle);
                                        LOG.log(Level.FINEST, "PPP unlockedTextCellStyle");
                                    }
                                    else {
                                        sheet.setDefaultColumnStyle(valueEditableCell.getColumnIndex(), unlockedCellStyle);
                                        LOG.log(Level.FINEST, "PPP unlockedCellStyle");
                                    }
                                }
                            }
                        }
                        //LOG.log(Level.INFO, "XXX categorySpecificProperty loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime5)});
                    }

                    if (includeProductsAndArticles || pIncludeP) {
                        List<Long> categoryItems = new ArrayList<>();
                        List<Long> alreadyIncludedProductsAndArticles = new ArrayList<>();
                        List<ProductAPI> productAPIs;
                        if (categoryToProductsMap != null) {
                            productAPIs = categoryToProductsMap.get(category.getUniqueId());
                        } else {
                            productAPIs = productController.findByCategory(organizationUniqueId, category.getUniqueId(), userAPI);
                        }

                        if (exportByProducts)
                            productAPIs = products;

                        List<ArticleAPI> articleAPIs = new ArrayList<>();
                        List<ArticleAPI> articleAPIs2 = new ArrayList<>();


                        if (productAPIs != null && !productAPIs.isEmpty()) {
                            //long startTime6 = System.nanoTime();
                            for (ProductAPI productAPI : productAPIs) {

                                List<Article.Type> articleTypesX = new ArrayList<>();

                                if (pIncludeI)
                                    articleTypesX.add(Article.Type.I);
                                if (pIncludeT)
                                    articleTypesX.add(Article.Type.T);
                                if (pIncludeTj)
                                    articleTypesX.add(Article.Type.Tj);
                                if (pIncludeR)
                                    articleTypesX.add(Article.Type.R);

                                List<Product.Status> statusesX = new ArrayList<>();
                                if (includePublished)
                                    statusesX.add(Product.Status.PUBLISHED);
                                if (includeDiscontinued)
                                    statusesX.add(Product.Status.DISCONTINUED);
                                List<SearchProductsAndArticlesAPI> spaaapis = searchController.searchArticlesByProduct(organizationUniqueId, productAPI.getId(), "", statusesX, articleTypesX, 0, 1000, false);

                                if (spaaapis != null && !spaaapis.isEmpty()) {
                                    //long startTime7 = System.nanoTime();
                                    for (SearchProductsAndArticlesAPI spaaAPI : spaaapis) {
                                        articleAPIs2.add(articleController.getArticleAPIByArticleNumber(organizationUniqueId, spaaAPI.getNumber()));
                                    }
                                    //LOG.log(Level.INFO, "XXX spaaapis loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime7)});
                                }

                                if (productAPI.getStatus().equals("PUBLISHED") && includePublished || productAPI.getStatus().equals("DISCONTINUED") && includeDiscontinued) {
                                    if (!exportByProducts || exportByProducts && sheet.getSheetName().substring(0, 6).matches("[0-9]+")) {



                                        //lägger till produkt
                                        if (!exportByProducts || exportByProducts && category.getCode() == productAPI.getCategory().getCode()) {
                                            SXSSFRow row = sheet.createRow(rowNumber++);
                                                handleProductOrArticle(workbook, row, productAPI, null, categorySpecificPropertys, unlockedCellStyle, greyStyle, unlockedTextCellStyle, unlockedDateCellStyle, category);
                                            alreadyIncludedProductsAndArticles.add(productAPI.getId());
                                        }
                                    }

                                    if (category.getArticleType() == Article.Type.H || category.getArticleType() == Article.Type.T) {
                                        categoryItems.add(productAPI.getId());
                                        if (pIncludeA || includeProductsAndArticles) {
                                            List<ArticleAPI> articlesBasedOnProduct = productController.getArticleAPIsBasedOnProduct(organizationUniqueId, productAPI.getId(), userAPI);
                                            if (articlesBasedOnProduct != null && !articlesBasedOnProduct.isEmpty()) {
                                                for (ArticleAPI articleAPI : articlesBasedOnProduct) {
                                                    if (articleAPI.getStatus().equals("PUBLISHED") && includePublished || articleAPI.getStatus().equals("DISCONTINUED") && includeDiscontinued) {
                                                        if (!alreadyIncludedProductsAndArticles.contains(articleAPI.getId())) {
                                                            if (!exportByProducts || exportByProducts && category.getCode() == articleAPI.getCategory().getCode()) {
                                                                SXSSFRow articleRow = sheet.createRow(rowNumber++);
                                                                //lägger till huvudhjälpmedel
                                                                handleProductOrArticle(workbook, articleRow, productAPI, articleAPI, categorySpecificPropertys, unlockedCellStyle, greyStyle, unlockedTextCellStyle, unlockedDateCellStyle, category);

                                                                alreadyIncludedProductsAndArticles.add(articleAPI.getId());
                                                            }
                                                        }
                                                        categoryItems.add(articleAPI.getId());
                                                    }
                                                }
                                            }
                                        }
                                    } else { // NOT type H
                                        //long startTime8 = System.nanoTime();
                                        for (long articleid : categoryItems) {
                                            Article article = this.articleController.getArticle(organizationUniqueId, articleid, userAPI);

                                            List<Product.Status> statuses = new ArrayList<>();
                                            List<Article.Type> articleTypes = new ArrayList<>();
                                            articleTypes.add(category.getArticleType());
                                            if (includePublished)
                                                statuses.add(Product.Status.PUBLISHED);
                                            if (includeDiscontinued)
                                                statuses.add(Product.Status.DISCONTINUED);
                                            List<SearchProductsAndArticlesAPI> connectedarticles;

                                            if (!exportByProducts) {
                                                connectedarticles = searchController.searchProductsAndArticlesForArticle(organizationUniqueId, article, "", statuses, articleTypes, 0, 1000, false);
                                                //long startTime9 = System.nanoTime();
                                                for (SearchProductsAndArticlesAPI ad : connectedarticles) {
                                                    ArticleAPI articleAPI = this.articleController.getArticleAPI(organizationUniqueId, ad.getId(), userAPI, sessionId, requestIp);
                                                    if (!alreadyIncludedProductsAndArticles.contains(articleAPI.getId())) {
                                                        SXSSFRow articleRow = sheet.createRow(rowNumber++);
                                                        handleProductOrArticle(workbook, articleRow, productAPI, articleAPI, categorySpecificPropertys, unlockedCellStyle, greyStyle, unlockedTextCellStyle, unlockedDateCellStyle, category);
                                                        alreadyIncludedProductsAndArticles.add(articleAPI.getId());
                                                    }
                                                }
                                                //LOG.log(Level.INFO, "XXX connectedarticles loop done: {0} sec", new Object[]{(System.nanoTime() - startTime9)});
                                            }
                                        }
                                        //LOG.log(Level.INFO, "XXX categoryItems loop done: {0} sec", new Object[]{(System.nanoTime() - startTime8)});
                                    }
                                }


                                // if the category is not of type H we will miss articles if we don't search these specifically
                                // this is because articles of type H are always based on a product, but articles
                                // of other types are not
                                //if (productIds.isEmpty()) {
                                //if (pIncludeA || includeProductsAndArticles) {
                                //List<ArticleAPI> articleAPIs = new ArrayList<>();

                            }
                            //LOG.log(Level.INFO, "XXX productAPIs loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime6)});
                        }


                        articleAPIs = articleController.findByCategory(organizationUniqueId, category.getUniqueId(), userAPI);



                        //filtrera!!!
                        if (exportByProducts) {
                            List<ArticleAPI> articleAPIstemp = new ArrayList<>();
                            //long startTime10 = System.nanoTime();
                            for (ArticleAPI articleAPI : articleAPIs) {
                                //long startTime11 = System.nanoTime();
                                for (ArticleAPI articleAPIx : articleAPIs2) {
                                    if (articleAPI.getId() == articleAPIx.getId()) {
                                        articleAPIstemp.add(articleAPI);
                                    }
                                }
                                //LOG.log(Level.INFO, "XXX articleAPIs2 loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime11)});
                            }
                            //LOG.log(Level.INFO, "XXX articleAPIs loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime10)});
                            articleAPIs = articleAPIstemp;
                        }


                        if (articleAPIs != null && !articleAPIs.isEmpty()) {
                            Collections.sort(articleAPIs, new Comparator<ArticleAPI>() {
                                public int compare(ArticleAPI s1, ArticleAPI s2) {
                                    return s1.getArticleNumber().compareToIgnoreCase(s2.getArticleNumber());
                                }
                            });
                            //long startTime12 = System.nanoTime();
                            for (ArticleAPI articleAPI : articleAPIs) {
                                if ((pIncludeP && pIncludeA) || includeProductsAndArticles || (pIncludeP && !pIncludeA && category.getArticleType() != Article.Type.H)) {
                                    if (!alreadyIncludedProductsAndArticles.contains(articleAPI.getId())) {
                                        if (articleAPI.getStatus().equals("PUBLISHED") && includePublished || articleAPI.getStatus().equals("DISCONTINUED") && includeDiscontinued) {
                                            SXSSFRow articleRow = sheet.createRow(rowNumber++);

                                            handleProductOrArticle(workbook, articleRow, null, articleAPI, categorySpecificPropertys, unlockedCellStyle, greyStyle, unlockedTextCellStyle, unlockedDateCellStyle, category);
                                            alreadyIncludedProductsAndArticles.add(articleAPI.getId());


                                        }
                                    }
                                }
                            }
                            //LOG.log(Level.INFO, "XXX articleAPIs loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime12)});
                        }
                    }
                    // size columns
                    //long startTime13 = System.nanoTime();
                    for (int i = 0; i <= headerRow.getLastCellNum(); i++) {
                        sheet.autoSizeColumn(i);
                    }
                    //LOG.log(Level.INFO, "XXX autoSize loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime13)});
                    // hide unique id column
                    sheet.setColumnHidden(0, true);


                    if (category.getArticleType() == Article.Type.R ||
                            (category.getArticleType() == Article.Type.T && category.getCode() == null) ||
                            category.getArticleType() == Article.Type.Tj ||
                            category.getArticleType() == Article.Type.I) {
                        // hide based on..
                        sheet.setColumnHidden(5, false);
                        sheet.setColumnHidden(6, true);
                    }


                    // hide field names row
                    fieldNamesRow.setZeroHeight(true);

                    categoriesSheets.add(sheet);
                    sheet.trackAllColumnsForAutoSizing();

                    setIntegerValidationForCategory(sheet, 2, 50000, categorySpecificPropertyIntegerColumns);
                }
                //LOG.log(Level.INFO, "XXX categories loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime4)});

                //long startTime14 = System.nanoTime();
                for (SXSSFSheet sheet : categoriesSheets) {
                    // Need to fix this better, dont have time now.....
                    String sheetName = "temp";
                    try {
                        sheetName = sheet.getSheetName().substring(0, 6);
                    } catch (Exception ex) {
                        sheetName = "temp";
                    }

                    int offset = 0;
                    if (includeI || includeR || includeT || includeTj || pIncludeI || pIncludeR || pIncludeT || pIncludeTj) {
                        if (sheetName.matches("[0-9]+")) {
                            offset = -2;
                        }
                    }

                    setProductArticleValidation(workbook, sheet, 2, 50000, !sheetName.matches("[0-9]+"));
                    setStandardsValidation(workbook, sheet, 2, 50000, standards, standardsColumnNumber + offset);
                    setDirectivesValidation(workbook, sheet, 2, 50000, directives, directivesColumnNumber + offset);
                    setYesNoDataValidation(workbook, sheet, 2, 50000, customerUniqueColumnNumber + offset, ceMarkedColumnNumber + offset, inactivateRowsOnDiscontinuedColumnNumber + offset);
                    setOrderUnitWithDeleteDataValidation(workbook, sheet, 2, 50000, orderUnits, articleQuantityInOuterPackageUnitColumnNumber + offset);
                    setPackageUnitWithDeleteDataValidation(workbook, sheet, 2, 50000, packageUnits, packageContentUnitColumnNumber + offset);
                    setOrderUnitWithoutDeleteDataValidation(workbook, sheet, 2, 50000, orderUnits, orderUnitColumnNumber + offset);
                    setPreventiveMaintenanceValidFromDataValidation(workbook, sheet, 2, 50000, preventiveMaintenances, preventiveMaintenanceValidFromColumnNumber + offset);

                    setIntegerValidation(sheet, 2, 50000, articleQuantityInOuterPackageColumnNumber + offset, packageContentColumnNumber + offset, packageLevelTopColumnNumber + offset, packageLevelMiddleColumnNumber + offset, packageLevelBaseColumnNumber + offset, preventiveMaintenanceNumberOfDaysColumnNumber + offset);
                    //setDateValidation(sheet, 2, 50000, discontinuedColumnNumber + offset);
                    setDateValidation2(workbook, sheet, 2, 50000, dates, discontinuedColumnNumber + offset);
                    setDocumentTypesValidation(workbook, 2, 50000, documentTypes);
//                sheet.getRow(0).setZeroHeight(true);
                }
                //LOG.log(Level.INFO, "XXX categoriesSheets loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime14)});

            //long startTime15 = System.nanoTime();
            for (SXSSFSheet sheet : listOfSheetsThatWillHaveTheirHeaderRowsLocked) {
                sheet.createFreezePane(0, 2);
            }
            //LOG.log(Level.INFO, "XXX freeze loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime15)});


            // size columns for media sheets
            int imagesSheetLastCellNum = workbook.getSheet(IMAGES_SHEET_NAME).getRow(workbook.getSheet(IMAGES_SHEET_NAME).getLastRowNum()).getLastCellNum();
            //long startTime16 = System.nanoTime();
            for (int i = 0; i <= imagesSheetLastCellNum; i++) {
                imagesSheet.autoSizeColumn(i);
            }
            //LOG.log(Level.INFO, "XXX images loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime16)});

            int documentsSheetLastCellNum = workbook.getSheet(DOCUMENTS_SHEET_NAME).getRow(workbook.getSheet(DOCUMENTS_SHEET_NAME).getLastRowNum()).getLastCellNum();
            //long startTime17 = System.nanoTime();
            for (int i = 0; i <= documentsSheetLastCellNum; i++) {
                documentsSheet.autoSizeColumn(i);
            }
            //LOG.log(Level.INFO, "XXX docs loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime17)});

            int videosSheetLastCellNum = workbook.getSheet(VIDEOS_SHEET_NAME).getRow(workbook.getSheet(VIDEOS_SHEET_NAME).getLastRowNum()).getLastCellNum();
            //long startTime18 = System.nanoTime();
            for (int i = 0; i <= videosSheetLastCellNum; i++) {
                videosSheet.autoSizeColumn(i);
            }
            //LOG.log(Level.INFO, "XXX videos loop done: {0} sec",  new Object[]{(System.nanoTime() - startTime18)});

            // hide unique id column
            imagesSheet.setColumnHidden(0, true);
            documentsSheet.setColumnHidden(0, true);
            videosSheet.setColumnHidden(0, true);

            // hide field names row
            //imagesSheet.getRow(0).setZeroHeight(true);
            //documentsSheet.getRow(0).setZeroHeight(true);
            //videosSheet.getRow(0).setZeroHeight(true);

            workbook.setSheetOrder(IMAGES_SHEET_NAME, workbook.getNumberOfSheets() - 1);
            workbook.setSheetOrder(DOCUMENTS_SHEET_NAME, workbook.getNumberOfSheets() - 1);
            workbook.setSheetOrder(VIDEOS_SHEET_NAME, workbook.getNumberOfSheets() - 1);
            workbook.setSheetOrder(CATEGORIES_SHEET_NAME, workbook.getNumberOfSheets() - 1);
            workbook.setSheetOrder(VALUELIST_SHEET_NAME, workbook.getNumberOfSheets() - 1);
            workbook.setActiveSheet(0);
            workbook.setSelectedTab(0);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            //long startTimeW = System.nanoTime();
            workbook.write(baos);
            //LOG.log(Level.INFO, "XXX write baos done: {0} sec",  new Object[]{(System.nanoTime() - startTimeW)});

            //LOG.log(Level.INFO, "XXX export done, total time: {0} sec",  new Object[]{(System.nanoTime() - startTime)});
            return baos.toByteArray();
        } catch (IOException ex) {
            LOG.log(Level.WARNING, "Failed to export organization product/article search to file", ex);
            throw validationMessageService.generateValidationException("export", "export.productarticle.error");
        }
    }

    private String addConnectionHeading(Category category) {

        String connectionsHeading;

        switch (category.getArticleType()) {
            case T:
                if (category.getCode() == null) {
                    connectionsHeading = "Tillbehör till";
                    break;
                }
                else{
                    connectionsHeading = null;
                    break;
                }
            case I:
                connectionsHeading = "Inställning till";
                break;
            case R:
                connectionsHeading = "Reservdel till";
                break;
            case Tj:
                connectionsHeading = "Tjänst för";
                break;
            default:
                connectionsHeading = null;
        }

        return connectionsHeading;
    }


    private int cellUnlockedCellStyle(SXSSFRow fieldNamesRow, SXSSFRow headerRow, SXSSFSheet sheet, int columnPosition, String fieldType, String headerRowField) {

        SXSSFCell cell = fieldNamesRow.createCell(columnPosition);
        cell.setCellValue(fieldType);

        sheet.setDefaultColumnStyle(columnPosition, unlockedCellStyle);
        headerRow.createCell(columnPosition++).setCellValue(headerRowField);
        return columnPosition;
    }

    private int cellWithAdditionalFieldUnlockedCellStyle(SXSSFRow fieldNamesRow, SXSSFRow headerRow, SXSSFSheet sheet, int columnPosition, String fieldType, String headerRowField) {

        headerRow.createCell(columnPosition++).setCellValue(headerRowField);
        fieldNamesRow.createCell(columnPosition).setCellValue(fieldType);
        sheet.setDefaultColumnStyle(columnPosition, unlockedCellStyle);
        headerRow.createCell(columnPosition++).setCellValue(headerRowField);
        return columnPosition;
    }

    private int cellWithAdditionalFieldUnlockedTextCellStyle(SXSSFRow fieldNamesRow, SXSSFRow headerRow, SXSSFSheet sheet, int columnPosition, String fieldType, String headerRowField) {

        headerRow.createCell(columnPosition++).setCellValue(headerRowField);
        fieldNamesRow.createCell(columnPosition).setCellValue(fieldType);
        sheet.setDefaultColumnStyle(columnPosition, unlockedTextCellStyle);
        headerRow.createCell(columnPosition++).setCellValue(headerRowField);
        return columnPosition;
    }

    private int cellWithAdditionalFieldUnlockedDateCellStyle(SXSSFRow fieldNamesRow, SXSSFRow headerRow, SXSSFSheet sheet, int columnPosition, String fieldType, String headerRowField) {

        headerRow.createCell(columnPosition++).setCellValue(headerRowField);
        fieldNamesRow.createCell(columnPosition).setCellValue(fieldType);
        sheet.setDefaultColumnStyle(columnPosition, unlockedDateCellStyle);
        headerRow.createCell(columnPosition++).setCellValue(headerRowField);
        return columnPosition;
    }

    private int cellUnlockedTextCellStyle(SXSSFRow fieldNamesRow, SXSSFRow headerRow, SXSSFSheet sheet, int columnPosition, String fieldType, String headerRowField) {

        fieldNamesRow.createCell(columnPosition).setCellValue(fieldType);
        sheet.setDefaultColumnStyle(columnPosition, unlockedTextCellStyle);
        headerRow.createCell(columnPosition++).setCellValue(headerRowField);
        return columnPosition;
    }

    private void handleMediaForProduct(SXSSFWorkbook workbook, ProductAPI productAPI) {
        LOG.log(Level.FINEST, "handleMediaForProduct( productAPI -> id: {0} )", new Object[]{productAPI.getId()});
        List<MediaImage> mediaImages = mediaController.getProductMediaImages(productAPI.getId());
        if (mediaImages != null && !mediaImages.isEmpty()) {
            SXSSFSheet imagesSheet = workbook.getSheet(IMAGES_SHEET_NAME);
            int startOnRow = imagesSheet.getLastRowNum() + 1;
            for (MediaImage mediaImage : mediaImages) {
                if (!mediaImage.isMainImage()) {
                    SXSSFRow row = imagesSheet.createRow(startOnRow++);
                    row.createCell(0).setCellValue(mediaImage.getUniqueId());
                    row.createCell(1).setCellValue(productAPI.getProductNumber());
                    row.createCell(2).setCellValue(mediaImage.getUrl());
                    row.createCell(4).setCellValue(mediaImage.getDescription());
                    row.createCell(6).setCellValue(mediaImage.getAlternativeText());

                    for (int i = 0; i < row.getLastCellNum(); i++) {
                        imagesSheet.autoSizeColumn(i);
                    }

                }


            }
        }

        List<MediaDocument> mediaDocuments = mediaController.getProductMediaDocuments(productAPI.getId());
        if (mediaDocuments != null && !mediaDocuments.isEmpty()) {
            SXSSFSheet documentsSheet = workbook.getSheet(DOCUMENTS_SHEET_NAME);
            int startOnRow = documentsSheet.getLastRowNum() + 1;
            for (MediaDocument mediaDocument : mediaDocuments) {
                SXSSFRow row = documentsSheet.createRow(startOnRow++);
                row.createCell(0).setCellValue(mediaDocument.getUniqueId());
                row.createCell(1).setCellValue(productAPI.getProductNumber());
                row.createCell(2).setCellValue(mediaDocument.getUrl());
                row.createCell(4).setCellValue(mediaDocument.getDescription());
                if (mediaDocument.getDocumentType() != null) {
                    row.createCell(6).setCellValue(mediaDocument.getDocumentType().getValue());
                }

                for (int i = 0; i < row.getLastCellNum(); i++) {
                    documentsSheet.autoSizeColumn(i);
                }
            }
        }

        List<MediaVideo> mediaVideos = mediaController.getProductMediaVideos(productAPI.getId());
        if (mediaVideos != null && !mediaVideos.isEmpty()) {
            SXSSFSheet videosSheet = workbook.getSheet(VIDEOS_SHEET_NAME);
            int startOnRow = videosSheet.getLastRowNum() + 1;
            for (MediaVideo mediaVideo : mediaVideos) {
                SXSSFRow row = videosSheet.createRow(startOnRow++);
                row.createCell(0).setCellValue(mediaVideo.getUniqueId());
                row.createCell(1).setCellValue(productAPI.getProductNumber());
                row.createCell(2).setCellValue(mediaVideo.getUrl());
                row.createCell(4).setCellValue(mediaVideo.getDescription());
                row.createCell(6).setCellValue(mediaVideo.getAlternativeText());

                for (int i = 0; i < row.getLastCellNum(); i++) {
                    videosSheet.autoSizeColumn(i);
                }

            }
        }

    }

    private void handleMediaForArticle(SXSSFWorkbook workbook, ArticleAPI articleAPI) {
        LOG.log(Level.FINEST, "handleMediaForArticle( articleAPI -> id: {0} )", new Object[]{articleAPI.getId()});
        List<ArticleMediaImage> articleMediaImages = mediaController.getArticleMediaImages(articleAPI.getId());
        if (articleMediaImages != null && !articleMediaImages.isEmpty()) {
            SXSSFSheet imagesSheet = workbook.getSheet(IMAGES_SHEET_NAME);
            int startOnRow = imagesSheet.getLastRowNum() + 1;
            for (ArticleMediaImage articleMediaImage : articleMediaImages) {
                if (!articleMediaImage.getMediaImage().isMainImage()) {
                    SXSSFRow row = imagesSheet.createRow(startOnRow++);
                    row.createCell(0).setCellValue(articleMediaImage.getUniqueId());
                    row.createCell(1).setCellValue(articleAPI.getArticleNumber());
                    row.createCell(2).setCellValue(articleMediaImage.getMediaImage().getUrl());
                    row.createCell(4).setCellValue(articleMediaImage.getMediaImage().getDescription());
                    row.createCell(6).setCellValue(articleMediaImage.getMediaImage().getAlternativeText());
                }
            }
        }

        List<ArticleMediaDocument> articleMediaDocuments = mediaController.getArticleMediaDocuments(articleAPI.getId());
        if (articleMediaDocuments != null && !articleMediaDocuments.isEmpty()) {
            SXSSFSheet documentsSheet = workbook.getSheet(DOCUMENTS_SHEET_NAME);
            int startOnRow = documentsSheet.getLastRowNum() + 1;
            for (ArticleMediaDocument articleMediaDocument : articleMediaDocuments) {
                SXSSFRow row = documentsSheet.createRow(startOnRow++);
                row.createCell(0).setCellValue(articleMediaDocument.getUniqueId());
                row.createCell(1).setCellValue(articleAPI.getArticleNumber());
                row.createCell(2).setCellValue(articleMediaDocument.getMediaDocument().getUrl());
                row.createCell(4).setCellValue(articleMediaDocument.getMediaDocument().getDescription());
                if (articleMediaDocument.getMediaDocument().getDocumentType() != null) {
                    row.createCell(6).setCellValue(articleMediaDocument.getMediaDocument().getDocumentType().getValue());
                }
            }
        }

        List<ArticleMediaVideo> articleMediaVideos = mediaController.getArticleMediaVideos(articleAPI.getId());
        if (articleMediaVideos != null && !articleMediaVideos.isEmpty()) {
            SXSSFSheet videosSheet = workbook.getSheet(VIDEOS_SHEET_NAME);
            int startOnRow = videosSheet.getLastRowNum() + 1;
            for (ArticleMediaVideo articleMediaVideo : articleMediaVideos) {
                SXSSFRow row = videosSheet.createRow(startOnRow++);
                row.createCell(0).setCellValue(articleMediaVideo.getUniqueId());
                row.createCell(1).setCellValue(articleAPI.getArticleNumber());
                row.createCell(2).setCellValue(articleMediaVideo.getMediaVideo().getUrl());
                row.createCell(4).setCellValue(articleMediaVideo.getMediaVideo().getDescription());
                row.createCell(6).setCellValue(articleMediaVideo.getMediaVideo().getAlternativeText());
            }
        }

    }

    private void handleProductOrArticle(SXSSFWorkbook workbook,
                                        SXSSFRow row,
                                        ProductAPI product,
                                        ArticleAPI article,
                                        List<CategorySpecificProperty> categorySpecificPropertys,
                                        CellStyle unlockedCellStyle,
                                        CellStyle greyStyle,
                                        CellStyle unlockedTextCellStyle,
                                        CellStyle unlockedDateCellStyle,
                                        Category category) {
        LOG.log(Level.FINEST, "handleProductOrArticle(product: {0}, article: {1} )", new Object[]{product == null ? null : product.getId(), article == null ? null : article.getId()});
        CellStyle colorStyle = article == null ? orangeStyle : goldStyle;

        int cellNumber = 0;

        // unique id
        row.createCell(cellNumber++).setCellValue(article == null ? product.getId() : article.getId()); // all products and articles in this method are being updated

        // type
        String type = article == null ? PRODUCT_TYPE_NAME : ARTICLE_TYPE_NAME;
        SXSSFCell numberCell = row.createCell(cellNumber++);
        numberCell.setCellStyle(colorStyle);
        numberCell.setCellValue(type);

        // current number
        String number = article == null ? product.getProductNumber() : article.getArticleNumber();
        SXSSFCell numberCellEdit = row.createCell(cellNumber++);
        numberCellEdit.setCellValue(number);

        //  current name
        String name = article == null ? product.getProductName() : article.getArticleName();
        SXSSFCell nameCell = row.createCell(cellNumber++);
        nameCell.setCellValue(name);

        // new name
        SXSSFCell nameEditableCell = row.createCell(cellNumber++);
        //nameEditableCell.setCellStyle(unlockedCellStyle);
        nameEditableCell.setCellStyle(unlockedTextCellStyle);

        //  based on
        String basedOnProduct = "";
        if (article != null && article.getBasedOnProduct() != null) {
            basedOnProduct = article.getBasedOnProduct().getProductNumber();
        }
        SXSSFCell basedOnProductCell = row.createCell(cellNumber++);
        basedOnProductCell.setCellValue(basedOnProduct);

        // new based on
        // HJAL-2022, 2081
        SXSSFCell basedOnProductEditableCell = row.createCell(cellNumber++);
        //basedOnProductEditableCell.setCellStyle(unlockedCellStyle);
        basedOnProductEditableCell.setCellStyle(unlockedTextCellStyle);

        // connections on articles Mattias
        //if (category.getArticleType() != Article.Type.H && article != null) {
        if (article != null && (category.getArticleType() == Article.Type.Tj || category.getArticleType() == Article.Type.R || category.getArticleType() == Article.Type.I || (category.getArticleType() == Article.Type.T && category.getCode() == null))) {
            // current connections
            SXSSFCell connectionsCell = row.createCell(cellNumber++);
            connectionsCell.setCellValue(getConnectionsAsStringArticle(article));

            // new connections
            SXSSFCell connectionsEditableCell = row.createCell(cellNumber++);
            connectionsEditableCell.setCellStyle(unlockedTextCellStyle);
        }

        // current category
        String categoryString;
        if (article != null) {
            if (article.getCategory() != null) {
                categoryString = article.getCategory().getCode();
            } else {
                categoryString = article.getBasedOnProduct().getCategory().getCode();
            }
        } else {
            categoryString = product.getCategory().getCode();
        }

        // If article type T (not Tiso), I, R, Tj

        if (article != null && categoryString == null || categoryString.isEmpty()) {
            //case 1
            if( article.getBasedOnProduct() == null ) {
                categoryString = String.valueOf(article.getCategory().getCode());
            } else {
                categoryString = String.valueOf(article.getBasedOnProduct().getCategory().getCode());
            }

            //case 2
            if (category.getCode() == null){
                if (article.getFitsToProducts() != null && article.getFitsToProducts().size() > 0) {
                    categoryString = article.getFitsToProducts().get(0).getCategory().getCode();
                }
            }

            //case 3
            if (category.getCode() == null){
                if (article.getFitsToArticles() != null && article.getFitsToArticles().size() > 0) {
                    if (article.getFitsToArticles().get(0).getBasedOnProduct() != null) {
                        categoryString = String.valueOf(article.getFitsToArticles().get(0).getBasedOnProduct().getCategory().getCode());
                    }
                    // kan tänkas att detta ska med? PA 2020-10-05
                    else{
                        categoryString = article.getFitsToArticles().get(0).getCategory().getCode();
                    }
                }
            }
            if (category.getCode() == null){
                if (article.getFitsToArticles() != null && article.getFitsToArticles().size() > 0) {
                    if (article.getFitsToArticles().get(0).getFitsToProducts() != null && article.getFitsToArticles().get(0).getFitsToProducts().size() > 0) {
                        categoryString = article.getFitsToArticles().get(0).getFitsToProducts().get(0).getCategory().getCode();
                    }
                }
            }

            categoryString = "(" + categoryString + ")";

//            searchProductsAndArticlesAPI.setCode("(" + categoryString + ")");

//                        if( category.getCode() != null && category.getCode().length() > 6 ) {
//                            // we do not want to include categories longer than 6 digits
//                            // let's find the parent category with six digits
//                            category = category.getParent();
//                        }
        }



        SXSSFCell categoryCell = row.createCell(cellNumber++);
        categoryCell.setCellValue(categoryString);

        // extended categories
        String extendedCategoriesString;
        if (article != null) {
            extendedCategoriesString = getExtendedCategoriesAsString(article.getExtendedCategories());
        } else {
            extendedCategoriesString = getExtendedCategoriesAsString(product.getExtendedCategories());
        }
        SXSSFCell extendedCategoriesCell = row.createCell(cellNumber++);
        extendedCategoriesCell.setCellValue(extendedCategoriesString);

        // new extended categories
        SXSSFCell extendedCategoriesEditableCell = row.createCell(cellNumber++);
        extendedCategoriesEditableCell.setCellStyle(unlockedTextCellStyle);

        // current gtin (only articles
        String gtin = "";
        if (article != null) {
            gtin = article.getGtin();
        }
        SXSSFCell gtinCell = row.createCell(cellNumber++);
        gtinCell.setCellValue(gtin);

        // new gtin
        SXSSFCell gtinEditableCell = row.createCell(cellNumber++);
        gtinEditableCell.setCellStyle(unlockedTextCellStyle);

        // current customer unique
        String customerUnique = "";
        if (article != null) {
            customerUnique = article.isCustomerUnique() ? YES : NO;
        } else {
            customerUnique = product.isCustomerUnique() ? YES : NO;
        }
        SXSSFCell customerUniqueCell = row.createCell(cellNumber++);
        customerUniqueCell.setCellValue(customerUnique);

        // new customer unique
        SXSSFCell customerUniqueEditableCell = row.createCell(cellNumber++);
        customerUniqueEditableCell.setCellStyle(unlockedCellStyle);

        // current order unit
        String orderUnit = "";
        if (article != null) {
            orderUnit = article.getOrderUnit() != null ? article.getOrderUnit().getName() : "";
        } else {
            orderUnit = product.getOrderUnit().getName();
        }
        SXSSFCell orderUnitCell = row.createCell(cellNumber++);
        orderUnitCell.setCellValue(orderUnit);

        // new order unit
        SXSSFCell orderUnitEditableCell = row.createCell(cellNumber++);
        orderUnitEditableCell.setCellStyle(unlockedCellStyle);

        // current article quantity in outer package
        String articleQuantityInOuterPackage = "";
        if (article != null) {
            articleQuantityInOuterPackage = article.getArticleQuantityInOuterPackage() == null ? "" : exportHelper.parseNumberValueToStringWithCorrectDecimalFormatting(article.getArticleQuantityInOuterPackage());
        } else {
            articleQuantityInOuterPackage = product.getArticleQuantityInOuterPackage() == null ? "" : exportHelper.parseNumberValueToStringWithCorrectDecimalFormatting(product.getArticleQuantityInOuterPackage());
        }
        SXSSFCell articleQuantityInOuterPackageCell = row.createCell(cellNumber++);
        articleQuantityInOuterPackageCell.setCellValue(articleQuantityInOuterPackage);

        // new article quantity in outer package
        SXSSFCell articleQuantityInOuterPackageEditableCell = row.createCell(cellNumber++);
        articleQuantityInOuterPackageEditableCell.setCellStyle(unlockedCellStyle);

        // current article quantity in outer package unit
        String articleQuantityInOuterPackageUnit = "";
        if (article != null) {
            articleQuantityInOuterPackageUnit = article.getArticleQuantityInOuterPackageUnit() == null ? "" : article.getArticleQuantityInOuterPackageUnit().getName();
        } else {
            articleQuantityInOuterPackageUnit = product.getArticleQuantityInOuterPackageUnit() == null ? "" : product.getArticleQuantityInOuterPackageUnit().getName();
        }
        SXSSFCell articleQuantityInOuterPackageUnitCell = row.createCell(cellNumber++);
        articleQuantityInOuterPackageUnitCell.setCellValue(articleQuantityInOuterPackageUnit);

        // new article quantity in outer package unit
        SXSSFCell articleQuantityInOuterPackageUnitEditableCell = row.createCell(cellNumber++);
        articleQuantityInOuterPackageUnitEditableCell.setCellStyle(unlockedCellStyle);

        // current package content
        String packageContent = "";
        if (article != null) {
            packageContent = article.getPackageContent() == null ? "" : exportHelper.parseNumberValueToStringWithCorrectDecimalFormatting(article.getPackageContent());
        } else {
            packageContent = product.getPackageContent() == null ? "" : exportHelper.parseNumberValueToStringWithCorrectDecimalFormatting(product.getPackageContent());
        }
        SXSSFCell packageContentCell = row.createCell(cellNumber++);
        packageContentCell.setCellValue(packageContent);

        // new package content
        SXSSFCell packageContentEditableCell = row.createCell(cellNumber++);
        packageContentEditableCell.setCellStyle(unlockedCellStyle);

        // current package content unit
        String packageContentUnit = "";
        if (article != null) {
            packageContentUnit = article.getPackageContentUnit() == null ? "" : article.getPackageContentUnit().getName();
        } else {
            packageContentUnit = product.getPackageContentUnit() == null ? "" : product.getPackageContentUnit().getName();
        }
        SXSSFCell packageContentUnitCell = row.createCell(cellNumber++);
        packageContentUnitCell.setCellValue(packageContentUnit);

        // new package content unit
        SXSSFCell packageContentUnitEditableCell = row.createCell(cellNumber++);
        packageContentUnitEditableCell.setCellStyle(unlockedCellStyle);

        // current package level base
        String packageLevelBase = "";
        if (article != null) {
            packageLevelBase = article.getPackageLevelBase() == null ? "" : article.getPackageLevelBase().toString();
        } else {
            packageLevelBase = product.getPackageLevelBase() == null ? "" : product.getPackageLevelBase().toString();
        }
        SXSSFCell packageLevelBaseCell = row.createCell(cellNumber++);
        packageLevelBaseCell.setCellValue(packageLevelBase);

        // new package level base
        SXSSFCell packageLevelBaseEditableCell = row.createCell(cellNumber++);
        packageLevelBaseEditableCell.setCellStyle(unlockedCellStyle);

        // current package level middle
        String packageLevelMiddle = "";
        if (article != null) {
            packageLevelMiddle = article.getPackageLevelMiddle() == null ? "" : article.getPackageLevelMiddle().toString();
        } else {
            packageLevelMiddle = product.getPackageLevelMiddle() == null ? "" : product.getPackageLevelMiddle().toString();
        }
        SXSSFCell packageLevelMiddleCell = row.createCell(cellNumber++);
        packageLevelMiddleCell.setCellValue(packageLevelMiddle);

        // new package level middle
        SXSSFCell packageLevelMiddleEditableCell = row.createCell(cellNumber++);
        packageLevelMiddleEditableCell.setCellStyle(unlockedCellStyle);

        // current package level top
        String packageLevelTop = "";
        if (article != null) {
            packageLevelTop = article.getPackageLevelTop() == null ? "" : article.getPackageLevelTop().toString();
        } else {
            packageLevelTop = product.getPackageLevelTop() == null ? "" : product.getPackageLevelTop().toString();
        }
        SXSSFCell packageLevelTopCell = row.createCell(cellNumber++);
        packageLevelTopCell.setCellValue(packageLevelTop);

        // new package level top
        SXSSFCell packageLevelTopEditableCell = row.createCell(cellNumber++);
        packageLevelTopEditableCell.setCellStyle(unlockedCellStyle);

        // current ce marked
        String ceMarked = "";
        if (article != null) {
            ceMarked = article.isCeMarked() ? YES : NO;
        } else {
            ceMarked = product.isCeMarked() ? YES : NO;
        }
        SXSSFCell ceMarkedCell = row.createCell(cellNumber++);
        ceMarkedCell.setCellValue(ceMarked);

        // new ce marked
        SXSSFCell ceMarkedEditableCell = row.createCell(cellNumber++);
        ceMarkedEditableCell.setCellStyle(unlockedCellStyle);

        // current directive
        String directive = "";
        if (article != null) {
            directive = article.getCeDirective() == null ? "" : article.getCeDirective().getName();
        } else {
            directive = product.getCeDirective() == null ? "" : product.getCeDirective().getName();
        }
        SXSSFCell directiveCell = row.createCell(cellNumber++);
        directiveCell.setCellValue(directive);

        // new directive
        SXSSFCell directiveEditableCell = row.createCell(cellNumber++);
        directiveEditableCell.setCellStyle(unlockedCellStyle);

        // current standard
        String standard = "";
        if (article != null) {
            standard = article.getCeStandard() == null ? "" : article.getCeStandard().getName();
        } else {
            standard = product.getCeStandard() == null ? "" : product.getCeStandard().getName();
        }
        SXSSFCell standardCell = row.createCell(cellNumber++);
        standardCell.setCellValue(standard);

        // new standard
        SXSSFCell standardEditableCell = row.createCell(cellNumber++);
        standardEditableCell.setCellStyle(unlockedCellStyle);

        // preventive maintenance valid from
        String preventiveMaintenanceValidFrom = "";
        if (article != null) {
            preventiveMaintenanceValidFrom = article.getPreventiveMaintenanceValidFrom() == null ? "" : article.getPreventiveMaintenanceValidFrom().getName();
        } else {
            preventiveMaintenanceValidFrom = product.getPreventiveMaintenanceValidFrom() == null ? "" : product.getPreventiveMaintenanceValidFrom().getName();
        }
        SXSSFCell preventiveMaintenanceValidFromCell = row.createCell(cellNumber++);
        preventiveMaintenanceValidFromCell.setCellValue(preventiveMaintenanceValidFrom);

        // new preventive maintenance
        SXSSFCell preventiveMaintenanceValidFromEditableCell = row.createCell(cellNumber++);
        preventiveMaintenanceValidFromEditableCell.setCellStyle(unlockedCellStyle);

        // current preventive maintenance number of days
        String preventiveMaintenanceNumberOfDays = "";
        if (article != null) {
            if (article.getPreventiveMaintenanceNumberOfDays() != null) {
                preventiveMaintenanceNumberOfDays = article.getPreventiveMaintenanceNumberOfDays().toString();
            }
        } else {
            if (product.getPreventiveMaintenanceNumberOfDays() != null) {
                preventiveMaintenanceNumberOfDays = product.getPreventiveMaintenanceNumberOfDays().toString();
            }
        }
        SXSSFCell preventiveMaintenanceNumberOfDaysCell = row.createCell(cellNumber++);
        preventiveMaintenanceNumberOfDaysCell.setCellValue(preventiveMaintenanceNumberOfDays);

        // new current preventive maintenance number of days
        SXSSFCell preventiveMaintenanceNumberOfDaysEditableCell = row.createCell(cellNumber++);
        preventiveMaintenanceNumberOfDaysEditableCell.setCellStyle(unlockedCellStyle);

        // current manufacturer
        String manufacturer = "";
        if (article != null) {
            manufacturer = article.getManufacturer();
        } else {
            manufacturer = product.getManufacturer();
        }
        SXSSFCell manufacturerCell = row.createCell(cellNumber++);
        manufacturerCell.setCellValue(manufacturer);

        // new manufacturer
        SXSSFCell manufacturerEditableCell = row.createCell(cellNumber++);
        manufacturerEditableCell.setCellStyle(unlockedTextCellStyle);

        // current manufacturer product number
        String manufacturerNumber = "";
        if (article != null) {
            manufacturerNumber = article.getManufacturerArticleNumber();
        } else {
            manufacturerNumber = product.getManufacturerProductNumber();
        }
        SXSSFCell manufacturerNumberCell = row.createCell(cellNumber++);
        manufacturerNumberCell.setCellValue(manufacturerNumber);

        // new manufacturer product number
        SXSSFCell manufacturerNumberEditableCell = row.createCell(cellNumber++);
        manufacturerNumberEditableCell.setCellStyle(unlockedTextCellStyle);

        // current manufacturer web
        String manufacturerWeb = "";
        if (article != null) {
            if (article.getManufacturerElectronicAddress() != null) {
                manufacturerWeb = article.getManufacturerElectronicAddress().getWeb();
            }
        } else {
            if (product.getManufacturerElectronicAddress() != null) {
                manufacturerWeb = product.getManufacturerElectronicAddress().getWeb();
            }
        }
        SXSSFCell manufacturerWebCell = row.createCell(cellNumber++);
        manufacturerWebCell.setCellValue(manufacturerWeb);

        // new manufcaturer web
        SXSSFCell manufacturerWebEditableCell = row.createCell(cellNumber++);
        manufacturerWebEditableCell.setCellStyle(unlockedTextCellStyle);

        // current trademark
        String trademark = "";
        if (article != null) {
            trademark = article.getTrademark();
        } else {
            trademark = product.getTrademark();
        }
        SXSSFCell trademarkCell = row.createCell(cellNumber++);
        trademarkCell.setCellValue(trademark);

        // new trademark
        SXSSFCell trademarkEditableCell = row.createCell(cellNumber++);
        trademarkEditableCell.setCellStyle(unlockedTextCellStyle);

        // current color
        String color;
        if (article != null) {
            color = article.getColor();
        } else {
            color = product.getColor();
        }
        SXSSFCell colorCell = row.createCell(cellNumber++);
        colorCell.setCellValue(color);

        // new color
        SXSSFCell colorEditableCell = row.createCell(cellNumber++);
        colorEditableCell.setCellStyle(unlockedTextCellStyle);

        // current supplemented information
        String supplementedInformation;
        if (article != null) {
            supplementedInformation = article.getSupplementedInformation();
        } else {
            supplementedInformation = product.getSupplementedInformation();
        }
        SXSSFCell supplementedInformationCell = row.createCell(cellNumber++);
        supplementedInformationCell.setCellValue(supplementedInformation);

        // new supplemented information
        SXSSFCell supplementedInformationEditableCell = row.createCell(cellNumber++);
        supplementedInformationEditableCell.setCellStyle(unlockedTextCellStyle);

        // current description for 1177
        String descriptionForElvasjuttiosju;
        if (article != null) {
            descriptionForElvasjuttiosju = null;
        } else {
            descriptionForElvasjuttiosju = product.getDescriptionForElvasjuttiosju();
        }
        SXSSFCell descriptionForElvasjuttiosjuCell = row.createCell(cellNumber++);
        descriptionForElvasjuttiosjuCell.setCellValue(descriptionForElvasjuttiosju);

        // new description for 1177
        SXSSFCell descriptionForElvasjuttiosjuEditableCell = row.createCell(cellNumber++);
        if (article == null) {
            descriptionForElvasjuttiosjuEditableCell.setCellStyle(unlockedTextCellStyle);
        }

        // current main image
        String mainImage = "";
        String mainImageDescription = "";
        String mainImageAltText = "";
        if (article != null) {
            ArticleMediaImage articleMediaImage = mediaController.getArticleMainImage(article.getId());
            if (articleMediaImage != null) {
                mainImage = articleMediaImage.getMediaImage().getUrl();
                mainImageDescription = articleMediaImage.getMediaImage().getDescription();
                mainImageAltText = articleMediaImage.getMediaImage().getAlternativeText();
            }
        } else {
            MediaImage productMediaImage = mediaController.getProductMainImage(product.getId());
            if (productMediaImage != null) {
                mainImage = productMediaImage.getUrl();
                mainImageDescription = productMediaImage.getDescription();
                mainImageAltText = productMediaImage.getAlternativeText();
            }
        }
        SXSSFCell mainImageCell = row.createCell(cellNumber++);
        mainImageCell.setCellValue(mainImage);

        // new main image
        SXSSFCell mainImageEditableCell = row.createCell(cellNumber++);
        mainImageEditableCell.setCellStyle(unlockedTextCellStyle);

        // current main image description
        SXSSFCell mainImageDescriptionCell = row.createCell(cellNumber++);
        mainImageDescriptionCell.setCellValue(mainImageDescription);

        // new main image description
        SXSSFCell mainImageDescriptionEditableCell = row.createCell(cellNumber++);
        mainImageDescriptionEditableCell.setCellStyle(unlockedTextCellStyle);

        // current main image alt text
        SXSSFCell mainImageAltTextCell = row.createCell(cellNumber++);
        mainImageAltTextCell.setCellValue(mainImageAltText);

        // new main image alt text
        SXSSFCell mainImageAltTextEditableCell = row.createCell(cellNumber++);
        mainImageAltTextEditableCell.setCellStyle(unlockedTextCellStyle);

        // current discontinued date
        String discontinued = "";
        if (article != null) {
            if (article.getReplacementDate() != null) {
                ZonedDateTime replacementDateDateTime = Instant.ofEpochMilli(article.getReplacementDate()).atZone(ZoneId.systemDefault());
                discontinued = dateFormatter.format(replacementDateDateTime);
            }
        } else {
            if (product.getReplacementDate() != null) {
                ZonedDateTime replacementDateDateTime = Instant.ofEpochMilli(product.getReplacementDate()).atZone(ZoneId.systemDefault());
                discontinued = dateFormatter.format(replacementDateDateTime);
            }
        }
        SXSSFCell discontinuedCell = row.createCell(cellNumber++);
        discontinuedCell.setCellValue(discontinued);

        // new discontinued
        SXSSFCell discontinuedEditableCell = row.createCell(cellNumber++);
        //HJAL-2115
        //discontinuedEditableCell.setCellStyle(unlockedDateCellStyle);
        discontinuedEditableCell.setCellStyle(unlockedTextCellStyle);

        // current inactivate rows on discontinued
        String inactivateRowsOnDiscontinued = "";
        if (article != null) {
            if (article.getInactivateRowsOnReplacement() != null) {
                inactivateRowsOnDiscontinued = article.getInactivateRowsOnReplacement() ? YES : NO;
            }
        } else {
            if (product.getInactivateRowsOnReplacement() != null) {
                inactivateRowsOnDiscontinued = product.getInactivateRowsOnReplacement() ? YES : NO;
            }
        }
        SXSSFCell inactivateRowsOnDiscontinuedCell = row.createCell(cellNumber++);
        inactivateRowsOnDiscontinuedCell.setCellValue(inactivateRowsOnDiscontinued);

        // new inactivate rows on discontinued
        SXSSFCell inactivateRowsOnDiscontinuedEditableCell = row.createCell(cellNumber++);
        inactivateRowsOnDiscontinuedEditableCell.setCellStyle(unlockedCellStyle);

        // current replaced by
        String replacedBy = "";
        if (article != null) {
            if (article.getReplacedByArticles() != null) {
                StringBuilder replacedByBuilder = new StringBuilder();
                replacedByBuilder.append(article.
                        getReplacedByArticles().
                        stream().
                        map(a -> a.getArticleNumber()).
                        collect(Collectors.joining(CONNECTIONS_SEPARATOR)));
                replacedBy = replacedByBuilder.toString();
            }
        } else {
            if (product.getReplacedByProducts() != null) {
                StringBuilder replacedByBuilder = new StringBuilder();
                replacedByBuilder.append(product.
                        getReplacedByProducts().
                        stream().
                        map(p -> p.getProductNumber()).
                        collect(Collectors.joining(CONNECTIONS_SEPARATOR)));
                replacedBy = replacedByBuilder.toString();
            }
        }
        SXSSFCell replacesCell = row.createCell(cellNumber++);
        replacesCell.setCellValue(replacedBy);

        // new replaces
        SXSSFCell replacesEditableCell = row.createCell(cellNumber++);
        replacesEditableCell.setCellStyle(unlockedTextCellStyle);

        // created
        ZonedDateTime createdZonedDateTime;
        if (article != null) {
            createdZonedDateTime = Instant.ofEpochMilli(article.getCreated()).atZone(ZoneId.systemDefault());
        } else {
            createdZonedDateTime = Instant.ofEpochMilli(product.getCreated()).atZone(ZoneId.systemDefault());
        }
        String created = dateTimeFormatter.format(createdZonedDateTime);
        SXSSFCell createdCell = row.createCell(cellNumber++);
        createdCell.setCellValue(created);

        // updated
        String updated = "";
        if (article != null) {
            if (article.getUpdated() != null) {
                ZonedDateTime updatedZonedDateTime = Instant.ofEpochMilli(article.getUpdated()).atZone(ZoneId.systemDefault());
                updated = dateTimeFormatter.format(updatedZonedDateTime);
            }
        } else {
            if (product.getUpdated() != null) {
                ZonedDateTime updatedZonedDateTime = Instant.ofEpochMilli(product.getUpdated()).atZone(ZoneId.systemDefault());
                updated = dateTimeFormatter.format(updatedZonedDateTime);
            }
        }
        SXSSFCell updatedCell = row.createCell(cellNumber++);
        updatedCell.setCellValue(updated);


        if (categorySpecificPropertys != null && !categorySpecificPropertys.isEmpty()) {
            List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = article != null ? article.getCategoryPropertys() : product.getCategoryPropertys();
            for (CategorySpecificProperty categorySpecificProperty : categorySpecificPropertys) {
                ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = findByCategorySpecificProperty(categorySpecificProperty, resourceSpecificPropertyAPIs);
                if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.INTERVAL) {
                    // interval must be handled separately since it includes one "object"
                    // but are displayed as two separate values in Excel

                    // first min value
                    SXSSFCell oldValueCellMin = row.createCell(cellNumber++);
                    if (resourceSpecificPropertyAPI != null) {
                        if (resourceSpecificPropertyAPI.getIntervalFromValue() != null) {
                            oldValueCellMin.setCellValue(resourceSpecificPropertyAPI.getIntervalFromValue());
                        }
                    }
                    SXSSFCell newValueCellMin = row.createCell(cellNumber++);
                    newValueCellMin.setCellStyle(unlockedCellStyle);

                    // second max value
                    SXSSFCell oldValueCellMax = row.createCell(cellNumber++);
                    if (resourceSpecificPropertyAPI != null) {
                        if (resourceSpecificPropertyAPI.getIntervalToValue() != null) {
                            oldValueCellMax.setCellValue(resourceSpecificPropertyAPI.getIntervalToValue());
                        }
                    }
                    SXSSFCell newValueCellMax = row.createCell(cellNumber++);
                    newValueCellMax.setCellStyle(unlockedCellStyle);
                } else {
                    SXSSFCell oldValueCell = row.createCell(cellNumber++);
                    if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_MULTIPLE) {
                        oldValueCell.setCellStyle(greyStyle);
                    } else {
                        if (resourceSpecificPropertyAPI != null) {
                            writeResourceSpecificPropertyValue(resourceSpecificPropertyAPI, categorySpecificProperty, oldValueCell);
                        }
                    }
                    SXSSFCell newValueCell = row.createCell(cellNumber++);
                    if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_MULTIPLE) {
                        newValueCell.setCellStyle(greyStyle);
                    } else {
                        if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.TEXTFIELD) {
                            newValueCell.setCellStyle(unlockedTextCellStyle);
                        }
                        else{
                            newValueCell.setCellStyle(unlockedCellStyle);
                        }
                    }
                }
            }
        }
        if (article != null) {
            handleMediaForArticle(workbook, article);
        } else {
            handleMediaForProduct(workbook, product);
        }

    }


    private CellStyle createUnlockedCellStyle(SXSSFWorkbook workbook) {
        CellStyle unlockedCellStyle = workbook.createCellStyle();
        unlockedCellStyle.setLocked(false);
        unlockedCellStyle.setFillForegroundColor(IndexedColors.YELLOW.index);
        unlockedCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        unlockedCellStyle.setBorderBottom(BorderStyle.HAIR);
        unlockedCellStyle.setBorderTop(BorderStyle.HAIR);
        unlockedCellStyle.setBorderLeft(BorderStyle.HAIR);
        unlockedCellStyle.setBorderRight(BorderStyle.HAIR);
        unlockedCellStyle.setBottomBorderColor(IndexedColors.GREY_40_PERCENT.index);
        unlockedCellStyle.setTopBorderColor(IndexedColors.GREY_25_PERCENT.index);
        unlockedCellStyle.setLeftBorderColor(IndexedColors.GREY_25_PERCENT.index);
        unlockedCellStyle.setRightBorderColor(IndexedColors.GREY_25_PERCENT.index);
        return unlockedCellStyle;
    }

    //PELLE
    private CellStyle createUnlockedTextCellStyle(SXSSFWorkbook workbook) {
        CellStyle unlockedTextCellStyle = createUnlockedCellStyle(workbook);
        DataFormat dataFormat = workbook.createDataFormat();
        unlockedTextCellStyle.setDataFormat(dataFormat.getFormat("@"));
        return unlockedTextCellStyle;
    }

    private CellStyle createUnlockedDateCellStyle(SXSSFWorkbook workbook) {
        CellStyle unlockedDateCellStyle = createUnlockedCellStyle(workbook);
        DataFormat dataFormat = workbook.createDataFormat();
        unlockedDateCellStyle.setDataFormat(dataFormat.getFormat("yyyy-mm-dd"));
        return unlockedDateCellStyle;
    }

    private void setOrderUnitWithDeleteDataValidation(Workbook workbook,
                                                      SXSSFSheet sheet,
                                                      int firstRow,
                                                      int lastRow,
                                                      List<CVOrderUnit> orderUnits,
                                                      int articleQuantityInOuterPackageUnitColumnNumber) {
        DataValidationHelper dataValidationHelper = sheet.getDataValidationHelper();
        XSSFName name = (XSSFName) workbook.getName("VLOrderUnits");
        if (name == null) {
            name = (XSSFName) workbook.createName();
            name.setNameName("VLOrderUnits");
            int numberOfValues = orderUnits.size() + 1; // one extra for delete value
            char columnPosition = (char) (numberOfValues + 'A'); // find which is last letter of directives row
            name.setRefersToFormula(VALUELIST_SHEET_NAME + "!$B$4:$" + columnPosition + "$4");
        }

        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createFormulaListConstraint("VLOrderUnits");

        CellRangeAddressList addressListArticleQuantityInOuterPackageUnit = new CellRangeAddressList(firstRow, lastRow, articleQuantityInOuterPackageUnitColumnNumber, articleQuantityInOuterPackageUnitColumnNumber);
        XSSFDataValidation dataValidationArticleQuantityInOuterPackageUnit = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, addressListArticleQuantityInOuterPackageUnit);
        dataValidationArticleQuantityInOuterPackageUnit.createErrorBox("Felaktigt värde", "Välj värde ur listan.");
        dataValidationArticleQuantityInOuterPackageUnit.setShowErrorBox(true);
        sheet.addValidationData(dataValidationArticleQuantityInOuterPackageUnit);

    }

    private void setPackageUnitWithDeleteDataValidation(Workbook workbook,
                                                        SXSSFSheet sheet,
                                                        int firstRow,
                                                        int lastRow,
                                                        List<CVPackageUnit> packageUnits,
                                                        int packageContentUnitColumnNumber) {
        DataValidationHelper dataValidationHelper = sheet.getDataValidationHelper();
        XSSFName name = (XSSFName) workbook.getName("VLPackageUnits");
        if (name == null) {
            name = (XSSFName) workbook.createName();
            name.setNameName("VLPackageUnits");
            int numberOfValues = packageUnits.size() + 1; // one extra for delete value
            char columnPosition = (char) (numberOfValues + 'A'); // find which is last letter of directives row
            name.setRefersToFormula(VALUELIST_SHEET_NAME + "!$B$8:$" + columnPosition + "$8");
        }

        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createFormulaListConstraint("VLPackageUnits");

        CellRangeAddressList packageContentUnit = new CellRangeAddressList(firstRow, lastRow, packageContentUnitColumnNumber, packageContentUnitColumnNumber);
        XSSFDataValidation dataValidationPackageContentUnitUnit = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, packageContentUnit);
        dataValidationPackageContentUnitUnit.createErrorBox("Felaktigt värde", "Välj värde ur listan.");
        dataValidationPackageContentUnitUnit.setShowErrorBox(true);
        sheet.addValidationData(dataValidationPackageContentUnitUnit);
    }

    private void setOrderUnitWithoutDeleteDataValidation(Workbook workbook,
                                                         SXSSFSheet sheet,
                                                         int firstRow,
                                                         int lastRow,
                                                         List<CVOrderUnit> orderUnits,
                                                         int orderUnitColumnNumber) {
        DataValidationHelper dataValidationHelper = sheet.getDataValidationHelper();
        XSSFName name = (XSSFName) workbook.getName("VLOrderUnitsNoDelete");
        if (name == null) {
            name = (XSSFName) workbook.createName();
            name.setNameName("VLOrderUnitsNoDelete");
            int numberOfValues = orderUnits.size(); // NOT one extra for delete value
            char columnPosition = (char) (numberOfValues + 'A'); // find which is last letter of directives row
            name.setRefersToFormula(VALUELIST_SHEET_NAME + "!$B$4:$" + columnPosition + "$4");
        }

        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createFormulaListConstraint("VLOrderUnitsNoDelete");
        CellRangeAddressList addressList = new CellRangeAddressList(firstRow, lastRow, orderUnitColumnNumber, orderUnitColumnNumber);
        XSSFDataValidation dataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, addressList);
        dataValidation.createErrorBox("Felaktigt värde", "Välj värde ur listan.");
        dataValidation.setShowErrorBox(true);
        sheet.addValidationData(dataValidation);
    }

    private void setPreventiveMaintenanceValidFromDataValidation(Workbook workbook,
                                                                 SXSSFSheet sheet,
                                                                 int firstRow,
                                                                 int lastRow,
                                                                 List<CVPreventiveMaintenance> preventiveMaintenances,
                                                                 int preventiveMaintenanceValidFromColumnNumber) {
        DataValidationHelper dataValidationHelper = sheet.getDataValidationHelper();
        XSSFName name = (XSSFName) workbook.getName("VLPreventiveMaintenanceValidFrom");
        if (name == null) {
            name = (XSSFName) workbook.createName();
            name.setNameName("VLPreventiveMaintenanceValidFrom");
            int numberOfValues = preventiveMaintenances.size() + 1; // one extra for delete value
            char columnPosition = (char) (numberOfValues + 'A'); // find which is last letter of directives row
            name.setRefersToFormula(VALUELIST_SHEET_NAME + "!$B$6:$" + columnPosition + "$6");
        }

        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createFormulaListConstraint("VLPreventiveMaintenanceValidFrom");
        CellRangeAddressList addressList = new CellRangeAddressList(firstRow, lastRow, preventiveMaintenanceValidFromColumnNumber, preventiveMaintenanceValidFromColumnNumber);
        XSSFDataValidation dataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, addressList);
        dataValidation.createErrorBox("Felaktigt värde", "Välj värde ur listan.");
        dataValidation.setShowErrorBox(true);
        sheet.addValidationData(dataValidation);
    }

    private void setYesNoDataValidation(Workbook workbook, SXSSFSheet sheet, int firstRow, int lastRow,
                                        int customerUniqueColumnNumber,
                                        int ceMarkedColumnNumber,
                                        int inactivateRowsOnDiscontinuedColumnNumber) {
        DataValidationHelper dataValidationHelper = sheet.getDataValidationHelper();
        XSSFName name = (XSSFName) workbook.getName("VLYesNo");
        if (name == null) {
            name = (XSSFName) workbook.createName();
            name.setNameName("VLYesNo");
            name.setRefersToFormula(VALUELIST_SHEET_NAME + "!$B$3:$C$3");
        }

        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createFormulaListConstraint("VLYesNo");

        // customer unique yes/no
        CellRangeAddressList customerUniqueAddressList = new CellRangeAddressList(firstRow, lastRow, customerUniqueColumnNumber, customerUniqueColumnNumber);
        XSSFDataValidation customerUniqueDataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, customerUniqueAddressList);
        customerUniqueDataValidation.createErrorBox("Felaktigt värde", "Välj värde ur listan.");
        customerUniqueDataValidation.setShowErrorBox(true);
        sheet.addValidationData(customerUniqueDataValidation);

        // ce marked yes/no
        CellRangeAddressList ceMarkedAddressList = new CellRangeAddressList(firstRow, lastRow, ceMarkedColumnNumber, ceMarkedColumnNumber);
        XSSFDataValidation ceMarkedDataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, ceMarkedAddressList);
        ceMarkedDataValidation.createErrorBox("Felaktigt värde", "Välj värde ur listan.");
        ceMarkedDataValidation.setShowErrorBox(true);
        sheet.addValidationData(ceMarkedDataValidation);

        // inactivate rows on discontinued yes/no
        CellRangeAddressList inactivateRowsOnDiscontinuedAddressList = new CellRangeAddressList(firstRow, lastRow, inactivateRowsOnDiscontinuedColumnNumber, inactivateRowsOnDiscontinuedColumnNumber);
        XSSFDataValidation inactivateRowsOnDiscontinuedDataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, inactivateRowsOnDiscontinuedAddressList);
        inactivateRowsOnDiscontinuedDataValidation.createErrorBox("Felaktigt värde", "Välj värde ur listan.");
        inactivateRowsOnDiscontinuedDataValidation.setShowErrorBox(true);
        sheet.addValidationData(inactivateRowsOnDiscontinuedDataValidation);
    }

    private void setProductArticleValidation(Workbook workbook, SXSSFSheet sheet, int firstRow, int lastRow, boolean onlyArticle) {
        DataValidationHelper dataValidationHelper = sheet.getDataValidationHelper();

        if (onlyArticle) {

            XSSFName name = (XSSFName) workbook.getName("VLOnlyArticle");
            if (name == null) {
                name = (XSSFName) workbook.createName();
                name.setNameName("VLOnlyArticle");
                name.setRefersToFormula(VALUELIST_SHEET_NAME + "!$C$5:$C$5");
            }
        } else {
            XSSFName name = (XSSFName) workbook.getName("VLProductArticle");
            if (name == null) {
                name = (XSSFName) workbook.createName();
                name.setNameName("VLProductArticle");
                name.setRefersToFormula(VALUELIST_SHEET_NAME + "!$B$5:$C$5");
            }
        }

        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createFormulaListConstraint(onlyArticle ? "VLOnlyArticle" : "VLProductArticle");
        CellRangeAddressList customerUniqueAddressList = new CellRangeAddressList(firstRow, lastRow, 1, 1);
        XSSFDataValidation productArticleDataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, customerUniqueAddressList);
        productArticleDataValidation.createErrorBox("Felaktigt värde", "Välj värde ur listan.");
        productArticleDataValidation.setShowErrorBox(true);
        sheet.addValidationData(productArticleDataValidation);

    }

    private void setIntegerValidation(SXSSFSheet sheet,
                                      int firstRow,
                                      int lastRow,
                                      int articleQuantityInOuterPackageColumnNumber,
                                      int packageContentColumnNumber,
                                      int packageLevelTopColumnNumber,
                                      int packageLevelMiddleColumnNumber,
                                      int packageLevelBaseColumnNumber,
                                      int preventiveMaintenanceNumberOfDaysColumnNumber) {

        DataValidationHelper dataValidationHelper = sheet.getDataValidationHelper();
        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createIntegerConstraint(DataValidationConstraint.OperatorType.GREATER_OR_EQUAL, "-1", null);

        CellRangeAddressList articleQuantityInOuterPackageAddressList = new CellRangeAddressList(firstRow, lastRow, articleQuantityInOuterPackageColumnNumber, articleQuantityInOuterPackageColumnNumber);
        DataValidation articleQuantityInOuterPackageDataValidation = dataValidationHelper.createValidation(dataValidationConstraint, articleQuantityInOuterPackageAddressList);
        articleQuantityInOuterPackageDataValidation.createErrorBox("Felaktigt värde", "Giltigt värde är ett heltal större än -2.");
        articleQuantityInOuterPackageDataValidation.setShowErrorBox(true);
        sheet.addValidationData(articleQuantityInOuterPackageDataValidation);

        CellRangeAddressList packageContentAddressList = new CellRangeAddressList(firstRow, lastRow, packageContentColumnNumber, packageContentColumnNumber);
        DataValidation packageContentDataValidation = dataValidationHelper.createValidation(dataValidationConstraint, packageContentAddressList);
        packageContentDataValidation.createErrorBox("Felaktigt värde", "Giltigt värde är ett heltal större än -2.");
        packageContentDataValidation.setShowErrorBox(true);
        sheet.addValidationData(packageContentDataValidation);

        CellRangeAddressList packageLevelTopAddressList = new CellRangeAddressList(firstRow, lastRow, packageLevelTopColumnNumber, packageLevelTopColumnNumber);
        DataValidation packageLevelTopDataValidation = dataValidationHelper.createValidation(dataValidationConstraint, packageLevelTopAddressList);
        packageLevelTopDataValidation.createErrorBox("Felaktigt värde", "Giltigt värde är ett heltal större än -2.");
        packageLevelTopDataValidation.setShowErrorBox(true);
        sheet.addValidationData(packageLevelTopDataValidation);

        CellRangeAddressList packageLevelMiddleAddressList = new CellRangeAddressList(firstRow, lastRow, packageLevelMiddleColumnNumber, packageLevelMiddleColumnNumber);
        DataValidation packageLevelMiddleDataValidation = dataValidationHelper.createValidation(dataValidationConstraint, packageLevelMiddleAddressList);
        packageLevelMiddleDataValidation.createErrorBox("Felaktigt värde", "Giltigt värde är ett heltal större än -2.");
        packageLevelMiddleDataValidation.setShowErrorBox(true);
        sheet.addValidationData(packageLevelMiddleDataValidation);

        CellRangeAddressList packageLevelBaseAddressList = new CellRangeAddressList(firstRow, lastRow, packageLevelBaseColumnNumber, packageLevelBaseColumnNumber);
        DataValidation packageLevelBaseDataValidation = dataValidationHelper.createValidation(dataValidationConstraint, packageLevelBaseAddressList);
        packageLevelBaseDataValidation.createErrorBox("Felaktigt värde", "Giltigt värde är ett heltal större än -2.");
        packageLevelBaseDataValidation.setShowErrorBox(true);
        sheet.addValidationData(packageLevelBaseDataValidation);

        CellRangeAddressList preventiveMaintenanceNumberOfDaysAddressList = new CellRangeAddressList(firstRow, lastRow, preventiveMaintenanceNumberOfDaysColumnNumber, preventiveMaintenanceNumberOfDaysColumnNumber);


        DataValidation preventiveMaintenanceNumberOfDaysDataValidation = dataValidationHelper.createValidation(dataValidationConstraint, preventiveMaintenanceNumberOfDaysAddressList);
        preventiveMaintenanceNumberOfDaysDataValidation.createErrorBox("Felaktigt värde", "Giltigt värde är ett heltal större än -2.");
        preventiveMaintenanceNumberOfDaysDataValidation.setShowErrorBox(true);
        sheet.addValidationData(preventiveMaintenanceNumberOfDaysDataValidation);

    }

    private void setIntegerValidationForCategory(SXSSFSheet sheet, int firstRow, int lastRow, List<Integer> categorySpecificPropertyIntegerColumns) {
        DataValidationHelper dataValidationHelper = sheet.getDataValidationHelper();
        DataValidationConstraint dataValidationConstraint = dataValidationHelper.createIntegerConstraint(DataValidationConstraint.OperatorType.GREATER_OR_EQUAL, "-1", null);

        if (categorySpecificPropertyIntegerColumns != null && !categorySpecificPropertyIntegerColumns.isEmpty()) {
            for (Integer columnIndex : categorySpecificPropertyIntegerColumns) {
                CellRangeAddressList categorySpecificPropertyAddressList = new CellRangeAddressList(firstRow, lastRow, columnIndex, columnIndex);
                XSSFDataValidation categorySpecificPropertyDataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, categorySpecificPropertyAddressList);
                categorySpecificPropertyDataValidation.createErrorBox("Felaktigt värde", "Giltigt värde är ett heltal större än -2.");
                categorySpecificPropertyDataValidation.setShowErrorBox(true);
                sheet.addValidationData(categorySpecificPropertyDataValidation);
            }
        }

    }

    private void setDateValidation(SXSSFSheet sheet, int firstRow, int lastRow, int discontinuedColumnNumber) {
        DataValidationHelper dataValidationHelper = sheet.getDataValidationHelper();
        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createDateConstraint(DataValidationConstraint.OperatorType.GREATER_THAN, "25569", null, "yyyy-mm-dd");

        CellRangeAddressList discontinuedAddressList = new CellRangeAddressList(firstRow, lastRow, discontinuedColumnNumber, discontinuedColumnNumber);
        XSSFDataValidation adiscontinuedDataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, discontinuedAddressList);

        adiscontinuedDataValidation.createErrorBox("Felaktigt värde", "Giltigt värde är ett datum senare än 1970-01-01, formatet ska vara yyyy-mm-dd.");

        adiscontinuedDataValidation.setShowErrorBox(true);
        sheet.addValidationData(adiscontinuedDataValidation);
    }

    private void setDateValidation2(Workbook workbook, SXSSFSheet sheet, int firstRow, int lastRow, List<String> dates, int discontinuedColumnNumber) {
        DataValidationHelper dataValidationHelper = sheet.getDataValidationHelper();
        XSSFName name = (XSSFName) workbook.getName("VLDates");
        if (name == null) {
            name = (XSSFName) workbook.createName();
            name.setNameName("VLDates");
            int numberOfValues = dates.size();
            String columnPosition = CellReference.convertNumToColString(numberOfValues); // find which is last letter of dates row
            name.setRefersToFormula(VALUELIST_SHEET_NAME + "!$B$9:$" + columnPosition + "$9");
        }
        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createFormulaListConstraint("VLDates");
        CellRangeAddressList addressList = new CellRangeAddressList(firstRow, lastRow, discontinuedColumnNumber, discontinuedColumnNumber);
        XSSFDataValidation dataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, addressList);
        dataValidation.createErrorBox("Felaktigt datum", "Välj datum ur listan.");
        dataValidation.setShowErrorBox(true);
        sheet.addValidationData(dataValidation);
    }

    private void setDirectivesValidation(Workbook workbook, SXSSFSheet sheet, int firstRow, int lastRow, List<CVCEDirective> directives, int directivesColumnNumber) {
        DataValidationHelper dataValidationHelper = sheet.getDataValidationHelper();
        XSSFName name = (XSSFName) workbook.getName("VLDirectives");
        if (name == null) {
            name = (XSSFName) workbook.createName();
            name.setNameName("VLDirectives");
            int numberOfValues = directives.size() + 1; // one extra for delete value
            char columnPosition = (char) (numberOfValues + 'A'); // find which is last letter of directives row
            name.setRefersToFormula(VALUELIST_SHEET_NAME + "!$B$2:$" + columnPosition + "$2");
        }
        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createFormulaListConstraint("VLDirectives");
        CellRangeAddressList addressList = new CellRangeAddressList(firstRow, lastRow, directivesColumnNumber, directivesColumnNumber);
        XSSFDataValidation dataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, addressList);
        dataValidation.createErrorBox("Felaktigt värde", "Välj värde ur listan.");
        dataValidation.setShowErrorBox(true);
        sheet.addValidationData(dataValidation);
    }

    private void setStandardsValidation(Workbook workbook, SXSSFSheet sheet, int firstRow, int lastRow, List<CVCEStandard> standards, int standardsColumnNumber) {
        DataValidationHelper dataValidationHelper = sheet.getDataValidationHelper();
        XSSFName name = (XSSFName) workbook.getName("VLStandards");
        if (name == null) {
            name = (XSSFName) workbook.createName();
            name.setNameName("VLStandards");
            int numberOfValues = standards.size() + 1; // one extra for delete value
            char columnPosition = (char) (numberOfValues + 'A'); // find which is last letter of standards row
            name.setRefersToFormula(VALUELIST_SHEET_NAME + "!$B$1:$" + columnPosition + "$1");
        }
        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createFormulaListConstraint("VLStandards");
        CellRangeAddressList addressList = new CellRangeAddressList(firstRow, lastRow, standardsColumnNumber, standardsColumnNumber);
        XSSFDataValidation dataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, addressList);
        dataValidation.createErrorBox("Felaktigt värde", "Välj värde ur listan.");
        dataValidation.setShowErrorBox(true);
        sheet.addValidationData(dataValidation);
    }

    private void setDocumentTypesValidation(SXSSFWorkbook workbook, int firstRow, int lastRow, List<CVDocumentType> documentTypes) {
        SXSSFSheet documentsSheet = workbook.getSheet(DOCUMENTS_SHEET_NAME);
        DataValidationHelper dataValidationHelper = documentsSheet.getDataValidationHelper();
        XSSFName name = (XSSFName) workbook.getName("VLDocumentTypes");
        if (name == null) {
            name = (XSSFName) workbook.createName();
            name.setNameName("VLDocumentTypes");
            int numberOfValues = documentTypes.size();
            char columnPosition = (char) (numberOfValues + 'A'); // find which is last letter of document types row
            name.setRefersToFormula(VALUELIST_SHEET_NAME + "!$B$7:$" + columnPosition + "$7");
        }
        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createFormulaListConstraint("VLDocumentTypes");
        CellRangeAddressList addressList = new CellRangeAddressList(firstRow, lastRow, 7, 7);
        XSSFDataValidation dataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, addressList);
        dataValidation.createErrorBox("Felaktigt värde", "Välj värde ur listan.");
        dataValidation.setShowErrorBox(true);
        documentsSheet.addValidationData(dataValidation);
    }

    private void setCategorySpecificPropertyValidation(SXSSFWorkbook workbook, CategorySpecificProperty categorySpecificProperty, SXSSFSheet sheet, int firstRow, int lastRow, int cellNumber, SXSSFSheet valueListSheet, int nextValueListRow) {
        List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues = categorySpecificProperty.getCategorySpecificPropertyListValues();

        SXSSFRow row = valueListSheet.createRow(nextValueListRow);
        SXSSFCell nameCell = row.createCell(0);
        nameCell.setCellValue(categorySpecificProperty.getName());
        for (int i = 0; i < categorySpecificPropertyListValues.size(); i++) {
            SXSSFCell valueCell = row.createCell(i + 1);
            valueCell.setCellValue(categorySpecificPropertyListValues.get(i).getCode());
        }
        SXSSFCell deleteCell = row.createCell(categorySpecificPropertyListValues.size() + 1);
        deleteCell.setCellValue(CLEAR_VALUE_STRING);

        DataValidationHelper dataValidationHelper = sheet.getDataValidationHelper();
        XSSFName name = (XSSFName) workbook.createName();
        name.setNameName("VLCSPLV" + nextValueListRow);
        char columnPosition = (char) (categorySpecificPropertyListValues.size() + 1 + 'A'); // find which is last letter of standards row
        name.setRefersToFormula(VALUELIST_SHEET_NAME + "!$B$" + (nextValueListRow + 1) + ":$" + columnPosition + "$" + (nextValueListRow + 1));

        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createFormulaListConstraint("VLCSPLV" + nextValueListRow);
        CellRangeAddressList addressList = new CellRangeAddressList(firstRow, lastRow, cellNumber, cellNumber);
        XSSFDataValidation dataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, addressList);
        dataValidation.createErrorBox("Felaktigt värde", "Välj värde ur listan.");
        dataValidation.setShowErrorBox(true);
        sheet.addValidationData(dataValidation);
    }

    private ResourceSpecificPropertyAPI findByCategorySpecificProperty(CategorySpecificProperty categorySpecificProperty, List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs) {
        if (resourceSpecificPropertyAPIs != null && !resourceSpecificPropertyAPIs.isEmpty()) {
            for (ResourceSpecificPropertyAPI resourceSpecificPropertyAPI : resourceSpecificPropertyAPIs) {
                if (resourceSpecificPropertyAPI.getProperty().getId().equals(categorySpecificProperty.getUniqueId())) {
                    return resourceSpecificPropertyAPI;
                }
            }
        }
        return null;
    }

    private void writeResourceSpecificPropertyValue(ResourceSpecificPropertyAPI resourceSpecificPropertyAPI, CategorySpecificProperty categorySpecificProperty, SXSSFCell oldValueCell) {
        if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.TEXTFIELD) {
            oldValueCell.setCellValue(resourceSpecificPropertyAPI.getTextValue());
        } else if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.DECIMAL) {
            if (resourceSpecificPropertyAPI.getDecimalValue() != null) {
                oldValueCell.setCellValue(resourceSpecificPropertyAPI.getDecimalValue());
            }
        } else if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_SINGLE) {
            if (resourceSpecificPropertyAPI.getSingleListValue() != null) {
                // in api we use long id of list value, but we want the string value
                String singleListValueValue = getListValueFromListId(resourceSpecificPropertyAPI.getSingleListValue(), categorySpecificProperty.getCategorySpecificPropertyListValues());
                oldValueCell.setCellValue(singleListValueValue);
            }
        }
    }

    private String getListValueFromListId(long listValue, List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues) {
        if (categorySpecificPropertyListValues != null) {
            for (CategorySpecificPropertyListValue categorySpecificPropertyListValue : categorySpecificPropertyListValues) {
                if (categorySpecificPropertyListValue.getUniqueId().equals(listValue)) {
                    return categorySpecificPropertyListValue.getValue();
                }
            }
        }
        return null;
    }

    private int fillValueListSheet(SXSSFSheet valueListSheet,
                                   List<CVCEStandard> standards,
                                   List<CVCEDirective> directives,
                                   List<CVOrderUnit> orderUnits,
                                   List<CVPackageUnit> packageUnits,
                                   List<CVPreventiveMaintenance> preventiveMaintenances,
                                   List<CVDocumentType> documentTypes,
                                   List<String> dates){
        // standards
        SXSSFRow standardsRow = valueListSheet.createRow(0);
        SXSSFCell standardsNameCell = standardsRow.createCell(0);
        standardsNameCell.setCellValue("Standard");
        for (int i = 0; i < standards.size(); i++) {
            SXSSFCell standardsValueCell = standardsRow.createCell(i + 1);
            standardsValueCell.setCellValue(standards.get(i).getName());
        }
        SXSSFCell standardsDeleteValueCell = standardsRow.createCell(standards.size() + 1);
        standardsDeleteValueCell.setCellValue(CLEAR_VALUE_STRING);

        // directives
        SXSSFRow directivesRow = valueListSheet.createRow(1);
        SXSSFCell directivesNameCell = directivesRow.createCell(0);
        directivesNameCell.setCellValue("Direktiv");
        for (int i = 0; i < directives.size(); i++) {
            SXSSFCell directivesValueCell = directivesRow.createCell(i + 1);
            directivesValueCell.setCellValue(directives.get(i).getName());
        }
        SXSSFCell directivesDeleteValueCell = directivesRow.createCell(directives.size() + 1);
        directivesDeleteValueCell.setCellValue(CLEAR_VALUE_STRING);

        // yes/no values
        SXSSFRow yesNoRow = valueListSheet.createRow(2);
        SXSSFCell yesNoNameCell = yesNoRow.createCell(0);
        yesNoNameCell.setCellValue("Ja/Nej");
        SXSSFCell yesValueCell = yesNoRow.createCell(1);
        yesValueCell.setCellValue(YES);
        SXSSFCell noValueCell = yesNoRow.createCell(2);
        noValueCell.setCellValue(NO);

        // order units
        SXSSFRow orderUnitsRow = valueListSheet.createRow(3);
        SXSSFCell orderUnitsNameCell = orderUnitsRow.createCell(0);
        orderUnitsNameCell.setCellValue("Beställningsenhet");
        for (int i = 0; i < orderUnits.size(); i++) {
            SXSSFCell orderUnitsValueCell = orderUnitsRow.createCell(i + 1);
            orderUnitsValueCell.setCellValue(orderUnits.get(i).getName());
        }
        SXSSFCell orderUnitsDeleteValueCell = orderUnitsRow.createCell(orderUnits.size() + 1);
        orderUnitsDeleteValueCell.setCellValue(CLEAR_VALUE_STRING);

        // product or article
        SXSSFRow productOrArticleRow = valueListSheet.createRow(4);
        SXSSFCell productOrArticleNameCell = productOrArticleRow.createCell(0);
        productOrArticleNameCell.setCellValue("Produkt/Artikel");
        SXSSFCell productValueCell = productOrArticleRow.createCell(1);
        productValueCell.setCellValue(PRODUCT_TYPE_NAME);
        SXSSFCell articleValueCell = productOrArticleRow.createCell(2);
        articleValueCell.setCellValue(ARTICLE_TYPE_NAME);

        // preventive maintenance valid from
        SXSSFRow preventiveMaintenceRow = valueListSheet.createRow(5);
        SXSSFCell preventiveMaintenceNameCell = preventiveMaintenceRow.createCell(0);
        preventiveMaintenceNameCell.setCellValue("Förebyggande underhåll gäller från");
        for (int i = 0; i < preventiveMaintenances.size(); i++) {
            SXSSFCell preventiveMaintenanceValueCell = preventiveMaintenceRow.createCell(i + 1);
            preventiveMaintenanceValueCell.setCellValue(preventiveMaintenances.get(i).getName());
        }
        SXSSFCell preventiveMaintenancesDeleteValueCell = preventiveMaintenceRow.createCell(preventiveMaintenances.size() + 1);
        preventiveMaintenancesDeleteValueCell.setCellValue(CLEAR_VALUE_STRING);

        // document types
        SXSSFRow documentTypesRow = valueListSheet.createRow(6);
        SXSSFCell documentTypesNameCell = documentTypesRow.createCell(0);
        documentTypesNameCell.setCellValue("Dokumenttyp");
        for (int i = 0; i < documentTypes.size(); i++) {
            SXSSFCell documentTypesValueCell = documentTypesRow.createCell(i + 1);
            documentTypesValueCell.setCellValue(documentTypes.get(i).getValue());
        }

        // package unit types
        SXSSFRow packageUnitsRow = valueListSheet.createRow(7);
        SXSSFCell packageUnitsNameCell = packageUnitsRow.createCell(0);
        packageUnitsNameCell.setCellValue("Förpackningsenhet");
        for (int i = 0; i < packageUnits.size(); i++) {
            SXSSFCell packageUnitsValueCell = packageUnitsRow.createCell(i + 1);
            packageUnitsValueCell.setCellValue(packageUnits.get(i).getName());
        }
        SXSSFCell packageUnitsDeleteValueCell = packageUnitsRow.createCell(packageUnits.size() + 1);
        packageUnitsDeleteValueCell.setCellValue(CLEAR_VALUE_STRING);

        // dates
        SXSSFRow datesRow = valueListSheet.createRow(8);
        SXSSFCell datesNameCell = datesRow.createCell(0);
        datesNameCell.setCellValue("Datum");
        for (int i = 0; i < dates.size(); i++) {
            SXSSFCell datesValueCell = datesRow.createCell(i + 1);
            datesValueCell.setCellValue(dates.get(i));

        }


        //return 7;
        return 8;
    }

    private List<String> getDates(int yearsBack, int yearsForward){


        Calendar date = Calendar.getInstance();
        date.add(Calendar.YEAR, -yearsBack);
        Calendar stopDate = Calendar.getInstance();
        stopDate.add(Calendar.YEAR, yearsForward);
        int numberOfDates = (int)((stopDate.getTimeInMillis() - date.getTimeInMillis())/(1000*60*60*24));
        List<String> dates = new ArrayList<>();
        String strYear;
        String strMonth;
        String strDay;

        for (int i=0; i < numberOfDates; i++) {
            strYear = Integer.toString(date.get(Calendar.YEAR));
            strMonth = date.get(Calendar.MONTH) <  9 ? "0" + Integer.toString(date.get(Calendar.MONTH) + 1) : Integer.toString(date.get(Calendar.MONTH) + 1);
            strDay = date.get(Calendar.DATE) < 10 ?  "0" + Integer.toString(date.get(Calendar.DATE)) : Integer.toString(date.get(Calendar.DATE));

            dates.add(strYear + "-" + strMonth + "-" + strDay);
            date.add(Calendar.DATE, 1);
        }
        return dates;
    }

    String getConnectionsAsStringArticle(ArticleAPI article) {
        StringBuilder connections = new StringBuilder();
        if (article.getFitsToProducts() != null && !article.getFitsToProducts().isEmpty()) {
            connections.append(article.
                    getFitsToProducts().
                    stream().
                    map(p -> p.getProductNumber()).
                    collect(Collectors.joining(CONNECTIONS_SEPARATOR)));
        }
        if (article.getFitsToArticles() != null && !article.getFitsToArticles().isEmpty()) {
            if (connections.length() != 0) {
                connections.append(";");
            }
            connections.append(article.
                    getFitsToArticles().
                    stream().
                    map(a -> a.getArticleNumber()).
                    collect(Collectors.joining(CONNECTIONS_SEPARATOR)));
        }
        return connections.toString();
    }

    private String getExtendedCategoriesAsString(List<CategoryAPI> extendedCategories) {
        StringBuilder connections = new StringBuilder();
        if (extendedCategories != null) {
            connections.append(extendedCategories.
                    stream().
                    map(c -> c.getCode()).
                    collect(Collectors.joining(CONNECTIONS_SEPARATOR)));
        }
        return connections.toString();
    }

    /**
     * Create the sheet basics for media sheet, like field names and header rows,
     * locking etc
     *
     * @param mediaSheet
     * @param unlockedTextCellStyle
     */
    private void prepareMediaSheet(SXSSFSheet mediaSheet, CellStyle unlockedTextCellStyle, MediaType mediaType) {

        // write field names (used when reading back file), could have used
        // column numbers alone, but that would break if the Excel file
        // was changed in a later release
        SXSSFRow fieldNamesRow = mediaSheet.createRow(0);
        fieldNamesRow.setZeroHeight(true);

        // write headers
        Integer cellNumber = 0;
        SXSSFRow headerRow = mediaSheet.createRow(1);
        headerRow.createCell(cellNumber++).setCellValue("Unikt Id");

        // lock for editing (except cells that are opened later)
        mediaSheet.protectSheet(UUID.randomUUID().toString());
        //mediaSheet.protectSheet("apa");
        // allow column sizes to be changed even if sheet is locked
        mediaSheet.lockFormatColumns(false);

        // product/article number
        mediaSheet.setDefaultColumnStyle(cellNumber, unlockedTextCellStyle);
        fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_NUMBER_NAME);
        headerRow.createCell(cellNumber++).setCellValue("Produktnummer/Artikelnummer");

        // url
        headerRow.createCell(cellNumber++).setCellValue("URL");
        fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_MEDIA_URL_NAME);
        mediaSheet.setDefaultColumnStyle(cellNumber, unlockedTextCellStyle);
        headerRow.createCell(cellNumber++).setCellValue("URL");

        // description
        headerRow.createCell(cellNumber++).setCellValue("Beskrivning");
        fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_MEDIA_DESCRIPTION_NAME);
        mediaSheet.setDefaultColumnStyle(cellNumber, unlockedTextCellStyle);
        headerRow.createCell(cellNumber++).setCellValue("Beskrivning");

        if (MediaType.DOCUMENT == mediaType) {
            // document type
            headerRow.createCell(cellNumber++).setCellValue("Dokumenttyp");
            fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_MEDIA_DOCUMENTTYPE_NAME);
            mediaSheet.setDefaultColumnStyle(cellNumber, unlockedTextCellStyle);
            headerRow.createCell(cellNumber++).setCellValue("Dokumenttyp");
        } else if (MediaType.IMAGE == mediaType || MediaType.VIDEO == mediaType) {
            // main image alt text
            headerRow.createCell(cellNumber++).setCellValue("Alt-text");
            fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_MEDIA_ALTTEXT_NAME);
            mediaSheet.setDefaultColumnStyle(cellNumber, unlockedTextCellStyle);
            headerRow.createCell(cellNumber++).setCellValue("Alt-text");
        }

    }

}
