package se.inera.hjalpmedelstjansten.business.user.dao;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.NoResultException;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import lombok.NoArgsConstructor;
import org.apache.poi.ss.formula.eval.NotImplementedException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.dto.NotificationDTO;
import se.inera.hjalpmedelstjansten.model.entity.Notification;
import se.inera.hjalpmedelstjansten.model.entity.UserNotification;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

@Stateless
@NoArgsConstructor
public class NotificationDAO extends StandardAbstractDAO<UserNotification> {

    @Inject
    HjmtLogger LOG;


    @Override
    public Optional<UserNotification> create(UserNotification newEntity) {

        LOG.log(Level.FINE, "NotificationDAO.create()");

        try {
            entityManager.persist(newEntity);
            return Optional.ofNullable(entityManager.find(UserNotification.class, newEntity.getUniqueId()));

        } catch (Exception e) {
            LOG.log(Level.FINEST, "Error while persisting/finding UserNotification");
            LOG.log(Level.FINEST, e.getMessage());
        }

        // If persisting failed. Return empty.
        return Optional.empty();
    }


    /**
     * Get UserNotification by uniqueId
     * @param uniqueId Id of unique UserNotification.
     * @return Optional of UserNotification or empty.
     */
    @Override
    public Optional<UserNotification> read(long uniqueId) {
        LOG.log(Level.FINE, "NotificationDAO.create()");
        return Optional.ofNullable(entityManager.find(clazz, uniqueId));
    }


    /**
     * Basic version aimed at only needing the userId to fetch notifications.
     * It is recommended to use the DTO-version to get more information available.
     * This method is used either way to fetch initial basic values.
     *
     * @param userId userId.
     * @return Optional of UserNotification or exception.
     *
     */
    public Optional<UserNotification> findByUserId(long userId) {

        LOG.log(Level.FINE, "NotificationDAO.findNotificationsByUsername()");

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserNotification> criteria = builder.createQuery(UserNotification.class);

        Root<UserNotification> from = criteria.from(UserNotification.class);
        criteria.select(from);

        // Get the current users persisted UserNotifications.
        criteria.where(builder.equal(from.get("userAccount").get("uniqueId"), (userId)));
        TypedQuery<UserNotification> typed = entityManager.createQuery(criteria);

        // Return optional result.
        try {
            return Optional.of(typed.getSingleResult());

        } catch (NoResultException exc) {
            LOG.log(Level.WARNING, "No UserNotification found for user: " + userId);
            return Optional.empty();

        } catch (Exception exc) {
            LOG.log(Level.SEVERE, exc.getMessage());
            return Optional.empty();
        }

    }


    /**
     * Find all available notifications for user.
     *
     * @param notificationDTO notification Data Transfer Object
     * @return updated Data Transfer Object.
     */
    public NotificationDTO findAllAvailableToUser(NotificationDTO notificationDTO) {

        if(null == notificationDTO.getAllNotificationsMap()) {
            notificationDTO.setAllNotificationsMap(new HashMap<>());
        }

        if(null == notificationDTO.getAllNotificationTypeSet()) {
            notificationDTO.setAllNotificationTypeSet(new HashSet<>());
        }

        // Create query for matching 'recipient/organizationType' & 'permission/UserRole.RoleName'.
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        // Define notification criteria
        CriteriaQuery<Notification> notificationCriteria = builder.createQuery(Notification.class);

        // Set Notification as Root
        Root<Notification> from = notificationCriteria.from(Notification.class);

        // Use criteria to build query
        // (SELECT FROM Notification n WHERE n.getRecipient = OrganizationType AND n.getUserRole EXISTS in UserRoles)
        final CriteriaQuery<Notification> query = notificationCriteria.select(from)
                .where(
                    builder.equal(from.get("recipient"), notificationDTO.getOrganizationType())
//                    builder.and(notificationDTO.getUserRoles().contains(from.get("permission"))
//                    fixme: find out how to sort by (from.get("permission") in DTO.getRoles::getName
        );

        // Send query.
        final List<Notification> resultList = entityManager.createQuery(query).getResultList();

        // If no results. Return DTO with soft error.
        if(null == resultList || resultList.isEmpty()) {
            notificationDTO.getError().put("", "No notifications available for user : " + notificationDTO.getUserId());
            return notificationDTO;
        }

        // Merge results.
        resultList.forEach(notification -> {

            // related, remove this when query is fixed.
            for(UserRole role : notificationDTO.getUserRoles()) {
                // If notification permission is one of users roles, add to DTO.
                if (notification.getPermission() == role.getName()) {
                    notificationDTO.getAllNotificationTypeSet().add(notification);
                    notificationDTO.getAllNotificationsMap().put(notification.getType().toString(), notification.getDescription());
                }
            }

        });

        // Return updated DTO.
        return notificationDTO;

    }


    @Override
    public Optional<UserNotification> update(long uniqueId, UserNotification entity) throws NotImplementedException {

        LOG.log(Level.FINE, "NotificationDAO.update()");

        try {

            // Maybe should check reference and flush instead of merge. Let's see.
            return Optional.ofNullable(entityManager.merge(entity));

        } catch (Exception exc) {
            LOG.log(Level.SEVERE, exc.getMessage());
        }

        return Optional.empty();

    }

    @Override
    public Optional<UserNotification> delete(long Id) throws NotImplementedException {

        LOG.log(Level.FINE, "NotificationDAO.delete()");
        throw new NotImplementedException("Not yet implemented. Maybe never will be.");

    }

}
