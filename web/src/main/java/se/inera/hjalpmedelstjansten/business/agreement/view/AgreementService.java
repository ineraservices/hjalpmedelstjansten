package se.inera.hjalpmedelstjansten.business.agreement.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.ResultExporterController;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementController;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.ArticleChanged;
import se.inera.hjalpmedelstjansten.model.entity.ChangeChecker;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.logging.Level;


/**
 * REST API for agreements.
 *
 */
@Stateless
@Path("organizations/{organizationUniqueId}/agreements")
@Interceptors({ PerformanceLogInterceptor.class })
public class AgreementService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    AgreementController agreementController;

    @Inject
    AgreementPricelistController pricelistController;

    @Inject
    ArticleController articleController;


    @EJB
    UserController userController;


    @Inject
    ResultExporterController resultExporterController;

    @EJB
    AuthHandler authHandler;

    /**
     * Search agreements by user query
     *
     * @param query the user query
     * @param organizationUniqueId
     * @param statuses
     * @param include
     * @param offset for pagination purpose
     * @param limit for assortment we need to be able to return all agreements, 0
     * means return all agreements
     * @return a list of <code>AgreementAPI</code> matching the query and a
     * header X-Total-Count giving the total number of agreements for pagination
     */
    @GET
    @SecuredService(permissions = {"agreement:view_own"})
    public Response searchAgreeements(
            @DefaultValue(value = "") @QueryParam("query") String query,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @QueryParam("status") List<Agreement.Status> statuses,
            @QueryParam("include") AgreementAPI.Include include,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @DefaultValue(value = "25") @QueryParam("limit") int limit) {
        LOG.log(Level.FINEST, "searchAgreeements( organizationUniqueId: {0}, offset: {1} )", new Object[] {organizationUniqueId, offset});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        if( query != null && query.isEmpty() ) {
            query = null;
        }
        boolean onlyWhereContactPerson = false;
        if(include == AgreementAPI.Include.MINE) {
            onlyWhereContactPerson = true;
        }
        long countSearch = agreementController.countSearchAgreements(query, statuses, organizationUniqueId, userAPI, onlyWhereContactPerson);
        List<AgreementAPI> agreementAPIs = agreementController.searchAgreements(query,statuses, organizationUniqueId, userAPI, onlyWhereContactPerson, offset, limit);
        return Response.ok(agreementAPIs).
                header("X-Total-Count", countSearch).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }

    @GET
    @Path("adminViewAgreements")
    public Response adminSearchAllAgreeementsForOrganization(
            @Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @QueryParam("query") String query,
            @QueryParam("status") List<Agreement.Status> statuses,
            @QueryParam("include") AgreementAPI.Include include) {
        LOG.log(Level.FINEST, "searchAgreeements( organizationUniqueId: {0}, offset: {1} )", new Object[] {organizationUniqueId, -1});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        Long removedUserId = Long.valueOf(query);
        UserAPI removedUserAPI = userController.getUserAPI(organizationUniqueId,removedUserId,userAPI,authHandler.getSessionId(),getRequestIp(httpServletRequest));
        boolean onlyWhereContactPerson = false;
        if(include == AgreementAPI.Include.MINE) {
            onlyWhereContactPerson = true;
        }

        long countSearch = agreementController.countSearchAgreements(null, statuses, organizationUniqueId, removedUserAPI, onlyWhereContactPerson);
        List<AgreementAPI> agreementAPIs = agreementController.searchAgreements(null,statuses, organizationUniqueId, removedUserAPI, onlyWhereContactPerson, 0, 0);
        return Response.ok(agreementAPIs).
                header("X-Total-Count", countSearch).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }

    /**
     * Export search result list to file. Same permission required as
     * searching.
     *
     * @param organizationUniqueId
     * @param query the user query
     * @param statuses
     * @param include
     * @return
     */
    @GET
    @Path("export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"agreement:view_own"})
    public Response exportSearchAgreeements(
            @DefaultValue(value = "") @QueryParam("query") String query,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @QueryParam("status") List<Agreement.Status> statuses,
            @QueryParam("include") AgreementAPI.Include include) {
        LOG.log(Level.FINEST, "searchAgreeements( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        if( query != null && query.isEmpty() ) {
            query = null;
        }
        boolean onlyWhereContactPerson = false;
        if(include == AgreementAPI.Include.MINE) {
            onlyWhereContactPerson = true;
        }
        List<AgreementAPI> agreementAPIs = agreementController.searchAgreements(query,statuses, organizationUniqueId, userAPI, onlyWhereContactPerson, 0, Integer.MAX_VALUE);
        if( agreementAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                String filename = "Export_Avtal_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";
                byte[] exportBytes = resultExporterController.generateAgreementResultList(agreementAPIs);
                return Response.ok(
                        exportBytes,
                        jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                        .header("content-disposition", "attachment; filename = " + filename)
                        .build();
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to export agreements search to file", ex);
                return Response.serverError().build();
            }
        }
    }

    /**
     * Get information about a specific agreement
     *
     * @param httpServletRequest
     * @param organizationUniqueId the unique id of the organization to get agreement from
     * @param uniqueId the id of the agreement to return
     * @return the corresponding <code>AgreementAPI</code>
     */
    @GET
    @Path("{uniqueId : \\d+}") // the backslash backslash d+ part is to distinguish this path from another by saying it's only digits
    @SecuredService(permissions = {"agreement:view_own"})
    public Response getAgreement(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("uniqueId") long uniqueId ) {
        LOG.log(Level.FINEST, "getAgreement( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        AgreementAPI agreementAPI = agreementController.getAgreementAPI( organizationUniqueId, uniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest) );
        if( agreementAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(agreementAPI).build();
        }
    }

    /**
     * Create new agreement
     *
     * @param organizationUniqueId unique id of the organization to create agreement on
     * @param agreementAPI user supplied values
     * @return the created <code>AgreementAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException
     */
    @POST
    @SecuredService(permissions = {"agreement:create_own"})
    public Response createAgreement(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            AgreementAPI agreementAPI) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "createAgreement( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        agreementAPI = agreementController.createAgreement(organizationUniqueId, agreementAPI, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        return Response.ok(agreementAPI).build();
    }

    /**
     * Update an existing <code>Agreement</code>
     *
     * @param organizationUniqueId unique id of organization
     * @param uniqueId the id of the agreement to update
     * @param agreementAPI user supplied values
     * @return the updated <code>AgreementAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case validation fails
     */
    @PUT
    @Path("{uniqueId}")
    @SecuredService(permissions = {"agreement:update_own"})
    public Response updateAgreement(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("uniqueId") long uniqueId,
            AgreementAPI agreementAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateAgreement( uniqueId: {0} )", new Object[] {uniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        agreementAPI = agreementController.updateAgreement(organizationUniqueId, uniqueId, agreementAPI, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( agreementAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(agreementAPI).build();
        }
    }

    @GET
    @Path("pricelistapprovers")
    @SecuredService(permissions = {"agreement:view_own"})
    public Response searchPricelistApprovers(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @QueryParam("query") String query,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @QueryParam("exclude") List<Long> excludeUserEngagementIds,
            @QueryParam("businessLevel") List<Long> businessLevelIds) {
        LOG.log(Level.FINEST, "searchPricelistApprovers( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        SearchDTO searchDTO = agreementController.searchPricelistApprovers(organizationUniqueId, query, offset, 25, excludeUserEngagementIds, businessLevelIds);
        return Response.ok(searchDTO.getItems()).
                header("X-Total-Count", searchDTO.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }



    @DELETE
    @Path("{uniqueId}/delete")
    @SecuredService(permissions = {"agreement:delete_own"})
    public Response deleteAgreement (@Context HttpServletRequest httpServletRequest,
                                     @PathParam("organizationUniqueId") long organizationUniqueId,
                                     @PathParam("uniqueId") long agreementUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteAgreement( uniqueId: {0} )", new Object[]{agreementUniqueId});

        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if (!authorizeHandleOrganization(organizationUniqueId, userAPI)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        List<AgreementPricelistAPI> pricelistAPIs = pricelistController.getPricelistAPIsOnAgreement( organizationUniqueId, agreementUniqueId, userAPI );

        agreementController.deleteAgreement(pricelistAPIs, agreementUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest) );
        return Response.ok().build();
    }

    /**
     * Get pricelists where actions needed from suppliers or customers
     *
     */
    @GET
    @Path("getPricelistsFilteredBy/{filter}")
    @SecuredService(permissions = {"agreement:view_own"})
    public Response getPricelistsFilteredBy(@Context HttpServletRequest httpServletRequest,
                                                            @PathParam("organizationUniqueId") long organizationUniqueId, @PathParam("filter") String filter) {
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);

            List<Object> pricelistsAPI = pricelistController.getPricelistsFilteredBy(organizationUniqueId, userAPI, filter);



        return Response.ok(pricelistsAPI).build();

    }

    @GET
    @Path("getAgreementsToExtend")
    @SecuredService(permissions = {"agreement:view_own"})
    public Response getAgreementsExtensionReminder (@Context HttpServletRequest httpServletRequest,
                                                    @PathParam("organizationUniqueId") long organizationUniqueId) {
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);

        List<Object> agreementsAPI = pricelistController.getAgreementsExtensionReminder(organizationUniqueId, userAPI);

        return Response.ok(agreementsAPI).build();
    }

    @GET
    @Path("getAgreementsChangedByCustomer")
    @SecuredService(permissions = {"agreement:view_own"})
    public Response getAgreementsChangedByCustomer (@Context HttpServletRequest httpServletRequest,
                                                    @PathParam("organizationUniqueId") long organizationUniqueId) {
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);

        List<Object> changedAgreementsAPI = agreementController.getAgreementsChangedByCustomer(userAPI);

        return Response.ok(changedAgreementsAPI).build();
    }

    @GET
    @Path("getPricelistRowsApprovedByCustomer")
    @SecuredService(permissions = {"agreement:view_own"})
    public Response getPricelistRowsApprovedByCustomer (@Context HttpServletRequest httpServletRequest,
                                                    @PathParam("organizationUniqueId") long organizationUniqueId) {
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);

        List<Object> approvedPricelistRowsAPI = agreementController.getPricelistRowsApprovedByCustomer(userAPI);

        return Response.ok(approvedPricelistRowsAPI).build();
    }

    @DELETE
    @Path("removeAgreementChangedNotice/{agreementChangedId}")
    @SecuredService(permissions = {"agreement:view_own"})
    public Response removeAgreementChangedNotice (@Context HttpServletRequest httpServletRequest,
                                                    @PathParam("organizationUniqueId") long organizationUniqueId, @PathParam("agreementChangedId") Long agreementChangedId) {
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);

        List<ChangeChecker> changedAgreementsAPI = agreementController.removeAgreementChangedNotice(agreementChangedId, userAPI.getId());

        return Response.ok(changedAgreementsAPI).build();
    }

    /**
     * Get information about which articles been changed by supplier, and is on agreements where logged in user is pricelistApprover
     * HJAL-2386
     *
     * @param organizationUniqueId the unique id of the organization
     * @return a <code>ArticlesChanged</code> list of articles changed by suppliers
     */
    @GET
    @Path("getArticlesChanged")
    @SecuredService(permissions = {"agreement:view_own"})
    public Response getArticlesChanged(
            @PathParam("organizationUniqueId") long organizationUniqueId) {
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        List<ArticleChanged> changedArticles = articleController.getArticlesChanged(userAPI);
        if( changedArticles.size() == 0 ) {
            return null;
        } else {
            return Response.ok(changedArticles).build();
        }

    }

    /**
     * Delete from changechecker, the user that has visited the changed article
     * HJAL-2386
     *
     * @param organizationUniqueId the unique id of the organization
     * @param userId userId
     * @param articleChangedId articleChanged unique Id which is saved as changeId in ChangeChecker
     */
    @DELETE
    @Path("removeArticleChangedNotice/{articleChangedId}/{userId}/{removeAll}")
    @SecuredService(permissions = {"agreement:view_own"})
    public Response removeArticleChangedNotice(
            @PathParam("articleChangedId") long articleChangedId, @PathParam("organizationUniqueId") long organizationUniqueId, @PathParam("userId") long userId, @PathParam("removeAll") boolean removeAll
            ) {

        List<ChangeChecker> cc = articleController.removeArticleChangedNotice(articleChangedId, userId, removeAll);

        return Response.ok().build();

    }

    @DELETE
    @Path("removeApprovedPricelistRowNotice/{approvementId}")
    @SecuredService(permissions = {"agreement:view_own"})
    public Response removeApprovedPricelistRowNotice (@Context HttpServletRequest httpServletRequest,
                                                      @PathParam("organizationUniqueId") long organizationUniqueId, @PathParam("approvementId") Long approvementId) {
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);

        List<ChangeChecker> approvementAPI = agreementController.removeApprovedPricelistRowNotice(approvementId, userAPI.getId());

        return Response.ok(approvementAPI).build();
    }

}
