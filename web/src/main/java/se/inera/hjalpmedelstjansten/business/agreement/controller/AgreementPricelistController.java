package se.inera.hjalpmedelstjansten.business.agreement.controller;

import se.inera.hjalpmedelstjansten.business.BaseController;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.organization.controller.BusinessLevelController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelist;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import jakarta.ejb.Stateless;
import jakarta.enterprise.event.Event;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

/**
 * Class for handling business logic of Pricelists on Agreements. This includes talking to the
 * database.
 *
 */
@Stateless
public class AgreementPricelistController extends BaseController {

    @Inject
    HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    AgreementPricelistValidation pricelistValidation;

    @Inject
    AgreementPricelistMapper agreementPricelistMapper;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    OrganizationController organizationController;

    @Inject
    BusinessLevelController businessLevelController;

    @Inject
    UserController userController;

    @Inject
    AgreementController agreementController;

    @Inject
    ElasticSearchController elasticSearchController;

    @Inject
    Event<InternalAuditEvent> internalAuditEvent;

    @Inject
    String defaultPricelistNumber;

    /**
     * Get the List of <code>PricelistAPI</code> for the agreement
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param userAPI logged in user information
     * @return the corresponding List of <code>AgreementPricelistAPI</code>
     */
    public List<AgreementPricelistAPI> getPricelistAPIsOnAgreement(long organizationUniqueId, long agreementUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getPricelistAPIsOnAgreement( organizationUniqueId: {0}, agreementUniqueId: {1} )", new Object[] {organizationUniqueId, agreementUniqueId});
        List<AgreementPricelist> pricelists = getPricelistsOnAgreement(organizationUniqueId, agreementUniqueId, userAPI);
        if( pricelists != null ) {
            AgreementPricelist currentPricelist = getCurrentPricelist( agreementUniqueId );
            List<AgreementPricelistAPI> agreementPricelistAPIs = AgreementPricelistMapper.map( pricelists, false, currentPricelist);
            if( agreementPricelistAPIs != null ) {
                for( AgreementPricelistAPI agreementPricelistAPI : agreementPricelistAPIs ) {
                    long numberOfRowsOnPricelist = getNumberOfRowsOnPricelist(agreementPricelistAPI.getId());
                    agreementPricelistAPI.setHasPricelistRows(numberOfRowsOnPricelist > 0);
                }
            }
            return agreementPricelistAPIs;
        }
        return null;
    }

    /**
     * Get the List of <code>PricelistAPI</code> for the agreement where specific article can be added
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param userAPI logged in user information
     * @param articleUniqueId unique id of the agreement
     * @return the corresponding List of <code>AgreementPricelistAPI</code>
     */
    public List<AgreementPricelistAPI> getSelectablePricelistAPIsOnAgreement(long organizationUniqueId, long agreementUniqueId, UserAPI userAPI, long articleUniqueId) {
        LOG.log(Level.FINEST, "getSelectablePricelistAPIsOnAgreement( organizationUniqueId: {0}, agreementUniqueId: {1}, articleUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, articleUniqueId});
        List<AgreementPricelist> pricelists = getSelectablePricelistsOnAgreement(organizationUniqueId, agreementUniqueId, userAPI, articleUniqueId);
        if( pricelists != null ) {
            AgreementPricelist currentPricelist = getCurrentPricelist( agreementUniqueId );
            List<AgreementPricelistAPI> agreementPricelistAPIs = AgreementPricelistMapper.map( pricelists, false, currentPricelist);
            if( agreementPricelistAPIs != null ) {
                for( AgreementPricelistAPI agreementPricelistAPI : agreementPricelistAPIs ) {
                    long numberOfRowsOnPricelist = getNumberOfRowsOnPricelist(agreementPricelistAPI.getId());
                    agreementPricelistAPI.setHasPricelistRows(numberOfRowsOnPricelist > 0);
                }
            }
            return agreementPricelistAPIs;
        }
        return null;
    }


    /**
     * Get all pricelists on the given agreement.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param userAPI logged in user information
     * @return the corresponding List of <code>AgreementPricelist</code>
     */
    private List<AgreementPricelist> getPricelistsOnAgreement(long organizationUniqueId, long agreementUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getPricelistsOnAgreement( organizationUniqueId: {0}, agreementUniqueId: {1} )", new Object[] {organizationUniqueId, agreementUniqueId});
        // make sure user can fetch agreement
        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);
        if( agreement != null ) {
            List<AgreementPricelist> pricelists = em.createNamedQuery(AgreementPricelist.GET_ALL_BY_AGREEMENT).
                    setParameter("agreementUniqueId", agreementUniqueId).
                    getResultList();
            if( pricelists != null ) {
                return pricelists;
            }
        } else {
            LOG.log( Level.WARNING, "Attempt by user: {0} to fetch pricelists on agreement: {1} but the agreement is not visible to the user", new Object[] {userAPI.getId(), agreementUniqueId});
        }
        return null;
    }

    /**
     * Get all pricelists on the given agreement where specific article can be added
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param userAPI logged in user information
     * @param articleUniqueId unique id of the article
     * @return the corresponding List of <code>AgreementPricelist</code>
     */
    private List<AgreementPricelist> getSelectablePricelistsOnAgreement(long organizationUniqueId, long agreementUniqueId, UserAPI userAPI, long articleUniqueId) {
        LOG.log(Level.FINEST, "getSelectablePricelistsOnAgreement( organizationUniqueId: {0}, agreementUniqueId: {1}, articleUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, articleUniqueId});
        // make sure user can fetch agreement
        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);
        if( agreement != null ) {
            List<AgreementPricelist> pricelists = em
                    .createNativeQuery("SELECT ap.* FROM hjmtj.AgreementPricelist ap WHERE agreementId = :agreementUniqueId AND ap.uniqueId NOT IN (SELECT agreementPricelistId FROM hjmtj.AgreementPricelistRow WHERE articleId = :articleUniqueId)", AgreementPricelist.class)
                    .setParameter("agreementUniqueId", agreementUniqueId)
                    .setParameter("articleUniqueId", articleUniqueId)
                    .getResultList();
            if( pricelists != null ) {
                return pricelists;
            }
        } else {
            LOG.log( Level.WARNING, "Attempt by user: {0} to fetch pricelists on agreement: {1} but the agreement is not visible to the user", new Object[] {userAPI.getId(), agreementUniqueId});
        }
        return null;
    }

    /**
     * Get the <code>PricelistAPI</code> for the given unique id on the
     * specified organization
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist to get
     * @param userAPI user information
     * @return the corresponding <code>AgreementAPI</code>
     */
    public AgreementPricelistAPI getPricelistAPI(long organizationUniqueId,
            long agreementUniqueId,
            long pricelistUniqueId,
            UserAPI userAPI,
            String sessionId,
            String requestIp) {
        LOG.log(Level.FINEST, "getPricelistAPI( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        AgreementPricelist pricelist = getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist != null ) {
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST, InternalAudit.ActionType.VIEW, userAPI.getId(), sessionId, pricelistUniqueId, requestIp));
            AgreementPricelist currentPricelist = getCurrentPricelist( agreementUniqueId );
            return AgreementPricelistMapper.mapWithPricelist(pricelist, true, currentPricelist);
        }
        return null;
    }

    /**
     * Search available articles for pricelist, meaning all actice articles on organisation
     * that is not already in the pricelist
     *
     * @param organizationUniqueId
     * @param agreementUniqueId
     * @param pricelistUniqueId
     * @param query
     * @param statuses
     * @param articleTypes
     * @param offset
     * @param limit
     * @param userAPI
     * @return
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
     */
    public SearchDTO searchArticlesForAgreementPricelist(long organizationUniqueId,
            long agreementUniqueId,
            long pricelistUniqueId,
            String query,
            List<Product.Status> statuses,
            List<Article.Type> articleTypes,
            int offset,
            int limit,
            UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "searchArticlesForAgreementPricelist( organizationUniqueId: {0}, statuses: {1}, articleTypes: {2}, offset: {3}, limit: {4} )", new Object[] {organizationUniqueId, statuses, articleTypes, offset, limit});
        AgreementPricelist pricelist = getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist != null ) {
            List<Long> articlesAlreadyInPricelist = em.createQuery("SELECT agp.article.uniqueId FROM AgreementPricelistRow agp WHERE agp.pricelist.uniqueId = :agreementPricelistId").
                    setParameter("agreementPricelistId", pricelistUniqueId)
                    .getResultList();

            Product.Status status = null;
            if (statuses != null && statuses.size() == 1) {
                status = statuses.get(0);
            }

            return elasticSearchController.searchOrganization(query,
                    organizationUniqueId,
                    false,
                    true,
                    "",
                    "",
                    articleTypes,
                    articlesAlreadyInPricelist,
                    status,
                    25,
                    offset,
                    false);
        }
        return null;
    }

    /**
     * Get the <code>Pricelist</code> with the given id on the specified organization.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param userAPI user information
     * @return the corresponding <code>Pricelist</code>
     */
    public AgreementPricelist getPricelist(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getPricelist( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        AgreementPricelist pricelist = em.find(AgreementPricelist.class, pricelistUniqueId);
        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist: {1} that does not exist. Returning 404.", new Object[] {userAPI.getId(), pricelistUniqueId});
            return null;
        }
        // make sure user can fetch agreement connected to pricelist
        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);
        if( agreement == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist: {1}, but agreement: {2} is not available. Returning 404.", new Object[] {userAPI.getId(), pricelistUniqueId, agreementUniqueId});
            return null;
        }
        if( !pricelist.getAgreement().getUniqueId().equals(agreement.getUniqueId()) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist: {1} not on given agreement: {2}. Returning 404.", new Object[] {userAPI.getId(), pricelistUniqueId, agreement.getUniqueId()});
            return null;
        }
        return pricelist;
    }

    /**
     * Proposal of updated general version of method fetching pricelists (only for dashboard?),
     * filtered by pricelist row status, depending on CUSTOMER, SUPPLIER, SERVICE_OWNER.
     *
     *
     * @param organizationUniqueId The uniqueId for the organization to fetch agreements for. (CUSTOMER/SUPPLIER/SERVICE_OWNER)
     * @param userAPI The UserAPI Object (?)
     * @param filterStrings A String of agreementPriceListRows(or something?) statuses we want to sort from. (?)
     * @return A list of general objects corresponding to the PriceLists. (?)
     */
    public List<Object> getPricelistsFilteredBy(Long organizationUniqueId, UserAPI userAPI, String filterStrings) {
        final Long userId = userAPI.getId();
        final Organization organization = organizationController.getOrganizationFromLoggedInUser(userAPI);
        final String filteredBy = filterStrings;
        String sql = null;
        switch (organization.getOrganizationType()) {
            case CUSTOMER:
                if (filteredBy.equals("PENDING_APPROVAL")) {
                    //prislistor med rader som ska godkännas
                    //hämta avtalsId, prislistId, avtalsnamn, avtalsnummer, prislistenummer(inte id), leverantörsId, verksamhetsområde
                    sql = "SELECT ap.agreementId, ap.uniqueId, a.agreementName, a.agreementNumber, ap.number, a.supplierOrganizationId, o.organizationName, b.name, a.customerBusinessLevelId, max(apr.lastUpdated)";
                    sql += " FROM hjmtj.Agreement a";
                    sql += " INNER JOIN hjmtj.AgreementPricelist as ap ON ap.agreementId = a.uniqueId";
                    sql += " INNER JOIN hjmtj.AgreementPricelistRow as apr ON apr.agreementPricelistId = ap.uniqueId";
                    sql += " INNER JOIN hjmtj.Organization o ON a.supplierOrganizationId = o.uniqueId";
                    sql += " LEFT JOIN hjmtj.BusinessLevel b ON a.customerBusinessLevelId = b.uniqueId";
                    sql += " INNER JOIN hjmtj.AgreementUserEngagementCustomer aec ON a.uniqueId = aec.agreementId";
                    sql += " INNER JOIN hjmtj.UserEngagement ue ON aec.userEngagementId = ue.uniqueId";
                    sql += " WHERE a.customerOrganizationId = " + organizationUniqueId;
                    sql += " AND a.status IN ('CURRENT', 'FUTURE')";
                    sql += " AND apr.status IN ('PENDING_APPROVAL')";
                    sql += " AND ap.uniqueId IN (SELECT a1.uniqueId FROM hjmtj.AgreementPricelist a1 INNER JOIN hjmtj.AgreementPricelist a2 ON a1.uniqueID = a2.uniqueId";
                    sql += " WHERE a1.validFrom > current_date() OR a1.validFrom = (SELECT max(a2.validFrom) FROM hjmtj.AgreementPricelist a2 WHERE a1.agreementId = a2.agreementId AND validFrom <= current_date()))";
                    sql += " AND ue.uniqueId = " + userId;
                    sql += " GROUP BY ap.uniqueId";
                }
                if (filteredBy.equals("PENDING_INACTIVATION")) {
                    //prislistor med rader som ska godkännas för inaktivering
                    //hämta avtalsId, prislistId, avtalsnamn, avtalsnummer, prislistenummer(inte id), leverantörsId, verksamhetsområde
                    sql = "SELECT ap.agreementId, ap.uniqueId, a.agreementName, a.agreementNumber, ap.number, a.supplierOrganizationId, o.organizationName, b.name, a.customerBusinessLevelId, max(apr.lastUpdated)";
                    sql += " FROM hjmtj.Agreement a";
                    sql += " INNER JOIN hjmtj.AgreementPricelist as ap ON ap.agreementId = a.uniqueId";
                    sql += " INNER JOIN hjmtj.AgreementPricelistRow as apr ON apr.agreementPricelistId = ap.uniqueId";
                    sql += " INNER JOIN hjmtj.Organization o ON a.supplierOrganizationId = o.uniqueId";
                    sql += " LEFT JOIN hjmtj.BusinessLevel b ON a.customerBusinessLevelId = b.uniqueId";
                    sql += " INNER JOIN hjmtj.AgreementUserEngagementCustomer aec ON a.uniqueId = aec.agreementId";
                    sql += " INNER JOIN hjmtj.UserEngagement ue ON aec.userEngagementId = ue.uniqueId";
                    sql += " WHERE a.customerOrganizationId = " + organizationUniqueId;
                    sql += " AND a.status IN ('CURRENT', 'FUTURE')";
                    sql += " AND apr.status IN ('PENDING_INACTIVATION')";
                    sql += " AND ap.uniqueId IN (SELECT a1.uniqueId FROM hjmtj.AgreementPricelist a1 INNER JOIN hjmtj.AgreementPricelist a2 ON a1.uniqueID = a2.uniqueId";
                    sql += " WHERE a1.validFrom > current_date() OR a1.validFrom = (SELECT max(a2.validFrom) FROM hjmtj.AgreementPricelist a2 WHERE a1.agreementId = a2.agreementId AND validFrom <= current_date()))";
                    sql += " AND ue.uniqueId = " + userId;
                    sql += " GROUP BY ap.uniqueId";
                }

                break;
            case SUPPLIER:
                if(filteredBy.equals("EMPTY_PRICELISTS")) {
                    //avtal med tomma prislistor
                    //hämta avtalsId, prislistId, avtalsnamn, avtalsnummer, prislistenummer(inte id) kundId, verksamhetsområde

                    sql = "SELECT ap.agreementId, ap.uniqueId as emptyPricelists, a.agreementName, a.agreementNumber, ap.number as pricelistNumber, a.customerOrganizationId, o.organizationName, b.name, a.customerBusinessLevelId as businessLevelId, ap.created ";
                    sql += "FROM hjmtj.AgreementPricelist ap ";
                    sql += "INNER JOIN hjmtj.Agreement a ON ap.agreementId = a.uniqueId ";
                    sql += "INNER JOIN hjmtj.Organization o ON a.customerOrganizationId = o.uniqueId ";
                    sql += "LEFT JOIN hjmtj.BusinessLevel b ON a.customerBusinessLevelId = b.uniqueId ";
                    sql += "WHERE (a.status = 'CURRENT' OR a.status = 'FUTURE') AND ";
                    sql += "ap.uniqueId NOT IN (SELECT agreementPricelistId FROM hjmtj.AgreementPricelistRow) AND ";
                    sql += "ap.uniqueId IN (SELECT a1.uniqueId FROM hjmtj.AgreementPricelist a1 INNER JOIN hjmtj.AgreementPricelist a2 ON a1.uniqueID = a2.uniqueId ";
                    sql += "WHERE a1.validFrom > current_date() OR a1.validFrom = (SELECT max(validFrom) FROM hjmtj.AgreementPricelist a1 WHERE a1.agreementId = a2.agreementId AND validFrom <= current_date())) AND ";
                    sql += "a.supplierOrganizationId = " + organizationUniqueId;
                }
                if(filteredBy.equals("DECLINED")) {
                    //pricelists with declined rows
                    //hämta avtalsId, prislistId, avtalsnamn, avtalsnummer, prislistenummer(inte id) kundId, verksamhetsområde

                    sql = "SELECT ap.agreementId, apr.agreementPricelistId, a.agreementName, a.agreementNumber, ap.number as pricelistNumber, a.customerOrganizationId, o.organizationName as customerName, b.name as businessLevel, ap.validFrom, max(apr.lastUpdated) FROM hjmtj.Agreement a";
                    sql += " INNER JOIN hjmtj.Organization o ON a.customerOrganizationId = o.uniqueId";
                    sql += " INNER JOIN hjmtj.AgreementPricelist ap ON ap.agreementId = a.uniqueId";
                    sql += " INNER JOIN hjmtj.AgreementPricelistRow apr ON apr.agreementPricelistId = ap.uniqueId";
                    sql += " LEFT JOIN hjmtj.BusinessLevel b ON b.uniqueId = a.customerBusinessLevelId";
                    sql += " WHERE apr.status = 'DECLINED'";
                    sql += " AND a.supplierOrganizationId = " + organizationUniqueId;
                    sql += " AND a.status IN ('CURRENT', 'FUTURE')";
                    sql += " AND ap.uniqueId IN";
                    sql += " (SELECT a1.uniqueId FROM hjmtj.AgreementPricelist a1 INNER JOIN hjmtj.AgreementPricelist a2 ON a1.uniqueID = a2.uniqueId";
                    sql += " WHERE a1.validFrom > current_date() OR a1.validFrom = (SELECT max(a2.validFrom)";
                    sql += " FROM hjmtj.AgreementPricelist a2 WHERE a2.validFrom <= current_date() AND a1.agreementId = a2.agreementId ))";
                    sql += " GROUP BY ap.uniqueId";
                }
                if (filteredBy.equals("NEW_OR_CHANGED")) {
                    //prio 1.2 att hitta avtal med prislisterader som är skapad eller ändrad (Status New/Changed), bara i avtal med status CURRENT & FUTURE, samt endast från prislistor som är aktuella eller framtida.
//                    sql = "SELECT ap.agreementId, ap.uniqueId as pricelistIdWithNewOrChangedRows, a.agreementName, a.agreementNumber, ap.number as pricelistNumber, a.customerOrganizationId, o.organizationName, b.name, a.customerBusinessLevelId as businessLevelId\n\n" +
//                            "FROM hjmtj.Agreement as a\n" +
//                            "INNER JOIN hjmtj.AgreementPricelist as ap ON ap.agreementId = a.uniqueId\n" +
//                            "INNER JOIN hjmtj.AgreementPricelistRow as apr ON apr.agreementPricelistId = ap.uniqueId\n" +
//                            "INNER JOIN hjmtj.Organization o ON a.customerOrganizationId = o.uniqueId\n" +
//                            "LEFT JOIN hjmtj.BusinessLevel b ON a.customerBusinessLevelId = b.uniqueId\n" +
//                            "WHERE a.supplierOrganizationId = " + organizationUniqueId + " AND (apr.status = 'CREATED' OR apr.status = 'CHANGED') GROUP BY ap.uniqueId";

                    sql = "SELECT ap.agreementId, ap.uniqueId as pricelistIdWithNewOrChangedRows, a.agreementName, a.agreementNumber, ap.number as pricelistNumber, a.customerOrganizationId, o.organizationName, b.name, a.customerBusinessLevelId as businessLevelId, max(apr.lastUpdated)\n" +
                        "FROM hjmtj.Agreement as a\n" +
                        "INNER JOIN hjmtj.AgreementPricelist as ap ON ap.agreementId = a.uniqueId\n" +
                        "INNER JOIN hjmtj.AgreementPricelistRow as apr ON apr.agreementPricelistId = ap.uniqueId\n" +
                        "INNER JOIN hjmtj.Organization o ON a.customerOrganizationId = o.uniqueId\n" +
                        "LEFT JOIN hjmtj.BusinessLevel b ON a.customerBusinessLevelId = b.uniqueId\n" +
                        "WHERE a.supplierOrganizationId = " + organizationUniqueId + " AND (apr.status = 'CREATED' OR apr.status = 'CHANGED') \n" +
                        "AND a.status IN ('CURRENT', 'FUTURE')\n" +
                        "AND ap.uniqueId IN (SELECT a1.uniqueId FROM hjmtj.AgreementPricelist a1 INNER JOIN hjmtj.AgreementPricelist a2 ON a1.uniqueID = a2.uniqueId\n" +
                        "WHERE a1.validFrom > current_date() OR a1.validFrom = (SELECT max(a2.validFrom) \n" +
                        "FROM hjmtj.AgreementPricelist a2 WHERE a2.validFrom <= current_date() AND a1.agreementId = a2.agreementId ))\n" +
                        "GROUP BY ap.uniqueId";
                }
                if (filteredBy.equals("PENDING_APPROVAL")) {
                    //avtal med prislisterader som är PENDING APPROVAL, inte än godkänt av kund, bara i avtal med status CURRENT & FUTURE, samt endast från prislistor som är aktuella eller framtida.
                    //hämta avtalsId, prislistId, avtalsnamn, avtalsnummer, prislistenummer(inte id) kundId, verksamhetsområde

//                    sql = "SELECT ap.agreementId, ap.uniqueId as pricelistIdWithpendingRows, a.agreementName, a.agreementNumber, ap.number as pricelistNumber, a.customerOrganizationId, o.organizationName, b.name, a.customerBusinessLevelId as businessLevelId\n\n" +
//                            "FROM hjmtj.Agreement as a\n" +
//                            "INNER JOIN hjmtj.AgreementPricelist as ap ON ap.agreementId = a.uniqueId\n" +
//                            "INNER JOIN hjmtj.AgreementPricelistRow as apr ON apr.agreementPricelistId = ap.uniqueId\n" +
//                            "INNER JOIN hjmtj.Organization o ON a.customerOrganizationId = o.uniqueId\n" +
//                            "LEFT JOIN hjmtj.BusinessLevel b ON a.customerBusinessLevelId = b.uniqueId\n" +
//                            "WHERE a.supplierOrganizationId = " + organizationUniqueId + " AND apr.status = 'PENDING_APPROVAL' GROUP BY ap.uniqueId";

                    sql = "SELECT ap.agreementId, ap.uniqueId as pricelistIdWithpendingRows, a.agreementName, a.agreementNumber, ap.number as pricelistNumber, a.customerOrganizationId, o.organizationName, b.name, a.customerBusinessLevelId as businessLevelId, max(apr.lastUpdated)\n" +
                        " FROM hjmtj.Agreement as a\n" +
                        " INNER JOIN hjmtj.AgreementPricelist as ap ON ap.agreementId = a.uniqueId\n" +
                        " INNER JOIN hjmtj.AgreementPricelistRow as apr ON apr.agreementPricelistId = ap.uniqueId\n" +
                        " INNER JOIN hjmtj.Organization o ON a.customerOrganizationId = o.uniqueId\n" +
                        " LEFT JOIN hjmtj.BusinessLevel b ON a.customerBusinessLevelId = b.uniqueId\n" +
                        " WHERE a.supplierOrganizationId = " + organizationUniqueId + " AND a.status IN ('CURRENT', 'FUTURE')\n" +
                        " AND apr.status = 'PENDING_APPROVAL' \n" +
                        " AND ap.uniqueId IN (SELECT a1.uniqueId FROM hjmtj.AgreementPricelist a1 INNER JOIN hjmtj.AgreementPricelist a2 ON a1.uniqueID = a2.uniqueId\n" +
                        " WHERE a1.validFrom > current_date() OR a1.validFrom = (SELECT max(a2.validFrom) \n" +
                        " FROM hjmtj.AgreementPricelist a2 WHERE a2.validFrom <= current_date() AND a1.agreementId = a2.agreementId ))\n" +
                        " GROUP BY ap.uniqueId";
                }
                break;
            case SERVICE_OWNER:
                break;
        }
        // FIXME : Investigate if we can or should use .createNativeQuery(sql, class) for type safety.
        //nedanstående rad ger nullpointerexception
        //List<Object> pricelistsAPI = em.createNativeQuery(sql).getResultList();
        //if( pricelistsAPI.size() == 0 ) {
        //    return null;
        //} else {
        //    return pricelistsAPI;
        //}
        //lägger in detta istället
        jakarta.persistence.Query query  = em.createNativeQuery(sql);
        return (query == null) ? null : query.getResultList();
    }

    /**
     * Get Agreements where sendreminder date is today or passed
     *
     * TODO: Should also probably be checked/pre-cast into a list of Agreements instead of Object for type safety.
     *
     * @param organizationUniqueId The uniqueId for the organization to fetch agreements for. (CUSTOMER/SUPPLIER/SERVICE_OWNER)
     * @return A list of general objects corresponding to the Agreements.
     */

    public List<Object> getAgreementsExtensionReminder(Long organizationUniqueId, UserAPI userAPI) {

        final Long userId = userAPI.getId();

        String sql = "SELECT a.uniqueId, a.agreementName, a.agreementNumber, a.supplierOrganizationId, o.organizationName, b.name, a.customerBusinessLevelId, a.sendReminderOn FROM hjmtj.Agreement as a \n" +
                "INNER JOIN hjmtj.Organization o ON a.supplierOrganizationId = o.uniqueId \n" +
                "LEFT JOIN hjmtj.BusinessLevel b ON a.customerBusinessLevelId = b.uniqueId\n" +
                "INNER JOIN hjmtj.AgreementUserEngagementCustomer aec ON a.uniqueId = aec.agreementId\n" +
                "INNER JOIN hjmtj.UserEngagement ue ON aec.userEngagementId = ue.uniqueId\n" +
                "WHERE a.status IN ('CURRENT', 'FUTURE') AND a.customerOrganizationId = " + organizationUniqueId + " AND a.sendReminderOn <= current_date() AND ue.uniqueId =" + userId + ";";

        // FIXME : Investigate if we can or should use .createNativeQuery(sql, class) for type safety.
        List<Object> agreementsAPI = em.createNativeQuery(sql).getResultList();
        if( agreementsAPI.size() == 0 ) {
            return null;
        } else {
            return agreementsAPI;
        }
    }

    /**
     * When an agreement is created, one pricelist should be created by default.
     *
     * @param organizationUniqueId
     * @param agreement the created agreement
     * @param userAPI
     * @param sessionId
     * @return the created <code>PricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public AgreementPricelistAPI createFirstPricelistForAgreement( long organizationUniqueId, Agreement agreement, UserAPI userAPI, String sessionId, String requestIp ) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createFirstPricelistForAgreement( agreement: {0} )", new Object[] {agreement.getUniqueId()});
        AgreementPricelistAPI pricelistAPI = new AgreementPricelistAPI();
        pricelistAPI.setNumber(defaultPricelistNumber);
        pricelistAPI.setValidFrom(agreement.getValidFrom().getTime());
        return createPricelist(organizationUniqueId, agreement, pricelistAPI, userAPI, sessionId, requestIp);
    }

    public AgreementPricelistAPI updateFirstPricelistForAgreement( long organizationUniqueId, Agreement agreement, UserAPI userAPI, String sessionId, String requestIp ) throws HjalpmedelstjanstenValidationException {
        //hämta id för minsta uniqueId för att få ut den först skapade (kunden kan ha döpt om den från 001 till nåt annat)
        Long minId = 0L;
        int i = 0;
        for (AgreementPricelistAPI plapi : getPricelistAPIsOnAgreement(organizationUniqueId, agreement.getUniqueId(), userAPI)){
            minId = (i == 0)? plapi.getId() : (plapi.getId() < minId) ? plapi.getId() : minId;
            i++;
        }
        AgreementPricelistAPI agreementPricelistAPI = getPricelistAPI(organizationUniqueId, agreement.getUniqueId(), minId, userAPI, sessionId, requestIp);
        agreementPricelistAPI.setValidFrom(agreement.getValidFrom().getTime());
        return updatePricelist(organizationUniqueId, agreement.getUniqueId(), minId, agreementPricelistAPI, userAPI, sessionId);
    }

    /**
     * Create a pricelist based on the supplied data
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param pricelistAPI user supplied data
     * @param userAPI logged in user's session information
     * @param sessionId
     * @return the created <code>PricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public AgreementPricelistAPI createPricelist( long organizationUniqueId, long agreementUniqueId, AgreementPricelistAPI pricelistAPI, UserAPI userAPI, String sessionId, String requestIp ) throws HjalpmedelstjanstenValidationException {
        //TODO mail here aswell HJAL-95?
        LOG.log(Level.FINEST, "createPricelist( organizationUniqueId: {0}, agreementUniqueId: {1} )", new Object[]{organizationUniqueId, agreementUniqueId});
        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);

        List<UserAPI> supplierAgreementManager = null;
        Organization customerOrganization = null;
        if (userController != null){
            List<UserRole.RoleName> roleNames = new ArrayList<>();
            roleNames.add(UserRole.RoleName.SupplierAgreementManager);
            supplierAgreementManager = userController.getUsersInRolesOnOrganization(agreement.getSupplier().getUniqueId(), roleNames);
            customerOrganization = organizationController.getOrganization(organizationUniqueId);
        }
        AgreementPricelistAPI agreementPricelistAPI = createPricelist(organizationUniqueId, agreement, pricelistAPI, userAPI, sessionId, requestIp);
        if(customerOrganization != null && supplierAgreementManager != null) {
            agreementController.sendNotificationPricelistAddedMail(supplierAgreementManager, customerOrganization.getOrganizationName(), agreement.getAgreementNumber(), agreementPricelistAPI.getNumber());
        }
        return agreementPricelistAPI;
    }

    /**
     * Create a pricelist based on the supplied data
     *
     * @param agreement the agreement of the pricelist
     * @param pricelistAPI user supplied data
     * @return the created <code>PricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    private AgreementPricelistAPI createPricelist( long organizationUniqueId,
            Agreement agreement,
            AgreementPricelistAPI pricelistAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createPricelist( agreement: {0} )", new Object[] {agreement.getUniqueId()});

        // organizations/business levels (customers) that the agreement is shared
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to create agreement pricelist: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }

        pricelistValidation.validateForCreate(pricelistAPI, agreement);
        AgreementPricelist pricelist = AgreementPricelistMapper.map(pricelistAPI);
        // pricelists valid from must always be at least one day in the future
        //pricelist.setStatus(Pricelist.Status.FUTURE);
        pricelist.setAgreement(agreement);
        em.persist(pricelist);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, pricelist.getUniqueId(), requestIp));
        AgreementPricelist currentPricelist = getCurrentPricelist( agreement.getUniqueId() );
        return AgreementPricelistMapper.mapWithPricelist(pricelist, true, currentPricelist);
    }

    /**
     * Update a pricelist based on the supplied data
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param pricelistUniqueId unique id of the pricelist
     * @param agreementPricelistAPI user supplied data
     * @param userAPI logged in user's session information
     * @param sessionId
     * @return the created <code>PricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public AgreementPricelistAPI updatePricelist(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, AgreementPricelistAPI agreementPricelistAPI, UserAPI userAPI, String sessionId) throws HjalpmedelstjanstenValidationException {
        AgreementPricelist agreementPricelist = getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( agreementPricelist == null ) {
            return null;
        }

        // organizations/business levels (customers) that the agreement is shared
        // with can read the agreement, but only the customer can edit it
        if( !agreementPricelist.getAgreement().getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to edit agreement pricelist: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreementPricelist.getAgreement().getUniqueId(), organizationUniqueId});
            return null;
        }

        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);
        AgreementPricelist currentPricelist = getCurrentPricelist( agreement.getUniqueId() );
        AgreementPricelist.Status status = AgreementPricelistMapper.getPricelistStatus(agreementPricelist, currentPricelist);

        // future pricelists can always be modified
        //HJAL-2117
        /*
        if( status != AgreementPricelist.Status.FUTURE ) {
            // no rows may exist on pricelist for update to be valid
            long numberOfRowsOnAgreement = getNumberOfRowsOnPricelist(pricelistUniqueId);
            if( numberOfRowsOnAgreement > 0 ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to update pricelist: {1} but rows already exist in pricelist", new Object[] {userAPI.getId(), pricelistUniqueId});
                throw validationMessageService.generateValidationException("rows", "pricelist.update.rowsExist");
            }
        }
        */

        pricelistValidation.validateForUpdate(agreementPricelistAPI, agreementPricelist, agreement);
        agreementPricelist.setNumber(agreementPricelistAPI.getNumber());
        agreementPricelist.setValidFrom(new Date(agreementPricelistAPI.getValidFrom()));
        return AgreementPricelistMapper.mapWithPricelist(agreementPricelist, true, currentPricelist);
    }


    /**
     * Get the total number of rows in pricelist
     *
     * @param pricelistUniqueId the unique id of the pricelist
     * @return the number of rows in the pricelist
     */
    private long getNumberOfRowsOnPricelist(long pricelistUniqueId) {
        Long count = (Long) em.createNamedQuery(AgreementPricelistRow.COUNT_BY_PRICELIST).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                getSingleResult();
        return count == null ? 0: count;
    }


    /**
     * Find pricelists on a specific agreement with given validFrom
     *
     * @param validFrom
     * @param agreementUniqueId unique id of the agreement
     * @return
     */
    public List<AgreementPricelist> findByValidFromAndAgreement(long validFrom, long agreementUniqueId) {
        LOG.log(Level.FINEST, "findByValidFromAndAgreement( agreementUniqueId: {0} )", new Object[] {agreementUniqueId} );
        Date date = new Date(validFrom);
        return em.createNamedQuery(AgreementPricelist.FIND_BY_VALID_FROM_AND_AGREEMENT).
                setParameter("agreementUniqueId", agreementUniqueId).
                setParameter("validFrom", date).
                getResultList();
    }

    /**
     * Find pricelists on a specific agreement with given number
     *
     * @param number
     * @param agreementUniqueId unique id of the agreement
     * @return
     */
    public List<AgreementPricelist> findByNumberAndAgreement(String number, long agreementUniqueId) {
        LOG.log(Level.FINEST, "findByNumberAndAgreement( agreementUniqueId: {0} )", new Object[] {agreementUniqueId} );
        return em.createNamedQuery(AgreementPricelist.FIND_BY_NUMBER_AND_AGREEMENT).
                setParameter("agreementUniqueId", agreementUniqueId).
                setParameter("number", number).
                getResultList();
    }

    /**
     * Return the current pricelist on an agreement (if any). There may be just
     * FUTURE ones
     *
     * @param agreementUniqueId unique id of the agreement
     * @return
     */
    public AgreementPricelist getCurrentPricelist(long agreementUniqueId) {
        LOG.log(Level.FINEST, "getCurrentPricelist( agreementUniqueId: {0} )", new Object[] {agreementUniqueId} );
        List<AgreementPricelist> pricelists = em.createNamedQuery(AgreementPricelist.FIND_BY_PASSED_VALID_FROM_AND_AGREEMENT).
                setParameter("agreementUniqueId", agreementUniqueId).
                setParameter("validFrom", new Date()).
                getResultList();
        if( pricelists != null && !pricelists.isEmpty() ) {
            // query is ORDER BY validFrom DESC so first is most recent
            return pricelists.get(0);
        }
        return null;
    }

}
