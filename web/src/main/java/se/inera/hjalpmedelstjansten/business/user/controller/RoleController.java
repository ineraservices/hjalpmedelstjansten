package se.inera.hjalpmedelstjansten.business.user.controller;

import se.inera.hjalpmedelstjansten.business.BaseController;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.RoleAPI;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * Business methods for handling roles
 *
 */
@Stateless
public class RoleController extends BaseController {

    @Inject
    private HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;

    @Inject
    OrganizationController organizationController;

    @Inject
    ValidationMessageService validationMessageService;

    private final List<UserRole.RoleName> customerRoleNames = Arrays.asList(
                UserRole.RoleName.CustomerAgreementViewer,
                UserRole.RoleName.CustomerAgreementManager,
                UserRole.RoleName.Customeradmin,
                UserRole.RoleName.CustomerAssortmentManager,
                UserRole.RoleName.CustomerAssignedAssortmentManager);
    private final List<UserRole.RoleName> supplierRoleNames = Arrays.asList(
                UserRole.RoleName.SupplierAgreementViewer,
                UserRole.RoleName.SupplierAgreementManager,
                UserRole.RoleName.SupplierProductAndArticleHandler,
                UserRole.RoleName.Supplieradmin);
    private final List<UserRole.RoleName> serviceOwnerRoleNames = Arrays.asList(
                UserRole.RoleName.Superadmin);

    /**
     * Get roles available for the <code>Organization</code> with the given
     * unique id
     *
     * @param organizationUniqueId the unique id of the organization
     * @return a list of <code>RoleAPI</code> matching the available roles
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
     */
    public List<RoleAPI> getRolesForOrganization(long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "getRolesForOrganization( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        if( organization == null ) {
            throw validationMessageService.generateValidationException("roles", "userRole.organization.notExists");
        }
        return RoleMapper.map(em.createNamedQuery(UserRole.FIND_BY_NAMES).
                setParameter("names", getRoleNames(organization.getOrganizationType())).
                getResultList());
    }

    /**
     * Get all <code>UserRole.RoleName</code>s for the given Organization.OrganizationType
     *
     * @param organizationType the organization type
     * @return a list of <code>UserRole.RoleName</code>s available to the given
     * organization type
     */
    public List<UserRole.RoleName> getRoleNames(Organization.OrganizationType organizationType) {
        if( organizationType == Organization.OrganizationType.CUSTOMER ) {
            return customerRoleNames;
        } else if( organizationType == Organization.OrganizationType.SERVICE_OWNER ) {
            return serviceOwnerRoleNames;
        } else if( organizationType == Organization.OrganizationType.SUPPLIER ) {
            return supplierRoleNames;
        }
        return null;
    }

    /**
     * Checks whether the given <code>UserRole</code> is available to the given
     * <code>Organization</code>
     *
     * @param userRole the role to check
     * @param organization the organization to check if the role is available to
     * @return true if the <code>UserRole</code> is available to the organization,
     * otherwise false
     */
    public boolean validOrganizationRole(UserRole userRole, Organization organization) {
        List<UserRole.RoleName> validRoles = getRoleNames(organization.getOrganizationType());
        return validRoles.contains(userRole.getName());
    }

}
