package se.inera.hjalpmedelstjansten.business.security.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.controller.LogoutController;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import java.util.logging.Level;

@Stateless
@Path("logout")
@Interceptors({ PerformanceLogInterceptor.class })
public class LogoutService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private LogoutController logoutController;

    @Inject
    private AuthHandler authHandler;

    @POST
    public Response logoutUser(@Context HttpServletRequest httpServletRequest) {
        LOG.log(Level.FINEST, "logoutUser(...)");
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        logoutController.logoutUser(userAPI, getRequestIp(httpServletRequest));
        return Response.ok().build();
    }

}
