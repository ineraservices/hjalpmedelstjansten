package se.inera.hjalpmedelstjansten.business.generalpricelist.view;

import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.ResultExporterController;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistRowController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * REST API for pricelist rows.
 *
 */
@Stateless
@Path("organizations/{organizationUniqueId}/generalpricelists/pricelists/{pricelistUniqueId}/pricelistrows")
@Interceptors({ PerformanceLogInterceptor.class })
public class GeneralPricelistPricelistRowService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    GeneralPricelistPricelistRowController generalPricelistPricelistRowController;

    @EJB
    AuthHandler authHandler;

    @Inject
    ResultExporterController resultExporterController;

    /**
     * Search pricelist rows by user query. There can be only one general pricelist
     * per supplier organization
     *
     * @param query the user query
     * @param organizationUniqueId the unique id of the organization
     * @param pricelistUniqueId the uniqueid of the pricelist
     * @param offset for pagination purpose
     * @param statuses list of statuses to include in search, if null or empty,
     * all statuses are included
     * @param articleTypes list of article types to include in search, if null or empty,
     * all article types are included
     * @param showRowsWithPrices include rows where price is not null, if neither this
     * not showRowsWithoutPrices is set, then all rows are included
     * @param showRowsWithoutPrices include rows where price is null, if neither this
     * not showRowsWithPrices is set, then all rows are included
     * @return a list of <code>GeneralPricelistPricelistRowAPI</code> matching the query and a
     * header X-Total-Count giving the total number of agreements for pagination
     */
    @GET
    @SecuredService(permissions = {"generalpricelist_pricelistrow:view_own","generalpricelist_pricelistrow:view_all"})
    public Response searchGeneralPricelistPricelistRows(
            @DefaultValue(value = "") @QueryParam("query") String query,
            @DefaultValue(value = "") @QueryParam("sortOrder") String sortOrder,
            @DefaultValue(value = "") @QueryParam("sortType") String sortType,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            @DefaultValue(value = "0") @QueryParam("assortmentId") long assortmentId,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @QueryParam("status") List<GeneralPricelistPricelistRow.Status> statuses,
            @QueryParam("type") List<Article.Type> articleTypes,
            @QueryParam("showRowsWithPrices") Boolean showRowsWithPrices,
            @QueryParam("showRowsWithoutPrices") Boolean showRowsWithoutPrices,
            @QueryParam("category") Long categoryId,
            @QueryParam("articleStatus") List<Product.Status> articleStatuses) {
        LOG.log(Level.FINEST, "searchGeneralPricelistPricelistRows( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[] {organizationUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authHandler.hasPermission("generalpricelist_pricelistrow:view_all") && !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        SearchDTO searchDTO = generalPricelistPricelistRowController.searchPricelistRows(query, sortOrder, sortType,organizationUniqueId, pricelistUniqueId, assortmentId, userAPI, offset, 25, statuses, articleTypes, showRowsWithPrices, showRowsWithoutPrices, categoryId, articleStatuses);

        if( searchDTO == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(searchDTO.getItems()).
                    header("X-Total-Count", searchDTO.getCount()).
                    header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                    build();
        }
    }

    /**
     * Export pricelistrows for a specific pricelist
     *
     * @param httpServletRequest
     * @param query
     * @param organizationUniqueId unique id of the organization
     * @param pricelistId
     * @return an excel containing the list of search results
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"generalpricelist_pricelistrow:view_own","generalpricelist_pricelistrow:view_all"})
    //@SecuredService(permissions = {"assortmentarticle:view"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportGeneralPricelistPricelistRows(
            @Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("pricelistUniqueId") long pricelistId,
            @DefaultValue(value = "0") @QueryParam("assortmentId") long assortmentId,
            @QueryParam("status") List<GeneralPricelistPricelistRow.Status> statuses,
            @QueryParam("type") List<Article.Type> articleTypes,
            @QueryParam("showRowsWithPrices") Boolean showRowsWithPrices,
            @QueryParam("showRowsWithoutPrices") Boolean showRowsWithoutPrices,
            @QueryParam("category") Long categoryId,
            @QueryParam("articleStatus") List<Product.Status> articleStatuses,
            @DefaultValue(value = "0") @QueryParam("ownOrganizationUniqueId") long ownOrganizationUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query,
            @DefaultValue(value = "") @QueryParam("sortOrder") String sortOrder,
            @DefaultValue(value = "") @QueryParam("sortType") String sortType) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "exportGeneralPricelistPricelistRows( organizationUniqueId: {0}, pricelistId: {1} )", new Object[]{organizationUniqueId, pricelistId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        //Own organization ID should be the one that gets auothrized here, not the owner of the pricelistRow.
        if(!isSuperAdmin && !authHandler.hasPermission("generalpricelist_pricelistrow:view_all") && !authorizeHandleOrganization(ownOrganizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        int maximumNumberOfResults = 55000;

        SearchDTO searchDTO = generalPricelistPricelistRowController.searchPricelistRows(query,
                sortOrder,
                sortType,
                organizationUniqueId,
                pricelistId,
                assortmentId,
                userAPI,
                0,
                maximumNumberOfResults,
                statuses,
                articleTypes,
                showRowsWithPrices,
                showRowsWithoutPrices,
                categoryId,
                articleStatuses);

        if (searchDTO == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                String filename = "Export_ArtiklarPrislista_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";

                List<GeneralPricelistPricelistRowAPI> searchGeneralPricelistPricelistRowAPIs = (List<GeneralPricelistPricelistRowAPI>) searchDTO.getItems();

                byte[] exportBytes = resultExporterController.generatePricelistRowList(searchGeneralPricelistPricelistRowAPIs, ownOrganizationUniqueId);

                return Response.ok(
                        exportBytes,
                        jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                        .header("content-disposition", "attachment; filename = " + filename)
                        .build();
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to export articles for assortment to file", ex);
                return Response.serverError().build();
            }
        }
    }

    /**
     * Create new pricelist row
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs user supplied values
     * @return the created <code>GeneralPricelistPricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @POST
    @SecuredService(permissions = {"generalpricelist_pricelistrow:create_own"})
    public Response createPricelistRow(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            List<GeneralPricelistPricelistRowAPI> pricelistRowAPIs) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createPricelistRow( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[] {organizationUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistRowAPIs = generalPricelistPricelistRowController.createPricelistRows(organizationUniqueId, pricelistUniqueId, pricelistRowAPIs, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        return Response.ok(pricelistRowAPIs).build();
    }

    /**
     * Delete supplied row. Supplier deletes a row in the general price list
     *
     * @param organizationUniqueId unique id of the organization
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowUniqueId user supplied unique id
     * @return new list
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @DELETE
    @Path("{pricelistRowUniqueId}")
    @SecuredService(permissions = {"generalpricelist_pricelistrow:delete_own"})
    public Response deleteRow(@Context HttpServletRequest httpServletRequest,
                                   @PathParam("organizationUniqueId") long organizationUniqueId,
                                   @PathParam("pricelistUniqueId") long pricelistUniqueId,
                                   @PathParam("pricelistRowUniqueId") long pricelistRowUniqueId
                                   ) throws HjalpmedelstjanstenValidationException {

        LOG.log(Level.FINEST, "Deleterow( organizationUniqueId: {0}, pricelistRowUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, pricelistRowUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        GeneralPricelistPricelistRowAPI pricelistRowAPI = generalPricelistPricelistRowController.deleteRow(organizationUniqueId,pricelistUniqueId, pricelistRowUniqueId, userAPI,authHandler.getSessionId(),getRequestIp(httpServletRequest));

        if( pricelistRowAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPI).build();
        }
    }

    /**
     * Update a pricelist row on a general pricelist
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowUniqueId unique id of the row
     * @param pricelistRowAPI user supplied values
     * @return the created <code>GeneralPricelistPricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @PUT
    @Path("{pricelistRowUniqueId}")
    @SecuredService(permissions = {"generalpricelist_pricelistrow:update_own"})
    public Response updatePricelistRow(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            @PathParam("pricelistRowUniqueId") long pricelistRowUniqueId,
            GeneralPricelistPricelistRowAPI pricelistRowAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updatePricelistRow( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[] {organizationUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistRowAPI = generalPricelistPricelistRowController.updatePricelistRow(organizationUniqueId, pricelistUniqueId, pricelistRowUniqueId, pricelistRowAPI, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPI).build();
        }
    }

    /**
     * Inactivate supplied rows.
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs user supplied rows
     * @return the created <code>GeneralPricelistPricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @POST
    @Path("inactivate")
    @SecuredService(permissions = {"generalpricelist_pricelistrow:inactivate_own"})
    public Response inactivateRows(@Context HttpServletRequest httpServletRequest,
                                   @PathParam("organizationUniqueId") long organizationUniqueId,
                                   @PathParam("pricelistUniqueId") long pricelistUniqueId,
                                   List<GeneralPricelistPricelistRowAPI> pricelistRowAPIs) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "inactivateRows( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[] {organizationUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistRowAPIs = generalPricelistPricelistRowController.inactivateRows(organizationUniqueId, pricelistUniqueId, pricelistRowAPIs, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPIs).build();
        }
    }

}
