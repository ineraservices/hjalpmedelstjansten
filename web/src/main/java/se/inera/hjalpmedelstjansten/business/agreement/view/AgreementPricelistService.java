package se.inera.hjalpmedelstjansten.business.agreement.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Product;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Level;

/**
 * REST API for agreement pricelists.
 *
 */
@Stateless
@Path("organizations/{organizationUniqueId}/agreements/{agreementUniqueId}/pricelists")
@Interceptors({ PerformanceLogInterceptor.class })
public class AgreementPricelistService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @Inject
    AgreementPricelistController agreementPricelistController;

    @EJB
    AuthHandler authHandler;

    /**
     * Get all pricelists on a specific agreement
     *
     * @param organizationUniqueId the unique id of the organization to get agreement from
     * @param agreementUniqueId the id of the agreement
     * @return the corresponding List of <code>PricelistAPI</code>
     */
    @GET
    @SecuredService(permissions = {"pricelist:view_own"})
    public Response getPricelistsOnAgreement(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId ) {
        LOG.log(Level.FINEST, "getPricelistsOnAgreement( organizationUniqueId: {0}, agreementUniqueId: {1} )", new Object[] {organizationUniqueId, agreementUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        List<AgreementPricelistAPI> pricelistAPIs = agreementPricelistController.getPricelistAPIsOnAgreement( organizationUniqueId, agreementUniqueId, userAPI );
        if( pricelistAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistAPIs).build();
        }
    }

    /**
     * Get all pricelists where an article can be added on a specific agreement (i.e. where the article is not present already)
     *
     * @param organizationUniqueId the unique id of the organization to get agreement from
     * @param agreementUniqueId the id of the agreement
     * @param articleUniqueId the id of the article
     * @return the corresponding List of <code>PricelistAPI</code>
     */
    @GET
    @Path("article/{articleUniqueId}")
    @SecuredService(permissions = {"pricelist:view_own"})
    public Response getSelectablePricelistsOnAgreement(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("articleUniqueId") long articleUniqueId) {
        LOG.log(Level.FINEST, "getSelectablePricelistsOnAgreement( organizationUniqueId: {0}, agreementUniqueId: {1}, articleUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, articleUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        List<AgreementPricelistAPI> pricelistAPIs = agreementPricelistController.getSelectablePricelistAPIsOnAgreement( organizationUniqueId, agreementUniqueId, userAPI, articleUniqueId);
        if( pricelistAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistAPIs).build();
        }
    }

    /**
     * Get information about a specific pricelist
     *
     * @param organizationUniqueId the unique id of the organization to get agreement from
     * @param agreementUniqueId the id of the agreement
     * @param pricelistUniqueId the id of the pricelist
     * @return the corresponding <code>PricelistAPI</code>
     */
    @GET
    @Path("{pricelistUniqueId}")
    @SecuredService(permissions = {"pricelist:view_own"})
    public Response getPricelist(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId ) {
        LOG.log(Level.FINEST, "getPricelist( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        AgreementPricelistAPI pricelistAPI = agreementPricelistController.getPricelistAPI( organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest) );
        if( pricelistAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistAPI).build();
        }
    }

    /**
     * Create new pricelist
     *
     * @param organizationUniqueId unique id of the organization to create pricelist on
     * @param agreementUniqueId unique id of the agreement to create the pricelist on
     * @param pricelistAPI user supplied values
     * @return the created <code>PricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @SecuredService(permissions = {"pricelist:create_own"})
    public Response createPricelist(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            AgreementPricelistAPI pricelistAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createPricelist( organizationUniqueId: {0}, agreementUniqueId: {1} )", new Object[] {organizationUniqueId, agreementUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistAPI = agreementPricelistController.createPricelist(organizationUniqueId, agreementUniqueId, pricelistAPI, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(pricelistAPI).build();
    }

    /**
     * Update pricelist. Updatable fields are name and validFrom.
     *
     * @param organizationUniqueId unique id of the organization to update pricelist on
     * @param agreementUniqueId unique id of the agreement to update the pricelist on
     * @param pricelistUniqueId unique id of the pricelist to update
     * @param pricelistAPI user supplied values
     * @return the updated <code>PricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    @PUT
    @Path("{pricelistUniqueId}")
    @SecuredService(permissions = {"pricelist:update_own"})
    public Response updatePricelist(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            AgreementPricelistAPI pricelistAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updatePricelist( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistAPI = agreementPricelistController.updatePricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistAPI, userAPI, authHandler.getSessionId());
        if( pricelistAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(pricelistAPI).build();
    }

    /**
     * Search articles available to add on a specific pricelist
     *
     * @param organizationUniqueId the unique id of the organization to get agreement from
     * @param agreementUniqueId the id of the agreement
     * @param pricelistUniqueId the id of the pricelist
     * @param query
     * @param offset
     * @param statuses
     * @param articleTypes
     * @return the corresponding <code>PricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
     */
    @GET
    @Path("{pricelistUniqueId}/articles")
    @SecuredService(permissions = {"pricelist:view_own"})
    public Response searchArticlesForPricelist(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @QueryParam("status") List<Product.Status> statuses,
            @QueryParam("type") List<Article.Type> articleTypes) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "searchArticlesForPricelist( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        SearchDTO searchDTO = agreementPricelistController.searchArticlesForAgreementPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, query, statuses, articleTypes, offset, 25, userAPI);
        if( searchDTO == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(searchDTO.getItems()).
                header("X-Total-Count", searchDTO.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
        }
    }

}
