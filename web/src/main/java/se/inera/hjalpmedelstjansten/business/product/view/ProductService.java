package se.inera.hjalpmedelstjansten.business.product.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.media.controller.MediaController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductController;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductMapper;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.media.MediaAPI;
import se.inera.hjalpmedelstjansten.model.api.media.MediaImageAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaDocument;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaImage;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaVideo;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

/**
 * REST API for products
 *
 */
@Stateless
@Path("organizations/{organizationUniqueId}/products")
@Interceptors({ PerformanceLogInterceptor.class })
public class ProductService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    private ProductController productController;

    @EJB
    private OrganizationController organizationController;

    @EJB
    private MediaController mediaController;

    @EJB
    private AuthHandler authHandler;

    /**
     * Get products by organization
     *
     * @param organizationUniqueId the unique id of the organization to search products on
     * @param statuses limit results to products with these statuses
     * @param articleTypes limit results to product with category of this articleTypes
     * @return a list of <code>ProductAPI</code> matching the query
     */
    @GET
    @SecuredService(permissions = {"product:view"})
    public Response getProducts(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @DefaultValue(value = "PUBLISHED") @QueryParam("status") List<Product.Status> statuses,
            @QueryParam("type") List<Article.Type> articleTypes) {
        LOG.log(Level.FINEST, "getProducts( organizationUniqueId: {0}, statuses: {1}, articleTypes: {2} )", new Object[] {organizationUniqueId, statuses, articleTypes});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        LinkedList<Article.Type> types = new LinkedList<>(articleTypes);
        if( types.isEmpty() ) {
            types.addAll(Arrays.asList(Article.Type.values()));
        }
        List<ProductAPI> productAPIs =  productController.getProductsByOrganizations(organizationUniqueId, statuses, types);
        return Response.ok(productAPIs).build();
    }

    /**
     * Get information about a specific product
     *
     * @param httpServletRequest
     * @param organizationUniqueId the unique id of the organization to get product from
     * @param uniqueId the id of the product to return
     * @return the corresponding <code>ProductAPI</code>
     */
    @GET
    @Path("{uniqueId : \\d+}")
    @SecuredService(permissions = {"product:view"})
    public Response getProduct(@Context HttpServletRequest httpServletRequest,
                               @PathParam("organizationUniqueId") long organizationUniqueId,
                               @PathParam("uniqueId") long uniqueId ) {
        LOG.log(Level.FINEST, "getProduct( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        ProductAPI productAPI = productController.getProductAPI( organizationUniqueId, uniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest) );
        if( productAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(productAPI).build();
        }
    }

    /**
     * Search articles based on product
     *
     * @param organizationUniqueId the unique id of the organization to search products on
     * @param uniqueId unique id of the product
     * @param offset for pagination purpose
     * @return a list of <code>ArticleAPI</code> matching query and a
     * header X-Total-Count giving the total number of articles for pagination
     */
    @GET
    @Path("{uniqueId}/basedon")
    @SecuredService(permissions = {"product:view"})
    public Response searchBasedOnProduct(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("uniqueId") long uniqueId,
            @DefaultValue(value = "0") @QueryParam("offset") int offset) {
        LOG.log(Level.FINEST, "searchBasedOnProduct( organizationUniqueId: {0}, uniqueId: {1}, offset: {0} )", new Object[] {organizationUniqueId, uniqueId, offset});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        long countSearch = productController.countSearchBasedOnProduct(organizationUniqueId, uniqueId);
        List<ArticleAPI> articleAPIs =  productController.searchArticlesBasedOnProduct(organizationUniqueId, uniqueId, offset, 25);
        return Response.ok(articleAPIs).
                header("X-Total-Count", countSearch).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }

    /**
     * Search articles that fits to product
     *
     * @param organizationUniqueId the unique id of the organization to search products on
     * @param uniqueId unique id of the product
     * @param offset for pagination purpose
     * @return a list of <code>ArticleAPI</code> matching query and a
     * header X-Total-Count giving the total number of articles for pagination
     */
    @GET
    @Path("{uniqueId}/fitsto")
    @SecuredService(permissions = {"product:view"})
    public Response searchFitsToProduct(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("uniqueId") long uniqueId,
            @DefaultValue(value = "0") @QueryParam("offset") int offset) {
        LOG.log(Level.FINEST, "searchFitsToProduct( organizationUniqueId: {0}, uniqueId: {1}, offset: {0} )", new Object[] {organizationUniqueId, uniqueId, offset});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        long countSearch = productController.countSearchFitsToProduct(organizationUniqueId, uniqueId);
        List<ArticleAPI> articleAPIs =  productController.searchArticlesFitsToProduct(organizationUniqueId, uniqueId, offset, 25);
        return Response.ok(articleAPIs).
                header("X-Total-Count", countSearch).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }

    /**
     * Create new product
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to create product on
     * @param productAPI user supplied values
     * @return the created <code>ProductAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @SecuredService(permissions = {"product:create_own"})
    public Response createProduct(@Context HttpServletRequest httpServletRequest,
                                  @PathParam("organizationUniqueId") long organizationUniqueId,
                                  ProductAPI productAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createProduct( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        productAPI = productController.createProduct(organizationUniqueId, productAPI, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest), true);
        if( productAPI == null ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        Product product = ProductMapper.map(productAPI);
        product.setUniqueId(productAPI.getId());
        product.setOrganization(organization);
        MediaImageAPI mediaImageAPI = new MediaImageAPI();
        mediaImageAPI.setMainImage(true);
        mediaImageAPI.setProductId(product.getUniqueId());
        mediaImageAPI.setAlternativeText(product.getMainImageAltText());
        mediaImageAPI.setUrl(product.getMainImageUrl());
        mediaImageAPI.setDescription(product.getMainImageDescription());
        mediaImageAPI.setOriginalUrl(product.getMainImageOriginalUrl());
        if (mediaImageAPI.getOriginalUrl() != null && !mediaImageAPI.getOriginalUrl().isEmpty()) {
            mediaController.handleMainImageUpload(product, mediaImageAPI, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest ));
        }

        return Response.ok(productAPI).build();
    }

    /**
     * Delete product
     *
     * @param organizationUniqueId unique id of organization
     * @param uniqueId the id of the product to delete
     * @return response code 200 if delete is successful, otherwise 404 if the user
     * cannot read the article or 400 if there is a validation error
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case validation fails
     */
    @DELETE
    @Path("{uniqueId}")
    @SecuredService(permissions = {"product:update_own"})
    public Response deleteProduct(@Context HttpServletRequest httpServletRequest,
                                  @PathParam("organizationUniqueId") long organizationUniqueId,
                                  @PathParam("uniqueId") long uniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteProduct( uniqueId: {0} )", new Object[] {uniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        List<MediaVideo> productMediaVideos = mediaController.getProductMediaVideos(uniqueId);
        List<MediaImage> productMediaImages = mediaController.getProductMediaImages(uniqueId);
        List<MediaDocument> productMediaDocuments = mediaController.getProductMediaDocuments(uniqueId);

        if (productMediaImages != null) {
            for (MediaImage media : productMediaImages) {
                mediaController.deleteProductMedia(productController.getProduct(organizationUniqueId, uniqueId, userAPI), media, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
            }
        }
        if (productMediaDocuments != null) {
            for (MediaDocument media : productMediaDocuments) {
                mediaController.deleteProductMedia(productController.getProduct(organizationUniqueId, uniqueId, userAPI), media, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
            }
        }
        if (productMediaVideos != null) {
            for (MediaVideo media : productMediaVideos){
                mediaController.deleteProductMedia(productController.getProduct(organizationUniqueId, uniqueId, userAPI), media, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
            }
        }

        productController.delete(organizationUniqueId, uniqueId, userAPI);
        return Response.ok().build();
    }

    /**
     * Update an existing <code>Product</code>
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of organization
     * @param uniqueId the id of the organization to update
     * @param productAPI user supplied values
     * @return the updated <code>ProductAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case validation fails
     */
    @PUT
    @Path("{uniqueId}")
    @SecuredService(permissions = {"product:update_own"})
    public Response updateProduct(@Context HttpServletRequest httpServletRequest,
                                  @PathParam("organizationUniqueId") long organizationUniqueId,
                                  @PathParam("uniqueId") long uniqueId,
                                  ProductAPI productAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateProduct( uniqueId: {0} )", new Object[] {uniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        productAPI = productController.updateProduct(organizationUniqueId, uniqueId, productAPI, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest), true, false);
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        Product product = ProductMapper.map(productAPI);
        product.setUniqueId(productAPI.getId());
        product.setOrganization(organization);
        MediaImageAPI mediaImageAPI = new MediaImageAPI();
        mediaImageAPI.setMainImage(true);
        mediaImageAPI.setProductId(product.getUniqueId());
        mediaImageAPI.setAlternativeText(product.getMainImageAltText());
        mediaImageAPI.setUrl(product.getMainImageUrl());
        mediaImageAPI.setDescription(product.getMainImageDescription());
        mediaImageAPI.setOriginalUrl(product.getMainImageOriginalUrl());
//        if (mediaImageAPI.getOriginalUrl() != null && !mediaImageAPI.getOriginalUrl().isEmpty()) {
//            mediaController.handleMainImageUpload(product, mediaImageAPI, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest ));
//        }

        if( productAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(productAPI).build();
        }
    }

}
