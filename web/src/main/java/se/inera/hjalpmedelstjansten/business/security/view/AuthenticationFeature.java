package se.inera.hjalpmedelstjansten.business.security.view;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;

import jakarta.inject.Inject;
import jakarta.ws.rs.container.DynamicFeature;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.FeatureContext;
import jakarta.ws.rs.ext.Provider;
import java.lang.reflect.Method;
import java.util.logging.Level;

/**
 * This feature checks the APIs for the <code>SecuredService</code> annotation
 * and adds the <code>AuthenticationFilter</code> to it.
 *
 * @see se.inera.hjalpmedelstjansten.business.security.view.AuthenticationFilter
 * @see se.inera.hjalpmedelstjansten.business.security.view.SecuredService
 */
@Provider
public class AuthenticationFeature implements DynamicFeature {

    @Inject
    private HjmtLogger LOG;

    @Override
    public void configure(ResourceInfo resourceInfo, FeatureContext context) {
        Method method = resourceInfo.getResourceMethod();
        LOG.log(Level.FINEST, "Checking for security annotation class: {0}, method: {1}", new Object[] {method.getDeclaringClass().getName(), method.getName()});
        if( method.isAnnotationPresent(SecuredService.class) ) {
            LOG.log(Level.FINEST, "Securing method: {0}", new Object[] {method.getName()} );
            SecuredService securedService = method.getAnnotation(SecuredService.class);
            AuthenticationFilter authenticationFilter = new AuthenticationFilter(securedService.permissions());
            context.register(authenticationFilter);
        } else {
            LOG.log(Level.INFO, "Method: {0} in class: {1} is NOT secured", new Object[] {method.getName(), method.getDeclaringClass().getName()} );
        }
    }

}
