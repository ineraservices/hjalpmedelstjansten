package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.security.controller.SecurityController;
import se.inera.hjalpmedelstjansten.clustering.ClusterController;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificPropertyListValue;
import se.inera.hjalpmedelstjansten.model.entity.ElectronicAddress;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.PostAddress;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValue;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueDecimal;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueTextField;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueValueListSingle;
import se.inera.hjalpmedelstjansten.model.entity.UserAccount;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEDirective;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEStandard;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCountry;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.ScheduleExpression;
import jakarta.ejb.Timeout;
import jakarta.ejb.TimerConfig;
import jakarta.ejb.TimerService;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

/**
 * Timer for bulk creating organizations with products and articles. This is only
 * for testing and should NEVER be enabled in production. To enable this locally:
 * uncomment
 * @Singleton
 * @Startup
 * @DependsOn
 * @Resource
 *
 * and add the following configuration to dev.properties:
 * createSampleDataTimerEnabled=true
 * createSampleDataTimerHour=
 * createSampleDataTimerMinute=
 * createSampleDataTimerSecond=
 * with appropriate values
 *
 */
//@Singleton
//@Startup
//@DependsOn("PropertyLoader")
public class CreateSampleDataTimer {

    @Inject
    private HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;

    @Inject
    private SecurityController securityController;

    @Inject
    private boolean createSampleDataTimerEnabled;

    @Inject
    private String createSampleDataTimerHour;

    @Inject
    private String createSampleDataTimerMinute;

    @Inject
    private String createSampleDataTimerSecond;

    @Inject
    private ClusterController clusterController;

    @Inject
    private ElasticSearchController elasticSearchController;

    @Inject
    private String testPassword;

    //@Resource
    private TimerService timerService;

    @Timeout
    private void schedule() {
        LOG.log( Level.FINEST, "schedule" );
        // attempt to get lock
        boolean lockReceived = clusterController.getLock(this.getClass().getName(), 30);
        if( lockReceived ) {
            LOG.log( Level.FINEST, "Received lock!" );
            long start = System.currentTimeMillis();
            Long numberOfOrganizations = (Long) em.
                    createQuery("SELECT COUNT(o.uniqueId) FROM Organization o WHERE o.organizationType = :organizationType").
                    setParameter("organizationType", Organization.OrganizationType.SUPPLIER).
                    getSingleResult();
            if( numberOfOrganizations < 300 ) {
                LOG.log( Level.INFO, "Less than 300 suppliers found");
                List<CVCountry> countrys = em.createQuery("SELECT c FROM CVCountry c").getResultList();
                List<Organization> suppliers = loadMoreSuppliers(countrys.get(0), numberOfOrganizations);
                List<UserRole> userRoles = em.createQuery("SELECT u FROM UserRole u").getResultList();
                List<CVCEDirective> directives = em.createQuery("SELECT d FROM CVCEDirective d").getResultList();
                List<CVCEStandard> standards = em.createQuery("SELECT s FROM CVCEStandard s").getResultList();
                List<Category> categorys = em.createQuery("SELECT c FROM Category c WHERE c.articleType = :articleType").
                        setParameter("articleType", Article.Type.H).
                        getResultList();
                List<CVOrderUnit> orderUnits = em.createQuery("SELECT o FROM CVOrderUnit o").getResultList();
                Category category010101 = (Category) em.createQuery("SELECT c FROM Category c WHERE c.code = '010101'").getResultList().get(0);
                loadMoreProductsAndArticles(suppliers, userRoles, directives.get(0), standards.get(0), category010101, orderUnits.get(0));
                reindexOrganizations(suppliers, category010101);
                long total = System.currentTimeMillis() - start;
                if( total > 60000 ) {
                    LOG.log( Level.WARNING, "Scheduled job took: {0} ms which is more than 1 minute which is a long time, a developer should take a look at this.", new Object[] {total});
                } else {
                    LOG.log( Level.INFO, "Scheduled job took: {0} ms.", new Object[] {total});
                }
            } else {
                LOG.log( Level.INFO, "More than 300 suppliers found");
            }
            //release lock
            clusterController.releaseLock(this.getClass().getName());
        } else {
            LOG.log( Level.INFO, "Did not receive lock!" );
        }
    }

    private void loadMoreProductsAndArticles(List<Organization> suppliers,
            List<UserRole> userRoles,
            CVCEDirective directive,
            CVCEStandard standard,
            Category category010101,
            CVOrderUnit orderUnit) {
        int o = 0;
        List<CategorySpecificProperty> categorySpecificPropertys = em.createQuery("SELECT csp FROM CategorySpecificProperty csp WHERE csp.category.uniqueId = :categoryUniqueId").
                setParameter("categoryUniqueId", category010101.getUniqueId()).
                getResultList();
        CategorySpecificProperty textCategorySpecificProperty = null;
        CategorySpecificProperty singleListCategorySpecificProperty = null;
        CategorySpecificProperty decimalCategorySpecificProperty = null;
        CategorySpecificPropertyListValue categorySpecificPropertyListValue = null;
        for( CategorySpecificProperty categorySpecificProperty : categorySpecificPropertys ) {
            if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.TEXTFIELD ) {
                textCategorySpecificProperty = categorySpecificProperty;
            } else if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_SINGLE ) {
                singleListCategorySpecificProperty = categorySpecificProperty;
                categorySpecificPropertyListValue = singleListCategorySpecificProperty.getCategorySpecificPropertyListValues().get(0);
            } else if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.DECIMAL ) {
                decimalCategorySpecificProperty = categorySpecificProperty;
            }
        }
        for( Organization organization : suppliers ) {
            o++;
            LOG.log( Level.FINEST, "Loading products for organization {0} of {1}", new Object[]{o, suppliers.size()});
            List<UserRole> supplierRoles = new ArrayList<>();
            supplierRoles.add(getRoleByName(UserRole.RoleName.Baseuser, userRoles));
            supplierRoles.add(getRoleByName(UserRole.RoleName.SupplierProductAndArticleHandler, userRoles));
            for( int i = 0; i<50; i++ ) {
                Product product = new Product();
                product.setOrganization(organization);
                product.setProductName(organization.getOrganizationName() + " - Produkt " + i );
                product.setProductNumber(organization.getOrganizationNumber() + "-" + i);
                product.setCeMarked(true);
                product.setStatus(Product.Status.PUBLISHED);
                product.setCeDirective(directive);
                product.setCeStandard(standard);
                product.setCategory(category010101);
                product.setSupplementedInformation("Mera information " + product.getProductName());
                product.setDescriptionForElvasjuttiosju("Detta visas på 1177 om produkten utbudas " + product.getProductName());
                product.setOrderUnit(orderUnit);
                product.setMediaFolderName("media-" + i + "-" + organization.getOrganizationNumber());

                List<ResourceSpecificPropertyValue> productResourceSpecificPropertyValues = new ArrayList<>();
                ResourceSpecificPropertyValueTextField resourceSpecificPropertyValueTextfield = new ResourceSpecificPropertyValueTextField();
                resourceSpecificPropertyValueTextfield.setProduct(product);
                resourceSpecificPropertyValueTextfield.setCategorySpecificProperty(textCategorySpecificProperty);
                resourceSpecificPropertyValueTextfield.setValue("test");
                em.persist(resourceSpecificPropertyValueTextfield);
                productResourceSpecificPropertyValues.add(resourceSpecificPropertyValueTextfield);

                ResourceSpecificPropertyValueValueListSingle resourceSpecificPropertyValueValueListSingle = new ResourceSpecificPropertyValueValueListSingle();
                resourceSpecificPropertyValueValueListSingle.setCategorySpecificProperty(singleListCategorySpecificProperty);
                resourceSpecificPropertyValueValueListSingle.setProduct(product);
                resourceSpecificPropertyValueValueListSingle.setValue(categorySpecificPropertyListValue);
                em.persist(resourceSpecificPropertyValueValueListSingle);
                productResourceSpecificPropertyValues.add(resourceSpecificPropertyValueValueListSingle);

                product.setResourceSpecificPropertyValues(productResourceSpecificPropertyValues);

                em.persist(product);
                for( int x = 0; x<30; x++ ) {
                    Article article = new Article();
                    article.setBasedOnProduct(product);
                    article.setOrganization(organization);
                    article.setArticleName(organization.getOrganizationName() + " - Produkt " + i + " - Artikel " + x);
                    article.setArticleNumber(organization.getOrganizationNumber() + "-" + i + "-" + x);
                    article.setStatus(Product.Status.PUBLISHED);
                    article.setSupplementedInformation("Mera information " + article.getArticleName());
                    article.setOrderUnit(orderUnit);
                    article.setMediaFolderName("media-" + i + "-" + "-" + x + "-" + organization.getOrganizationNumber());

                    List<ResourceSpecificPropertyValue> articleResourceSpecificPropertyValues = new ArrayList<>();
                    ResourceSpecificPropertyValueDecimal resourceSpecificPropertyValueDecimal = new ResourceSpecificPropertyValueDecimal();
                    resourceSpecificPropertyValueDecimal.setCategorySpecificProperty(decimalCategorySpecificProperty);
                    resourceSpecificPropertyValueDecimal.setArticle(article);
                    resourceSpecificPropertyValueDecimal.setValue(1.0);
                    em.persist(resourceSpecificPropertyValueDecimal);
                    articleResourceSpecificPropertyValues.add(resourceSpecificPropertyValueDecimal);
                    article.setResourceSpecificPropertyValues(articleResourceSpecificPropertyValues);

                    em.persist(article);
                }
            }
        }
    }

    private List<Organization> loadMoreSuppliers(CVCountry country, Long startAt) {
        LOG.log( Level.FINEST, "loadMoreSuppliers(...)");
        List<Organization> suppliers = new ArrayList<>();
        int start = startAt.intValue();
        int end = start + 4;
        for( int i = start; i<end; i++ ) {
            Organization organization = new Organization();
            organization.setOrganizationName("orgname " + i);
            organization.setOrganizationNumber("orgnumber " + i);
            organization.setOrganizationType(Organization.OrganizationType.SUPPLIER);
            organization.setMediaFolderName(UUID.randomUUID().toString());
            organization.setGln("more" + i);
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.MONTH, Calendar.JANUARY);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            organization.setValidFrom(calendar.getTime());
            organization.setCountry(country);

            // Postal Addresses
            List<PostAddress> postAddresses = new ArrayList<>();
            PostAddress visitPostAddress = new PostAddress();
            visitPostAddress.setAddressType(PostAddress.AddressType.VISIT);
            visitPostAddress.setStreetAddress("Visit Street Address");
            visitPostAddress.setPostCode("11111");
            visitPostAddress.setCity("Visit City");
            visitPostAddress.setOrganization(organization);
            postAddresses.add(visitPostAddress);
            PostAddress deliveryPostAddress = new PostAddress();
            deliveryPostAddress.setAddressType(PostAddress.AddressType.DELIVERY);
            deliveryPostAddress.setStreetAddress("Delivery Street Address");
            deliveryPostAddress.setPostCode("22222");
            deliveryPostAddress.setCity("Delivery City");
            deliveryPostAddress.setOrganization(organization);
            postAddresses.add(deliveryPostAddress);
            organization.setPostAddresses(postAddresses);

            // Electronic Addresses
            ElectronicAddress electronicAddress = new ElectronicAddress();
            electronicAddress.setWeb("https://www.inera.se/");
            organization.setElectronicAddress(electronicAddress);
            em.persist(organization);
            suppliers.add( organization );
        }
        return suppliers;
    }

    private UserEngagement loadUser(Organization organization, List<UserRole> userRoles, String username, String userFirstName, String userLastName) {
        LOG.log( Level.INFO, "loadUser()" );
        // User Account
        UserAccount userAccount = new UserAccount();
        userAccount.setFirstName(userFirstName);
        userAccount.setLastName(userLastName);
        userAccount.setUsername(username);
        userAccount.setTitle("Title");
        userAccount.setLoginType(UserAccount.LoginType.PASSWORD);
        String salt = securityController.generateSalt();
        int iterations = securityController.generateIterations();
        String hashedPassword = securityController.hashPassword(testPassword, salt, iterations);
        userAccount.setSalt(salt);
        userAccount.setIterations(iterations);
        userAccount.setPassword(hashedPassword);

        // Electronic Addresses
        ElectronicAddress electronicAddress = new ElectronicAddress();
        electronicAddress.setEmail("");
        electronicAddress.setMobile("");
        electronicAddress.setTelephone("");
        userAccount.setElectronicAddress(electronicAddress);

        // The Engagement
        UserEngagement userEngagement = new UserEngagement();
        userEngagement.setOrganization(organization);
        userEngagement.setUserAccount(userAccount);
        userEngagement.setValidFrom(new Date());

        // Roles
        userEngagement.setRoles(userRoles);

        em.persist(userEngagement);

        return userEngagement;
    }

    private UserRole getRoleByName(UserRole.RoleName roleName, List<UserRole> userRoles) {
        for( UserRole userRole : userRoles ) {
            if( userRole.getName().equals(roleName) ) {
                return userRole;
            }
        }
        return null;
    }

    @PostConstruct
    private void initialize() {
        LOG.log( Level.FINEST, "initialize()" );
        if( createSampleDataTimerEnabled ) {
            LOG.log( Level.FINEST, "Timer is ENABLED" );
            if (timerService.getTimers().isEmpty()) {
                String name = this.getClass().getName();
                TimerConfig configuration = new TimerConfig();
                configuration.setPersistent(false);
                configuration.setInfo(name);
                ScheduleExpression scheduleExpression = new ScheduleExpression();
                scheduleExpression.hour(createSampleDataTimerHour).minute(createSampleDataTimerMinute).second(createSampleDataTimerSecond);
                timerService.createCalendarTimer(scheduleExpression, configuration);
            }
        } else {
            LOG.log( Level.INFO, "Timer is NOT ENABLED" );
        }
    }

    private void reindexOrganizations(List<Organization> suppliers, Category category) {
        List<CategorySpecificProperty> categorySpecificPropertys = em.createQuery("SELECT csp FROM CategorySpecificProperty csp WHERE csp.category.uniqueId = :categoryUniqueId").
                setParameter("categoryUniqueId", category.getUniqueId()).
                getResultList();
        for( Organization organization : suppliers ) {
            List<Product> products = em.createQuery("SELECT p FROM Product p WHERE p.organization.uniqueId = :organizationUniqueId").
                    setParameter("organizationUniqueId", organization.getUniqueId()).
                    getResultList();
            for( Product product : products ) {
                ProductAPI productAPI = ProductMapper.map(product, true, categorySpecificPropertys);
                elasticSearchController.bulkIndexProduct(productAPI);
            }
            List<Article> articles = em.createQuery("SELECT a FROM Article a WHERE a.organization.uniqueId = :organizationUniqueId").
                    setParameter("organizationUniqueId", organization.getUniqueId()).
                    getResultList();
            for( Article article : articles ) {
                ArticleAPI articleAPI = ArticleMapper.map(article, true, categorySpecificPropertys);
                elasticSearchController.bulkIndexArticle(articleAPI);
            }
        }
    }

}
