package se.inera.hjalpmedelstjansten.business;

import se.inera.hjalpmedelstjansten.model.api.ErrorAPI;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * This exception causes a HTTP response code of 500 and lists error in the
 * response body.
 *
 * @see se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException
 */
@Provider
public class HjalpmedelstjanstenExceptionMapper implements ExceptionMapper<HjalpmedelstjanstenException> {

    @Override
    public Response toResponse(HjalpmedelstjanstenException exception) {
        if( exception.getType() != null && exception.getType() == HjalpmedelstjanstenException.Type.FORBIDDEN ) {
            return Response.status(Response.Status.FORBIDDEN).
                build();
        }
        ErrorAPI errorAPI = new ErrorAPI();
        errorAPI.setErrors(exception.getValidationMessages());
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
            .entity(errorAPI)
            .build();
    }

}
