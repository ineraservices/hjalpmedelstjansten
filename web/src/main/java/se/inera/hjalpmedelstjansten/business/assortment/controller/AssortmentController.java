package se.inera.hjalpmedelstjansten.business.assortment.controller;

import lombok.NoArgsConstructor;
import se.inera.hjalpmedelstjansten.business.BaseController;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleMapper;
import se.inera.hjalpmedelstjansten.business.property.view.PropertyLoader;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.EmailController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.*;
import se.inera.hjalpmedelstjansten.model.api.cv.CVMunicipalityAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.*;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCounty;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVMunicipality;

import jakarta.ejb.Stateless;
import jakarta.enterprise.event.Event;
import jakarta.inject.Inject;
import jakarta.mail.MessagingException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;

/**
 * Class for handling business logic of Assortments. This includes talking to the
 * database. Only customers can handle assortments.
 *
 */
@Stateless
@NoArgsConstructor
public class AssortmentController extends BaseController {

    @Inject
    HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    AssortmentValidation assortmentValidation;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    UserController userController;

    @Inject
    OrganizationController organizationController;

    @Inject
    CountyController countyController;

    @Inject
    ArticleController articleController;

    @Inject
    Event<InternalAuditEvent> internalAuditEvent;

    @Inject
    private EmailController emailController;

    private String assortmentExpiredReminderMailSubject;
    private String assortmentExpiredReminderMailBody;

    @Inject
    public AssortmentController(PropertyLoader propertyLoader) {

        assortmentExpiredReminderMailSubject = propertyLoader.getMessage("assortmentExpiredReminderMailSubject");
        assortmentExpiredReminderMailBody = propertyLoader.getMessage("assortmentExpiredReminderMailBody");
    }


    /**
     * Create an assortment on the <code>Organization</code> with the given id.
     *
     * @param organizationUniqueId unique id of the Organization
     * @param assortmentAPI user supplied values
     * @param userAPI user information, needed for business levels validation
     * @param sessionId
     * @param requestIp
     * @return the mapped <code>ArticleAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    public AssortmentAPI createAssortment(long organizationUniqueId, AssortmentAPI assortmentAPI, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createAssortment( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        assortmentValidation.validateForCreate(assortmentAPI);
        Assortment assortment = AssortmentMapper.map(assortmentAPI);

        // add customer organization
        setCustomer(assortment, organizationUniqueId);

        // add managers
        setAssortmentManagers(organizationUniqueId, assortmentAPI, assortment, userAPI);

        // save it
        em.persist(assortment);

        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ASSORTMENT, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, assortment.getUniqueId(), requestIp));
        return AssortmentMapper.map(assortment);
    }

    /**
     * Update an assortment with the given id.
     *
     * @param organizationUniqueId unique id of the Organization
     * @param assortmentUniqueId
     * @param assortmentAPI user supplied values
     * @param userAPI user information, needed for business levels validation
     * @param sessionId
     * @param requestIp
     * @param isSuperAdmin
     * @return the mapped <code>ArticleAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    public AssortmentAPI updateAssortment(long organizationUniqueId,
            long assortmentUniqueId,
            AssortmentAPI assortmentAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp,
            boolean isSuperAdmin) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateAssortment( organizationUniqueId: {0} assortmentUniqueId: {1})", new Object[] {organizationUniqueId, assortmentUniqueId});
        Assortment assortment = getAssortment(organizationUniqueId, assortmentUniqueId, userAPI);
        if( assortment == null ) {
            // assortment either does not exist or the user is not allowed to view it
            return null;
        }
        assortmentValidation.validateForUpdate(assortmentAPI, assortment, isSuperAdmin);

        if( !isSuperAdmin ) {
            // update fields
            assortment.setName(assortmentAPI.getName());
            assortment.setValidFrom(new Date(assortmentAPI.getValidFrom()));
            assortment.setValidTo(assortmentAPI.getValidTo() == null ? null : new Date(assortmentAPI.getValidTo()));
            // add managers
            setAssortmentManagers(organizationUniqueId, assortmentAPI, assortment, userAPI);
            // add categoryExceptions
            setCategoryExceptions(organizationUniqueId, assortmentAPI, assortment, userAPI);
        }

        setCountyAndMunicipalities(assortmentAPI, assortment);

        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ASSORTMENT, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, assortment.getUniqueId(), requestIp));
        return AssortmentMapper.map(assortment);
    }

    /**
     * Get the <code>AssortmentAPI</code> for the given unique id on the
     * specified organization
     *
     * @param organizationUniqueId unique id of the organization
     * @param assortmentUniqueId unique id of the assortment to get
     * @param userAPI user information, needed for business levels validation
     * @param sessionId
     * @return the corresponding <code>AgreementAPI</code>
     */
    public AssortmentAPI getAssortmentAPI(long organizationUniqueId, long assortmentUniqueId, UserAPI userAPI, String sessionId, String requestIp) {
        LOG.log(Level.FINEST, "getAssortmentAPI( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        Assortment assortment = getAssortment(organizationUniqueId, assortmentUniqueId, userAPI);
        if( assortment != null ) {
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ASSORTMENT, InternalAudit.ActionType.VIEW, userAPI.getId(), sessionId, assortmentUniqueId, requestIp));
            return AssortmentMapper.map( assortment );
        }
        return null;
    }

    /**
     * Get the <code>Assortment</code> with the given id on the specified organization.
     * The agreement can be read by the defined customer
     *
     * @param organizationUniqueId unique id of the organization
     * @param assortmentUniqueId unique id of the Assortment to get
     * @param userAPI user information, needed for business levels validation
     * @return the corresponding <code>Assortment</code>
     */
    public Assortment getAssortment(long organizationUniqueId, long assortmentUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        Assortment assortment = em.find(Assortment.class, assortmentUniqueId);
        if( assortment == null ) {
            return null;
        }
        if( !assortment.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user to access assortment: {0} not on given organization: {1} or in user business levels. Returning null.", new Object[] {assortmentUniqueId, organizationUniqueId});
            return null;
        }
        return assortment;
    }

    public List<ExternalCategoryGrouping> getAllOtherExternalCategories(String query, long organizationUniqueId, long assortmentUniqueId) {
        LOG.log(Level.FINEST, "getAllOtherExternalCategories( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        Assortment assortment = em.find(Assortment.class, assortmentUniqueId);
        if( assortment == null ) {
            return null;
        }
        if( !assortment.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user to access assortment: {0} not on given organization: {1} or in user business levels. Returning null.", new Object[] {assortmentUniqueId, organizationUniqueId});
            return null;
        }


        String sql = "SELECT distinct ecg2.* FROM hjmtj.ExternalCategoryGrouping ecg2";
        sql += " INNER JOIN hjmtj.ExternalCategoryGrouping ecg ON ecg2.uniqueId = ecg.parentId";
        sql += " INNER JOIN hjmtj.Product p ON ecg.categoryId = p.categoryId";
        sql += " INNER JOIN hjmtj.Article art ON p.uniqueId = art.basedOnProductId";
        sql += " INNER JOIN hjmtj.AssortmentArticle aa ON art.uniqueId = aa.articleId";
        sql += " INNER JOIN hjmtj.Assortment a ON a.uniqueId = aa.assortmentId";
        sql += " WHERE a.uniqueId = " + assortmentUniqueId;

        if( query != null && !query.isEmpty() && !query.equals("undefined") && !query.equals("null")) {
            sql += " AND p.categoryId != " + query;
        }

        sql += " ORDER BY ecg2.name";



        Query searchQuery = em.createNativeQuery(sql);


        return (List<ExternalCategoryGrouping>) searchQuery.getResultList();
    }


    public List<ExternalCategoryGrouping> getExternalCategories(String query, long organizationUniqueId, long assortmentUniqueId) {
        LOG.log(Level.FINEST, "getExternalCategories( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        Assortment assortment = em.find(Assortment.class, assortmentUniqueId);
        if( assortment == null ) {
            return null;
        }
        if( !assortment.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user to access assortment: {0} not on given organization: {1} or in user business levels. Returning null.", new Object[] {assortmentUniqueId, organizationUniqueId});
            return null;
        }


        String sql = "SELECT distinct ecg2.* FROM hjmtj.ExternalCategoryGrouping ecg2";
        sql += " INNER JOIN hjmtj.ExternalCategoryGrouping ecg ON ecg2.uniqueId = ecg.parentId";
        sql += " INNER JOIN hjmtj.Product p ON ecg.categoryId = p.categoryId";
        sql += " INNER JOIN hjmtj.Article art ON p.uniqueId = art.basedOnProductId";
        sql += " INNER JOIN hjmtj.AssortmentArticle aa ON art.uniqueId = aa.articleId";
        sql += " INNER JOIN hjmtj.Assortment a ON a.uniqueId = aa.assortmentId";
        sql += " WHERE a.uniqueId = " + assortmentUniqueId;

        if( query != null && !query.isEmpty() && !query.equals("undefined") && !query.equals("null")) {
            sql += " AND p.categoryId = " + query;
        }

        sql += " ORDER BY ecg2.name";



        Query searchQuery = em.createNativeQuery(sql);


        return (List<ExternalCategoryGrouping>) searchQuery.getResultList();
    }


    //PELLE
    public List<Long> getCategoryExceptions(long organizationUniqueId, long assortmentUniqueId ) {
        LOG.log(Level.FINEST, "getCategoryExceptions( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        Assortment assortment = em.find(Assortment.class, assortmentUniqueId);
        if( assortment == null ) {
            return null;
        }
        if( !assortment.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user to access assortment: {0} not on given organization: {1} or in user business levels. Returning null.", new Object[] {assortmentUniqueId, organizationUniqueId});
            return null;
        }


        String sql = "SELECT externalCategoryGroupingId FROM hjmtj.AssortmentExternalCategoryGrouping WHERE assortmentId = " + assortmentUniqueId;



        Query searchQuery = em.createNativeQuery(sql);


        return (List<Long>) searchQuery.getResultList();
    }


    /**
     * Get the <code>Assortment</code> with the given id on the specified organization.
     * The agreement can be read by the defined customer
     *
     * @param organizationUniqueId unique id of the organization
     * @param assortmentUniqueId unique id of the Assortment to get
     * @param userAPI user information, needed for business levels validation
     * @param statuses list of statuses to include in search, if null or empty,
     * all statuses are included
     * @param articleTypes list of article types to include in search, if null or empty,
     * all article types are included
     * @param categoryUniqueId unique id of the category
     * @return a list of <code>AgreementPricelistRowAPI</code> matching the query parameters
     */
    public SearchDTO searchArticlesOnAssortment(String query,
                                                int offset,
                                                int limit,
                                                long organizationUniqueId,
                                                long assortmentUniqueId,
                                                long selectedAssortmentUniqueId,
                                                UserAPI userAPI,
                                                String sessionId,
                                                String requestIp, List<Product.Status> statuses, List<Article.Type> articleTypes, Long categoryUniqueId, String sortOrder, String sortType) {
        LOG.log(Level.FINEST, "searchArticlesOnAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        Assortment assortment = getAssortment(organizationUniqueId, assortmentUniqueId, userAPI);
        if( assortment == null ) {
            // assortment either does not exist or the user is not allowed to view it
            return null;
        }

        List<Long> articleIdsOnAssortment = new ArrayList<>();
        if (selectedAssortmentUniqueId != 0) {
            Assortment selectedAssortment = getAssortment(organizationUniqueId, selectedAssortmentUniqueId, userAPI);
            List<Article> articlesOnAssortment = selectedAssortment.getArticles();

            for (Article art : articlesOnAssortment){
                articleIdsOnAssortment.add(art.getUniqueId());
            }
        }

        // count
        StringBuilder countSqlBuilder = new StringBuilder();
        countSqlBuilder.append("SELECT COUNT(ar.uniqueId) FROM Assortment a JOIN a.articles ar ");
        if ((articleTypes != null && !articleTypes.isEmpty()) || categoryUniqueId != null ) {
            countSqlBuilder.append("LEFT JOIN ar.category ac ");
            countSqlBuilder.append("LEFT JOIN ar.basedOnProduct.category apc ");
        }
        countSqlBuilder.append("WHERE a.uniqueId = :assortmentUniqueId ");
        if( query != null && !query.isEmpty() ) {
            countSqlBuilder.append("AND (ar.articleName LIKE :query OR ar.articleNumber LIKE :query) " );
        }
        if (!articleIdsOnAssortment.isEmpty()){
            countSqlBuilder.append("AND ar.uniqueId NOT IN :articleIdsOnAssortment ");
        }
        if (statuses != null && !statuses.isEmpty()) {
            countSqlBuilder.append("AND ar.status IN :statuses ");
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            countSqlBuilder.append("AND (");
            for (int i = 0; i < articleTypes.size(); i++) {
                if (i != 0) {
                    countSqlBuilder.append("OR ");
                }
                countSqlBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(" ) ");
            }
            countSqlBuilder.append(") ");
        }
        if( categoryUniqueId != null ) {
            countSqlBuilder.append("AND (ac.uniqueId = :categoryUniqueId OR apc.uniqueId = :categoryUniqueId) ");
        }
        String countQuerySql = countSqlBuilder.toString();
        LOG.log(Level.FINEST, "countQuerySql: {0}", new Object[] {countQuerySql});
        Query countQuery = em.createQuery(countQuerySql).
                setParameter("assortmentUniqueId", assortmentUniqueId);
        if (!articleIdsOnAssortment.isEmpty()){
            countQuery.setParameter("articleIdsOnAssortment", articleIdsOnAssortment);
        }
        if( query != null && !query.isEmpty() ) {
            countQuery.setParameter("query", "%" + query + "%");
        }
        if (statuses != null && !statuses.isEmpty()) {
            countQuery.setParameter("statuses", statuses);
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            for (int i = 0; i < articleTypes.size(); i++) {
                countQuery.setParameter("type" + i, articleTypes.get(i));
            }
        }
        if( categoryUniqueId != null ) {
            countQuery.setParameter("categoryUniqueId", categoryUniqueId);
        }
        Long count = (Long) countQuery.getSingleResult();

        // search
        StringBuilder searchSqlBuilder = new StringBuilder();
        searchSqlBuilder.append("SELECT ar FROM Assortment a JOIN a.articles ar ");
        if ((articleTypes != null && !articleTypes.isEmpty()) || categoryUniqueId != null) {
            searchSqlBuilder.append("LEFT JOIN ar.category ac ");
            searchSqlBuilder.append("LEFT JOIN ar.basedOnProduct.category apc ");
        }
        searchSqlBuilder.append("WHERE a.uniqueId = :assortmentUniqueId ");
        if( query != null && !query.isEmpty() ) {
            searchSqlBuilder.append("AND (ar.articleName LIKE :query OR ar.articleNumber LIKE :query) " );
        }
        if (!articleIdsOnAssortment.isEmpty()){
            searchSqlBuilder.append("AND ar.uniqueId NOT IN :articleIdsOnAssortment ");
        }
        if (statuses != null && !statuses.isEmpty()) {
            searchSqlBuilder.append("AND ar.status IN :statuses ");
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            searchSqlBuilder.append("AND (");
            for (int i = 0; i < articleTypes.size(); i++) {
                if (i != 0) {
                    searchSqlBuilder.append("OR ");
                }
                searchSqlBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(" ) ");
            }
            searchSqlBuilder.append(") ");
        }
        if (categoryUniqueId != null) {
            searchSqlBuilder.append("AND (ac.uniqueId = :categoryUniqueId OR apc.uniqueId = :categoryUniqueId) ");
        }

        if (!Objects.equals(sortType, "")) {
            sortType = sortType.toUpperCase();
            sortOrder = sortOrder.toUpperCase();

            switch (sortType) {
                case "NAME":
                    searchSqlBuilder.append("ORDER BY ar.articleName ");
                    break;
                case "NUMBER":
                    searchSqlBuilder.append("ORDER BY ar.articleNumber ");
                    break;
                case "ORGANIZATIONNAME":
                    searchSqlBuilder.append("ORDER BY ar.organization.organizationName ");
                    break;
                case "BASEDONPRODUCT":
                    searchSqlBuilder.append("ORDER BY ar.basedOnProduct.productName ");
                    break;
                case "ARTICLETYPE":
                    searchSqlBuilder.append("ORDER BY ar.basedOnProduct.category.articleType ");
                    break;
                case "CODE":
                    searchSqlBuilder.append("ORDER BY ar.basedOnProduct.category.code ");
                    break;
                case "STATUS":
                    searchSqlBuilder.append("ORDER BY ar.status ");
                    break;
            }

            if (sortOrder.equals("DESC"))
                searchSqlBuilder.append("DESC ");
            else
                searchSqlBuilder.append("ASC ");
        } else {
            searchSqlBuilder.append("ORDER BY ar.articleName ASC");
        }



        String searchQuerySql = searchSqlBuilder.toString();
        LOG.log(Level.FINEST, "searchQuerySql: {0}", new Object[] {searchQuerySql});
        Query searchQuery = em.createQuery(searchQuerySql).
                setParameter("assortmentUniqueId", assortmentUniqueId);
        if (!articleIdsOnAssortment.isEmpty()){
            searchQuery.setParameter("articleIdsOnAssortment", articleIdsOnAssortment);
        }
        if( query != null && !query.isEmpty() ) {
            searchQuery.setParameter("query", "%" + query + "%");
        }
        if (statuses != null && !statuses.isEmpty()) {
            searchQuery.setParameter("statuses", statuses);
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            for (int i = 0; i < articleTypes.size(); i++) {
                searchQuery.setParameter("type" + i, articleTypes.get(i));
            }
        }
        if (categoryUniqueId != null) {
            searchQuery.setParameter("categoryUniqueId", categoryUniqueId);
        }

        List<ArticleAPI> articleAPIs = ArticleMapper.map(searchQuery.
            setMaxResults(limit).
            setFirstResult(offset).
            getResultList(), false);
        return new SearchDTO(count, articleAPIs);
    }

    public SearchDTO searchArticlesOnAssortmentAll(String query,
                                                long organizationUniqueId,
                                                long assortmentUniqueId,
                                                long selectedAssortmentUniqueId,
                                                UserAPI userAPI,
                                                String sessionId,
                                                String requestIp, List<Product.Status> statuses, List<Article.Type> articleTypes, Long categoryUniqueId) {
        LOG.log(Level.FINEST, "searchArticlesOnAssortmentAll( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        Assortment assortment = getAssortment(organizationUniqueId, assortmentUniqueId, userAPI);
        if( assortment == null ) {
            // assortment either does not exist or the user is not allowed to view it
            return null;
        }

        List<Long> articleIdsOnAssortment = new ArrayList<>();
        if (selectedAssortmentUniqueId != 0) {
            Assortment selectedAssortment = getAssortment(organizationUniqueId, selectedAssortmentUniqueId, userAPI);
            List<Article> articlesOnAssortment = selectedAssortment.getArticles();

            for (Article art : articlesOnAssortment){
                articleIdsOnAssortment.add(art.getUniqueId());
            }
        }

        /*
        // count
        StringBuilder countSqlBuilder = new StringBuilder();
        countSqlBuilder.append("SELECT COUNT(ar.uniqueId) FROM Assortment a JOIN a.articles ar ");
        if ((articleTypes != null && !articleTypes.isEmpty()) || categoryUniqueId != null ) {
            countSqlBuilder.append("LEFT JOIN ar.category ac ");
            countSqlBuilder.append("LEFT JOIN ar.basedOnProduct.category apc ");
        }
        countSqlBuilder.append("WHERE a.uniqueId = :assortmentUniqueId ");
        if( query != null && !query.isEmpty() ) {
            countSqlBuilder.append("AND (ar.articleName LIKE :query OR ar.articleNumber LIKE :query) " );
        }
        if (!articleIdsOnAssortment.isEmpty()){
            countSqlBuilder.append("AND ar.uniqueId NOT IN :articleIdsOnAssortment ");
        }
        if (statuses != null && !statuses.isEmpty()) {
            countSqlBuilder.append("AND ar.status IN :statuses ");
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            countSqlBuilder.append("AND (");
            for (int i = 0; i < articleTypes.size(); i++) {
                if (i != 0) {
                    countSqlBuilder.append("OR ");
                }
                countSqlBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(" ) ");
            }
            countSqlBuilder.append(") ");
        }
        if( categoryUniqueId != null ) {
            countSqlBuilder.append("AND (ac.uniqueId = :categoryUniqueId OR apc.uniqueId = :categoryUniqueId) ");
        }
        String countQuerySql = countSqlBuilder.toString();
        LOG.log(Level.FINEST, "countQuerySql: {0}", new Object[] {countQuerySql});
        Query countQuery = em.createQuery(countQuerySql).
            setParameter("assortmentUniqueId", assortmentUniqueId);
        if (!articleIdsOnAssortment.isEmpty()){
            countQuery.setParameter("articleIdsOnAssortment", articleIdsOnAssortment);
        }
        if( query != null && !query.isEmpty() ) {
            countQuery.setParameter("query", "%" + query + "%");
        }
        if (statuses != null && !statuses.isEmpty()) {
            countQuery.setParameter("statuses", statuses);
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            for (int i = 0; i < articleTypes.size(); i++) {
                countQuery.setParameter("type" + i, articleTypes.get(i));
            }
        }
        if( categoryUniqueId != null ) {
            countQuery.setParameter("categoryUniqueId", categoryUniqueId);
        }
        Long count = (Long) countQuery.getSingleResult();
*/
        // search
        StringBuilder searchSqlBuilder = new StringBuilder();
        searchSqlBuilder.append("SELECT ar FROM Assortment a JOIN a.articles ar ");
        if ((articleTypes != null && !articleTypes.isEmpty()) || categoryUniqueId != null) {
            searchSqlBuilder.append("LEFT JOIN ar.category ac ");
            searchSqlBuilder.append("LEFT JOIN ar.basedOnProduct.category apc ");
        }
        searchSqlBuilder.append("WHERE a.uniqueId = :assortmentUniqueId ");
        if( query != null && !query.isEmpty() ) {
            searchSqlBuilder.append("AND (ar.articleName LIKE :query OR ar.articleNumber LIKE :query) " );
        }
        if (!articleIdsOnAssortment.isEmpty()){
            searchSqlBuilder.append("AND ar.uniqueId NOT IN :articleIdsOnAssortment ");
        }
        if (statuses != null && !statuses.isEmpty()) {
            searchSqlBuilder.append("AND ar.status IN :statuses ");
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            searchSqlBuilder.append("AND (");
            for (int i = 0; i < articleTypes.size(); i++) {
                if (i != 0) {
                    searchSqlBuilder.append("OR ");
                }
                searchSqlBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(" ) ");
            }
            searchSqlBuilder.append(") ");
        }
        if (categoryUniqueId != null) {
            searchSqlBuilder.append("AND (ac.uniqueId = :categoryUniqueId OR apc.uniqueId = :categoryUniqueId) ");
        }
        searchSqlBuilder.append("ORDER BY ar.articleName ASC");
        String searchQuerySql = searchSqlBuilder.toString();
        LOG.log(Level.FINEST, "searchQuerySql: {0}", new Object[] {searchQuerySql});
        Query searchQuery = em.createQuery(searchQuerySql).
            setParameter("assortmentUniqueId", assortmentUniqueId);
        if (!articleIdsOnAssortment.isEmpty()){
            searchQuery.setParameter("articleIdsOnAssortment", articleIdsOnAssortment);
        }
        if( query != null && !query.isEmpty() ) {
            searchQuery.setParameter("query", "%" + query + "%");
        }
        if (statuses != null && !statuses.isEmpty()) {
            searchQuery.setParameter("statuses", statuses);
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            for (int i = 0; i < articleTypes.size(); i++) {
                searchQuery.setParameter("type" + i, articleTypes.get(i));
            }
        }
        if (categoryUniqueId != null) {
            searchQuery.setParameter("categoryUniqueId", categoryUniqueId);
        }

        List<ArticleAPI> articleAPIs = ArticleMapper.map(searchQuery.
            getResultList(), false);
        return new SearchDTO((long)articleAPIs.size(), articleAPIs);
    }

    /**
     * Add articles to given assortment. A user that has all permissions
     * assortmentarticle:create_all
     * can add articles to any assortment in the given organization. A user that only
     * has
     * assortmentarticle:create_own
     * can only add articles to assortments on own organization where the user is
     * a designated "manager".
     *
     * @param organizationUniqueId
     * @param assortmentUniqueId
     * @param articleAPIs
     * @param userAPI
     * @param sessionId
     * @param requestIp
     * @param hasAllPermissions true if user can add articles to any assortment in organization
     * @return
     * @throws HjalpmedelstjanstenValidationException
     */
    public AssortmentAPI addArticlesToAssortment(long organizationUniqueId, long assortmentUniqueId, List<ArticleAPI> articleAPIs, UserAPI userAPI, String sessionId, String requestIp, boolean hasAllPermissions) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "addArticlesToAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1}, hasAllPermissions: {2} )", new Object[] {organizationUniqueId, assortmentUniqueId, hasAllPermissions});
        Assortment assortment = getAssortment(organizationUniqueId, assortmentUniqueId, userAPI);
        if( assortment == null ) {
            // assortment either does not exist or the user is not allowed to view it
            return null;
        }
        if( !hasAllPermissions ) {
            // user can only add articles to assortment where manager
            if( !userIsAssortmentManager(assortment, userAPI) ) {
                LOG.log( Level.WARNING, "User {0} does not have the right to add articles to assortment: {1}", new Object[]{userAPI.getId(), assortment.getUniqueId()});
                return null;
            }
        }
        if( assortment.getArticles() == null ) {
            assortment.setArticles(new ArrayList<>());
        }
        List<Long> currentArticleUniqueIdsInAssortment = getAssortmentArticleUniqueIds(assortment.getUniqueId());
        if( articleAPIs != null && !articleAPIs.isEmpty() ) {
            for( ArticleAPI articleAPI : articleAPIs ) {
                // article
                Article article = articleController.getArticle(articleAPI.getId(), userAPI);
                if( article == null ) {
                    LOG.log( Level.WARNING, "Attempt by user: {0} to add article: {1} which doesn't exist to assortment: {2}", new Object[] {userAPI.getId(), articleAPI.getId(), assortment.getUniqueId()});
                    throw validationMessageService.generateValidationException("article", "assortment.article.notExist");
                }
                if( currentArticleUniqueIdsInAssortment.contains(article.getUniqueId())) {
                    LOG.log( Level.FINEST, "Article {0} already exists in assortment: {1}. Skip.", new Object[] {article.getUniqueId(), assortment.getUniqueId()});
                    continue;
                }
                assortment.getArticles().add(article);
            }
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ASSORTMENT, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, assortment.getUniqueId(), requestIp));
        }
        return AssortmentMapper.map(assortment);
    }

    /**
     * Delete given assortment (and all articlerows). A user that has all permissions
     * assortmentarticle:delete_all
     * can delete any assortment in the given organization. A user that only
     * has
     * assortmentarticle:delete_own
     * can only delete assortments on own organization where the user is
     * a designated "manager".
     *
     * @param organizationUniqueId
     * @param assortmentUniqueId
     * @param userAPI
     * @param sessionId
     * @param requestIp
     * @param hasAllPermissions true if user can delete any assortment in organization
     * @return
     * @throws HjalpmedelstjanstenValidationException
     */
    public AssortmentAPI deleteAssortment(long organizationUniqueId, long assortmentUniqueId, UserAPI userAPI, String sessionId, String requestIp, boolean hasAllPermissions) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1}, hasAllPermissions: {2} )", new Object[] {organizationUniqueId, assortmentUniqueId, hasAllPermissions});
        Assortment assortment = getAssortment(organizationUniqueId, assortmentUniqueId, userAPI);
        if( assortment == null ) {
            // assortment either does not exist or the user is not allowed to view it
            return null;
        }
        if( !hasAllPermissions ) {
            // user can only delete assortment where manager
            if( !userIsAssortmentManager(assortment, userAPI) ) {
                LOG.log( Level.WARNING, "User {0} does not have the right to delete assortment: {1}", new Object[]{userAPI.getId(), assortment.getUniqueId()});
                return null;
            }
        }

        doDeleteAssortment(assortmentUniqueId);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ASSORTMENT, InternalAudit.ActionType.DELETE, userAPI.getId(), sessionId, assortmentUniqueId, requestIp));
        return AssortmentMapper.map(assortment);
    }

    public boolean doDeleteAssortment(long assortmentUniqueId) throws HjalpmedelstjanstenValidationException {
        try {
            //Assortment assortment = getAssortment(organizationUniqueId, assortmentUniqueId, userAPI);
            //AssortmentAPI assortmentAPI = getAssortmentAPI( organizationUniqueId, assortmentUniqueId, userAPI, sessionId, requestIp);

            StringBuilder sb = new StringBuilder();

            sb.append("DELETE FROM hjmtj.AssortmentArticle WHERE assortmentId = :assortmentUniqueId ");
            em.createNativeQuery(sb.toString()).setParameter("assortmentUniqueId", assortmentUniqueId).executeUpdate();
            sb.delete(0, sb.length());
            sb.append("DELETE FROM hjmtj.AssortmentUserEngagement WHERE assortmentId = :assortmentUniqueId ");
            em.createNativeQuery(sb.toString()).setParameter("assortmentUniqueId", assortmentUniqueId).executeUpdate();
            sb.delete(0, sb.length());
            sb.append("DELETE FROM hjmtj.AssortmentCVMunicipality WHERE assortmentId = :assortmentUniqueId ");
            em.createNativeQuery(sb.toString()).setParameter("assortmentUniqueId", assortmentUniqueId).executeUpdate();
            sb.delete(0, sb.length());
            sb.append("DELETE FROM hjmtj.Assortment WHERE uniqueId = :assortmentUniqueId ");
            em.createNativeQuery(sb.toString()).setParameter("assortmentUniqueId", assortmentUniqueId).executeUpdate();

            //em.remove(assortment);

        }
        catch(Exception e) {
            LOG.log(Level.WARNING, e.getMessage());
            throw validationMessageService.generateValidationException("code", "Fel när utbud och samtliga artikelrader skulle tas bort");
        }
        return true;
    }


    /**
     * Delete articles from given assortment. A user that has all permissions
     * assortmentarticle:delete_all
     * can delete articles from any assortment in the given organization. A user that only
     * has
     * assortmentarticle:delete_own
     * can only delete articles from assortments on own organization where the user is
     * a designated "manager".
     *
     * @param organizationUniqueId
     * @param assortmentUniqueId
     * @param articleUniqueId unique id of the article to remove from the assortment
     * @param userAPI
     * @param sessionId
     * @param requestIp
     * @param hasAllPermissions true if user can delete articles from any assortment in organization
     * @return
     * @throws HjalpmedelstjanstenValidationException
     */
    public AssortmentAPI deleteArticleFromAssortment(long organizationUniqueId, long assortmentUniqueId, long articleUniqueId, UserAPI userAPI, String sessionId, String requestIp, boolean hasAllPermissions) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteArticlesFromAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1}, hasAllPermissions: {2} )", new Object[] {organizationUniqueId, assortmentUniqueId, hasAllPermissions});
        Assortment assortment = getAssortment(organizationUniqueId, assortmentUniqueId, userAPI);
        if( assortment == null ) {
            // assortment either does not exist or the user is not allowed to view it
            return null;
        }
        if( !hasAllPermissions ) {
            // user can only remove articles from assortment where manager
            if( !userIsAssortmentManager(assortment, userAPI) ) {
                LOG.log( Level.WARNING, "User {0} does not have the right to delete articles from assortment: {1}", new Object[]{userAPI.getId(), assortment.getUniqueId()});
                return null;
            }
        }
        if( assortment.getArticles() == null || assortment.getArticles().isEmpty() ) {
            LOG.log( Level.INFO, "Attempt to remove articles from assortment: {0} that has no articles.", new Object[]{assortment.getUniqueId()});
        } else {
            // article
            Article article = articleController.getArticle(articleUniqueId, userAPI);
            if( article == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to delete article: {1} which doesn't exist from assortment: {2}", new Object[] {userAPI.getId(), articleUniqueId, assortment.getUniqueId()});
                throw validationMessageService.generateValidationException("article", "assortment.article.delete.notExist");
            }
            assortment.getArticles().remove(article);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ASSORTMENT, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, assortment.getUniqueId(), requestIp));
        }
        return AssortmentMapper.map(assortment);
    }

    /**
     * When an article is changed in its "essence" (like switches based on product)
     * all references to that article must be removed.
     *
     * @param article the article to find assortments for
     */
    public void deleteRowsByArticle( Article article ) {
        LOG.log(Level.FINEST, "deleteRowsByArticle( article->uniqueId: {0} )", new Object[] {article.getUniqueId()});
        List<Assortment> assortments =
                em.createNamedQuery(Assortment.FIND_BY_ARTICLE).
                setParameter("articleUniqueId", article.getUniqueId()).
                getResultList();
        if( assortments != null && !assortments.isEmpty() ) {
            for( Assortment assortment : assortments ) {
                assortment.getArticles().remove(article);
            }
        }
    }

    /**
     * Set customer organization on assortment
     *
     * @param assortment the assortment to set organization on
     * @param organizationUniqueId unique id of the customer organization
     * @throws HjalpmedelstjanstenValidationException if the organizatino doesn't exist
     * or is not of type CUSTOMER
     */
    void setCustomer(Assortment assortment, long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setCustomer( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        Organization customerOrganization = organizationController.getOrganization(organizationUniqueId);
        if( customerOrganization == null ) {
            throw validationMessageService.generateValidationException("customer", "assortment.customerOrganization.notExists");
        }
        if( customerOrganization.getOrganizationType() != Organization.OrganizationType.CUSTOMER ) {
            throw validationMessageService.generateValidationException("customer", "assortment.customerOrganization.invalidType");
        }
        assortment.setCustomer(customerOrganization);
    }

    /**
     * Set the manager for this assortment. Only the customer can do this.
     * Each person added must have the role CustomerAssignedAssortmentManager.
     *
     * @param organizationUniqueId unique id of the organization
     * @param assortmentAPI user supplied values
     * @param assortment assortment to add managers to
     * @param userAPI the currently logged in person
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * user does not have valid role or doesn't exist
     */
    void setAssortmentManagers(long organizationUniqueId, AssortmentAPI assortmentAPI, Assortment assortment, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setAssortmentManagers( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        if( assortmentAPI.getManagers() != null && !assortmentAPI.getManagers().isEmpty() ) {
            List<UserEngagement> assortmentManagers = new ArrayList<>();
            for( UserAPI assortmentManagerAPI : assortmentAPI.getManagers() ) {
                UserEngagement assortmentManagerUserEngagement = userController.getUserEngagement(organizationUniqueId, assortmentManagerAPI.getId());
                if( assortmentManagerUserEngagement == null ) {
                    LOG.log( Level.WARNING, "Attempt by user: {0} to add assortment manager: {1} which does not exist or does not belong to organization: {2}", new Object[] {userAPI.getId(), assortmentManagerAPI.getId(), organizationUniqueId});
                    throw validationMessageService.generateValidationException("customerPricelistApprovers", "agreement.customerPricelistApprovers.notExist");
                }
                checkAssortmentManagerRoles(assortmentManagerUserEngagement, userAPI);
                assortmentManagers.add(assortmentManagerUserEngagement);
            }
            assortment.setManagers(assortmentManagers);
        } else {
            assortment.setManagers(null);
        }
    }

    void setCategoryExceptions(long organizationUniqueId, AssortmentAPI assortmentAPI, Assortment assortment, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setCategoryExceptions( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});

        List<ExternalCategoryGrouping> categoryExceptions = new ArrayList<>();

        if( assortmentAPI.getExternalCategoriesGroupings() != null && !assortmentAPI.getExternalCategoriesGroupings().isEmpty() ) {
            for(ExternalCategoryGroupingAPI categoryGroupingAPI : assortmentAPI.getExternalCategoriesGroupings() ) {
                ExternalCategoryGrouping categoryException = new ExternalCategoryGrouping();

                categoryException.setUniqueId(categoryGroupingAPI.getId());
                categoryExceptions.add(categoryException);
            }
        }
        assortment.setExternalCategoryGroupings(categoryExceptions);
    }

    private void checkAssortmentManagerRoles(UserEngagement assortmentManagerUserEngagement, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        boolean roleFound = false;
        if( assortmentManagerUserEngagement.getRoles() != null && !assortmentManagerUserEngagement.getRoles().isEmpty() ) {
            for( UserRole userRole : assortmentManagerUserEngagement.getRoles() ) {
                if( userRole.getName() == UserRole.RoleName.CustomerAssignedAssortmentManager ) {
                    roleFound = true;
                    break;
                }
            }
        }
        if( !roleFound ) {
            LOG.log(Level.WARNING, "attempt by user: {0} to set assortment manager which does not have valid role, this is an odd situation and should be checked", new Object[] {userAPI.getId()});
            throw validationMessageService.generateValidationException("managers", "assortment.managers.wrongRole", assortmentManagerUserEngagement.getUserAccount().getFullName());
        }
    }

    /**
     * Set county and municipalities on assortment
     *
     * @param assortment the assortment to set organization on
     * @throws HjalpmedelstjanstenValidationException if the organizatino doesn't exist
     * or is not of type CUSTOMER
     */
    void setCountyAndMunicipalities(AssortmentAPI assortmentAPI, Assortment assortment) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setCountyAndMunicipalities( assortment->uniqueId: {0} )", new Object[] {assortment.getUniqueId()});

        if (assortmentAPI.getCounty() == null) {
            return;
        }

        // county
        CVCounty county = countyController.findCountyById(assortmentAPI.getCounty().getId());
        if( county == null ) {
            throw validationMessageService.generateValidationException("county", "assortment.county.notExists");
        }
        assortment.setCounty(county);

        // municipalities
        assortment.setMunicipalities(new ArrayList<>());
        if( county.getShowMunicipalities() ) {
            if( assortmentAPI.getMunicipalities() == null || assortmentAPI.getMunicipalities().isEmpty() ) {
                throw validationMessageService.generateValidationException("municipalities", "assortment.municipalities.notNull");
            }
            for( CVMunicipalityAPI municipalityAPI : assortmentAPI.getMunicipalities() ) {
                CVMunicipality municipality = countyController.findMunicipalityById(municipalityAPI.getId());
                // municipality must be in the municipalities of the county we already fetched
                if( !county.getMunicipalities().contains(municipality) ) {
                    throw validationMessageService.generateValidationException("municipalities", "assortment.municipalities.notInCounty");
                }
                assortment.getMunicipalities().add(municipality);
            }
        }
    }

    /**
     * Search agreements for the user supplied query. Search is paginated so only
     * results valid for given offset and limit is returned.
     *
     * @param organizationUniqueId
     * @param queryString user supplied query
     * @param statuses
     * @param onlyWhereManager
     * @param userAPI user information, needed for business levels validation
     * @param offset start returning from this search result
     * @param limit maximum number of search results to return
     * @return a SearchDTO object of number of hits and list of <code>AssortmentAPI</code> matching the query parameters
     */
    public SearchDTO searchAssortments(Long organizationUniqueId,
            String queryString,
            List<Assortment.Status> statuses,
            boolean onlyWhereManager,
            UserAPI userAPI,
            int offset,
            int limit) {
        LOG.log(Level.FINEST, "searchAssortments( offset: {0}, limit: {1}, organizationUniqueId: {2} )", new Object[] {offset, limit, organizationUniqueId});

        // create SQL
        boolean whereSet = false;
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT a FROM Assortment a ");
        if( onlyWhereManager ) {
            queryBuilder.append("JOIN a.managers am ");
        }
        if( organizationUniqueId != null ) {
            queryBuilder.append( "WHERE a.customer.uniqueId = :customerOrganizationId " );
            whereSet = true;
        }
        if( queryString != null && !queryString.isEmpty() ) {
            queryBuilder.append(whereSet ? "AND ": "WHERE " );
            queryBuilder.append("a.name LIKE :query ");
            whereSet = true;
        }
        if( onlyWhereManager ) {
            queryBuilder.append(whereSet ? "AND ": "WHERE " );
            queryBuilder.append("(am.uniqueId = :userEngagementId) ");
            whereSet = true;
        }
        if( statuses != null && !statuses.isEmpty() ) {
            queryBuilder.append(whereSet ? "AND (": "WHERE (" );
            for( int i = 0; i<statuses.size(); i++ ) {
                Assortment.Status status = statuses.get(i);
                if( status == Assortment.Status.CURRENT ) {
                    queryBuilder.append("(a.validFrom < :now AND (a.validTo IS NULL OR a.validTo > :now)) ");
                } else if( status == Assortment.Status.FUTURE ) {
                    queryBuilder.append("(a.validFrom > :now) ");
                } else {
                    // DISCONTINUED
                    queryBuilder.append("(a.validTo < :now) ");
                }
                if( i < (statuses.size()-1) ) {
                    queryBuilder.append("OR ");
                }
            }
            queryBuilder.append(") ");
            whereSet = true;
        }
        queryBuilder.append("ORDER BY a.name");
        String querySql = queryBuilder.toString();
        LOG.log( Level.FINEST, "querySql: {0}", new Object[] {querySql});
        // create query and populate parameters
        Query query = em.createQuery(querySql);
        if( queryString != null && !queryString.isEmpty() ) {
            query.setParameter("query", "%" + queryString + "%");
        }
        if( organizationUniqueId != null ) {
            query.setParameter("customerOrganizationId", organizationUniqueId);
        }
        if( onlyWhereManager ) {
            query.setParameter("userEngagementId", userAPI.getId());
        }
        if( statuses != null && !statuses.isEmpty() ) {
            query.setParameter("now", new Date());
        }

        // execute query
        List<Assortment> assortments = query.setMaxResults(limit).
                    setFirstResult(offset).
                    getResultList();

        long count = countSearchAssortments(organizationUniqueId,
                queryString,
                statuses,
                onlyWhereManager,
                userAPI);
        return new SearchDTO(count, AssortmentMapper.map(assortments));
    }

    /**
     * Counts the total number of agreements that matches a user supplied search query
     *
     * @param queryString user supplied query
     * @return total number of agreements matching the query parameters
     */
    private long countSearchAssortments(Long organizationUniqueId,
            String queryString,
            List<Assortment.Status> statuses,
            boolean onlyWhereManager,
            UserAPI userAPI) {
        LOG.log(Level.FINEST, "countSearchAssortments( ... )");

        // create SQL
        boolean whereSet = false;
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT COUNT(a.uniqueId) FROM Assortment a ");
        if( onlyWhereManager ) {
            queryBuilder.append("JOIN a.managers am ");
        }
        if( organizationUniqueId != null ) {
            queryBuilder.append( "WHERE a.customer.uniqueId = :customerOrganizationId " );
            whereSet = true;
        }
        if( queryString != null && !queryString.isEmpty() ) {
            queryBuilder.append(whereSet ? "AND ": "WHERE " );
            queryBuilder.append("a.name LIKE :query ");
            whereSet = true;
        }
        if( onlyWhereManager ) {
            queryBuilder.append(whereSet ? "AND ": "WHERE " );
            queryBuilder.append("(am.uniqueId = :userEngagementId) ");
            whereSet = true;
        }
        if( statuses != null && !statuses.isEmpty() ) {
            queryBuilder.append(whereSet ? "AND (": "WHERE (" );
            for( int i = 0; i<statuses.size(); i++ ) {
                Assortment.Status status = statuses.get(i);
                if( status == Assortment.Status.CURRENT ) {
                    queryBuilder.append("(a.validFrom < :now AND (a.validTo IS NULL OR a.validTo > :now)) ");
                } else if( status == Assortment.Status.FUTURE ) {
                    queryBuilder.append("(a.validFrom > :now) ");
                } else {
                    // DISCONTINUED
                    queryBuilder.append("(a.validTo < :now) ");
                }
                if( i < (statuses.size()-1) ) {
                    queryBuilder.append("OR ");
                }
            }
            queryBuilder.append(") ");
            whereSet = true;
        }
        String querySql = queryBuilder.toString();
        LOG.log( Level.FINEST, "querySql: {0}", new Object[] {querySql});

        // create query and populate parameters
        Query query = em.createQuery(querySql);
        if( queryString != null && !queryString.isEmpty() ) {
            query.setParameter("query", "%" + queryString + "%");
        }
        if( organizationUniqueId != null ) {
            query.setParameter("customerOrganizationId", organizationUniqueId);
        }
        if( onlyWhereManager ) {
            query.setParameter("userEngagementId", userAPI.getId());
        }
        if( statuses != null && !statuses.isEmpty() ) {
            query.setParameter("now", new Date());
        }
        return (Long) query.getSingleResult();
    }

    private List<Long> getAssortmentArticleUniqueIds(Long assortmentUniqueId) {
        LOG.log(Level.FINEST, "getAssortmentArticleUniqueIds( assortmentUniqueId: {0})", new Object[] {assortmentUniqueId});
        return (List<Long>) em.createNamedQuery(Assortment.GET_ARTICLE_UNIQUE_IDS).
                setParameter("assortmentUniqueId", assortmentUniqueId).
                getResultList();
    }

    private boolean userIsAssortmentManager(Assortment assortment, UserAPI userAPI) {
        LOG.log(Level.FINEST, "userIsAssortmentManager( assortment->uniqueId: {0})", new Object[] {assortment.getUniqueId()});
        boolean isManager = false;
        if( assortment.getManagers() != null && !assortment.getManagers().isEmpty() ) {
            for( UserEngagement userEngagement : assortment.getManagers() ) {
                if( userEngagement.getUniqueId().equals(userAPI.getId()) ) {
                    isManager = true;
                    break;
                }
            }
        }
        return isManager;
    }

    public void sendEmailAssortmentExpired() {
        //find the assortments that expires in a week (7 days)
        String sql = "SELECT uniqueId FROM hjmtj.Assortment WHERE substring(validTo, 1, 10) = DATE_ADD(current_date(), INTERVAL 7 DAY)";
        List<Object> assortmentIds = em.createNativeQuery(sql).getResultList();

        for (Object assortmentId : assortmentIds){
            sql = "SELECT name, substring(validTo, 1, 10) FROM hjmtj.Assortment WHERE uniqueId = " + assortmentId;

            Object[] assortmentNameAndDate = (Object[]) em.createNativeQuery(sql).getSingleResult();
            String mailBody = String.format(assortmentExpiredReminderMailBody, assortmentNameAndDate[0].toString(), assortmentNameAndDate[1].toString());

            sql = "SELECT ea.email"; //find the email addresses of the assigned assortments managers
            sql+= " FROM hjmtj.Assortment a";
            sql+= " INNER JOIN hjmtj.AssortmentUserEngagement aue ON a.uniqueId = aue.assortmentId";
            sql+= " INNER JOIN hjmtj.UserEngagement ue ON aue.userEngagementId = ue.uniqueId";
            sql+= " INNER JOIN hjmtj.UserAccount ua ON ue.userId = ua.uniqueId";
            sql+= " INNER JOIN hjmtj.ElectronicAddress ea ON ua.primaryElectronicAdressId = ea.uniqueId";
            sql+= " WHERE substring(a.validTo, 1, 10) = DATE_ADD(current_date(), INTERVAL 7 DAY)";
            sql+= " AND a.uniqueId = " + assortmentId;
            sql+= " UNION";
            sql+= " SELECT ea.email"; //find the email addresses of the assortment managers
            sql+= " FROM hjmtj.Assortment a";
            sql+= " INNER JOIN hjmtj.UserEngagement ue ON a.customerOrganizationId = ue.orgId";
            sql+= " INNER JOIN hjmtj.UserEngagementUserRole ueur ON ue.uniqueId = ueur.userEngagementId";
            sql+= " INNER JOIN hjmtj.UserRole ur ON ur.uniqueId = ueur.roleId";
            sql+= " INNER JOIN hjmtj.UserAccount ua ON ue.userId = ua.uniqueId";
            sql+= " INNER JOIN hjmtj.ElectronicAddress ea ON ua.primaryElectronicAdressId = ea.uniqueId";
            sql+= " WHERE substring(a.validTo, 1, 10) = DATE_ADD(current_date(), INTERVAL 7 DAY)";
            sql+= " AND ur.name = 'CustomerAssortmentManager'";
            sql+= " AND (ue.validTo IS NULL OR ue.validTo >= CURDATE())";
            sql+= " AND a.uniqueId = " + assortmentId;
            List<String> addresses = em.createNativeQuery(sql).getResultList();

            for (String address : addresses) {
                try {
                    if(address != null && !address.isEmpty()) {
                        emailController.send(address, assortmentExpiredReminderMailSubject, mailBody );
                    }
                } catch (MessagingException ex) {
                    LOG.log(Level.SEVERE, "Failed to send notification message to customer regarding expired assortments");
                }

            }
        }
    }

    public List<Object> getArticlesDiscontinued(Long organizationUniqueId, UserAPI userAPI) {

        String sql = "SELECT cc.* FROM hjmtj.ChangeChecker cc WHERE cc.type = 'ASSORTMENT_ARTICLE_DISCONTINUED' AND cc.userId = " + userAPI.getId();
        List<ChangeChecker> changeCheckers = em.createNativeQuery(sql, ChangeChecker.class).getResultList();


        List<Object> articleAssortmentPairList = new ArrayList<>();

        changeCheckers.forEach(cc -> {
            List<Object> articleAssortmentPair = new ArrayList<>();

            long articleIdLong = cc.getChangeId();
            long assortmentIdLong = cc.getAssortmentId();

            articleAssortmentPair.add(ArticleMapper.map(articleController.getArticle(articleIdLong, userAPI), true, null, userAPI));
            articleAssortmentPair.add(AssortmentMapper.map(getAssortment(organizationUniqueId, assortmentIdLong, userAPI)));
            articleAssortmentPairList.add(articleAssortmentPair);
        });

        return articleAssortmentPairList;
    }


    public List<ChangeChecker> removeArticleDiscontinuedNotice(long assortmentId, long articleId, long userId, String remove) {
        //ta bort alla notiser
        String sql = "SELECT * FROM hjmtj.ChangeChecker cc WHERE cc.userId = " + userId + " AND cc.type = 'ASSORTMENT_ARTICLE_DISCONTINUED' " ;
        //ta bara bort en notis
        if (Objects.equals(remove, "one")){
            sql += "AND cc.changeId = " + articleId + " AND cc.assortmentId = " + assortmentId;
        }
        //ta bort alla notiser gällande specifik artikel
        if (Objects.equals(remove, "art")){
            sql += "AND cc.changeId = " + articleId;
        }
        //ta bort alla notiser gällande specifikt utbud
        if (Objects.equals(remove, "ass")){
            sql += "AND cc.assortmentId = " + assortmentId;
        }
        List<ChangeChecker> cc = em.createNativeQuery(sql, ChangeChecker.class).getResultList();

        cc.forEach(c ->{
            ChangeChecker ccToRemove = em.find(ChangeChecker.class, c.getUniqueId());
            em.remove(ccToRemove);
        });
        return cc;
    }
}
