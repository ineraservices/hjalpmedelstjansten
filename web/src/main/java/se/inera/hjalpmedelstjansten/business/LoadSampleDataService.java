package se.inera.hjalpmedelstjansten.business;

import jakarta.persistence.EntityExistsException;
import se.inera.hjalpmedelstjansten.business.importstatus.controller.ImportStatusController;
import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductController;
import se.inera.hjalpmedelstjansten.business.security.controller.SecurityController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserMapper;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.api.DeletedUserAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.ImportStatusAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEDirectiveAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEStandardAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVOrderUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.media.MediaAPI;
import se.inera.hjalpmedelstjansten.model.api.media.MediaImageAPI;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelist;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificPropertyListValue;
import se.inera.hjalpmedelstjansten.model.entity.DynamicText;
import se.inera.hjalpmedelstjansten.model.entity.ElectronicAddress;
import se.inera.hjalpmedelstjansten.model.entity.ExternalCategoryGrouping;
import se.inera.hjalpmedelstjansten.model.entity.Notification;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.PostAddress;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.UserAccount;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;
import se.inera.hjalpmedelstjansten.model.entity.UserPermission;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEDirective;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEStandard;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCountry;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCounty;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVDocumentType;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVMunicipality;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.DependsOn;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonObject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.logging.Level;
import java.util.stream.Collectors;

@Singleton
@Startup
@DependsOn(value = {"PropertyLoader", "ElasticSearchController"})
public class LoadSampleDataService {

    @Inject
    private HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;

    @Inject
    private SecurityController securityController;

    private int amountOfArticlesPerProductGenerated = Integer.parseInt(System.getenv("AMOUNTOFARTICLESGENERATED"));

    @Inject
    private ProductController productController;

    @Inject
    private ArticleController articleController;

    @Inject
    private ElasticSearchController elasticSearchController;

    @Inject
    private ImportStatusController importStatusController;

    @Inject
    private UserController userController;

    @Inject
    private String testPassword;

    @Inject
    private String defaultPricelistNumber;

    private final String LOAD_SAMPLE_DATA_SERVICE_DISABLED = "LOAD_SAMPLE_DATA_SERVICE_DISABLED";

    private final boolean disabled = Boolean.parseBoolean(System.getenv().getOrDefault(LOAD_SAMPLE_DATA_SERVICE_DISABLED, "false"));

    @PostConstruct
    public void loadSampleData() {
        LOG.log( Level.INFO, "loadSampleData()");
        try {

            LOG.log( Level.INFO, "{0} = {1}", new Object[] {LOAD_SAMPLE_DATA_SERVICE_DISABLED, disabled});
            if(disabled){
                return;
            }

            this.amountOfArticlesPerProductGenerated = Integer.parseInt(System.getenv("AMOUNTOFARTICLESGENERATED"));

            if(this.amountOfArticlesPerProductGenerated == 0) {
                this.amountOfArticlesPerProductGenerated = 2;
            }

            List<Organization> organizations = em.createNamedQuery(Organization.FIND_ALL).
                    getResultList();
            if( organizations != null && !organizations.isEmpty() ) {
                LOG.log( Level.INFO, "Sample data already appears to exist, do not create again");
                return;
            }

            LOG.log( Level.INFO, "Add elastic index and search mapping");
            if( elasticSearchController.indexExists() ) {
                elasticSearchController.deleteIndex();
            }
            if( !elasticSearchController.createIndex()) {
                LOG.log( Level.WARNING, "Failed to create elastic search index. Search will not work properly" );
            }

            LOG.log( Level.INFO, "loadSampleData()" );

            loadDynamicText();
            loadSampleImportStatus();
            loadSampleDeletedUsers();
            loadSampleNotifications();
            List<CVPreventiveMaintenance> preventiveMaintenances = loadPreventiveMaintenances();
            List<CVGuaranteeUnit> guaranteeUnits = loadGuaranteeUnits();
            List<CVOrderUnit> orderUnits = loadOrderUnits();
            List<CVPackageUnit> packageUnits = loadPackageUnits();
            List<CVCEDirective> directives = loadDirectives();
            List<CVCEStandard> standards = loadStandards();
            List<UserRole> userRoles = loadRolesAndPermissions();
            List<CVDocumentType> documentTypes = loadDocumentTypes();
            CVCountry sweden = loadCountries();
            List<List<Organization>> suppliersAndCustomers = loadOrganizations(sweden, userRoles);
            List<Organization> suppliers = suppliersAndCustomers.get(0);
            List<Organization> customers = suppliersAndCustomers.get(1);
            loadCategories();
            loadCategoryProperties();
            loadExternalCategoryGroupings();
            loadProducts(suppliers, directives, standards, orderUnits);
            loadAgreements(suppliers, customers, guaranteeUnits.get(0), preventiveMaintenances);
            loadCountiesAndMunicipalities();
        } catch (HjalpmedelstjanstenValidationException ex) {
            LOG.log(Level.SEVERE, "Failed to load sample data", ex);
            if( ex.getValidationMessages() != null ) {
                for( ErrorMessageAPI errorMessageAPI : ex.getValidationMessages() ) {
                    LOG.log(Level.SEVERE, errorMessageAPI.getMessage());
                }
            }
        }
    }

    /**
     * Load countries based on ISO 3166-1
     *
     * @return
     */
    private CVCountry loadCountries() {
        LOG.log( Level.INFO, "loadCountries()" );
        String[] allCountryIsos = Locale.getISOCountries();
        Locale seLocale = new Locale("sv", "SE");
        CVCountry sweden = null;
        for( String countryIso : allCountryIsos ) {
            Locale l = new Locale("", countryIso);
            CVCountry country = new CVCountry();
            country.setCode(countryIso);
            country.setName(l.getDisplayCountry(seLocale));
            em.persist(country);
            if( country.getCode().equals("SE") ) {
                sweden = country;
            }
        }
        return sweden;
    }

    private void loadSampleImportStatus(){
        LOG.log( Level.INFO, "loadSampleImportStatus()" );
        ImportStatusAPI status = new ImportStatusAPI();
//        long id = 1;
//        status.setUniqueId(id);

        Calendar calendar = Calendar.getInstance();

//        em.persist(status);

        for( int i = 0; i<15; i++ ) {

            status.setImportStartDate(calendar.getTime());
            status.setImportEndDate(calendar.getTime());
            status.setFileName("testfile.xlsx");
            status.setReadStatus(0);
            status.setChangedBy("416");
            importStatusController.createImportStatusForFile(status);
        }
    }

    private void loadSampleDeletedUsers(){
        LOG.log( Level.INFO, "loadSampleDeletedUsers()" );

//        long id = 1;
//        status.setUniqueId(id);

        Calendar calendar = Calendar.getInstance();
        long test = 419;
        long test1 = 1235;
        long test2 = 6123;
        String testy  = "en bra förklaring";
//        em.persist(status);

        for( int i = 0; i<15; i++ ) {
            DeletedUserAPI deletedUser = new DeletedUserAPI();
            deletedUser.setDeletedDate(calendar.getTime());
            deletedUser.setOldUniqueId(test);
            deletedUser.setDeletedByUserId(test1);
            deletedUser.setDeletedUserOrg(test2);
            deletedUser.setReasonCode(testy);
            userController.writeDeletedUser(deletedUser);
        }
    }

    /**
     * Load dynamicText
     *
     * @return
     */
    private void loadDynamicText() {
        LOG.log( Level.INFO, "loadDynamicText()" );
        DynamicText one = new DynamicText();
        DynamicText two = new DynamicText();
        DynamicText three = new DynamicText();

        one.setDynamicText("Text om aktuell information.");
        one.setChangedBy("hjm1");
        one.setWhenChanged(Date.from(Instant.now()));

        two.setDynamicText("Välkomsttext.");
        two.setChangedBy("hjm2");
        two.setWhenChanged(Date.from(Instant.now()));

        three.setDynamicText("https://soprasteria-se.atlassian.net/wiki/spaces/IK1/pages/3288924165/L+nkar+HJM");
        three.setChangedBy("hjm3");
        three.setWhenChanged(Date.from(Instant.now()));

        em.persist(one);
        em.persist(two);
        em.persist(three);
    }

    private void loadSampleNotifications() {

        //supplier

        Notification m10 = new Notification();
        m10.setType(Notification.NotificationType.AGREEMENT_NEW);
        m10.setPermission(UserRole.RoleName.SupplierAgreementManager);
        m10.setRecipient(Organization.OrganizationType.SUPPLIER);
        m10.setDescription("Nytt inköpsavtal");

        Notification m11 = new Notification();
        m11.setType(Notification.NotificationType.AGREEMENT_CHANGED);
        m11.setPermission(UserRole.RoleName.SupplierAgreementManager);
        m11.setRecipient(Organization.OrganizationType.SUPPLIER);
        m11.setDescription("Förändring av inköpsavtal");

        Notification m13_1 = new Notification();
        m13_1.setType(Notification.NotificationType.AGREEMENT_NEW_PRICELIST);
        m13_1.setPermission(UserRole.RoleName.SupplierAgreementManager);
        m13_1.setRecipient(Organization.OrganizationType.SUPPLIER);
        m13_1.setDescription("Ny prislista tillgänglig");

        Notification m17_1 = new Notification();
        m17_1.setType(Notification.NotificationType.AGREEMENT_PRICELISTEROW_CUSTOMER_APPROVED);
        m17_1.setPermission(UserRole.RoleName.SupplierAgreementManager);
        m17_1.setRecipient(Organization.OrganizationType.SUPPLIER);
        m17_1.setDescription("Kund har godkänt prislisterader");

        Notification m18_1 = new Notification();
        m18_1.setType(Notification.NotificationType.AGREEMENT_PRICELISTEROW_CUSTOMER_DECLINED);
        m18_1.setPermission(UserRole.RoleName.SupplierAgreementManager);
        m18_1.setRecipient(Organization.OrganizationType.SUPPLIER);
        m18_1.setDescription("Kund har avböjt prislisterader");

        Notification m20 = new Notification();
        m20.setType(Notification.NotificationType.AGREEMENT_PRICELISTEROW_CUSTOMER_APPROVED_INACTIVATION);
        m20.setPermission(UserRole.RoleName.SupplierAgreementManager);
        m20.setRecipient(Organization.OrganizationType.SUPPLIER);
        m20.setDescription("Kund har godkänt inaktivering av prislisterader");

        Notification m21_1 = new Notification();
        m21_1.setType(Notification.NotificationType.AGREEMENT_PRICELISTEROW_CUSTOMER_DECLINED_INACTIVATION);
        m21_1.setPermission(UserRole.RoleName.SupplierAgreementManager);
        m21_1.setRecipient(Organization.OrganizationType.SUPPLIER);
        m21_1.setDescription("Kund har avböjt inaktivering av prislisterader");

        //customer

        Notification m9_1 = new Notification();
        m9_1.setType(Notification.NotificationType.ARTICLES_CHANGED);
        m9_1.setPermission(UserRole.RoleName.CustomerAgreementManager);
        m9_1.setRecipient(Organization.OrganizationType.CUSTOMER);
        m9_1.setDescription("Förändring av artikelinformation");

        Notification m12 = new Notification();
        m12.setType(Notification.NotificationType.AGREEMENT_SHARED_CHANGED);
        m12.setPermission(UserRole.RoleName.CustomerAgreementManager);
        m12.setRecipient(Organization.OrganizationType.CUSTOMER);
        m12.setDescription("Förändring av delat inköpsavtal");

//        Notification m8 = new Notification();
//        m8.setType(Notification.NotificationType.ARTICLES_EXPIRED);
//        m8.setPermission(UserRole.RoleName.CustomerAgreementManager);
//        m8.setRecipient(Organization.OrganizationType.CUSTOMER);
//        m8.setDescription("Utgångna artiklar och ersättningshantering");

        Notification m16_0 = new Notification();
        m16_0.setType(Notification.NotificationType.AGREEMENT_PRICELISTROW_APPROVE);
        m16_0.setPermission(UserRole.RoleName.CustomerAgreementManager);
        m16_0.setRecipient(Organization.OrganizationType.CUSTOMER);
        m16_0.setDescription("Prislisterad att hantera (leverantör har skickat prislisterad för godkännande)");

        Notification m16_2 = new Notification();
        m16_2.setType(Notification.NotificationType.AGREEMENT_PRICELISTROW_APPROVE_BASED_ON);
        m16_2.setPermission(UserRole.RoleName.CustomerAgreementManager);
        m16_2.setRecipient(Organization.OrganizationType.CUSTOMER);
        m16_2.setDescription("Prislisterad att hantera (produkt som artikeln baseras på har ändrats)");

        Notification m19_1 = new Notification();
        m19_1.setType(Notification.NotificationType.ARTICLES_EXPIRED);
        m19_1.setPermission(UserRole.RoleName.CustomerAgreementManager);
        m19_1.setRecipient(Organization.OrganizationType.CUSTOMER);
        m19_1.setDescription("Prislisterad att inaktivera");

        Notification m22 = new Notification();
        m22.setType(Notification.NotificationType.GENERAL_PRICELIST_CHANGED);
        m22.setPermission(UserRole.RoleName.CustomerAgreementViewer); //strange name for this role...
        m22.setRecipient(Organization.OrganizationType.CUSTOMER);
        m22.setDescription("Förändringar på generell prislista");


        //supplier
        em.persist(m10);
        em.persist(m11);
        em.persist(m13_1);
        em.persist(m17_1);
        em.persist(m18_1);
        em.persist(m20);
        em.persist(m21_1);
        //em.persist(m21_2);
        //em.persist(m21_3);
        //em.persist(m21_4);

        //customer
        em.persist(m9_1);
        //em.persist(m12);
        em.persist(m16_0);
        em.persist(m16_2);
        em.persist(m19_1);
        //em.persist(m22);


    }

    private void loadServiceOwnerOrganization(CVCountry country, List<UserRole> userRoles, String orgNumber, String gln, String orgName, String username, String userFirstName, String userLastName) {
        LOG.log( Level.INFO, "loadServiceOwnerOrganization()" );
        Organization organization = new Organization();
        organization.setOrganizationName(orgName);
        organization.setOrganizationNumber(orgNumber);
        organization.setOrganizationType(Organization.OrganizationType.SERVICE_OWNER);
        organization.setMediaFolderName(UUID.randomUUID().toString());
        organization.setGln(gln);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, Calendar.JANUARY);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        organization.setValidFrom(calendar.getTime());
        organization.setCountry(country);

        // Postal Addresses
        List<PostAddress> postAddresses = new ArrayList<>();
        PostAddress visitPostAddress = new PostAddress();
        visitPostAddress.setAddressType(PostAddress.AddressType.VISIT);
        visitPostAddress.setStreetAddress("Visit Street Address Service Owner");
        visitPostAddress.setPostCode("11111");
        visitPostAddress.setCity("Visit City");
        visitPostAddress.setOrganization(organization);
        postAddresses.add(visitPostAddress);
        PostAddress deliveryPostAddress = new PostAddress();
        deliveryPostAddress.setAddressType(PostAddress.AddressType.DELIVERY);
        deliveryPostAddress.setStreetAddress("Delivery Street Address Service Owner");
        deliveryPostAddress.setPostCode("22222");
        deliveryPostAddress.setCity("Delivery City");
        deliveryPostAddress.setOrganization(organization);
        postAddresses.add(deliveryPostAddress);
        organization.setPostAddresses(postAddresses);

        // Electronic Addresses
        ElectronicAddress electronicAddress = new ElectronicAddress();
        electronicAddress.setWeb("https://www.inera.se/");
        organization.setElectronicAddress(electronicAddress);
        em.persist(organization);
        // Admin user
        List<UserRole> serviceOwnerRoles = new ArrayList<>();
        serviceOwnerRoles.add(getRoleByName(UserRole.RoleName.Baseuser, userRoles));
        serviceOwnerRoles.add(getRoleByName(UserRole.RoleName.Superadmin, userRoles));
        loadUser(organization, serviceOwnerRoles, username, userFirstName, userLastName);
    }

    private Organization loadSupplierOrganization(CVCountry country, List<UserRole> userRoles, String orgNumber, String gln, String orgName, String username, String userFirstName, String userLastName) {
        LOG.log( Level.INFO, "loadSupplierOrganization()" );
        Organization organization = new Organization();
        organization.setOrganizationName(orgName);
        organization.setOrganizationNumber(orgNumber);
        organization.setOrganizationType(Organization.OrganizationType.SUPPLIER);
        organization.setMediaFolderName(UUID.randomUUID().toString());
        organization.setGln(gln);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, Calendar.JANUARY);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        organization.setValidFrom(calendar.getTime());
        organization.setCountry(country);

        // Postal Addresses
        List<PostAddress> postAddresses = new ArrayList<>();
        PostAddress visitPostAddress = new PostAddress();
        visitPostAddress.setAddressType(PostAddress.AddressType.VISIT);
        visitPostAddress.setStreetAddress("Visit Street Address");
        visitPostAddress.setPostCode("11111");
        visitPostAddress.setCity("Visit City");
        visitPostAddress.setOrganization(organization);
        postAddresses.add(visitPostAddress);
        PostAddress deliveryPostAddress = new PostAddress();
        deliveryPostAddress.setAddressType(PostAddress.AddressType.DELIVERY);
        deliveryPostAddress.setStreetAddress("Delivery Street Address");
        deliveryPostAddress.setPostCode("22222");
        deliveryPostAddress.setCity("Delivery City");
        deliveryPostAddress.setOrganization(organization);
        postAddresses.add(deliveryPostAddress);
        organization.setPostAddresses(postAddresses);

        // Electronic Addresses
        ElectronicAddress electronicAddress = new ElectronicAddress();
        electronicAddress.setWeb("https://www.inera.se/");
        organization.setElectronicAddress(electronicAddress);
        em.persist(organization);

        // Admin user
        List<UserRole> supplierRoles = new ArrayList<>();
        supplierRoles.add(getRoleByName(UserRole.RoleName.Baseuser, userRoles));
        supplierRoles.add(getRoleByName(UserRole.RoleName.Supplieradmin, userRoles));
        supplierRoles.add(getRoleByName(UserRole.RoleName.SupplierAgreementViewer, userRoles));
        supplierRoles.add(getRoleByName(UserRole.RoleName.SupplierAgreementManager, userRoles));
        supplierRoles.add(getRoleByName(UserRole.RoleName.SupplierProductAndArticleHandler, userRoles));
        loadUser(organization, supplierRoles, username, userFirstName, userLastName);
        return organization;
    }

    private Organization loadCustomerOrganization(CVCountry country, List<UserRole> userRoles, String orgNumber, String gln, String orgName, String username, String userFirstName, String userLastName) {
        LOG.log( Level.INFO, "loadCustomerOrganization()" );
        Organization organization = new Organization();
        organization.setOrganizationName(orgName);
        organization.setOrganizationNumber(orgNumber);
        organization.setOrganizationType(Organization.OrganizationType.CUSTOMER);
        organization.setMediaFolderName(UUID.randomUUID().toString());
        organization.setGln(gln);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, Calendar.JANUARY);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        organization.setValidFrom(calendar.getTime());
        organization.setCountry(country);

        // Postal Addresses
        List<PostAddress> postAddresses = new ArrayList<>();
        PostAddress visitPostAddress = new PostAddress();
        visitPostAddress.setAddressType(PostAddress.AddressType.VISIT);
        visitPostAddress.setStreetAddress("Visit Street Address");
        visitPostAddress.setPostCode("11111");
        visitPostAddress.setCity("Visit City");
        visitPostAddress.setOrganization(organization);
        postAddresses.add(visitPostAddress);
        PostAddress deliveryPostAddress = new PostAddress();
        deliveryPostAddress.setAddressType(PostAddress.AddressType.DELIVERY);
        deliveryPostAddress.setStreetAddress("Delivery Street Address");
        deliveryPostAddress.setPostCode("22222");
        deliveryPostAddress.setCity("Delivery City");
        deliveryPostAddress.setOrganization(organization);
        postAddresses.add(deliveryPostAddress);
        organization.setPostAddresses(postAddresses);

        // Electronic Addresses
        ElectronicAddress electronicAddress = new ElectronicAddress();
        electronicAddress.setWeb("https://www.inera.se/");
        organization.setElectronicAddress(electronicAddress);

        BusinessLevel syn = new BusinessLevel();
        syn.setName("Syn");
        syn.setStatus(BusinessLevel.Status.ACTIVE);
        syn.setOrganization(organization);

        BusinessLevel horsel = new BusinessLevel();
        horsel.setName("Hörsel");
        horsel.setStatus(BusinessLevel.Status.ACTIVE);
        horsel.setOrganization(organization);

        List<BusinessLevel> businessLevels = new ArrayList<>();
        businessLevels.add(syn);
        businessLevels.add(horsel);
        organization.setBusinessLevels(businessLevels);

        em.persist(organization);

        // Admin user
        List<UserRole> customerRoles = new ArrayList<>();
        customerRoles.add(getRoleByName(UserRole.RoleName.Baseuser, userRoles));
        customerRoles.add(getRoleByName(UserRole.RoleName.Customeradmin, userRoles));
        customerRoles.add(getRoleByName(UserRole.RoleName.CustomerAgreementViewer, userRoles));
        customerRoles.add(getRoleByName(UserRole.RoleName.CustomerAgreementManager, userRoles));
        customerRoles.add(getRoleByName(UserRole.RoleName.CustomerAssortmentManager, userRoles));
        customerRoles.add(getRoleByName(UserRole.RoleName.CustomerAssignedAssortmentManager, userRoles));

        loadUser(organization, customerRoles, username, userFirstName, userLastName);
        return organization;
    }

    private List<List<Organization>> loadOrganizations(CVCountry country, List<UserRole> userRoles) {
        LOG.log( Level.INFO, "loadOrganizations()" );
        List<String> lines = new ArrayList<>();
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            inputStream = this.getClass().getClassLoader().getResourceAsStream("organizations.csv");
            if( inputStream != null ) {
                bufferedReader =  new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                while( (nextLine = bufferedReader.readLine()) != null ) {
                    lines.add(nextLine);
                }
            } else {
                LOG.log(Level.INFO, "No organizations.csv found");
            }
        } catch (IOException ex) {
            LOG.log(Level.INFO, "No organizations.csv found", ex);
        } finally {
            try {
                if( bufferedReader != null ) {
                    bufferedReader.close();
                }
                if( inputStream != null ) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to close organizations.csv", ex);
            }
        }
        // first line is only description of other lines
        List<List<Organization>> suppliersAndCustomers = new ArrayList<>();
        List<Organization> suppliers = new ArrayList<>();
        List<Organization> customers = new ArrayList<>();
        for( int i = 1; i<lines.size(); i++ ) {
            StringTokenizer stringTokenizer = new StringTokenizer(lines.get(i), ";");
            String type = stringTokenizer.nextToken();
            String orgNumber = stringTokenizer.nextToken();
            String gln = stringTokenizer.nextToken();
            String orgName = stringTokenizer.nextToken();
            String username = stringTokenizer.nextToken();
            String userFirstName = stringTokenizer.nextToken();
            String userLastName = stringTokenizer.nextToken();
            Organization.OrganizationType organizationType = Organization.OrganizationType.valueOf(type);
            LOG.log(Level.INFO, "Loading organization with name " + orgName);
            if( organizationType == Organization.OrganizationType.SERVICE_OWNER ) {
                loadServiceOwnerOrganization(country, userRoles, orgNumber, gln, orgName, username, userFirstName, userLastName);
            } else if( organizationType == Organization.OrganizationType.SUPPLIER ) {
                suppliers.add(loadSupplierOrganization(country, userRoles, orgNumber, gln, orgName, username, userFirstName, userLastName));
            } else {
                customers.add(loadCustomerOrganization(country, userRoles, orgNumber, gln, orgName, username, userFirstName, userLastName));
            }
        }
        suppliersAndCustomers.add(suppliers);
        suppliersAndCustomers.add(customers);
        return suppliersAndCustomers;
    }

    private void loadUser(Organization organization, List<UserRole> userRoles, String username, String userFirstName, String userLastName) {
        LOG.log( Level.INFO, "loadUser()" );
        // User Account
        UserAccount userAccount = new UserAccount();
        userAccount.setFirstName(userFirstName);
        userAccount.setLastName(userLastName);
        userAccount.setUsername(username);
        userAccount.setTitle("Title");
        userAccount.setLoginType(UserAccount.LoginType.PASSWORD);
        String salt = securityController.generateSalt();
        int iterations = securityController.generateIterations();
        String hashedPassword = securityController.hashPassword(testPassword, salt, iterations);
        userAccount.setSalt(salt);
        userAccount.setIterations(iterations);
        userAccount.setPassword(hashedPassword);

        // Electronic Addresses
        ElectronicAddress electronicAddress = new ElectronicAddress();
        electronicAddress.setEmail(username + "@" + organization.getOrganizationName().replace(" ","").toLowerCase() + ".com");
        electronicAddress.setMobile("");
        electronicAddress.setTelephone("");
        userAccount.setElectronicAddress(electronicAddress);

        // The Engagement
        UserEngagement userEngagement = new UserEngagement();
        userEngagement.setOrganization(organization);
        userEngagement.setUserAccount(userAccount);
        userEngagement.setValidFrom(new Date());

        // Roles
        userEngagement.setRoles(userRoles);

        em.persist(userEngagement);
    }

    private List<UserRole> loadRolesAndPermissions() {
        LOG.log( Level.INFO, "loadRolesAndPermissions()" );
        List<UserRole> userRoles = new ArrayList<>();

        UserPermission organizationViewPermission = new UserPermission();
        organizationViewPermission.setName("organization:view");
        organizationViewPermission.setDescription("View organization details");
        em.persist(organizationViewPermission);

        UserPermission organizationCreatePermission = new UserPermission();
        organizationCreatePermission.setName("organization:create");
        organizationCreatePermission.setDescription("Create organizations");
        em.persist(organizationCreatePermission);

        UserPermission organizationUpdatePermission = new UserPermission();
        organizationUpdatePermission.setName("organization:update");
        organizationUpdatePermission.setDescription("Update organizations");
        em.persist(organizationUpdatePermission);

        UserPermission organizationUpdateContactPermission = new UserPermission();
        organizationUpdateContactPermission.setName("organization:update_contact");
        organizationUpdateContactPermission.setDescription("Update organization contact information on own organization");
        em.persist(organizationUpdateContactPermission);

        UserPermission organizationDeletePermission = new UserPermission();
        organizationDeletePermission.setName("organization:delete");
        organizationDeletePermission.setDescription("Delete organizations");
        em.persist(organizationDeletePermission);

        UserPermission businessLevelViewPermission = new UserPermission();
        businessLevelViewPermission.setName("businesslevel:view");
        businessLevelViewPermission.setDescription("View business levels on organization");
        em.persist(businessLevelViewPermission);

        UserPermission businessLevelCreatePermission = new UserPermission();
        businessLevelCreatePermission.setName("businesslevel:create");
        businessLevelCreatePermission.setDescription("Create business levels on organization");
        em.persist(businessLevelCreatePermission);

        UserPermission businessLevelInactivatePermission = new UserPermission();
        businessLevelInactivatePermission.setName("businesslevel:inactivate");
        businessLevelInactivatePermission.setDescription("Inactivate business levels on organization");
        em.persist(businessLevelInactivatePermission);

        UserPermission businessLevelDeletePermission = new UserPermission();
        businessLevelDeletePermission.setName("businesslevel:delete");
        businessLevelDeletePermission.setDescription("Delete business levels on organization");
        em.persist(businessLevelDeletePermission);

        UserPermission userViewPermission = new UserPermission();
        userViewPermission.setName("user:view");
        userViewPermission.setDescription("View user details");
        em.persist(userViewPermission);

        UserPermission userCreatePermission = new UserPermission();
        userCreatePermission.setName("user:create");
        userCreatePermission.setDescription("Create users");
        em.persist(userCreatePermission);

        UserPermission userCreateOwnPermission = new UserPermission();
        userCreateOwnPermission.setName("user:create_own");
        userCreateOwnPermission.setDescription("Create users on own organization");
        em.persist(userCreateOwnPermission);

        UserPermission userUpdatePermission = new UserPermission();
        userUpdatePermission.setName("user:update");
        userUpdatePermission.setDescription("Update users");
        em.persist(userUpdatePermission);

        UserPermission userUpdateOwnPermission = new UserPermission();
        userUpdateOwnPermission.setName("user:update_own");
        userUpdateOwnPermission.setDescription("Update users on own organization");
        em.persist(userUpdateOwnPermission);

        UserPermission userUpdateContactPermission = new UserPermission();
        userUpdateContactPermission.setName("user:update_contact");
        userUpdateContactPermission.setDescription("Update my own contact information");
        em.persist(userUpdateContactPermission);

        UserPermission userDeletePermission = new UserPermission();
        userDeletePermission.setName("user:delete");
        userDeletePermission.setDescription("Delete users");
        em.persist(userDeletePermission);

        UserPermission userDeleteOwnPermission = new UserPermission();
        userDeleteOwnPermission.setName("user:delete_own");
        userDeleteOwnPermission.setDescription("Delete users on own organization");
        em.persist(userDeleteOwnPermission);

        UserPermission profileViewPermission = new UserPermission();
        profileViewPermission.setName("profile:view");
        profileViewPermission.setDescription("View personal profile");
        em.persist(profileViewPermission);

        UserPermission rolesViewPermission = new UserPermission();
        rolesViewPermission.setName("roles:view");
        rolesViewPermission.setDescription("View available roles");
        em.persist(rolesViewPermission);

        UserPermission productViewPermission = new UserPermission();
        productViewPermission.setName("product:view");
        productViewPermission.setDescription("View product information");
        em.persist(productViewPermission);

        UserPermission productCreateOwnPermission = new UserPermission();
        productCreateOwnPermission.setName("product:create_own");
        productCreateOwnPermission.setDescription("Create product on own organization");
        em.persist(productCreateOwnPermission);

        UserPermission productUpdateOwnPermission = new UserPermission();
        productUpdateOwnPermission.setName("product:update_own");
        productUpdateOwnPermission.setDescription("Update product on own organization");
        em.persist(productUpdateOwnPermission);

        UserPermission articleViewPermission = new UserPermission();
        articleViewPermission.setName("article:view");
        articleViewPermission.setDescription("View article information");
        em.persist(articleViewPermission);

        UserPermission articleCreateOwnPermission = new UserPermission();
        articleCreateOwnPermission.setName("article:create_own");
        articleCreateOwnPermission.setDescription("Create article on own organization");
        em.persist(articleCreateOwnPermission);

        UserPermission articleUpdateOwnPermission = new UserPermission();
        articleUpdateOwnPermission.setName("article:update_own");
        articleUpdateOwnPermission.setDescription("Update article on own organization");
        em.persist(articleUpdateOwnPermission);

        UserPermission categoryViewPermission = new UserPermission();
        categoryViewPermission.setName("category:view");
        categoryViewPermission.setDescription("View categories on products");
        em.persist(categoryViewPermission);

        UserPermission ceViewPermission = new UserPermission();
        ceViewPermission.setName("ce:view");
        ceViewPermission.setDescription("View CE standards and directives");
        em.persist(ceViewPermission);

        UserPermission orderUnitViewPermission = new UserPermission();
        orderUnitViewPermission.setName("orderunit:view");
        orderUnitViewPermission.setDescription("View order units");
        em.persist(orderUnitViewPermission);

        UserPermission packageUnitViewPermission = new UserPermission();
        packageUnitViewPermission.setName("packageunit:view");
        packageUnitViewPermission.setDescription("View package units");
        em.persist(packageUnitViewPermission);

        UserPermission guaranteeUnitViewPermission = new UserPermission();
        guaranteeUnitViewPermission.setName("guaranteeunit:view");
        guaranteeUnitViewPermission.setDescription("View guarantee units");
        em.persist(guaranteeUnitViewPermission);

        UserPermission preventiveMaintenanceViewPermission = new UserPermission();
        preventiveMaintenanceViewPermission.setName("preventivemaintenance:view");
        preventiveMaintenanceViewPermission.setDescription("View preventive maintenances");
        em.persist(preventiveMaintenanceViewPermission);

        UserPermission agreementViewOwnPermission = new UserPermission();
        agreementViewOwnPermission.setName("agreement:view_own");
        agreementViewOwnPermission.setDescription("View agreement on own organization");
        em.persist(agreementViewOwnPermission);

        UserPermission agreementCreateOwnPermission = new UserPermission();
        agreementCreateOwnPermission.setName("agreement:create_own");
        agreementCreateOwnPermission.setDescription("Create agreement on own organization");
        em.persist(agreementCreateOwnPermission);

        UserPermission agreementUpdateOwnPermission = new UserPermission();
        agreementUpdateOwnPermission.setName("agreement:update_own");
        agreementUpdateOwnPermission.setDescription("Update agreement on own organization");
        em.persist(agreementUpdateOwnPermission);

        UserPermission agreementDeleteOwnPermission = new UserPermission();
        agreementDeleteOwnPermission.setName("agreement:delete_own");
        agreementDeleteOwnPermission.setDescription("Delete agreement on own organization");
        em.persist(agreementDeleteOwnPermission);

        UserPermission pricelistViewOwnPermission = new UserPermission();
        pricelistViewOwnPermission.setName("pricelist:view_own");
        pricelistViewOwnPermission.setDescription("View pricelist on own organization");
        em.persist(pricelistViewOwnPermission);

        UserPermission pricelistCreateOwnPermission = new UserPermission();
        pricelistCreateOwnPermission.setName("pricelist:create_own");
        pricelistCreateOwnPermission.setDescription("Create pricelist on own organization");
        em.persist(pricelistCreateOwnPermission);

        UserPermission pricelistUpdateOwnPermission = new UserPermission();
        pricelistUpdateOwnPermission.setName("pricelist:update_own");
        pricelistUpdateOwnPermission.setDescription("Update pricelist on own organization");
        em.persist(pricelistUpdateOwnPermission);

        UserPermission pricelistRowViewExistsPermission = new UserPermission();
        pricelistRowViewExistsPermission.setName("pricelistrow:view_exists");
        pricelistRowViewExistsPermission.setDescription("View pricelist exists on row on any");
        em.persist(pricelistRowViewExistsPermission);

        UserPermission pricelistRowViewOwnPermission = new UserPermission();
        pricelistRowViewOwnPermission.setName("pricelistrow:view_own");
        pricelistRowViewOwnPermission.setDescription("View pricelist row on pricelist");
        em.persist(pricelistRowViewOwnPermission);

        UserPermission pricelistRowCreateOwnPermission = new UserPermission();
        pricelistRowCreateOwnPermission.setName("pricelistrow:create_own");
        pricelistRowCreateOwnPermission.setDescription("Create pricelist row on pricelist");
        em.persist(pricelistRowCreateOwnPermission);

        UserPermission pricelistRowUpdateOwnPermission = new UserPermission();
        pricelistRowUpdateOwnPermission.setName("pricelistrow:update_own");
        pricelistRowUpdateOwnPermission.setDescription("Update pricelist row on pricelist");
        em.persist(pricelistRowUpdateOwnPermission);

        UserPermission pricelistRowSendForCustomerApprovalPermission = new UserPermission();
        pricelistRowSendForCustomerApprovalPermission.setName("pricelistrow:sendForCustomerApproval");
        pricelistRowSendForCustomerApprovalPermission.setDescription("Send pricelist row on pricelist for customer approval");
        em.persist(pricelistRowSendForCustomerApprovalPermission);

        UserPermission pricelistRowDeclinePermission = new UserPermission();
        pricelistRowDeclinePermission.setName("pricelistrow:decline");
        pricelistRowDeclinePermission.setDescription("Decline pricelist row on pricelist");
        em.persist(pricelistRowDeclinePermission);

        UserPermission pricelistRowActivatePermission = new UserPermission();
        pricelistRowActivatePermission.setName("pricelistrow:activate");
        pricelistRowActivatePermission.setDescription("Activate pricelist row on pricelist");
        em.persist(pricelistRowActivatePermission);

        UserPermission pricelistRowInactivatePermission = new UserPermission();
        pricelistRowInactivatePermission.setName("pricelistrow:inactivate");
        pricelistRowInactivatePermission.setDescription("Inactivate pricelist row on pricelist");
        em.persist(pricelistRowInactivatePermission);

        UserPermission pricelistRowApproveInactivatePermission = new UserPermission();
        pricelistRowApproveInactivatePermission.setName("pricelistrow:approve_inactivate");
        pricelistRowApproveInactivatePermission.setDescription("Approve inactivation of pricelist row on pricelist");
        em.persist(pricelistRowApproveInactivatePermission);

        UserPermission pricelistRowDeclineInactivatePermission = new UserPermission();
        pricelistRowDeclineInactivatePermission.setName("pricelistrow:decline_inactivate");
        pricelistRowDeclineInactivatePermission.setDescription("Decline inactivation of pricelist row on pricelist");
        em.persist(pricelistRowDeclineInactivatePermission);

        UserPermission pricelistRowReopenInactivatedPermission = new UserPermission();
        pricelistRowReopenInactivatedPermission.setName("pricelistrow:reopen");
        pricelistRowReopenInactivatedPermission.setDescription("Reopen inactivated pricelist rows on pricelist");
        em.persist(pricelistRowReopenInactivatedPermission);

        UserPermission generalPricelistViewPermission = new UserPermission();
        generalPricelistViewPermission.setName("generalpricelist:view");
        generalPricelistViewPermission.setDescription("View general pricelist");
        em.persist(generalPricelistViewPermission);

        UserPermission generalPricelistViewAllPermission = new UserPermission();
        generalPricelistViewAllPermission.setName("generalpricelist:view_all");
        generalPricelistViewAllPermission.setDescription("View general pricelist any organization");
        em.persist(generalPricelistViewAllPermission);

        UserPermission generalPricelistCreateOwnPermission = new UserPermission();
        generalPricelistCreateOwnPermission.setName("generalpricelist:create_own");
        generalPricelistCreateOwnPermission.setDescription("Create general pricelist");
        em.persist(generalPricelistCreateOwnPermission);

        UserPermission generalPricelistUpdateOwnPermission = new UserPermission();
        generalPricelistUpdateOwnPermission.setName("generalpricelist:update_own");
        generalPricelistUpdateOwnPermission.setDescription("Update general pricelist");
        em.persist(generalPricelistUpdateOwnPermission);

        UserPermission generalPricelistPricelistViewOwnPermission = new UserPermission();
        generalPricelistPricelistViewOwnPermission.setName("generalpricelist_pricelist:view_own");
        generalPricelistPricelistViewOwnPermission.setDescription("View pricelist on general pricelist own organization");
        em.persist(generalPricelistPricelistViewOwnPermission);

        UserPermission generalPricelistPricelistViewAllPermission = new UserPermission();
        generalPricelistPricelistViewAllPermission.setName("generalpricelist_pricelist:view_all");
        generalPricelistPricelistViewAllPermission.setDescription("View pricelist on general pricelist any organization");
        em.persist(generalPricelistPricelistViewAllPermission);

        UserPermission generalPricelistPricelistCreateOwnPermission = new UserPermission();
        generalPricelistPricelistCreateOwnPermission.setName("generalpricelist_pricelist:create_own");
        generalPricelistPricelistCreateOwnPermission.setDescription("Create pricelist on general pricelist own organization");
        em.persist(generalPricelistPricelistCreateOwnPermission);

        UserPermission generalPricelistPricelistUpdateOwnPermission = new UserPermission();
        generalPricelistPricelistUpdateOwnPermission.setName("generalpricelist_pricelist:update_own");
        generalPricelistPricelistUpdateOwnPermission.setDescription("Update pricelist on general pricelist own organization");
        em.persist(generalPricelistPricelistUpdateOwnPermission);

        UserPermission generalPricelistPricelistRowViewOwnPermission = new UserPermission();
        generalPricelistPricelistRowViewOwnPermission.setName("generalpricelist_pricelistrow:view_own");
        generalPricelistPricelistRowViewOwnPermission.setDescription("View pricelist rows on general pricelist own organization");
        em.persist(generalPricelistPricelistRowViewOwnPermission);

        UserPermission generalPricelistPricelistRowViewAllPermission = new UserPermission();
        generalPricelistPricelistRowViewAllPermission.setName("generalpricelist_pricelistrow:view_all");
        generalPricelistPricelistRowViewAllPermission.setDescription("View pricelist rows on general pricelist any organization");
        em.persist(generalPricelistPricelistRowViewAllPermission);

        UserPermission generalPricelistPricelistRowCreateOwnPermission = new UserPermission();
        generalPricelistPricelistRowCreateOwnPermission.setName("generalpricelist_pricelistrow:create_own");
        generalPricelistPricelistRowCreateOwnPermission.setDescription("Create pricelist row on general pricelist own organization");
        em.persist(generalPricelistPricelistRowCreateOwnPermission);

        UserPermission generalPricelistPricelistRowInactivateOwnPermission = new UserPermission();
        generalPricelistPricelistRowInactivateOwnPermission.setName("generalpricelist_pricelistrow:inactivate_own");
        generalPricelistPricelistRowInactivateOwnPermission.setDescription("Inactivate pricelist row on general pricelist own organization");
        em.persist(generalPricelistPricelistRowInactivateOwnPermission);

        UserPermission generalPricelistPricelistRowDeleteOwnPermission = new UserPermission();
        generalPricelistPricelistRowDeleteOwnPermission.setName("generalpricelist_pricelistrow:delete_own");
        generalPricelistPricelistRowDeleteOwnPermission.setDescription("Delete pricelist row on general pricelist own organization");
        em.persist(generalPricelistPricelistRowDeleteOwnPermission);

        UserPermission generalPricelistPricelistRowUpdateOwnPermission = new UserPermission();
        generalPricelistPricelistRowUpdateOwnPermission.setName("generalpricelist_pricelistrow:update_own");
        generalPricelistPricelistRowUpdateOwnPermission.setDescription("Update pricelist row on general pricelist own organization");
        em.persist(generalPricelistPricelistRowUpdateOwnPermission);

        // assortment
        UserPermission assortmentViewAllPermission = new UserPermission();
        assortmentViewAllPermission.setName("assortment:view_all");
        assortmentViewAllPermission.setDescription("View all assortments");
        em.persist(assortmentViewAllPermission);

        UserPermission assortmentViewPermission = new UserPermission();
        assortmentViewPermission.setName("assortment:view");
        assortmentViewPermission.setDescription("View assortment on organization");
        em.persist(assortmentViewPermission);

        UserPermission assortmentCreatePermission = new UserPermission();
        assortmentCreatePermission.setName("assortment:create");
        assortmentCreatePermission.setDescription("Create assortment on organization");
        em.persist(assortmentCreatePermission);

        UserPermission assortmentUpdatePermission = new UserPermission();
        assortmentUpdatePermission.setName("assortment:update");
        assortmentUpdatePermission.setDescription("Update assortment on organization");
        em.persist(assortmentUpdatePermission);

        UserPermission assortmentArticleViewPermission = new UserPermission();
        assortmentArticleViewPermission.setName("assortmentarticle:view");
        assortmentArticleViewPermission.setDescription("View articles on assortment");
        em.persist(assortmentArticleViewPermission);

        UserPermission assortmentArticleCreateAllPermission = new UserPermission();
        assortmentArticleCreateAllPermission.setName("assortmentarticle:create_all");
        assortmentArticleCreateAllPermission.setDescription("Create articles on any assortment on organization");
        em.persist(assortmentArticleCreateAllPermission);

        UserPermission assortmentArticleCreateOwnPermission = new UserPermission();
        assortmentArticleCreateOwnPermission.setName("assortmentarticle:create_own");
        assortmentArticleCreateOwnPermission.setDescription("Create articles on assigned assortments on organization");
        em.persist(assortmentArticleCreateOwnPermission);

        UserPermission assortmentArticleDeleteAllPermission = new UserPermission();
        assortmentArticleDeleteAllPermission.setName("assortmentarticle:delete_all");
        assortmentArticleDeleteAllPermission.setDescription("Delete articles on any assortment on organization");
        em.persist(assortmentArticleDeleteAllPermission);

        UserPermission assortmentArticleDeleteOwnPermission = new UserPermission();
        assortmentArticleDeleteOwnPermission.setName("assortmentarticle:delete_own");
        assortmentArticleDeleteOwnPermission.setDescription("Delete articles on assigned assortments on organization");
        em.persist(assortmentArticleDeleteOwnPermission);

        UserPermission countiesViewPermission = new UserPermission();
        countiesViewPermission.setName("counties:view");
        countiesViewPermission.setDescription("View counties for assortments");
        em.persist(countiesViewPermission);

        // media
        UserPermission documentTypeViewPermission = new UserPermission();
        documentTypeViewPermission.setName("documenttype:view");
        documentTypeViewPermission.setDescription("View document types");
        em.persist(documentTypeViewPermission);

        UserPermission mediaViewPermission = new UserPermission();
        mediaViewPermission.setName("media:view");
        mediaViewPermission.setDescription("View media on article or product");
        em.persist(mediaViewPermission);

        UserPermission mediaCreatePermission = new UserPermission();
        mediaCreatePermission.setName("media:create");
        mediaCreatePermission.setDescription("Create media on article or product");
        em.persist(mediaCreatePermission);

        UserPermission mediaDeletePermission = new UserPermission();
        mediaDeletePermission.setName("media:delete");
        mediaDeletePermission.setDescription("Delete media on article or product");
        em.persist(mediaDeletePermission);

        // export/import
        UserPermission productArticleExportViewPermission = new UserPermission();
        productArticleExportViewPermission.setName("productarticleexport:view");
        productArticleExportViewPermission.setDescription("View export for products/articles");
        em.persist(productArticleExportViewPermission);

        UserPermission productArticleImportViewPermission = new UserPermission();
        productArticleImportViewPermission.setName("productarticleimport:view");
        productArticleImportViewPermission.setDescription("Upload import for products/articles");
        em.persist(productArticleImportViewPermission);

        UserPermission pricelistExportViewPermission = new UserPermission();
        pricelistExportViewPermission.setName("agreementpricelistexport:view");
        pricelistExportViewPermission.setDescription("View export for agreement pricelist");
        em.persist(pricelistExportViewPermission);

        UserPermission pricelistImportViewPermission = new UserPermission();
        pricelistImportViewPermission.setName("agreementpricelistimport:view");
        pricelistImportViewPermission.setDescription("Upload import for agreement pricelist");
        em.persist(pricelistImportViewPermission);

        UserPermission generalPricelistPricelistExportViewPermission = new UserPermission();
        generalPricelistPricelistExportViewPermission.setName("generalpricelistpricelistexport:view");
        generalPricelistPricelistExportViewPermission.setDescription("View export for general pricelist pricelist");
        em.persist(generalPricelistPricelistExportViewPermission);

        UserPermission generalPricelistPricelistImportViewPermission = new UserPermission();
        generalPricelistPricelistImportViewPermission.setName("generalpricelistpricelistimport:view");
        generalPricelistPricelistImportViewPermission.setDescription("Upload import for general pricelist pricelist");
        em.persist(generalPricelistPricelistImportViewPermission);

        // export settings (xml)
        UserPermission exportSettingsViewAllPermission = new UserPermission();
        exportSettingsViewAllPermission.setName("exportsettings:view_all");
        exportSettingsViewAllPermission.setDescription("View all export settings (admin)");
        em.persist(exportSettingsViewAllPermission);

        UserPermission exportSettingsCreateAllPermission = new UserPermission();
        exportSettingsCreateAllPermission.setName("exportsettings:create_all");
        exportSettingsCreateAllPermission.setDescription("Create all export settings (admin)");
        em.persist(exportSettingsCreateAllPermission);

        UserPermission exportSettingsUpdateAllPermission = new UserPermission();
        exportSettingsUpdateAllPermission.setName("exportsettings:update_all");
        exportSettingsUpdateAllPermission.setDescription("Update all export settings (admin)");
        em.persist(exportSettingsUpdateAllPermission);

        UserPermission exportSettingsViewOwnPermission = new UserPermission();
        exportSettingsViewOwnPermission.setName("exportsettings:view_own");
        exportSettingsViewOwnPermission.setDescription("View own export settings");
        em.persist(exportSettingsViewOwnPermission);

        UserPermission exportSettingsUpdateOwnPermission = new UserPermission();
        exportSettingsUpdateOwnPermission.setName("exportsettings:update_own");
        exportSettingsUpdateOwnPermission.setDescription("Update own export settings");
        em.persist(exportSettingsUpdateOwnPermission);

        UserPermission textsViewPermission = new UserPermission();
        textsViewPermission.setName("texts:view");
        textsViewPermission.setDescription("View system texts");
        em.persist(textsViewPermission);

        UserPermission tokenUserViewPermission = new UserPermission();
        tokenUserViewPermission.setName("tokenuser:view");
        tokenUserViewPermission.setDescription("View token users");
        em.persist(tokenUserViewPermission);

        UserPermission tokenUserCreatePermission = new UserPermission();
        tokenUserCreatePermission.setName("tokenuser:create");
        tokenUserCreatePermission.setDescription("Create token user");
        em.persist(tokenUserCreatePermission);

        // BASE USER everyone should have this role
        List<UserPermission> baseUserPermissions = new ArrayList<>();
        baseUserPermissions.add(organizationViewPermission);
        baseUserPermissions.add(businessLevelViewPermission);
        baseUserPermissions.add(userViewPermission);
        baseUserPermissions.add(rolesViewPermission);
        baseUserPermissions.add(userUpdateContactPermission);
        baseUserPermissions.add(profileViewPermission);
        baseUserPermissions.add(categoryViewPermission);
        baseUserPermissions.add(ceViewPermission);
        baseUserPermissions.add(orderUnitViewPermission);
        baseUserPermissions.add(packageUnitViewPermission);
        baseUserPermissions.add(documentTypeViewPermission);
        baseUserPermissions.add(guaranteeUnitViewPermission);
        baseUserPermissions.add(preventiveMaintenanceViewPermission);
        baseUserPermissions.add(productViewPermission);
        baseUserPermissions.add(articleViewPermission);
        baseUserPermissions.add(mediaViewPermission);
        baseUserPermissions.add(textsViewPermission);
        baseUserPermissions.add(generalPricelistPricelistRowViewOwnPermission);
        baseUserPermissions.add(pricelistRowViewExistsPermission);
        UserRole baseUserRole = new UserRole();
        baseUserRole.setName(UserRole.RoleName.Baseuser);
        baseUserRole.setDescription("Grundläggande visning och sök");
        baseUserRole.setPermissions(baseUserPermissions);
        em.persist(baseUserRole);
        userRoles.add(baseUserRole);

        // BASE TOKEN USER for users logging in with tokens, limited permissions
        List<UserPermission> baseTokenUserPermissions = new ArrayList<>();
        baseTokenUserPermissions.add(businessLevelViewPermission);
        baseTokenUserPermissions.add(organizationViewPermission);
        baseTokenUserPermissions.add(ceViewPermission);
        baseTokenUserPermissions.add(orderUnitViewPermission);
        baseTokenUserPermissions.add(packageUnitViewPermission);
        baseTokenUserPermissions.add(documentTypeViewPermission);
        baseTokenUserPermissions.add(guaranteeUnitViewPermission);
        baseTokenUserPermissions.add(preventiveMaintenanceViewPermission);
        baseTokenUserPermissions.add(profileViewPermission);
        baseTokenUserPermissions.add(productViewPermission);
        baseTokenUserPermissions.add(articleViewPermission);
        baseTokenUserPermissions.add(categoryViewPermission);
        baseTokenUserPermissions.add(mediaViewPermission);
        baseTokenUserPermissions.add(textsViewPermission);
        UserRole baseTokenUserRole = new UserRole();
        baseTokenUserRole.setName(UserRole.RoleName.BaseTokenuser);
        baseTokenUserRole.setDescription("Grundläggande visning och sök");
        baseTokenUserRole.setPermissions(baseTokenUserPermissions);
        em.persist(baseTokenUserRole);

        // SUPER ADMIN
        List<UserPermission> superAdminPermissions = new ArrayList<>();
        superAdminPermissions.add(organizationCreatePermission);
        superAdminPermissions.add(organizationUpdatePermission);
        superAdminPermissions.add(organizationDeletePermission);
        superAdminPermissions.add(businessLevelCreatePermission);
        superAdminPermissions.add(businessLevelInactivatePermission);
        superAdminPermissions.add(businessLevelDeletePermission);
        superAdminPermissions.add(userCreatePermission);
        superAdminPermissions.add(userUpdatePermission);
        superAdminPermissions.add(userDeletePermission);
        superAdminPermissions.add(exportSettingsViewAllPermission);
        superAdminPermissions.add(exportSettingsCreateAllPermission);
        superAdminPermissions.add(exportSettingsUpdateAllPermission);
        superAdminPermissions.add(tokenUserViewPermission);
        superAdminPermissions.add(tokenUserCreatePermission);
        superAdminPermissions.add(assortmentViewAllPermission);
        superAdminPermissions.add(assortmentViewPermission);
        superAdminPermissions.add(assortmentUpdatePermission);
        superAdminPermissions.add(assortmentArticleViewPermission);
        superAdminPermissions.add(countiesViewPermission);
        superAdminPermissions.add(agreementViewOwnPermission);
        superAdminPermissions.add(pricelistViewOwnPermission);
        superAdminPermissions.add(pricelistRowViewOwnPermission);
        superAdminPermissions.add(generalPricelistViewAllPermission);
        superAdminPermissions.add(generalPricelistPricelistViewAllPermission);
        superAdminPermissions.add(generalPricelistPricelistRowViewAllPermission);

        UserRole superAdminRole = new UserRole();
        superAdminRole.setName(UserRole.RoleName.Superadmin);
        superAdminRole.setDescription("Administratörsroll för tjänsteägare");
        superAdminRole.setPermissions(superAdminPermissions);
        em.persist(superAdminRole);
        userRoles.add(superAdminRole);

        // SUPPLIER ADMIN
        List<UserPermission> supplierAdminPermissions = new ArrayList<>();
        supplierAdminPermissions.add(organizationUpdateContactPermission);
        supplierAdminPermissions.add(userCreateOwnPermission);
        supplierAdminPermissions.add(userUpdateOwnPermission);
        supplierAdminPermissions.add(userDeleteOwnPermission);
        UserRole supplierAdminRole = new UserRole();
        supplierAdminRole.setName(UserRole.RoleName.Supplieradmin);
        supplierAdminRole.setDescription("Hantera användare och organisation (lokal admin)");
        supplierAdminRole.setPermissions(supplierAdminPermissions);
        em.persist(supplierAdminRole);
        userRoles.add(supplierAdminRole);

        // SUPPLIER AGREEMENT/GP VIEWER
        List<UserPermission> supplierAgreementViewerPermissions = new ArrayList<>();
        supplierAgreementViewerPermissions.add(agreementViewOwnPermission);
        supplierAgreementViewerPermissions.add(pricelistViewOwnPermission);
        supplierAgreementViewerPermissions.add(pricelistRowViewOwnPermission);
        supplierAgreementViewerPermissions.add(generalPricelistViewPermission);
        supplierAgreementViewerPermissions.add(generalPricelistPricelistViewOwnPermission);
        supplierAgreementViewerPermissions.add(generalPricelistPricelistRowViewOwnPermission);
        UserRole supplierAgreementViewerRole = new UserRole();
        supplierAgreementViewerRole.setName(UserRole.RoleName.SupplierAgreementViewer);
        supplierAgreementViewerRole.setDescription("Sök inköpsavtal och GP med priser");
        supplierAgreementViewerRole.setPermissions(supplierAgreementViewerPermissions);
        em.persist(supplierAgreementViewerRole);
        userRoles.add(supplierAgreementViewerRole);


        // SUPPLIER AGREEMENT/GP MANAGER
        List<UserPermission> supplierAgreementManagerPermissions = new ArrayList<>();
        supplierAgreementManagerPermissions.add(agreementViewOwnPermission);
        supplierAgreementManagerPermissions.add(pricelistViewOwnPermission);
        supplierAgreementManagerPermissions.add(pricelistExportViewPermission);
        supplierAgreementManagerPermissions.add(pricelistImportViewPermission);
        supplierAgreementManagerPermissions.add(pricelistRowViewOwnPermission);
        supplierAgreementManagerPermissions.add(pricelistRowCreateOwnPermission);
        supplierAgreementManagerPermissions.add(pricelistRowUpdateOwnPermission);
        supplierAgreementManagerPermissions.add(pricelistRowSendForCustomerApprovalPermission);
        supplierAgreementManagerPermissions.add(pricelistRowInactivatePermission);
        supplierAgreementManagerPermissions.add(pricelistRowReopenInactivatedPermission);
        supplierAgreementManagerPermissions.add(generalPricelistViewPermission);
        supplierAgreementManagerPermissions.add(generalPricelistCreateOwnPermission);
        supplierAgreementManagerPermissions.add(generalPricelistUpdateOwnPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistExportViewPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistImportViewPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistViewOwnPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistCreateOwnPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistUpdateOwnPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistRowViewOwnPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistRowCreateOwnPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistRowUpdateOwnPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistRowInactivateOwnPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistRowDeleteOwnPermission);

        UserRole supplierAgreementManagerRole = new UserRole();
        supplierAgreementManagerRole.setName(UserRole.RoleName.SupplierAgreementManager);
        supplierAgreementManagerRole.setDescription("Hantera prislistor och generell prislista (GP)");
        supplierAgreementManagerRole.setPermissions(supplierAgreementManagerPermissions);
        em.persist(supplierAgreementManagerRole);
        userRoles.add(supplierAgreementManagerRole);


        // SUPPLIER HANDLE PRODUCTS AND ARTICLES
        List<UserPermission> supplierProductAndArticlePermissions = new ArrayList<>();
        supplierProductAndArticlePermissions.add(productCreateOwnPermission);
        supplierProductAndArticlePermissions.add(productUpdateOwnPermission);
        supplierProductAndArticlePermissions.add(articleCreateOwnPermission);
        supplierProductAndArticlePermissions.add(articleUpdateOwnPermission);
        supplierProductAndArticlePermissions.add(mediaCreatePermission);
        supplierProductAndArticlePermissions.add(mediaDeletePermission);
        supplierProductAndArticlePermissions.add(productArticleExportViewPermission);
        supplierProductAndArticlePermissions.add(productArticleImportViewPermission);
        UserRole supplierProductAndArticleRole = new UserRole();
        supplierProductAndArticleRole.setName(UserRole.RoleName.SupplierProductAndArticleHandler);
        supplierProductAndArticleRole.setDescription("Hantera produkt och artikel");
        supplierProductAndArticleRole.setPermissions(supplierProductAndArticlePermissions);
        em.persist(supplierProductAndArticleRole);
        userRoles.add(supplierProductAndArticleRole);

        // CUSTOMER ADMIN
        List<UserPermission> customerAdminPermissions = new ArrayList<>();
        customerAdminPermissions.add(organizationUpdateContactPermission);
        customerAdminPermissions.add(userCreateOwnPermission);
        customerAdminPermissions.add(userUpdateOwnPermission);
        customerAdminPermissions.add(userDeleteOwnPermission);
        customerAdminPermissions.add(exportSettingsViewOwnPermission);
        customerAdminPermissions.add(exportSettingsUpdateOwnPermission);
        UserRole customerAdminRole = new UserRole();
        customerAdminRole.setName(UserRole.RoleName.Customeradmin);
        customerAdminRole.setDescription("Hantera användare, organisation och exportfil (lokal admin)");
        customerAdminRole.setPermissions(customerAdminPermissions);
        em.persist(customerAdminRole);
        userRoles.add(customerAdminRole);

        // CUSTOMER AGREEMENT/GP VIEWER
        List<UserPermission> customerAgreementViewerPermissions = new ArrayList<>();
        customerAgreementViewerPermissions.add(agreementViewOwnPermission);
        customerAgreementViewerPermissions.add(pricelistViewOwnPermission);
        customerAgreementViewerPermissions.add(pricelistRowViewOwnPermission);
        customerAgreementViewerPermissions.add(generalPricelistViewAllPermission);
        customerAgreementViewerPermissions.add(generalPricelistPricelistViewAllPermission);
        customerAgreementViewerPermissions.add(generalPricelistPricelistRowViewAllPermission);
        // the below 4 row permissions is required for this role since the role can be added
        // as prielist approver and then must be able to work with rows
        customerAgreementViewerPermissions.add(pricelistRowActivatePermission);
        customerAgreementViewerPermissions.add(pricelistRowDeclinePermission);
        customerAgreementViewerPermissions.add(pricelistRowApproveInactivatePermission);
        customerAgreementViewerPermissions.add(pricelistRowDeclineInactivatePermission);
        UserRole customerAgreementViewerRole = new UserRole();
        customerAgreementViewerRole.setName(UserRole.RoleName.CustomerAgreementViewer);
        customerAgreementViewerRole.setDescription("Sök inköpsavtal och GP med priser");
        customerAgreementViewerRole.setPermissions(customerAgreementViewerPermissions);
        em.persist(customerAgreementViewerRole);
        userRoles.add(customerAgreementViewerRole);

        // CUSTOMER AGREEMENT/GP MANAGER
        List<UserPermission> customerAgreementManagerPermissions = new ArrayList<>();
        customerAgreementManagerPermissions.add(agreementViewOwnPermission);
        customerAgreementManagerPermissions.add(agreementCreateOwnPermission);
        customerAgreementManagerPermissions.add(agreementUpdateOwnPermission);
        customerAgreementManagerPermissions.add(agreementDeleteOwnPermission);
        customerAgreementManagerPermissions.add(pricelistViewOwnPermission);
        customerAgreementManagerPermissions.add(pricelistCreateOwnPermission);
        customerAgreementManagerPermissions.add(pricelistUpdateOwnPermission);
        customerAgreementManagerPermissions.add(pricelistRowViewOwnPermission);
        customerAgreementManagerPermissions.add(pricelistRowActivatePermission);
        customerAgreementManagerPermissions.add(pricelistRowDeclinePermission);
        customerAgreementManagerPermissions.add(pricelistRowApproveInactivatePermission);
        customerAgreementManagerPermissions.add(pricelistRowDeclineInactivatePermission);
        UserRole customerAgreementManagerRole = new UserRole();
        customerAgreementManagerRole.setName(UserRole.RoleName.CustomerAgreementManager);
        customerAgreementManagerRole.setDescription("Hantera inköpsavtal");
        customerAgreementManagerRole.setPermissions(customerAgreementManagerPermissions);
        em.persist(customerAgreementManagerRole);
        userRoles.add(customerAgreementManagerRole);

        // CUSTOMER ASSORTMENT MANAGER
        List<UserPermission> customerAssortmentManagerPermissions = new ArrayList<>();
        customerAssortmentManagerPermissions.add(assortmentViewPermission);
        customerAssortmentManagerPermissions.add(assortmentCreatePermission);
        customerAssortmentManagerPermissions.add(assortmentUpdatePermission);
        customerAssortmentManagerPermissions.add(assortmentArticleViewPermission);
        customerAssortmentManagerPermissions.add(assortmentArticleCreateAllPermission);
        customerAssortmentManagerPermissions.add(assortmentArticleDeleteAllPermission);
        customerAssortmentManagerPermissions.add(countiesViewPermission);
        UserRole customerAssortmentManagerRole = new UserRole();
        customerAssortmentManagerRole.setName(UserRole.RoleName.CustomerAssortmentManager);
        customerAssortmentManagerRole.setDescription("Hantera utbud");
        customerAssortmentManagerRole.setPermissions(customerAssortmentManagerPermissions);
        em.persist(customerAssortmentManagerRole);
        userRoles.add(customerAssortmentManagerRole);

        // CUSTOMER ASSIGNED ASSORTMENT MANAGER
        List<UserPermission> customerAssignedAssortmentManagerPermissions = new ArrayList<>();
        customerAssignedAssortmentManagerPermissions.add(assortmentViewPermission);
        customerAssignedAssortmentManagerPermissions.add(assortmentArticleViewPermission);
        customerAssignedAssortmentManagerPermissions.add(assortmentArticleCreateOwnPermission);
        customerAssignedAssortmentManagerPermissions.add(assortmentArticleDeleteOwnPermission);
        customerAssignedAssortmentManagerPermissions.add(agreementViewOwnPermission);
        customerAssignedAssortmentManagerPermissions.add(pricelistViewOwnPermission);
        customerAssignedAssortmentManagerPermissions.add(pricelistRowViewOwnPermission);
        customerAssignedAssortmentManagerPermissions.add(countiesViewPermission);
        UserRole customerAssignedAssortmentManagerRole = new UserRole();
        customerAssignedAssortmentManagerRole.setName(UserRole.RoleName.CustomerAssignedAssortmentManager);
        customerAssignedAssortmentManagerRole.setDescription("Hantera tilldelade utbud");
        customerAssignedAssortmentManagerRole.setPermissions(customerAssignedAssortmentManagerPermissions);
        em.persist(customerAssignedAssortmentManagerRole);
        userRoles.add(customerAssignedAssortmentManagerRole);

        return userRoles;
    }

    private UserRole getRoleByName(UserRole.RoleName roleName, List<UserRole> userRoles) {
        for( UserRole userRole : userRoles ) {
            if( userRole.getName().equals(roleName) ) {
                return userRole;
            }
        }
        return null;
    }

    private void loadProducts(List<Organization> suppliers, List<CVCEDirective> directives, List<CVCEStandard> standards, List<CVOrderUnit> orderUnits) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.INFO, "loadProducts()" );
        Random r = new Random();
        List<Category> categoryHCodes = em.createQuery("SELECT c FROM Category c WHERE c.articleType = :articleType AND c.code IS NOT NULL")
            .setParameter("articleType", Article.Type.H)
            .getResultList();
        Category categoryHCode = categoryHCodes.get(0);
        CategoryAPI categoryHCodeAPI = new CategoryAPI();
        categoryHCodeAPI.setId(categoryHCode.getUniqueId());

        List<Category> categoryTNoCodes = em.createQuery("SELECT c FROM Category c WHERE c.articleType = :articleType AND c.code IS NULL")
            .setParameter("articleType", Article.Type.T)
            .getResultList();
        Category categoryTNoCode = categoryTNoCodes.get(0);
        CategoryAPI categoryTNoCodeAPI = new CategoryAPI();
        categoryTNoCodeAPI.setId(categoryTNoCode.getUniqueId());

        List<Category> categoryINoCodes = em.createQuery("SELECT c FROM Category c WHERE c.articleType = :articleType AND c.code IS NULL")
            .setParameter("articleType", Article.Type.I)
            .getResultList();
        Category categoryINoCode = categoryINoCodes.get(0);
        CategoryAPI categoryINoCodeAPI = new CategoryAPI();
        categoryINoCodeAPI.setId(categoryINoCode.getUniqueId());

        List<Category> categoryRNoCodes = em.createQuery("SELECT c FROM Category c WHERE c.articleType = :articleType AND c.code IS NULL")
            .setParameter("articleType", Article.Type.R)
            .getResultList();
        Category categoryRNoCode = categoryRNoCodes.get(0);
        CategoryAPI categoryRNoCodeAPI = new CategoryAPI();
        categoryRNoCodeAPI.setId(categoryRNoCode.getUniqueId());

        List<Category> categoryTjNoCodes = em.createQuery("SELECT c FROM Category c WHERE c.articleType = :articleType AND c.code IS NULL")
            .setParameter("articleType", Article.Type.Tj)
            .getResultList();
        Category categoryTjNoCode = categoryTjNoCodes.get(0);
        CategoryAPI categoryTjNoCodeAPI = new CategoryAPI();
        categoryTjNoCodeAPI.setId(categoryTjNoCode.getUniqueId());

        MediaImageAPI mediaImageAPI = new MediaImageAPI();
        for( Organization supplier : suppliers ) {
            List<UserEngagement> supplierAdmins = em.createNamedQuery(UserEngagement.GET_BY_ORGANIZATION_AND_ROLES).
                setParameter("organizationUniqueId", supplier.getUniqueId()).
                setParameter("roleNames", UserRole.RoleName.Supplieradmin).
                getResultList();
            UserAPI userAPI = UserMapper.map(supplierAdmins.get(0), true);
            List<ProductAPI> organizationProductAPIs = new ArrayList<>();
            for( int i = 0; i<40; i++ ) {
                ProductAPI productAPI = new ProductAPI();
                productAPI.setOrganizationId(supplier.getUniqueId());
                productAPI.setProductName(supplier.getOrganizationName() + " - Produkt " + i );
                productAPI.setProductNumber(supplier.getOrganizationNumber() + "-" + i);
                mediaImageAPI = new MediaImageAPI();
                mediaImageAPI.setProductId(productAPI.getId());
                mediaImageAPI.setMainImage(true);
                mediaImageAPI.setUrl("test.png");
                mediaImageAPI.setAlternativeText("test");
                productAPI.setMainImageUrl("https://upload.wikimedia.org/wikipedia/commons/thumb/2/28/JPG_Test.jpg/477px-JPG_Test.jpg");
                productAPI.setMainImageAltText("test " + i);
                productAPI.setCeMarked(true);
                productAPI.setStatus(Product.Status.PUBLISHED.toString());
                CVCEDirectiveAPI randomDirective = new CVCEDirectiveAPI();
                randomDirective.setId(directives.get(r.nextInt(directives.size())).getUniqueId());
                productAPI.setCeDirective(randomDirective);
                CVCEStandardAPI randomStandardAPI = new CVCEStandardAPI();
                randomStandardAPI.setId(standards.get(r.nextInt(standards.size())).getUniqueId());
                productAPI.setCeStandard(randomStandardAPI);
                productAPI.setCategory(categoryHCodeAPI);
                productAPI.setSupplementedInformation("Mera information " + productAPI.getProductName());
                productAPI.setDescriptionForElvasjuttiosju("Beskrivning som visas på 1177.se om produkten utbudas " + productAPI.getProductName());

                CVOrderUnitAPI cVOrderUnitAPI = new CVOrderUnitAPI();
                cVOrderUnitAPI.setId(orderUnits.get(r.nextInt(orderUnits.size())).getUniqueId());
                productAPI.setOrderUnit(cVOrderUnitAPI);

                productAPI = productController.createProduct(supplier.getUniqueId(), productAPI, userAPI, "na", null, true);
                for( int x = 0; x< amountOfArticlesPerProductGenerated; x++ ) {
                    ArticleAPI articleAPI = new ArticleAPI();
                    articleAPI.setOrganizationId(supplier.getUniqueId());
                    articleAPI.setBasedOnProduct(productAPI);
                    articleAPI.setStatus(Product.Status.PUBLISHED.toString());
                    articleAPI.setArticleName("Produkt " + i + " - Huvudhjälpmedel " + x);
                    articleAPI.setArticleNumber("P-" + i + "-H-" + x);
                    articleAPI.setCategory(categoryHCodeAPI);
                    articleAPI.setOrderUnit(cVOrderUnitAPI);
                    if(x % 2 == 1) {
                        articleAPI.setSupplementedInformation(null);
                    } else {
                        articleAPI.setSupplementedInformation("Mera information " + articleAPI.getArticleName());
                    }
                    articleController.createArticle(supplier.getUniqueId(), articleAPI, userAPI, "na", null, true);
                }
                organizationProductAPIs.add(productAPI);
            }
            for( int i = 0; i<2; i++ ) {
                ArticleAPI articleAPI = new ArticleAPI();
                articleAPI.setOrganizationId(supplier.getUniqueId());
                articleAPI.setCategory(categoryTNoCodeAPI);
                articleAPI.setArticleName("Tillbehör " + i);
                articleAPI.setArticleNumber("T-"+ i);
                articleAPI.setStatus(Product.Status.PUBLISHED.toString());
                articleAPI.setSupplementedInformation("Mera information " + articleAPI.getArticleName());
                CVOrderUnitAPI cVOrderUnitAPI = new CVOrderUnitAPI();
                cVOrderUnitAPI.setId(orderUnits.get(r.nextInt(orderUnits.size())).getUniqueId());
                articleAPI.setOrderUnit(cVOrderUnitAPI);
                List<ProductAPI> fitsToProductAPIs = new ArrayList<>();
                fitsToProductAPIs.add(organizationProductAPIs.get(r.nextInt(organizationProductAPIs.size())));
                articleAPI.setFitsToProducts(fitsToProductAPIs);
                articleController.createArticle(supplier.getUniqueId(), articleAPI, userAPI, "na", null, true);
            }
            for( int i = 0; i<2; i++ ) {
                ArticleAPI articleAPI = new ArticleAPI();
                articleAPI.setOrganizationId(supplier.getUniqueId());
                articleAPI.setCategory(categoryINoCodeAPI);
                articleAPI.setArticleName("Inställning " + i);
                articleAPI.setArticleNumber("I-"+ i);
                articleAPI.setStatus(Product.Status.PUBLISHED.toString());
                articleAPI.setSupplementedInformation("Mera information " + articleAPI.getArticleName());
                CVOrderUnitAPI cVOrderUnitAPI = new CVOrderUnitAPI();
                cVOrderUnitAPI.setId(orderUnits.get(r.nextInt(orderUnits.size())).getUniqueId());
                articleAPI.setOrderUnit(cVOrderUnitAPI);
                List<ProductAPI> fitsToProductAPIs = new ArrayList<>();
                fitsToProductAPIs.add(organizationProductAPIs.get(r.nextInt(organizationProductAPIs.size())));
                articleAPI.setFitsToProducts(fitsToProductAPIs);
                articleController.createArticle(supplier.getUniqueId(), articleAPI, userAPI, "na", null, true);
            }
            for( int i = 0; i<2; i++ ) {
                ArticleAPI articleAPI = new ArticleAPI();
                articleAPI.setOrganizationId(supplier.getUniqueId());
                articleAPI.setCategory(categoryRNoCodeAPI);
                articleAPI.setArticleName("Reservdel " + i);
                articleAPI.setArticleNumber("R-"+ i);
                articleAPI.setStatus(Product.Status.PUBLISHED.toString());
                articleAPI.setSupplementedInformation("Mera information " + articleAPI.getArticleName());
                CVOrderUnitAPI cVOrderUnitAPI = new CVOrderUnitAPI();
                cVOrderUnitAPI.setId(orderUnits.get(r.nextInt(orderUnits.size())).getUniqueId());
                articleAPI.setOrderUnit(cVOrderUnitAPI);
                List<ProductAPI> fitsToProductAPIs = new ArrayList<>();
                fitsToProductAPIs.add(organizationProductAPIs.get(r.nextInt(organizationProductAPIs.size())));
                articleAPI.setFitsToProducts(fitsToProductAPIs);
                articleController.createArticle(supplier.getUniqueId(), articleAPI, userAPI, "na", null, true);
            }

            for( int i = 0; i<2; i++ ) {
                ArticleAPI articleAPI = new ArticleAPI();
                articleAPI.setOrganizationId(supplier.getUniqueId());
                articleAPI.setCategory(categoryTjNoCodeAPI);
                articleAPI.setArticleName("Tjänst " + i);
                articleAPI.setArticleNumber("TJ-"+ i);
                articleAPI.setStatus(Product.Status.PUBLISHED.toString());
                articleAPI.setSupplementedInformation("Mera information " + articleAPI.getArticleName());
                CVOrderUnitAPI cVOrderUnitAPI = new CVOrderUnitAPI();
                cVOrderUnitAPI.setId(orderUnits.get(r.nextInt(orderUnits.size())).getUniqueId());
                articleAPI.setOrderUnit(cVOrderUnitAPI);
                List<ProductAPI> fitsToProductAPIs = new ArrayList<>();
                fitsToProductAPIs.add(organizationProductAPIs.get(r.nextInt(organizationProductAPIs.size())));
                articleAPI.setFitsToProducts(fitsToProductAPIs);
                articleController.createArticle(supplier.getUniqueId(), articleAPI, userAPI, "na", null, true);
            }
        }
    }

    private void loadCountiesAndMunicipalities() {
        LOG.log( Level.INFO, "loadCountiesAndMunicipalities()" );
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            inputStream = this.getClass().getClassLoader().getResourceAsStream("countymunicipality.csv");
            if( inputStream != null ) {
                bufferedReader =  new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                CVCounty lastCounty = null;
                while( (nextLine = bufferedReader.readLine()) != null ) {
                    if( first ) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if( nextLine != null && !nextLine.isEmpty() ) {
                        String[] lineParts = nextLine.split(";", -1);
                        String code = lineParts[0];
                        String name = lineParts[1];
                        Boolean showMunicipalities;
                        if( code.length() == 2 ) {
                            String showMunicipalitiesString = lineParts[2];
                            showMunicipalities = Boolean.parseBoolean(showMunicipalitiesString);
                            CVCounty county = new CVCounty();
                            county.setCode(code);
                            county.setName(name);
                            county.setShowMunicipalities(showMunicipalities);
                            em.persist(county);
                            lastCounty = county;
                        } else {
                            CVMunicipality municipality = new CVMunicipality();
                            municipality.setCode(code);
                            municipality.setName(name);
                            municipality.setCounty(lastCounty);
                            em.persist(municipality);
                        }
                    }
                }

            } else {
                LOG.log(Level.INFO, "No countymunicipality.csv found");
            }
        } catch (IOException ex) {
            LOG.log(Level.INFO, "No countymunicipality.csv found", ex);
        } finally {
            try {
                if( bufferedReader != null ) {
                    bufferedReader.close();
                }
                if( inputStream != null ) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to close countymunicipality.csv", ex);
            }
        }
    }

    private List<Category> loadCategories() {
        LOG.log( Level.INFO, "loadCategories()" );
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        List<Category> leafNodeCategories = new ArrayList<>();
        List<Category> firstLevelCategories = new ArrayList<>();
        try {
            inputStream = this.getClass().getClassLoader().getResourceAsStream("categories.csv");
            if( inputStream != null ) {
                bufferedReader =  new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                while( (nextLine = bufferedReader.readLine()) != null ) {
                    if( first ) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if( nextLine != null && !nextLine.isEmpty() ) {
                        String[] lineParts = nextLine.split(";", -1);
                        String code = lineParts[0];
                        String name = lineParts[1];
                        String description = lineParts[2];
                        Category category = new Category();
                        category.setCode(code.equals("") ? null: code);
                        category.setName(name);
                        LOG.log( Level.INFO, "adding category with code " + category.getCode() + "and name " + category.getName());
                        category.setDescription(description);
                        if( lineParts.length > 3 ) {
                            String type = lineParts[3];
                            if( type != null && !type.isEmpty() ) {
                                category.setArticleType(Article.Type.valueOf(type));
                            }
                        }
                        if( code.length() == 2 || code.length() == 0 ) {
                            firstLevelCategories.add(category);
                        }
                        em.persist(category);
                        if( category.getArticleType() != null ) {
                            leafNodeCategories.add(category);
                        }
                    }
                }

            } else {
                LOG.log(Level.INFO, "No categories.csv found");
            }
        } catch (IOException ex) {
            LOG.log(Level.INFO, "No categories.csv found", ex);
        } finally {
            try {
                if( bufferedReader != null ) {
                    bufferedReader.close();
                }
                if( inputStream != null ) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to close categories.csv", ex);
            }
        }
        for( Category firstLevelCategory : firstLevelCategories ) {
            handleFirstLevelCategory( firstLevelCategory );
        }

        return leafNodeCategories;
    }

    private void loadExternalCategoryGroupings() {
        LOG.log( Level.INFO, "loadExternalCategoryGroupings()" );
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("externalcategorygroupingsv2.json");
        if( inputStream != null ) {
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8"))) ) {
                String jsonString = bufferedReader.lines().collect(Collectors.joining("\n"));
                JsonArray jsonArray = Json.createReader(new StringReader(jsonString)).readArray();
                for( int i=0; i<jsonArray.size(); i++ ) {
                    JsonObject jsonObject = jsonArray.getJsonObject(i);
                    ExternalCategoryGrouping externalCategoryGrouping = new ExternalCategoryGrouping();
//                    externalCategoryGrouping.setDisplayOrder(jsonObject.getInt("displayOrder"));
                    externalCategoryGrouping.setName(jsonObject.getString("name"));
                    externalCategoryGrouping.setDescription(jsonObject.getString("description"));
//                    externalCategoryGrouping.setParent((ExternalCategoryGrouping) jsonObject.get("parentId"));
//                    externalCategoryGrouping.setCategory((Category) jsonObject.get("categoryId"));
                    em.persist(externalCategoryGrouping);
//                    JsonArray children = jsonObject.getJsonArray("children");
//                    if( children != null ) {
//                        handleExternalCategoryGroupingsChildren(children, externalCategoryGrouping);
//                    }
                }
            } catch (IOException ex) {
                LOG.log(Level.INFO, "No externalcategorygroupings.json found", ex);
            } finally {
                try {
                    inputStream.close();
                } catch (IOException ex) {
                    LOG.log(Level.SEVERE, "Failed to close externalcategorygroupings.json", ex);
                }
            }
        } else {
            LOG.log(Level.INFO, "No categories.csv found");
        }
    }


    private void handleExternalCategoryGroupingsChildren(JsonArray children, ExternalCategoryGrouping parent) {
        for( int i=0; i<children.size(); i++ ) {
            JsonObject jsonObject = children.getJsonObject(i);
            ExternalCategoryGrouping externalCategoryGrouping = new ExternalCategoryGrouping();
            externalCategoryGrouping.setName(jsonObject.getString("name"));
            externalCategoryGrouping.setDescription(jsonObject.getString("description"));
            externalCategoryGrouping.setDisplayOrder(i+1);
            externalCategoryGrouping.setParent(parent);
            if( jsonObject.containsKey("category") ) {
                String code = jsonObject.getString("category");
                List<Category> possibleCategories = em.createNamedQuery(Category.FIND_BY_CODE).
                        setParameter("code", code).getResultList();
                if( possibleCategories == null || possibleCategories.isEmpty() ) {
                    LOG.log(Level.WARNING, "Failed to find category by code: " + code);
                } else {
                    externalCategoryGrouping.setCategory(possibleCategories.get(0));
                }
            }
            em.persist(externalCategoryGrouping);
            JsonArray childrenChildren = jsonObject.getJsonArray("children");
            if( childrenChildren != null ) {
                handleExternalCategoryGroupingsChildren(childrenChildren, externalCategoryGrouping);
            }
        }
    }

    private void loadCategoryProperties() {
        LOG.log( Level.INFO, "loadCategoryProperties()" );
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            inputStream = this.getClass().getClassLoader().getResourceAsStream("categoryproperties.csv");
            if( inputStream != null ) {
                bufferedReader =  new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                while( (nextLine = bufferedReader.readLine()) != null ) {
                    if( first ) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if( nextLine != null && !nextLine.isEmpty() ) {
                        String[] lineParts = nextLine.split(";", -1);
                        String code = lineParts[0];
                        String name = lineParts[1];
                        String description = lineParts[2];
                        String type = lineParts[3];
                        String list = lineParts[4];

                        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
                        List<Category> categorys = em.createNamedQuery(Category.FIND_BY_CODE_AND_LEAFNODE).
                                setParameter("code", code).
                                getResultList();
                        if( categorys != null && categorys.size() == 1 ) {
                            categorySpecificProperty.setCategory(categorys.get(0));
                        } else {
                            LOG.log( Level.SEVERE, "Unable to find category by code: {0}", new Object[] {code} );
                            continue;
                        }
                        categorySpecificProperty.setName(name);
                        categorySpecificProperty.setDescription(description);
                        categorySpecificProperty.setType(CategorySpecificProperty.Type.valueOf(type));
                        if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_SINGLE ||
                                categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_MULTIPLE ) {
                            String[] listValues = list.split(":");
                            List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues = new ArrayList<>();
                            for( String listValue : listValues ) {
                                CategorySpecificPropertyListValue categorySpecificPropertyListValue = new CategorySpecificPropertyListValue();
                                categorySpecificPropertyListValue.setCategorySpecificProperty(categorySpecificProperty);
                                categorySpecificPropertyListValue.setValue(listValue);
                                categorySpecificPropertyListValue.setCode(listValue);
                                categorySpecificPropertyListValues.add(categorySpecificPropertyListValue);
                            }
                            categorySpecificProperty.setCategorySpecificPropertyListValues(categorySpecificPropertyListValues);
                        }
                        em.persist(categorySpecificProperty);
                    }
                }

            } else {
                LOG.log(Level.INFO, "No categories.csv found");
            }
        } catch (IOException ex) {
            LOG.log(Level.INFO, "No categories.csv found", ex);
        } finally {
            try {
                if( bufferedReader != null ) {
                    bufferedReader.close();
                }
                if( inputStream != null ) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to close categories.csv", ex);
            }
        }
    }

    private void handleFirstLevelCategory(Category firstLevelCategory) {
        if( firstLevelCategory.getCode() != null && !firstLevelCategory.getCode().isEmpty() ) {
            List<Category> secondLevelCategories = em.createQuery("SELECT c FROM Category c WHERE c.code LIKE :code").
                    setParameter("code", firstLevelCategory.getCode() + "%").
                    getResultList();
            for( Category secondLevelCategory : secondLevelCategories ) {
                if( secondLevelCategory.getCode().length() == 4 ) {
                    secondLevelCategory.setParent(firstLevelCategory);
                    em.merge(secondLevelCategory);
                    handleSecondLevelCategory(secondLevelCategory);
                }
            }
        }
    }

    private void handleSecondLevelCategory(Category secondLevelCategory) {
        List<Category> thirdLevelCategories = em.createQuery("SELECT c FROM Category c WHERE c.code LIKE :code").
                setParameter("code", secondLevelCategory.getCode() + "%").
                getResultList();
        for( Category thirdLevelCategory : thirdLevelCategories ) {
            if( thirdLevelCategory.getCode().length() == 6 ) {
                thirdLevelCategory.setParent(secondLevelCategory);
                em.merge(thirdLevelCategory);
                handleFourthLevelCategory(thirdLevelCategory);
            }
        }
    }

    private void handleFourthLevelCategory(Category thirdLevelCategory) {
        List<Category> fourthLevelCategories = em.createQuery("SELECT c FROM Category c WHERE c.code LIKE :code").
                setParameter("code", thirdLevelCategory.getCode() + "%").
                getResultList();
        for( Category fourthLevelCategory : fourthLevelCategories ) {
            if( fourthLevelCategory.getCode().length() > 6 ) {
                fourthLevelCategory.setParent(thirdLevelCategory);
                em.merge(fourthLevelCategory);
            }
        }
    }

    private List<CVCEDirective> loadDirectives() {
        LOG.log( Level.INFO, "loadDirectives()" );
        List<CVCEDirective> directives = new ArrayList<>();
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            inputStream = this.getClass().getClassLoader().getResourceAsStream("directives.csv");
            if( inputStream != null ) {
                bufferedReader =  new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                while( (nextLine = bufferedReader.readLine()) != null ) {
                    if( first ) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if( nextLine != null && !nextLine.isEmpty() ) {
                        String[] lineParts = nextLine.split(";");
                        int order = Integer.parseInt(lineParts[0]);
                        String code = lineParts[1];
                        String name = lineParts[2];
                        CVCEDirective directive = new CVCEDirective();
                        directive.setCode(code);
                        directive.setDisplayOrder(order);
                        directive.setName(name);
                        em.persist(directive);
                        directives.add(directive);
                    }
                }
            } else {
                LOG.log(Level.INFO, "No directives.csv found");
            }
        } catch (IOException ex) {
            LOG.log(Level.INFO, "No directives.csv found", ex);
        } finally {
            try {
                if( bufferedReader != null ) {
                    bufferedReader.close();
                }
                if( inputStream != null ) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to close directives.csv", ex);
            }
        }
        return directives;
    }

    private List<CVCEStandard> loadStandards() {
        LOG.log( Level.INFO, "loadStandards()" );
        List<CVCEStandard> standards = new ArrayList<>();
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            inputStream = this.getClass().getClassLoader().getResourceAsStream("standards.csv");
            if( inputStream != null ) {
                bufferedReader =  new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                while( (nextLine = bufferedReader.readLine()) != null ) {
                    if( first ) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if( nextLine != null && !nextLine.isEmpty() ) {
                        String[] lineParts = nextLine.split(";");
                        int order = Integer.parseInt(lineParts[0]);
                        String code = lineParts[1];
                        String name = lineParts[2];
                        CVCEStandard standard = new CVCEStandard();
                        standard.setDisplayOrder(order);
                        standard.setCode(code);
                        standard.setName(name);
                        em.persist(standard);
                        standards.add(standard);
                    }
                }
            } else {
                LOG.log(Level.INFO, "No standards.csv found");
            }
        } catch (IOException ex) {
            LOG.log(Level.INFO, "No standards.csv found", ex);
        } finally {
            try {
                if( bufferedReader != null ) {
                    bufferedReader.close();
                }
                if( inputStream != null ) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to close standards.csv", ex);
            }
        }
        return standards;
    }

    private List<CVOrderUnit> loadOrderUnits() {
        LOG.log( Level.INFO, "loadOrderUnits()" );
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        List<CVOrderUnit> orderUnits = new ArrayList<>();
        try {
            inputStream = this.getClass().getClassLoader().getResourceAsStream("orderunits.csv");
            if( inputStream != null ) {
                bufferedReader =  new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                while( (nextLine = bufferedReader.readLine()) != null ) {
                    if( first ) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if( nextLine != null && !nextLine.isEmpty() ) {
                        String[] lineParts = nextLine.split(";");
                        int order = Integer.parseInt(lineParts[0]);
                        String code = lineParts[1];
                        String name = lineParts[2];
                        CVOrderUnit orderUnit = new CVOrderUnit();
                        orderUnit.setDisplayOrder(order);
                        orderUnit.setCode(code);
                        orderUnit.setName(name);
                        em.persist(orderUnit);
                        orderUnits.add(orderUnit);
                    }
                }
            } else {
                LOG.log(Level.INFO, "No orderunits.csv found");
            }
        } catch (IOException ex) {
            LOG.log(Level.INFO, "No orderunits.csv found", ex);
        } finally {
            try {
                if( bufferedReader != null ) {
                    bufferedReader.close();
                }
                if( inputStream != null ) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to close orderunits.csv", ex);
            }
        }

        return orderUnits;
    }

    private List<CVPackageUnit> loadPackageUnits() {
        LOG.log( Level.INFO, "loadPackageUnits()" );
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        List<CVPackageUnit> packageUnits = new ArrayList<>();
        try {
            inputStream = this.getClass().getClassLoader().getResourceAsStream("packageunits.csv");
            if( inputStream != null ) {
                bufferedReader =  new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                while( (nextLine = bufferedReader.readLine()) != null ) {
                    if( first ) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if( nextLine != null && !nextLine.isEmpty() ) {
                        String[] lineParts = nextLine.split(";");
                        int order = Integer.parseInt(lineParts[0]);
                        String code = lineParts[1];
                        String name = lineParts[2];
                        CVPackageUnit packageUnit = new CVPackageUnit();
                        packageUnit.setDisplayOrder(order);
                        packageUnit.setCode(code);
                        packageUnit.setName(name);
                        em.persist(packageUnit);
                        packageUnits.add(packageUnit);
                    }
                }
            } else {
                LOG.log(Level.INFO, "No packageunits.csv found");
            }
        } catch (IOException ex) {
            LOG.log(Level.INFO, "No packageunits.csv found", ex);
        } finally {
            try {
                if( bufferedReader != null ) {
                    bufferedReader.close();
                }
                if( inputStream != null ) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to close packageunits.csv", ex);
            }
        }

        return packageUnits;
    }

    private List<CVDocumentType> loadDocumentTypes() {
        LOG.log( Level.INFO, "loadDocumentTypes()" );
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        List<CVDocumentType> documentTypes = new ArrayList<>();
        try {
            inputStream = this.getClass().getClassLoader().getResourceAsStream("documenttypes.csv");
            if( inputStream != null ) {
                bufferedReader =  new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                while( (nextLine = bufferedReader.readLine()) != null ) {
                    if( first ) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if( nextLine != null && !nextLine.isEmpty() ) {
                        String[] lineParts = nextLine.split(";");
                        int order = Integer.parseInt(lineParts[0]);
                        String code = lineParts[1];
                        String value = lineParts[2];
                        CVDocumentType documentType = new CVDocumentType();
                        documentType.setDisplayOrder(order);
                        documentType.setCode(code);
                        documentType.setValue(value);
                        em.persist(documentType);
                        documentTypes.add(documentType);
                    }
                }
            } else {
                LOG.log(Level.INFO, "No documenttypes.csv found");
            }
        } catch (IOException ex) {
            LOG.log(Level.INFO, "No documenttypes.csv found", ex);
        } finally {
            try {
                if( bufferedReader != null ) {
                    bufferedReader.close();
                }
                if( inputStream != null ) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to close documenttypes.csv", ex);
            }
        }

        return documentTypes;
    }

    private List<CVGuaranteeUnit> loadGuaranteeUnits() {
        LOG.log( Level.INFO, "loadGuaranteeUnits()" );
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        List<CVGuaranteeUnit> guaranteeUnits = new ArrayList<>();
        try {
            inputStream = this.getClass().getClassLoader().getResourceAsStream("guaranteeunits.csv");
            if( inputStream != null ) {
                bufferedReader =  new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                while( (nextLine = bufferedReader.readLine()) != null ) {
                    if( first ) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if( nextLine != null && !nextLine.isEmpty() ) {
                        String[] lineParts = nextLine.split(";");
                        int order = Integer.parseInt(lineParts[0]);
                        String code = lineParts[1];
                        String name = lineParts[2];
                        CVGuaranteeUnit guaranteeUnit = new CVGuaranteeUnit();
                        guaranteeUnit.setDisplayOrder(order);
                        guaranteeUnit.setCode(code);
                        guaranteeUnit.setName(name);
                        em.persist(guaranteeUnit);
                        guaranteeUnits.add(guaranteeUnit);
                    }
                }
            } else {
                LOG.log(Level.INFO, "No guaranteeunits.csv found");
            }
        } catch (IOException ex) {
            LOG.log(Level.INFO, "No guaranteeunits.csv found", ex);
        } finally {
            try {
                if( bufferedReader != null ) {
                    bufferedReader.close();
                }
                if( inputStream != null ) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to close guaranteeunits.csv", ex);
            }
        }

        return guaranteeUnits;
    }

    private List<CVPreventiveMaintenance> loadPreventiveMaintenances() {
        LOG.log( Level.INFO, "loadPreventiveMaintenances()" );
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        List<CVPreventiveMaintenance> preventiveMaintenances = new ArrayList<>();
        try {
            inputStream = this.getClass().getClassLoader().getResourceAsStream("preventivemaintenances.csv");
            if( inputStream != null ) {
                bufferedReader =  new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                while( (nextLine = bufferedReader.readLine()) != null ) {
                    if( first ) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if( nextLine != null && !nextLine.isEmpty() ) {
                        String[] lineParts = nextLine.split(";");
                        int order = Integer.parseInt(lineParts[0]);
                        String code = lineParts[1];
                        String name = lineParts[2];
                        CVPreventiveMaintenance preventiveMaintenance = new CVPreventiveMaintenance();
                        preventiveMaintenance.setDisplayOrder(order);
                        preventiveMaintenance.setCode(code);
                        preventiveMaintenance.setName(name);
                        em.persist(preventiveMaintenance);
                        preventiveMaintenances.add(preventiveMaintenance);
                    }
                }
            } else {
                LOG.log(Level.INFO, "No preventivemaintenances.csv found");
            }
        } catch (IOException ex) {
            LOG.log(Level.INFO, "No preventivemaintenances.csv found", ex);
        } finally {
            try {
                if( bufferedReader != null ) {
                    bufferedReader.close();
                }
                if( inputStream != null ) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to close preventivemaintenances.csv", ex);
            }
        }

        return preventiveMaintenances;
    }

    private void loadAgreements(List<Organization> suppliers, List<Organization> customers, CVGuaranteeUnit unit, List<CVPreventiveMaintenance> preventiveMaintenances) {
        LOG.log( Level.INFO, "loadAgreements()" );
        Random r = new Random();
        int c = 1;
        for( Organization customer : customers ) {
            for( int i = 0; i<26; i++ ) {
                Agreement agreement = new Agreement();
                Organization randomSupplier = suppliers.get(r.nextInt(suppliers.size()));
                agreement.setAgreementName("Avtal " + i + " K: " + customer.getOrganizationName() + ", L: " + randomSupplier.getOrganizationName() );
                agreement.setAgreementNumber("A-" + customer.getOrganizationName() + "-" + i);
                Calendar yesterday = Calendar.getInstance();
                yesterday.add(Calendar.DAY_OF_YEAR, -1);
                yesterday.set(Calendar.HOUR_OF_DAY, 0);
                yesterday.set(Calendar.MINUTE, 0);
                yesterday.set(Calendar.SECOND, 0);
                agreement.setValidFrom(DateUtils.beginningOfDay(yesterday.getTime()));
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.YEAR, 1);
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
                agreement.setStatus(Agreement.Status.CURRENT);
                agreement.setValidTo(calendar.getTime());
                agreement.setCustomer(customer);
                agreement.setSupplier(randomSupplier);
                agreement.setDeliveryTimeH(1);
                agreement.setDeliveryTimeI(2);
                agreement.setDeliveryTimeR(3);
                agreement.setDeliveryTimeT(4);
                agreement.setDeliveryTimeTJ(5);
                agreement.setWarrantyQuantityH(1);
                agreement.setWarrantyQuantityI(2);
                agreement.setWarrantyQuantityR(3);
                agreement.setWarrantyQuantityT(4);
                agreement.setWarrantyQuantityTJ(5);
                agreement.setWarrantyTermsH("test 1");
                agreement.setWarrantyTermsI("test 2");
                agreement.setWarrantyTermsR("test 3");
                agreement.setWarrantyTermsT("test 4");
                agreement.setWarrantyTermsTJ("test 5");
                agreement.setWarrantyValidFromH(preventiveMaintenances.get(0));
                agreement.setWarrantyValidFromI(preventiveMaintenances.get(0));
                agreement.setWarrantyValidFromR(preventiveMaintenances.get(0));
                agreement.setWarrantyValidFromT(preventiveMaintenances.get(0));
                agreement.setWarrantyValidFromTJ(preventiveMaintenances.get(0));
                agreement.setWarrantyQuantityHUnit(unit);
                agreement.setWarrantyQuantityIUnit(unit);
                agreement.setWarrantyQuantityRUnit(unit);
                agreement.setWarrantyQuantityTUnit(unit);
                agreement.setWarrantyQuantityTJUnit(unit);
                List<UserRole.RoleName> roleNames = new ArrayList<>();
                roleNames.add(UserRole.RoleName.CustomerAgreementManager);
                List<UserEngagement> userEngagements = em.createNamedQuery(UserEngagement.GET_BY_ORGANIZATION_AND_ROLES).
                    setParameter("organizationUniqueId", customer.getUniqueId()).
                    setParameter("roleNames", roleNames).
                    getResultList();
                agreement.setCustomerPricelistApprovers(userEngagements);
                em.persist(agreement);

                // create first pricelist
                AgreementPricelist pricelist = new AgreementPricelist();
                pricelist.setAgreement(agreement);
                pricelist.setNumber(defaultPricelistNumber);
                pricelist.setValidFrom(agreement.getValidFrom());
                em.persist(pricelist);

                List<Article> articles = em.createQuery("SELECT a FROM Article a Where a.organization.uniqueId = :organizationUniqueId").
                        setParameter("organizationUniqueId", randomSupplier.getUniqueId()).
                        getResultList();
                // add a price list rows
                for(Article article : articles ) {
                    AgreementPricelistRow pricelistRow = new AgreementPricelistRow();
                    pricelistRow.setArticle(article);
                    pricelistRow.setPricelist(pricelist);
                    pricelistRow.setPrice(BigDecimal.valueOf(100));
                    pricelistRow.setStatus(AgreementPricelistRow.Status.CREATED);
                    pricelistRow.setValidFrom(null); // inherit from pricelist
                    pricelistRow.setLeastOrderQuantity(1);
                    em.persist(pricelistRow);
                }
            }
            LOG.log( Level.INFO,  "Loaded agreements for " + c + " of " + customers.size() + " customers...");
            c++;
        }
    }

}
