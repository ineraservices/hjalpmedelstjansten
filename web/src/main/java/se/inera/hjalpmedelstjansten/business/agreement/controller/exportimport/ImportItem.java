package se.inera.hjalpmedelstjansten.business.agreement.controller.exportimport;

import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistRowAPI;


public class ImportItem {

    private AgreementPricelistRowAPI agreementPricelistRowAPI;
    private GeneralPricelistPricelistRowAPI generalPricelistPricelistRowAPI;
    private int rowNumber;

    public ImportItem(AgreementPricelistRowAPI agreementPricelistRowAPI, int rowNumber) {
        this.agreementPricelistRowAPI = agreementPricelistRowAPI;
        this.rowNumber = rowNumber;
    }

    public ImportItem(GeneralPricelistPricelistRowAPI generalPricelistPricelistRowAPI, int rowNumber) {
        this.generalPricelistPricelistRowAPI = generalPricelistPricelistRowAPI;
        this.rowNumber = rowNumber;
    }
    
    public AgreementPricelistRowAPI getAgreementPricelistRowAPI() {
        return agreementPricelistRowAPI;
    }

    public GeneralPricelistPricelistRowAPI getGeneralPricelistPricelistRowAPI() {
        return generalPricelistPricelistRowAPI;
    }

    public int getRowNumber() {
        return rowNumber;
    }

}
