package se.inera.hjalpmedelstjansten.business;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

@ApplicationPath("resources/v1")
public class JAXRSConfiguration extends Application {

}
