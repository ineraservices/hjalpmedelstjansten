package se.inera.hjalpmedelstjansten.business.generalpricelist.controller;

import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistAPI;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelist;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

/**
 * Validation methods for general pricelist pricelist
 *
 */
@Stateless
public class GeneralPricelistPricelistValidation {

    @Inject
    HjmtLogger LOG;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    GeneralPricelistPricelistController pricelistController;

    /**
     * Validate user input for creation of pricelist
     *
     * @param pricelistAPI
     * @param generalPricelistUniqueId
     * @throws HjalpmedelstjanstenValidationException
     */
    public void validateForCreate(GeneralPricelistPricelistAPI pricelistAPI, long generalPricelistUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForCreate(...)" );
        HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");

        validatePricelistAPI(pricelistAPI);

        // pricelist number is unique withing agreement
        validateNumberUnique(pricelistAPI.getNumber(), generalPricelistUniqueId, null, exception);

        // maximum one pricelist per date/per agreement
        validateValidFromUnique(pricelistAPI.getValidFrom(), generalPricelistUniqueId, null, exception);

        if( !exception.getValidationMessages().isEmpty() ) {
            throw exception;
        }

        // garbage collect help
        exception = null;
    }

    /**
     * Validate user input for update of pricelist.
     *
     * @param generalPricelistPricelistAPI user supplied values
     * @param generalPricelistPricelist pricelist to update
     * @param generalPricelist general pricelist of the pricelist
     * @throws HjalpmedelstjanstenValidationException
     */
    public void validateForUpdate(GeneralPricelistPricelistAPI generalPricelistPricelistAPI, GeneralPricelistPricelist generalPricelistPricelist, GeneralPricelist generalPricelist) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForUpdate(...)" );
        validatePricelistAPI(generalPricelistPricelistAPI);

        HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");

        // pricelist number is unique withing agreement
        validateNumberUnique(generalPricelistPricelistAPI.getNumber(), generalPricelist.getUniqueId(), generalPricelistPricelist.getUniqueId(), exception);

        // maximum one pricelist per date/per agreement
        validateValidFromUnique(generalPricelistPricelistAPI.getValidFrom(), generalPricelist.getUniqueId(), generalPricelistPricelist.getUniqueId(), exception);

        if( !exception.getValidationMessages().isEmpty() ) {
            throw exception;
        }

        // garbage collect help
        exception = null;
    }

    /**
     * Make sure number of pricelist is unique within given agreement
     *
     * @param number the number to check
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist to update or null if this is create
     * @param exception
     */
    private void validateNumberUnique(String number, long generalPricelistUniqueId, Long pricelistUniqueId, HjalpmedelstjanstenValidationException exception) {
        List<GeneralPricelistPricelist> pricelists = pricelistController.findByNumberAndGeneralPricelist(number, generalPricelistUniqueId);
        if( pricelists != null && !pricelists.isEmpty() ) {
            if( pricelistUniqueId == null ) {
                // this is an attempt to create pricelist and there already is one with the number
                exception.addValidationMessage("number", validationMessageService.getMessage("pricelist.number.alreadyExist"));
            } else {
                // this is an attempt to update pricelist, no OTHER pricelist can have the same number
                for( GeneralPricelistPricelist pricelist : pricelists ) {
                    if( !pricelist.getUniqueId().equals(pricelistUniqueId) ) {
                        exception.addValidationMessage("number", validationMessageService.getMessage("pricelist.number.alreadyExist"));
                    }
                }
            }
        }
    }

    /**
     * Check that validfrom is unique within given agreement
     *
     * @param validFrom the valid from to check
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist to update or null if this is create
     * @param exception
     */
    private void validateValidFromUnique(long validFrom, long generalPricelistUniqueId, Long pricelistUniqueId, HjalpmedelstjanstenValidationException exception) {
        List<GeneralPricelistPricelist> pricelists = pricelistController.findByValidFromAndGeneralPricelist(validFrom, generalPricelistUniqueId);
        if( pricelists != null && !pricelists.isEmpty() ) {
            if( pricelistUniqueId == null ) {
                // this is an attempt to create pricelist and there already is one with the same valid from
                exception.addValidationMessage("validFrom", validationMessageService.getMessage("pricelist.validFrom.alreadyExist"));
            } else {
                // this is an attempt to update pricelist, no OTHER pricelist can have the same valid from
                for( GeneralPricelistPricelist pricelist : pricelists ) {
                    if( !pricelist.getUniqueId().equals(pricelistUniqueId) ) {
                        exception.addValidationMessage("validFrom", validationMessageService.getMessage("pricelist.validFrom.alreadyExist"));
                    }
                }
            }
        }
    }

    /**
     * Bean validation of pricelist API
     *
     * @param pricelistAPI
     * @throws HjalpmedelstjanstenValidationException
     */
    private void validatePricelistAPI(GeneralPricelistPricelistAPI pricelistAPI) throws HjalpmedelstjanstenValidationException {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<GeneralPricelistPricelistAPI>> constraintViolations = validator.validate(pricelistAPI, Default.class);
        if( constraintViolations != null && !constraintViolations.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            for( ConstraintViolation constraintViolation : constraintViolations ) {
                exception.addValidationMessage(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage());
            }
            throw exception;
        }
    }

}
