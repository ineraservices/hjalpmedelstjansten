package se.inera.hjalpmedelstjansten.business.logging.view;

import java.io.IOException;
import jakarta.inject.Inject;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;

@WebFilter("/*")
public class MDCServletFilter implements Filter {

    @Inject
    private MDCHelper mdcHelper;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {
        try {
            if (request instanceof HttpServletRequest) {
                final var httpServletRequest = (HttpServletRequest) request;
                mdcHelper.decorateWithUser(httpServletRequest);
            }
            chain.doFilter(request, response);
        } finally {
            mdcHelper.clear();
        }
    }

    @Override
    public void destroy() {
        mdcHelper.clear();
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }
}
