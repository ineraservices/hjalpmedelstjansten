package se.inera.hjalpmedelstjansten.business.logging.view;

import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;

import jakarta.inject.Inject;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.AroundTimeout;
import jakarta.interceptor.InvocationContext;
import java.lang.reflect.Modifier;

/**
 * Interceptor for logging performance of methods
 *
 */
public class PerformanceLogInterceptor {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private AuthHandler authHandler;

    @AroundInvoke
    @AroundTimeout
    public Object logPerformance( InvocationContext invocationContext ) throws Exception {
        if( Modifier.isPublic(invocationContext.getMethod().getModifiers()) ) {
            // only log public methods. other REST-class methods should be private
            String className = invocationContext.getMethod().getDeclaringClass().getName();
            String methodName = invocationContext.getMethod().getName();
            String sessionId = authHandler.getSessionId();
            long start = System.currentTimeMillis();
            try {
                return invocationContext.proceed();
            } finally {
                long total = System.currentTimeMillis() - start;
                LOG.logPerformance(className, methodName, total, sessionId);
            }
        } else {
            return invocationContext.proceed();
        }
    }

}
