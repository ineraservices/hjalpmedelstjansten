package se.inera.hjalpmedelstjansten.business.importstatus.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.importstatus.controller.ImportStatusController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.model.api.ImportStatusAPI;
import se.inera.hjalpmedelstjansten.model.entity.ImportStatus;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Level;

/**
 * REST API for import status.
 *
 */
@Stateless
@Path("importStatus")
@Interceptors({ PerformanceLogInterceptor.class })
public class ImportStatusService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @Inject
    ImportStatusController importStatusController;


//    @EJB
//    AuthHandler authHandler;

    /**
     * Search importStatus by userName
     *
     * @param changedBy
     * @return a string of <code>importStatusAPI</code>
     */

    @GET
    @Path("{userId}")
    //@SecuredService(permissions = {"assortment:view"})
    public Response getImportsStatus(@Context HttpServletRequest httpServletRequest,
            @PathParam("userId") String userId ) {
        LOG.log(Level.FINEST, "getImportStatus( importStatus: {0} )", new Object[] {userId});

        List<ImportStatus> importStatus = importStatusController.getAllImportsStatus(userId);
        LOG.log(Level.FINEST, "importStatus: {0}", new Object[] {importStatus});

        if( importStatus == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(importStatus).build();
        }

    }


    /**
     * Update importStatus
     *
     * @param fileName the id of the importStatus to update
     * @param importStatusAPI
     */


    @PUT
    @Path("{fileName}")
    public Response updateImportStatus(@Context HttpServletRequest httpServletRequest,
                                      @PathParam("fileName") String fileName, ImportStatusAPI importStatusAPI) throws HjalpmedelstjanstenValidationException
    {
        LOG.log(Level.FINEST, "updateImportStatus( fileName: {0}, importStatus: {1} )", new Object[] {fileName, importStatusAPI.getReadStatus()});

//        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
//        if( !isSuperAdmin) {
//            return Response.status(Response.Status.FORBIDDEN).build();
//        }



         importStatusController.updateImportStatus(importStatusAPI);


        if( importStatusAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(importStatusAPI).build();
        }

    }

    @PUT
    @Path("{changedBy}")
    public Response createImportStatus(@Context HttpServletRequest httpServletRequest,
                                       @PathParam("changedBy") String fileName, ImportStatusAPI importStatusAPI) throws HjalpmedelstjanstenValidationException
    {
        LOG.log(Level.FINEST, "updateImportStatus( fileName: {0}, importStatus: {1} )", new Object[] {fileName, importStatusAPI.getReadStatus()});

//        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
//        if( !isSuperAdmin) {
//            return Response.status(Response.Status.FORBIDDEN).build();
//        }


        //importStatusAPI = importStatusController.createImportStatusForFile(importStatusAPI);
//
//

            return Response.status(Response.Status.NOT_FOUND).build();



    }


}
