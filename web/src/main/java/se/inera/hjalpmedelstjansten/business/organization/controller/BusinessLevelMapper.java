package se.inera.hjalpmedelstjansten.business.organization.controller;

import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;
import se.inera.hjalpmedelstjansten.model.entity.Organization;

import java.util.ArrayList;
import java.util.List;


public class BusinessLevelMapper {
    
    public static final List<BusinessLevelAPI> map(List<BusinessLevel> businessLevels, boolean includeAll) {
        if( businessLevels == null ) {
            return null;
        }
        List<BusinessLevelAPI> businessLevelAPIs = new ArrayList<>();
        for( BusinessLevel businessLevel : businessLevels ) {
            businessLevelAPIs.add(map(businessLevel, includeAll));
        }
        return businessLevelAPIs;
    }
    
    public static final BusinessLevelAPI map(BusinessLevel businessLevel, boolean includeAll) {
        if( businessLevel == null ) {
            return null;
        }
        BusinessLevelAPI businessLevelAPI = new BusinessLevelAPI();
        businessLevelAPI.setId(businessLevel.getUniqueId());
        businessLevelAPI.setName(businessLevel.getName());
        businessLevelAPI.setStatus(businessLevel.getStatus().toString());
        if( includeAll ) {
            businessLevelAPI.setOrganization(OrganizationMapper.map(businessLevel.getOrganization(), false));
        }
        return businessLevelAPI;
    }

    public static final BusinessLevel map(BusinessLevelAPI businessLevelAPI, Organization organization, BusinessLevel.Status status) {
        if( businessLevelAPI == null ) {
            return null;
        }
        BusinessLevel businessLevel = new BusinessLevel();
        businessLevel.setName(businessLevelAPI.getName());
        businessLevel.setOrganization(organization);
        businessLevel.setStatus(status);
        return businessLevel;
    }
    
}
