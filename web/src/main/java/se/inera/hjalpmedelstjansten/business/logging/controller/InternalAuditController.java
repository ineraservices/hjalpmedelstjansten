package se.inera.hjalpmedelstjansten.business.logging.controller;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.logging.Level;
import jakarta.ejb.Asynchronous;
import jakarta.ejb.Stateless;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.logging.view.MdcToolkit;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;

@Stateless
public class InternalAuditController {

    @Inject
    private HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;

    private void saveInternalAudit(InternalAudit internalAudit) {
        LOG.log( Level.FINEST, "saveInternalAudit( internalAudit: {0} )", new Object[] {internalAudit});
        em.persist(internalAudit);
    }

    public void removeInternalAuditsOlderThan(int internalAuditMaxAgeMonths) {
        LOG.log( Level.FINEST, "removeInternalAuditsOlderThan( internalAuditMaxAgeMonths: {0} )", new Object[] {internalAuditMaxAgeMonths});
        Date date = new Date();
        LocalDateTime localDateTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime = localDateTime.minusMonths(internalAuditMaxAgeMonths);
        date = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        LOG.log( Level.FINEST, "Remove logs older than: {0}", new Object[] {date});
        em.createQuery("DELETE FROM InternalAudit ia WHERE ia.auditTime < :date").
                setParameter("date", date).
                executeUpdate();
    }

    @Asynchronous
    public void handleInternalAuditEvent(@Observes InternalAuditEvent internalAuditEvent) {
        try {
            MdcToolkit.setMdc(internalAuditEvent.getMdc());
            LOG.log( Level.FINEST, "handleInternalAuditEvent( internalAuditEvent: {0} )", new Object[] {internalAuditEvent});

            if( internalAuditEvent.getActionType() != InternalAudit.ActionType.VIEW) {
                final var internalAudit = createInternalAudit(internalAuditEvent);
                saveInternalAudit(internalAudit);
            }

            LOG.logAudit(
                internalAuditEvent.getUserEngagementId() == null ? null: internalAuditEvent.getUserEngagementId().toString(),
                internalAuditEvent.getActionType() == null ? null: internalAuditEvent.getActionType().toString(),
                internalAuditEvent.getEntityType() == null ? null: internalAuditEvent.getEntityType().toString(),
                internalAuditEvent.getEntityId() == null ? null: internalAuditEvent.getEntityId().toString(),
                internalAuditEvent.getSessionId()
            );
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error handling internal audit event", e);
        } finally {
            MdcToolkit.clearMdc();
        }
    }

    private static InternalAudit createInternalAudit(InternalAuditEvent internalAuditEvent) {
        final var internalAudit = new InternalAudit();
        internalAudit.setAuditTime(internalAuditEvent.getAuditTime());
        internalAudit.setEntityType(internalAuditEvent.getEntityType());
        internalAudit.setActionType(internalAuditEvent.getActionType());
        internalAudit.setEntityId(internalAuditEvent.getEntityId());
        internalAudit.setUserEngagementId(internalAuditEvent.getUserEngagementId());
        internalAudit.setRequestIp(internalAuditEvent.getRequestIp());
        internalAudit.setSessionId(internalAuditEvent.getSessionId());
        return internalAudit;
    }
}
