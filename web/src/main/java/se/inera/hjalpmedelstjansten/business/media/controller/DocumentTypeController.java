package se.inera.hjalpmedelstjansten.business.media.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.api.cv.CVDocumentTypeAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVDocumentType;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Level;

@Stateless
public class DocumentTypeController {

    @Inject
    private HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;

    public List<CVDocumentTypeAPI> getAllDocumentTypes() {
        LOG.log(Level.FINEST, "getAllDocumentTypes()");
        return DocumentTypeMapper.map(findAll());
    }

    public List<CVDocumentType> findAll() {
        LOG.log(Level.FINEST, "findAll()");
        return em.createNamedQuery(CVDocumentType.FIND_ALL)
                .getResultList();
    }

    public CVDocumentType getDocumentType(long uniqueId) {
        LOG.log(Level.FINEST, "getDocumentType( uniqueId: {0} )");
        return em.find(CVDocumentType.class, uniqueId);
    }

    public CVDocumentType findByValue(String value) {
        LOG.log(Level.FINEST, "findByValue( value: {0} )", new Object[] {value});
        List<CVDocumentType> documentTypes = em.
                createNamedQuery(CVDocumentType.FIND_BY_VALUE).
                setParameter("value", value).
                getResultList();
        if( documentTypes == null || documentTypes.isEmpty() ) {
            return null;
        } else if( documentTypes.size() > 1 ) {
            LOG.log( Level.WARNING, "Multiple document types found for value: {1}", new Object[] {value});
            return null;
        } else {
            return documentTypes.get(0);
        }
    }

}
