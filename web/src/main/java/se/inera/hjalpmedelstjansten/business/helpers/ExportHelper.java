package se.inera.hjalpmedelstjansten.business.helpers;

import jakarta.ejb.Stateless;

@Stateless
public class ExportHelper {

    private boolean doesTheNumberContainDecimals(Number number) {

        if(number.doubleValue() % 1 == 0) {
            return false;
        } else return true;

    }

    public String parseNumberValueToStringWithCorrectDecimalFormatting(Number number) {

        //If number is not an actual double return long representation of the number
        if(!doesTheNumberContainDecimals(number)) {
            String notADecimalValue = String.valueOf(number.longValue());
            return notADecimalValue;

            // Else return String representation of the double value of number and add visual formatting rules.
        } else {
            return String.valueOf(number.doubleValue()).replaceAll("[.]", ",");
        }
    }

    public String replaceSpecialCharacters(String string){
        return string
            .replaceAll("[åä]", "a")
            .replaceAll("[ÅÄ]", "A")
            .replaceAll("ö", "o")
            .replaceAll("Ö", "O")
            .replaceAll("[^\\x00-\\x7F]", "")
            .replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", "")
            .replaceAll("\\p{C}", "");
    }
}
