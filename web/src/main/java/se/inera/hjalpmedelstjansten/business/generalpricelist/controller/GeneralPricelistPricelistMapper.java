package se.inera.hjalpmedelstjansten.business.generalpricelist.controller;

import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistAPI;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelist;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Class for mapping between API and Entity classes
 * 
 */
public class GeneralPricelistPricelistMapper {
    
    public static final List<GeneralPricelistPricelistAPI> map(List<GeneralPricelistPricelist> pricelists, boolean includeEverything, GeneralPricelistPricelist currentPricelist) {
        if( pricelists == null ) {
            return null;
        }
        List<GeneralPricelistPricelistAPI> pricelistAPIs = new ArrayList<>();
        for( GeneralPricelistPricelist pricelist : pricelists ) {
            pricelistAPIs.add(mapWithPricelist(pricelist, includeEverything, currentPricelist, null));
        }
        return pricelistAPIs;
    }
        
    public static final GeneralPricelistPricelistAPI mapWithPricelist(GeneralPricelistPricelist pricelist, boolean includeEverything, GeneralPricelistPricelist currentPricelist, Long numberOfPricelistRows) {
        if( pricelist == null ) {
            return null;
        }
        return mapWithStatus(pricelist, includeEverything, getPricelistStatus(pricelist, currentPricelist), null);
    }

    public static final GeneralPricelistPricelistAPI mapWithStatus(GeneralPricelistPricelist pricelist, boolean includeEverything, GeneralPricelistPricelist.Status status, Long numberOfPricelistRows) {
        if( pricelist == null ) {
            return null;
        }
        GeneralPricelistPricelistAPI pricelistAPI = new GeneralPricelistPricelistAPI();
        pricelistAPI.setId(pricelist.getUniqueId());
        pricelistAPI.setNumber(pricelist.getNumber());
        pricelistAPI.setValidFrom(pricelist.getValidFrom().getTime());
        pricelistAPI.setStatus(status == null ? null: status.toString());
        pricelistAPI.setHasPricelistRows(numberOfPricelistRows == null ? false: numberOfPricelistRows > 0);
        if( includeEverything ) {
            pricelistAPI.setGeneralPricelist(GeneralPricelistMapper.map(pricelist.getGeneralPricelist(), false));            
        }
        return pricelistAPI;
    }
    
    public static final GeneralPricelistPricelist map(GeneralPricelistPricelistAPI pricelistAPI) {
        if( pricelistAPI == null ) {
            return null;
        }
        GeneralPricelistPricelist pricelist = new GeneralPricelistPricelist();
        pricelist.setNumber(pricelistAPI.getNumber());
        pricelist.setValidFrom(new Date(pricelistAPI.getValidFrom()));
        return pricelist;
    }
    
    public static GeneralPricelistPricelist.Status getPricelistStatus(GeneralPricelistPricelist pricelist, GeneralPricelistPricelist currentPricelist) {
        Date now = new Date();
        GeneralPricelistPricelist.Status status;
        if( now.before(pricelist.getValidFrom()) ) {
            status = GeneralPricelistPricelist.Status.FUTURE;
        } else {
            if( currentPricelist == null || pricelist.getUniqueId().equals(currentPricelist.getUniqueId()) ) {
                status = GeneralPricelistPricelist.Status.CURRENT;
            } else {
                status = GeneralPricelistPricelist.Status.PAST;
            }
        }
        //HJAL-2406
        GeneralPricelist gp = pricelist.getGeneralPricelist();
        if (gp != null && gp.getValidTo()!= null && gp.getValidTo().before(new Date())) {
            status = GeneralPricelistPricelist.Status.PAST;
        }

        return status;
    }
    
}
