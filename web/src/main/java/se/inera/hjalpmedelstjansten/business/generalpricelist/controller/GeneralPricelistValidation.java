package se.inera.hjalpmedelstjansten.business.generalpricelist.controller;

import se.inera.hjalpmedelstjansten.business.DateUtils;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistAPI;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelist;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

/**
 * Validation methods for general pricelist
 *
 */
@Stateless
public class GeneralPricelistValidation {

    @Inject
    HjmtLogger LOG;

    @Inject
    ValidationMessageService validationMessageService;

    public void validateForCreate(GeneralPricelistAPI generalPricelistAPI, long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForCreate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForCreate(organizationUniqueId, generalPricelistAPI);
        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }
    }

    public void validateForUpdate(GeneralPricelistAPI generalPricelistAPI, GeneralPricelist generalPricelist, long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForUpdate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForUpdate(organizationUniqueId, generalPricelistAPI, generalPricelist);
        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }

    }

    /**
     * Generates a Set of <code>ErrorMessageAPI</code> in case the given generalPricelistAPI
     * does not validate entirely for create
     *
     * @param organizationUniqueId
     * @param generalPricelistAPI
     * @return
     */
    public Set<ErrorMessageAPI> tryForCreate(long organizationUniqueId, GeneralPricelistAPI generalPricelistAPI) {
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        validateGeneralPricelistAPI(generalPricelistAPI, errorMessageAPIs);
        if( !errorMessageAPIs.isEmpty() ) {
            return errorMessageAPIs;
        }
        if( !validFromDate(generalPricelistAPI.getValidFrom()) ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("validFrom", validationMessageService.getMessage("generalPricelist.validFrom.notFuture")));
        }
        return errorMessageAPIs;
    }

    /**
     * Generates a Set of <code>ErrorMessageAPI</code> in case the given generalPricelistAPI
     * does not validate entirely for update
     *
     * @param organizationUniqueId
     * @param generalPricelistAPI
     * @param generalPricelist
     * @return
     */
    public Set<ErrorMessageAPI> tryForUpdate(long organizationUniqueId, GeneralPricelistAPI generalPricelistAPI, GeneralPricelist generalPricelist) {
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        validateGeneralPricelistAPI(generalPricelistAPI, errorMessageAPIs);
        if( !errorMessageAPIs.isEmpty() ) {
            return errorMessageAPIs;
        }
        if( !DateUtils.isSameDay(generalPricelistAPI.getValidFrom(), generalPricelist.getValidFrom().getTime()) &&
                !validFromDate(generalPricelistAPI.getValidFrom()) ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("validFrom", validationMessageService.getMessage("generalPricelist.validFrom.notFuture")));
        }
        return errorMessageAPIs;
    }

    private void validateGeneralPricelistAPI(GeneralPricelistAPI generalPricelistAPI, Set<ErrorMessageAPI> errorMessageAPIs) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<GeneralPricelistAPI>> constraintViolations = validator.validate(generalPricelistAPI, Default.class);
        if( constraintViolations != null && !constraintViolations.isEmpty() ) {
            for( ConstraintViolation constraintViolation : constraintViolations ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage()));
            }
        }
    }

    private static boolean validFromDate(Long dateAsMillis) {
        // valid from must be today or a future date when creating or updating validFrom
        Instant nowInstant = Instant.now();
        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime todayStartOfDay = ZonedDateTime.ofInstant(nowInstant, zoneId).toLocalDate().atStartOfDay(zoneId);
        Instant validFromInstant = Instant.ofEpochMilli(dateAsMillis);
        ZonedDateTime validFrom = ZonedDateTime.ofInstant(validFromInstant, zoneId);
        if( validFrom.isBefore(todayStartOfDay) ) {
            return false;
        }
        return true;
    }

}
