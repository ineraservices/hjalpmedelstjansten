package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.LoggerService;
import se.inera.hjalpmedelstjansten.model.api.ExternalCategoryGroupingAPI;
import se.inera.hjalpmedelstjansten.model.entity.ExternalCategoryGrouping;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Class for mapping between API and Entity classes
 *
 */

public class ExternalCategoryMapper {
    private static final HjmtLogger LOG = LoggerService.getLogger(CategoryMapper.class.getName());

    public static final List<ExternalCategoryGroupingAPI> map(List<ExternalCategoryGrouping> externalCategoryGroupings) {
        LOG.log( Level.FINEST, "map(...)" );
        if( externalCategoryGroupings == null || externalCategoryGroupings.isEmpty() ) {
            return null;
        }
        List<ExternalCategoryGroupingAPI> externalCategoryGroupingAPIs = new ArrayList<>();
        for( ExternalCategoryGrouping externalCategoryGrouping : externalCategoryGroupings ) {
            externalCategoryGroupingAPIs.add(map(externalCategoryGrouping));
        }
        return externalCategoryGroupingAPIs;
    }

    public static final ExternalCategoryGroupingAPI map(ExternalCategoryGrouping externalCategoryGrouping) {
        LOG.log( Level.FINEST, "map(...)" );
        if( externalCategoryGrouping == null ) {
            return null;
        }
        ExternalCategoryGroupingAPI externalCategoryGroupingAPI = new ExternalCategoryGroupingAPI();
        externalCategoryGroupingAPI.setId(externalCategoryGrouping.getUniqueId());
        externalCategoryGroupingAPI.setName(externalCategoryGrouping.getName());
        externalCategoryGroupingAPI.setParent(externalCategoryGrouping.getParent() != null ? externalCategoryGrouping.getParent().getUniqueId() : null);
        externalCategoryGroupingAPI.setDescription(externalCategoryGrouping.getDescription());
        externalCategoryGroupingAPI.setCategory(externalCategoryGrouping.getCategory() != null ? externalCategoryGrouping.getCategory().getUniqueId() : null);
        externalCategoryGroupingAPI.setDisplayOrder(externalCategoryGrouping.getDisplayOrder());
        return externalCategoryGroupingAPI;
    }

    public static final List<ExternalCategoryGroupingAPI> mapExc(List<ExternalCategoryGrouping> externalCategoryGroupings) {
        LOG.log( Level.FINEST, "map(...)" );
        if( externalCategoryGroupings == null || externalCategoryGroupings.isEmpty() ) {
            return null;
        }
        List<ExternalCategoryGroupingAPI> externalCategoryGroupingAPIs = new ArrayList<>();
        for( ExternalCategoryGrouping externalCategoryGrouping : externalCategoryGroupings ) {
            externalCategoryGroupingAPIs.add(mapExc(externalCategoryGrouping));
        }
        return externalCategoryGroupingAPIs;
    }
    public static final ExternalCategoryGroupingAPI mapExc(ExternalCategoryGrouping externalCategoryGrouping) {
        LOG.log( Level.FINEST, "map(...)" );
        if( externalCategoryGrouping == null ) {
            return null;
        }
        ExternalCategoryGroupingAPI externalCategoryGroupingAPI = new ExternalCategoryGroupingAPI();
        externalCategoryGroupingAPI.setId(externalCategoryGrouping.getUniqueId());
        externalCategoryGroupingAPI.setDescription(externalCategoryGrouping.getDescription());
        externalCategoryGroupingAPI.setDisplayOrder(externalCategoryGrouping.getDisplayOrder());
        externalCategoryGroupingAPI.setName(externalCategoryGrouping.getName());
        externalCategoryGroupingAPI.setCategory(externalCategoryGrouping.getCategory() != null ? externalCategoryGrouping.getCategory().getUniqueId() : null);
        externalCategoryGroupingAPI.setParent(externalCategoryGrouping.getParent() != null ? externalCategoryGrouping.getParent().getUniqueId() : null);
        return externalCategoryGroupingAPI;
    }

    public static final ExternalCategoryGrouping map(ExternalCategoryGroupingAPI externalCategoryGroupingApi) {
        LOG.log( Level.FINEST, "map(...)" );
        if( externalCategoryGroupingApi == null ) {
            return null;
        }
        ExternalCategoryGrouping externalCategoryGrouping = new ExternalCategoryGrouping();
        externalCategoryGrouping.setUniqueId(externalCategoryGroupingApi.getId());
        externalCategoryGrouping.setName(externalCategoryGroupingApi.getName());
//        externalCategoryGrouping.setParent(externalCategoryGroupingApi.getParent());
        externalCategoryGrouping.setDescription(externalCategoryGroupingApi.getDescription());
//        externalCategoryGrouping.setCategory(externalCategoryGroupingApi.getCategoryId());
        externalCategoryGrouping.setDisplayOrder(externalCategoryGroupingApi.getDisplayOrder());

        return externalCategoryGrouping;
    }
}
