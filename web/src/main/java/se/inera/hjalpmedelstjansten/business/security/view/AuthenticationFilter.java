package se.inera.hjalpmedelstjansten.business.security.view;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.LoggerService;

import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.core.Response;
import java.io.IOException;
import java.util.logging.Level;

/**
 * If a REST API requires some form of permission, the request will pass
 * thru this and be validated. If the user lacks the correct permission, HTTP
 * response code 403 is returned.
 *
 * @see se.inera.hjalpmedelstjansten.business.security.view.AuthenticationFeature
 * @see se.inera.hjalpmedelstjansten.business.security.view.SecuredService
 */
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

    private static final HjmtLogger LOG = LoggerService.getLogger(AuthenticationFilter.class.getName());

    private final String[] requiresPermissions;

    public AuthenticationFilter(String[] requiresPermissions) {
        this.requiresPermissions = requiresPermissions;
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        Subject subject = SecurityUtils.getSubject();
        if( subject == null || !subject.isAuthenticated() ) {
            LOG.log( Level.FINEST, "User is not authenticated." );
            requestContext.abortWith(
                    Response.status(Response.Status.UNAUTHORIZED)
                    .build()
                );
        } else {
            boolean permissionFound = false;
            for( String requiresPermission: requiresPermissions ) {
                if( subject.isPermitted(requiresPermission) ) {
                    permissionFound = true;
                    break;
                }
            }
            if( !permissionFound ) {
                LOG.log( Level.FINEST, "User is not permitted." );
                requestContext.abortWith(
                    Response.status(Response.Status.FORBIDDEN)
                    .build()
                );
            }
        }
    }

}
