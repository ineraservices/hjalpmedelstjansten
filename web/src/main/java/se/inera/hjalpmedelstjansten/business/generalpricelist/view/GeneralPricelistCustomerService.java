package se.inera.hjalpmedelstjansten.business.generalpricelist.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import java.util.logging.Level;

/**
 * REST API for general pricelists from a customer point of view. Customer can
 * search all active general pricelists from all suppliers.
 *
 */
@Stateless
@Path("generalpricelists")
@Interceptors({ PerformanceLogInterceptor.class })
public class GeneralPricelistCustomerService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    GeneralPricelistController generalPricelistController;

    /**
     * Search all general pricelists. For customers who wants to include general pricelists
     * in the export settings
     *
     * @param userSearchQuery user can search for gp on certain values
     * @param offset
     * @return a list of <code>GeneralPricelistAPI</code>
     */
    @GET
    @SecuredService(permissions = {"generalpricelist:view_all"})
    public Response searchAllGeneralPricelist(
            @DefaultValue(value = "") @QueryParam("query") String userSearchQuery,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @DefaultValue(value = "") @QueryParam("sortType") String sortType,
            @DefaultValue("") @QueryParam("sortOrder") String sortOrder) {
        LOG.log(Level.FINEST, "searchAllGeneralPricelist()");
        SearchDTO searchDTO = generalPricelistController.searchAllGeneralPricelistAPIs(userSearchQuery, 25, offset, sortType, sortOrder);
        return Response.ok(searchDTO.getItems()).
                header("X-Total-Count", searchDTO.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }
    /**
     * Search all general pricelists. For customers who wants to include general pricelists
     * in the export settings
     *
     * @param organizationUniqueId user can search for gp on certain values
     * @return a list of <code>GeneralPricelistAPI</code>
     */
    @GET
    @Path("organization/{organizationUniqueId}/exportsettings/{exportSettingsUniqueId}")
    @SecuredService(permissions = {"generalpricelist:view_all","exportsettings:view_own"})
    public Response searchAllUnselectedGeneralPricelist(
            @PathParam("exportSettingsUniqueId") long exportSettingsUniqueId,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String userSearchQuery,
            @DefaultValue(value = "0") @QueryParam("offset") int offset)
        {

        LOG.log(Level.FINEST, "searchAllUnselectedGeneralPricelist()");

        SearchDTO sdto = generalPricelistController.searchAllUnselectedPricelistAPIs(exportSettingsUniqueId, userSearchQuery, 25, offset);
        return Response.ok(sdto.getItems()).
                header("X-Total-Count", sdto.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }


}
