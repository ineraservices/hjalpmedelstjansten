package se.inera.hjalpmedelstjansten.business.generalpricelist.controller;

import se.inera.hjalpmedelstjansten.business.BaseController;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.organization.controller.BusinessLevelController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.Product;

import jakarta.ejb.Stateless;
import jakarta.enterprise.event.Event;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

/**
 * Class for handling business logic of Pricelists on General Pricelists. This
 * includes talking to the database.
 *
 */
@Stateless
public class GeneralPricelistPricelistController extends BaseController {

    @Inject
    HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    GeneralPricelistPricelistValidation pricelistValidation;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    OrganizationController organizationController;

    @Inject
    BusinessLevelController businessLevelController;

    @Inject
    UserController userController;

    @Inject
    GeneralPricelistController generalPricelistController;

    @Inject
    ElasticSearchController elasticSearchController;

    @Inject
    Event<InternalAuditEvent> internalAuditEvent;

    @Inject
    String defaultPricelistNumber;

    /**
     * Get the List of pricelists for the general pricelist
     *
     * @param organizationUniqueId unique id of the organization
     * @param userAPI user information
     * @return the corresponding List of <code>GeneralPricelistPricelistAPI</code>
     */
    public List<GeneralPricelistPricelistAPI> getPricelistAPIsOnGeneralPricelist(long organizationUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getPricelistAPIsOnGeneralPricelist( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        GeneralPricelist generalPricelist = generalPricelistController.getGeneralPricelist(organizationUniqueId);
        List<GeneralPricelistPricelist> pricelists = getPricelistsOnGeneralPricelist(organizationUniqueId, generalPricelist.getUniqueId());
        Organization organization = organizationController.getOrganizationFromLoggedInUser(userAPI);
        if( organization.getOrganizationType() == Organization.OrganizationType.CUSTOMER ) {
            pricelists = getValidPricelistsOnGeneralPricelist(organizationUniqueId, generalPricelist.getUniqueId());
        }
        if( pricelists != null ) {
            GeneralPricelistPricelist currentPricelist = getCurrentPricelist( organizationUniqueId );
            List<GeneralPricelistPricelistAPI> generalPricelistPricelistAPIs = GeneralPricelistPricelistMapper.map( pricelists, false, currentPricelist);
            if( generalPricelistPricelistAPIs != null ) {
                for( GeneralPricelistPricelistAPI generalPricelistPricelistAPI : generalPricelistPricelistAPIs ) {
                    long numberOfRowsOnPricelist = getNumberOfRowsOnPricelist(generalPricelistPricelistAPI.getId());
                    generalPricelistPricelistAPI.setHasPricelistRows(numberOfRowsOnPricelist > 0);
                }
            }
            return generalPricelistPricelistAPIs;
        }
        return null;
    }

    /**
     * Get all pricelists on the given general pricelist.
     *
     * @param organizationUniqueId unique id of the organization
     * @param generalPricelistUniqueId unique id of the general pricelist
     * @return the corresponding List of <code>GeneralPricelistPricelist</code>
     */
    private List<GeneralPricelistPricelist> getPricelistsOnGeneralPricelist(long organizationUniqueId, long generalPricelistUniqueId) {
        LOG.log(Level.FINEST, "getPricelistsOnGeneralPricelist( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        List<GeneralPricelistPricelist> pricelists = em.createNamedQuery(GeneralPricelistPricelist.GET_ALL_BY_GENERAL_PRICELIST).
                setParameter("generalPricelistUniqueId", generalPricelistUniqueId).
                getResultList();
        if( pricelists != null ) {
            return pricelists;
        }
        return null;
    }

    /**
     * Get all valid pricelists on the given general pricelist.
     *
     * @param organizationUniqueId unique id of the organization
     * @param generalPricelistUniqueId unique id of the general pricelist
     * @return the corresponding List of <code>GeneralPricelistPricelist</code>
     */
    private List<GeneralPricelistPricelist> getValidPricelistsOnGeneralPricelist(long organizationUniqueId, long generalPricelistUniqueId) {
        LOG.log(Level.FINEST, "getValidPricelistsOnGeneralPricelist( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        List<GeneralPricelistPricelist> pricelists = em.createNamedQuery(GeneralPricelistPricelist.FIND_BY_PASSED_VALID_FROM_AND_GENERAL_PRICELIST).
                setParameter("generalPricelistUniqueId", generalPricelistUniqueId).
                setParameter("validFrom", new Date()).
                getResultList();
        if( pricelists != null ) {
            return pricelists;
        }
        return null;
    }


    /**
     * Get the <code>PricelistAPI</code> for the given unique id on the
     * specified organization
     *
     * @param organizationUniqueId unique id of the organization
     * @param pricelistUniqueId unique id of the pricelist to get
     * @param userAPI user information
     * @param sessionId
     * @return the corresponding <code>GeneralPricelistPricelistAPI</code>
     */
    public GeneralPricelistPricelistAPI getPricelistAPI(long organizationUniqueId, long pricelistUniqueId, UserAPI userAPI, String sessionId, String requestIp) {
        LOG.log(Level.FINEST, "getPricelistAPI( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[] {organizationUniqueId, pricelistUniqueId});
        GeneralPricelistPricelist pricelist = getPricelist(organizationUniqueId, pricelistUniqueId, userAPI);
        if( pricelist != null ) {
            GeneralPricelistPricelist currentPricelist = getCurrentPricelist( organizationUniqueId );
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.GENERAL_PRICELIST_PRICELIST, InternalAudit.ActionType.VIEW, userAPI.getId(), sessionId, pricelistUniqueId, requestIp));
            long numberOfRowsOnPricelist = getNumberOfRowsOnPricelist(pricelistUniqueId);
            return GeneralPricelistPricelistMapper.mapWithPricelist(pricelist, true, currentPricelist, numberOfRowsOnPricelist);
        }
        return null;
    }

    /**
     * Get the <code>GeneralPricelistPricelist</code> for the given unique id on the
     * specified organization
     *
     * @param organizationUniqueId unique id of the organization
     * @param pricelistUniqueId unique id of the pricelist to get
     * @param userAPI user information
     * @return the corresponding <code>GeneralPricelistPricelist</code>
     */
    public GeneralPricelistPricelist getPricelist(long organizationUniqueId, long pricelistUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getPricelist( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[] {organizationUniqueId, pricelistUniqueId});
        GeneralPricelist generalPricelist = generalPricelistController.getGeneralPricelist(organizationUniqueId);
        return getPricelist(organizationUniqueId, generalPricelist.getUniqueId(), pricelistUniqueId, userAPI);
    }

    /**
     * Get the <code>Pricelist</code> with the given id on the specified organization.
     *
     * @param organizationUniqueId unique id of the organization
     * @param generalPricelistUniqueId
     * @param pricelistUniqueId unique id of the pricelist
     * @param userAPI user information
     * @return the corresponding <code>GeneralPricelistPricelist</code>
     */
    public GeneralPricelistPricelist getPricelist(long organizationUniqueId, long generalPricelistUniqueId, long pricelistUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getPricelist( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[] {organizationUniqueId, pricelistUniqueId});
        GeneralPricelistPricelist pricelist = em.find(GeneralPricelistPricelist.class, pricelistUniqueId);
        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist: {1} that does not exist. Returning 404.", new Object[] {userAPI.getId(), pricelistUniqueId});
            return null;
        }
        if( !pricelist.getGeneralPricelist().getUniqueId().equals(generalPricelistUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist: {1} not on given general pricelist: {2}. Returning 404.", new Object[] {userAPI.getId(), pricelistUniqueId, generalPricelistUniqueId});
            return null;
        }
        return pricelist;
    }

    /**
     * When a general pricelist is created, one pricelist should be created by default.
     *
     * @param generalPricelist the created general pricelist
     * @param userAPI
     * @param sessionId
     * @return the created <code>GeneralPricelistPricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public GeneralPricelistPricelistAPI createFirstPricelistForGeneralPricelist( GeneralPricelist generalPricelist, UserAPI userAPI, String sessionId, String requestIp ) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createFirstPricelistForGeneralPricelist( generalPricelist->uniqueId: {0} )", new Object[] {generalPricelist.getUniqueId()});
        GeneralPricelistPricelistAPI pricelistAPI = new GeneralPricelistPricelistAPI();
        pricelistAPI.setNumber(defaultPricelistNumber);
        pricelistAPI.setValidFrom(generalPricelist.getValidFrom().getTime());
        return createPricelist(generalPricelist, pricelistAPI, userAPI, sessionId, requestIp);
    }

    /**
     * Create a pricelist based on the supplied data
     *
     * @param organizationUniqueId unique id of the organization
     * @param pricelistAPI user supplied data
     * @param userAPI logged in user's session information
     * @param sessionId
     * @return the created <code>GeneralPricelistPricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public GeneralPricelistPricelistAPI createPricelist( long organizationUniqueId, GeneralPricelistPricelistAPI pricelistAPI, UserAPI userAPI, String sessionId, String requestIp ) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createPricelist( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        GeneralPricelist generalPricelist = generalPricelistController.getGeneralPricelist(organizationUniqueId);
        return createPricelist(generalPricelist, pricelistAPI, userAPI, sessionId, requestIp);
    }

    /**
     * Create a pricelist based on the supplied data
     *
     * @param generalPricelist the general pricelist of the pricelist
     * @param pricelistAPI user supplied data
     * @return the created <code>GeneralPricelistPricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    private GeneralPricelistPricelistAPI createPricelist( GeneralPricelist generalPricelist, GeneralPricelistPricelistAPI pricelistAPI, UserAPI userAPI, String sessionId, String requestIp ) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createPricelist( generalPricelist->uniqueId: {0} )", new Object[] {generalPricelist.getUniqueId()});
        pricelistValidation.validateForCreate(pricelistAPI, generalPricelist.getUniqueId());
        GeneralPricelistPricelist generalPricelistPricelist = GeneralPricelistPricelistMapper.map(pricelistAPI);
        generalPricelistPricelist.setGeneralPricelist(generalPricelist);
        em.persist(generalPricelistPricelist);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.GENERAL_PRICELIST_PRICELIST, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, generalPricelistPricelist.getUniqueId(), requestIp));
        GeneralPricelistPricelist currentPricelist = getCurrentPricelist( generalPricelist.getUniqueId() );
        // 0 since new pricelist has no rows
        return GeneralPricelistPricelistMapper.mapWithPricelist(generalPricelistPricelist, true, currentPricelist, 0l);
    }

    /**
     * Create a pricelist based on the supplied data
     *
     * @param organizationUniqueId unique id of the organization
     * @param pricelistUniqueId unique id of the pricelist
     * @param generalPricelistPricelistAPI
     * @param userAPI logged in user's session information
     * @param sessionId
     * @return the created <code>GeneralPricelistPricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public GeneralPricelistPricelistAPI updatePricelist(long organizationUniqueId, long pricelistUniqueId, GeneralPricelistPricelistAPI generalPricelistPricelistAPI, UserAPI userAPI, String sessionId) throws HjalpmedelstjanstenValidationException {
        GeneralPricelistPricelist generalPricelistPricelist = getPricelist(organizationUniqueId, pricelistUniqueId, userAPI);
        if( generalPricelistPricelist == null ) {
            return null;
        }
        GeneralPricelist generalPricelist = generalPricelistController.getGeneralPricelist(organizationUniqueId);
        GeneralPricelistPricelist currentPricelist = getCurrentPricelist( generalPricelist.getUniqueId() );
        GeneralPricelistPricelist.Status status = GeneralPricelistPricelistMapper.getPricelistStatus(generalPricelistPricelist, currentPricelist);
        long numberOfRowsOnPricelist = getNumberOfRowsOnPricelist(pricelistUniqueId);

        // future pricelists can always be modified
        if( status != GeneralPricelistPricelist.Status.FUTURE ) {
            // no rows may exist on pricelist for update to be valid
            if( numberOfRowsOnPricelist > 0 ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to update generalpricelist pricelist: {1} but rows already exist in pricelist", new Object[] {userAPI.getId(), pricelistUniqueId});
                throw validationMessageService.generateValidationException("rows", "generalpricelist.update.rowsExist");
            }
        }

        pricelistValidation.validateForUpdate(generalPricelistPricelistAPI, generalPricelistPricelist, generalPricelist);
        generalPricelistPricelist.setNumber(generalPricelistPricelistAPI.getNumber());
        generalPricelistPricelist.setValidFrom(new Date(generalPricelistPricelistAPI.getValidFrom()));

        return GeneralPricelistPricelistMapper.mapWithPricelist(generalPricelistPricelist, true, currentPricelist, numberOfRowsOnPricelist);
    }

    /**
     * Get the total number of rows in pricelist
     *
     * @param pricelistUniqueId the unique id of the pricelist
     * @return the number of rows in the pricelist
     */
    private long getNumberOfRowsOnPricelist(long pricelistUniqueId) {
        Long count = (Long) em.createNamedQuery(GeneralPricelistPricelistRow.COUNT_BY_PRICELIST).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                getSingleResult();
        return count == null ? 0: count;
    }

    /**
     * Find pricelists on a specific general pricelist with given validFrom
     *
     * @param validFrom the valid from date to search for
     * @param generalPricelistUniqueId unique id of the general pricelist
     * @return a list of general pricelist pricelists that match the query params
     */
    public List<GeneralPricelistPricelist> findByValidFromAndGeneralPricelist(long validFrom, long generalPricelistUniqueId) {
        LOG.log(Level.FINEST, "findByValidFromAndAgreement( generalPricelistUniqueId: {0} )", new Object[] {generalPricelistUniqueId} );
        Date date = new Date(validFrom);
        return em.createNamedQuery(GeneralPricelistPricelist.FIND_BY_VALID_FROM_AND_GENERAL_PRICELIST).
                setParameter("generalPricelistUniqueId", generalPricelistUniqueId).
                setParameter("validFrom", date).
                getResultList();
    }

    /**
     * Find pricelists on a specific general pricelist with given number
     *
     * @param number the number to search for
     * @param generalPricelistUniqueId unique id of the general pricelist
     * @return a list of general pricelist pricelists that match the query params
     */
    public List<GeneralPricelistPricelist> findByNumberAndGeneralPricelist(String number, long generalPricelistUniqueId) {
        LOG.log(Level.FINEST, "findByNumberAndAgreement( generalPricelistUniqueId: {0} )", new Object[] {generalPricelistUniqueId} );
        return em.createNamedQuery(GeneralPricelistPricelist.FIND_BY_NUMBER_AND_GENERAL_PRICELIST).
                setParameter("generalPricelistUniqueId", generalPricelistUniqueId).
                setParameter("number", number).
                getResultList();
    }

    /**
     * Return the current pricelist on a general pricelist (if any). There may be
     * just FUTURE ones
     *
     * @param organizationUniqueId unique id of the organization
     * @return the current <code>GeneralPricelistPricelist</code> or null if none exist
     */
    public GeneralPricelistPricelist getCurrentPricelist(long organizationUniqueId) {
        LOG.log(Level.FINEST, "getCurrentPricelist( organizationUniqueId: {0} )", new Object[] {organizationUniqueId} );
        List<GeneralPricelistPricelist> pricelists = em.createNamedQuery(GeneralPricelistPricelist.FIND_BY_PASSED_VALID_FROM_AND_ORGANIZATION).
                setParameter("organizationUniqueId", organizationUniqueId).
                setParameter("validFrom", new Date()).
                getResultList();
        if( pricelists != null && !pricelists.isEmpty() ) {
            // query is ORDER BY validFrom DESC so first is most recent
            return pricelists.get(0);
        }
        return null;
    }

    /**
     * Search available articles for pricelist, meaning all active articles on organisation
     * that is not already in the pricelist
     *
     * @param organizationUniqueId
     * @param pricelistUniqueId
     * @param query
     * @param statuses
     * @param articleTypes
     * @param offset
     * @param limit
     * @param userAPI
     * @return
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
     */
    public SearchDTO searchArticlesForGeneralPricelistPricelist(long organizationUniqueId,
            long pricelistUniqueId,
            String query,
            List<Product.Status> statuses,
            List<Article.Type> articleTypes,
            int offset,
            int limit,
            UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "searchArticlesForGeneralPricelistPricelist( organizationUniqueId: {0}, statuses: {1}, articleTypes: {2}, offset: {3}, limit: {4} )", new Object[] {organizationUniqueId, statuses, articleTypes, offset, limit});
        GeneralPricelistPricelist pricelist = getPricelist(organizationUniqueId, pricelistUniqueId, userAPI);
        if( pricelist != null ) {
            List<Long> articlesAlreadyInPricelist = em.createQuery("SELECT pricelistRow.article.uniqueId FROM GeneralPricelistPricelistRow pricelistRow WHERE pricelistRow.pricelist.uniqueId = :generalPricelistPricelistId").
                    setParameter("generalPricelistPricelistId", pricelistUniqueId)
                    .getResultList();

            Product.Status status = null;
            if (statuses != null && statuses.size() == 1) {
                status = statuses.get(0);
            }
            return elasticSearchController.searchOrganization(query,
                    organizationUniqueId,
                    false,
                    true,
                    "",
                    "",
                    articleTypes,
                    articlesAlreadyInPricelist,
                    status,
                    25,
                    offset,
                    false);
        }
        return null;
    }

}
