package se.inera.hjalpmedelstjansten.business.product.controller;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Level;
import jakarta.ejb.Stateless;
import jakarta.enterprise.event.Event;
import jakarta.inject.Inject;
import jakarta.mail.MessagingException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceContext;
import lombok.NoArgsConstructor;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementMapper;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistController;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistMapper;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowController;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowMapper;
import se.inera.hjalpmedelstjansten.business.assortment.controller.AssortmentController;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistMapper;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistController;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistMapper;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistRowController;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistRowMapper;
import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.media.controller.MediaController;
import se.inera.hjalpmedelstjansten.business.organization.controller.ElectronicAddressMapper;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.service.SendMailRegardingDiscontinuedArticlesOnAssortment;
import se.inera.hjalpmedelstjansten.business.property.view.PropertyLoader;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.EmailController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.BooleanAPI;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.HJMTJRow;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.ResourceSpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVOrderUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPackageUnitAPI;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelist;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.ArticleChanged;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificPropertyListValue;
import se.inera.hjalpmedelstjansten.model.entity.ChangeChecker;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValue;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueDecimal;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueInterval;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueTextField;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueValueListMultiple;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueValueListSingle;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEDirective;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEStandard;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

/**
 * Class for handling business logic of Articles. This includes talking to the
 * database.
 *
 */
@Stateless
@NoArgsConstructor
public class ArticleController {

    @Inject
    HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    ArticleValidation articleValidation;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    ProductController productController;

    @Inject
    CategoryController categoryController;

    @Inject
    CeController ceController;

    @Inject
    EmailController emailController;

    @Inject
    OrganizationController organizationController;

    @Inject
    AgreementPricelistRowController agreementPricelistRowController;

    @Inject
    AgreementPricelistController agreementPricelistController;

    @Inject
    GeneralPricelistPricelistController generalPricelistPricelistController;

    @Inject
    GeneralPricelistPricelistRowController generalPricelistPricelistRowController;

    @Inject
    CVPreventiveMaintenanceController cVPreventiveMaintenanceController;

    @Inject
    OrderUnitController orderUnitController;

    @Inject
    PackageUnitController packageUnitController;

    @Inject
    UserController userController;

    @Inject
    MediaController mediaController;

    @Inject
    AssortmentController assortmentController;

    @Inject
    ElasticSearchController elasticSearchController;

    @Inject
    Event<InternalAuditEvent> internalAuditEvent;

    @Inject
    SendMailRegardingDiscontinuedArticlesOnAssortment sendMailRegardingDiscontinuedArticlesOnAssortment;

    private String rowsSentForApprovalOnArticleChangedBasedOnMailSubject;
    private String rowsSentForApprovalOnArticleChangedBasedOnMailBody;
    private String articlesChangedMailSubject;
    private String articlesChangedMailBody;

    @Inject
    public ArticleController(PropertyLoader propertyLoader) {

        rowsSentForApprovalOnArticleChangedBasedOnMailSubject = propertyLoader.getMessage("rowsSentForApprovalOnArticleChangedBasedOnMailSubject");
        rowsSentForApprovalOnArticleChangedBasedOnMailBody = propertyLoader.getMessage("rowsSentForApprovalOnArticleChangedBasedOnMailBody");
        articlesChangedMailSubject = propertyLoader.getMessage("articlesChangedMailSubject");
        articlesChangedMailBody = propertyLoader.getMessage("articlesChangedMailBody");

    }

    public List<ArticleAPI> findByCategory(long organizationUniqueId, long categoryUniqueId) {
        return findByCategory(organizationUniqueId, categoryUniqueId, null);
    }

    public List<ArticleAPI> findByCategory(long organizationUniqueId, long categoryUniqueId, UserAPI userAPI) {
        List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(categoryUniqueId);
        return ArticleMapper.map(em.createNamedQuery(Article.FIND_BY_ORGANIZATION_AND_CATEGORY).
                setParameter("organizationUniqueId", organizationUniqueId).
                setParameter("categoryUniqueId", categoryUniqueId).
                getResultList(), true, categorySpecificPropertys, userAPI);
    }

    /**
     * Create an article on the <code>Organization</code> with the given id. An
     * article must be based on a product or fit to one. We only check this when
     * creating the Article. Adding fits to product or fits to article is done
     * using separate APIs.
     *
     * @param organizationUniqueId unique id of the Organization
     * @param articleAPI user supplied values
     * @param userAPI logged in users sessions information
     * @param sessionId
     * @param requestIp
     * @param doIndex
     * @return the mapped <code>ArticleAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    public ArticleAPI createArticle(long organizationUniqueId,
            ArticleAPI articleAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp,
            boolean doIndex) throws HjalpmedelstjanstenValidationException {
        Category category;
        Product basedOnProduct = null;
        if( articleAPI.getBasedOnProduct() == null ) {
            // add primary category, if article is based on product, category is inherited
            if( articleAPI.getCategory() == null || articleAPI.getCategory().getId() == null ) {
                LOG.log(Level.FINEST, "no category sent by frontend and no basedon selected");
                if (articleAPI.getCategory().getArticleType().name().equals("H") || articleAPI.getCategory().getArticleType().name().equals("T"))
                    throw validationMessageService.generateValidationException("product", "article.notbasedon.category.notsent.HT");
                else
                    throw validationMessageService.generateValidationException("category", "article.notbasedon.category.notsent");
            }
            List<Category> categories = categoryController.getChildlessById(articleAPI.getCategory().getId());
            if( categories == null || categories.isEmpty() ) {
                LOG.log(Level.FINEST, "category {0} does not exist or is not a leaf node", new Object[] {articleAPI.getCategory().getId()});
                throw validationMessageService.generateValidationException("category", "article.category.notExists");
            }
            category = categories.get(0);
        } else {
            if( articleAPI.getBasedOnProduct().getId() != null ) {
                basedOnProduct = productController.getProduct(organizationUniqueId, articleAPI.getBasedOnProduct().getId(), userAPI);
            } else {
                basedOnProduct = productController.findByProductNumberAndOrganization( articleAPI.getBasedOnProduct().getProductNumber(), organizationUniqueId);
            }
            if( basedOnProduct == null ) {
                LOG.log(Level.FINEST, "product {0} does not exist", new Object[] {articleAPI.getBasedOnProduct().getId()});
                throw validationMessageService.generateValidationException("basedOnProduct", "article.product.basedOn.notExists");
            }
            category = basedOnProduct.getCategory();
        }
        List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(category.getUniqueId());
        Map<Long, CVOrderUnit> allOrderUnits = orderUnitController.findAllAsIdMap();
        return createArticle(organizationUniqueId, articleAPI, category, basedOnProduct, categorySpecificPropertys, allOrderUnits, userAPI, sessionId, requestIp, doIndex, true);
    }

    public ArticleAPI createArticle(long organizationUniqueId,
            ArticleAPI articleAPI,
            Category category,
            Product basedOnProduct,
            List<CategorySpecificProperty> categorySpecificPropertys,
            Map<Long, CVOrderUnit> allOrderUnits,
            UserAPI userAPI,
            String sessionId,
            String requestIp,
            boolean doIndex,
            boolean validationRequired) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createArticle( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        Article article = createArticle(organizationUniqueId, articleAPI, category, basedOnProduct, categorySpecificPropertys, allOrderUnits, userAPI, sessionId, requestIp, validationRequired);
        articleAPI = ArticleMapper.map(article, true, categorySpecificPropertys);
        articleAPI.setNumberEditable(isArticleNumberEditable(article));
        if( doIndex ) {
            elasticSearchController.indexArticle(articleAPI);
        }
        return articleAPI;
    }

    public Article createArticle(long organizationUniqueId,
            ArticleAPI articleAPI,
            List<CategorySpecificProperty> categorySpecificPropertys,
            Map<Long, CVOrderUnit> allOrderUnits,
            UserAPI userAPI,
            String sessionId,
            String requestIp,
            boolean validationRequired) throws HjalpmedelstjanstenValidationException {
        Product basedOnProduct;
        if( articleAPI.getBasedOnProduct().getId() != null ) {
            basedOnProduct = productController.getProduct(organizationUniqueId, articleAPI.getBasedOnProduct().getId(), userAPI);
        } else {
            basedOnProduct = productController.findByProductNumberAndOrganization( articleAPI.getBasedOnProduct().getProductNumber(), organizationUniqueId);
        }
        if( basedOnProduct == null ) {
            LOG.log(Level.FINEST, "product {0} does not exist", new Object[] {articleAPI.getBasedOnProduct().getId()});
            throw validationMessageService.generateValidationException("basedOnProduct", "article.product.basedOn.notExists");
        }
        Category category = basedOnProduct.getCategory();
        return createArticle(organizationUniqueId, articleAPI, category, basedOnProduct, categorySpecificPropertys, allOrderUnits, userAPI, sessionId, requestIp, validationRequired);
    }

    public Article createArticle(long organizationUniqueId,
            ArticleAPI articleAPI,
            Category category,
            Product basedOnProduct,
            List<CategorySpecificProperty> categorySpecificPropertys,
            Map<Long, CVOrderUnit> allOrderUnits,
            UserAPI userAPI,
            String sessionId,
            String requestIp,
            boolean validationRequired) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createArticle( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        ArticleMapper.fix(articleAPI);

        // remove any id references to category specific properties that may be
        // sent. when creating, there are no previous values. it's easier to do
        // it here in backend than to solve it in frontend and it also makes
        // more sense to have it here.
        if( articleAPI.getCategoryPropertys() != null && !articleAPI.getCategoryPropertys().isEmpty() ) {
            for( ResourceSpecificPropertyAPI resourceSpecificPropertyAPI : articleAPI.getCategoryPropertys() ) {
                resourceSpecificPropertyAPI.setId(null);
            }
        }

        if( validationRequired ) {
            articleValidation.validateForCreate(articleAPI, organizationUniqueId);
        }
        Article article = ArticleMapper.map(articleAPI);

        // add organization
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        if( organization == null ) {
            throw validationMessageService.generateValidationException("organizationId", "article.organization.notExists");
        }
        article.setOrganization(organization);

        // add based on product relation
        if( articleAPI.getBasedOnProduct() != null ) {
            article.setBasedOnProduct(basedOnProduct);
            //article.setArticleQuantityInOuterPackageUnit();
            if(article.getStatus() == Product.Status.DISCONTINUED) {
                article.setOrderUnit(basedOnProduct.getOrderUnit());
            }
        } else {
            // add primary category, if article is based on product, category is inherited
            article.setCategory(category);
        }

        // add fits to
        setFitsTo(organizationUniqueId, articleAPI, article, userAPI);

        // add extended categories
        setExtendedCategories(articleAPI, article);

        // add ce, directives and standards (if any)
        setCE(articleAPI, article);

        // add manufacturer
        setManufacturer(articleAPI, article);

        // add preventive maintainance
        setPreventiveMaintenance(articleAPI, article, userAPI);

        // customer unique article
        setCustomerUnique(articleAPI, article);

        // order information
        checkSetOrderInformation(articleAPI, article, allOrderUnits);

        // set supplemental information
        setSupplementedInformation(articleAPI, article);

        // category specific properties
        setResourceSpecificProperties(articleAPI, article, category, categorySpecificPropertys);

        article.setMediaFolderName(UUID.randomUUID().toString());

        // HJAL-1825
        if(articleAPI.getReplacementDate() != null) {
            Date replacementDate = new Date(articleAPI.getReplacementDate());
            article.setReplacementDate(replacementDate);
        }


        // save it
        em.persist(article);

        if( basedOnProduct != null ) {
            // inherit media
            mediaController.inheritMediaFromProduct(article, basedOnProduct);
        }

        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ARTICLE, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, article.getUniqueId(), requestIp));

        return article;
    }

    /**
     * Get the <code>ArticleAPI</code> for the given unique id on the
     * specified organization
     *
     * @param organizationUniqueId unique id of the organization
     * @param uniqueId unique id of the article to get
     * @param userAPI
     * @param sessionId
     * @param requestIp
     * @return the corresponding <code>ArticleAPI</code>
     */
    public ArticleAPI getArticleAPI(long organizationUniqueId, long uniqueId, UserAPI userAPI, String sessionId, String requestIp) {
        LOG.log(Level.FINEST, "getArticleAPI( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        Article article = getArticle(organizationUniqueId, uniqueId, userAPI);
        if( article != null ) {
            Category category = article.getBasedOnProduct() == null ? article.getCategory(): article.getBasedOnProduct().getCategory();

            List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(category.getUniqueId());
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ARTICLE, InternalAudit.ActionType.VIEW, userAPI.getId(), sessionId, uniqueId, requestIp));
            ArticleAPI articleAPI = ArticleMapper.map( article, true, categorySpecificPropertys, userAPI );
            articleAPI.setNumberEditable(isArticleNumberEditable(article));
            return articleAPI;
        }
        return null;
    }

    /**
     * Get the <code>ArticleAPI</code> for the article with the given article number
     * on the specified organization
     *
     * @param organizationUniqueId unique id of the organization
     * @param articleNumber the article number
     * @return the corresponding <code>ArticleAPI</code>
     */
    public ArticleAPI getArticleAPIByArticleNumber(long organizationUniqueId, String articleNumber) {
        LOG.log(Level.FINEST, "getArticleAPIByArticleNumber( organizationUniqueId: {0}, articleNumber: {1} )", new Object[] {organizationUniqueId, articleNumber});
        Article article = findByArticleNumberAndOrganization(articleNumber, organizationUniqueId);
        return ArticleMapper.map( article, false, null );
    }

    public Article getArticle(long organizationUniqueId, long uniqueId, UserAPI userAPI) {
        Article article = em.find(Article.class, uniqueId);
        if( article == null ) {
            return null;
        }
        if( !article.getOrganization().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user to access article: {0} not on given organization: {1}. Returning 404.", new Object[] {uniqueId, organizationUniqueId});
            return null;
        }
        if( checkCustomerUnique(article, userAPI) ) {
            return article;
        } else {
            return null;
        }
    }

    public Article getArticle(long uniqueId, UserAPI userAPI) {
        Article article = em.find(Article.class, uniqueId);
        if( article == null ) {
            return null;
        }
        if( checkCustomerUnique(article, userAPI) ) {
            return article;
        } else {
            return null;
        }
    }

    /**
     * Get the <code>Article</code> with the given id on the specified organization
     *
     * @param article unique id of the organization
     * @param userAPI
     * @return the corresponding <code>Article</code>
     */
    private boolean checkCustomerUnique(Article article, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getArticle( article->uniqueId: {0} )", new Object[] {article.getUniqueId()});
        boolean customerUnique = article.isCustomerUniqueOverridden() ? article.isCustomerUnique(): article.getBasedOnProduct() == null ? article.isCustomerUnique(): article.getBasedOnProduct().isCustomerUnique();
        if( customerUnique ) {
            LOG.log(Level.FINEST, "customerUnique");
            // this means only the organization that created the article can read it
            // or the article is on an agreement
            // or you are serviceOwner
            Organization organization = organizationController.getOrganizationFromLoggedInUser(userAPI);
            if( organization.getOrganizationType() != Organization.OrganizationType.SERVICE_OWNER ) {
                if (organization.getOrganizationType() == Organization.OrganizationType.CUSTOMER) {
                    // must have the product on an agreement
                    List<AgreementPricelistRow> agreementPricelistRows = agreementPricelistRowController.getOrganizationPricelistRowsByArticle(organization, article.getUniqueId(), userAPI);
                    if (agreementPricelistRows == null || agreementPricelistRows.isEmpty()) {
                        LOG.log(Level.INFO, "Attempt by user on customer organization to access article: {0} not available to logged in user organization: {1}. Returning null.", new Object[]{article.getUniqueId(), organization.getUniqueId()});
                        return false;
                    }
                } else if (organization.getOrganizationType() == Organization.OrganizationType.SUPPLIER) {
                    // must be the supplier of the article
                    if (!article.getOrganization().getUniqueId().equals(organization.getUniqueId())) {
                        LOG.log(Level.INFO, "Attempt by user on supplier organization to access article: {0} not available to logged in user organization: {1}. Returning null.", new Object[]{article.getUniqueId(), organization.getUniqueId()});
                        return false;
                    }
                } else {
                    LOG.log(Level.INFO, "Attempt by user to access article: {0} not available to logged in user organization: {1}. Returning null.", new Object[]{article.getUniqueId(), organization.getUniqueId()});
                    return false;
                }
            }
        }
        else {
            LOG.log(Level.FINEST, "NOT customerUnique");
        }
        return true;
    }

    /**
     * Update the <code>Article</code> with the given id with the supplied values.
     *
     * @param organizationUniqueId the unique id of the <code>Organization</code>
     * @param uniqueId the unique id of the <code>Article</code>
     * @param articleAPI the user supplied values
     * @param userAPI logged in users sessions information
     * @param sessionId
     * @param requestIp
     * @param doIndex
     * @return the updated <code>ArticleAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case of validation failure
     */
    public ArticleAPI updateArticle(long organizationUniqueId,
                                    long uniqueId,
                                    ArticleAPI articleAPI,
                                    UserAPI userAPI,
                                    String sessionId,
                                    String requestIp,
                                    boolean doIndex,
                                    Boolean causedByOrganizationInactivation) throws HjalpmedelstjanstenValidationException {
        Article article = getArticle(organizationUniqueId, uniqueId, userAPI);
        if( article == null ) {
            return null;
        }
        Category category = article.getBasedOnProduct() == null ? article.getCategory(): article.getBasedOnProduct().getCategory();
        Map<Long, CVOrderUnit> allOrderUnits = orderUnitController.findAllAsIdMap();
        List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(category.getUniqueId());
        article = updateArticle(article,
                articleAPI,
                allOrderUnits,
                categorySpecificPropertys,
                category,
                userAPI,
                sessionId,
                requestIp,
                causedByOrganizationInactivation);
        articleAPI = ArticleMapper.map( article, true, categorySpecificPropertys, userAPI);
        articleAPI.setNumberEditable(isArticleNumberEditable(article));
        if( doIndex ) {
            if (causedByOrganizationInactivation)
                elasticSearchController.bulkUpdateIndexArticle(articleAPI);
            else
                elasticSearchController.updateIndexArticle(articleAPI);
        }
        return articleAPI;
    }

    public ArticleAPI updateArticleFromProduct(long organizationUniqueId,
                                               long uniqueId,
                                               ArticleAPI articleAPI,
                                               UserAPI userAPI,
                                               String sessionId,
                                               String requestIp,
                                               boolean doIndex,
                                               Date oldArticleRepDate,
                                               Boolean causedByOrganizationInactivation) throws HjalpmedelstjanstenValidationException {
        Article article = getArticle(organizationUniqueId, uniqueId, userAPI);
        if( article == null ) {
            return null;
        }
//        //HJAL-2386
//        if (!isArticleNumberEditable(article)) {
//            changedArticle(article, articleAPI);
//        }
        Category category = article.getBasedOnProduct() == null ? article.getCategory(): article.getBasedOnProduct().getCategory();
        Map<Long, CVOrderUnit> allOrderUnits = orderUnitController.findAllAsIdMap();
        List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(category.getUniqueId());
        article = updateArticleFromProduct(article,
                articleAPI,
                allOrderUnits,
                categorySpecificPropertys,
                category,
                userAPI,
                sessionId,
                requestIp,
                oldArticleRepDate,
                causedByOrganizationInactivation);
        articleAPI = ArticleMapper.map( article, true, categorySpecificPropertys, userAPI);
        articleAPI.setNumberEditable(isArticleNumberEditable(article));
        if( doIndex ) {
            //elasticSearchController.updateIndexArticle(articleAPI);
            elasticSearchController.bulkUpdateIndexArticle(articleAPI);
        }
        return articleAPI;
    }

    /**
     * Update the <code>Article</code> with the given id with the supplied values.
     *
     * @param organizationUniqueId the unique id of the <code>Organization</code>
     * @param uniqueId the unique id of the <code>Article</code>
     * @param articleAPI the user supplied values
     * @param allOrderUnits
     * @param categorySpecificPropertys
     * @param userAPI logged in users sessions information
     * @param sessionId
     * @param requestIp
     * @param doIndex
     * @return the updated <code>ArticleAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case of validation failure
     */
    //unused?
    public ArticleAPI updateArticle(long organizationUniqueId,
                                    long uniqueId,
                                    ArticleAPI articleAPI,
                                    Map<Long, CVOrderUnit> allOrderUnits,
                                    List<CategorySpecificProperty> categorySpecificPropertys,
                                    UserAPI userAPI,
                                    String sessionId,
                                    String requestIp,
                                    boolean doIndex,
                                    Boolean causedByOrganizationInactivation) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateArticle( organizationUniqueId: {0}, uniqueId: {1}, doIndex: {2} )", new Object[] {organizationUniqueId, uniqueId, doIndex});
        Article article = getArticle(organizationUniqueId, uniqueId, userAPI);
        if( article == null ) {
            return null;
        }
        Category category = article.getBasedOnProduct() == null ? article.getCategory(): article.getBasedOnProduct().getCategory();
        article = updateArticle(article,
                articleAPI,
                allOrderUnits,
                categorySpecificPropertys,
                category,
                userAPI,
                sessionId,
                requestIp,
                causedByOrganizationInactivation);
        articleAPI = ArticleMapper.map( article, true, categorySpecificPropertys, userAPI);
        if( doIndex ) {
            elasticSearchController.updateIndexArticle(articleAPI);
        }
        return articleAPI;
    }

    /**
     * Update the <code>Article</code> with the given id with the supplied values.
     *
     * @param organizationUniqueId
     * @param uniqueId
     * @param articleAPI the user supplied values
     * @param allOrderUnits
     * @param categorySpecificPropertys
     * @param userAPI logged in users sessions information
     * @param sessionId
     * @param requestIp
     * @return the updated <code>ArticleAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case of validation failure
     */
    //used by excel import
    public Article updateArticle(long organizationUniqueId,
                                 long uniqueId,
                                 ArticleAPI articleAPI,
                                 Map<Long, CVOrderUnit> allOrderUnits,
                                 List<CategorySpecificProperty> categorySpecificPropertys,
                                 UserAPI userAPI,
                                 String sessionId,
                                 String requestIp,
                                 Boolean causedByOrganizationInactivation) throws HjalpmedelstjanstenValidationException {
        Article article = getArticle(organizationUniqueId, uniqueId, userAPI);
        if( article == null ) {
            return null;
        }
        Category category = article.getBasedOnProduct() == null ? article.getCategory(): article.getBasedOnProduct().getCategory();
        return updateArticle(article, articleAPI, allOrderUnits, categorySpecificPropertys, category, userAPI, sessionId, requestIp, causedByOrganizationInactivation);
    }

    /**
     * Update the <code>Article</code> with the given id with the supplied values.
     *
     * @param article
     * @param articleAPI the user supplied values
     * @param allOrderUnits
     * @param categorySpecificPropertys
     * @param category
     * @param userAPI logged in users sessions information
     * @param sessionId
     * @param requestIp
     * @return the updated <code>ArticleAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case of validation failure
     */
    public Article updateArticle(
            Article article,
            ArticleAPI articleAPI,
            Map<Long, CVOrderUnit> allOrderUnits,
            List<CategorySpecificProperty> categorySpecificPropertys,
            Category category,
            UserAPI userAPI,
            String sessionId,
            String requestIp,
            Boolean causedByOrganizationInactivation) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateArticle( article-uniqueId: {0} )", new Object[] {article.getUniqueId()});


        ArticleMapper.fix(articleAPI);


        articleValidation.validateForUpdate(articleAPI, article, article.getOrganization().getUniqueId(),userAPI);

        String oldProductOrderUnitName = article.getBasedOnProduct() == null ? null : article.getBasedOnProduct().getOrderUnit().getName();
        String oldProductIsCeMarked = article.getBasedOnProduct() == null ? null : article.getBasedOnProduct().isCeMarked() ? "true" : "false";
        String oldProductCeDirective = article.getBasedOnProduct() == null ? null : article.getBasedOnProduct().getCeDirective() == null ? null : article.getBasedOnProduct().getCeDirective().getName();
        String oldProductCeStandard = article.getBasedOnProduct() == null ? null : article.getBasedOnProduct().getCeStandard() == null ? null : article.getBasedOnProduct().getCeStandard().getName();

        String oldArticleOrderUnitName = article.getOrderUnit() == null ? null : article.getOrderUnit().getName();
        String oldArticleIsCeMarked = article.isCeMarked() ? "true" : "false";
        String oldArticleCeDirective = article.getCeDirective() == null ? null : article.getCeDirective().getName();
        String oldArticleCeStandard = article.getCeStandard() == null ? null : article.getCeStandard().getName();

        //HJAL-2386
        //if (!isArticleNumberEditable(article)) {
            //changedArticle(article, articleAPI, false, null, null, null, null);
        //    changedArticle(article, articleAPI, false, oldProductOrderUnitName, oldProductIsCeMarked, oldProductCeDirective, oldProductCeStandard);
        //}
        if (!isArticleNumberEditable(article)) {
            //changedArticle(article, articleAPI, false, null, null, null, null);
            changedArticle(article, articleAPI, false, oldProductOrderUnitName, oldProductIsCeMarked, oldProductCeDirective, oldProductCeStandard, oldArticleOrderUnitName, oldArticleIsCeMarked, oldArticleCeDirective, oldArticleCeStandard);
        }

        Product.Status oldArticleStatus = article.getStatus();

        handleSetStatus(article, articleAPI);
        handleReplaceArticle(article, articleAPI, article.getOrganization().getUniqueId(), userAPI);

        // if the article was previously discontinued, the only possible action
        // is to republish it, or change replaced by articles
        if( oldArticleStatus != Product.Status.DISCONTINUED ) {
            article.setArticleName(articleAPI.getArticleName());
            if( isArticleNumberEditable(article) ) {
                article.setArticleNumber(articleAPI.getArticleNumber());
            }

            // update gtin
            if (articleAPI.getGtin() != null) {
                article.setGtin(articleAPI.getGtin());
            } else {
                article.setGtin(null);
            }

            // update category
            updateCategory(articleAPI, article);

            // add fits to
            setFitsTo(article.getOrganization().getUniqueId(), articleAPI, article, userAPI);

            // update extended categories
            setExtendedCategories(articleAPI, article);

            // update CE marking
            setCE(articleAPI, article);

            // update manufacturer
            setManufacturer(articleAPI, article);

            // update customer unique
            setCustomerUnique(articleAPI, article);

            // update preventive maintainance
            setPreventiveMaintenance(articleAPI, article, userAPI);

            // order information
            checkSetOrderInformation(articleAPI, article, allOrderUnits);

            // supplemental information
            setSupplementedInformation(articleAPI, article);
        }

        // category specific properties
        setResourceSpecificProperties(articleAPI, article, category, categorySpecificPropertys);




//HJAL-2386 mejl vid varje uppdatering utgår och ersätts av samlingsmail
//        sendEmailToCustomerOnChangedStatus(oldArtRepDate, article, articleAPI, userAPI, causedByOrganizationInactivation);

        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ARTICLE, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, article.getUniqueId(), requestIp));
        return article;
    }

    public Article updateArticleFromProduct(
            Article article,
            ArticleAPI articleAPI,
            Map<Long, CVOrderUnit> allOrderUnits,
            List<CategorySpecificProperty> categorySpecificPropertys,
            Category category,
            UserAPI userAPI,
            String sessionId,
            String requestIp,
            Date oldArtRepDate,
            Boolean causedByOrganizationInactivation) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateArticle( article-uniqueId: {0} )", new Object[] {article.getUniqueId()});

        ArticleMapper.fix(articleAPI);
        articleValidation.validateForUpdate(articleAPI, article, article.getOrganization().getUniqueId(),userAPI);





        Product.Status oldArticleStatus = article.getStatus();

        handleSetStatus(article, articleAPI);
        handleReplaceArticle(article, articleAPI, article.getOrganization().getUniqueId(), userAPI);

        // if the article was previously discontinued, the only possible action
        // is to republish it, or change replaced by articles
        if( oldArticleStatus != Product.Status.DISCONTINUED ) {
            article.setArticleName(articleAPI.getArticleName());
            if( isArticleNumberEditable(article) ) {
                article.setArticleNumber(articleAPI.getArticleNumber());
            }
            article.setGtin(articleAPI.getGtin());

            // update category
            updateCategory(articleAPI, article);

            // add fits to
            setFitsTo(article.getOrganization().getUniqueId(), articleAPI, article, userAPI);

            // update extended categories
            setExtendedCategories(articleAPI, article);

            // update CE marking
            setCE(articleAPI, article);

            // update manufacturer
            setManufacturer(articleAPI, article);

            // update customer unique
            setCustomerUnique(articleAPI, article);

            // update preventive maintainance
            setPreventiveMaintenance(articleAPI, article, userAPI);

            // order information
            checkSetOrderInformation(articleAPI, article, allOrderUnits);

            // supplemental information
            setSupplementedInformation(articleAPI, article);
        }

        // category specific properties
        setResourceSpecificProperties(articleAPI, article, category, categorySpecificPropertys);


//HJAL-2386 mejl vid varje uppdatering utgår och ersätts av samlingsmail
//        sendEmailToCustomerOnChangedStatus(oldArtRepDate, article, articleAPI, userAPI, causedByOrganizationInactivation);


        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ARTICLE, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, article.getUniqueId(), requestIp));
        return article;
    }

    /**
     * Save the old and new values from a changed Article to the <code>ArticleChanged</code> .
     * HJAL-2386
     * @param article the old article values
     * @param articleAPI the user supplied values
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case of validation failure
     */
    public void changedArticle(
            Article article,
            ArticleAPI articleAPI,
            Boolean causedByProductChange,
            String oldProductOrderUnitName,
            String oldProductIsCeMarked,
            String oldProductCeDirective,
            String oldProductCeStandard,
            String oldArticleOrderUnitName,
            String oldArticleIsCeMarked,
            String oldArticleCeDirective,
            String oldArticleCeStandard) throws HjalpmedelstjanstenValidationException {

        //HJAL-2386
        Calendar calendar = Calendar.getInstance();
        ArticleChanged ac = new ArticleChanged();
        ac.setDateChanged(calendar.getTime());
        ac.setArticleId(article.getUniqueId());
        ac.setArticleNumber(article.getArticleNumber());
        ac.setSupplierId(article.getOrganization().getUniqueId());
        ac.setSupplierName(article.getOrganization().getOrganizationName());

        if (articleAPI.getArticleName() != null) {
            if (Objects.equals(articleAPI.getArticleName(), article.getArticleName())) {
                ac.setOldArticleName(null);
                ac.setNewArticleName(null);
            } else {
                ac.setNewArticleName(articleAPI.getArticleName());
                ac.setOldArticleName(article.getArticleName());
            }
        }

        //isoCode
        //H
        if (article.getBasedOnProduct() != null) {
            String oldIsoCode = article.getBasedOnProduct().getCategory() != null ? article.getBasedOnProduct().getCategory().getCode() : null;
            String newIsoCode = articleAPI.getCategory() != null ? articleAPI.getCategory().getCode() : null;
            if (Objects.equals(oldIsoCode, newIsoCode)) {
                ac.setNewIsoCode(null);
                ac.setOldIsoCode(null);
            } else {
                ac.setNewIsoCode(newIsoCode);
                ac.setOldIsoCode(oldIsoCode);
            }
        }
        //T-Iso
        if(article.getCategory() != null && article.getCategory().getArticleType() == Article.Type.T && article.getCategory().getCode() != null && !article.getCategory().getCode().isEmpty()) {
            String oldIsoCode = article.getCategory() != null ? article.getCategory().getCode() : null;
            String newIsoCode = articleAPI.getCategory() != null ? articleAPI.getCategory().getCode() : null;
            if (Objects.equals(oldIsoCode, newIsoCode)) {
                ac.setNewIsoCode(null);
                ac.setOldIsoCode(null);
            } else {
                ac.setNewIsoCode(newIsoCode);
                ac.setOldIsoCode(oldIsoCode);
            }
        }

        //Orderunit
        if( article.getBasedOnProduct() == null ) { //tillbehör, inställning, reservdel, tjänst
            if (article.getOrderUnit() != null) {
                if (Objects.equals(articleAPI.getOrderUnit().getName(), article.getOrderUnit().getName())) {
                    ac.setNewOrderUnit(null);
                    ac.setOldOrderUnit(null);
                } else {
                    ac.setNewOrderUnit(articleAPI.getOrderUnit().getName());
                    ac.setOldOrderUnit(article.getOrderUnit().getName());
                }
            }
        } else { //huvudhjälpmedel
            if (!causedByProductChange) {
                if (article.isOrderInformationOverridden()) {
                    if (!Objects.equals(articleAPI.getOrderUnit().getName(), oldArticleOrderUnitName)) {
                        ac.setNewOrderUnit(articleAPI.getOrderUnit().getName());
                        ac.setOldOrderUnit(oldArticleOrderUnitName);
                    }
                    else {
                        ac.setNewOrderUnit(null);
                        ac.setOldOrderUnit(null);
                    }
                }
                else { // article.isOrderInformationOverridden() = false
                    if (!Objects.equals(articleAPI.getOrderUnit().getName(), oldProductOrderUnitName)){
                        ac.setNewOrderUnit(articleAPI.getOrderUnit().getName());
                        ac.setOldOrderUnit(oldProductOrderUnitName);
                    }
                    else {
                        ac.setNewOrderUnit(null);
                        ac.setOldOrderUnit(null);
                    }
                }
            } else { //causedByProductChange
                if (!Objects.equals(articleAPI.getOrderUnit().getName(), oldProductOrderUnitName)) {
                    ac.setNewOrderUnit(articleAPI.getOrderUnit().getName());
                    ac.setOldOrderUnit(oldProductOrderUnitName);
                } else {
                    ac.setNewOrderUnit(null);
                    ac.setOldOrderUnit(null);
                }
            }
        }

        //CE-märkning
        if( article.getBasedOnProduct() == null ) { //tillbehör, inställning, reservdel, tjänst
            if (articleAPI.isCeMarked() && article.isCeMarked() || !articleAPI.isCeMarked() && !article.isCeMarked()) {
                ac.setNewCEmarked(null);
                ac.setOldCEmarked(null);
            } else {
                ac.setNewCEmarked(String.valueOf(articleAPI.isCeMarked()));
                ac.setOldCEmarked(String.valueOf(article.isCeMarked()));
            }
        } else { //huvudhjälpmedel
            if (!causedByProductChange) {
                if (article.isCeMarkedOverridden()) {
                    //från ibockad till urbockad
                    if (Objects.equals(oldArticleIsCeMarked, "true") && !articleAPI.isCeMarked()){
                        ac.setOldCEmarked("true");
                        ac.setNewCEmarked("false");
                    }
                    //från urbockad till ibockad
                    else if (Objects.equals(oldArticleIsCeMarked, "false") && articleAPI.isCeMarked()){
                        ac.setOldCEmarked("false");
                        ac.setNewCEmarked("true");
                    }
                    //från urbockad till urbockad eller från ibockad till ibockad
                    else {
                        ac.setOldCEmarked(null);
                        ac.setNewCEmarked(null);
                    }
                }
                else { // article.isCeMarkedOverridden() = false
                    //från ibockad till urbockad
                    if (Objects.equals(oldProductIsCeMarked, "true") && !articleAPI.isCeMarked()){
                        ac.setOldCEmarked("true");
                        ac.setNewCEmarked("false");
                    }
                    //från urbockad till ibockad
                    else if (Objects.equals(oldProductIsCeMarked, "false") && articleAPI.isCeMarked()){
                        ac.setOldCEmarked("false");
                        ac.setNewCEmarked("true");
                    }
                    //från urbockad till urbockad eller från ibockad till ibockad
                    else {
                        ac.setOldCEmarked(null);
                        ac.setNewCEmarked(null);
                    }
                }
            } else { //causedByProductChange
                if (!Objects.equals(String.valueOf(articleAPI.isCeMarked()), oldProductIsCeMarked)) {
                    ac.setNewCEmarked(String.valueOf(articleAPI.isCeMarked()));
                    ac.setOldCEmarked(oldProductIsCeMarked);
                } else {
                    ac.setNewCEmarked(null);
                    ac.setOldCEmarked(null);
                }
            }
        }

        //CE-direktiv
        if( article.getBasedOnProduct() == null ) { //tillbehör, inställning, reservdel, tjänst
            if (article.getCeDirective() != null) {
                if (Objects.equals(articleAPI.getCeDirective().getName(), article.getCeDirective().getName())) {
                    ac.setNewCVCEdirective(null);
                    ac.setOldCVCEdirective(null);
                } else {
                    ac.setNewCVCEdirective(articleAPI.getCeDirective().getName());
                    ac.setOldCVCEdirective(article.getCeDirective().getName());
                }
            }
        } else { //huvudhjälpmedel
            if (!causedByProductChange) {
                if (article.isCeDirectiveOverridden()) {
                    if (oldArticleCeDirective != null && articleAPI.getCeDirective() != null && !oldArticleCeDirective.equals(articleAPI.getCeDirective().getName())){
                        ac.setOldCVCEdirective(oldArticleCeDirective);
                        ac.setNewCVCEdirective(articleAPI.getCeDirective().getName());
                    }
                    else if (oldArticleCeDirective != null && articleAPI.getCeDirective() == null){
                        ac.setOldCVCEdirective(oldArticleCeDirective);
                        ac.setNewCVCEdirective(null);
                    }
                    else if (oldArticleCeDirective == null && articleAPI.getCeDirective() != null){
                        ac.setOldCVCEdirective(null);
                        ac.setNewCVCEdirective(articleAPI.getCeDirective().getName());
                    }
                    else{
                        ac.setOldCVCEdirective(null);
                        ac.setNewCVCEdirective(null);
                    }
                }
                else { // article.isCeDirectiveOverridden() = false
                    if (oldProductCeDirective != null && articleAPI.getCeDirective() != null && !oldProductCeDirective.equals(articleAPI.getCeDirective().getName())){
                        ac.setOldCVCEdirective(oldProductCeDirective);
                        ac.setNewCVCEdirective(articleAPI.getCeDirective().getName());
                    }
                    else if (oldProductCeDirective != null && articleAPI.getCeDirective() == null){
                        ac.setOldCVCEdirective(oldProductCeDirective);
                        ac.setNewCVCEdirective(null);
                    }
                    else if (oldProductCeDirective == null && articleAPI.getCeDirective() != null){
                        ac.setOldCVCEdirective(null);
                        ac.setNewCVCEdirective(articleAPI.getCeDirective().getName());
                    }
                    else{
                        ac.setOldCVCEdirective(null);
                        ac.setNewCVCEdirective(null);
                    }
                }
            } else { //caused by product change
                if (articleAPI.getCeDirective() != null && oldProductCeDirective != null) {
                    if (Objects.equals(articleAPI.getCeDirective().getName(), oldProductCeDirective)) {
                        ac.setNewCVCEdirective(null);
                        ac.setOldCVCEdirective(null);
                    } else {
                        ac.setNewCVCEdirective(articleAPI.getCeDirective().getName());
                        ac.setOldCVCEdirective(oldProductCeDirective);
                    }
                } else if (articleAPI.getCeDirective() == null && oldProductCeDirective == null) {
                    ac.setNewCVCEdirective(null);
                    ac.setOldCVCEdirective(null);
                } else if (articleAPI.getCeDirective() == null) {
                    ac.setNewCVCEdirective(null);
                    ac.setOldCVCEdirective(oldProductCeDirective);
                } else {
                    ac.setNewCVCEdirective(articleAPI.getCeDirective().getName());
                    ac.setOldCVCEdirective(null);
                }
            }
        }

        //CE-standard
        if( article.getBasedOnProduct() == null ) { //tillbehör, inställning, reservdel, tjänst
            if (article.getCeStandard() != null) {
                if (Objects.equals(articleAPI.getCeStandard().getName(), article.getCeStandard().getName())) {
                    ac.setNewCVCEstandard(null);
                    ac.setOldCVCEstandard(null);
                } else {
                    ac.setNewCVCEstandard(articleAPI.getCeStandard().getName());
                    ac.setOldCVCEstandard(article.getCeStandard().getName());
                }
            }
        } else { //huvudhjälpmedel
            if (!causedByProductChange) {
                if (article.isCeStandardOverridden()) {
                    if (oldArticleCeStandard != null && articleAPI.getCeStandard() != null && !oldArticleCeStandard.equals(articleAPI.getCeStandard().getName())) {
                        ac.setOldCVCEstandard(oldArticleCeStandard);
                        ac.setNewCVCEstandard(articleAPI.getCeStandard().getName());
                    } else if (oldArticleCeStandard != null && articleAPI.getCeStandard() == null) {
                        ac.setOldCVCEstandard(oldArticleCeStandard);
                        ac.setNewCVCEstandard(null);
                    } else if (oldArticleCeStandard == null && articleAPI.getCeStandard() != null) {
                        ac.setOldCVCEstandard(null);
                        ac.setNewCVCEstandard(articleAPI.getCeStandard().getName());
                    } else {
                        ac.setOldCVCEstandard(null);
                        ac.setNewCVCEstandard(null);
                    }
                } else { // article.isCeStandardOverridden() = false
                    if (oldProductCeStandard != null && articleAPI.getCeStandard() != null && !oldProductCeStandard.equals(articleAPI.getCeStandard().getName())) {
                        ac.setOldCVCEstandard(oldProductCeStandard);
                        ac.setNewCVCEstandard(articleAPI.getCeStandard().getName());
                    } else if (oldProductCeStandard != null && articleAPI.getCeStandard() == null) {
                        ac.setOldCVCEstandard(oldProductCeStandard);
                        ac.setNewCVCEstandard(null);
                    } else if (oldProductCeStandard == null && articleAPI.getCeStandard() != null) {
                        ac.setOldCVCEstandard(null);
                        ac.setNewCVCEstandard(articleAPI.getCeStandard().getName());
                    } else {
                        ac.setOldCVCEstandard(null);
                        ac.setNewCVCEstandard(null);
                    }
                }
            } else { //caused by product change
                if (articleAPI.getCeStandard() != null && oldProductCeStandard != null) {
                    if (Objects.equals(articleAPI.getCeStandard().getName(), oldProductCeStandard)) {
                        ac.setNewCVCEstandard(null);
                        ac.setOldCVCEstandard(null);
                    } else {
                        ac.setNewCVCEstandard(articleAPI.getCeStandard().getName());
                        ac.setOldCVCEstandard(oldProductCeStandard);
                    }
                } else if (articleAPI.getCeStandard() == null && oldProductCeStandard == null) {
                    ac.setNewCVCEstandard(null);
                    ac.setOldCVCEstandard(null);
                } else if (articleAPI.getCeStandard() == null) {
                    ac.setNewCVCEstandard(null);
                    ac.setOldCVCEstandard(oldProductCeStandard);
                } else {
                    ac.setNewCVCEstandard(articleAPI.getCeStandard().getName());
                    ac.setOldCVCEstandard(null);
                }
            }
        }

        if(article.getReplacementDate() != null) {
            ac.setOldReplacementDate(article.getReplacementDate());
        }

        if (articleAPI.getReplacementDate() != null) {
            ac.setNewReplacementDate(articleAPI.getReplacementDate());
        }

        //if (articleAPI.getReplacedByArticles().size() == 0) {
        if (articleAPI.getReplacedByArticles() == null || articleAPI.getReplacedByArticles().isEmpty()) {
            ac.setReplacementArticles(null);
        } else {
            List<ArticleAPI> rpArticles = articleAPI.getReplacedByArticles();
            StringBuilder articleNames = new StringBuilder();
            for (ArticleAPI rpArticle :rpArticles) {
                Long artId = rpArticle.getId();
                Article a = em.find(Article.class, artId);
                //ArticleAPI artAPI = ArticleMapper.map(a, true, null);
                Category category = article.getBasedOnProduct() == null ? article.getCategory() : article.getBasedOnProduct().getCategory();
                List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(category.getUniqueId());
                ArticleAPI artAPI = ArticleMapper.map(a, true, categorySpecificPropertys);

                String artName = artAPI.getArticleName();
                if (rpArticles.indexOf(rpArticle) == rpArticles.size() -1) {
                    articleNames.append(artName);
                } else {
                    articleNames.append(artName).append(", ");
                }
            }

            String replacementArticles = String.valueOf(articleNames);

            ac.setReplacementArticles(replacementArticles);
        }

        if (ac.getNewArticleName() != null ||
                ac.getNewOrderUnit() != null ||
                ac.getNewCEmarked() != null ||
                !Objects.equals(ac.getNewCVCEdirective(), ac.getOldCVCEdirective()) ||
                !Objects.equals(ac.getNewCVCEstandard(), ac.getOldCVCEstandard()) ||
                ac.getNewReplacementDate() != null ||
                ac.getOldReplacementDate() != null ||
                ac.getReplacementArticles() != null ||
                ac.getNewIsoCode() != null) {
            //Save old and new values in ArticleChanged
            em.persist(ac);

            //get uniqueId of the ArticleChanged
            Long changeId = ac.getUniqueId();

            Long articleId = article.getUniqueId();

            //find users who should look att the changes (customerAgreementManagers and approver of the current or future pricelists the article is on) De ska få mejl och se notisen tills dom klickat på raden.

            String sql = "SELECT DISTINCT(ue.uniqueId) FROM hjmtj.AgreementPricelistRow apr";
            sql += " INNER JOIN  hjmtj.AgreementPricelist ap ON ap.uniqueId = apr.agreementpricelistId";
            sql += " INNER JOIN hjmtj.Agreement a ON a.uniqueId = ap.agreementId";
            sql += " INNER JOIN hjmtj.AgreementUserEngagementCustomer auec ON auec.agreementId = a.uniqueId";
            sql += " INNER JOIN hjmtj.UserEngagement ue on ue.uniqueId = auec.userEngagementId";
            sql += " WHERE (a.status = 'CURRENT' OR a.status = 'FUTURE')";
            sql += " AND ap.uniqueId IN (SELECT a1.uniqueId FROM hjmtj.AgreementPricelist a1 INNER JOIN hjmtj.AgreementPricelist a2 ON a1.uniqueID = a2.uniqueId";
            sql += " WHERE a1.validFrom > current_date() OR a1.validFrom = (SELECT max(a2.validFrom)";
            sql += " FROM hjmtj.AgreementPricelist a2 WHERE a2.validFrom <= current_date() AND a1.agreementId = a2.agreementId ))";
            sql += " AND apr.articleId = " + articleId;

            List<Object> userIds = em.createNativeQuery(sql).getResultList();

            if (!userIds.isEmpty()) {
                for (Object userId : userIds) {
                    String user = String.valueOf(userId);
                    Long useridLong = Long.valueOf(user);
                    ChangeChecker cc = new ChangeChecker();
                    cc.setType("ARTICLE");
                    cc.setChangeId(changeId);
                    cc.setUserId(useridLong);
                    em.persist(cc);
                }
            }
        }



    }

    /**
     * Save the old and new values from an Article changed by switchBasedOnProduct to the <code>ArticleChanged</code> .
     * HJAL-2386
     * @param article the old article values
     * @param product the new product the article is based on
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case of validation failure
     */
    public void changedArticleBySwitchBasedOn(
            Article article,
            Product product) throws HjalpmedelstjanstenValidationException {

        //HJAL-2386
        Calendar calendar = Calendar.getInstance();
        ArticleChanged ac = new ArticleChanged();
        ac.setDateChanged(calendar.getTime());
        ac.setArticleId(article.getUniqueId());
        ac.setArticleNumber(article.getArticleNumber());
        ac.setSupplierId(article.getOrganization().getUniqueId());
        ac.setSupplierName(article.getOrganization().getOrganizationName());

        ArticleAPI art = ArticleMapper.map(article, true, null);
        ProductAPI prod = ProductMapper.map(product, true, null);

        if (Objects.equals(prod.getCategory().getCode(), art.getBasedOnProduct().getCategory().getCode())) {
            ac.setNewIsoCode(null);
            ac.setOldIsoCode(null);
        } else {
            ac.setNewIsoCode(prod.getCategory().getCode());
            ac.setOldIsoCode(art.getBasedOnProduct().getCategory().getCode());
        }


        ac.setNewBasedOnProductNumber(prod.getProductNumber());
        ac.setOldBasedOnProductNumber(art.getBasedOnProduct().getProductNumber());

        //Save old and new values in ArticleChanged
        em.persist(ac);

        //get uniqueId of the ArticleChanged
        Long changeId = ac.getUniqueId();

        Long articleId = article.getUniqueId();

        //find users who should look att the changes (customerAgreementManagers and approver of the pricelists the article is on) De ska få mejl och se notisen tillds dom klickat på raden.

        String sql = "SELECT DISTINCT(ue.uniqueId) as userId FROM hjmtj.AgreementPricelistRow apr\n" +
                "INNER JOIN  hjmtj.AgreementPricelist ap ON ap.uniqueId = apr.agreementpricelistId\n" +
                "INNER JOIN hjmtj.Agreement a ON a.uniqueId = ap.agreementId\n" +
                "INNER JOIN hjmtj.AgreementUserEngagementCustomer auec ON auec.agreementId = a.uniqueId\n" +
                "INNER JOIN hjmtj.UserEngagement ue on ue.uniqueId = auec.userEngagementId\n" +
                "WHERE (a.status = 'CURRENT' OR a.status = 'FUTURE') AND apr.articleId = " + articleId  ;

        List<Object> userIds = em.createNativeQuery(sql).getResultList();

        if (!userIds.isEmpty()) {
            for (Object userId : userIds) {
                String user = String.valueOf(userId);
                Long useridLong = Long.valueOf(user);
                ChangeChecker cc = new ChangeChecker();
                cc.setType("ARTICLE");
                cc.setChangeId(changeId);
                cc.setUserId(useridLong);
                em.persist(cc);
            }
        }

    }

    public List<ArticleChanged> getArticlesChanged(UserAPI userAPI) {

        //Hämta de artiklar som ändrats ur ArticleChanged, utifrån changeId i Changechecker
        //HJAL-2386

        String sql = "SELECT ac.*, cc.changeId, cc.type, cc.userId, cc.assortmentId FROM hjmtj.ArticleChanged ac \n" +
                "INNER JOIN hjmtj.ChangeChecker cc on cc.changeId = ac.uniqueId\n" +
                "WHERE cc.type = 'ARTICLE' AND cc.userId = " + userAPI.getId();
        return (List<ArticleChanged>) em.createNativeQuery(sql, ArticleChanged.class).getResultList();
    }

    public List<ChangeChecker> removeArticleChangedNotice(long articleId, long userId, boolean removeAll) {
        String sql;
        if (!removeAll) {
            //Ta bort endast en notis
            sql = "SELECT * FROM hjmtj.ChangeChecker cc WHERE cc.userId = " + userId +" AND cc.changeId = " + articleId + " AND cc.type = 'ARTICLE' " ;
        } else {
            //Ta bort alla artikelnotiser för denna användare
            sql = "SELECT * FROM hjmtj.ChangeChecker cc WHERE cc.userId = " + userId +" AND cc.type = 'ARTICLE' " ;

        }


        List<ChangeChecker> cc = em.createNativeQuery(sql, ChangeChecker.class).getResultList();

        for (ChangeChecker c : cc) {
            ChangeChecker removecc = em.find(ChangeChecker.class, c.getUniqueId());
            em.remove(removecc);
        }

        String sqlCCrows = "SELECT count(*) FROM hjmtj.ChangeChecker cc WHERE cc.changeId = " + articleId + " AND cc.type = 'ARTICLE'";
        Long rowsCc = (Long) em.createNativeQuery(sqlCCrows).getSingleResult();


        if (rowsCc == 0) {

            String sqlRemoveAC = "SELECT * FROM hjmtj.ArticleChanged ac WHERE ac.uniqueId = " + articleId;
            List<ArticleChanged> removeAC = em.createNativeQuery(sqlRemoveAC, ArticleChanged.class).getResultList();
            for (ArticleChanged ac : removeAC) {
                ArticleChanged artChanged = em.find(ArticleChanged.class, ac.getUniqueId());
                em.remove(artChanged);
            }
        }

        return cc;

    }

    public void sendEmailForArticlesChanged() {
        //mail M9-1

        String sql = "SELECT DISTINCT(ea.email) FROM hjmtj.ChangeChecker cc \n" +
            "INNER JOIN hjmtj.UserEngagement ue ON cc.userId = ue.uniqueId \n" +
            "INNER JOIN hjmtj.UserAccount ua ON ue.userId = ua.uniqueId \n" +
            "INNER JOIN hjmtj.ElectronicAddress ea ON ua.primaryElectronicAdressId = ea.uniqueId \n" +
            "INNER JOIN hjmtj.UserAccountNotification un ON un.userId = ua.uniqueId \n" +
            "INNER JOIN hjmtj.Notification n ON n.uniqueId = un.notificationId \n" +
            "WHERE cc.type = 'ARTICLE' AND n.type = 'ARTICLES_CHANGED'\n" +
            "AND (ue.validTo IS NULL OR ue.validTo >= CURDATE())";

        List<String> emailadresses = em.createNativeQuery(sql).getResultList();

        for (String emailadress : emailadresses) {
            try {
                if(emailadress != null && !emailadress.isEmpty()) {
                    emailController.send(emailadress, articlesChangedMailSubject, articlesChangedMailBody);
                }

            } catch (MessagingException ex) {

                LOG.log(Level.SEVERE, "Failed to send notification message to customer about articles on pricelists that hs changed");
            }

        }

    }

    /**
     * Switch the product an articles is based on. This function affects the article a lot
     *  - removes article from pricelists agreements/gps
     *  - removes article from Assortment
     * @param organizationUniqueId
     * @param uniqueId
     * @param productUniqueId
     * @param userAPI
     * @param sessionId
     * @param requestIp
     * @return
     * @throws HjalpmedelstjanstenValidationException
     */
    public ArticleAPI switchArticleBasedOn(long organizationUniqueId,
            long uniqueId,
            long productUniqueId,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        Article article = getArticle(organizationUniqueId, uniqueId, userAPI);
        if( article == null ) {
            return null;
        }
        if( article.getBasedOnProduct() == null ) {
            throw validationMessageService.generateValidationException("basedOnProduct", "article.switchbasedon.notbasedon");
        }
        if( article.getBasedOnProduct().getUniqueId().equals(productUniqueId) ) {
            throw validationMessageService.generateValidationException("basedOnProduct", "article.switchbasedon.sameproduct");
        }
        Product product = productController.getProduct(organizationUniqueId, productUniqueId, userAPI);
        if( product == null ) {
            return null;
        }
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ARTICLE, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, article.getUniqueId(), requestIp));

        //HJAL-2386

        if (!isArticleNumberEditable(article)) {
            changedArticleBySwitchBasedOn(article, product);
        }


        //2143 artiklar ska inte längre försvinna från avtal / gp

        // delete all references from gp pricelist rows
        //generalPricelistPricelistRowController.deleteRowsByArticle(article);

        // delete all references from agreement pricelist rows
        //agreementPricelistRowController.deleteRowsByArticle(article);
        // delete all references from assortment
        assortmentController.deleteRowsByArticle(article);

        // remove all previous cateogry specific properties
        article.getResourceSpecificPropertyValues().clear();

        article.setBasedOnProduct(product);
        // inherit all media from new product
        mediaController.inheritMediaFromProduct(article, product);

        // only articles based on can switch based on
        Category category = article.getBasedOnProduct().getCategory();
        List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(category.getUniqueId());
        ArticleAPI articleAPI =  ArticleMapper.map( article, true, categorySpecificPropertys);
        List<AgreementPricelistRowAPI> agreementPricelistRowAPIS = getArticlePricelistRows(articleAPI.getId(),userAPI);
        String selectedSubject = rowsSentForApprovalOnArticleChangedBasedOnMailSubject;
        String selectedBody = rowsSentForApprovalOnArticleChangedBasedOnMailBody;

        List<String> customerMailList = new ArrayList<>();

        for(AgreementPricelistRowAPI agreementPriceListRowAPI : agreementPricelistRowAPIS) {
            LOG.log( Level.FINEST, "rows sent for approval, send mail for agreement: {0}", new Object[] {agreementPriceListRowAPI.getPricelist().getAgreement().getAgreementName()});
            AgreementAPI agreement = agreementPriceListRowAPI.getPricelist().getAgreement();
            List<AgreementPricelistRowAPI> tempList = agreementPricelistRowController.sendRowForCustomerApprovalWithoutMail(organizationUniqueId,agreement.getId(),agreementPriceListRowAPI.getPricelist().getId(),agreementPriceListRowAPI,userAPI,sessionId,requestIp);

            //check if approver has approved to receive mail regarding approval
            List<?> realApprovers = approversWhoApprovedToReceiveApproveMail();

            if(tempList != null){
                try{
                    //List<String> customerMailList = new ArrayList<>();
                    for (UserAPI userEngagement : agreement.getCustomerPricelistApprovers()) {
                        if (realApprovers.contains(convertLongToBigInteger(userEngagement.getId()))) {
                            LOG.log(Level.FINEST, userEngagement.getUsername() + " has chosen to receive email regarding approvals caused by change of based on");
                            if (!customerMailList.contains(userEngagement.getElectronicAddress().getEmail())) {
                                emailController.send(userEngagement.getElectronicAddress().getEmail(), selectedSubject, selectedBody);
                                customerMailList.add(userEngagement.getElectronicAddress().getEmail());
                            }
                            else{
                                LOG.log(Level.FINEST, userEngagement.getUsername() + " has already received an email regarding approvals caused by change of based on product for this article");
                            }
                        }
                        else{
                            LOG.log(Level.FINEST, userEngagement.getUsername() + " has chosen NOT to receive mail regarding approvals caused by change of based on");
                        }
                    }
                }
                catch (MessagingException ex) {
                    LOG.log(Level.SEVERE, "Failed to send message on rows sent for approval, article switched based on", ex);
                }
            }
        }
        elasticSearchController.updateIndexArticle(articleAPI);
        return articleAPI;
    }

    private BigInteger convertLongToBigInteger(long value){
        return new BigInteger(ByteBuffer.allocate(Long.SIZE/Byte.SIZE).putLong(value).array());
    }

    private List<?> approversWhoApprovedToReceiveApproveMail() {
        String sql = "SELECT ue.uniqueId FROM hjmtj.UserAccountNotification uan";
        sql +=" INNER JOIN hjmtj.Notification n ON uan.notificationId = n.uniqueId";
        sql +=" INNER JOIN hjmtj.UserEngagement ue ON ue.userId = uan.userId";
        sql +=" WHERE n.type = 'AGREEMENT_PRICELISTROW_APPROVE_BASED_ON'";
        return em.createNativeQuery(sql).getResultList();
    }

    /**
     * Find articles with a specific gtin.
     *
     * @param gtin the article gtin
     * @return the article or null
     */
    public Article findByGtin(String gtin) {
        LOG.log(Level.FINEST, "findByGtin( gtin: {0} )", new Object[] {gtin});
        try {
            return (Article) em.createNamedQuery(Article.FIND_BY_GTIN).
                    setParameter("gtin", gtin).
                    getSingleResult();
        } catch( NoResultException ex ) {
            return null;
        }
    }

    /**
     * Find articles with a specific articleNumber. Must check per organization
     * since uniqueness is only guaranteed within an organization.
     *
     * @param articleNumber the article number
     * @param organizationUniqueId unique id of the organization
     * @return the article or null
     */
    public Article findByArticleNumberAndOrganization( String articleNumber, long organizationUniqueId ) {
        LOG.log(Level.FINEST, "findByArticleNumberAndOrganization( articleNumber: {0}, organizationUniqueId: {1} )", new Object[] {articleNumber, organizationUniqueId});
        try {
            return (Article)  em.createNamedQuery(Article.FIND_BY_ARTICLE_NUMBER_AND_ORGANIZATION).
                    setParameter("articleNumber", articleNumber).
                    setParameter("organizationUniqueId", organizationUniqueId).
                    getSingleResult();
        } catch( NoResultException ex ) {
            return null;
        }
    }

    public void delete(long organizationUniqueId,
                       long articleUniqueId,
                       UserAPI userAPI) {
        Article article = getArticle(organizationUniqueId, articleUniqueId, userAPI);

        ArticleAPI articleAPI = getArticleAPIByArticleNumber( organizationUniqueId, article.getArticleNumber());
        em.remove(article);
        elasticSearchController.removeIndexArticle(articleAPI);
    }

    public boolean soDelete(long organizationUniqueId,
                            long articleUniqueId,
                            UserAPI userAPI)throws HjalpmedelstjanstenValidationException{


        try {
            Article article = getArticle(articleUniqueId, userAPI);
            ArticleAPI articleAPI = getArticleAPIByArticleNumber( organizationUniqueId, article.getArticleNumber());

            StringBuilder sb = new StringBuilder();

            sb.append("DELETE FROM hjmtj.GeneralPricelistPricelistRow WHERE articleId = :articleId ");
            em.createNativeQuery(sb.toString()).setParameter("articleId", articleUniqueId).executeUpdate();
            sb.delete(0, sb.length());
            sb.append("DELETE FROM hjmtj.ArticleMediaImage WHERE articleId = :articleId ");
            em.createNativeQuery(sb.toString()).setParameter("articleId", articleUniqueId).executeUpdate();
            sb.delete(0, sb.length());
            sb.append("DELETE FROM hjmtj.ArticleMediaDocument WHERE articleId = :articleId ");
            em.createNativeQuery(sb.toString()).setParameter("articleId", articleUniqueId).executeUpdate();
            sb.delete(0, sb.length());
            sb.append("DELETE FROM hjmtj.ArticleMediaVideo WHERE articleId = :articleId ");
            em.createNativeQuery(sb.toString()).setParameter("articleId", articleUniqueId).executeUpdate();
            sb.delete(0, sb.length());
            sb.append("DELETE FROM hjmtj.ResourceSpecificPropertyValue WHERE articleId = :articleId ");
            em.createNativeQuery(sb.toString()).setParameter("articleId", articleUniqueId).executeUpdate();
            sb.delete(0, sb.length());
            sb.append("DELETE FROM hjmtj.ArticleFitsToArticle WHERE fittedByArticleId = :articleId ");
            em.createNativeQuery(sb.toString()).setParameter("articleId", articleUniqueId).executeUpdate();
            sb.delete(0, sb.length());
            sb.append("DELETE FROM hjmtj.ArticleFitsToArticle WHERE fitsToArticleId = :articleId ");
            em.createNativeQuery(sb.toString()).setParameter("articleId", articleUniqueId).executeUpdate();
            sb.delete(0, sb.length());
            sb.append("DELETE FROM hjmtj.AssortmentArticle WHERE articleId = :articleId ");
            em.createNativeQuery(sb.toString()).setParameter("articleId", articleUniqueId).executeUpdate();
            sb.delete(0, sb.length());
            sb.append("DELETE FROM hjmtj.AgreementPricelistRow WHERE articleId = :articleId ");
            em.createNativeQuery(sb.toString()).setParameter("articleId", articleUniqueId).executeUpdate();
            sb.delete(0, sb.length());
            sb.append("DELETE FROM hjmtj.ArticleFitsToProduct WHERE articleId = :articleId ");
            em.createNativeQuery(sb.toString()).setParameter("articleId", articleUniqueId).executeUpdate();
            sb.delete(0, sb.length());
            sb.append("DELETE FROM hjmtj.ArticleExtendedCategory WHERE articleId = :articleId ");
            em.createNativeQuery(sb.toString()).setParameter("articleId", articleUniqueId).executeUpdate();
            sb.delete(0, sb.length());
            sb.append("DELETE FROM hjmtj.ArticleReplacedByArticle WHERE articleId = :articleId ");
            em.createNativeQuery(sb.toString()).setParameter("articleId", articleUniqueId).executeUpdate();
            sb.delete(0, sb.length());
            sb.append("DELETE FROM hjmtj.ArticleReplacedByArticle WHERE replacedByArticleId = :articleId ");
            em.createNativeQuery(sb.toString()).setParameter("articleId", articleUniqueId).executeUpdate();
            sb.delete(0, sb.length());
            sb.append("DELETE FROM hjmtj.Article WHERE uniqueId = :articleId ");
            em.createNativeQuery(sb.toString()).setParameter("articleId", articleUniqueId).executeUpdate();

            elasticSearchController.removeIndexArticle(articleAPI);



            //  Block of code to try
        }
        catch(Exception e) {
            //  Block of code to handle errors
            throw validationMessageService.generateValidationException("code", "Fel när artikel och samtliga kopplingar skulle tas bort");
        }
        return true;
    }

    public ArticleAPI deleteFitsTo(long organizationUniqueId,
                                   long articleUniqueId,
                                   long fitsToUniqueId,
                                   UserAPI userAPI,
                                   String sessionId,
                                   String requestIp) throws HjalpmedelstjanstenValidationException {
        Article article = getArticle(organizationUniqueId, articleUniqueId, userAPI);
        if( article == null ) {
            return null;
        }
        Product fitsToProduct = productController.getProduct(organizationUniqueId, fitsToUniqueId, userAPI);
        Article fitsToArticle = getArticle(organizationUniqueId, fitsToUniqueId, userAPI);
        if( fitsToArticle == null ) {
            if( fitsToProduct == null ) {
                return null;
            }
        }
        // since we allow disconnection from "both directions" we first need to check
        // which direction the connection is from
        boolean connectionFound = false;
        int thisArticleTotalFitsTo = article.getFitsToArticles().size() + article.getFitsToProducts().size();
        if( fitsToArticle != null ) {
            Article removedArticle = removeArticleConnectionForArticle(article, fitsToUniqueId);
            if( removedArticle != null ) {
                connectionFound = true;
            }
        } else {
            for( Product articleFitsToProduct : article.getFitsToProducts() ) {
                if( articleFitsToProduct.getUniqueId().equals(fitsToUniqueId) ) {
                    connectionFound = true;
                    // ok, article this article fits to other product, make sure it's
                    // no the last connection
                    if( thisArticleTotalFitsTo < 2 ) {
                        // cannot remove last connection
                        throw validationMessageService.generateValidationException("fitsToArticle", "article.fitsTo.lastConnection", article.getArticleName());
                    } else {
                        article.getFitsToProducts().remove(articleFitsToProduct);
                        break;
                    }

                }
            }
        }

        if( !connectionFound && fitsToArticle != null ) {
            Article removedArticle = removeArticleConnectionForArticle(fitsToArticle, articleUniqueId);
            if( removedArticle != null ) {
                connectionFound = true;
            }
        }
        if( !connectionFound ) {
            throw validationMessageService.generateValidationException("fitsToArticle", "article.fitsTo.notFound", article.getArticleName());
        }
        Category category = article.getBasedOnProduct() == null ? article.getCategory(): article.getBasedOnProduct().getCategory();
        List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(category.getUniqueId());
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ARTICLE, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, articleUniqueId, requestIp));
        return ArticleMapper.map(article, true, categorySpecificPropertys);
    }

    /**
     * Finds and removes fits to article for a specific article
     *
     * @param article the article to look for fits to
     * @param fitsToArticleUniqueId unique id of the article to look for in fits to
     * @return the removed fits to article or null if the article was not found
     * @throws HjalpmedelstjanstenValidationException if an attempt is made to remove the last
     * connection from the given article
     */
    Article removeArticleConnectionForArticle( Article article, long fitsToArticleUniqueId ) throws HjalpmedelstjanstenValidationException {
        int thisArticleTotalFitsTo = article.getFitsToArticles().size() + article.getFitsToProducts().size();
        // check connections from fitsToArticle since fitsToProduct has no connections
        for( Article articleFitsToArticle : article.getFitsToArticles() ) {
            if( articleFitsToArticle.getUniqueId().equals(fitsToArticleUniqueId) ) {
                // ok, article this article fits to other article, make sure it's
                // no the last connection
                if( thisArticleTotalFitsTo < 2 ) {
                    // cannot remove last connection
                    throw validationMessageService.generateValidationException("fitsToArticle", "article.fitsTo.lastConnection", article.getArticleName());
                } else {
                    article.getFitsToArticles().remove(articleFitsToArticle);
                    return articleFitsToArticle;
                }
            }
        }
        return null;
    }

    /**
     * Set article status.
     *
     * @param article
     * @param articleAPI
     */
    private void handleSetStatus(Article article, ArticleAPI articleAPI) {
        if( articleAPI.getReplacementDate() == null  && article.getStatus() != Product.Status.DISCONTINUED) {
            article.setStatus(Product.Status.valueOf(articleAPI.getStatus()));
        }
        if( article.getBasedOnProduct() == null ) {
            article.setStatusOverridden(true);
        }
        /*else {
            if (!Objects.equals(articleAPI.getBasedOnProduct().getStatus(), articleAPI.getStatus())) {
                article.setStatusOverridden(true);
            } else {
                article.setStatusOverridden(false);
            }
        }

         */
    }

    /**
     * Handle article replacement functionality and checks.
     *
     * @param article article to handle replacement of
     * @param articleAPI user supplied values
     * @param organizationUniqueId unique id of the organization
     * @param userAPI
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * if the article is discontineued
     */
    public void handleReplaceArticle(Article article, ArticleAPI articleAPI, long organizationUniqueId, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "handleReplaceArticle( organizationUniqueId: {0}, article->uniqueId: {1} )", new Object[] {organizationUniqueId, article.getUniqueId()});
        if( article.getStatus() == Product.Status.DISCONTINUED ) {
            // if article already is discontinued, we can only republish or change replacement article
            if( articleAPI.getReplacementDate() != null ) {
                Date replacementDate = new Date(articleAPI.getReplacementDate());
                if( replacementDate.after(article.getReplacementDate())) {
                    LOG.log(Level.FINEST, "attempt to update replacement date on discontinued article {0} which is not allowed", new Object[] {article.getUniqueId()});
                    throw validationMessageService.generateValidationException("replacementDate", "article.replacementDate.change.discontinued");
                }
                updateReplacedByArticle(organizationUniqueId, articleAPI.getReplacedByArticles(), article, userAPI);
            } else {
                // this is a request to republish article
                if( article.getBasedOnProduct() != null && article.getBasedOnProduct().getStatus() == Product.Status.DISCONTINUED ) {
                    LOG.log(Level.FINEST, "attempt to republish for article {0} but based on product is DISCONTINUED which is not allowed", new Object[] {article.getUniqueId()});
                    throw validationMessageService.generateValidationException("replacementDate", "article.replacementDate.change.basedOnProductDiscontinued");
                }
                article.setStatus(Product.Status.PUBLISHED);
                if (articleAPI.getBasedOnProduct() != null) {
                    String basedOnProductStatus = articleAPI.getBasedOnProduct().getStatus();
                    String articleStatus = article.getStatus().toString();
                    article.setStatusOverridden(!basedOnProductStatus.equalsIgnoreCase(articleStatus));
                }
                article.setReplacementDate(null);
                article.setReplacedByArticles(null);
                article.setInactivateRowsOnReplacement(null);
            }
        } else {
            if( articleAPI.getReplacementDate() != null ) {
                final var replacementDate = new Date(articleAPI.getReplacementDate());
                doReplacementArticle(article, articleAPI.getReplacedByArticles(), articleAPI.getInactivateRowsOnReplacement(), replacementDate, organizationUniqueId, userAPI);
                if (discontinuedNow(replacementDate)) {
                    sendMailRegardingDiscontinuedArticlesOnAssortment.send(List.of(article.getUniqueId()));
                }
            }
        }
    }

    /**
     * Handle article replacement. If replacementdate is in the past, the article
     * is discontinued
     *
     * @param article article to handle replacement of
     * @param replacedByArticles list of articles to be replaced by, may be null
     * @param inactivateRows whether rows (gp and agreement) should be inactivated or not
     * @param replacementDate date of replacement
     * @param organizationUniqueId unique id of the organization
     */
    public void doReplacementArticle( Article article, List<ArticleAPI> replacedByArticles, Boolean inactivateRows, Date replacementDate, long organizationUniqueId, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "doReplacementArticle( organizationUniqueId: {0}, article->uniqueId: {1} )", new Object[] {organizationUniqueId, article.getUniqueId()});
        if(discontinuedNow(replacementDate)) {
            handleArticleDiscontinued(article, inactivateRows);
        }
        article.setReplacementDate(replacementDate);
        article.setInactivateRowsOnReplacement(inactivateRows);
        updateReplacedByArticle(organizationUniqueId, replacedByArticles, article, userAPI);
    }

    private boolean discontinuedNow(Date replacementDate) {
        final var dateToBeReplaced = replacementDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return dateToBeReplaced.isBefore(LocalDate.now()) || dateToBeReplaced.isEqual(LocalDate.now());
    }

    /**
     * Set article discontinued values. Also sets all rows for this article on
     * agreement and general pricelist to inactive
     *
     * @param article the article to update
     */
    public void handleArticleDiscontinued( Article article, Boolean inactivateRows ) {
        article.setStatus(Product.Status.DISCONTINUED);
        if( article.getBasedOnProduct() == null ) {
            article.setStatusOverridden(true);
        }
        else {
            article.setStatusOverridden(!Objects.equals(article.getBasedOnProduct().getStatus(), article.getStatus()));
        }
        copyInheritedValuesToArticle(article);
        if( inactivateRows != null && inactivateRows ) {
            agreementPricelistRowController.handleRowsWhereArticleIsDiscontinued(article);
            generalPricelistPricelistRowController.inactivateRowsByArticle(article);
        }
    }

    /**
     * An article cannot reinherit resource specific property values from a product.
     * If the article has overridden once, it cannot go back. (Really?)
     *
     * @param articleAPI
     * @param article
     * @param category
     * @param categorySpecificPropertys
     * @throws HjalpmedelstjanstenValidationException
     */
    public void setResourceSpecificProperties(ArticleAPI articleAPI, Article article, Category category, List<CategorySpecificProperty> categorySpecificPropertys) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setResourceSpecificProperties( article->uniqueId: {1} )", new Object[] {article.getUniqueId()});
        // add/update properties
        if( articleAPI.getCategoryPropertys() != null && !articleAPI.getCategoryPropertys().isEmpty() ) {
            if( article.getResourceSpecificPropertyValues() == null ) {
                article.setResourceSpecificPropertyValues(new ArrayList<>());
            }
            for( ResourceSpecificPropertyAPI resourceSpecificPropertyAPI : articleAPI.getCategoryPropertys() ) {
                // first make sure the property exists for the article category
                CategorySpecificProperty categorySpecificProperty = productController.categorySpecificPropertysContains(resourceSpecificPropertyAPI.getProperty().getId(), categorySpecificPropertys);
                if( categorySpecificProperty == null ) {
                    LOG.log(Level.WARNING, "attempt to set category specific property for article {0} but category: {1} of article does not have it", new Object[] {article.getUniqueId(), category.getUniqueId()});
                    throw validationMessageService.generateValidationException("categoryProperties", "article.categoryProperty.notExist");
                }
                LOG.log( Level.FINEST, "Check if category specific property: {0} is already answered", new Object[] {categorySpecificProperty.getUniqueId()});
                if( resourceSpecificPropertyAPI.getId() != null ) {
                    // previously answered question, value may be updated
                    ResourceSpecificPropertyValue resourceSpecificPropertyValue = productController.getResourceSpecificPropertyValue(article.getResourceSpecificPropertyValues(), resourceSpecificPropertyAPI.getId());
                    if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueTextField ) {
                        ResourceSpecificPropertyValueTextField resourceSpecificPropertyValueTextfield = (ResourceSpecificPropertyValueTextField) resourceSpecificPropertyValue;
                        resourceSpecificPropertyValueTextfield.setValue(resourceSpecificPropertyAPI.getTextValue());
                    } else if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueDecimal ) {
                        ResourceSpecificPropertyValueDecimal resourceSpecificPropertyValueDecimal = (ResourceSpecificPropertyValueDecimal) resourceSpecificPropertyValue;
                        resourceSpecificPropertyValueDecimal.setValue(resourceSpecificPropertyAPI.getDecimalValue());
                    } else if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueInterval ) {
                        ResourceSpecificPropertyValueInterval resourceSpecificPropertyValueInterval = (ResourceSpecificPropertyValueInterval) resourceSpecificPropertyValue;
                        resourceSpecificPropertyValueInterval.setFromValue(resourceSpecificPropertyAPI.getIntervalFromValue());
                        resourceSpecificPropertyValueInterval.setToValue(resourceSpecificPropertyAPI.getIntervalToValue());
                    } else if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueValueListSingle ) {
                        CategorySpecificPropertyListValue categorySpecificPropertyListValue = resourceSpecificPropertyAPI.getSingleListValue() == null ? null: categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyAPI.getSingleListValue());
                        ResourceSpecificPropertyValueValueListSingle resourceSpecificPropertyValueValueListSingle = (ResourceSpecificPropertyValueValueListSingle) resourceSpecificPropertyValue;
                        if (categorySpecificPropertyListValue != null) { //HJAL-2428 only set the value if != null
                            resourceSpecificPropertyValueValueListSingle.setValue(categorySpecificPropertyListValue);
                        }
                        else{//HJAL-2428 else delete the row
                            article.getResourceSpecificPropertyValues().remove(resourceSpecificPropertyValueValueListSingle);
                        }
                    } else if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueValueListMultiple ) {
                        List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues = new ArrayList<>();
                        ResourceSpecificPropertyValueValueListMultiple resourceSpecificPropertyValueValueListMultiple = (ResourceSpecificPropertyValueValueListMultiple) resourceSpecificPropertyValue;
                        if( resourceSpecificPropertyAPI.getMultipleListValue() != null ) {
                            for( Long listValue : resourceSpecificPropertyAPI.getMultipleListValue() ) {
                                CategorySpecificPropertyListValue categorySpecificPropertyListValue = categoryController.getCategorySpecificPropertyListValue(listValue);
                                categorySpecificPropertyListValues.add(categorySpecificPropertyListValue);
                            }
                        }
                        resourceSpecificPropertyValueValueListMultiple.setValues(categorySpecificPropertyListValues);
                    }
                } else {
                    // make sure property hasn't already been answered
                    ResourceSpecificPropertyValue alreadyAnsweredResourceSpecificPropertyValue = productController.getResourceSpecificPropertyValueByCategorySpecificProperty(article.getResourceSpecificPropertyValues(), resourceSpecificPropertyAPI.getProperty().getId());
                    if( alreadyAnsweredResourceSpecificPropertyValue != null ) {
                        LOG.log(Level.WARNING, "attempt to set category specific property: {0} for article {1} but property has already been set in request ", new Object[] {categorySpecificProperty.getUniqueId(), article.getUniqueId()});
                        throw validationMessageService.generateValidationException("categoryProperties", "article.categoryProperty.alreadySet");
                    }
                    // only save new property if a value is set and the value differs
                    // from that of the product (if product has the property)
                    ResourceSpecificPropertyValue productResourceSpecificPropertyValue = productController.getResourceSpecificPropertyValueByCategorySpecificProperty(article.getBasedOnProduct().getResourceSpecificPropertyValues(), resourceSpecificPropertyAPI.getProperty().getId());
                    boolean saveProperty = true;
                    if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.TEXTFIELD ) {
                        String productValue = productResourceSpecificPropertyValue == null ? null: (( ResourceSpecificPropertyValueTextField ) productResourceSpecificPropertyValue).getValue();
                        String articleValue = resourceSpecificPropertyAPI.getTextValue();
                        if( !Objects.equals(productValue, articleValue) ) {
                            ResourceSpecificPropertyValueTextField resourceSpecificPropertyValueTextfield = new ResourceSpecificPropertyValueTextField();
                            resourceSpecificPropertyValueTextfield.setArticle(article);
                            resourceSpecificPropertyValueTextfield.setCategorySpecificProperty(categorySpecificProperty);
                            resourceSpecificPropertyValueTextfield.setValue(resourceSpecificPropertyAPI.getTextValue());
                            em.persist(resourceSpecificPropertyValueTextfield);
                            article.getResourceSpecificPropertyValues().add(resourceSpecificPropertyValueTextfield);
                        }
                    } else if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.DECIMAL ) {
                        Double productValue = productResourceSpecificPropertyValue == null ? null: (( ResourceSpecificPropertyValueDecimal ) productResourceSpecificPropertyValue).getValue();
                        Double articleValue = resourceSpecificPropertyAPI.getDecimalValue();
                        if( !Objects.equals(productValue, articleValue) ) {
                            ResourceSpecificPropertyValueDecimal resourceSpecificPropertyValueDecimal = new ResourceSpecificPropertyValueDecimal();
                            resourceSpecificPropertyValueDecimal.setCategorySpecificProperty(categorySpecificProperty);
                            resourceSpecificPropertyValueDecimal.setArticle(article);
                            resourceSpecificPropertyValueDecimal.setValue(resourceSpecificPropertyAPI.getDecimalValue());
                            em.persist(resourceSpecificPropertyValueDecimal);
                            article.getResourceSpecificPropertyValues().add(resourceSpecificPropertyValueDecimal);
                        }
                    } else if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.INTERVAL ) {
                        Double productFromValue = productResourceSpecificPropertyValue == null ? null: (( ResourceSpecificPropertyValueInterval ) productResourceSpecificPropertyValue).getFromValue();
                        Double productToValue = productResourceSpecificPropertyValue == null ? null: (( ResourceSpecificPropertyValueInterval ) productResourceSpecificPropertyValue).getToValue();
                        Double articleFromValue = resourceSpecificPropertyAPI.getIntervalFromValue();
                        Double articleToValue = resourceSpecificPropertyAPI.getIntervalToValue();
                        if( !Objects.equals(productFromValue, articleFromValue) || !Objects.equals(productToValue, articleToValue) ) {
                            ResourceSpecificPropertyValueInterval resourceSpecificPropertyValueInterval = new ResourceSpecificPropertyValueInterval();
                            resourceSpecificPropertyValueInterval.setCategorySpecificProperty(categorySpecificProperty);
                            resourceSpecificPropertyValueInterval.setArticle(article);
                            resourceSpecificPropertyValueInterval.setFromValue(resourceSpecificPropertyAPI.getIntervalFromValue());
                            resourceSpecificPropertyValueInterval.setToValue(resourceSpecificPropertyAPI.getIntervalToValue());
                            em.persist(resourceSpecificPropertyValueInterval);
                            article.getResourceSpecificPropertyValues().add(resourceSpecificPropertyValueInterval);
                        }
                    } else if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_SINGLE ) {
                        //HJAL-2428
                        //Long productValue = productResourceSpecificPropertyValue == null ? null : (( ResourceSpecificPropertyValueValueListSingle ) productResourceSpecificPropertyValue).getValue().getUniqueId();
                        Long productValue = productResourceSpecificPropertyValue == null ? null : (( ResourceSpecificPropertyValueValueListSingle ) productResourceSpecificPropertyValue).getValue() == null ? null : (( ResourceSpecificPropertyValueValueListSingle ) productResourceSpecificPropertyValue).getValue().getUniqueId();
                        Long articleValue = resourceSpecificPropertyAPI.getSingleListValue();
                        if( !Objects.equals(productValue, articleValue) ) {
                            CategorySpecificPropertyListValue categorySpecificPropertyListValue = resourceSpecificPropertyAPI.getSingleListValue() == null ? null: categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyAPI.getSingleListValue());
                            ResourceSpecificPropertyValueValueListSingle resourceSpecificPropertyValueValueListSingle = new ResourceSpecificPropertyValueValueListSingle();
                            resourceSpecificPropertyValueValueListSingle.setCategorySpecificProperty(categorySpecificProperty);
                            resourceSpecificPropertyValueValueListSingle.setArticle(article);
                            resourceSpecificPropertyValueValueListSingle.setValue(categorySpecificPropertyListValue);
                            em.persist(resourceSpecificPropertyValueValueListSingle);
                            article.getResourceSpecificPropertyValues().add(resourceSpecificPropertyValueValueListSingle);
                        }
                    } else if( resourceSpecificPropertyAPI.getMultipleListValue() != null ) {
                        List<CategorySpecificPropertyListValue> productListValues = productResourceSpecificPropertyValue == null ? null: (( ResourceSpecificPropertyValueValueListMultiple ) productResourceSpecificPropertyValue).getValues();
                        List<Long> articleListValues = resourceSpecificPropertyAPI.getMultipleListValue();
                        int productNumberOfValues = productListValues == null ? 0: productListValues.size();
                        int articleNumberOfValues = articleListValues.size();
                        if( productNumberOfValues != articleNumberOfValues || !articleValuesAreSameAsProduct(productListValues, articleListValues) ) {
                            List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues = new ArrayList<>();
                            ResourceSpecificPropertyValueValueListMultiple resourceSpecificPropertyValueValueListMultiple = new ResourceSpecificPropertyValueValueListMultiple();
                            resourceSpecificPropertyValueValueListMultiple.setCategorySpecificProperty(categorySpecificProperty);
                            resourceSpecificPropertyValueValueListMultiple.setArticle(article);
                            for( Long listValue : resourceSpecificPropertyAPI.getMultipleListValue() ) {
                                CategorySpecificPropertyListValue categorySpecificPropertyListValue = categoryController.getCategorySpecificPropertyListValue(listValue);
                                categorySpecificPropertyListValues.add(categorySpecificPropertyListValue);
                            }
                            resourceSpecificPropertyValueValueListMultiple.setValues(categorySpecificPropertyListValues);
                            em.persist(resourceSpecificPropertyValueValueListMultiple);
                            article.getResourceSpecificPropertyValues().add(resourceSpecificPropertyValueValueListMultiple);
                        }
                    }
                }
            }
        }
    }

    /**
     * When an article is set to DISCONTINUED it should keep the "state" of it's
     * variables as was at that moment. If the article is based on a Product then
     * all values that are inherited must be copied down to the article since even
     * if the Product changes it's values, DISCONTINUED articles should no longer
     * inherit the new values.
     *
     * @param article
     */
    public void copyInheritedValuesToArticle(Article article) {
        if( article.getStatus() == Product.Status.DISCONTINUED && article.getBasedOnProduct() != null ) {
            Product product = article.getBasedOnProduct();
            article.setCategory(product.getCategory());

            // ce
            if( !article.isCeDirectiveOverridden() ) {
                article.setCeDirective(product.getCeDirective());
            }
            if( !article.isCeStandardOverridden() ) {
                article.setCeStandard(product.getCeStandard());
            }
            if( !article.isCeMarkedOverridden() ) {
                article.setCeMarked(product.isCeMarked());
            }

            // manufacturer
            if( !article.isManufacturerArticleNumberOverridden() ) {
                article.setManufacturerArticleNumber(product.getManufacturerProductNumber());
            }
            if( !article.isManufacturerOverridden() ) {
                article.setManufacturer(product.getManufacturer());
            }
            if( !article.isManufacturerElectronicAddressOverridden() ) {
                article.setManufacturerElectronicAddress(product.getManufacturerElectronicAddress());
            }
            if( !article.isTrademarkOverridden() ) {
                article.setTrademark(product.getTrademark());
            }

            // preventive maintenance
            if( !article.isPreventiveMaintenanceDescriptionOverridden() ) {
                article.setPreventiveMaintenanceDescription(product.getPreventiveMaintenanceDescription());
            }
            if( !article.isPreventiveMaintenanceNumberOfDaysOverridden()) {
                article.setPreventiveMaintenanceNumberOfDays(product.getPreventiveMaintenanceNumberOfDays());
            }
            if( !article.isPreventiveMaintenanceValidFromOverridden()) {
                article.setPreventiveMaintenanceValidFrom(product.getPreventiveMaintenanceValidFrom());
            }

            // order information
            if( !article.isOrderInformationOverridden() ) {
                article.setOrderUnit(product.getOrderUnit());
                article.setArticleQuantityInOuterPackage(product.getArticleQuantityInOuterPackage());
                article.setArticleQuantityInOuterPackageUnit(product.getArticleQuantityInOuterPackageUnit());
                article.setPackageContent(product.getPackageContent());
                article.setPackageContentUnit(product.getPackageContentUnit());
                article.setPackageLevelBase(product.getPackageLevelBase());
                article.setPackageLevelMiddle(product.getPackageLevelMiddle());
                article.setPackageLevelTop(product.getPackageLevelTop());
            }

            // supplemental info
            if (!article.isSupplementedInformationOverridden()) {
                article.setSupplementedInformation(product.getSupplementedInformation());
            }
        }
    }

    /**
     * When an article is set to PUBLISHED after being DISCONTINUED it should
     * re-inherit values which were uninherited when the article was set to
     * DISCONTINUED
     *
     * @param article
     */
    public void reInheritProductValuesToArticle(Article article) {
        if( article.getStatus() == Product.Status.PUBLISHED && article.getBasedOnProduct() != null ) {
            Product product = article.getBasedOnProduct();
            article.setCategory(null);

            // ce
            if( article.isCeDirectiveOverridden() ) {
                article.setCeDirective(null);
            }
            if( article.isCeStandardOverridden() ) {
                article.setCeStandard(null);
            }
            if( article.isCeMarkedOverridden() ) {
                article.setCeMarked(product.isCeMarked());
            }

            // manufacturer
            if( article.isManufacturerArticleNumberOverridden() ) {
                article.setManufacturerArticleNumber(null);
            }
            if( article.isManufacturerOverridden() ) {
                article.setManufacturer(null);
            }
            if( article.isManufacturerElectronicAddressOverridden() ) {
                article.setManufacturerElectronicAddress(null);
            }
            if( article.isTrademarkOverridden() ) {
                article.setTrademark(null);
            }


            // preventive maintenance
            if( article.isPreventiveMaintenanceDescriptionOverridden() ) {
                article.setPreventiveMaintenanceDescription(null);
            }
            if( article.isPreventiveMaintenanceNumberOfDaysOverridden()) {
                article.setPreventiveMaintenanceNumberOfDays(null);
            }
            if( article.isPreventiveMaintenanceValidFromOverridden()) {
                article.setPreventiveMaintenanceValidFrom(null);
            }

            // order information
            if( article.isOrderInformationOverridden() ) {
                article.setOrderUnit(null);
                article.setArticleQuantityInOuterPackage(null);
                article.setArticleQuantityInOuterPackageUnit(null);
                article.setPackageContent(null);
                article.setPackageContentUnit(null);
                article.setPackageLevelBase(null);
                article.setPackageLevelMiddle(null);
                article.setPackageLevelTop(null);
            }

        }
    }

    /**
     * Update replaced by article
     *
     * @param organizationUniqueId unique id of the organization
     * @param replacedByArticleAPIs articles to be replaced by, may be null
     * @param article article to replace
     * @throws HjalpmedelstjanstenValidationException if validation fails e.g.
     * replacement article does not exist
     */
    private void updateReplacedByArticle(long organizationUniqueId, List<ArticleAPI> replacedByArticleAPIs, Article article, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        if( replacedByArticleAPIs != null ) {
            article.setReplacedByArticles(new ArrayList());
            for( ArticleAPI articleAPI : replacedByArticleAPIs ) {
                Article replacementArticle = getArticle(organizationUniqueId, articleAPI.getId(), userAPI);
                if( replacementArticle == null ) {
                    LOG.log(Level.FINEST, "cannot set replacement article for article {0} since it does not exist", new Object[] {article.getUniqueId()});
                    throw validationMessageService.generateValidationException("replacedByArticle", "article.replacedBy.notExist");
                }
                article.getReplacedByArticles().add(replacementArticle);
            }
        } else {
            if( article.getReplacedByArticles() != null ) {
                article.setReplacedByArticles(null);
            }
        }
    }

    /**
     * Set directive and standard on article
     *
     * @param articleAPI user supplied values
     * @param article the article to set info on
     * @throws HjalpmedelstjanstenValidationException
     */
    public void setCE( ArticleAPI articleAPI, Article article ) throws HjalpmedelstjanstenValidationException {
        if( article.getBasedOnProduct() == null ) {
            article.setCeMarked(articleAPI.isCeMarked());
        } else {
            if( articleAPI.isCeMarked() != article.getBasedOnProduct().isCeMarked() ) {
                article.setCeMarked(articleAPI.isCeMarked());
                article.setCeMarkedOverridden(true);
            } else {
                article.setCeMarkedOverridden(false);
            }
        }
        setDirective(articleAPI, article);
        setStandard(articleAPI, article);
    }

    /**
     * Check if directive is to be updated/overridden
     *
     * @param articleAPI user supplied values
     * @param article the article to set info on
     * @throws HjalpmedelstjanstenValidationException
     */
    private void setDirective( ArticleAPI articleAPI, Article article ) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setDirective(...)");
        if( article.getBasedOnProduct() == null ) {
            if(articleAPI.getCeDirective() != null ) {
                getAndSetDirective(articleAPI, article);
            } else {
                article.setCeDirective(null);
            }
        } else {
            if( articleAPI.getCeDirective() != null ) {
                if( article.getBasedOnProduct().getCeDirective() != null && article.getBasedOnProduct().getCeDirective().getUniqueId().equals(articleAPI.getCeDirective().getId())  ) {
                    getAndSetDirective(articleAPI, article);
                    article.setCeDirectiveOverridden(false);
                } else {
                    getAndSetDirective(articleAPI, article);
                    article.setCeDirectiveOverridden(true);
                }
            } else {
                if( article.getBasedOnProduct().getCeDirective() == null ) {
                    article.setCeDirectiveOverridden(false);
                } else {
                    article.setCeDirective(null);
                    article.setCeDirectiveOverridden(true);
                }
            }
        }
    }

    /**
     * Get directive and set it on the article
     *
     * @param articleAPI user supplied values
     * @param article the article to set info on
     * @throws HjalpmedelstjanstenValidationException if the directive does not exist
     */
    private void getAndSetDirective( ArticleAPI articleAPI, Article article ) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "getAndSetDirective(...)");
        CVCEDirective directive = ceController.findCEDirectiveById(articleAPI.getCeDirective().getId());
        if( directive == null ) {
            LOG.log(Level.FINEST, "directive {0} does not exist", new Object[] {articleAPI.getCeDirective().getId()});
            throw validationMessageService.generateValidationException("ceDirective", "article.directive.notExists");
        }
        article.setCeDirective(directive);
    }

    /**
     * Sets supplemented information. Marked as overridden if not the same as on product.
     *
     * @param articleAPI user supplied values
     * @param article    the product to set info on
     */
    public void setSupplementedInformation( ArticleAPI articleAPI, Article article ) {
        LOG.log(Level.FINEST, "setSupplementedInformation(...)");

        if( article.getBasedOnProduct() == null ) {
            article.setSupplementedInformationOverridden(false); // unnecessary but for clarity
            article.setSupplementedInformation(articleAPI.getSupplementedInformation());
            article.setColorOverridden(true);
            article.setColor(articleAPI.getColor());
        } else {
            // supplemented information
            String productSupplementedInformation = article.getBasedOnProduct().getSupplementedInformation();
            String articleAPISupplementedInformation = articleAPI.getSupplementedInformation();
            String fixedProductSupplementedInformation = (productSupplementedInformation == null || productSupplementedInformation.trim().isEmpty()) ? null : productSupplementedInformation.toUpperCase().trim();
            String fixedArticleAPISupplementedInformation = (articleAPISupplementedInformation == null || articleAPISupplementedInformation.trim().isEmpty()) ? null : articleAPISupplementedInformation.toUpperCase().trim();
            //if( !Objects.equals(productSupplementedInformation, articleAPISupplementedInformation) ) {
            if( !Objects.equals(fixedProductSupplementedInformation, fixedArticleAPISupplementedInformation) ) {
                article.setSupplementedInformationOverridden(true);
                article.setSupplementedInformation(articleAPISupplementedInformation);
            } else {
                article.setSupplementedInformationOverridden(false); // unnecessary but for clarity
            }

            // color
            String productColor = article.getBasedOnProduct().getColor();
            String articleAPIColor = articleAPI.getColor();
            String fixedProductColor = (productColor == null || productColor.trim().isEmpty()) ? null : productColor.toUpperCase().trim();
            String fixedArticleAPIColor = (articleAPIColor == null || articleAPIColor.trim().isEmpty()) ? null : articleAPIColor.toUpperCase().trim();
            //if( !Objects.equals(productColor.trim().toUpperCase(), articleAPIColor.trim().toUpperCase()) ) {
            if( !Objects.equals(fixedProductColor, fixedArticleAPIColor) ) {
                article.setColorOverridden(true);
                article.setColor(articleAPIColor);
            } else {
                article.setColorOverridden(false); // unnecessary but for clarity
            }
        }

    }

    /**
     * Check if standard is to be updated/overridden
     *
     * @param articleAPI user supplied values
     * @param article the article to set info on
     * @throws HjalpmedelstjanstenValidationException
     */
    private void setStandard( ArticleAPI articleAPI, Article article ) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setStandard(...)");
        if( article.getBasedOnProduct() == null ) {
            if(articleAPI.getCeStandard() != null ) {
                getAndSetStandard(articleAPI, article);
            } else {
                article.setCeStandard(null);
            }
        } else {
            if( articleAPI.getCeStandard() != null ) {
                if( article.getBasedOnProduct().getCeStandard() != null && article.getBasedOnProduct().getCeStandard().getUniqueId().equals(articleAPI.getCeStandard().getId())  ) {
                    getAndSetStandard(articleAPI, article);
                    article.setCeStandardOverridden(false);
                } else {
                    getAndSetStandard(articleAPI, article);
                    article.setCeStandardOverridden(true);
                }
            } else {
                if( article.getBasedOnProduct().getCeStandard() == null ) {
                    article.setCeStandardOverridden(false);
                } else {
                    article.setCeStandard(null);
                    article.setCeStandardOverridden(true);
                }
            }
        }
    }

    /**
     * Get standard and set it on the article
     *
     * @param articleAPI user supplied values
     * @param article the article to set info on
     * @throws HjalpmedelstjanstenValidationException if the directive does not exist
     */
    private void getAndSetStandard( ArticleAPI articleAPI, Article article ) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "getAndSetStandard(...)");
        CVCEStandard standard = ceController.findCEStandardById(articleAPI.getCeStandard().getId());
        if( standard == null ) {
            LOG.log(Level.FINEST, "standard {0} does not exist", new Object[] {articleAPI.getCeStandard().getId()});
            throw validationMessageService.generateValidationException("ceStandard", "article.standard.notExists");
        }
        article.setCeStandard(standard);
    }

    /**
     * Set manufacturer information on article (multiple fields) and perhaps
     * overridden values
     *
     * @param articleAPI user supplied values
     * @param article the product to set info on
     */
    public void setManufacturer(ArticleAPI articleAPI, Article article) {
        if( article.getBasedOnProduct() == null ) {
            article.setTrademark(articleAPI.getTrademark());
            article.setManufacturer(articleAPI.getManufacturer());
            article.setManufacturerArticleNumber(articleAPI.getManufacturerArticleNumber());
            article.setManufacturerElectronicAddress(ElectronicAddressMapper.map(articleAPI.getManufacturerElectronicAddress()));
        } else {
            // trademark
            String articleTrademark = articleAPI.getTrademark() == null ? "": articleAPI.getTrademark();
            String productTrademark = article.getBasedOnProduct().getTrademark() == null ? "": article.getBasedOnProduct().getTrademark();
            if( !articleTrademark.trim().equalsIgnoreCase(productTrademark.trim())) {
                article.setTrademark(articleAPI.getTrademark());
                article.setTrademarkOverridden(true);
            } else {
                article.setTrademarkOverridden(false);
            }

            // manufacturer
            String articleManufacturer = articleAPI.getManufacturer() == null ? "": articleAPI.getManufacturer();
            String productManufacturer = article.getBasedOnProduct().getManufacturer() == null ? "": article.getBasedOnProduct().getManufacturer();
            if( !articleManufacturer.trim().equalsIgnoreCase(productManufacturer.trim()) ) {
                article.setManufacturer(articleAPI.getManufacturer());
                article.setManufacturerOverridden(true);
            } else {
                article.setManufacturerOverridden(false);
            }

            // manufacturer article number
            String articleManufacturerArticleNumber = articleAPI.getManufacturerArticleNumber() == null ? "": articleAPI.getManufacturerArticleNumber();
            String productManufacturerProductNumber = article.getBasedOnProduct().getManufacturerProductNumber() == null ? "": article.getBasedOnProduct().getManufacturerProductNumber();
            if( !articleManufacturerArticleNumber.trim().equalsIgnoreCase(productManufacturerProductNumber.trim()) ) {
                article.setManufacturerArticleNumber(articleAPI.getManufacturerArticleNumber());
                article.setManufacturerArticleNumberOverridden(true);
            } else {
                article.setManufacturerArticleNumberOverridden(false);
            }

            // manufacturer electronic address
            String articleWebAddress = articleAPI.getManufacturerElectronicAddress() == null || articleAPI.getManufacturerElectronicAddress().getWeb() == null ? "": articleAPI.getManufacturerElectronicAddress().getWeb();
            String productWebAddress = article.getBasedOnProduct().getManufacturerElectronicAddress() == null || article.getBasedOnProduct().getManufacturerElectronicAddress().getWeb() == null ? "": article.getBasedOnProduct().getManufacturerElectronicAddress().getWeb();
            if( !articleWebAddress.trim().equalsIgnoreCase(productWebAddress.trim()) ) {
                article.setManufacturerElectronicAddress(ElectronicAddressMapper.map(articleAPI.getManufacturerElectronicAddress()));
                article.setManufacturerElectronicAddressOverridden(true);
            } else {
                article.setManufacturerElectronicAddressOverridden(false);
            }
        }
    }

    /**
     * Set preventive maintenance information on article (multiple fields) and perhaps
     * overridden values
     *
     * @param articleAPI user supplied values
     * @param article the product to set info on
     */
    public void setPreventiveMaintenance(ArticleAPI articleAPI, Article article, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        if( article.getBasedOnProduct() == null ) {
            setPreventiveMaintenanceValidFrom(articleAPI, article, userAPI);
            article.setPreventiveMaintenanceDescription(articleAPI.getPreventiveMaintenanceDescription());
            article.setPreventiveMaintenanceNumberOfDays(articleAPI.getPreventiveMaintenanceNumberOfDays());
        } else {
            // preventive maintenance description
            String articlePreventiveMaintenanceDescription = articleAPI.getPreventiveMaintenanceDescription() == null ? "": articleAPI.getPreventiveMaintenanceDescription();
            String productPreventiveMaintenanceDescription = article.getBasedOnProduct().getPreventiveMaintenanceDescription() == null ? "": article.getBasedOnProduct().getPreventiveMaintenanceDescription();
            if( !articlePreventiveMaintenanceDescription.equals(productPreventiveMaintenanceDescription) ) {
                article.setPreventiveMaintenanceDescription(articleAPI.getPreventiveMaintenanceDescription());
                article.setPreventiveMaintenanceDescriptionOverridden(true);
            } else {
                article.setPreventiveMaintenanceDescriptionOverridden(false);
            }

            // preventive maintenance number of days
            int articlePreventiveMaintenanceNumberOfDays = articleAPI.getPreventiveMaintenanceNumberOfDays() == null ? -1: articleAPI.getPreventiveMaintenanceNumberOfDays();
            int productPreventiveMaintenanceNumberOfDays = article.getBasedOnProduct().getPreventiveMaintenanceNumberOfDays() == null ? -1: article.getBasedOnProduct().getPreventiveMaintenanceNumberOfDays();
            if( articlePreventiveMaintenanceNumberOfDays != productPreventiveMaintenanceNumberOfDays ) {
                article.setPreventiveMaintenanceNumberOfDays(articleAPI.getPreventiveMaintenanceNumberOfDays());
                article.setPreventiveMaintenanceNumberOfDaysOverridden(true);
            } else {
                article.setPreventiveMaintenanceNumberOfDaysOverridden(false);
            }

            // preventive maintenance valid from
            String articlePreventiveMaintenanceValidFrom = articleAPI.getPreventiveMaintenanceValidFrom() == null ? "": articleAPI.getPreventiveMaintenanceValidFrom().getCode();
            String productPreventiveMaintenanceValidFrom = article.getBasedOnProduct().getPreventiveMaintenanceValidFrom() == null ? "": article.getBasedOnProduct().getPreventiveMaintenanceValidFrom().getCode();
            if( !articlePreventiveMaintenanceValidFrom.equals(productPreventiveMaintenanceValidFrom) ) {
                setPreventiveMaintenanceValidFrom(articleAPI, article, userAPI);
                article.setPreventiveMaintenanceValidFromOverridden(true);
            } else {
                article.setPreventiveMaintenanceValidFromOverridden(false);
            }
        }
    }

    /**
     * Set extended categories on the given article
     *
     * @param articleAPI user supplied values
     * @param article the article to update
     * @throws HjalpmedelstjanstenValidationException
     */
    public void setExtendedCategories(ArticleAPI articleAPI, Article article) throws HjalpmedelstjanstenValidationException {
        if( articleAPI.getExtendedCategories() != null && !articleAPI.getExtendedCategories().isEmpty() ) {
            Category category = article.getBasedOnProduct() == null ? article.getCategory(): article.getBasedOnProduct().getCategory();
            if( category.getCode() == null || category.getCode().isEmpty() ) {
                LOG.log(Level.WARNING, "article {0} main category {1} has no code and extended categories can therefore not be set", new Object[] {article.getUniqueId(), category.getUniqueId()});
                throw validationMessageService.generateValidationException("extendedCategories", "article.extendedCategory.categoryNoCode");
            }
            List<Category> extendedCategories = new ArrayList<>();
            for( CategoryAPI categoryAPI : articleAPI.getExtendedCategories()) {
                List<Category> extendedCategoriesSearchResult = categoryController.getChildlessById(categoryAPI.getId());
                if( extendedCategoriesSearchResult == null || extendedCategoriesSearchResult.isEmpty() ) {
                    LOG.log(Level.FINEST, "category {0} does not exist or is not a leaf node", new Object[] {categoryAPI.getId()});
                    throw validationMessageService.generateValidationException("extendedCategories", "article.extendedCategory.notExists");
                }
                extendedCategories.add(extendedCategoriesSearchResult.get(0));
            }
            article.setExtendedCategories(extendedCategories);
        }
    }

    /**
     * Update category on the given article. Only Articles with article type T,
     * R, I and Tj (no code) can change and only to those article types (no code)
     *
     * @param articleAPI user supplied values
     * @param article the article to update category on.
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    private void updateCategory(ArticleAPI articleAPI, Article article) throws HjalpmedelstjanstenValidationException {
        if( article.getBasedOnProduct() == null ) {
            if( !articleAPI.getCategory().getId().equals(article.getCategory().getUniqueId()) ) {
                if( article.getCategory().getCode() != null ) {
                    LOG.log(Level.FINEST, "article {0} has code and can therefore not change category", new Object[] {article.getUniqueId()});
                    throw validationMessageService.generateValidationException("category", "article.category.changeInvalid");
                }
                if( article.getCategory().getArticleType() == Article.Type.H ) {
                    LOG.log(Level.FINEST, "article {0} has type H and can therefore not change category", new Object[] {article.getUniqueId()});
                    throw validationMessageService.generateValidationException("category", "article.category.changeInvalid");
                }
                // category can only be changed to T, R, I and Tj (no code)
                List<Category> categories = categoryController.getChildlessById(articleAPI.getCategory().getId());
                if( categories == null || categories.isEmpty() ) {
                    LOG.log(Level.FINEST, "category {0} does not exist or is not a leaf node", new Object[] {articleAPI.getCategory().getId()});
                    throw validationMessageService.generateValidationException("category", "article.category.notExists");
                }
                Category category = categories.get(0);
                if( category.getCode() != null ) {
                    LOG.log(Level.FINEST, "category {0} has code and can therefore not be the new category for article {1}", new Object[] {category.getUniqueId(), article.getUniqueId()});
                    throw validationMessageService.generateValidationException("category", "article.category.changeInvalid");
                }
                if( article.getCategory().getArticleType() == Article.Type.H ) { //ska det inte stå "if( articleAPI.getCategory().getArticleType() == Article.Type.H )" ?
                    LOG.log(Level.FINEST, "category {0} has type H and can therefore not be the new category for article {1}", new Object[] {category.getUniqueId(), article.getUniqueId()});
                    throw validationMessageService.generateValidationException("category", "article.category.changeInvalid");
                }
                article.setCategory(category);
            }
        }
    }

    /**
     * Adds fits to products and fits to articles.
     * * Articles with category of articleType H cannot have any fits to at all
     * * Articles with category code set cannot have any fits to at all
     * * Articles cannot fit to products of type I, T, Tj
     * * Only articles of type R can fit to products of type R
     *
     * @param organizationUniqueId
     * @param articleAPI
     * @param article
     * @throws HjalpmedelstjanstenValidationException if the rules for which products/articles that can be fitted is violated
     */
    public void setFitsTo(long organizationUniqueId, ArticleAPI articleAPI, Article article, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        List<Product> products = validateAndGetFitsToProducts(organizationUniqueId, articleAPI, article, userAPI);
        List<Article> articles = validateAndGetFitsToArticles(organizationUniqueId, articleAPI, article, userAPI);

        // add fits to product relations
        article.setFitsToProducts(products);

        // add fits to article relations
        article.setFitsToArticles(articles);
    }

    /**
     * Validates that the user supplied list of products that fit to the article
     * is correct. Articles of type H cannot fit to anything.
     * Articles with a code cannot fit to anything.
     *
     * @param organizationUniqueId unique id of the organization
     * @param articleAPI user supplied data
     * @return a list of <code>Product</code> entities or null if no products are available.
     * @throws HjalpmedelstjanstenValidationException
     */
    private List<Product> validateAndGetFitsToProducts(long organizationUniqueId, ArticleAPI articleAPI, Article article, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        Category category = article.getBasedOnProduct() == null ? article.getCategory(): article.getBasedOnProduct().getCategory();
        List<Product> products = new ArrayList<>();
        if( !isEmptyList(articleAPI.getFitsToProducts())) {
            if( category.getCode() != null && !category.getCode().isEmpty() ) {
                // if any fits to is set, then that is an error
                LOG.log(Level.FINEST, "article {0} cannot have fitsToProducts since it has category with code", new Object[] {article.getUniqueId()});
                throw validationMessageService.generateValidationException("fitsToProducts", "article.product.isoCode.fitsTo.notNull");
            } else if( category.getArticleType() == Article.Type.H ) {
                // articles with article type H cannot fit to anything
                LOG.log(Level.FINEST, "article {0} cannot have fitsToProducts since it has category with article type H", new Object[] {article.getUniqueId()});
                throw validationMessageService.generateValidationException("fitsToProducts", "article.product.fitsTo.articleTypeH");
            } else {
                for( ProductAPI productAPI : articleAPI.getFitsToProducts() ) {
                    Product fitsToProduct = productController.getProduct(organizationUniqueId, productAPI.getId(), userAPI);
                    if( fitsToProduct == null ) {
                        LOG.log(Level.INFO, "product {0} does not exist", new Object[] {productAPI.getId()});
                        throw validationMessageService.generateValidationException("fitsToProducts", "article.product.fitsTo.notExists");
                    }
                    // products can only be of article type H or T with an iso code
                    // this is limited by other code, but we check here anyway.
                    Category fitsToProductCategory = fitsToProduct.getCategory();
                    Article.Type fitsToProductArticleType = fitsToProductCategory.getArticleType();
                    if( fitsToProductCategory.getCode() == null || fitsToProductCategory.getCode().isEmpty() ) {
                        // this is an invalid state, a product must have a code
                        LOG.log(Level.WARNING, "fits to product {0} has no code, which is invalid and therefore cannot fit to article: {1}", new Object[] {fitsToProduct.getUniqueId(), article.getUniqueId()});
                        throw validationMessageService.generateValidationException("fitsToProducts", "article.product.fitsTo.noCode", fitsToProduct.getProductName());
                    }
                    if( fitsToProductArticleType == Article.Type.I ||
                            fitsToProductArticleType == Article.Type.R ||
                            fitsToProductArticleType == Article.Type.Tj ) {
                        // not ok
                        LOG.log(Level.WARNING, "fits to product {0} is of invalid article type: {1} and cannot fit to article: {2}", new Object[] {fitsToProduct.getUniqueId(), fitsToProductArticleType, article.getUniqueId()});
                        throw validationMessageService.generateValidationException("fitsToProducts", "article.product.fitsTo.productWrongType");
                    }
                    products.add(fitsToProduct);
                }
            }
        }
        return products.isEmpty() ? null: products;
    }

    /**
     * Validates that the user supplied list of articles that fit to the article
     * is correct.
     *
     * @param organizationUniqueId unique id of the organization
     * @param articleAPI user supplied data
     * @return a list of <code>Article</code> entities or null if no articles are available.
     * @throws HjalpmedelstjanstenValidationException
     */
    private List<Article> validateAndGetFitsToArticles(long organizationUniqueId, ArticleAPI articleAPI, Article article, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        Category category = article.getBasedOnProduct() == null ? article.getCategory(): article.getBasedOnProduct().getCategory();
        List<Article> articles = new ArrayList<>();
        if( !isEmptyList(articleAPI.getFitsToArticles()) ) {
            if( category.getCode() != null && !category.getCode().isEmpty() ) {
                throw validationMessageService.generateValidationException("fitsToArticles", "article.article.isoCode.fitsTo.notNull");
            } else if( category.getArticleType() == Article.Type.H ) {
                // articles with article type H cannot fit to anything
                LOG.log(Level.FINEST, "article {0} cannot have fitsToProducts since it has category with article type H", new Object[] {article.getUniqueId()});
                throw validationMessageService.generateValidationException("fitsToProducts", "article.article.fitsTo.articleTypeH");
            } else {
                for( ArticleAPI fitsToArticleAPI : articleAPI.getFitsToArticles() ) {
                    if( fitsToArticleAPI.getId().equals(article.getUniqueId()) ) {
                        LOG.log(Level.FINEST, "cannot fit article {0} to itself", new Object[] {fitsToArticleAPI.getId()});
                        throw validationMessageService.generateValidationException("fitsToArticles", "article.article.fitsTo.same");
                    }
                    Article fitsToArticle = getArticle(organizationUniqueId, fitsToArticleAPI.getId(), userAPI);
                    if( fitsToArticle == null ) {
                        LOG.log(Level.FINEST, "article {0} does not exist", new Object[] {fitsToArticleAPI.getId()});
                        throw validationMessageService.generateValidationException("fitsToArticles", "article.article.fitsTo.notExists");
                    }

                    Category fitsToArticleCategory = fitsToArticle.getBasedOnProduct() == null ? fitsToArticle.getCategory(): fitsToArticle.getBasedOnProduct().getCategory();
                    Article.Type fitsToArticleArticleType = fitsToArticleCategory.getArticleType();
                    // article cannot fit to other articles of type I, R, Tj
                    if( fitsToArticleArticleType == Article.Type.I ||
                            fitsToArticleArticleType == Article.Type.R ||
                            fitsToArticleArticleType == Article.Type.Tj ) {
                        // not ok
                        LOG.log(Level.INFO, "fits to article {0} is of wrong article type and cannot fit to article: {1}", new Object[] {fitsToArticleAPI.getId(), article.getUniqueId()});
                        throw validationMessageService.generateValidationException("fitsToArticles", "article.article.fitsTo.articleWrongType");
                    }
                    articles.add(fitsToArticle);
                }
            }
        }
        return articles.isEmpty() ? null: articles;
    }

    /**
     * Set preventive maintenance valid from on the <code>Product</code> (if
     * given)
     *
     * @param articleAPI user supplied values
     * @param article the Product to add data to
     * @throws HjalpmedelstjanstenValidationException if validations fails, e.g.
     * if a <code>CVPreventiveMaintenance</code> does not exist.
     */
    private void setPreventiveMaintenanceValidFrom(ArticleAPI articleAPI, Article article, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setPreventiveMaintenanceValidFrom(...)");
        if( articleAPI.getPreventiveMaintenanceValidFrom() != null ) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(articleAPI.getPreventiveMaintenanceValidFrom().getCode());
            if( preventiveMaintenance == null ) {
                LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[] {userAPI.getId(), articleAPI.getPreventiveMaintenanceValidFrom()});
                throw validationMessageService.generateValidationException("preventiveMaintenanceValidFrom", "article.preventiveMaintenanceValidFrom.notExist");
            }
            article.setPreventiveMaintenanceValidFrom(preventiveMaintenance);
        } else {
            article.setPreventiveMaintenanceValidFrom(null);
        }
    }

    /**
     * Set if the article is customer unique or not. If article is based on product,
     * the article value of customer unique cannot differ from the product.
     *
     * @param articleAPI user supplied values
     * @param article the article to update
     * @throws HjalpmedelstjanstenValidationException
     */
    public void setCustomerUnique(ArticleAPI articleAPI, Article article) throws HjalpmedelstjanstenValidationException {
        if( article.getBasedOnProduct() != null ) {
            if (article.getBasedOnProduct().isCustomerUnique()){
                if (!articleAPI.isCustomerUnique()) {
                    // this is an error, an article based on a customer unique product cannot be overridden as non unique
                    throw validationMessageService.generateValidationException("customerUnique", "article.customerUnique.cannotOverride");
                }
                else{
                    article.setCustomerUnique(true);
                    article.setCustomerUniqueOverridden(false);
                }
            }
            else{ //basedOnproduct is NOT customerUnique
                if (!articleAPI.isCustomerUnique()) {
                    article.setCustomerUnique(false);
                    article.setCustomerUniqueOverridden(false);
                }
                else{
                    article.setCustomerUnique(true);
                    article.setCustomerUniqueOverridden(true);
                }
            }
        }
        else{ //article is not based on product
            article.setCustomerUnique(articleAPI.isCustomerUnique());
            article.setCustomerUniqueOverridden(true);
        }
        /*
        if( article.isCustomerUniqueOverridden() ) {
            article.setCustomerUnique(articleAPI.isCustomerUnique());
        } else {
            if( article.getBasedOnProduct() != null ) {
                if( !articleAPI.isCustomerUnique() && article.getBasedOnProduct().isCustomerUnique() ) {
                    // this is an error, customer unique product cannot be overridden as non unique
                    throw validationMessageService.generateValidationException("customerUnique", "article.customerUnique.cannotOverride");
                } else {
                    if( articleAPI.isCustomerUnique() != article.getBasedOnProduct().isCustomerUnique() ) {
                        article.setCustomerUnique(articleAPI.isCustomerUnique());
                        article.setCustomerUniqueOverridden(true);
                    }
                }
            } else {
                article.setCustomerUnique(articleAPI.isCustomerUnique());
            }
        }
         */
    }

    /**
     * Checks if order information should be set and if so, calls another method
     * to set it.
     *
     * @param articleAPI user supplied values
     * @param article the Article to add data to
     * @throws HjalpmedelstjanstenValidationException if validations fails, e.g.
     * if a <code>Unit</code> does not exist.
     */
    public void checkSetOrderInformation(ArticleAPI articleAPI, Article article, Map<Long, CVOrderUnit> allOrderUnits) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setOrderInformation(...)");
        // is any of the order information fields different from the product?
        if( article.getBasedOnProduct() != null ) {
            if( article.isOrderInformationOverridden() || isOrderInformationFieldsOverridden(articleAPI, article) ) {
                LOG.log(Level.FINEST, "Order information overridden before or is in this request" );
                if(isOrderInformationFieldsOverridden(articleAPI, article)){
                    article.setOrderInformationOverridden(true);
                    setOrderInformation(articleAPI, article, allOrderUnits);
                }
                if(article.isOrderInformationOverridden() && !isOrderInformationFieldsOverridden(articleAPI, article)){
                    article.setOrderInformationOverridden(false);
                    setOrderInformation(articleAPI, article, allOrderUnits);
                }
            } else {
                article.setOrderInformationOverridden(false);
            }
        } else {
            setOrderInformation(articleAPI, article, allOrderUnits);
        }
    }

    /**
     * Set order information on the <code>Article</code>
     *
     * @param articleAPI user supplied values
     * @param article the Article to add data to
     * @throws HjalpmedelstjanstenValidationException if validations fails, e.g.
     * if a <code>Unit</code> does not exist.
     */
    void setOrderInformation(ArticleAPI articleAPI, Article article, Map<Long, CVOrderUnit> allOrderUnits) throws HjalpmedelstjanstenValidationException {
        // order unit
        if( articleAPI.getOrderUnit() != null ) {
            CVOrderUnit orderUnit = allOrderUnits.get(articleAPI.getOrderUnit().getId());
            if( orderUnit == null ) {
                LOG.log(Level.FINEST, "cannot set order unit for article {0} since it does not exist", new Object[] {article.getUniqueId()});
                throw validationMessageService.generateValidationException("orderUnit", "article.orderUnit.notExist");
            }
            article.setOrderUnit(orderUnit);
        } else {
            article.setOrderUnit(null);
        }

        // article quantity in outer package
        article.setArticleQuantityInOuterPackage(articleAPI.getArticleQuantityInOuterPackage());
        if( articleAPI.getArticleQuantityInOuterPackageUnit() != null ) {
            CVOrderUnit articleQuantityInOuterPackageUnit = orderUnitController.getUnit(articleAPI.getArticleQuantityInOuterPackageUnit().getId());
            if( articleQuantityInOuterPackageUnit == null ) {
                LOG.log(Level.FINEST, "cannot set article quantity in outer package unit for article {0} since it does not exist", new Object[] {article.getUniqueId()});
                throw validationMessageService.generateValidationException("articleQuantityInOuterPackageUnit", "article.articleQuantityInOuterPackageUnit.notExist");
            }
            article.setArticleQuantityInOuterPackageUnit(articleQuantityInOuterPackageUnit);
        } else {
            article.setArticleQuantityInOuterPackageUnit(null);
        }

        // package content
        article.setPackageContent(articleAPI.getPackageContent());
        if( articleAPI.getPackageContentUnit() != null ) {
            CVPackageUnit packageContentUnit = packageUnitController.getUnit(articleAPI.getPackageContentUnit().getId());
            if( packageContentUnit == null ) {
                LOG.log(Level.FINEST, "cannot set article quantity in outer package unit for article {0} since it does not exist", new Object[] {article.getUniqueId()});
                throw validationMessageService.generateValidationException("packageContentUnit", "article.packageContentUnit.notExist");
            }
            article.setPackageContentUnit(packageContentUnit);
        } else {
            article.setPackageContentUnit(null);
        }

        // package level base
        article.setPackageLevelBase(articleAPI.getPackageLevelBase());

        // package level middle
        article.setPackageLevelMiddle(articleAPI.getPackageLevelMiddle());

        // package level top
        article.setPackageLevelTop(articleAPI.getPackageLevelTop());
    }

    /**
     * Checks to see if any of the fields that has to do with order information
     * is modified in the user supplied values.
     *
     * @param articleAPI user supplied values
     * @param article the Article to check against
     * @return true if any of the fields is modified, otherwise false
     */
    boolean isOrderInformationFieldsOverridden(ArticleAPI articleAPI, Article article) {
        if( isUnitInfoOverridden(
                article.getBasedOnProduct().getOrderUnit(),
                articleAPI.getOrderUnit())) {
            return true;
        }
        if( isUnitInfoOverridden(
                article.getBasedOnProduct().getArticleQuantityInOuterPackageUnit(),
                articleAPI.getArticleQuantityInOuterPackageUnit())) {
            return true;
        }
        if( isPackageUnitInfoOverridden(
                article.getBasedOnProduct().getPackageContentUnit(),
                articleAPI.getPackageContentUnit())) {
            return true;
        }
        if( doubleValuesOverridden(articleAPI.getArticleQuantityInOuterPackage(), article.getBasedOnProduct().getArticleQuantityInOuterPackage()) ) {
            return true;
        }
        if( doubleValuesOverridden(articleAPI.getPackageContent(), article.getBasedOnProduct().getPackageContent()) ) {
            return true;
        }
        if( integerValuesOverridden(articleAPI.getPackageLevelBase(), article.getBasedOnProduct().getPackageLevelBase()) ) {
            return true;
        }
        if( integerValuesOverridden(articleAPI.getPackageLevelMiddle(), article.getBasedOnProduct().getPackageLevelMiddle()) ) {
            return true;
        }
        return integerValuesOverridden(articleAPI.getPackageLevelTop(), article.getBasedOnProduct().getPackageLevelTop());
    }

    /**
     * Checks to see if the Double fields differ.
     *
     * @param articleBasedOnProductDouble Double field from Article Based On Product field
     * @param articleApiDouble Double field from Article
     * @return true if the Doubles differ, otherwise false
     */
    boolean doubleValuesOverridden( Double articleBasedOnProductDouble, Double articleApiDouble ) {
        if( articleBasedOnProductDouble != null ) {
            if( articleApiDouble == null ) {
                return true;
            } else {
                return !articleBasedOnProductDouble.equals(articleApiDouble);
            }
        } else {
            return articleApiDouble != null;
        }
    }

    /**
     * Checks to see if the Integer fields differ.
     *
     * @param articleBasedOnProductInteger Integer field from Article Based On Product field
     * @param articleApiInteger Integer field from Article
     * @return true if the Integers differ, otherwise false
     */
    boolean integerValuesOverridden( Integer articleBasedOnProductInteger, Integer articleApiInteger ) {
        if( articleBasedOnProductInteger != null ) {
            if( articleApiInteger == null ) {
                return true;
            } else {
                return !articleBasedOnProductInteger.equals(articleApiInteger);
            }
        } else {
            return articleApiInteger != null;
        }
    }

    /**
     * Compare a <code>CVOrderUnit</code> and <code>CVOrderUnitAPI</code> to see if the values
     * in the API is different than the unit.
     *
     * @param articleBasedOnProductUnit
     * @param articleApiUnit
     * @return
     */
    boolean isUnitInfoOverridden( CVOrderUnit articleBasedOnProductUnit, CVOrderUnitAPI articleApiUnit) {
        if( articleApiUnit != null ) {
            if( articleBasedOnProductUnit == null ) {
                return true;
            } else {
                return !articleBasedOnProductUnit.getUniqueId().equals(articleApiUnit.getId());
            }
        } else {
            return articleBasedOnProductUnit != null;
        }
    }

    /**
     * Compare a <code>CVPackageUnit</code> and <code>UnitAPI</code> to see if the values
     * in the API is different than the unit.
     *
     * @param articleBasedOnProductUnit
     * @param articleApiUnit
     * @return
     */
    boolean isPackageUnitInfoOverridden( CVPackageUnit articleBasedOnProductUnit, CVPackageUnitAPI articleApiUnit) {
        if( articleApiUnit != null ) {
            if( articleBasedOnProductUnit == null ) {
                return true;
            } else {
                return !articleBasedOnProductUnit.getUniqueId().equals(articleApiUnit.getId());
            }
        } else {
            return articleBasedOnProductUnit != null;
        }
    }

    public static boolean isEmptyList(List<?> list) { //static?
        return list == null || list.isEmpty();
    }

    private boolean articleValuesAreSameAsProduct(List<CategorySpecificPropertyListValue> productValues, List<Long> articleValues) {
        // do they have the same number of values
        int productNumberOfValues = productValues == null ? 0: productValues.size();
        int articleNumberOfValues = articleValues == null ? 0: articleValues.size();
        LOG.log( Level.FINEST, "productNumberOfValues: {0}, articleNumberOfValues: {1}", new Object[] {productNumberOfValues, articleNumberOfValues});
        if( productNumberOfValues != articleNumberOfValues ) {
            return false;
        }

        // is there anything in the list to compare? we know the lists are the same size
        if( productNumberOfValues == 0 ) {
            return true;
        }

        // same number of values, are they the same values?
        for( CategorySpecificPropertyListValue productValue : productValues ) {
            if( !articleValues.contains(productValue.getUniqueId()) ) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get pricelistrows where given article exists for the given organization
     *
     * @param articleUniqueId
     * @param userAPI
     * @return
     */
    public List<AgreementPricelistRowAPI> getArticlePricelistRows(long articleUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getArticlePricelistRows(articleUniqueId: {0})", new Object[] {articleUniqueId});
        Organization organization = organizationController.getOrganizationFromLoggedInUser(userAPI);

        List<AgreementPricelistRow> agreementPricelistRows = agreementPricelistRowController.getOrganizationPricelistRowsByArticle(organization, articleUniqueId, userAPI);

        List<AgreementPricelistRowAPI> agreementPricelistRowAPIS = AgreementPricelistRowMapper.map(agreementPricelistRows, true, true);

        restrictCustomersFromSeeingPriceOfDiscontinuedArticles(organization, new ArrayList<>(agreementPricelistRowAPIS));

        return agreementPricelistRowAPIS;
    }

    /**
     * Get pricelistrows where given article exists for the given organization
     * include status of pricelist
     * @param articleUniqueId
     * @param userAPI
     * @param filter
     * @return
     */
    public List<AgreementPricelistRowAPI> getArticlePricelistRowsAndAgreementPricelistStatus(long articleUniqueId, UserAPI userAPI, int filter) {
        LOG.log(Level.FINEST, "getArticlePricelistRowsAndPricelistStatus(articleUniqueId: {0}, filter {1})", new Object[] {articleUniqueId, filter});
        Organization organization = organizationController.getOrganizationFromLoggedInUser(userAPI);

        List<AgreementPricelistRow> agreementPricelistRows = agreementPricelistRowController.getOrganizationPricelistRowsByArticle(organization, articleUniqueId, userAPI);

        List<AgreementPricelistRowAPI> agreementPricelistRowAPIS = AgreementPricelistRowMapper.map(agreementPricelistRows, true, true);

        addAgreementPricelistStatusToPricelistRow(new ArrayList<>(agreementPricelistRowAPIS));

        agreementPricelistRowAPIS = filterByPricelistStatus(new ArrayList<>(agreementPricelistRowAPIS), filter);

        restrictCustomersFromSeeingPriceOfDiscontinuedArticles(organization, new ArrayList<>(agreementPricelistRowAPIS));

        return agreementPricelistRowAPIS;
    }

    /**
     * Get general pricelistrows where given article exists
     *
     * @param articleUniqueId
     * @param userAPI
     * @return
     */
    public List<GeneralPricelistPricelistRowAPI> getArticleGeneralPricelistRows(long articleUniqueId, UserAPI userAPI, Boolean includeAllGP) {
        LOG.log(Level.FINEST, "getArticleGeneralPricelistRows(articleUniqueId: {0})", new Object[]{articleUniqueId});
        Organization organization = organizationController.getOrganizationFromLoggedInUser(userAPI);
        List<GeneralPricelistPricelistRow> generalPricelistPricelistRows = generalPricelistPricelistRowController.getOrganizationGeneralPricelistRowsByArticle(organization, articleUniqueId, includeAllGP);

        List<GeneralPricelistPricelistRowAPI> generalPricelistPricelistRowAPIS = GeneralPricelistPricelistRowMapper.map(generalPricelistPricelistRows, true, true);

        restrictCustomersFromSeeingPriceOfDiscontinuedArticles(organization, new ArrayList<>(generalPricelistPricelistRowAPIS));

        return generalPricelistPricelistRowAPIS;
    }

    /**
     * Get general pricelistrows where given article exists
     * include status of pricelist
     * @param articleUniqueId
     * @param userAPI
     * @return
     */
    public List<GeneralPricelistPricelistRowAPI> getArticleGeneralPricelistRowsAndGPPricelistStatus(long articleUniqueId, UserAPI userAPI, Boolean includeAllGP, int filter) {
        LOG.log(Level.FINEST, "getArticleGeneralPricelistRows(articleUniqueId: {0})", new Object[]{articleUniqueId});
        Organization organization = organizationController.getOrganizationFromLoggedInUser(userAPI);
        List<GeneralPricelistPricelistRow> generalPricelistPricelistRows = generalPricelistPricelistRowController.getOrganizationGeneralPricelistRowsByArticle(organization, articleUniqueId, includeAllGP);

        List<GeneralPricelistPricelistRowAPI> generalPricelistPricelistRowAPIS = GeneralPricelistPricelistRowMapper.map(generalPricelistPricelistRows, true, true);

        addGPPricelistStatusToPricelistRow(new ArrayList<>(generalPricelistPricelistRowAPIS));

        generalPricelistPricelistRowAPIS = filterByGPPricelistStatus(new ArrayList<>(generalPricelistPricelistRowAPIS), filter, userAPI);

        restrictCustomersFromSeeingPriceOfDiscontinuedArticles(organization, new ArrayList<>(generalPricelistPricelistRowAPIS));

        return generalPricelistPricelistRowAPIS;
    }

    public List<HJMTJRow> restrictCustomersFromSeeingPriceOfDiscontinuedArticles(Organization organization, List<HJMTJRow> rows) {
        for(HJMTJRow row: rows) {
            if(organization.getOrganizationType() == Organization.OrganizationType.CUSTOMER && row.getArticle().getStatus().equalsIgnoreCase("discontinued")) {
                row.setPrice(null);
            }
        }
        return rows;
    }

    public List<AgreementPricelistRowAPI> filterByPricelistStatus(List<AgreementPricelistRowAPI> agreementPricelistRowAPIs, int filter) {

        Iterator<AgreementPricelistRowAPI> agreementPricelistRowAPIIterator = agreementPricelistRowAPIs.iterator();

        switch (filter) {
            case 1: { //Future checked
                while (agreementPricelistRowAPIIterator.hasNext()) {
                    AgreementPricelistRowAPI agreementPricelistRowAPI = agreementPricelistRowAPIIterator.next();
                    if (!Objects.equals(agreementPricelistRowAPI.getPricelist().getStatus(), AgreementPricelist.Status.FUTURE.name())) {
                        LOG.log(Level.FINEST, "removing: {0}", new Object[]{agreementPricelistRowAPI.getPricelist().getStatus()});
                        agreementPricelistRowAPIIterator.remove();
                    }
                }
                return agreementPricelistRowAPIs;
            }
            case 2: {//Current checked
                while (agreementPricelistRowAPIIterator.hasNext()) {
                    AgreementPricelistRowAPI agreementPricelistRowAPI = agreementPricelistRowAPIIterator.next();
                    if (!Objects.equals(agreementPricelistRowAPI.getPricelist().getStatus(), AgreementPricelist.Status.CURRENT.name())) {
                        LOG.log(Level.FINEST, "removing: {0}", new Object[]{agreementPricelistRowAPI.getPricelist().getStatus()});
                        agreementPricelistRowAPIIterator.remove();
                    }
                }
                return agreementPricelistRowAPIs;
            }
            case 3: {//Future and Current checked
                while (agreementPricelistRowAPIIterator.hasNext()) {
                    AgreementPricelistRowAPI agreementPricelistRowAPI = agreementPricelistRowAPIIterator.next();
                    if (Objects.equals(agreementPricelistRowAPI.getPricelist().getStatus(), AgreementPricelist.Status.PAST.name())) {
                        LOG.log(Level.FINEST, "removing: {0}", new Object[]{agreementPricelistRowAPI.getPricelist().getStatus()});
                        agreementPricelistRowAPIIterator.remove();
                    }
                }
                return agreementPricelistRowAPIs;
            }
            case 4: {//Past checked
                while (agreementPricelistRowAPIIterator.hasNext()) {
                    AgreementPricelistRowAPI agreementPricelistRowAPI = agreementPricelistRowAPIIterator.next();
                    if (!Objects.equals(agreementPricelistRowAPI.getPricelist().getStatus(), AgreementPricelist.Status.PAST.name())) {
                        LOG.log(Level.FINEST, "removing: {0}", new Object[]{agreementPricelistRowAPI.getPricelist().getStatus()});
                        agreementPricelistRowAPIIterator.remove();
                    }
                }
                return agreementPricelistRowAPIs;
            }
            case 5: {//Future and Past checked, filter
                while (agreementPricelistRowAPIIterator.hasNext()) {
                    AgreementPricelistRowAPI agreementPricelistRowAPI = agreementPricelistRowAPIIterator.next();
                    if (Objects.equals(agreementPricelistRowAPI.getPricelist().getStatus(), AgreementPricelist.Status.CURRENT.name())) {
                        LOG.log(Level.FINEST, "removing: {0}", new Object[]{agreementPricelistRowAPI.getPricelist().getStatus()});
                        agreementPricelistRowAPIIterator.remove();
                    }
                }
                return agreementPricelistRowAPIs;
            }
            case 6: {//Current and Past checked
                while (agreementPricelistRowAPIIterator.hasNext()) {
                    AgreementPricelistRowAPI agreementPricelistRowAPI = agreementPricelistRowAPIIterator.next();
                    if (Objects.equals(agreementPricelistRowAPI.getPricelist().getStatus(), AgreementPricelist.Status.FUTURE.name())) {
                        LOG.log(Level.FINEST, "removing: {0}", new Object[]{agreementPricelistRowAPI.getPricelist().getStatus()});
                        agreementPricelistRowAPIIterator.remove();
                    }
                }
                return agreementPricelistRowAPIs;
            }
            default: {//No checked (0) or All checked (7), no filter
                return agreementPricelistRowAPIs;
            }
        }
    }

    public List<GeneralPricelistPricelistRowAPI> filterByGPPricelistStatus(List<GeneralPricelistPricelistRowAPI> generalPricelistPricelistRowAPIS, int filter, UserAPI userAPI) {

        Iterator<GeneralPricelistPricelistRowAPI> generalPricelistPricelistRowAPIIterator = generalPricelistPricelistRowAPIS.iterator();

        Organization organization = organizationController.getOrganizationFromLoggedInUser(userAPI);
        boolean isCustomer;
        isCustomer = organization.getOrganizationType().equals(Organization.OrganizationType.CUSTOMER);

        switch (filter) {
            case 1: { //Future checked
                while (generalPricelistPricelistRowAPIIterator.hasNext()) {
                    GeneralPricelistPricelistRowAPI generalPricelistPricelistRowAPI = generalPricelistPricelistRowAPIIterator.next();
                    if (!Objects.equals(generalPricelistPricelistRowAPI.getPricelist().getStatus(), AgreementPricelist.Status.FUTURE.name())) {
                        LOG.log(Level.FINEST, "removing: {0}", new Object[]{generalPricelistPricelistRowAPI.getPricelist().getStatus()});
                        generalPricelistPricelistRowAPIIterator.remove();
                    }
                }
                return generalPricelistPricelistRowAPIS;
            }
            case 2: {//Current checked
                while (generalPricelistPricelistRowAPIIterator.hasNext()) {
                    GeneralPricelistPricelistRowAPI generalPricelistPricelistRowAPI = generalPricelistPricelistRowAPIIterator.next();
                    if (!Objects.equals(generalPricelistPricelistRowAPI.getPricelist().getStatus(), AgreementPricelist.Status.CURRENT.name())) {
                        LOG.log(Level.FINEST, "removing: {0}", new Object[]{generalPricelistPricelistRowAPI.getPricelist().getStatus()});
                        generalPricelistPricelistRowAPIIterator.remove();
                    }
                }
                return generalPricelistPricelistRowAPIS;
            }
            case 3: {//Future and Current checked
                while (generalPricelistPricelistRowAPIIterator.hasNext()) {
                    GeneralPricelistPricelistRowAPI generalPricelistPricelistRowAPI = generalPricelistPricelistRowAPIIterator.next();
                    if (Objects.equals(generalPricelistPricelistRowAPI.getPricelist().getStatus(), AgreementPricelist.Status.PAST.name())) {
                        LOG.log(Level.FINEST, "removing: {0}", new Object[]{generalPricelistPricelistRowAPI.getPricelist().getStatus()});
                        generalPricelistPricelistRowAPIIterator.remove();
                    }
                }
                return generalPricelistPricelistRowAPIS;
            }
            case 4: {//Past checked
                while (generalPricelistPricelistRowAPIIterator.hasNext()) {
                    GeneralPricelistPricelistRowAPI generalPricelistPricelistRowAPI = generalPricelistPricelistRowAPIIterator.next();
                    if (!Objects.equals(generalPricelistPricelistRowAPI.getPricelist().getStatus(), AgreementPricelist.Status.PAST.name())) {
                        LOG.log(Level.FINEST, "removing: {0}", new Object[]{generalPricelistPricelistRowAPI.getPricelist().getStatus()});
                        generalPricelistPricelistRowAPIIterator.remove();
                    }
                }
                return generalPricelistPricelistRowAPIS;
            }
            case 5: {//Future and Past checked, filter
                while (generalPricelistPricelistRowAPIIterator.hasNext()) {
                    GeneralPricelistPricelistRowAPI generalPricelistPricelistRowAPI = generalPricelistPricelistRowAPIIterator.next();
                    if (Objects.equals(generalPricelistPricelistRowAPI.getPricelist().getStatus(), AgreementPricelist.Status.CURRENT.name())) {
                        LOG.log(Level.FINEST, "removing: {0}", new Object[]{generalPricelistPricelistRowAPI.getPricelist().getStatus()});
                        generalPricelistPricelistRowAPIIterator.remove();
                    }
                }
                return generalPricelistPricelistRowAPIS;
            }
            case 6: {//Current and Past checked
                while (generalPricelistPricelistRowAPIIterator.hasNext()) {
                    GeneralPricelistPricelistRowAPI generalPricelistPricelistRowAPI = generalPricelistPricelistRowAPIIterator.next();
                    if (Objects.equals(generalPricelistPricelistRowAPI.getPricelist().getStatus(), AgreementPricelist.Status.FUTURE.name())) {
                        LOG.log(Level.FINEST, "removing: {0}", new Object[]{generalPricelistPricelistRowAPI.getPricelist().getStatus()});
                        generalPricelistPricelistRowAPIIterator.remove();
                    }
                }
                return generalPricelistPricelistRowAPIS;
            }
            default: {//No checked (0) or All checked (7), no filter
                if (isCustomer){ //customer shall not see future
                    while (generalPricelistPricelistRowAPIIterator.hasNext()) {
                        GeneralPricelistPricelistRowAPI generalPricelistPricelistRowAPI = generalPricelistPricelistRowAPIIterator.next();
                        if (Objects.equals(generalPricelistPricelistRowAPI.getPricelist().getStatus(), AgreementPricelist.Status.FUTURE.name())) {
                            LOG.log(Level.FINEST, "removing: {0}", new Object[]{generalPricelistPricelistRowAPI.getPricelist().getStatus()});
                            generalPricelistPricelistRowAPIIterator.remove();
                        }
                    }
                }
                return generalPricelistPricelistRowAPIS;
            }
        }
    }

    public List<AgreementPricelistRowAPI> addAgreementPricelistStatusToPricelistRow(List<AgreementPricelistRowAPI> agreementPricelistRowAPIs) {
        for(AgreementPricelistRowAPI agreementPricelistRowAPI: agreementPricelistRowAPIs) {

            AgreementPricelistAPI agreementPricelistAPI = agreementPricelistRowAPI.getPricelist();

            AgreementPricelist agreementPricelist = AgreementPricelistMapper.map(agreementPricelistAPI);
            agreementPricelist.setValidFrom(new Date(agreementPricelistAPI.getValidFrom()));
            agreementPricelist.setUniqueId(agreementPricelistAPI.getId());

            AgreementAPI agreementAPI = new AgreementAPI();
            agreementAPI.setId(agreementPricelistAPI.getAgreement().getId());
            agreementAPI.setValidFrom(agreementPricelistAPI.getAgreement().getValidFrom());
            agreementAPI.setValidTo(agreementPricelistAPI.getAgreement().getValidTo());
            agreementAPI.setStatus(agreementPricelistAPI.getAgreement().getStatus());

            agreementPricelist.setAgreement(AgreementMapper.map(agreementAPI));

            AgreementPricelist currentPricelist = agreementPricelistController.getCurrentPricelist(agreementAPI.getId());

            agreementPricelistAPI.setStatus(AgreementPricelistMapper.getPricelistStatus(agreementPricelist, currentPricelist).toString());

            agreementPricelistRowAPI.setPricelist(agreementPricelistAPI);
        }
        return agreementPricelistRowAPIs;
    }

    public List<GeneralPricelistPricelistRowAPI> addGPPricelistStatusToPricelistRow(List<GeneralPricelistPricelistRowAPI> gpPricelistRowAPIs) {
        for(GeneralPricelistPricelistRowAPI gpPricelistRowAPI: gpPricelistRowAPIs) {

            GeneralPricelistPricelistAPI gpPricelistAPI = gpPricelistRowAPI.getPricelist();

            GeneralPricelistPricelist generalPricelistPricelist = GeneralPricelistPricelistMapper.map(gpPricelistAPI);
            generalPricelistPricelist.setValidFrom(new Date(gpPricelistAPI.getValidFrom()));
            generalPricelistPricelist.setUniqueId(gpPricelistAPI.getId());

            GeneralPricelistAPI generalPricelistAPI = new GeneralPricelistAPI();
            generalPricelistAPI.setId(gpPricelistAPI.getGeneralPricelist().getId());
            generalPricelistAPI.setValidFrom(gpPricelistAPI.getGeneralPricelist().getValidFrom());
            generalPricelistAPI.setValidTo(gpPricelistAPI.getGeneralPricelist().getValidTo());
            //generalPricelistAPI.setStatus(gpPricelistAPI.getGeneralPricelist().getStatus());

            generalPricelistPricelist.setGeneralPricelist(GeneralPricelistMapper.map(generalPricelistAPI));

            GeneralPricelistPricelist currentPricelist = generalPricelistPricelistController.getCurrentPricelist(gpPricelistRowAPI.getPricelist().getGeneralPricelist().getOwnerOrganization().getId());

            gpPricelistAPI.setStatus(GeneralPricelistPricelistMapper.getPricelistStatus(generalPricelistPricelist, currentPricelist).toString());

            gpPricelistRowAPI.setPricelist(gpPricelistAPI);
        }
        return gpPricelistRowAPIs;
    }


    /**
     *
     * @param articleUniqueId
     * @param userAPI
     * @return
     */
    public BooleanAPI getArticleExistsPricelistRows(long articleUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getArticleExistsPricelistRows(articleUniqueId: {0})", new Object[] {articleUniqueId});
        Organization organization = organizationController.getOrganizationFromLoggedInUser(userAPI);
        List<AgreementPricelistRow> agreementPricelistRows = agreementPricelistRowController.getOrganizationPricelistRowsByArticle(organization, articleUniqueId, userAPI);
        BooleanAPI booleanAPI = new BooleanAPI();
        if( agreementPricelistRows != null && !agreementPricelistRows.isEmpty() ) {
            booleanAPI.setIsTrue(true);
        }
        return booleanAPI;
    }

    /**
     * The article number is editable if the article:
     * - does not exist in an active or future agreement or gp pricelist
     * - articles that fits to this article does not exist in an active or future
     *   areement or gp pricelist
     *
     * @param article
     * @return
     */
    private boolean isArticleNumberEditable(Article article) {
        // get articles that fit to this article and check if they exist in pricelist
        List<Long> articlesToCheckIfInPricelists = em.createNamedQuery(Article.GET_IDS_FITS_TO_ARTICLE).
                setParameter("articleUniqueId", article.getUniqueId()).
                getResultList();
        articlesToCheckIfInPricelists.add(article.getUniqueId());

        if( agreementPricelistRowController.existRowsByArticle(articlesToCheckIfInPricelists) ||
                generalPricelistPricelistRowController.existRowsByArticle(articlesToCheckIfInPricelists)) {
            LOG.log( Level.FINEST, "Article: {0} or one that fits to it exists in agreement or gp pricelist so number is not editable", new Object[] {article.getUniqueId()});
            return false;
        }

        return true;
    }

    public List<String> getCustomerEmailByArticle(ArticleAPI articleAPI, UserAPI userAPI){
        //Hämta email för kunder som ska godkänna pristlistor för en artikel
        List<AgreementPricelistRowAPI> agreementPricelistRowAPIS = getArticlePricelistRows(articleAPI.getId(),userAPI);
        List<String> customerMailList = new ArrayList<>();
        for(AgreementPricelistRowAPI agreementPriceListRowAPI : agreementPricelistRowAPIS) {
            AgreementAPI agreement = agreementPriceListRowAPI.getPricelist().getAgreement();
            for (UserAPI userEngagement : agreement.getCustomerPricelistApprovers()) {
                if(!customerMailList.contains(userEngagement.getElectronicAddress().getEmail())){
                    customerMailList.add(userEngagement.getElectronicAddress().getEmail());
                }
            }
        }
        return customerMailList;
    }


    public void sendEmailToCustomerOnChangedStatus(Date oldArtRepDate, Article article, ArticleAPI articleAPI, UserAPI userAPI, Boolean causedByOrganizationInactivation){
        List<AgreementPricelistRowAPI> agreementPricelistRowAPIS = getArticlePricelistRows(articleAPI.getId(),userAPI);
/*        if(article.getStatus().toString().equalsIgnoreCase(Product.Status.DISCONTINUED.toString()) && articleAPI.getStatus().equalsIgnoreCase(Product.Status.DISCONTINUED.toString())) {
            if (article.getReplacementDate() != null) {
                //Återaktiverad artikel, skicka mail - HJAL1419
                List<String> customerMailList = new ArrayList<>();
                for(AgreementPricelistRowAPI agreementPriceListRowAPI : agreementPricelistRowAPIS) {
                    AgreementAPI agreement = agreementPriceListRowAPI.getPricelist().getAgreement();
                    for (UserAPI userEngagement : agreement.getCustomerPricelistApprovers()) {
                        if(!customerMailList.contains(userEngagement.getElectronicAddress().getEmail())){
                            customerMailList.add(userEngagement.getElectronicAddress().getEmail());
                        }
                    }
                    try {
                        String mailSubject = String.format("Ändrad status för artikel %s", articleAPI.getArticleName());
                        String mailBody = String.format("Leverantör %s har angivit att artikel %s med artikelnummer %s ej längre är inaktiverad. <br/><br/>  Artikeln finns på avtal: <br/>Avtalsnummer: %s <br/>Avtalsnamn: %s <br/>Prislistenummer: %s", articleAPI.getOrganizationName(), articleAPI.getArticleName(), articleAPI.getArticleNumber(),agreement.getAgreementNumber(),agreement.getAgreementName(),agreementPriceListRowAPI.getPricelist().getNumber());
                        for (String email : customerMailList) {
                            emailController.send(email, mailSubject, mailBody);
                        }
                    } catch (Exception ex) {
                        LOG.log(Level.SEVERE, "Failed to send email on update article", ex);
                    }
                }

            }
        } */
        if (!causedByOrganizationInactivation) {
            if (articleAPI.getReplacementDate() != null || article.getReplacementDate() != null) {
                for (AgreementPricelistRowAPI agreementPriceListRowAPI : agreementPricelistRowAPIS) {
                    AgreementAPI agreement = agreementPriceListRowAPI.getPricelist().getAgreement();
                    if (!agreement.getStatus().equalsIgnoreCase(Agreement.Status.DISCONTINUED.toString())) {
                        List<String> customerMailList = new ArrayList<>();
                        for (UserAPI userEngagement : agreement.getCustomerPricelistApprovers()) {
                            if (!customerMailList.contains(userEngagement.getElectronicAddress().getEmail())) {
                                customerMailList.add(userEngagement.getElectronicAddress().getEmail());
                            }
                        }
                        try {
                            Date date = new Date(articleAPI.getReplacementDate());
                            //if (article.getReplacementDate() == null) {

                            if (oldArtRepDate == null) {
                                //Nytt utgångsdatum
                                //if (articleAPI.getReplacementDate() != null && articleAPI.getInactivateRowsOnReplacement() != null) {
                                String mailSubject = String.format("Nytt utgångsdatum för artikel %s", articleAPI.getArticleName());
                                String mailBody = String.format("Leverantör %s har angivit ett utgångsdatum, %s, på artikel %s med artikelnummer %s.<br/><br/>  Artikeln finns på avtal: <br/>Avtalsnummer: %s <br/>Avtalsnamn: %s <br/>Prislistenummer: %s", articleAPI.getOrganizationName(), new SimpleDateFormat("yyyy-MM-dd").format(articleAPI.getReplacementDate()), articleAPI.getArticleName(), articleAPI.getArticleNumber(), agreement.getAgreementNumber(), agreement.getAgreementName(), agreementPriceListRowAPI.getPricelist().getNumber());
                                for (String email : customerMailList) {
                                    emailController.send(email, mailSubject, mailBody);
                                }
                                //}
                                //} else if (articleAPI.getReplacementDate() != article.getReplacementDate().getTime()) {
                            } else if (articleAPI.getReplacementDate() != oldArtRepDate.getTime()) {
                                //Uppdaterat utgångsdatum
                                String mailSubject = String.format("Uppdaterat utgångsdatum för artikel %s", articleAPI.getArticleName());
                                String mailBody = String.format("Leverantör %s har uppdaterat utgångsdatum, %s, på artikel %s med artikelnummer %s.<br/><br/>  Artikeln finns på avtal: <br/>Avtalsnummer: %s <br/>Avtalsnamn: %s <br/>Prislistenummer: %s", articleAPI.getOrganizationName(), new SimpleDateFormat("yyyy-MM-dd").format(date), articleAPI.getArticleName(), articleAPI.getArticleNumber(), agreement.getAgreementNumber(), agreement.getAgreementName(), agreementPriceListRowAPI.getPricelist().getNumber());
                                for (String email : customerMailList) {
                                    emailController.send(email, mailSubject, mailBody);
                                }
                            }

                        } catch (Exception ex) {
                            LOG.log(Level.SEVERE, "Failed to send email on update article", ex);
                        }
                    }
                }
            } else if (articleAPI.getReplacementDate() == null) {
                if (oldArtRepDate != null) {
                    //Återaktiverad artikel, skicka mail - HJAL1419
                    for (AgreementPricelistRowAPI agreementPriceListRowAPI : agreementPricelistRowAPIS) {
                        AgreementAPI agreement = agreementPriceListRowAPI.getPricelist().getAgreement();
                        List<String> customerMailList = new ArrayList();
                        for (UserAPI userEngagement : agreement.getCustomerPricelistApprovers()) {
                            if (!customerMailList.contains(userEngagement.getElectronicAddress().getEmail())) {
                                customerMailList.add(userEngagement.getElectronicAddress().getEmail());
                            }
                        }
                        try {
                            String mailSubject = String.format("Ändrad status för artikel %s", articleAPI.getArticleName());
                            String mailBody = String.format("Leverantör %s har angivit att artikel %s med artikelnummer %s ej längre är inaktiverad. <br/><br/>  Artikeln finns på avtal: <br/>Avtalsnummer: %s <br/>Avtalsnamn: %s <br/>Prislistenummer: %s", articleAPI.getOrganizationName(), articleAPI.getArticleName(), articleAPI.getArticleNumber(), agreement.getAgreementNumber(), agreement.getAgreementName(), agreementPriceListRowAPI.getPricelist().getNumber());
                            for (String email : customerMailList) {
                                emailController.send(email, mailSubject, mailBody);
                            }
                        } catch (Exception ex) {
                            LOG.log(Level.SEVERE, "Failed to send email on update article", ex);
                        }
                    }
                }
            }
        }
    }
}
