package se.inera.hjalpmedelstjansten.business.media.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.media.controller.DocumentTypeController;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.cv.CVDocumentTypeAPI;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import java.util.List;
import java.util.logging.Level;

/**
 * REST API for DocumentType functionality.
 *
 */
@Stateless
@Path("documenttypes")
@Interceptors({ PerformanceLogInterceptor.class })
public class DocumentTypeService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    private DocumentTypeController documentTypeController;

    /**
     * Return all document types.
     *
     * @return a list of <code>OrderUnitAPI</code>
     */
    @GET
    @SecuredService(permissions = {"documenttype:view"})
    public List<CVDocumentTypeAPI> getAllDocumentTypes() {
        LOG.log(Level.FINEST, "getAllDocumentTypes()");
        return documentTypeController.getAllDocumentTypes();
    }

}
