package se.inera.hjalpmedelstjansten.business.logging.view;

import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_ORGANIZATION_ID;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_SESSION_ID;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_TRACE_ID;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_USER_ID;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_USER_NAME;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_USER_ROLE;

import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;
import jakarta.ejb.ConcurrencyManagement;
import jakarta.ejb.ConcurrencyManagementType;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.MDC;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.model.api.RoleAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;

@Singleton
@Startup
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class MDCHelper {

    public static final String LOG_TRACE_ID_HEADER = "x-trace-id";
    private static final int LENGTH_LIMIT = 8;
    private static final char[] BASE62CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();

    @Inject
    private AuthHandler authHandler;

    public void decorateWithUser(HttpServletRequest httpServletRequest) {
        MDC.put(MDC_TRACE_ID, traceId(httpServletRequest));
        if (!authHandler.isAthenticated()) {
            return;
        }

        MDC.put(MDC_SESSION_ID, authHandler.getSessionId());

        final var fromSession = authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if (!(fromSession instanceof UserAPI)) {
            return;
        }

        final var userAPI = (UserAPI) fromSession;
        MDC.put(MDC_USER_ID, Long.toString(userAPI.getId()));
        MDC.put(MDC_USER_NAME, userAPI.getUsername());

        userAPI.getUserEngagements().stream().findFirst().ifPresent(
            userEngagementAPI -> {
                MDC.put(MDC_USER_ROLE, Arrays.toString(userEngagementAPI.getRoles().stream().map(RoleAPI::getName).toArray()));
                MDC.put(MDC_ORGANIZATION_ID, Long.toString(userEngagementAPI.getOrganizationId()));
            }
        );
    }

    public void clear() {
        MDC.clear();
    }

    private String traceId(HttpServletRequest http) {
        return Optional.ofNullable(
                http.getHeader(LOG_TRACE_ID_HEADER)
            )
            .orElse(generateTraceId());
    }

    private String generateTraceId() {
        final var charBuffer = CharBuffer.allocate(LENGTH_LIMIT);
        IntStream.generate(() -> ThreadLocalRandom.current().nextInt(BASE62CHARS.length))
            .limit(LENGTH_LIMIT)
            .forEach(value -> charBuffer.append(BASE62CHARS[value]));
        return charBuffer.rewind().toString();
    }
}
