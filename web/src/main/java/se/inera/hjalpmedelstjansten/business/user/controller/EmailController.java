package se.inera.hjalpmedelstjansten.business.user.controller;

import lombok.NoArgsConstructor;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.PropertyLoader;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;

@Stateless
@NoArgsConstructor
public class EmailController {

    @Inject
    private HjmtLogger LOG;

    private boolean mailSendingEnabled;

    private String smtpHost;
    private String smtpPort;
    private String mailFromAddress;
    private String allEmailsPostFix;

    @Inject
    private String mailEncoding;

    @Inject
    private String mailMimeSubtype;

    @Inject
    private String createPasswordPath;



    private String mailAccountCreationSubject;
    private String mailAccountCreationBody;
    private String mailResetPasswordSubject;
    private String mailResetPasswordBody;
    private String mailBodyPostFix;
    private String mailSubjectPreFix;

    @Inject
    public EmailController(PropertyLoader propertyLoader) {

        mailAccountCreationSubject = propertyLoader.getMessage("mailAccountCreationSubject");
        mailAccountCreationBody = propertyLoader.getMessage("mailAccountCreationBody");
        mailResetPasswordSubject = propertyLoader.getMessage("mailResetPasswordSubject");
        mailResetPasswordBody = propertyLoader.getMessage("mailResetPasswordBody");
        mailBodyPostFix = propertyLoader.getMessage("mailBodyPostFix");
        mailSubjectPreFix = propertyLoader.getMessage("mailSubjectPreFix");
    }

    public void sendAccountCreationEmail(String toAddress, String emailLinkToken, String baseUrl, String username, Date expirationTime) throws UnsupportedEncodingException, MessagingException {
        LOG.log( Level.FINEST, "sendAccountCreationEmail(...)" );
        String emailVerificationLink = baseUrl + createPasswordPath + "?email=" + URLEncoder.encode(toAddress, "UTF-8") + "&token=" + URLEncoder.encode(emailLinkToken, "UTF-8" );
        String message = String.format(mailAccountCreationBody, username, emailVerificationLink, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(expirationTime));
        send(toAddress, mailAccountCreationSubject, message);
    }

    public void sendResetPasswordEmail(String toAddress, String emailLinkToken, String baseUrl, String username) throws UnsupportedEncodingException, MessagingException {
        LOG.log( Level.FINEST, "sendResetPasswordEmail(...)" );
        String emailVerificationLink = baseUrl + createPasswordPath + "?email=" + URLEncoder.encode(toAddress, "UTF-8") + "&token=" + URLEncoder.encode(emailLinkToken, "UTF-8" );
        String message = String.format(mailResetPasswordBody, username, emailVerificationLink);
        send(toAddress, mailResetPasswordSubject, message);
    }

    public void send(String toAddress, String subject, String message) throws MessagingException {
        LOG.log( Level.FINEST, "send( ... )" );
        if( toAddress == null || toAddress.isEmpty() ) {
            LOG.log( Level.INFO, "No adress to send mail to" );
            return;
        }
        if( !mailSendingEnabled ) {
            LOG.log( Level.INFO, "Email sending is disabled" );
            return;
        }
        message += allEmailsPostFix;
        subject = mailSubjectPreFix + " " + subject;
        if( smtpHost != null && !smtpHost.isEmpty() &&
            smtpPort != null && !smtpPort.isEmpty() &&
            mailFromAddress != null && !mailFromAddress.isEmpty() &&
            mailEncoding != null && !mailEncoding.isEmpty() &&
            mailMimeSubtype != null && !mailMimeSubtype.isEmpty() ) {
            Properties properties = new Properties();
            properties.put("mail.smtp.host", smtpHost);
            properties.put("mail.smtp.port", smtpPort);
            Session session = Session.getDefaultInstance(properties);

            MimeMessage mimeMessage = new MimeMessage(session);
            mimeMessage.setFrom(new InternetAddress(mailFromAddress));
            mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
            mimeMessage.setSubject(subject, mailEncoding);
            mimeMessage.setText(message, mailEncoding, mailMimeSubtype);
            mimeMessage.setSentDate( new Date() );

            Transport.send(mimeMessage);
        } else {
            LOG.log( Level.WARNING, "Missing configuration for email notifications." );
        }

    }

    @PostConstruct
    private void initialize() {
        smtpHost = System.getenv("SMTP_HOST");
        smtpPort = System.getenv("SMTP_PORT");
        mailFromAddress = System.getenv("SMTP_MAIL_FROM_ADDRESS");
        mailSendingEnabled = System.getenv("MAIL_SENDING").equalsIgnoreCase("enabled");
        String serviceBaseUrl = System.getenv("HJMTJ_SERVICE_BASE_URL");
        allEmailsPostFix = String.format(mailBodyPostFix, serviceBaseUrl, serviceBaseUrl);
    }

}
