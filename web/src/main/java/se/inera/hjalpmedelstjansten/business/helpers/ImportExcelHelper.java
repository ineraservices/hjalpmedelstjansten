package se.inera.hjalpmedelstjansten.business.helpers;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.time.Year;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;

@ApplicationScoped
public class ImportExcelHelper {

    @Inject
    HjmtLogger LOG;

    public String getCellStringValueByName(XSSFRow fieldNamesRow, XSSFRow valuesRow, String fieldName) {
        XSSFCell cell = getCellByName(fieldNamesRow, valuesRow, fieldName);
        if (cell != null) {
            try {
                switch (cell.getCellType()) {
                    case STRING:
                        return cell == null ? null : cell.getStringCellValue();
                    case NUMERIC: {
                        if (cell.getNumericCellValue() % 1 == 0) {
                            Long tmp = (long) cell.getNumericCellValue();
                            return cell == null ? null : String.valueOf(tmp);
                        } else {

                            return cell == null ? null : String.valueOf(cell.getNumericCellValue());
                        }
                    }
                    default: {
                        LOG.log(Level.FINEST, "Non supported cell value: " + cell.getCellType() + " At Row: " + fieldNamesRow + " Field: " + fieldName + " Value: " + valuesRow);
                        return null;
                    }
                }
            } catch (NullPointerException e) {
                return null;
            }
        } else return null;
    }

    public Double getCellNumericValueByName(XSSFRow fieldNamesRow, XSSFRow valuesRow, String fieldName) {
        XSSFCell cell = getCellByName(fieldNamesRow, valuesRow, fieldName);

        if (cell != null) {
            try {
                switch (cell.getCellType()) {
                    case STRING: {
                        return cell == null ? null : removeAllNonNumericCharactersFromNumericField(cell.getStringCellValue());
                    }
                    case NUMERIC:
                        return cell == null ? null : cell.getNumericCellValue();
                    default: {
                        LOG.log(Level.FINEST, "Non supported cell value: " + cell.getCellType() + " At Row: " + fieldNamesRow + " with field name: " + fieldName + " and value: " + valuesRow);
                        return null;
                    }
                }
            } catch (NullPointerException e) {
                return null;
            }
        }

        return null;
    }

    public Date getCellDateValueByName(XSSFRow fieldNamesRow, XSSFRow valuesRow, String fieldName) {

        XSSFCell cell = getCellByName(fieldNamesRow, valuesRow, fieldName);
        DateHelper dateHelper = new DateHelper();


        if(cell != null) {

            Calendar calendar = new GregorianCalendar();
            String currentYear = Year.now().toString();
            StringBuffer rawStringValueOfNumericField = new StringBuffer();

            int year;
            int month;
            int day;

                switch(cell.getCellType()) {

                    case NUMERIC:

                        Double doubleValueOfField = Double.parseDouble(cell.getRawValue());
                        Long longValueOfField = doubleValueOfField.longValue();
                        rawStringValueOfNumericField.append(longValueOfField);

                            switch(rawStringValueOfNumericField.length()) {

                                case 5: // Default format
                                    return cell == null ? null : cell.getDateCellValue();

                                case 6: // 190208 format

                                    year = Integer.valueOf(currentYear.substring(0, 2)+rawStringValueOfNumericField.substring(0, 2));
                                    month = Integer.valueOf(rawStringValueOfNumericField.substring(2, 4)) - 1;
                                    day = Integer.valueOf(rawStringValueOfNumericField.substring(4, 6));

                                    calendar.set(year, month, day);

                                    return calendar.getTime();

                                case 8:  // 20190208 format
                                    year = Integer.valueOf(rawStringValueOfNumericField.substring(0, 4));
                                    month = Integer.valueOf(rawStringValueOfNumericField.substring(4, 6)) - 1;
                                    day = Integer.valueOf(rawStringValueOfNumericField.substring(6, 8));

                                    calendar.set(year, month, day);
                                    return calendar.getTime();

                                default: return null;
                            }

                    case STRING:

                        rawStringValueOfNumericField.append(cell.getStringCellValue());
                        String date = rawStringValueOfNumericField.toString().replaceAll("[^0-9]", "");


                        switch(date.length()) {

                                // case 5: should never happen since excel won't regard it as a dateField.

                                case 6: // 190208 format
                                   return dateHelper.formatStringDateAfterYYMMDD(date);

                                case 8: // 20190208 format
                                    return dateHelper.formatStringDateAfterYYYYMMDD(date);
                            }


                    default: cell.setCellType(CellType.BLANK);
                    return null;
                }
            }
        return null;
    }

    public XSSFCell getCellByName(XSSFRow fieldNamesRow, XSSFRow valuesRow, String fieldName) {
        for (int i = 0; i < fieldNamesRow.getLastCellNum(); i++) {
            XSSFCell cell = fieldNamesRow.getCell(i);
            String stringValueOfCell = parseCellDataToStringType(cell);
            if (cell != null && stringValueOfCell != null && fieldName.equals(stringValueOfCell)) {
                return valuesRow.getCell(i);
            }
        }
        return null;
    }

    public Double removeAllNonNumericCharactersFromNumericField(String numericField) {

        if (!numericField.isEmpty() && numericField != null) {

            StringBuilder filterDecimalSigns = new StringBuilder();

            //Replace all non numeric characters with empty-space, except for '.' and ',' and convert ',' to '.'
            filterDecimalSigns.append(numericField.replaceAll("[^0-9,.-]", "").replaceAll(",", "."));

            boolean hasDecimal = false;

            for (int i = 0; i < filterDecimalSigns.length(); i++) {

                //If entry will be a negative value.
                if (filterDecimalSigns.charAt(0) == '-') {
                    i++;
                }

                if (filterDecimalSigns.charAt(i) == '.') {
                    //If entry has more than one '.' remove.
                    if (hasDecimal) {
                        filterDecimalSigns.deleteCharAt(i);
                        i--;
                    }
                    hasDecimal = true;
                } else if (filterDecimalSigns.charAt(i) == '-') {
                    filterDecimalSigns.deleteCharAt(i);
                    i--;
                }
            }

            Double numberCharactersOfNumericField = Double.valueOf(filterDecimalSigns.toString());

            if (numberCharactersOfNumericField.isInfinite()) {
                numberCharactersOfNumericField = (double) numberCharactersOfNumericField.intValue();
            }

            return numberCharactersOfNumericField;
        }
        return null;
    }

    public String parseCellDataToStringType(XSSFCell cell) {

        if(cell != null) {
            switch (cell.getCellType()) {

                case STRING:
                    return cell.getStringCellValue();
                case NUMERIC:
                    return String.valueOf(cell.getNumericCellValue());
                default:
                    return null;
            }
        }
        return null;
    }

    public Double parseCellDataToNumericType(XSSFCell cell) {

        switch (cell.getCellType()) {

            case NUMERIC:
                return cell.getNumericCellValue();
            case STRING:
                return this.removeAllNonNumericCharactersFromNumericField(cell.getStringCellValue());
            default:
                return null;
        }
    }

    public boolean isProductOrArticleDiscontinued(Date date) {
        return date.before(Calendar.getInstance().getTime());
    }
}
