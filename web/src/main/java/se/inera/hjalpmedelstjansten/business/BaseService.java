package se.inera.hjalpmedelstjansten.business;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;

import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletRequest;
import java.util.logging.Level;

/**
 * Base class for JAX-RS services
 *
 */
public class BaseService {

    @Inject
    private HjmtLogger LOG;

    /**
     * Authorize whether user can handle this organization
     *
     * @param organizationUniqueId unique id of organization
     * @param userAPI
     * @return true if handle is ok, otherwise false
     */
    protected boolean authorizeHandleOrganization(long organizationUniqueId, UserAPI userAPI) {
        LOG.log( Level.FINEST, "authorizeHandleOrganization( organizationUniqueId: {0} )", new Object[] {organizationUniqueId} );
        if( UserController.getUserEngagementAPIByOrganizationId( organizationUniqueId, userAPI.getUserEngagements()) == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to handle organization not connected to", new Object[] {userAPI.getId()} );
            return false;
        }
        return true;
    }

    protected String getRequestIp(HttpServletRequest httpServletRequest) {
        // return httpServletRequest.getRemoteAddr();
        return httpServletRequest.getHeader("X-FORWARDED-FOR");
        // return null;
    }

}
