package se.inera.hjalpmedelstjansten.business.property.view;

import java.util.ResourceBundle;
import java.util.logging.Level;
import jakarta.annotation.PostConstruct;
import jakarta.ejb.ConcurrencyManagement;
import jakarta.ejb.ConcurrencyManagementType;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.inject.Inject;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;

@Singleton
@Startup
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class ValidationMessageService {

    @Inject
    private HjmtLogger LOG;

    private ResourceBundle swedishResources;

    public String getMessage(String key) {
        if( swedishResources.containsKey(key) ) {
            return swedishResources.getString(key);
        } else {
            LOG.log(Level.WARNING, "Missing validation message: {0}", new Object[] {key});
            return key;
        }
    }

    public String getMessage(String key, Object... args) {
        if( swedishResources.containsKey(key) ) {
            return String.format(swedishResources.getString(key), args);
        } else {
            LOG.log(Level.WARNING, "Missing validation message: {0}", new Object[] {key});
            return key;
        }
    }

    @PostConstruct
    private void init() {
        swedishResources = ResourceBundle.getBundle("ValidationMessages");
    }

    /**
     * Generate a validation exception based on the supplied message.
     *
     * @param field the specific field that failed
     * @param message the message
     * @return a custom validation exception with the supplied message.
     */
    public HjalpmedelstjanstenValidationException generateValidationException( String field, String message ) {
        LOG.log( Level.FINEST, "generateValidationException( message: {0} )", new Object[] {message});
        HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
        exception.addValidationMessage(field, getMessage(message));
        return exception;
    }

    /**
     * Generate a validation exception based on the supplied message.
     *
     * @param field the specific field that failed
     * @param message the message
     * @param args arguments for formatted string
     * @return a custom validation exception with the supplied message.
     */
    public HjalpmedelstjanstenValidationException generateValidationException( String field, String message, Object ... args ) {
        LOG.log( Level.FINEST, "generateValidationException( message: {0} )", new Object[] {message});
        HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
        exception.addValidationMessage(field, getMessage(message, args));
        return exception;
    }

}
