package se.inera.hjalpmedelstjansten.business.security.controller;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionException;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.eis.AbstractSessionDAO;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.LoggerService;
import se.inera.hjalpmedelstjansten.clustering.ClusterController;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

public class ShiroSessionController extends AbstractSessionDAO {

    private static final String SESSION_PREFIX = "session:";

    private static final HjmtLogger LOG = LoggerService.getLogger(ShiroSessionController.class.getName());
    ClusterController clusterController;

    public ShiroSessionController() throws NamingException {
        clusterController = (ClusterController) new InitialContext().lookup("java:app/hjmtj/ClusterController");
    }

    @Override
    protected Serializable doCreate(Session session) {
        LOG.log( Level.FINEST, "create( sessionId: {0} )", new Object[] {session.getId()} );
        Serializable sessionId = this.generateSessionId(session);
        this.assignSessionId(session, sessionId);
        try {
            clusterController.setValue(SESSION_PREFIX, sessionId.toString(), toString(session));
            clusterController.valueExpires(SESSION_PREFIX, sessionId.toString(), session.getTimeout() / 1000);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Failed to create redis session", ex);
            throw new SessionException("Failed to create redis session");
        }
        return session.getId();
    }

    @Override
    protected Session doReadSession(Serializable sessionId) {
        LOG.log( Level.FINEST, "readSession( sessionId: {0} )", new Object[] {sessionId} );
        try {
            String sessionAsString = clusterController.getValue(SESSION_PREFIX, sessionId.toString());
            if( sessionAsString == null ) {
                return null;
            }
            Session session = (Session) toObject(sessionAsString);
            return session;
        } catch (IOException|ClassNotFoundException ex) {
            LOG.log(Level.SEVERE, "Failed to read redis session", ex);
            throw new SessionException("Failed to read redis session");
        }
    }

    @Override
    public void update(Session updatedSession) throws UnknownSessionException {
        LOG.log( Level.FINEST, "update( sessionId: {0} )", new Object[] {updatedSession.getId()} );
        assertSessionExists(updatedSession);
        try {
            clusterController.setValue(SESSION_PREFIX, updatedSession.getId().toString(), toString(updatedSession));
            clusterController.valueExpires(SESSION_PREFIX, updatedSession.getId().toString(), updatedSession.getTimeout() / 1000);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Failed to create redis session", ex);
            throw new SessionException("Failed to create redis session");
        }
    }

    @Override
    public void delete(Session session) {
        LOG.log( Level.FINEST, "delete( sessionId: {0} )", new Object[] {session.getId()} );
        assertSessionExists(session);
        clusterController.removeValue(SESSION_PREFIX, session.getId().toString());
    }

    @Override
    public Collection<Session> getActiveSessions() {
        LOG.log( Level.FINEST, "getActiveSessions()" );
        List<Session> sessions = new ArrayList<>();
        Set<String> keys = clusterController.getKeys(SESSION_PREFIX);
        if( keys != null && !keys.isEmpty() ) {
            for( String key : keys ) {
                try {
                    sessions.add((Session) toObject(clusterController.getValue(SESSION_PREFIX, key)));
                } catch (IOException|ClassNotFoundException ex) {
                    LOG.log(Level.SEVERE, "Failed to get active redis sessions", ex);
                    throw new SessionException("Failed to get active redis sessions");
                }
            }
        }
        return sessions;
    }

    private void assertSessionExists( Session session ) {
        Session currentSession = readSession(session.getId());
        if( currentSession == null ) {
            throw new UnknownSessionException("No session found with id: " + session.getId());
        }
    }

    private static String toString( Session session ) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)) {
            objectOutputStream.writeObject(session);
        }
        return Base64.getEncoder().encodeToString(byteArrayOutputStream.toByteArray());
    }

    private static Object toObject( String sessionAsString ) throws IOException, ClassNotFoundException {
        byte[] objectBytes = Base64.getDecoder().decode(sessionAsString);
        Object object;
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(objectBytes))) {
            object = objectInputStream.readObject();
        }
        return object;
    }

}
