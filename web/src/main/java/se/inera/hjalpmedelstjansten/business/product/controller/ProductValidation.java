package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.EmailController;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Product;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

/**
 * Class for validation functionality for products
 *
 */
@Stateless
public class ProductValidation {

    @Inject
    HjmtLogger LOG;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    ProductController productController;

    @Inject
    ArticleController articleController;

    @Inject
    EmailController emailController;

    public void validateForCreate(ProductAPI productAPI, long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForCreate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForCreate(organizationUniqueId, productAPI);

        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }
    }

    public Set<ErrorMessageAPI> tryForCreate(long organizationUniqueId, ProductAPI productAPI) {
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        validateProductAPI(productAPI, errorMessageAPIs);
        if( errorMessageAPIs.size() > 0 ) {
            // break early if indata is in itself incorrect
            return errorMessageAPIs;
        }

        // product number must be unique within organization
        validateProductNumber(productAPI, organizationUniqueId, null, errorMessageAPIs);

        // only web can be set in the electronic address of the manufacturer
        validateOnlyManufacturerWebSet(productAPI, errorMessageAPIs);

        // first status cannot be DISCONTINUED
        /*Product.Status newStatus = Product.Status.valueOf(productAPI.getStatus());
        if( newStatus == Product.Status.DISCONTINUED ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("status", validationMessageService.getMessage("product.statusChange.invalid")));
        }*/

        return errorMessageAPIs;
    }

    public void validateForUpdate(ProductAPI productAPI, Product product, long organizationUniqueId, UserAPI userAPI, Boolean causedByOrganizationInactivation) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForUpdate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForUpdate(organizationUniqueId, productAPI, product, userAPI, causedByOrganizationInactivation);

        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }
    }

    public Set<ErrorMessageAPI> tryForUpdate(long organizationUniqueId, ProductAPI productAPI, Product product,UserAPI userAPI, Boolean causedByOrganizationInactivation) {
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        validateProductAPI(productAPI, errorMessageAPIs);
        validateProductStatusForUpdate(product,productAPI,errorMessageAPIs,userAPI, causedByOrganizationInactivation);
        if( errorMessageAPIs.size() > 0 ) {
            // break early if indata is in itself incorrect
            return errorMessageAPIs;
        }

        // product number must be unique within organization
        validateProductNumber(productAPI, organizationUniqueId, product, errorMessageAPIs);

        // only web can be set in the electronic address of the manufacturer
        validateOnlyManufacturerWebSet(productAPI, errorMessageAPIs);

        return errorMessageAPIs;
    }

    private void validateProductStatusForUpdate(Product product, ProductAPI productAPI, Set<ErrorMessageAPI> errorMessageAPIs, UserAPI userAPI, Boolean causedByOrganizationInactivation) {

        if(product.getStatus().toString().equalsIgnoreCase(Product.Status.DISCONTINUED.toString()) && productAPI.getStatus().equalsIgnoreCase(Product.Status.PUBLISHED.toString())){
            //ska inte gå att redigera en utgången produkt - HJAL2035
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("replacementDate", validationMessageService.getMessage("import.productarticle.product.expiredDate")));
        }
        if(product.getStatus().toString().equalsIgnoreCase(Product.Status.DISCONTINUED.toString()) && productAPI.getStatus().equalsIgnoreCase(Product.Status.DISCONTINUED.toString())){
            if(product.getReplacementDate() != null){
                try{
                    //2119 skicka mail för baserade artiklar
                    productController.sendEmailToAgreementPriceListApproversForArticles(userAPI,productAPI, true, causedByOrganizationInactivation);
                }
                catch(Exception ex){
                    LOG.log(Level.SEVERE, "Failed to send email on update product", ex);
                }
            }
        }
        else if(productAPI.getReplacementDate() != null || product.getReplacementDate() != null){
            try{
                //2119 skicka även för baserade artiklar
                //productController.sendEmailToAgreementPriceListApproversForArticles(userAPI,productAPI, false);
            }
            catch(Exception ex){
                LOG.log(Level.SEVERE, "Failed to send email on update product", ex);
            }
        }
    }

    private void validateOnlyManufacturerWebSet(ProductAPI productAPI,  Set<ErrorMessageAPI> errorMessageAPIs) {
        // only web can be set in the electronic address of the manufacturer
        if( productAPI.getManufacturerElectronicAddress() != null ) {
            ElectronicAddressAPI electronicAddressAPI = productAPI.getManufacturerElectronicAddress();
            if( !isEmpty(electronicAddressAPI.getEmail()) ||
                    !isEmpty(electronicAddressAPI.getFax()) ||
                    !isEmpty(electronicAddressAPI.getMobile()) ||
                    !isEmpty(electronicAddressAPI.getTelephone()) ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("web", validationMessageService.getMessage("product.manufacturer.onlyWeb")));
            }
        }
    }

    void validateProductNumber(ProductAPI productAPI,
                               long organizationUniqueId,
                               Product product,
                               Set<ErrorMessageAPI> errorMessageAPIs) {
        Product productWithNumber = productController.findByProductNumberAndOrganization(productAPI.getProductNumber(), organizationUniqueId);
        if( productWithNumber != null ) {
            if( product == null || !productWithNumber.getUniqueId().equals(product.getUniqueId()) ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("productNumber", validationMessageService.getMessage("product.number.notOrganizationUnique")));
            }
        }
        Article articleWithNumber = articleController.findByArticleNumberAndOrganization(productAPI.getProductNumber(), organizationUniqueId);
        if( articleWithNumber != null ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("productNumber", validationMessageService.getMessage("product.article.number.notOrganizationUnique")));
        }
    }

    private boolean isEmpty(String value) {
        LOG.log( Level.FINEST, "value: {0}", new Object[] {value});
        if( value == null || value.isEmpty() ) {
            return true;
        }
        return false;
    }

    private void validateProductAPI(ProductAPI productAPI, Set<ErrorMessageAPI> errorMessageAPIs) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<ProductAPI>> constraintViolations = validator.validate(productAPI, Default.class);
        if( constraintViolations != null && !constraintViolations.isEmpty() ) {
            for( ConstraintViolation constraintViolation : constraintViolations ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage()));
            }
        }
    }

}
