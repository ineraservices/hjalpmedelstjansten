package se.inera.hjalpmedelstjansten.business.user.controller;

import lombok.NoArgsConstructor;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.user.dao.NotificationDAO;
import se.inera.hjalpmedelstjansten.model.dto.NotificationDTO;
import se.inera.hjalpmedelstjansten.model.entity.Notification;
import se.inera.hjalpmedelstjansten.model.entity.UserAccount;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;
import se.inera.hjalpmedelstjansten.model.entity.UserNotification;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;

@Stateless
@NoArgsConstructor
public class NotificationController {

    @Inject
    HjmtLogger LOG;

    NotificationDAO notificationDAO;
    UserController userController;

    @Inject
    public NotificationController(NotificationDAO notificationDAO, UserController userController) {

        this.userController = userController;
        this.notificationDAO = notificationDAO;
    }


    private void createUserNotification(NotificationDTO notificationDTO) {

        try {

            // Try and fetch all available notifications for user and role.
            getAllAvailableNotifications(notificationDTO);

            // Error is generated if UserEngagement can't be found. Exit.
            if(!notificationDTO.getError().isEmpty()) {
                return;
            }

        } catch (EntityNotFoundException exc) {
            LOG.log(Level.SEVERE, "User with ID " + notificationDTO.getUserId() + " could not be found");
            return;
        }

        // Set up initial values for UserNotification entity.
        UserNotification newEntity = new UserNotification();
        // Set the associated user account.
        newEntity.setUserAccount(notificationDTO.getUser());
        // Set all available notifications as default.
        newEntity.setNotifications(notificationDTO.getAllNotificationTypeSet());

        try {
            notificationDAO.create(newEntity)
                    .ifPresent(notificationDTO::setUserNotification);

        } catch (Exception exc) {
            LOG.log(Level.SEVERE, exc.getMessage());
        }

    }

    /**
     * Get specified users NotificationTypes. If user doesn't have any, a new set is created.
     * @param notificationDTO NotificationDTO (Data Transfer Object)
     * @return NotificationDTO with updated values or including an error code.
     */
    public NotificationDTO getUserNotifications(NotificationDTO notificationDTO) {

        // Try and get UserNotification assigned to User.
        final Optional<UserNotification> optional = notificationDAO.findByUserId(notificationDTO.getUserId());

        if(!optional.isPresent()) {

            // If no UserNotification-object found, try and create for User, once.
            createUserNotification(notificationDTO);

            // If no UserEngagement can be found for User, something is wrong. Exit.
            if(!notificationDTO.getError().isEmpty()) {
                LOG.log(Level.SEVERE, "UserEngagement for User not found while creating UserNotification");
                return notificationDTO;
            }

        } else {

            // If found, set received userNotification in DTO.
            notificationDTO.setUserNotification(optional.get());

            // Fetch all available notifications for user.
            getAllAvailableNotifications(notificationDTO);

        }

        // UserNotification is either created or found. Transform types to frontend strings.
        if(null != notificationDTO.getUserNotification()) {
            // Set enum type set
            notificationDTO.setUserNotificationTypeSet(notificationDTO.getUserNotification().getNotifications());
            // Derive string set from enums.
            notificationDTO.setUserNotificationsStringSet(enumTypesToString(notificationDTO.getUserNotificationTypeSet()));
        }

        // Return DTO with 'users' and 'all' notifications set.
        return notificationDTO;

    }


    /**
     * Updates the specified users set of NotificationTypes.
     * @param notificationDTO NotificationDTO (Data Transfer Object)
     * @return NotificationDTO with updated values or including an error code.
     */
    public NotificationDTO updateUserNotifications(NotificationDTO notificationDTO) throws HjalpmedelstjanstenException {

        // Try and get UserNotification assigned to User.
        final Optional<UserNotification> optional = notificationDAO.findByUserId(notificationDTO.getUserId());

        // Abort if nothing is found.
        if(!optional.isPresent()) {
            notificationDTO.getError().put("error", "Could not find any UserNotification object to update. Try refreshing.");
            return notificationDTO;
        }

        // Update DTO to contain managed values for UserNotifications
        notificationDTO.setUserNotification(optional.get());

        // Update all available values to compare against.
        getAllAvailableNotifications(notificationDTO);

        if(null == notificationDTO.getUserNotificationsStringSet()) {
            LOG.log(Level.FINE, "Set is null. Nothing new to save.");
            notificationDTO.getError().put("error", "Set is null. Nothing new to save.");
            return notificationDTO;
        }

        // No error handling or sorting on this just plain re-write for now.
        notificationDTO.getUserNotification().getNotifications().clear();

        for(Notification notification : notificationDTO.getAllNotificationTypeSet()) {
            notificationDTO.getUserNotificationsStringSet().forEach((type) -> {
                if(type.equalsIgnoreCase(notification.getType().toString())) {
                    notificationDTO.getUserNotificationTypeSet().add(notification);
                    notificationDTO.getUserNotification().getNotifications().add(notification);
                }
            });
        }

        final Optional<UserNotification> update = notificationDAO.update(notificationDTO.getUserNotification().getUniqueId(), notificationDTO.getUserNotification());
        if(!update.isPresent()) throw new HjalpmedelstjanstenException("Something went wrong while merging users notifications.");
        return notificationDTO;

    }


/*    private NotificationDTO collectNotifications(NotificationDTO notificationDTO) {

        // We need organizationType and userRoles from userEngagement for sorting notifications.
        final UserEngagement userEngagement = userController.getUserEngagement(notificationDTO.getOrganizationId(), (notificationDTO.getUserId() - 1));

        if(null == userEngagement) {
            notificationDTO.getError().put("No UserEngagement found for user: ", String.valueOf(notificationDTO.getUserId()));
            return notificationDTO;
        }

        notificationDTO.setUserRoles(userEngagement.getRoles());
        notificationDTO.setOrganizationType(userEngagement.getOrganization().getOrganizationType());

        notificationDAO.findAllAvailableToUser(notificationDTO);

        if(null == notificationDTO.getUserNotification().getNotifications()) {
            notificationDTO.getUserNotification().setNotifications(new HashSet<>());
        }

        // TODO: Update this so it sorts by user role in the query (findAllAvailableToUser) instead. Remove _temp-set_ when done.
        // Only options for the current organizationType are currently in here.
        Set<Notification> temp = new HashSet<>();
        if(!notificationDTO.getAllNotificationTypeSet().isEmpty()) {

            notificationDTO.getAllNotificationTypeSet().forEach( (notification) -> {

                for(UserRole role : notificationDTO.getUserRoles()) {

                    if(notification.getPermission() == role.getName()) {
                        temp.add(notification);
                    }

                }

            });

        }
        notificationDTO.setAllNotificationTypeSet(temp);


        // Sort users temporary notifications on OrganizationType and UserRole just to make sure no cheatin' is going on.
        // Can be replaced/combined with allNotifications when I get the query to work.
        if(!notificationDTO.getUserNotification().getNotifications().isEmpty()) {

            notificationDTO.getUserNotification().getNotifications().forEach( (notification) -> {

                if(notification.getRecipient() == notificationDTO.getOrganizationType()) {

                    for(UserRole role : notificationDTO.getUserRoles()) {

                        if(notification.getPermission() == role.getName()) {
                            notificationDTO.getUserNotificationsMap()
                                .put(notification.getType().toString(), notification.getDescription());
                        }

                    }

                }

            });

        }

        return notificationDTO;

    }*/


    public NotificationDTO getAllAvailableNotifications(NotificationDTO notificationDTO) {

        // Fetch the current user.
        final UserAccount userAccount = userController.getUserById(notificationDTO.getUserId());

        // We need organizationType and userRoles from UserEngagement for sorting available notifications.
        final UserEngagement userEngagement = userController.findByUsername(userAccount.getUsername());

        // If UserEngagement is null, return with soft error.
        if(null == userEngagement) {
            notificationDTO.getError().put("No UserEngagement found for user: ", userAccount.getUsername());
            return notificationDTO;
        }

        // Set the current user.
        notificationDTO.setUser(userAccount);

        // Set OrganizationType and UserRoles in DTO.
        notificationDTO.setOrganizationType(userEngagement.getOrganization().getOrganizationType());
        notificationDTO.setUserRoles(userEngagement.getRoles());

        // Return whatever the dao can get.
        return notificationDAO.findAllAvailableToUser(notificationDTO);

    }

    private Set<String> enumTypesToString(Set<Notification> notificationTypes) {
        Set<String> mappedSet = new HashSet<>();
        for(Notification notification : notificationTypes) {
            mappedSet.add(notification.getType().toString());
        }
        return mappedSet;
    }

}
