package se.inera.hjalpmedelstjansten.business.media.controller;

public class S3Configuration {
    private String accessKeyId;
    private String bucketName;
    private String mediaBaseUrl;
    private String region;
    private String secretAccessKey;

    public S3Configuration () {
        this.setRegion(System.getenv("AWS_REGION"))
            .setAccessKeyId(System.getenv("AWS_ACCESS_KEY_ID"))
            .setSecretAccessKey(System.getenv("AWS_SECRET_ACCESS_KEY"))
            .setBucketName(System.getenv("AWS_BUCKET"))
            .setMediaBaseUrl(System.getenv("MEDIA_BASE_URL"));
    }

    public boolean isValid() {
        return isVariableValid(accessKeyId) &&
            isVariableValid(bucketName) &&
            isVariableValid(mediaBaseUrl) &&
            isVariableValid(region) &&
            isVariableValid(secretAccessKey);
    }

    private boolean isVariableValid(String variable) {
        return variable != null && !variable.isEmpty();
    }

    public S3Configuration setRegion(String region) {
        this.region = region;
        return this;
    }

    public S3Configuration setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
        return this;
    }

    public S3Configuration setSecretAccessKey(String secretAccessKey) {
        this.secretAccessKey = secretAccessKey;
        return this;
    }

    public S3Configuration setBucketName(String bucketName) {
        this.bucketName = bucketName;
        return this;
    }

    public S3Configuration setMediaBaseUrl(String mediaBaseUrl) {
        this.mediaBaseUrl = mediaBaseUrl;
        return this;
    }

    public String getRegion() {
        return region;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public String getSecretAccessKey() {
        return secretAccessKey;
    }

    public String getBucketName() {
        return bucketName;
    }

    public String getMediaBaseUrl() {
        return mediaBaseUrl;
    }
}
