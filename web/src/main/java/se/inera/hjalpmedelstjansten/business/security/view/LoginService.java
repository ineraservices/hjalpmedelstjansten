package se.inera.hjalpmedelstjansten.business.security.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.security.controller.LoginController;
import se.inera.hjalpmedelstjansten.model.api.LoginAPI;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;

@Stateless
@Path("login")
@Interceptors({ PerformanceLogInterceptor.class })
public class LoginService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private LoginController loginController;

    @Inject
    private ValidationMessageService validationMessageService;

    private String serviceBaseUrl;

    @POST
    public Response standardLoginUser(@Context HttpServletRequest httpServletRequest, LoginAPI loginAPI) {
        LOG.log(Level.FINEST, "standardLoginUser(...)");
        boolean success = loginController.standardLoginUser(loginAPI, getRequestIp(httpServletRequest));
        if( success ) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
    }

    @GET
    public Response tokenLoginUser(@Context HttpServletRequest httpServletRequest, @QueryParam("username") String username, @QueryParam("token") String token) throws URISyntaxException, UnsupportedEncodingException {
        LOG.log(Level.FINEST, "tokenLoginUser(...)");
        boolean success = loginController.tokenLoginUser(username, token, getRequestIp(httpServletRequest));
        if( success ) {
            return Response.temporaryRedirect(new URI(serviceBaseUrl + "start")).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).entity(validationMessageService.getMessage("login.token.fail")).build();
        }
    }

    @PostConstruct
    private void initialize() {
        serviceBaseUrl = System.getenv("HJMTJ_SERVICE_BASE_URL");
    }

}
