package se.inera.hjalpmedelstjansten.business.organization.controller;

import se.inera.hjalpmedelstjansten.business.assortment.controller.AssortmentMapper;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.PostAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountyAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVMunicipalityAPI;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.PostAddress;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCounty;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVMunicipality;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class OrganizationMapper {

    public static List<OrganizationAPI> map(List<Organization> organizations) {
        if( organizations == null ) {
            return null;
        }
        List<OrganizationAPI> organizationAPIs = new ArrayList<>();
        for( Organization organization : organizations ) {
            organizationAPIs.add(map(organization, false));
        }
        return organizationAPIs;
    }

    public static OrganizationAPI map(Organization organization, boolean includeEverything) {
        if( organization == null ) {
            return null;
        }
        OrganizationAPI organizationAPI = new OrganizationAPI();
        organizationAPI.setId(organization.getUniqueId());
        organizationAPI.setOrganizationName(organization.getOrganizationName());
        organizationAPI.setOrganisationType(organization.getOrganizationType().toString());
        organizationAPI.setOrganizationNumber(organization.getOrganizationNumber());
        organizationAPI.setGln(organization.getGln());
        organizationAPI.setCountry(CountryMapper.map(organization.getCountry()));
        organizationAPI.setValidFrom(organization.getValidFrom().getTime());
        if( organization.getCounties() != null ) {
            organizationAPI.setCounties(AssortmentMapper.mapCounties(organization.getCounties(), false));
        }
        if( organization.getValidTo() != null ) {
            organizationAPI.setValidTo(organization.getValidTo().getTime());
        }
        organizationAPI.setActive( calculateActive(organization.getValidFrom(), organization.getValidTo()) );
        if( includeEverything ) {
            if( organization.getPostAddresses() != null ) {
                organizationAPI.setPostAddresses(mapPostAddresses(organization.getPostAddresses()));
            }
            if( organization.getElectronicAddress() != null ) {
                organizationAPI.setElectronicAddress(ElectronicAddressMapper.map(organization.getElectronicAddress()));
            }
        }
        return organizationAPI;
    }

    public static Organization map(OrganizationAPI organizationAPI) {
        if( organizationAPI == null ) {
            return null;
        }
        Organization organization = new Organization();
        organization.setOrganizationName(organizationAPI.getOrganizationName());
        organization.setOrganizationNumber(organizationAPI.getOrganizationNumber());
        organization.setOrganizationType(Organization.OrganizationType.valueOf(organizationAPI.getOrganizationType()));
        organization.setValidFrom(new Date(organizationAPI.getValidFrom()));
        if( organizationAPI.getValidTo() != null ) {
            organization.setValidTo(new Date(organizationAPI.getValidTo()));
        }
        organization.setGln(organizationAPI.getGln());
        if( organizationAPI.getElectronicAddress() != null ) {
            organization.setElectronicAddress(ElectronicAddressMapper.map(organizationAPI.getElectronicAddress()));
        }
        if( organizationAPI.getPostAddresses() != null ) {
            List<PostAddress> postAddresses = mapPostAddressAPIs(organizationAPI.getPostAddresses(), organization);
            organization.setPostAddresses(postAddresses);
        }
        if( organizationAPI.getCounties() != null ) {
            List<CVCounty> counties = mapCountyAPIs(organizationAPI.getCounties(), organization);
            organization.setCounties(counties);
        }
        return organization;
    }

    private static List<PostAddressAPI> mapPostAddresses(List<PostAddress> postAddresses ) {
        List<PostAddressAPI> postAddressAPIs = new ArrayList<>();
        for( PostAddress postAddress : postAddresses ) {
            postAddressAPIs.add(mapPostAddress(postAddress));
        }
        return postAddressAPIs;
    }

    private static PostAddressAPI mapPostAddress(PostAddress postAddress ) {
        PostAddressAPI postAddressAPI = new PostAddressAPI();
        postAddressAPI.setId(postAddress.getUniqueId());
        postAddressAPI.setAddressType(postAddress.getAddressType());
        postAddressAPI.setStreetAddress(postAddress.getStreetAddress());
        postAddressAPI.setPostCode(postAddress.getPostCode());
        postAddressAPI.setCity(postAddress.getCity());
        return postAddressAPI;
    }

    private static List<PostAddress> mapPostAddressAPIs(List<PostAddressAPI> postAddresseAPIs, Organization organization) {
        List<PostAddress> postAddresses = new ArrayList<>();
        for( PostAddressAPI postAddressAPI : postAddresseAPIs ) {
            PostAddress postAddress = mapPostAddressAPI(postAddressAPI);
            postAddress.setOrganization(organization);
            postAddresses.add(postAddress);
        }
        return postAddresses;
    }

    private static PostAddress mapPostAddressAPI(PostAddressAPI postAddressAPI ) {
        PostAddress postAddress = new PostAddress();
        postAddress.setAddressType(postAddressAPI.getAddressType());
        postAddress.setStreetAddress(postAddressAPI.getStreetAddress());
        postAddress.setPostCode(postAddressAPI.getPostCode());
        postAddress.setCity(postAddressAPI.getCity());
        return postAddress;
    }

    static List<CVCounty> mapCountyAPIs(List<CVCountyAPI> countyAPIs, Organization organization) {
        List<CVCounty> counties = new ArrayList<>();
        for( CVCountyAPI countyAPI : countyAPIs ) {
            CVCounty county = mapCountyAPI(countyAPI);
            //county.setOrganization(organization);
            counties.add(county);
        }
        return counties;
    }

    private static CVCounty mapCountyAPI(CVCountyAPI countyAPI ) {
        CVCounty county = new CVCounty();
        county.setCode(countyAPI.getCode());
        county.setName(countyAPI.getName());
        //county.setMunicipalities(AssortmentMapper.mapMunicipalities(countyAPI.getMunicipalities()));
        county.setUniqueId(countyAPI.getId());
        return county;
    }

    static List<CVMunicipality> mapMunicipalityAPIs(List<CVMunicipalityAPI> municipalityAPIs) {
        List<CVMunicipality> municipalities = new ArrayList<>();
        for( CVMunicipalityAPI municipalityAPI : municipalityAPIs ) {
            CVMunicipality municipality = mapMunicipalityAPI(municipalityAPI);
            municipalities.add(municipality);
        }
        return municipalities;
    }
    private static CVMunicipality mapMunicipalityAPI(CVMunicipalityAPI municipalityAPI) {
        CVMunicipality municipality = new CVMunicipality();
        municipality.setCode(municipalityAPI.getCode());
        municipality.setName(municipalityAPI.getName());
        municipality.setUniqueId(municipalityAPI.getId());
        return municipality;
    }

    public static boolean calculateActive(Date validFrom, Date validTo) {
        Date now = new Date();
        if( now.before(validFrom) ) {
            return false;
        } else if( validTo == null ) {
            return true;
        } else {
            return now.before(validTo);
        }
    }

}
