package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.EmailController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Product;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

/**
 * Class for validation functionality for articles
 *
 */
@Stateless
public class ArticleValidation {

    @Inject
    HjmtLogger LOG;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    ArticleController articleController;

    @Inject
    ProductController productController;

    @Inject
    EmailController emailController;

    @Inject
    UserController userController;

    @Inject
    AgreementPricelistRowController agreementPricelistRowController;

    @Inject
    OrganizationController organizationController;

    public void validateForCreate(ArticleAPI articleAPI, long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForCreate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForCreate(organizationUniqueId, articleAPI);

        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }
    }

    public Set<ErrorMessageAPI> tryForCreate(long organizationUniqueId, ArticleAPI articleAPI) {
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        validateArticleAPI(articleAPI, errorMessageAPIs);
        if( errorMessageAPIs.size() > 0 ) {
            // break early if indata is in itself incorrect
            return errorMessageAPIs;
        }
        // only web can be set in the electronic address of the manufacturer
        validateOnlyManufacturerWebSet(articleAPI, errorMessageAPIs);

        // first status cannot be DISCONTINUED
        /* Product.Status newStatus = Product.Status.valueOf(articleAPI.getStatus());

        if( newStatus == Product.Status.DISCONTINUED ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("status", validationMessageService.getMessage("article.statusChange.invalid")));
        } */

        validateArticleNumber(articleAPI, organizationUniqueId, null, errorMessageAPIs);
        return errorMessageAPIs;
    }

    public void validateForUpdate(ArticleAPI articleAPI, Article article, long organizationUniqueId, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForUpdate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForUpdate(organizationUniqueId, articleAPI, article,userAPI);

        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }
    }

    public Set<ErrorMessageAPI> tryForUpdate(long organizationUniqueId, ArticleAPI articleAPI, Article article, UserAPI userAPI) {
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        validateArticleAPI(articleAPI, errorMessageAPIs);
        validateArticleStatusForUpdate(article,articleAPI,errorMessageAPIs,userAPI);
        if( errorMessageAPIs.size() > 0 ) {
            // break early if indata is in itself incorrect
            return errorMessageAPIs;
        }
        // only web can be set in the electronic address of the manufacturer
        validateOnlyManufacturerWebSet(articleAPI, errorMessageAPIs);
        validateArticleNumber(articleAPI, organizationUniqueId, article, errorMessageAPIs);
        return errorMessageAPIs;
    }

    private void validateArticleStatusForUpdate(Article article, ArticleAPI articleAPI, Set<ErrorMessageAPI> errorMessageAPIs, UserAPI userAPI) {
        if(article.getStatus().toString().equalsIgnoreCase(Product.Status.DISCONTINUED.toString()) && articleAPI.getStatus().equalsIgnoreCase(Product.Status.PUBLISHED.toString())){
            //ska inte gå att redigera en utgången artikel -
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("replacementDate", validationMessageService.getMessage("import.productarticle.article.expiredDate")));
        }
        Product basedOn = article.getBasedOnProduct();
        //HJAL-2128 ska inte gå att redigera en artikel baserad på utgången produkt
        if(basedOn != null){
            if (basedOn.getStatus().toString().equalsIgnoreCase(Product.Status.DISCONTINUED.toString()) && article.getStatus().toString().equalsIgnoreCase(Product.Status.PUBLISHED.toString())) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("replacementDate", validationMessageService.getMessage("import.productarticle.article.basedOnProductExpiredDate")));
            }
            else{
                //articleController.sendEmailToCustomerOnChangedStatus(article,articleAPI,userAPI);
            }
        }
        else{
            //articleController.sendEmailToCustomerOnChangedStatus(article,articleAPI,userAPI);
        }
    }

    private void validateArticleAPI(ArticleAPI articleAPI, Set<ErrorMessageAPI> errorMessageAPIs) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<ArticleAPI>> constraintViolations = validator.validate(articleAPI, Default.class);
        if( constraintViolations != null && !constraintViolations.isEmpty() ) {
            for( ConstraintViolation constraintViolation : constraintViolations ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage()));
            }
        }
    }

    private void validateOnlyManufacturerWebSet(ArticleAPI articleAPI, Set<ErrorMessageAPI> errorMessageAPIs) {
        // only web can be set in the electronic address of the manufacturer
        if( articleAPI.getManufacturerElectronicAddress() != null ) {
            ElectronicAddressAPI electronicAddressAPI = articleAPI.getManufacturerElectronicAddress();
            if( !isEmpty(electronicAddressAPI.getEmail()) ||
                    !isEmpty(electronicAddressAPI.getFax()) ||
                    !isEmpty(electronicAddressAPI.getMobile()) ||
                    !isEmpty(electronicAddressAPI.getTelephone()) ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("web", validationMessageService.getMessage("article.manufacturer.onlyWeb")));
            }
        }
    }

    void validateArticleNumber(ArticleAPI articleAPI,
            long organizationUniqueId,
            Article article,
            Set<ErrorMessageAPI> errorMessageAPIs) {
        Article articleWithNumber = articleController.findByArticleNumberAndOrganization(articleAPI.getArticleNumber(), organizationUniqueId);
        if( articleWithNumber != null ) {
            if( article == null || !articleWithNumber.getUniqueId().equals(article.getUniqueId()) ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("articleNumber", validationMessageService.getMessage("article.number.notOrganizationUnique")));
            }
        }
        Product productWithNumber = productController.findByProductNumberAndOrganization(articleAPI.getArticleNumber(), organizationUniqueId);
        if( productWithNumber != null ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("articleNumber", validationMessageService.getMessage("article.product.number.notOrganizationUnique")));
        }
    }

    private boolean isEmpty(String value) {
        LOG.log( Level.FINEST, "value: {0}", new Object[] {value});
        if( value == null || value.isEmpty() ) {
            return true;
        }
        return false;
    }

}
