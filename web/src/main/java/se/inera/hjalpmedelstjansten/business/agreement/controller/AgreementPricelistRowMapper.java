package se.inera.hjalpmedelstjansten.business.agreement.controller;

import se.inera.hjalpmedelstjansten.business.product.controller.ArticleMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.PreventiveMaintenanceMapper;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.Article;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Class for mapping between API and Entity classes
 *
 */
public class AgreementPricelistRowMapper {

    public static final List<AgreementPricelistRowAPI> map(List<AgreementPricelistRow> pricelistRows, boolean includePricelist, boolean includeAgreement) {
        if( pricelistRows == null ) {
            return null;
        }
        List<AgreementPricelistRowAPI> pricelistRowAPIs = new ArrayList<>();
        for( AgreementPricelistRow  pricelistRow : pricelistRows ) {
            pricelistRowAPIs.add(map(pricelistRow, includePricelist, includeAgreement));
        }
        return pricelistRowAPIs;
    }

    public static final AgreementPricelistRowAPI map(AgreementPricelistRow pricelistRow, boolean includePricelist, boolean includeAgreement) {
        if( pricelistRow == null ) {
            return null;
        }
        AgreementPricelistRowAPI pricelistRowAPI = new AgreementPricelistRowAPI();
        pricelistRowAPI.setId(pricelistRow.getUniqueId());
        pricelistRowAPI.setArticle(ArticleMapper.map(pricelistRow.getArticle(), true, null));
        pricelistRowAPI.setStatus(pricelistRow.getStatus().toString());
        pricelistRowAPI.setPrice(pricelistRow.getPrice());
        pricelistRowAPI.setLeastOrderQuantity(pricelistRow.getLeastOrderQuantity());
        if( pricelistRow.getValidFrom() != null ) {
            pricelistRowAPI.setValidFrom(pricelistRow.getValidFrom().getTime());
        } else {
            pricelistRowAPI.setValidFrom(pricelistRow.getPricelist().getValidFrom().getTime());
        }

        pricelistRowAPI.setComment(pricelistRow.getComment());

        // possible overridden fields
        Article.Type articleType = pricelistRow.getArticle().getBasedOnProduct() == null ? pricelistRow.getArticle().getCategory().getArticleType(): pricelistRow.getArticle().getBasedOnProduct().getCategory().getArticleType();
        setDeliveryTime(pricelistRow, pricelistRowAPI, articleType);
        setWarrantyQuantity(pricelistRow, pricelistRowAPI, articleType);
        setWarrantyTerms(pricelistRow, pricelistRowAPI, articleType);
        setWarrantyQuantityUnit(pricelistRow, pricelistRowAPI, articleType);
        setWarrantyValidFrom(pricelistRow, pricelistRowAPI, articleType);

        if( includePricelist ) {
            pricelistRowAPI.setPricelist(AgreementPricelistMapper.mapWithStatus(pricelistRow.getPricelist(), includeAgreement, null, null));
        }
        return pricelistRowAPI;
    }

    public static final AgreementPricelistRow map(AgreementPricelistRowAPI pricelistRowAPI, Date validFrom) {
        if( pricelistRowAPI == null ) {
            return null;
        }
        AgreementPricelistRow pricelistRow = new AgreementPricelistRow();
        pricelistRow.setStatus(AgreementPricelistRow.Status.valueOf(pricelistRowAPI.getStatus()));
        pricelistRow.setPrice(pricelistRowAPI.getPrice());
        pricelistRow.setLeastOrderQuantity(pricelistRowAPI.getLeastOrderQuantity());
        pricelistRow.setValidFrom(validFrom);
        return pricelistRow;
    }

    public static final void mapUpdatableFields(AgreementPricelistRowAPI pricelistRowAPI, AgreementPricelistRow pricelistRow, Article.Type articleType, boolean isCreate) {
        if( pricelistRow.isValidFromOverridden() ) {
            pricelistRow.setValidFrom(pricelistRowAPI.getValidFrom() == null ? null: new Date(pricelistRowAPI.getValidFrom()));
        } else {
            boolean overridden = false;
            if( pricelistRowAPI.getValidFrom() != null ) {
                if( pricelistRow.getPricelist().getValidFrom() == null ) {
                    overridden = true;
                } else {
                    if( !pricelistRowAPI.getValidFrom().equals(pricelistRow.getPricelist().getValidFrom().getTime()) ) {
                        overridden = true;
                    }
                }
            }
            if( overridden ) {
                pricelistRow.setValidFrom(new Date(pricelistRowAPI.getValidFrom()));
                pricelistRow.setValidFromOverridden(true);
            }
        }

        setOverrideDeliveryTime(pricelistRowAPI, pricelistRow, articleType, isCreate);
        setOverrideWarrantyQuantity(pricelistRowAPI, pricelistRow, articleType, isCreate);
        setOverrideWarrantyTerms(pricelistRowAPI, pricelistRow, articleType, isCreate);
    }

    private static void setDeliveryTime(AgreementPricelistRow pricelistRow, AgreementPricelistRowAPI pricelistRowAPI, Article.Type articleType) {
        if( pricelistRow.isDeliveryTimeOverridden() ) {
            pricelistRowAPI.setDeliveryTime(pricelistRow.getDeliveryTime());
        } else {
            if( articleType == Article.Type.H ) {
                pricelistRowAPI.setDeliveryTime(pricelistRow.getPricelist().getAgreement().getDeliveryTimeH());
            } else if( articleType == Article.Type.I ) {
                pricelistRowAPI.setDeliveryTime(pricelistRow.getPricelist().getAgreement().getDeliveryTimeI());
            } else if( articleType == Article.Type.R ) {
                pricelistRowAPI.setDeliveryTime(pricelistRow.getPricelist().getAgreement().getDeliveryTimeR());
            } else if( articleType == Article.Type.T ) {
                pricelistRowAPI.setDeliveryTime(pricelistRow.getPricelist().getAgreement().getDeliveryTimeT());
            } else if( articleType == Article.Type.Tj ) {
                pricelistRowAPI.setDeliveryTime(pricelistRow.getPricelist().getAgreement().getDeliveryTimeTJ());
            }
        }
    }

    private static void setWarrantyQuantity(AgreementPricelistRow pricelistRow, AgreementPricelistRowAPI pricelistRowAPI, Article.Type articleType) {
        if( pricelistRow.isWarrantyQuantityOverridden() ) {
            pricelistRowAPI.setWarrantyQuantity(pricelistRow.getWarrantyQuantity());
        } else {
            if( articleType == Article.Type.H ) {
                pricelistRowAPI.setWarrantyQuantity(pricelistRow.getPricelist().getAgreement().getWarrantyQuantityH());
            } else if( articleType == Article.Type.I ) {
                pricelistRowAPI.setWarrantyQuantity(pricelistRow.getPricelist().getAgreement().getWarrantyQuantityI());
            } else if( articleType == Article.Type.R ) {
                pricelistRowAPI.setWarrantyQuantity(pricelistRow.getPricelist().getAgreement().getWarrantyQuantityR());
            } else if( articleType == Article.Type.T ) {
                pricelistRowAPI.setWarrantyQuantity(pricelistRow.getPricelist().getAgreement().getWarrantyQuantityT());
            } else if( articleType == Article.Type.Tj ) {
                pricelistRowAPI.setWarrantyQuantity(pricelistRow.getPricelist().getAgreement().getWarrantyQuantityTJ());
            }
        }
    }

    private static void setWarrantyTerms(AgreementPricelistRow pricelistRow, AgreementPricelistRowAPI pricelistRowAPI, Article.Type articleType) {
        if( pricelistRow.isWarrantyTermsOverridden() ) {
            pricelistRowAPI.setWarrantyTerms(pricelistRow.getWarrantyTerms());
        } else {
            if( articleType == Article.Type.H ) {
                pricelistRowAPI.setWarrantyTerms(pricelistRow.getPricelist().getAgreement().getWarrantyTermsH());
            } else if( articleType == Article.Type.I ) {
                pricelistRowAPI.setWarrantyTerms(pricelistRow.getPricelist().getAgreement().getWarrantyTermsI());
            } else if( articleType == Article.Type.R ) {
                pricelistRowAPI.setWarrantyTerms(pricelistRow.getPricelist().getAgreement().getWarrantyTermsR());
            } else if( articleType == Article.Type.T ) {
                pricelistRowAPI.setWarrantyTerms(pricelistRow.getPricelist().getAgreement().getWarrantyTermsT());
            } else if( articleType == Article.Type.Tj ) {
                pricelistRowAPI.setWarrantyTerms(pricelistRow.getPricelist().getAgreement().getWarrantyTermsTJ());
            }
        }
    }

    private static void setWarrantyQuantityUnit(AgreementPricelistRow pricelistRow, AgreementPricelistRowAPI pricelistRowAPI, Article.Type articleType) {
        if( pricelistRow.isWarrantyQuantityUnitOverridden() ) {
            pricelistRowAPI.setWarrantyQuantityUnit(GuaranteeUnitMapper.map(pricelistRow.getWarrantyQuantityUnit()));
        } else {
            if( articleType == Article.Type.H ) {
                pricelistRowAPI.setWarrantyQuantityUnit(GuaranteeUnitMapper.map(pricelistRow.getPricelist().getAgreement().getWarrantyQuantityHUnit()));
            } else if( articleType == Article.Type.I ) {
                pricelistRowAPI.setWarrantyQuantityUnit(GuaranteeUnitMapper.map(pricelistRow.getPricelist().getAgreement().getWarrantyQuantityIUnit()));
            } else if( articleType == Article.Type.R ) {
                pricelistRowAPI.setWarrantyQuantityUnit(GuaranteeUnitMapper.map(pricelistRow.getPricelist().getAgreement().getWarrantyQuantityRUnit()));
            } else if( articleType == Article.Type.T ) {
                pricelistRowAPI.setWarrantyQuantityUnit(GuaranteeUnitMapper.map(pricelistRow.getPricelist().getAgreement().getWarrantyQuantityTUnit()));
            } else if( articleType == Article.Type.Tj ) {
                pricelistRowAPI.setWarrantyQuantityUnit(GuaranteeUnitMapper.map(pricelistRow.getPricelist().getAgreement().getWarrantyQuantityTJUnit()));
            }
        }
    }

    private static void setWarrantyValidFrom(AgreementPricelistRow pricelistRow, AgreementPricelistRowAPI pricelistRowAPI, Article.Type articleType) {
        if( pricelistRow.isWarrantyValidFromOverridden() ) {
            pricelistRowAPI.setWarrantyValidFrom(pricelistRow.getWarrantyValidFrom() == null ? null: PreventiveMaintenanceMapper.map(pricelistRow.getWarrantyValidFrom()));
        } else {
            if( articleType == Article.Type.H ) {
                if( pricelistRow.getPricelist().getAgreement().getWarrantyValidFromH() != null ) {
                    pricelistRowAPI.setWarrantyValidFrom(PreventiveMaintenanceMapper.map(pricelistRow.getPricelist().getAgreement().getWarrantyValidFromH()));
                }
            } else if( articleType == Article.Type.I ) {
                if( pricelistRow.getPricelist().getAgreement().getWarrantyValidFromI() != null ) {
                    pricelistRowAPI.setWarrantyValidFrom(PreventiveMaintenanceMapper.map(pricelistRow.getPricelist().getAgreement().getWarrantyValidFromI()));
                }
            } else if( articleType == Article.Type.R ) {
                if( pricelistRow.getPricelist().getAgreement().getWarrantyValidFromR() != null ) {
                    pricelistRowAPI.setWarrantyValidFrom(PreventiveMaintenanceMapper.map(pricelistRow.getPricelist().getAgreement().getWarrantyValidFromR()));
                }
            } else if( articleType == Article.Type.T ) {
                if( pricelistRow.getPricelist().getAgreement().getWarrantyValidFromT() != null ) {
                    pricelistRowAPI.setWarrantyValidFrom(PreventiveMaintenanceMapper.map(pricelistRow.getPricelist().getAgreement().getWarrantyValidFromT()));
                }
            } else if( articleType == Article.Type.Tj ) {
                if( pricelistRow.getPricelist().getAgreement().getWarrantyValidFromTJ() != null ) {
                    pricelistRowAPI.setWarrantyValidFrom(PreventiveMaintenanceMapper.map(pricelistRow.getPricelist().getAgreement().getWarrantyValidFromTJ()));
                }
            }
        }
    }

    private static void setOverrideDeliveryTime(AgreementPricelistRowAPI pricelistRowAPI, AgreementPricelistRow pricelistRow, Article.Type articleType, boolean isCreate) {
        if( isCreate && pricelistRowAPI.getDeliveryTime() == null ) {
            // do nothing, default to override
        } else {
            if( pricelistRow.isDeliveryTimeOverridden() ) {
                pricelistRow.setDeliveryTime(pricelistRowAPI.getDeliveryTime());
            } else {
                boolean overridden = false;
                if( pricelistRowAPI.getDeliveryTime() != null ) {
                    if( articleType == Article.Type.H ) {
                        if( pricelistRow.getPricelist().getAgreement().getDeliveryTimeH() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getDeliveryTime().equals(pricelistRow.getPricelist().getAgreement().getDeliveryTimeH()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.I ) {
                        if( pricelistRow.getPricelist().getAgreement().getDeliveryTimeI() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getDeliveryTime().equals(pricelistRow.getPricelist().getAgreement().getDeliveryTimeI()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.R ) {
                        if( pricelistRow.getPricelist().getAgreement().getDeliveryTimeR() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getDeliveryTime().equals(pricelistRow.getPricelist().getAgreement().getDeliveryTimeR()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.T ) {
                        if( pricelistRow.getPricelist().getAgreement().getDeliveryTimeT() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getDeliveryTime().equals(pricelistRow.getPricelist().getAgreement().getDeliveryTimeT()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.Tj ) {
                        if( pricelistRow.getPricelist().getAgreement().getDeliveryTimeTJ() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getDeliveryTime().equals(pricelistRow.getPricelist().getAgreement().getDeliveryTimeTJ()) ) {
                                overridden = true;
                            }
                        }
                    }
                    if( overridden ) {
                        pricelistRow.setDeliveryTime(pricelistRowAPI.getDeliveryTime());
                        pricelistRow.setDeliveryTimeOverridden(true);
                    }
                } else {
                    if( articleType == Article.Type.H && pricelistRow.getPricelist().getAgreement().getDeliveryTimeH() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.I && pricelistRow.getPricelist().getAgreement().getDeliveryTimeI() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.R && pricelistRow.getPricelist().getAgreement().getDeliveryTimeR() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.T && pricelistRow.getPricelist().getAgreement().getDeliveryTimeT() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.Tj && pricelistRow.getPricelist().getAgreement().getDeliveryTimeTJ() != null ) {
                        overridden = true;
                    }
                    if( overridden ) {
                        pricelistRow.setDeliveryTime(null);
                        pricelistRow.setDeliveryTimeOverridden(true);
                    }
                }
            }
        }
    }

    private static void setOverrideWarrantyQuantity(AgreementPricelistRowAPI pricelistRowAPI, AgreementPricelistRow pricelistRow, Article.Type articleType, boolean isCreate) {
        if( isCreate && pricelistRowAPI.getWarrantyQuantity() == null ) {
            // do nothing, default to override
        } else {
            if( pricelistRow.isWarrantyQuantityOverridden() ) {
                pricelistRow.setWarrantyQuantity(pricelistRowAPI.getWarrantyQuantity());
            } else {
                boolean overridden = false;
                if( pricelistRowAPI.getWarrantyQuantity() != null ) {
                    if( articleType == Article.Type.H ) {
                        if( pricelistRow.getPricelist().getAgreement().getWarrantyQuantityH() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyQuantity().equals(pricelistRow.getPricelist().getAgreement().getWarrantyQuantityH()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.I ) {
                        if( pricelistRow.getPricelist().getAgreement().getWarrantyQuantityI() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyQuantity().equals(pricelistRow.getPricelist().getAgreement().getWarrantyQuantityI()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.R ) {
                        if( pricelistRow.getPricelist().getAgreement().getWarrantyQuantityR() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyQuantity().equals(pricelistRow.getPricelist().getAgreement().getWarrantyQuantityR()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.T ) {
                        if( pricelistRow.getPricelist().getAgreement().getWarrantyQuantityT() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyQuantity().equals(pricelistRow.getPricelist().getAgreement().getWarrantyQuantityT()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.Tj ) {
                        if( pricelistRow.getPricelist().getAgreement().getWarrantyQuantityTJ() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyQuantity().equals(pricelistRow.getPricelist().getAgreement().getWarrantyQuantityTJ()) ) {
                                overridden = true;
                            }
                        }
                    }
                    if( overridden ) {
                        pricelistRow.setWarrantyQuantity(pricelistRowAPI.getWarrantyQuantity());
                        pricelistRow.setWarrantyQuantityOverridden(true);
                    }
                } else {
                    if( articleType == Article.Type.H && pricelistRow.getPricelist().getAgreement().getWarrantyQuantityH() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.I && pricelistRow.getPricelist().getAgreement().getWarrantyQuantityI() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.R && pricelistRow.getPricelist().getAgreement().getWarrantyQuantityR() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.T && pricelistRow.getPricelist().getAgreement().getWarrantyQuantityT() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.Tj && pricelistRow.getPricelist().getAgreement().getWarrantyQuantityTJ() != null ) {
                        overridden = true;
                    }
                    if( overridden ) {
                        pricelistRow.setWarrantyQuantity(null);
                        pricelistRow.setWarrantyQuantityOverridden(true);
                    }
                }
            }
        }
    }

    private static void setOverrideWarrantyTerms(AgreementPricelistRowAPI pricelistRowAPI, AgreementPricelistRow pricelistRow, Article.Type articleType, boolean isCreate) {
        if( isCreate && pricelistRowAPI.getWarrantyTerms() == null ) {
            // do nothing, default to override
        } else {
            if( pricelistRowAPI.getWarrantyTerms() != null && pricelistRowAPI.getWarrantyTerms().isEmpty() ) {
                // empty string is same as null
                pricelistRowAPI.setWarrantyTerms(null);
            }
            if( pricelistRow.isWarrantyTermsOverridden() ) {
                pricelistRow.setWarrantyTerms(pricelistRowAPI.getWarrantyTerms());
            } else {
                boolean overridden = false;
                if( pricelistRowAPI.getWarrantyTerms() != null ) {
                    if( articleType == Article.Type.H ) {
                        if( pricelistRow.getPricelist().getAgreement().getWarrantyTermsH() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyTerms().equals(pricelistRow.getPricelist().getAgreement().getWarrantyTermsH()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.I ) {
                        if( pricelistRow.getPricelist().getAgreement().getWarrantyTermsI() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyTerms().equals(pricelistRow.getPricelist().getAgreement().getWarrantyTermsI()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.R ) {
                        if( pricelistRow.getPricelist().getAgreement().getWarrantyTermsR() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyTerms().equals(pricelistRow.getPricelist().getAgreement().getWarrantyTermsR()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.T ) {
                        if( pricelistRow.getPricelist().getAgreement().getWarrantyTermsT() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyTerms().equals(pricelistRow.getPricelist().getAgreement().getWarrantyTermsT()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.Tj ) {
                        if( pricelistRow.getPricelist().getAgreement().getWarrantyTermsTJ() == null ) {
                            overridden = true;
                        } else {
                            if( !pricelistRowAPI.getWarrantyTerms().equals(pricelistRow.getPricelist().getAgreement().getWarrantyTermsTJ()) ) {
                                overridden = true;
                            }
                        }
                    }
                    if( overridden ) {
                        pricelistRow.setWarrantyTerms(pricelistRowAPI.getWarrantyTerms());
                        pricelistRow.setWarrantyTermsOverridden(true);
                    }
                } else {
                    if( articleType == Article.Type.H && pricelistRow.getPricelist().getAgreement().getWarrantyTermsH() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.I && pricelistRow.getPricelist().getAgreement().getWarrantyTermsI() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.R && pricelistRow.getPricelist().getAgreement().getWarrantyTermsR() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.T && pricelistRow.getPricelist().getAgreement().getWarrantyTermsT() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.Tj && pricelistRow.getPricelist().getAgreement().getWarrantyTermsTJ() != null ) {
                        overridden = true;
                    }
                    if( overridden ) {
                        pricelistRow.setWarrantyTerms(null);
                        pricelistRow.setWarrantyTermsOverridden(true);
                    }
                }
            }
        }
    }

}
