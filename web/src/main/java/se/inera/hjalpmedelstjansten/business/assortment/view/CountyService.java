package se.inera.hjalpmedelstjansten.business.assortment.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.assortment.controller.CountyController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountyAPI;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Level;

/**
 * REST API for counties and municipalities Unit functionality.
 *
 */
@Stateless
@Path("counties")
@Interceptors({ PerformanceLogInterceptor.class })
public class CountyService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    private CountyController countyMunicipalityController;

    /**
     * Return all counties in the database.
     *
     * @return a list of <code>CVCountyMunicipalityAPI</code>
     */
    @GET
    @SecuredService(permissions = {"counties:view"})
    public Response getAllCounties() {
        LOG.log(Level.FINEST, "getAllCounties()");
        List<CVCountyAPI> countyAPIs = countyMunicipalityController.getAllCounties();
        return Response.ok(countyAPIs).build();
    }

    /**
     * Return all counties for a specific organization.
     *
     * @return a list of <code>CVCountyMunicipalityAPI</code>
     */
    @GET
    @Path("organization/{organizationId}")
    @SecuredService(permissions = {"counties:view"})
    public Response getCountiesForOrganization(@PathParam("organizationId") long organizationId) {
        LOG.log(Level.FINEST, "getCountiesForAssortment()");
        List<CVCountyAPI> countyAPIs = countyMunicipalityController.getCountiesForOrganization(organizationId);
        return Response.ok(countyAPIs).build();
    }

}
