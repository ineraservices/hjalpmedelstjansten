package se.inera.hjalpmedelstjansten.business;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;

import jakarta.inject.Inject;
import java.util.logging.Level;

public class BaseController {

    @Inject
    private HjmtLogger LOG;

    /**
     * Generate a custom exception based on the supplied message and exception.
     *
     * @param field
     * @param message the message
     * @param cause the cause of the exception
     * @param validationMessageService
     * @return a custom exception containing the message and cause.
     */
    protected HjalpmedelstjanstenException generateException( String field, String message, Throwable cause, ValidationMessageService validationMessageService ) {
        LOG.log(Level.FINEST, "generateException( ... )");
        HjalpmedelstjanstenException exception = null;
        if( cause != null ) {
            exception = new HjalpmedelstjanstenException("Request failed", cause);
        } else {
            exception = new HjalpmedelstjanstenException("Request failed");
        }
        exception.addValidationMessage(field, validationMessageService.getMessage(message));
        return exception;
    }

    /**
     * Generate an exception because of a forbidden action, will cause service to return
     * a response with HTTP status code FORBIDDEN
     *
     * @return a forbidden exception
     */
    protected HjalpmedelstjanstenException generateForbiddenException() {
        LOG.log(Level.FINEST, "generateException( ... )");
        HjalpmedelstjanstenException exception = new HjalpmedelstjanstenException("Request failed", HjalpmedelstjanstenException.Type.FORBIDDEN);
        return exception;
    }

}
