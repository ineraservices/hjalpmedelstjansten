package se.inera.hjalpmedelstjansten.business.organization.controller;

import static se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationMapper.mapCountyAPIs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Level;
import jakarta.ejb.Asynchronous;
import jakarta.ejb.Stateless;
import jakarta.enterprise.event.Event;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.DateUtils;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementController;
import se.inera.hjalpmedelstjansten.business.assortment.controller.CountyController;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.product.controller.CategoryController;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductController;
import se.inera.hjalpmedelstjansten.business.product.controller.SearchController;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.user.controller.EmailController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.PostAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.SearchProductsAndArticlesAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.UserEngagementAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.PostAddress;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCountry;

//import static se.inera.hjalpmedelstjansten.business.product.controller.ProductMapper.mapCEDirective;

/**
 * Business logic for organizations
 *
 */
@Stateless
public class OrganizationController {

    @Inject
    HjmtLogger LOG;

    @Inject
    Event<InternalAuditEvent> internalAuditEvent;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    CountryController countryController;

    @Inject
    CountyController countyController;

    @Inject
    UserController userController;

    @Inject
    ProductController productController;

    @Inject
    ArticleController articleController;

    @Inject
    SearchController searchController;

    @Inject
    CategoryController categoryController;

    @Inject
    GeneralPricelistController generalPricelistController;

    @Inject
    AgreementController agreementController;

    @Inject
    EmailController emailController;

    @Inject
    AuthHandler authHandler;

    @Inject
    OrganizationValidation organizationValidation;

    //@Inject
    //ElasticSearchController elasticSearchController;

    /**
     * Get the <code>OrganizationAPI</code> for the <code>Organization</code>
     * with the given id
     *
     * @param uniqueId unique id of the organization to get
     * @param userAPI
     * @param sessionId
     * @param requestIp
     * @return the corresponding <code>OrganizationAPI</code>
     */
    public OrganizationAPI getOrganizationAPI(long uniqueId, UserAPI userAPI, String sessionId, String requestIp) {
        LOG.log(Level.FINEST, "getOrganizationAPI( uniqueId: {0} )", new Object[] {uniqueId});
        Organization organization = getOrganization(uniqueId);
        if( organization != null ) {
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ORGANIZATION, InternalAudit.ActionType.VIEW, userAPI.getId(), sessionId, uniqueId, requestIp));
            return OrganizationMapper.map( organization, true );
        }
        return null;
    }

    /**
     * Get the <code>Organization</code> with the given id
     *
     * @param uniqueId unique id of the organization to get
     * @return the corresponding <code>Organization</code>
     */
    public Organization getOrganization(long uniqueId) {
        LOG.log(Level.FINEST, "getOrganization( uniqueId: {0} )", new Object[] {uniqueId});
        return em.find(Organization.class, uniqueId);
    }

    /**
     * Create a new <code>Organization</code> with the supplied values
     *
     * @param organizationAPI the user supplied values
     * @param userAPI current user's session information
     * @param sessionId
     * @return an <code>OrganizationAPI</code> for the created organization
     * @throws HjalpmedelstjanstenValidationException in case validation fails
     */
    public OrganizationAPI createOrganization(OrganizationAPI organizationAPI,
                                              UserAPI userAPI,
                                              String sessionId,
                                              String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createOrganization(...)");
        organizationValidation.validateForCreate(organizationAPI);
        Organization organization = OrganizationMapper.map(organizationAPI);
        // add country
        CVCountry country = countryController.getCountry(organizationAPI.getCountry().getId());
        organization.setCountry(country);
        organization.setMediaFolderName(UUID.randomUUID().toString());
        em.persist(organization);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ORGANIZATION, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, organization.getUniqueId(), requestIp));
        return OrganizationMapper.map( organization, true );
    }

    /**
     * Update the <code>Organization</code> with the given id with the supplied values.
     * Users in role Superadmin can modify most values of an <code>Organization</code> while
     * "local" admins only can update address information.
     *
     * @param uniqueId the unique id of the <code>Organization</code>
     * @param organizationAPI the user supplied values
     * @param updateOrganization whether user has permission to update organization basic info
     * @param userAPI current user's session information
     * @param sessionId
     * @param requestIp
     * @return the updated <code>OrganizationAPI</code>
     * @throws HjalpmedelstjanstenValidationException in case validation fails
     */
    public OrganizationAPI updateOrganization(long uniqueId,
                                              OrganizationAPI organizationAPI,
                                              boolean updateOrganization,
                                              UserAPI userAPI,
                                              String sessionId,
                                              String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateOrganization( uniqueId: {0} )", new Object[] {uniqueId});
        Organization dbOrganization = getOrganization(uniqueId);
        if( dbOrganization == null ) {
            return null;
        }
        Date oldValidTo = dbOrganization.getValidTo();
        organizationValidation.validateForUpdate(organizationAPI, dbOrganization);

        if( updateOrganization ) {
            updateOrganizationBasics(dbOrganization, organizationAPI);
        }

        updateOrganizationAddresses(dbOrganization, organizationAPI);

        Date validTo = organizationAPI.getValidTo() != null ? DateUtils.endOfDay(new Date(organizationAPI.getValidTo())) : null;

        if (validTo != null && !Objects.equals(oldValidTo, validTo)) {
            updateEndDateOnAll(organizationAPI, uniqueId, userAPI, requestIp);
        }

        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ORGANIZATION, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, uniqueId, requestIp));
        return OrganizationMapper.map( dbOrganization, true );
    }

    @Asynchronous
    @Transactional
    @TransactionTimeout(3600)
    private void updateEndDateOnAll(OrganizationAPI organizationAPI, long uniqueId, UserAPI userAPI, String requestIp) throws HjalpmedelstjanstenValidationException {
        //HJAL-2177
        //När man som superuser skriver in giltig t.o.m. på en organisation, skall organisationens samtliga
        // användare,
        // produkter,
        // artiklar,
        // avtal,
        // prislistor samt
        // GP
        // få samma giltig t.o.m.

        LOG.log(Level.FINEST, "New validTo ({0}) set on {1} {2}!", new Object[] {DateUtils.endOfDay(new Date(organizationAPI.getValidTo())), organizationAPI.getOrganizationType(), organizationAPI.getOrganizationName()});
        Date endDate = DateUtils.endOfDay(new Date(organizationAPI.getValidTo()));
        Long endDateLong = endDate.getTime();
        Long orgUniqueId = uniqueId;
        int i = 0;

        //användare, inga användare hittas om onlyActives: true fast organisationen har användare
        //det här behöver kollas på, men hur mycket påverkar en sån ändring  av queryn i userController.searchUsers?
        //Fixat så de hittas med onlyActives: true //Pelle
        List<UserAPI> users = userController.searchUsers(true, "", uniqueId, 0, 0, null);

        for (UserAPI user : users) {
            UserEngagement dbUserEngagement = userController.getUserEngagement(orgUniqueId, user.getId());
            Date thisDate = dbUserEngagement.getValidTo();
            if (thisDate != null) {
                if (thisDate.getTime() > endDateLong) {
                    dbUserEngagement.setValidTo(endDate);
                    i++;
                }
            }
            else {
                dbUserEngagement.setValidTo(endDate);
                i++;
            }
        }
        LOG.log(Level.FINEST, "New enddate set on {0} out of {1} users", new Object[] {i, users.size()});

        //endast om leverantör
        //if (organizationAPI.getOrganizationType() == Organization.OrganizationType.SUPPLIER.toString()) {
        if (organizationAPI.getOrganizationType().equalsIgnoreCase(Organization.OrganizationType.SUPPLIER.toString())) {
            //produkter


            //
            List<Product.Status> statuses = new ArrayList<>();
            statuses.add(Product.Status.PUBLISHED);

            LinkedList<Article.Type> articleTypes = new LinkedList<>();
            articleTypes.addAll(Arrays.asList(Article.Type.values()));

            //List<ProductAPI> productAPIs = productController.getProductsByOrganizations(orgUniqueId, statuses, articleTypes);
            List<Product> products = productController.getProductsByOrganization(orgUniqueId, statuses, articleTypes);
            i=0;
            if (products != null) {
                for (Product product : products) {
                    //Product product = productController.getProduct(orgUniqueId, productAPI.getId(), userAPI);
                    Date pDat = product.getReplacementDate();
                    //productAPI.setCeMarked(product.isCeMarked());
                    //productAPI.setCeDirective(mapCEDirective(product.getCeDirective()));

                    if (pDat != null) { //vi har redan ett utgångsdatum på produkten
                        if (pDat.getTime() >= endDateLong) { //produktens utgångsdatum ligger efter organisationens
                            product.setReplacementDate(endDate);
                            product.setInactivateRowsOnReplacement(true);
                            i++;
                        }
                    } else { //produkten saknar utgångsdatum
                        product.setReplacementDate(endDate);
                        product.setInactivateRowsOnReplacement(true);
                        i++;
                    }
                    List<CategorySpecificProperty> categorySpecificProperties = categoryController.getCategoryPropertys(product.getCategory().getUniqueId());
                }
                LOG.log(Level.FINEST, "New replacementdate set on {0} out of {1} products", new Object[] {i, products.size()});
            } else {
                LOG.log(Level.FINEST, "Inga produkter hos leverantör");
            }

            // artiklar
            LOG.log(Level.FINEST, "artiklar start");
            long startTime = System.nanoTime();
            int articleCount = (int) searchController.countArticlesByOrganization(orgUniqueId, null, statuses, articleTypes);
            List<SearchProductsAndArticlesAPI> searchProductsAndArticlesAPIs = searchController.searchArticlesByOrganization(orgUniqueId, null, statuses, articleTypes, 0, articleCount);
            i=0;
            if (searchProductsAndArticlesAPIs != null) {
                long startTimeLoop = System.nanoTime();
                for (SearchProductsAndArticlesAPI searchProductsAndArticlesAPI : searchProductsAndArticlesAPIs) {
                    Article article = articleController.getArticle(orgUniqueId, searchProductsAndArticlesAPI.getId(), userAPI);
                    Date aDate = article.getReplacementDate();
                    if (aDate != null) { //vi har redan ett utgångsdatum på artikeln
                        if (aDate.getTime() >= endDateLong) { //artikelns utgångsdatum ligger efter organisationens
                            article.setReplacementDate(endDate);
                            article.setInactivateRowsOnReplacement(true);
                            i++;
                        }
                    } else { //artikeln saknar utgångsdatum
                        article.setReplacementDate(endDate);
                        article.setInactivateRowsOnReplacement(true);
                        i++;
                    }
                }
                LOG.log(Level.FINEST, "New replacementdate set on {0} out of {1} articles ", new Object[] {i, searchProductsAndArticlesAPIs.size()});
                LOG.log(Level.FINEST, "articles done");
                LOG.log(Level.INFO, "XXX articles LOOP done: {0} sec",  new Object[]{(System.nanoTime() - startTimeLoop)/1000000000});
            } else {
                LOG.log(Level.FINEST, "Inga artiklar hos leverantör");
            }
            LOG.log(Level.INFO, "XXX articles done: {0} sec",  new Object[]{(System.nanoTime() - startTime)/1000000000});

            // inköpsavtal leverantör
            LOG.log(Level.FINEST, "inköpsavtal leverantör start");
            List<Agreement.Status> agreementStatuses = new ArrayList<>();
            agreementStatuses.add(Agreement.Status.FUTURE);
            agreementStatuses.add(Agreement.Status.CURRENT);
            List<AgreementAPI> agreementAPIs = agreementController.searchAgreementsForUpdateValidToFromOrganization(agreementStatuses, orgUniqueId, userAPI);
            i = 0;
            List<String> approversMailList = new ArrayList<>();
            if (agreementAPIs != null) {
                for (AgreementAPI agreementAPI : agreementAPIs) {
                    LOG.log(Level.FINEST, agreementAPI.getAgreementName());
                    Long agrValidTo = agreementAPI.getValidTo();
                    Long agrValidFrom = agreementAPI.getValidFrom();


                    if (agrValidTo > endDateLong) { //avtalets slutdatum ligger efter organisationens utgångsdatum
                        if (agrValidFrom > endDateLong) { //avtalet startar efter organisationens utgångsdatum, avsluta i förtid
                            agreementAPI.setEndDate(endDateLong);
                        }
                        else {
                            agreementAPI.setValidTo(endDateLong);
                        }
                        agreementController.updateAgreementValidToFromOrganization(orgUniqueId, agreementAPI.getId(), agreementAPI, userAPI, authHandler.getSessionId(), requestIp);
                        //skapa en lista över alla som godkänner prislista (minst en per avtal)
                        for (UserAPI approvers : agreementAPI.getCustomerPricelistApprovers()) {
                            if (!approversMailList.contains(approvers.getElectronicAddress().getEmail())) {
                                approversMailList.add(approvers.getElectronicAddress().getEmail());
                            }
                        }
                        i++;
                    }
                }
                LOG.log(Level.FINEST, "New validTo set on {0} out of {1} agreements ", new Object[]{i, agreementAPIs.size()});

                //skicka mail till alla som godkänner prislista
                try {
                    String mailSubject = String.format("%s inaktiveras %s", organizationAPI.getOrganizationName(), new SimpleDateFormat("yyyy-MM-dd").format(endDateLong));
                    String mailBody = String.format("I Hjälpmedelstjänsten kommer leverantören '%s' att inaktiveras %s. Detta medför att leverantörens samtliga artiklar, inköpsavtal och prislistor inte längre är tillgängliga från och med %s.", organizationAPI.getOrganizationName(), new SimpleDateFormat("yyyy-MM-dd").format(endDateLong), new SimpleDateFormat("yyyy-MM-dd").format(endDateLong));
                    for (String email : approversMailList) {
                        emailController.send(email, mailSubject, mailBody);
                    }
                } catch (Exception ex) {
                    LOG.log(Level.SEVERE, "Failed to send email on expired organization", ex);
                }
                LOG.log(Level.FINEST, "inköpsavtal leverantör done");
            } else {
                LOG.log(Level.FINEST, "Inga inköpsavtal hos leverantör");
            }
        }

        // inköpsavtal kund
        if (organizationAPI.getOrganizationType().equalsIgnoreCase(Organization.OrganizationType.CUSTOMER.toString())) {
            LOG.log(Level.FINEST, "inköpsavtal kund start");
            List<Agreement.Status> agreementStatuses = new ArrayList<>();
            agreementStatuses.add(Agreement.Status.FUTURE);
            agreementStatuses.add(Agreement.Status.CURRENT);
            List<AgreementAPI> agreementAPIs = agreementController.searchAgreementsForUpdateValidToFromOrganization(agreementStatuses, orgUniqueId, userAPI);
            // om kunden är "huvudkund": sätt slutdatum för avtalet
            i = 0;
            if (agreementAPIs != null) {
                for (AgreementAPI agreementAPI : agreementAPIs) {
                    LOG.log(Level.FINEST, agreementAPI.getAgreementName());
                    Long agrDate = agreementAPI.getValidTo();
                    if (agrDate > endDateLong) { //avtalets slutdatum ligger efter organisationens utgångsdatum
                        agreementAPI.setValidTo(endDateLong);
                        agreementController.updateAgreementValidToFromOrganization(orgUniqueId, agreementAPI.getId(), agreementAPI, userAPI, authHandler.getSessionId(), requestIp);
                        i++;
                    }
                }
                //skicka mail till lev? skicka mail till kunder som avtalet delas med?
                LOG.log(Level.FINEST, "New validTo set on {0} out of {1} agreements ", new Object[]{i, agreementAPIs.size()});

                // om kunden inte är "huvudkund", dvs avtalet delas med annan kund som är "huvudkund": ta bort kunden från avtalet (hur?)
                LOG.log(Level.FINEST, "inköpsavtal kund done");
            } else {
                LOG.log(Level.FINEST, "Inga inköpsavtal hos kund");
            }

        }

        // prislistor saknar validTo...

        // GP
        // max en per organisation
        LOG.log(Level.FINEST, "GP start");
        GeneralPricelistAPI generalPricelistAPI = generalPricelistController.getGeneralPricelistAPI(orgUniqueId, userAPI, authHandler.getSessionId(), requestIp);
        i = 0;
        if (generalPricelistAPI != null) {
            Long GPDate = generalPricelistAPI.getValidTo();
            if (GPDate > endDateLong) { //Generella prislistans slutdatum ligger efter organisationens utgångsdatum
                generalPricelistAPI.setValidTo(endDateLong);
                generalPricelistController.updateGeneralPricelist(orgUniqueId, generalPricelistAPI, userAPI, authHandler.getSessionId(), requestIp);
                i++;
            }
            LOG.log(Level.FINEST, "New validTo set on {0} out of 1 GP ", new Object[] {i});
        }
        else {
            LOG.log(Level.FINEST, "New validTo set on 0 out of 0 GP");
        }
        LOG.log(Level.FINEST, "GP done");
        LOG.log(Level.FINEST, "All done...");
    }

    /**
     * Update basic information about an organization.
     *
     * @param organization the organization to update
     * @param organizationAPI the user supplied values
     */
    private void updateOrganizationBasics( Organization organization, OrganizationAPI organizationAPI ) {
        LOG.log(Level.FINEST, "updateOrganizationBasics( ... )");
        organization.setOrganizationName(organizationAPI.getOrganizationName());
        organization.setGln(organizationAPI.getGln());
        organization.setValidFrom(new Date(organizationAPI.getValidFrom()));
        organization.setValidTo(organizationAPI.getValidTo() == null ? null: DateUtils.endOfDay(new Date(organizationAPI.getValidTo())));
        organization.setCounties(mapCountyAPIs(organizationAPI.getCounties(), organization));


    }

    /**
     * Update address information about an organization.
     *
     * @param organization the organization to update
     * @param organizationAPI the user supplied values
     */
    private void updateOrganizationAddresses( Organization organization, OrganizationAPI organizationAPI ) {
        LOG.log(Level.FINEST, "updateOrganizationAddresses( ... )");
        if( organizationAPI.getPostAddresses() != null && !organizationAPI.getPostAddresses().isEmpty() ) {
            for( PostAddressAPI postAddressAPI : organizationAPI.getPostAddresses() ) {
                PostAddress postAddress = getByAddressType(organization, postAddressAPI.getAddressType() );
                if( postAddress != null ) {
                    postAddress.setStreetAddress(postAddressAPI.getStreetAddress());
                    postAddress.setPostCode(postAddressAPI.getPostCode());
                    postAddress.setCity(postAddressAPI.getCity());
                }
            }
        }
        if( organizationAPI.getElectronicAddress() != null ) {
            if( organization.getElectronicAddress() != null ) {
                organization.getElectronicAddress().setEmail(organizationAPI.getElectronicAddress().getEmail());
                organization.getElectronicAddress().setFax(organizationAPI.getElectronicAddress().getFax());
                organization.getElectronicAddress().setMobile(organizationAPI.getElectronicAddress().getMobile());
                organization.getElectronicAddress().setTelephone(organizationAPI.getElectronicAddress().getTelephone());
                organization.getElectronicAddress().setWeb(organizationAPI.getElectronicAddress().getWeb());
            }
        }
    }

    /**
     * Get the <code>PostAddress</code> of the specified type for the given organization.
     *
     * @param organization the organization
     * @param addressType the addressType to find
     * @return the PostAddress for the given addressType
     */
    private PostAddress getByAddressType( Organization organization, PostAddress.AddressType addressType ) {
        LOG.log(Level.FINEST, "getByAddressType( addressType: {0} )", new Object[] {addressType});
        if( organization.getPostAddresses() != null && !organization.getPostAddresses().isEmpty() ) {
            for( PostAddress postAddress : organization.getPostAddresses() ) {
                if( postAddress.getAddressType().equals(addressType) ) {
                    return postAddress;
                }
            }
        }
        return null;
    }

    /**
     * Search organizations for the user supplied query. Search is paginated so only
     * results valid for given offset and limit is returned.
     *
     * @param queryString user supplied query
     * @param organizationTypes types of organizations to return
     * @param offset start returning from this search result
     * @param limit maximum number of search results to return
     * @return a list of <code>OrganizationAPI</code> matching the query parameters
     */
    public SearchDTO searchOrganizations(String queryString, List<Organization.OrganizationType> organizationTypes, int offset, int limit) {
        LOG.log(Level.FINEST, "searchOrganizations( organizationTypes: {0}, offset: {1}, limit: {2} )", new Object[] {organizationTypes, offset, limit});
        StringBuilder countQueryBuilder = new StringBuilder();
        countQueryBuilder.append("SELECT COUNT(o) FROM Organization o WHERE o.organizationType IN :organizationTypes ");
        if( queryString != null && !queryString.isEmpty() ) {
            countQueryBuilder.append("AND (o.organizationName LIKE :query OR o.organizationNumber LIKE :query) ");
        }
        String countQueryString = countQueryBuilder.toString();
        Query countQuery = em.createQuery(countQueryString);
        countQuery.setParameter("organizationTypes", organizationTypes);
        if( queryString != null && !queryString.isEmpty() ) {
            countQuery.setParameter("query", "%" + queryString + "%");
        }
        long countOrganizations = (long) countQuery.getSingleResult();

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT o FROM Organization o WHERE o.organizationType IN :organizationTypes ");
        if( queryString != null && !queryString.isEmpty() ) {
           queryBuilder.append("AND (o.organizationName LIKE :query OR o.organizationNumber LIKE :query) ");
        }
        queryBuilder.append("ORDER BY o.organizationName ASC ");
        String querySql = queryBuilder.toString();
        LOG.log( Level.FINEST, "querySql: {0}", new Object[] {querySql});
        Query query = em.createQuery(querySql);
        query.setParameter("organizationTypes", organizationTypes);
        if( queryString != null && !queryString.isEmpty() ) {
            query.setParameter("query", "%" + queryString + "%");
        }

        if( limit > 0 ) {
            query.setMaxResults(limit);
        }
        List<Organization> organizations = query.setFirstResult(offset).
                getResultList();
        return new SearchDTO(countOrganizations, OrganizationMapper.map(organizations));
    }

    public SearchDTO searchValidOrganizations(String queryString, List<Organization.OrganizationType> organizationTypes, int offset, int limit) {
        LOG.log(Level.FINEST, "searchValidOrganizations( organizationTypes: {0}, offset: {1}, limit: {2} )", new Object[] {organizationTypes, offset, limit});
        long countOrganizations = countSearchValidOrganizations(queryString, organizationTypes);
        Query query = em.createNamedQuery(Organization.SEARCH_VALID).
                setParameter("query", "%" + queryString + "%").
                setParameter("organizationTypes", organizationTypes);
        if( limit > 0 ) {
            query.setMaxResults(limit);
        }
        List<Organization> organizations = query.setFirstResult(offset).
                getResultList();
        return new SearchDTO(countOrganizations, OrganizationMapper.map(organizations));
    }

    /**
     * Counts the total number of organizations that matches a user supplied search query
     *
     * @param query user supplied query
     * @param organizationTypes types of organizations to count
     * @return total number of organizations matching the query parameters
     */
    private long countSearchOrganizations(String query, List<Organization.OrganizationType> organizationTypes) {
        LOG.log(Level.FINEST, "countSearchOrganizations( organizationTypes: {0} )", new Object[] {organizationTypes});
        return  (Long) em.createNamedQuery(Organization.COUNT_SEARCH).
                setParameter("query", "%" + query + "%").
                setParameter("organizationTypes", organizationTypes).
                getSingleResult();
    }

    private long countSearchValidOrganizations(String query, List<Organization.OrganizationType> organizationTypes) {
        LOG.log(Level.FINEST, "countSearchValidOrganizations( organizationTypes: {0} )", new Object[] {organizationTypes});
        return  (Long) em.createNamedQuery(Organization.COUNT_SEARCH_VALID).
                setParameter("query", "%" + query + "%").
                setParameter("organizationTypes", organizationTypes).
                getSingleResult();
    }

    /**
     * Delete a specific organization. Only organizations with additional data,
     * like users can be deleted.
     *
     * @param uniqueId the unique id of the organization
     * @param userAPI
     * @param sessionId
     * @throws HjalpmedelstjanstenValidationException in case validation fails, e.g. organization has users
     */
    public void deleteOrganization(long uniqueId, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteOrganization( uniqueId: {0} )", new Object[] {uniqueId});
        Organization organization = getOrganization(uniqueId);
        organizationValidation.validateForDelete(organization);
        em.remove(organization);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ORGANIZATION, InternalAudit.ActionType.DELETE, userAPI.getId(), sessionId, uniqueId, requestIp));
    }

    /**
     * Find organizations for a specific organization number and type.
     *
     * @param organizationNumber the organization number
     * @param organizationType type of the organization
     * @return a list of organizations matching the gln and type. this should only be one
     */
    public List<Organization> findByOrganizationNumberAndType(String organizationNumber, Organization.OrganizationType organizationType) {
        LOG.log(Level.FINEST, "findByOrganizationNumberAndType( organizationNumber: {0}, organizationType: {1} )", new Object[] {organizationNumber, organizationType});
        return em.createNamedQuery(Organization.FIND_BY_ORGANIZATION_NUMBER_AND_TYPE).
                setParameter("organizationNumber", organizationNumber).
                setParameter("type", organizationType).
                getResultList();
    }

    /**
     * Find organizations for a specific organization gln and type.
     *
     * @param gln the organization gln
     * @param organizationType type of the organization
     * @return a list of organizations matching the gln and type. this should only be one
     */
    public List<Organization> findByGlnAndType(String gln, Organization.OrganizationType organizationType) {
        LOG.log(Level.FINEST, "findByGlnAndType( gln: {0}, type: {1} )", new Object[] {organizationType, organizationType});
        return em.createNamedQuery(Organization.FIND_BY_GLN_AND_TYPE).
                setParameter("gln", gln).
                setParameter("type", organizationType).
                getResultList();
    }

    /**
     *
     * @param userAPI
     * @return
     */
    public Organization getOrganizationFromLoggedInUser(UserAPI userAPI) {
        UserEngagementAPI userEngagementAPI = userAPI.getUserEngagements().get(0);
        Long loggedInUserOrganizationId = userEngagementAPI.getOrganizationId();
        Organization organization = getOrganization(loggedInUserOrganizationId);
        return organization;
    }


    public List<Long> findByValidTo(){
        LOG.log(Level.FINEST, "findByValidTo()");
        return em.createNamedQuery(Organization.FIND_BY_VALID_TO).
                setParameter("date", new Date()).
                getResultList();
    }



}
