package se.inera.hjalpmedelstjansten.business.user.domain.mapper;

import jakarta.enterprise.context.ApplicationScoped;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import se.inera.hjalpmedelstjansten.business.user.domain.NotificationRequest;
import se.inera.hjalpmedelstjansten.business.user.domain.NotificationResponse;
import se.inera.hjalpmedelstjansten.model.dto.NotificationDTO;

@ApplicationScoped
public class NotificationMapperImpl implements NotificationMapper {

    @Override
    public NotificationDTO toDTO(NotificationRequest request) {

        NotificationDTO notificationDTO = new NotificationDTO();

        if (request != null) {
            notificationDTO.setUserId(request.getUserId());
            notificationDTO.setOrganizationId(request.getOrganizationId());
            Set<String> set = request.getUserNotifications();
            if (set != null) {
                notificationDTO.setUserNotificationsStringSet(new LinkedHashSet<String>(set));
            }
        }

        return notificationDTO;
    }

    @Override
    public NotificationResponse toResponse(NotificationDTO dto) {

        NotificationResponse notificationResponse = new NotificationResponse();

        if (dto != null) {
            Map<String, String> map = dto.getError();
            if (map != null) {
                notificationResponse.setErrors(new LinkedHashMap<String, String>(map));
            }
            Set<String> set = dto.getUserNotificationsStringSet();
            if (set != null) {
                notificationResponse.setUserNotifications(new LinkedHashSet<String>(set));
            }
            Map<String, String> map1 = dto.getAllNotificationsMap();
            if (map1 != null) {
                notificationResponse.setAllNotifications(new LinkedHashMap<String, String>(map1));
            }
        }

        return notificationResponse;
    }
}
