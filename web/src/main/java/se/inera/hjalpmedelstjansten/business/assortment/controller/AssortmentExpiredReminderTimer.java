package se.inera.hjalpmedelstjansten.business.assortment.controller;

import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.assortment.controller.AssortmentController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.clustering.ClusterController;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import jakarta.ejb.*;
import jakarta.inject.Inject;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * Timer for sending email-reminder regarding expired assortments
 *
 */
@Singleton
@Startup
@DependsOn("PropertyLoader")
public class AssortmentExpiredReminderTimer {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private boolean assortmentExpiredReminderTimerEnabled;

    @Inject
    private String assortmentExpiredReminderTimerHour;

    @Inject
    private String assortmentExpiredReminderTimerMinute;

    @Inject
    private String assortmentExpiredReminderTimerSecond;

    @Inject
    private AssortmentController assortmentController;

    @Inject
    private ClusterController clusterController;

    @Resource
    private TimerService timerService;

    @Timeout
    @TransactionTimeout(unit=TimeUnit.MINUTES, value=20)
    private void schedule() {
        LOG.log(Level.FINEST, "schedule assortment reminder");
        //attempt to get lock
        boolean lockReceived = clusterController.getLock(this.getClass().getName(), 30);
        if (lockReceived) {
            LOG.log(Level.FINEST, "Received lock");
            long start = System.currentTimeMillis();
            assortmentController.sendEmailAssortmentExpired();
            long total = System.currentTimeMillis() - start;
            if( total > 60000) {
                LOG.log(Level.WARNING,"Scheduled job took: {0} ms which is more than 1 minute which is a long time, a developer should take a look at this.", new Object[] {total} );
            } else {
                LOG.log(Level.INFO, "Scheduled job took: {0} ms.", new Object[] {total});
            }
            //release lock
            clusterController.releaseLock(this.getClass().getName());
        } else {
            LOG.log(Level.INFO, "Did not receive lock");
        }
    }

    @PostConstruct
    private void initialize() {
        LOG.log(Level.FINEST, "initialize()");
        if(assortmentExpiredReminderTimerEnabled) {
            LOG.log(Level.FINEST, "Timer is enabled");
            if(timerService.getTimers().isEmpty()) {
                String name = this.getClass().getName();
                TimerConfig configuration = new TimerConfig();
                configuration.setPersistent(false);
                configuration.setInfo(name);
                ScheduleExpression scheduleExpression = new ScheduleExpression();
                scheduleExpression.
                        hour(assortmentExpiredReminderTimerHour).
                        minute(assortmentExpiredReminderTimerMinute).
                        second(assortmentExpiredReminderTimerSecond);
                timerService.createCalendarTimer(scheduleExpression, configuration);
            }
        } else {
            LOG.log(Level.INFO, "Timer is NOT enabled");
        }
    }


}
