package se.inera.hjalpmedelstjansten.business.security.controller;

import org.apache.shiro.authc.UsernamePasswordToken;


public class TokenUserToken extends UsernamePasswordToken {

    public TokenUserToken(String username, String password) {
        super(username, password);
    }
        
}
