package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPackageUnitAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

@Stateless
public class PackageUnitController {

    @Inject
    private HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;

    public List<CVPackageUnitAPI> getAllUnits() {
        LOG.log(Level.FINEST, "getAllUnits()");
        return PackageUnitMapper.map(findAll());
    }

    public List<CVPackageUnit> findAll() {
        LOG.log(Level.FINEST, "findAll()");
        return em.createNamedQuery(CVPackageUnit.FIND_ALL).getResultList();
    }

    public CVPackageUnit getUnit(long uniqueId) {
        LOG.log(Level.FINEST, "getUnit( uniqueId: {0} )", new Object[] {uniqueId});
        return em.find(CVPackageUnit.class, uniqueId);
    }

    public CVPackageUnit findByName(String packageUnitName) {
        LOG.log(Level.FINEST, "findByName( packageUnitName: {0} )", new Object[] {packageUnitName});
        List<CVPackageUnit> packageUnits = em.createNamedQuery(CVPackageUnit.FIND_BY_NAME).
                setParameter("name", packageUnitName).
                getResultList();
        // package unit names are unique so either we get one or nothing
        if( packageUnits != null && !packageUnits.isEmpty() ) {
            return packageUnits.get(0);
        }
        return null;
    }

    public Map<Long, CVPackageUnit> findAllAsIdMap() {
        Map<Long, CVPackageUnit> packageUnitsIdMap = new HashMap<>();
        List<CVPackageUnit> allUnits = findAll();
        if( allUnits != null && !allUnits.isEmpty() ) {
            allUnits.forEach((orderUnit) -> {
                packageUnitsIdMap.put(orderUnit.getUniqueId(), orderUnit);
            });
        }
        return packageUnitsIdMap;
    }

    public Map<Long, CVPackageUnit> getAllAsIdMap(List<CVPackageUnit> allUnits) {
        Map<Long, CVPackageUnit> packageUnitsIdMap = new HashMap<>();
        if( allUnits != null && !allUnits.isEmpty() ) {
            allUnits.forEach((packageUnit) -> {
                packageUnitsIdMap.put(packageUnit.getUniqueId(), packageUnit);
            });
        }
        return packageUnitsIdMap;
    }

}
