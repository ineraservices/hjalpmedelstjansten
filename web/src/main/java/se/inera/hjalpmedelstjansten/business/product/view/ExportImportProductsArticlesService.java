package se.inera.hjalpmedelstjansten.business.product.view;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.jboss.ejb3.annotation.TransactionTimeout;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.product.controller.exportimport.ExportProductsArticlesController;
import se.inera.hjalpmedelstjansten.business.product.controller.exportimport.ImportProductsArticlesController;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;

@Stateless
@Path("organizations/{organizationUniqueId}/products/")
@Interceptors({ PerformanceLogInterceptor.class })
public class ExportImportProductsArticlesService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    private AuthHandler authHandler;

    @EJB
    private ExportProductsArticlesController exportController;

    @EJB
    private ImportProductsArticlesController importController;

    /**
     * Export products and articles to Excel
     *
     * @param httpServletRequest
     * @param organizationUniqueId
     * @param categoryIds
     * @param productIds
     * @param includeProductsAndArticles
     * @return
     * @throws HjalpmedelstjanstenValidationException
     */
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @Path("export")
    @SecuredService(permissions = {"productarticleexport:view"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportProductsAndArticles(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @QueryParam("category") List<Long> categoryIds,
            @QueryParam("product") List<Long> productIds,
            @DefaultValue("false") @QueryParam("includeProductsAndArticles") boolean includeProductsAndArticles,
            @DefaultValue("false") @QueryParam("includeT") boolean includeT,
            @DefaultValue("false") @QueryParam("includeTj") boolean includeTj,
            @DefaultValue("false") @QueryParam("includeI") boolean includeI,
            @DefaultValue("false") @QueryParam("includeR") boolean includeR,
            @DefaultValue("false") @QueryParam("pIncludeA") boolean pIncludeA,
            @DefaultValue("false") @QueryParam("pIncludeT") boolean pIncludeT,
            @DefaultValue("false") @QueryParam("pIncludeTj") boolean pIncludeTj,
            @DefaultValue("false") @QueryParam("pIncludeI") boolean pIncludeI,
            @DefaultValue("false") @QueryParam("pIncludeR") boolean pIncludeR,
            @DefaultValue("false") @QueryParam("includePublished") boolean includePublished,
            @DefaultValue("false") @QueryParam("includeDiscontinued") boolean includeDiscontinued) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "exportProductsAndArticles( organizationUniqueId: {0}, categoryIds: {1}, productIds: {2} )", new Object[] {organizationUniqueId, categoryIds, productIds});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
        String filename = "Export_PoA_via_Kategori_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";
        if (productIds.size() != 0)
            filename = "Export_PoA_via_Produkt:" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";
        Set<Long> noDuplicatesCategories = new HashSet<>(categoryIds);
        byte[] exportBytes = exportController.exportFile(organizationUniqueId, noDuplicatesCategories, productIds, includeProductsAndArticles,includeT,includeTj,includeI,includeR, pIncludeA,pIncludeT,pIncludeTj,pIncludeI,pIncludeR, includePublished, includeDiscontinued, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        LOG.log( Level.FINEST, "Content-Length: {0}", new Object[] {exportBytes.length});
        return Response.ok(
                exportBytes,
                jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM).
                header("Content-Disposition", "attachment; filename=" + filename).
                header("Content-Length", exportBytes.length).
                build();
    }

    /**
     * Import products and articles from Excel
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization
     * @param multipartFormDataInput user uploaded file
     * @return a 200 response
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("import")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @SecuredService(permissions = {"productarticleimport:view"})
    @TransactionTimeout(value=40, unit = TimeUnit.MINUTES)
    public Response importProductsAndArticles(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            MultipartFormDataInput multipartFormDataInput) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "importProductsAndArticles( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        importController.importFile(organizationUniqueId, multipartFormDataInput, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        return Response.ok().build();
    }

}
