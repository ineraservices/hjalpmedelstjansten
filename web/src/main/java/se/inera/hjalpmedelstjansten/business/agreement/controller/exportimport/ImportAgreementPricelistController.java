package se.inera.hjalpmedelstjansten.business.agreement.controller.exportimport;

import jakarta.annotation.Resource;
import jakarta.ejb.EJBContext;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ooxml.POIXMLProperties;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.openxmlformats.schemas.officeDocument.x2006.customProperties.CTProperty;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.MdcToolkit;
import se.inera.hjalpmedelstjansten.business.media.controller.FileUploadValidationController;
import se.inera.hjalpmedelstjansten.business.product.controller.exportimport.ExportImportProductsArticlesController;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelist;

/**
 * Class for handling business logic of importing.
 *
 */
@Stateless
public class ImportAgreementPricelistController extends ExportImportPricelistController {

    @Inject
    HjmtLogger LOG;

    @Inject
    FileUploadValidationController fileUploadValidationController;

    @Inject
    ImportAgreementPricelistWriterAsynchController importPricelistWriterAsynchController;

    @Resource
    EJBContext context;

    public Boolean importAgreementPricelistFile(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, MultipartFormDataInput multipartFormDataInput, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "importAgreementPricelistFile( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId} );
        AgreementPricelist agreementPricelist = agreementPricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( agreementPricelist == null ) {
            return null;
        }
        InputStream inputStream = null;
        try {
            List<InputPart> inputParts = multipartFormDataInput.getFormDataMap().get("file");
            String contentDispositionHeader = inputParts.get(0).getHeaders().getFirst("Content-Disposition");
            String fileName = fileUploadValidationController.getFileName(contentDispositionHeader);
            String fileEnding = fileUploadValidationController.getFileEnding(fileName);
            fileUploadValidationController.validateImportFileEnding(fileEnding);
            inputStream = inputParts.get(0).getBody(InputStream.class,null);
            XSSFWorkbook workbook = (XSSFWorkbook) WorkbookFactory.create(inputStream);

            validateWorkbook(workbook);
            importPricelistWriterAsynchController.readAndSaveItems(agreementPricelist, organizationUniqueId, agreementUniqueId, pricelistUniqueId, workbook, userAPI, sessionId, requestIp, fileName, MdcToolkit.copyOfMdc());

        } catch (IOException | EncryptedDocumentException ex) {
            LOG.log(Level.SEVERE, "Failed to handle upload", ex);
            throw validationMessageService.generateValidationException("file", "import.productarticle.upload.fail");
        } finally {
            try {
                if( inputStream != null ) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Failed to close inputstream on upload", ex);
            }
        }
        return true;
    }

    private void validateWorkbook(XSSFWorkbook workbook) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateWorkbook(...)");

        POIXMLProperties properties = workbook.getProperties();
        POIXMLProperties.CustomProperties customProperties = properties.getCustomProperties();

        // validate type, this is to avoid users uploading pricelist rows to products upload
        // and vice versa
        CTProperty workbookExportTypeProperty = customProperties.getProperty(ExportImportProductsArticlesController.EXPORT_TYPE_NAME);
        if( workbookExportTypeProperty == null ) {
            throw validationMessageService.generateValidationException("file", "import.pricelist.invalidfile.missingtype");
        }
        String exportType = workbookExportTypeProperty.getLpwstr();
        if( exportType == null || !EXPORT_TYPE_AGREEMENT.equals(exportType) ) {
            throw validationMessageService.generateValidationException("file", "import.pricelist.invalidfile.invalidtype");
        }

        // validate version of excel file, version is an own concept that each time we make a change
        // that may break backwards compatibility we change version to avoid users to
        // use old excel files
        CTProperty workbookExportVersionProperty = customProperties.getProperty(ExportImportProductsArticlesController.EXPORT_VERSION_NAME);
        if( workbookExportVersionProperty == null ) {
            throw validationMessageService.generateValidationException("file", "import.pricelist.invalidfile.invalidversion");
        }
        String exportVersion = workbookExportVersionProperty.getLpwstr();
        if( exportVersion == null || !EXPORT_VERSION.equals(exportVersion) ) {
            throw validationMessageService.generateValidationException("file", "import.pricelist.invalidfile.invalidversion");
        }

        // workbook must have more than 1 sheet
        LOG.log( Level.FINEST, "Workbook contains {0} sheets", new Object[] {workbook.getNumberOfSheets()});
        if( workbook.getNumberOfSheets() < 2 ) {
            throw validationMessageService.generateValidationException("file", "import.pricelist.invalidfile.numberofsheets");
        }
        if( workbook.getSheet(VALUELIST_SHEET_NAME) == null ) {
            throw validationMessageService.generateValidationException("file", "import.pricelist.invalidfile.novaluelistsheet");
        }
    }

    /**
     * Add an error message for a specific row. Instead of having
     * to correct the row number for each call, we add 1 here.
     *
     * @param exception
     * @param rowNumber
     * @param field
     * @param message
     */
    public void addRowErrorMessage(HjalpmedelstjanstenValidationException exception, int rowNumber, String field, String message) {
        String errorOnRowMessage = validationMessageService.getMessage("import.pricelist.errorOnRow", rowNumber+1 );
        exception.addValidationMessage(field, errorOnRowMessage + " " + message);
    }


}
