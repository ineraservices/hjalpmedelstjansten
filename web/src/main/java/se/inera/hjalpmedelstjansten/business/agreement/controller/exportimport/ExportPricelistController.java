package se.inera.hjalpmedelstjansten.business.agreement.controller.exportimport;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import org.apache.poi.ooxml.POIXMLProperties;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFName;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowMapper;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistRowMapper;
import se.inera.hjalpmedelstjansten.business.helpers.ExportHelper;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.exportimport.ExportImportProductsArticlesController;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelist;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

/**
 * Class for handling business logic of exporting agreement pricelist rows.
 *
 */
@Stateless
public class ExportPricelistController extends ExportImportPricelistController {

    @Inject
    HjmtLogger LOG;

    @Inject
    ExportHelper exportHelper;

    @Inject
    OrganizationController organizationController;

    public byte[] exportGeneralPricelistFile(String discontinued, long organizationUniqueId, long pricelistUniqueId, UserAPI userAPI, String sessionId, String requestIp ) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "exportGeneralPricelistFile( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[] {organizationUniqueId, pricelistUniqueId} );
        GeneralPricelist pricelist = generalPricelistController.getGeneralPricelist(organizationUniqueId);
        boolean isAgreementPriceList = false;
        if( pricelist == null ) {
            return null;
        }
        //List<GeneralPricelistPricelistRow.Status> statuses = new ArrayList<>();
        //statuses.add(GeneralPricelistPricelistRow.Status.ACTIVE);

        if (discontinued.equalsIgnoreCase("discontinued")) {
            List<GeneralPricelistPricelistRow> generalPricelistPricelistRows = em.createNamedQuery(GeneralPricelistPricelistRow.FIND_BY_GENERAL_PRICELIST_PRICELIST_AND_DISCONTINUED).
                    setParameter("generalPricelistPricelistId", pricelistUniqueId).
                    getResultList();
            List<GeneralPricelistPricelistRowAPI> generalPricelistPricelistRowAPIs = GeneralPricelistPricelistRowMapper.map(generalPricelistPricelistRows, true);
            return exportPricelistFile(null, generalPricelistPricelistRowAPIs, organizationUniqueId, pricelistUniqueId, isAgreementPriceList);
        }
        else if (discontinued.equalsIgnoreCase("inProduction")) {
            List<GeneralPricelistPricelistRow> generalPricelistPricelistRows = em.createNamedQuery(GeneralPricelistPricelistRow.FIND_BY_GENERAL_PRICELIST_PRICELIST_AND_NOT_DISCONTINUED).
                    setParameter("generalPricelistPricelistId", pricelistUniqueId).
                    getResultList();
            List<GeneralPricelistPricelistRowAPI> generalPricelistPricelistRowAPIs = GeneralPricelistPricelistRowMapper.map(generalPricelistPricelistRows, true);
            return exportPricelistFile(null, generalPricelistPricelistRowAPIs, organizationUniqueId, pricelistUniqueId, isAgreementPriceList);
        }
        else // (discontinued.equalsIgnoreCase("all"))
        {
            List<GeneralPricelistPricelistRow> generalPricelistPricelistRows = em.createNamedQuery(GeneralPricelistPricelistRow.FIND_BY_GENERAL_PRICELIST_PRICELIST).
                    setParameter("generalPricelistPricelistId", pricelistUniqueId).
                    getResultList();
            List<GeneralPricelistPricelistRowAPI> generalPricelistPricelistRowAPIs = GeneralPricelistPricelistRowMapper.map(generalPricelistPricelistRows, true);
            return exportPricelistFile(null, generalPricelistPricelistRowAPIs, organizationUniqueId, pricelistUniqueId, isAgreementPriceList);
        }
    }


    public byte[] exportAgreementPricelistFile(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, UserAPI userAPI, String sessionId, String requestIp ) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "exportAgreementPricelistFile( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId} );
        AgreementPricelist pricelist = agreementPricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist == null ) {
            return null;
        }
        List<AgreementPricelistRow.Status> statuses = new ArrayList<>();
        statuses.add(AgreementPricelistRow.Status.CREATED);
        statuses.add(AgreementPricelistRow.Status.ACTIVE);
        statuses.add(AgreementPricelistRow.Status.DECLINED);
        statuses.add(AgreementPricelistRow.Status.CHANGED);
        List<AgreementPricelistRow> agreementPricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_PRICELIST_AND_STATUSES_AND_PUBLISHED_ARTICLE).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                setParameter("statuses", statuses).
                getResultList();
        List<AgreementPricelistRowAPI> agreementPricelistRowAPIs = AgreementPricelistRowMapper.map(agreementPricelistRows, true, false);
        boolean isAgreementPricelist = true;
        return exportPricelistFile(agreementPricelistRowAPIs, null, organizationUniqueId, pricelistUniqueId, isAgreementPricelist);
    }

    private byte[] exportPricelistFile(List<AgreementPricelistRowAPI> agreementPricelistRowAPIs, List<GeneralPricelistPricelistRowAPI> generalPricelistPricelistRowAPIs, long organizationUniqueId, long pricelistUniqueId, boolean isAgreementPricelist) throws HjalpmedelstjanstenValidationException {

        Organization organization = organizationController.getOrganization(organizationUniqueId);

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {

            boolean isCustomer = isCustomer(organization);



            workbook.lockStructure();
            POIXMLProperties properties = workbook.getProperties();
            POIXMLProperties.CustomProperties customProperties = properties.getCustomProperties();

            // set export type
            if( agreementPricelistRowAPIs != null ) {
                customProperties.addProperty(ExportImportProductsArticlesController.EXPORT_TYPE_NAME, EXPORT_TYPE_AGREEMENT);
            } else {
                customProperties.addProperty(ExportImportProductsArticlesController.EXPORT_TYPE_NAME, EXPORT_TYPE_GENERAL_PRICELIST);
            }
            // set export version
            customProperties.addProperty(ExportImportProductsArticlesController.EXPORT_VERSION_NAME, EXPORT_VERSION);

            // find values for drop downs
            List<CVGuaranteeUnit> guaranteeUnits = guaranteeUnitController.getAllGuaranteeUnits();
            List<CVPreventiveMaintenance> warrantyValidFroms = preventiveMaintenanceUnitController.getAllPreventiveMaintenances();
            //set how many years back and forward dates will go. 16383 is the maximum number of columns in Excel (almost 45 years...) so yearsBack + yearsForward < 45
            List<String> dates = getDates(5, 5, pricelistUniqueId, isAgreementPricelist);

            XSSFSheet valueListSheet = workbook.createSheet(VALUELIST_SHEET_NAME);
            valueListSheet.protectSheet(UUID.randomUUID().toString());
            //valueListSheet.protectSheet("apa");
            valueListSheet.lockFormatColumns(false);

            fillValueListSheet(valueListSheet, guaranteeUnits, warrantyValidFroms, dates,generalPricelistPricelistRowAPIs != null);

            XSSFCellStyle unlockedCellStyle = createUnlockedCellStyle(workbook);
            XSSFCellStyle unlockedTextCellStyle = createUnlockedTextCellStyle(workbook);

            XSSFCellStyle unlockedDateCellStyle = createUnlockedDateCellStyle(workbook);

            // create sheet and lock it in order to avoid users to edit fields that shouldn't be
            String rowSheetName;
            if( agreementPricelistRowAPIs != null ) {
                rowSheetName = PRICELISTROW_SHEET_NAME;
            } else {
                rowSheetName = GENERAL_PRICELISTROW_SHEET_NAME;
            }
            XSSFSheet sheet = workbook.createSheet(rowSheetName);
            sheet.protectSheet(UUID.randomUUID().toString());
            //sheet.protectSheet("apa");
            sheet.lockFormatColumns(false);


            int rowNumber = 0;
            int cellNumber = 0;
            XSSFRow fieldNamesRow = sheet.createRow(rowNumber++);
            XSSFRow headerRow = sheet.createRow(rowNumber++);

            int statusColumnNumber = 0;
            int priceColumnNumber = 0;
            int validFromColumnNumber = 0;
            int leastOrderQuantityColumnNumber = 0;
            int deliveryTimeColumnNumber = 0;
            int warrantyQuantityColumnNumber = 0;
            int warrantyQuantityUnitColumnNumber = 0;
            int warrantyValidFromColumnNumber = 0;

            // id
            headerRow.createCell(cellNumber++).setCellValue("Unikt Id");

            // type (Product/Article)
            headerRow.createCell(cellNumber++).setCellValue("Typ");

            // number
            //PELLE
            sheet.getColumnHelper().setColDefaultStyle(cellNumber, unlockedTextCellStyle); // Article number cannot be modified, but must be addable
            fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_ARTICLE_NUMBER);
            sheet.getColumnHelper().setColDefaultStyle(cellNumber, unlockedTextCellStyle);
            headerRow.createCell(cellNumber++).setCellValue("Artnr (*)");

            // name
            headerRow.createCell(cellNumber++).setCellValue("Benämning");

            // category
            headerRow.createCell(cellNumber++).setCellValue("Kategori");

            // order unit
            headerRow.createCell(cellNumber++).setCellValue("Beställningsenhet");

            // article discontinued
            headerRow.createCell(cellNumber++).setCellValue("Utgått");

            // status
            headerRow.createCell(cellNumber++).setCellValue("Status");

            // price
            headerRow.createCell(cellNumber++).setCellValue("Pris");
            fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_PRICE_NAME);
            sheet.getColumnHelper().setColDefaultStyle(cellNumber, unlockedCellStyle);
            priceColumnNumber = cellNumber;
            headerRow.createCell(cellNumber++).setCellValue("Pris");

            // valid from
            headerRow.createCell(cellNumber++).setCellValue("Gäller från");
            fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_VALID_FROM_NAME);
            //HJAL-2115
            //sheet.getColumnHelper().setColDefaultStyle(cellNumber, unlockedDateCellStyle);
            sheet.getColumnHelper().setColDefaultStyle(cellNumber, unlockedTextCellStyle);
            validFromColumnNumber = cellNumber;
            headerRow.createCell(cellNumber++).setCellValue("Gäller från");

            // least order quantity
            headerRow.createCell(cellNumber++).setCellValue("Min best");
            fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_LEAST_ORDER_QUANTITY_NAME);
            sheet.getColumnHelper().setColDefaultStyle(cellNumber, unlockedCellStyle);
            leastOrderQuantityColumnNumber = cellNumber;
            headerRow.createCell(cellNumber++).setCellValue("Min best (*)");

            // delivery time
            headerRow.createCell(cellNumber++).setCellValue("Levtid (dagar)");
            fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_DELIVERY_TIME_NAME);
            sheet.getColumnHelper().setColDefaultStyle(cellNumber, unlockedCellStyle);
            deliveryTimeColumnNumber = cellNumber;
            headerRow.createCell(cellNumber++).setCellValue("Levtid (dagar)");

            // warranty quantity
            headerRow.createCell(cellNumber++).setCellValue("Garanti (antal)");
            fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_WARRANTY_QUANTITY_NAME);
            sheet.getColumnHelper().setColDefaultStyle(cellNumber, unlockedCellStyle);
            warrantyQuantityColumnNumber = cellNumber;
            headerRow.createCell(cellNumber++).setCellValue("Garanti (antal)");

            // warranty quantity unit
            headerRow.createCell(cellNumber++).setCellValue("(enhet)");
            fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_WARRANTY_QUANTITY_UNIT_NAME);
            sheet.getColumnHelper().setColDefaultStyle(cellNumber, unlockedCellStyle);
            warrantyQuantityUnitColumnNumber = cellNumber;
            headerRow.createCell(cellNumber++).setCellValue("(enhet)");

            // warranty valid from
            headerRow.createCell(cellNumber++).setCellValue("(gäller från)");
            fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_WARRANTY_VALID_FROM_NAME);
            sheet.getColumnHelper().setColDefaultStyle(cellNumber, unlockedCellStyle);
            warrantyValidFromColumnNumber = cellNumber;
            headerRow.createCell(cellNumber++).setCellValue("(gäller från)");

            // warranty terms
            headerRow.createCell(cellNumber++).setCellValue("(villkor)");
            fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_WARRANTY_TERMS_NAME);
            sheet.getColumnHelper().setColDefaultStyle(cellNumber, unlockedTextCellStyle);
            headerRow.createCell(cellNumber++).setCellValue("(villkor)");
            //request inactivate row
            if(agreementPricelistRowAPIs != null && !isCustomer) {
                fieldNamesRow.createCell(cellNumber).setCellValue(FIELD_REQUEST_INACTIVE);
                sheet.getColumnHelper().setColDefaultStyle(cellNumber, unlockedTextCellStyle);
                headerRow.createCell(cellNumber++).setCellValue("begär inaktivering");
            }

            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
            if( agreementPricelistRowAPIs != null ) {
                for( AgreementPricelistRowAPI agreementPricelistRowAPI : agreementPricelistRowAPIs ) {
                    String categoryString = null;

                    //case 1 type H or Tiso
                    if( agreementPricelistRowAPI.getArticle().getBasedOnProduct() == null ) {
                        categoryString = agreementPricelistRowAPI.getArticle().getCategory().getCode();
                    } else {
                        categoryString = agreementPricelistRowAPI.getArticle().getBasedOnProduct().getCategory().getCode();
                    }

                    //case 2
                    if (categoryString == null){
                        if (agreementPricelistRowAPI.getArticle().getFitsToProducts() != null ) {
                            if (agreementPricelistRowAPI.getArticle().getFitsToProducts().size() > 0) {
                                categoryString = agreementPricelistRowAPI.getArticle().getFitsToProducts().get(0).getCategory().getCode();
                                categoryString = "(" + categoryString + ")";
                            }
                        }
                    }

                    //case 3
                    if (categoryString == null){
                        if (agreementPricelistRowAPI.getArticle().getFitsToArticles() != null && agreementPricelistRowAPI.getArticle().getFitsToArticles().size() > 0) {
                            if (agreementPricelistRowAPI.getArticle().getFitsToArticles().get(0).getBasedOnProduct() != null) {
                                categoryString = agreementPricelistRowAPI.getArticle().getFitsToArticles().get(0).getBasedOnProduct().getCategory().getCode();
                                categoryString = "(" + categoryString + ")";
                            }
                            // kan tänkas att detta ska med? PA 2020-10-05
                            else{
                                categoryString = agreementPricelistRowAPI.getArticle().getFitsToArticles().get(0).getCategory().getCode();
                                categoryString = "(" + categoryString + ")";
                            }
                        }
                    }
                    if (categoryString == null){
                        if (agreementPricelistRowAPI.getArticle().getFitsToArticles() != null && agreementPricelistRowAPI.getArticle().getFitsToArticles().size() > 0) {
                            if (agreementPricelistRowAPI.getArticle().getFitsToArticles().get(0).getFitsToProducts() != null && agreementPricelistRowAPI.getArticle().getFitsToArticles().get(0).getFitsToProducts().size() > 0) {
                                categoryString = agreementPricelistRowAPI.getArticle().getFitsToArticles().get(0).getFitsToProducts().get(0).getCategory().getCode();
                                categoryString = "(" + categoryString + ")";
                            }
                        }
                    }




//            searchProductsAndArticlesAPI.setCode("(" + categoryString + ")");

//                        if( category.getCode() != null && category.getCode().length() > 6 ) {
//                            // we do not want to include categories longer than 6 digits
//                            // let's find the parent category with six digits
//                            category = category.getParent();
//                        }
                    XSSFRow pricelistRow = sheet.createRow(rowNumber++);
                    fillRow(pricelistRow,
                            unlockedCellStyle,
                            unlockedDateCellStyle,
                            unlockedTextCellStyle,
                            dateFormatter,
                            agreementPricelistRowAPI.getId(),
                            agreementPricelistRowAPI.getArticle().getCategory().getArticleType().toString(),
                            agreementPricelistRowAPI.getArticle().getArticleNumber(),
                            agreementPricelistRowAPI.getArticle().getArticleName(),
                            categoryString,
                            agreementPricelistRowAPI.getArticle().getOrderUnit().getName(),
                            agreementPricelistRowAPI.getArticle().getStatus(),
                            agreementPricelistRowAPI.getStatus(),
                            agreementPricelistRowAPI.getPrice(),
                            agreementPricelistRowAPI.getValidFrom(),
                            agreementPricelistRowAPI.getLeastOrderQuantity(),
                            agreementPricelistRowAPI.getDeliveryTime(),
                            agreementPricelistRowAPI.getWarrantyQuantity(),
                            agreementPricelistRowAPI.getWarrantyQuantityUnit(),
                            agreementPricelistRowAPI.getWarrantyValidFrom(),
                            agreementPricelistRowAPI.getWarrantyTerms(),
                            isCustomer);
                }
            } else {
                for( GeneralPricelistPricelistRowAPI generalPricelistPricelistRowAPI : generalPricelistPricelistRowAPIs ) {
                    String categoryString = null;

                            //case 1 type H or Tiso
                            if( generalPricelistPricelistRowAPI.getArticle().getBasedOnProduct() == null ) {
                                categoryString = generalPricelistPricelistRowAPI.getArticle().getCategory().getCode();
                            } else {
                                categoryString = generalPricelistPricelistRowAPI.getArticle().getBasedOnProduct().getCategory().getCode();
                            }

                            //case 2
                            if (categoryString == null){
                                if (generalPricelistPricelistRowAPI.getArticle().getFitsToProducts() != null ) {
                                    if (generalPricelistPricelistRowAPI.getArticle().getFitsToProducts().size() > 0) {
                                        categoryString = generalPricelistPricelistRowAPI.getArticle().getFitsToProducts().get(0).getCategory().getCode();
                                        categoryString = "(" + categoryString + ")";
                                    }
                                }
                            }

                            //case 3
                            if (categoryString == null){
                                if (generalPricelistPricelistRowAPI.getArticle().getFitsToArticles() != null && generalPricelistPricelistRowAPI.getArticle().getFitsToArticles().size() > 0) {
                                    if (generalPricelistPricelistRowAPI.getArticle().getFitsToArticles().get(0).getBasedOnProduct() != null) {
                                        categoryString = generalPricelistPricelistRowAPI.getArticle().getFitsToArticles().get(0).getBasedOnProduct().getCategory().getCode();
                                        categoryString = "(" + categoryString + ")";
                                    }
                                    // kan tänkas att detta ska med? PA 2020-10-05
                                    else{
                                        categoryString = generalPricelistPricelistRowAPI.getArticle().getFitsToArticles().get(0).getCategory().getCode();
                                        categoryString = "(" + categoryString + ")";
                                    }
                                }
                            }
                             if (categoryString == null){
                                if (generalPricelistPricelistRowAPI.getArticle().getFitsToArticles() != null && generalPricelistPricelistRowAPI.getArticle().getFitsToArticles().size() > 0) {
                                    if (generalPricelistPricelistRowAPI.getArticle().getFitsToArticles().get(0).getFitsToProducts() != null && generalPricelistPricelistRowAPI.getArticle().getFitsToArticles().get(0).getFitsToProducts().size() > 0) {
                                        categoryString = generalPricelistPricelistRowAPI.getArticle().getFitsToArticles().get(0).getFitsToProducts().get(0).getCategory().getCode();
                                        categoryString = "(" + categoryString + ")";
                                    }
                                }
                            }




//            searchProductsAndArticlesAPI.setCode("(" + categoryString + ")");

//                        if( category.getCode() != null && category.getCode().length() > 6 ) {
//                            // we do not want to include categories longer than 6 digits
//                            // let's find the parent category with six digits
//                            category = category.getParent();
//                        }


                    XSSFRow pricelistRow = sheet.createRow(rowNumber++);
                    fillRow(pricelistRow,
                            unlockedCellStyle,
                            unlockedDateCellStyle,
                            unlockedTextCellStyle,
                            dateFormatter,
                            generalPricelistPricelistRowAPI.getId(),
                            generalPricelistPricelistRowAPI.getArticle().getCategory().getArticleType().toString(),
                            generalPricelistPricelistRowAPI.getArticle().getArticleNumber(),
                            generalPricelistPricelistRowAPI.getArticle().getArticleName(),
                            categoryString,
                            generalPricelistPricelistRowAPI.getArticle().getOrderUnit().getName(),
                            generalPricelistPricelistRowAPI.getArticle().getStatus(),
                            generalPricelistPricelistRowAPI.getStatus(),
                            generalPricelistPricelistRowAPI.getPrice(),
                            generalPricelistPricelistRowAPI.getValidFrom(),
                            generalPricelistPricelistRowAPI.getLeastOrderQuantity(),
                            generalPricelistPricelistRowAPI.getDeliveryTime(),
                            generalPricelistPricelistRowAPI.getWarrantyQuantity(),
                            generalPricelistPricelistRowAPI.getWarrantyQuantityUnit(),
                            generalPricelistPricelistRowAPI.getWarrantyValidFrom(),
                            generalPricelistPricelistRowAPI.getWarrantyTerms(),
                            isCustomer);
                }
            }

            //setStatusDataValidation(workbook, sheet, 2, 100000, statusColumnNumber);
            setNumericValidation(sheet, 2, 100000, priceColumnNumber);
            setIntegerValidation(sheet, 2, 100000, leastOrderQuantityColumnNumber, deliveryTimeColumnNumber);
            setIntegerValidationWithRemoveOption(sheet, 2, 100000, warrantyQuantityColumnNumber);
            //setDateValidation(sheet, 2, 100000, validFromColumnNumber);
            setDateValidation2(workbook, sheet, 2, 100000, dates, validFromColumnNumber);
            setGuaranteeUnitDataValidation(workbook, sheet, 2, 100000, guaranteeUnits, warrantyQuantityUnitColumnNumber);
            setWarrantyValidFromDataValidation(workbook, sheet, 2, 100000, warrantyValidFroms, warrantyValidFromColumnNumber);

            // size columns
            for( int i=0; i<=headerRow.getLastCellNum(); i++ ) {
                sheet.autoSizeColumn(i);
            }

            workbook.setSheetOrder(sheet.getSheetName(), 0);
            workbook.setActiveSheet(0);
            workbook.setSelectedTab(0);

            // hide unique id column
            sheet.getColumnHelper().getColumn(0, false).setHidden(true);

            // Freeze the first two rows
            sheet.createFreezePane(0, 2);

            // hide field names row
            fieldNamesRow.setZeroHeight(true);


            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            workbook.write(baos);
            return baos.toByteArray();
        } catch (IOException ex) {
            LOG.log(Level.WARNING, "Failed to export agreement or general pricelist rows to file", ex);
            throw validationMessageService.generateValidationException("export", "export.pricelist.error");
        }
    }

    private boolean isCustomer(Organization organization) {
        if(organization.getOrganizationType() == Organization.OrganizationType.CUSTOMER) {
            return true;
        } else {
             return false;
        }
    }

    private List<String> getDates(int yearsBack, int yearsForward, long pricelistUniqueId, boolean isAgreementPricelist){


        Date pricelistValidFrom;
        Calendar date = Calendar.getInstance();

        if (isAgreementPricelist == true) {

            List <AgreementPricelist> agreementPricelist = em.createNamedQuery(AgreementPricelist.FIND_BY_UNIQUE_ID).setParameter("pricelistUniqueId", pricelistUniqueId).getResultList();
            AgreementPricelist pricelist = agreementPricelist.get(0);
            pricelistValidFrom = pricelist.getValidFrom();
            date.setTime(pricelistValidFrom);

        } else {

            List <GeneralPricelistPricelist> generalPricelist = em.createNamedQuery(GeneralPricelistPricelist.FIND_BY_UNIQUE_ID).setParameter("generalPricelistUniqueId", pricelistUniqueId).getResultList();
            GeneralPricelistPricelist pricelist = generalPricelist.get(0);
            pricelistValidFrom = pricelist.getValidFrom();
            date.setTime(pricelistValidFrom);

        }





        Calendar stopDate = Calendar.getInstance();
        stopDate.add(Calendar.YEAR, yearsForward);
        int numberOfDates = (int)((stopDate.getTimeInMillis() - date.getTimeInMillis())/(1000*60*60*24));
        List<String> dates = new ArrayList<>();
        String strYear;
        String strMonth;
        String strDay;

        for (int i=0; i < numberOfDates; i++) {
            strYear = Integer.toString(date.get(Calendar.YEAR));
            strMonth = date.get(Calendar.MONTH) <  9 ? "0" + Integer.toString(date.get(Calendar.MONTH) + 1) : Integer.toString(date.get(Calendar.MONTH) + 1);
            strDay = date.get(Calendar.DATE) < 10 ?  "0" + Integer.toString(date.get(Calendar.DATE)) : Integer.toString(date.get(Calendar.DATE));

            dates.add(strYear + "-" + strMonth + "-" + strDay);
            date.add(Calendar.DATE, 1);
        }
        return dates;
    }



    private void fillRow(XSSFRow pricelistRow,
            XSSFCellStyle unlockedCellStyle,
            XSSFCellStyle unlockedDateCellStyle,
            XSSFCellStyle unlockedTextCellStyle,
            DateTimeFormatter dateFormatter,
            Long id,
            String articleType,
            String articleNumber,
            String articleName,
            String articleCategoryCode,
            String articleOrderUnitName,
            String articleStatus,
            String rowStatus,
            BigDecimal price,
            Long validFrom,
            Integer leastOrderQuantity,
            Integer deliveryTime,
            Integer warrantyQuantity,
            CVGuaranteeUnitAPI warrantyQuantityUnit,
            CVPreventiveMaintenanceAPI warrantyValidFrom,
            String warrantyTerms,
            boolean isCustomer) {
        int cellNumber = 0;

        // unique id
        pricelistRow.
                createCell(cellNumber++).
                setCellValue(id);

        // type
        pricelistRow.createCell(cellNumber++).
                setCellValue(articleType);

        // article number
        pricelistRow.createCell(cellNumber++).
                setCellValue(articleNumber);

        // article name
        pricelistRow.createCell(cellNumber++).
                setCellValue(articleName);

        // article category
            pricelistRow.createCell(cellNumber++).
                    setCellValue(articleCategoryCode);

        // article order unit
        pricelistRow.createCell(cellNumber++).
                setCellValue(articleOrderUnitName);

        // article discontinued
        pricelistRow.createCell(cellNumber++).
                setCellValue(Product.Status.DISCONTINUED.toString().equals(articleStatus) ? "Utgått": "");

        // current status
        XSSFCell statusCell = pricelistRow.createCell(cellNumber++);
        statusCell.setCellValue(getRowStatus(rowStatus));

        // current price
        XSSFCell priceCell = pricelistRow.createCell(cellNumber++);
        if(isCustomer && Product.Status.DISCONTINUED.toString().equals(articleStatus)) {
            //Print no price to price cell.
        } else {
            priceCell.setCellValue( price == null ? "": exportHelper.parseNumberValueToStringWithCorrectDecimalFormatting(price));
        }

        // new price
        XSSFCell priceEditableCell = pricelistRow.createCell(cellNumber++);
        priceEditableCell.setCellStyle(unlockedCellStyle);

        // current valid from
        XSSFCell validFromCell = pricelistRow.createCell(cellNumber++);
        ZonedDateTime validFromDateTime = Instant.ofEpochMilli(validFrom).atZone(ZoneId.systemDefault());
        validFromCell.setCellValue(dateFormatter.format(validFromDateTime));

        // new valid from
        XSSFCell validFromEditableCell = pricelistRow.createCell(cellNumber++);
        //HJAL-2115
        //validFromEditableCell.setCellStyle(unlockedDateCellStyle);
        validFromEditableCell.setCellStyle(unlockedTextCellStyle);

        // current least order quantity
        XSSFCell leastOrderQuantityCell = pricelistRow.createCell(cellNumber++);
        leastOrderQuantityCell.setCellValue(leastOrderQuantity.toString());

        // new least order quantity
        XSSFCell leastOrderQuantityEditableCell = pricelistRow.createCell(cellNumber++);
        leastOrderQuantityEditableCell.setCellStyle(unlockedCellStyle);

        // current delivery time
        XSSFCell deliveryTimeCell = pricelistRow.createCell(cellNumber++);
        deliveryTimeCell.setCellValue(deliveryTime);

        // new delivery time
        XSSFCell deliveryTimeEditableCell = pricelistRow.createCell(cellNumber++);
        deliveryTimeEditableCell.setCellStyle(unlockedCellStyle);

        // current warranty quantity
        XSSFCell warrantyQuantityCell = pricelistRow.createCell(cellNumber++);
        warrantyQuantityCell.setCellValue(warrantyQuantity == null ? "": warrantyQuantity.toString());

        // new  warranty quantity
        XSSFCell warrantyQuantityEditableCell = pricelistRow.createCell(cellNumber++);
        warrantyQuantityEditableCell.setCellStyle(unlockedCellStyle);

        // current warranty quantity unit
        XSSFCell warrantyQuantityUnitCell = pricelistRow.createCell(cellNumber++);
        warrantyQuantityUnitCell.setCellValue(warrantyQuantityUnit == null ? "": warrantyQuantityUnit.getName());

        // new  warranty quantity unit
        XSSFCell warrantyQuantityUnitEditableCell = pricelistRow.createCell(cellNumber++);
        warrantyQuantityUnitEditableCell.setCellStyle(unlockedCellStyle);

        // current warranty valid from
        XSSFCell warrantyValidFromCell = pricelistRow.createCell(cellNumber++);
        warrantyValidFromCell.setCellValue(warrantyValidFrom == null ? "": warrantyValidFrom.getName());

        // new  warranty valid from
        XSSFCell warrantyValidFromEditableCell = pricelistRow.createCell(cellNumber++);
        warrantyValidFromEditableCell.setCellStyle(unlockedCellStyle);

        // current warranty terms
        XSSFCell warrantyTermsCell = pricelistRow.createCell(cellNumber++);
        warrantyTermsCell.setCellValue(warrantyTerms);

        // new  warranty terms
        XSSFCell warrantyTermsEditableCell = pricelistRow.createCell(cellNumber++);
        warrantyTermsEditableCell.setCellStyle(unlockedCellStyle);




    }

    private XSSFCellStyle createUnlockedCellStyle(XSSFWorkbook workbook) {
        XSSFCellStyle unlockedCellStyle = workbook.createCellStyle();
        unlockedCellStyle.setLocked(false);
        unlockedCellStyle.setFillForegroundColor(IndexedColors.YELLOW.index);
        unlockedCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        unlockedCellStyle.setBorderBottom(BorderStyle.HAIR);
        unlockedCellStyle.setBorderTop(BorderStyle.HAIR);
        unlockedCellStyle.setBorderLeft(BorderStyle.HAIR);
        unlockedCellStyle.setBorderRight(BorderStyle.HAIR);
        unlockedCellStyle.setBottomBorderColor(IndexedColors.GREY_40_PERCENT.index);
        unlockedCellStyle.setTopBorderColor(IndexedColors.GREY_25_PERCENT.index);
        unlockedCellStyle.setLeftBorderColor(IndexedColors.GREY_25_PERCENT.index);
        unlockedCellStyle.setRightBorderColor(IndexedColors.GREY_25_PERCENT.index);
        return unlockedCellStyle;
    }

    private XSSFCellStyle createUnlockedTextCellStyle(XSSFWorkbook workbook) {
        XSSFCellStyle unlockedTextCellStyle = createUnlockedCellStyle(workbook);
        XSSFDataFormat dataFormat = workbook.createDataFormat();
        unlockedTextCellStyle.setDataFormat(dataFormat.getFormat("@"));
        return unlockedTextCellStyle;
    }

    private XSSFCellStyle createUnlockedDateCellStyle(XSSFWorkbook workbook) {
        XSSFCellStyle unlockedDateCellStyle = createUnlockedCellStyle(workbook);
        XSSFDataFormat dataFormat = workbook.createDataFormat();
        unlockedDateCellStyle.setDataFormat(dataFormat.getFormat("yyyy-mm-dd"));
        return unlockedDateCellStyle;
    }

    private void setGuaranteeUnitDataValidation(Workbook workbook,
            XSSFSheet sheet,
            int firstRow,
            int lastRow,
            List<CVGuaranteeUnit> guaranteeUnits,
            int orderUnitColumnNumber) {
        XSSFDataValidationHelper dataValidationHelper = new XSSFDataValidationHelper(sheet);
        XSSFName name = (XSSFName) workbook.getName("VLGuaranteeUnits");
        if( name == null ) {
            name = (XSSFName) workbook.createName();
            name.setNameName("VLGuaranteeUnits");
            int numberOfValues = guaranteeUnits.size();
            char columnPosition = (char) (numberOfValues+'A'); // find which is last letter of directives row
            name.setRefersToFormula(VALUELIST_SHEET_NAME + "!$B$2:$" + columnPosition + "$2" );
        }

        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createFormulaListConstraint("VLGuaranteeUnits");
        CellRangeAddressList addressList = new  CellRangeAddressList(firstRow, lastRow, orderUnitColumnNumber, orderUnitColumnNumber);
        XSSFDataValidation dataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, addressList);
        dataValidation.createErrorBox("Felaktigt värde", "Välj värde ur listan.");
        dataValidation.setShowErrorBox(true);
        sheet.addValidationData(dataValidation);
    }

    private void setStatusDataValidation(Workbook workbook, XSSFSheet sheet, int firstRow, int lastRow,
            int statusUniqueColumnNumber) {
        XSSFDataValidationHelper dataValidationHelper = new XSSFDataValidationHelper(sheet);
        XSSFName name = (XSSFName) workbook.getName("VLStatus");
        if( name == null ) {
            name = (XSSFName) workbook.createName();
            name.setNameName("VLStatus");
            name.setRefersToFormula(VALUELIST_SHEET_NAME + "!$B$1:$G$1" );
        }

        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createFormulaListConstraint("VLStatus");

        // agreemenent pricelist status
        CellRangeAddressList statusUniqueAddressList = new  CellRangeAddressList(firstRow, lastRow, statusUniqueColumnNumber, statusUniqueColumnNumber);
        XSSFDataValidation statusUniqueDataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, statusUniqueAddressList);
        statusUniqueDataValidation.createErrorBox("Felaktigt värde", "Välj värde ur listan.");
        statusUniqueDataValidation.setShowErrorBox(true);
        sheet.addValidationData(statusUniqueDataValidation);

    }

    private void setWarrantyValidFromDataValidation(Workbook workbook,
            XSSFSheet sheet,
            int firstRow,
            int lastRow,
            List<CVPreventiveMaintenance> preventiveMaintenances,
            int warrantyValidFromColumnNumber) {
        XSSFDataValidationHelper dataValidationHelper = new XSSFDataValidationHelper(sheet);
        XSSFName name = (XSSFName) workbook.getName("VLWarrantyValidFrom");
        if( name == null ) {
            name = (XSSFName) workbook.createName();
            name.setNameName("VLWarrantyValidFrom");
            int numberOfValues = preventiveMaintenances.size();
            char columnPosition = (char) (numberOfValues+'A'); // find which is last letter of directives row
            name.setRefersToFormula(VALUELIST_SHEET_NAME + "!$B$3:$" + columnPosition + "$3" );
        }

        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createFormulaListConstraint("VLWarrantyValidFrom");
        CellRangeAddressList addressList = new  CellRangeAddressList(firstRow, lastRow, warrantyValidFromColumnNumber, warrantyValidFromColumnNumber);
        XSSFDataValidation dataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, addressList);
        dataValidation.createErrorBox("Felaktigt värde", "Välj värde ur listan.");
        dataValidation.setShowErrorBox(true);
        sheet.addValidationData(dataValidation);
    }

    private void setDateValidation2(Workbook workbook, XSSFSheet sheet, int firstRow, int lastRow, List<String> dates, int discontinuedColumnNumber) {
        DataValidationHelper dataValidationHelper = sheet.getDataValidationHelper();
        XSSFName name = (XSSFName) workbook.getName("VLDates");
        if (name == null) {
            name = (XSSFName) workbook.createName();
            name.setNameName("VLDates");
            int numberOfValues = dates.size();
            String columnPosition = CellReference.convertNumToColString(numberOfValues); // find which is last letter of dates row
            name.setRefersToFormula(VALUELIST_SHEET_NAME + "!$B$9:$" + columnPosition + "$9");
        }
        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createFormulaListConstraint("VLDates");
        CellRangeAddressList addressList = new CellRangeAddressList(firstRow, lastRow, discontinuedColumnNumber, discontinuedColumnNumber);
        XSSFDataValidation dataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, addressList);
        dataValidation.createErrorBox("Felaktigt datum", "Välj datum ur listan.");
        dataValidation.setShowErrorBox(true);
        sheet.addValidationData(dataValidation);
    }

    private void setNumericValidation(XSSFSheet sheet,
            int firstRow,
            int lastRow,
            int statusColumnNumber) {
        XSSFDataValidationHelper dataValidationHelper = new XSSFDataValidationHelper(sheet);
        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createNumericConstraint(DataValidationConstraint.ValidationType.DECIMAL, DataValidationConstraint.OperatorType.GREATER_OR_EQUAL, String.valueOf(Integer.MIN_VALUE), String.valueOf(Integer.MAX_VALUE));

        CellRangeAddressList statusAddressList = new  CellRangeAddressList(firstRow, lastRow, statusColumnNumber, statusColumnNumber);
        XSSFDataValidation statusDataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, statusAddressList);
        statusDataValidation.createErrorBox("Felaktigt värde", "Giltigt värde är ett tal mellan " + String.valueOf(Integer.MIN_VALUE) + " och " + String.valueOf(Integer.MAX_VALUE) + ".");
        statusDataValidation.setShowErrorBox(true);
        sheet.addValidationData(statusDataValidation);
    }

    private void setIntegerValidation(XSSFSheet sheet,
            int firstRow,
            int lastRow,
            int leastOrderQuantityColumnNumber,
            int deliveryTimeColumnNumber) {
        XSSFDataValidationHelper dataValidationHelper = new XSSFDataValidationHelper(sheet);
        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createIntegerConstraint(DataValidationConstraint.OperatorType.GREATER_OR_EQUAL, "0", null);

        CellRangeAddressList leastOrderQuantityAddressList = new  CellRangeAddressList(firstRow, lastRow, leastOrderQuantityColumnNumber, leastOrderQuantityColumnNumber);
        XSSFDataValidation leastOrderQuantityDataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, leastOrderQuantityAddressList);
        leastOrderQuantityDataValidation.createErrorBox("Felaktigt värde", "Giltigt värde är ett heltal större än -1.");
        leastOrderQuantityDataValidation.setShowErrorBox(true);
        sheet.addValidationData(leastOrderQuantityDataValidation);

        CellRangeAddressList deliveryTimeAddressList = new  CellRangeAddressList(firstRow, lastRow, deliveryTimeColumnNumber, deliveryTimeColumnNumber);
        XSSFDataValidation deliveryTimeDataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, deliveryTimeAddressList);
        deliveryTimeDataValidation.createErrorBox("Felaktigt värde", "Giltigt värde är ett heltal större än -1.");
        deliveryTimeDataValidation.setShowErrorBox(true);
        sheet.addValidationData(deliveryTimeDataValidation);
    }

    private void setIntegerValidationWithRemoveOption(XSSFSheet sheet,
                                                      int firstRow,
                                                      int lastRow,
                                                      int warrantyQuantityColumnNumber) {
        XSSFDataValidationHelper dataValidationHelper = new XSSFDataValidationHelper(sheet);
        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createIntegerConstraint(DataValidationConstraint.OperatorType.GREATER_OR_EQUAL, "-1", null);

        CellRangeAddressList warrantyQuantityAddressList = new  CellRangeAddressList(firstRow, lastRow, warrantyQuantityColumnNumber, warrantyQuantityColumnNumber);
        XSSFDataValidation warrantyQuantityDataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, warrantyQuantityAddressList);
        warrantyQuantityDataValidation.createErrorBox("Felaktigt värde", "Giltigt värde är ett heltal större än -2.");
        warrantyQuantityDataValidation.setShowErrorBox(true);
        sheet.addValidationData(warrantyQuantityDataValidation);
    }

    private void setDateValidation(XSSFSheet sheet, int firstRow, int lastRow, int validFromColumnNumber) {
        XSSFDataValidationHelper dataValidationHelper = new XSSFDataValidationHelper(sheet);
        XSSFDataValidationConstraint dataValidationConstraint = (XSSFDataValidationConstraint) dataValidationHelper.createDateConstraint(DataValidationConstraint.OperatorType.GREATER_THAN, "25569", null, "yyyy-mm-dd");
        CellRangeAddressList validFromAddressList = new  CellRangeAddressList(firstRow, lastRow, validFromColumnNumber, validFromColumnNumber);
        XSSFDataValidation validFromDataValidation = (XSSFDataValidation) dataValidationHelper.createValidation(dataValidationConstraint, validFromAddressList);
        validFromDataValidation.createErrorBox("Felaktigt värde", "Giltigt värde är ett datum senare än 1970-01-01, formatet ska vara yyyy-mm-dd.");
        validFromDataValidation.setShowErrorBox(true);
        sheet.addValidationData(validFromDataValidation);
    }

    private void fillValueListSheet(XSSFSheet valueListSheet,
            List<CVGuaranteeUnit> guaranteeUnits,
            List<CVPreventiveMaintenance> preventiveMaintenances,
            List<String> dates,
            boolean isGeneralPricelist) {

        XSSFRow statusRow = valueListSheet.createRow(0);
        XSSFCell statusNameCell = statusRow.createCell(0);
        statusNameCell.setCellValue("Status");

        if( !isGeneralPricelist ) {
            // agreement row status
            XSSFCell statusCellCreated = statusRow.createCell(1);
            statusCellCreated.setCellValue(STATUS_NAME_CREATED);
            XSSFCell statusCellPendingApproval = statusRow.createCell(2);
            statusCellPendingApproval.setCellValue(STATUS_NAME_PENDING_APPROVAL);
            XSSFCell statusCellActive = statusRow.createCell(3);
            statusCellActive.setCellValue(STATUS_NAME_ACTIVE);
            XSSFCell statusCellDeclined = statusRow.createCell(4);
            statusCellDeclined.setCellValue(STATUS_NAME_DECLINED);
            XSSFCell statusCellPendingInactivation = statusRow.createCell(5);
            statusCellPendingInactivation.setCellValue(STATUS_NAME_PENDING_INACTIVATION);
            XSSFCell statusCellInactive = statusRow.createCell(6);
            statusCellInactive.setCellValue(STATUS_NAME_INACTIVE);
        } else {
            XSSFCell statusCellActive = statusRow.createCell(1);
            statusCellActive.setCellValue(STATUS_NAME_ACTIVE);
            XSSFCell statusCellInactive = statusRow.createCell(2);
            statusCellInactive.setCellValue(STATUS_NAME_INACTIVE);
        }

        // directives
        XSSFRow guaranteeUnitsRow = valueListSheet.createRow(1);
        XSSFCell guaranteeUnitsNameCell = guaranteeUnitsRow.createCell(0);
        guaranteeUnitsNameCell.setCellValue("Garantienhet");
        for( int i = 0; i<guaranteeUnits.size(); i++ ) {
            XSSFCell guaranteeUnitsValueCell = guaranteeUnitsRow.createCell(i+1);
            guaranteeUnitsValueCell.setCellValue(guaranteeUnits.get(i).getName());
        }

        // dates
        XSSFRow datesRow = valueListSheet.createRow(8);
        XSSFCell datesNameCell = datesRow.createCell(0);
        datesNameCell.setCellValue("Datum");
        for (int i = 0; i < dates.size(); i++) {
            XSSFCell datesValueCell = datesRow.createCell(i + 1);
            datesValueCell.setCellValue(dates.get(i));
        }

        // preventive maintenance valid from
        XSSFRow preventiveMaintenceRow = valueListSheet.createRow(2);
        XSSFCell preventiveMaintenceNameCell = preventiveMaintenceRow.createCell(0);
        preventiveMaintenceNameCell.setCellValue("Garanti gäller från");
        for( int i = 0; i<preventiveMaintenances.size(); i++ ) {
            XSSFCell preventiveMaintenanceValueCell = preventiveMaintenceRow.createCell(i+1);
            preventiveMaintenanceValueCell.setCellValue(preventiveMaintenances.get(i).getName());
        }
    }

    private String getRowStatus(String status) {
        if( AgreementPricelistRow.Status.ACTIVE.toString().equals(status) ) {
            return STATUS_NAME_ACTIVE;
        } else if( AgreementPricelistRow.Status.INACTIVE.toString().equals(status) ) {
            return STATUS_NAME_INACTIVE;
        } else if( AgreementPricelistRow.Status.DECLINED.toString().equals(status) ) {
            return STATUS_NAME_DECLINED;
        } else if( AgreementPricelistRow.Status.CREATED.toString().equals(status) ) {
            return STATUS_NAME_CREATED;
        } else if( AgreementPricelistRow.Status.PENDING_APPROVAL.toString().equals(status) ) {
            return STATUS_NAME_PENDING_APPROVAL;
        } else if( AgreementPricelistRow.Status.PENDING_INACTIVATION.toString().equals(status) ) {
            return STATUS_NAME_PENDING_INACTIVATION;
        } else if( AgreementPricelistRow.Status.CHANGED.toString().equals(status) ) {
            return STATUS_NAME_CHANGED;
        }
        return null;
    }

}
