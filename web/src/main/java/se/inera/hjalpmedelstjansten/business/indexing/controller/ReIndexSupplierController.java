package se.inera.hjalpmedelstjansten.business.indexing.controller;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;

import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 * Reindex products and articles for a specific supplier.
 *
 */
@Stateless
public class ReIndexSupplierController {

    @Inject
    HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    ReIndexProductController reIndexProductController;

    @Inject
    ReIndexArticleController reIndexArticleController;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void reIndexSupplier( long organizationUniqueId, String organizationName ) {
        LOG.log( Level.FINEST, "reIndexSupplier( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        Session session = em.unwrap(Session.class);
        session.setDefaultReadOnly(true);
        session.setHibernateFlushMode(FlushMode.MANUAL);
        long start = System.currentTimeMillis();
        // we want to reuse category specific propertys for each category as much
        // as possible
        Map<Long, List<CategorySpecificProperty>> categorySpecificPropertyMap = new HashMap<>();

        // handle products for organization
        List<Long> productIds =  em.createQuery("SELECT p.uniqueId FROM Product p WHERE p.organization.uniqueId = :organizationUniqueId").
                setParameter("organizationUniqueId", organizationUniqueId)
                .getResultList();
        if( productIds != null ) {
            LOG.log( Level.INFO, "reindex {0} products for supplier {1} )", new Object[] {productIds.size(), organizationUniqueId});
            int i = 1;
            int numberOfProducts = productIds.size();
            for( Long productId : productIds ) {
                LOG.log( Level.INFO, "Reindex productId {0} ({1} of {2}) for supplier {3}", new Object[] {productId, i, numberOfProducts, organizationName});
                reIndexProductController.reIndexProduct(organizationUniqueId, productId, categorySpecificPropertyMap);
                i++;
            }
        }

        // handle articles for organization
        List<Long> articleIds =  em.createQuery("SELECT a.uniqueId FROM Article a WHERE a.organization.uniqueId = :organizationUniqueId").
                setParameter("organizationUniqueId", organizationUniqueId)
                .getResultList();
        if( articleIds != null ) {
            LOG.log( Level.INFO, "reindex {0} articles for supplier {1} )", new Object[] {articleIds.size(), organizationUniqueId});
            int i = 1;
            int numberOfArticles = articleIds.size();
            for( Long articleId : articleIds ) {
                LOG.log( Level.INFO, "Reindex articleId {0} ({1} of {2}) for supplier {3}", new Object[] {articleId, i, numberOfArticles, organizationName});
                reIndexArticleController.reIndexArticle(organizationUniqueId, articleId, categorySpecificPropertyMap);
                i++;
            }
        }
        long total = System.currentTimeMillis() - start;
        LOG.logPerformance(this.getClass().getName(), "reIndexSupplier()", total, null);
    }


}
