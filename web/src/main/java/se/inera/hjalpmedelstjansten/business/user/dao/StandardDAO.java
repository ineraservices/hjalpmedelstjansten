package se.inera.hjalpmedelstjansten.business.user.dao;

import java.io.Serializable;
import java.util.Optional;

public interface StandardDAO<T extends Serializable> {

    Optional<T> create(T newEntity);
    Optional<T> read(long Id);
    Optional<T> update(long Id, T entity);
    Optional<T> delete(long Id);

}
