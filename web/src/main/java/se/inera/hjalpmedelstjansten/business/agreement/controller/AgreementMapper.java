package se.inera.hjalpmedelstjansten.business.agreement.controller;

import se.inera.hjalpmedelstjansten.business.DateUtils;
import se.inera.hjalpmedelstjansten.business.organization.controller.BusinessLevelMapper;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.PreventiveMaintenanceMapper;
import se.inera.hjalpmedelstjansten.business.user.controller.UserMapper;
import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for mapping between API and Entity classes
 * 
 */
public class AgreementMapper {
    
    public static final List<AgreementAPI> map(List<Agreement> agreements, boolean includeEverything) {
        if( agreements == null ) {
            return null;
        }
        List<AgreementAPI> agreementAPIs = new ArrayList<>();
        for( Agreement agreement : agreements ) {
            agreementAPIs.add(map(agreement, includeEverything));
        }
        return agreementAPIs;
    }
    
    public static final AgreementAPI map(Agreement agreement, boolean includeEverything) {
        return map(agreement, null, includeEverything);
    }
    
    public static final AgreementAPI map(Agreement agreement, Long numberOfRows, boolean includeEverything) {
        if( agreement == null ) {
            return null;
        }
        AgreementAPI agreementAPI = new AgreementAPI();
        agreementAPI.setId(agreement.getUniqueId());
        agreementAPI.setAgreementName(agreement.getAgreementName());
        agreementAPI.setAgreementNumber(agreement.getAgreementNumber());
        agreementAPI.setValidFrom(agreement.getValidFrom().getTime());
        agreementAPI.setValidTo(agreement.getValidTo().getTime());
        agreementAPI.setStatus(agreement.getStatus().toString());
        agreementAPI.setCustomerOrganization(OrganizationMapper.map(agreement.getCustomer(), false));
        if( agreement.getCustomerBusinessLevel() != null ) {
            agreementAPI.setCustomerBusinessLevel(BusinessLevelMapper.map(agreement.getCustomerBusinessLevel(), false));
        }
        agreementAPI.setSupplierOrganization(OrganizationMapper.map(agreement.getSupplier(), false));
        if( agreement.getCustomerPricelistApprovers() != null && !agreement.getCustomerPricelistApprovers().isEmpty() ) {
            agreementAPI.setCustomerPricelistApprovers(UserMapper.map(agreement.getCustomerPricelistApprovers(), false));
        }

        if( includeEverything ) {
            // delivery times
            agreementAPI.setDeliveryTimeH(agreement.getDeliveryTimeH());
            agreementAPI.setDeliveryTimeT(agreement.getDeliveryTimeT());
            agreementAPI.setDeliveryTimeI(agreement.getDeliveryTimeI());
            agreementAPI.setDeliveryTimeR(agreement.getDeliveryTimeR());
            agreementAPI.setDeliveryTimeTJ(agreement.getDeliveryTimeTJ());

            // warranty quantity
            agreementAPI.setWarrantyQuantityH(agreement.getWarrantyQuantityH());
            agreementAPI.setWarrantyQuantityT(agreement.getWarrantyQuantityT());
            agreementAPI.setWarrantyQuantityI(agreement.getWarrantyQuantityI());
            agreementAPI.setWarrantyQuantityR(agreement.getWarrantyQuantityR());
            agreementAPI.setWarrantyQuantityTJ(agreement.getWarrantyQuantityTJ());

            // warranty valid from
            if( agreement.getWarrantyValidFromH() != null ) {
                agreementAPI.setWarrantyValidFromH(PreventiveMaintenanceMapper.map(agreement.getWarrantyValidFromH()));
            }
            if( agreement.getWarrantyValidFromT() != null ) {
                agreementAPI.setWarrantyValidFromT(PreventiveMaintenanceMapper.map(agreement.getWarrantyValidFromT()));
            }
            if( agreement.getWarrantyValidFromI() != null ) {
                agreementAPI.setWarrantyValidFromI(PreventiveMaintenanceMapper.map(agreement.getWarrantyValidFromI()));
            }
            if( agreement.getWarrantyValidFromR() != null ) {
                agreementAPI.setWarrantyValidFromR(PreventiveMaintenanceMapper.map(agreement.getWarrantyValidFromR()));
            }
            if( agreement.getWarrantyValidFromTJ() != null ) {
                agreementAPI.setWarrantyValidFromTJ(PreventiveMaintenanceMapper.map(agreement.getWarrantyValidFromTJ()));
            }

            // warranty terms
            agreementAPI.setWarrantyTermsH(agreement.getWarrantyTermsH());
            agreementAPI.setWarrantyTermsT(agreement.getWarrantyTermsT());
            agreementAPI.setWarrantyTermsI(agreement.getWarrantyTermsI());
            agreementAPI.setWarrantyTermsR(agreement.getWarrantyTermsR());
            agreementAPI.setWarrantyTermsTJ(agreement.getWarrantyTermsTJ());

            // warranty quantity units
            agreementAPI.setWarrantyQuantityHUnit(GuaranteeUnitMapper.map(agreement.getWarrantyQuantityHUnit()));
            agreementAPI.setWarrantyQuantityTUnit(GuaranteeUnitMapper.map(agreement.getWarrantyQuantityTUnit()));
            agreementAPI.setWarrantyQuantityIUnit(GuaranteeUnitMapper.map(agreement.getWarrantyQuantityIUnit()));
            agreementAPI.setWarrantyQuantityRUnit(GuaranteeUnitMapper.map(agreement.getWarrantyQuantityRUnit()));
            agreementAPI.setWarrantyQuantityTJUnit(GuaranteeUnitMapper.map(agreement.getWarrantyQuantityTJUnit()));
            
            if( agreement.getEndDate() != null ) {
                agreementAPI.setEndDate(agreement.getEndDate().getTime());
            }
            if( agreement.getExtensionOptionTo() != null ) {
                agreementAPI.setExtensionOptionTo(agreement.getExtensionOptionTo().getTime());
            }
            if( agreement.getSendReminderOn() != null ) {
                agreementAPI.setSendReminderOn(agreement.getSendReminderOn().getTime());
            }
            if( agreement.getSharedWithCustomers() != null && !agreement.getSharedWithCustomers().isEmpty() ) {
                agreementAPI.setSharedWithCustomers(OrganizationMapper.map(agreement.getSharedWithCustomers()));
            }
            if( agreement.getSharedWithCustomerBusinessLevels() != null && !agreement.getSharedWithCustomerBusinessLevels().isEmpty() ) {
                agreementAPI.setSharedWithCustomersBusinessLevels(BusinessLevelMapper.map(agreement.getSharedWithCustomerBusinessLevels(), true));
            }
            if( numberOfRows != null ) {
                agreementAPI.setArticlesExistOnAgreement( numberOfRows > 0 );
            }
        }
        return agreementAPI;
    }

    public static final Agreement map(AgreementAPI agreementAPI) {
        if( agreementAPI == null ) {
            return null;
        }
        Agreement agreement = new Agreement();
        agreement.setAgreementName(agreementAPI.getAgreementName());
        agreement.setAgreementNumber(agreementAPI.getAgreementNumber());
        agreement.setValidFrom(DateUtils.beginningOfDay(agreementAPI.getValidFrom()));
        agreement.setValidTo(DateUtils.endOfDay(agreementAPI.getValidTo()));

        mapDeliveryAndWarrantyFields(agreementAPI, agreement);

        if( agreementAPI.getEndDate() != null ) {
            agreement.setEndDate(DateUtils.beginningOfDay(agreementAPI.getEndDate()));
        }
        if( agreementAPI.getExtensionOptionTo() != null ) {
            agreement.setExtensionOptionTo(DateUtils.beginningOfDay(agreementAPI.getExtensionOptionTo()));
        }
        if( agreementAPI.getSendReminderOn() != null ) {
            agreement.setSendReminderOn(DateUtils.beginningOfDay(agreementAPI.getSendReminderOn()));
        }
        agreement.setStatus(getAgreementStatus(agreement));
        return agreement;
    }
    
    public static void mapDeliveryAndWarrantyFields(AgreementAPI agreementAPI, Agreement agreement ) {
        // delivery times
        agreement.setDeliveryTimeH(agreementAPI.getDeliveryTimeH());
        agreement.setDeliveryTimeT(agreementAPI.getDeliveryTimeT());
        agreement.setDeliveryTimeI(agreementAPI.getDeliveryTimeI());
        agreement.setDeliveryTimeR(agreementAPI.getDeliveryTimeR());
        agreement.setDeliveryTimeTJ(agreementAPI.getDeliveryTimeTJ());
        
        // warranty quantity
        agreement.setWarrantyQuantityH(agreementAPI.getWarrantyQuantityH());
        agreement.setWarrantyQuantityT(agreementAPI.getWarrantyQuantityT());
        agreement.setWarrantyQuantityI(agreementAPI.getWarrantyQuantityI());
        agreement.setWarrantyQuantityR(agreementAPI.getWarrantyQuantityR());
        agreement.setWarrantyQuantityTJ(agreementAPI.getWarrantyQuantityTJ());
        
        // warranty terms
        agreement.setWarrantyTermsH(agreementAPI.getWarrantyTermsH());
        agreement.setWarrantyTermsT(agreementAPI.getWarrantyTermsT());
        agreement.setWarrantyTermsI(agreementAPI.getWarrantyTermsI());
        agreement.setWarrantyTermsR(agreementAPI.getWarrantyTermsR());
        agreement.setWarrantyTermsTJ(agreementAPI.getWarrantyTermsTJ());
    }

    public static Agreement.Status getAgreementStatus(Agreement agreement) {
        Instant nowInstant = Instant.now();
        ZoneId zoneId = ZoneId.systemDefault();
        if( agreement.getEndDate() != null ) {
            ZonedDateTime now = ZonedDateTime.ofInstant(nowInstant, zoneId).toLocalDate().atStartOfDay(zoneId);
            Instant endDateInstant = Instant.ofEpochMilli(agreement.getEndDate().getTime());
            ZonedDateTime endDate = ZonedDateTime.ofInstant(endDateInstant, zoneId);
            if( now.isAfter(endDate) || now.isEqual(endDate) ) {
                return Agreement.Status.DISCONTINUED;
            }
        }
        ZonedDateTime now = ZonedDateTime.ofInstant(nowInstant, zoneId).toLocalDate().atStartOfDay(zoneId);
        Instant validToInstant = Instant.ofEpochMilli(agreement.getValidTo().getTime());
        ZonedDateTime validTo = ZonedDateTime.ofInstant(validToInstant, zoneId);
        if( now.isAfter(validTo) || now.isEqual(validTo) ) {
            return Agreement.Status.DISCONTINUED;
        }
        ZonedDateTime tomorrowStartOfDay = ZonedDateTime.ofInstant(nowInstant, zoneId).toLocalDate().plusDays(1).atStartOfDay(zoneId);
        Instant validFromInstant = Instant.ofEpochMilli(agreement.getValidFrom().getTime());
        ZonedDateTime validFrom = ZonedDateTime.ofInstant(validFromInstant, zoneId);
        if( validFrom.isBefore(tomorrowStartOfDay) ) {
            return Agreement.Status.CURRENT;
        } else {
            return Agreement.Status.FUTURE;
        }
    }
    
}
