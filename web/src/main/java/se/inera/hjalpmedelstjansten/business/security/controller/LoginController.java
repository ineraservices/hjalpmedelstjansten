package se.inera.hjalpmedelstjansten.business.security.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationMapper;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserMapper;
import se.inera.hjalpmedelstjansten.model.api.LoginAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;
import se.inera.hjalpmedelstjansten.model.entity.UserAccount;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;

import jakarta.ejb.Stateless;
import jakarta.enterprise.event.Event;
import jakarta.inject.Inject;
import java.util.Date;
import java.util.logging.Level;

/**
 * Conditions for password login:
 * - A UserEngagement exists
 * - The UserEngagement is valid
 * - Correct username/password
 * - Not first login (is password set)
 * - User has accepted GDPR
 * - User has Roles on the UserEngagement
 * - The Organization connected to the UserEngagement is valid
 *
 * Conditions for token login:
 * - A UserEngagementexists
 * - The UserEngagement is valid
 * - Correct username/token
 * - User has Roles
 * - The Organization connected to the UserEngagement is valid
 */
@Stateless
public class LoginController {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private Event<InternalAuditEvent> internalAuditEvent;

    @Inject
    private UserController userController;

    @Inject
    private SecurityController securityController;

    @Inject
    private AuthHandler authHandler;

    public boolean standardLoginUser(LoginAPI loginAPI, String requestIp) {
        LOG.log(Level.INFO, "standardLoginUser( ... )");
        try {
            if (loginAPI.getUsername() == null || loginAPI.getUsername().isEmpty()) {
                LOG.log(Level.INFO, "Attempt to login without username");
                return false;
            }
            if (loginAPI.getPassword() == null || loginAPI.getPassword().isEmpty()) {
                LOG.log(Level.INFO, "Attempt to login without password");
                return false;
            }
            String username = loginAPI.getUsername();
            UserEngagement userEngagement = userController.findByUsername(username);
            if (userEngagement == null) {
                LOG.log(Level.INFO, "Attempt to login by non-existing user");
                return false;
            }
            if (userEngagement.getUserAccount().getPassword() == null || userEngagement.getUserAccount().getPassword().isEmpty()) {
                LOG.log(Level.INFO, "Attempt to login without a set password");
                return false;
            }
            if( userEngagement.getUserAccount().getLoginType() != UserAccount.LoginType.PASSWORD ) {
                LOG.log(Level.INFO, "Attempt to login using password by token user");
                return false;
            }
            String hashedPassword = securityController.hashPassword(loginAPI.getPassword(), userEngagement.getUserAccount().getSalt(), userEngagement.getUserAccount().getIterations());
            if (!userEngagement.getUserAccount().getPassword().equals(hashedPassword)) {
                LOG.log(Level.INFO, "Attempt to login with wrong password");
                return false;
            }
            if (!UserMapper.calculateActive(userEngagement.getValidFrom(), userEngagement.getValidTo())) {
                LOG.log(Level.INFO, "Attempt to login with inactive engagement");
                return false;
            }
            if (userEngagement.getOrganization() == null) {
                LOG.log(Level.WARNING, "Attempt to login by user with no organization on the engagement");
                return false;
            }
            if (!OrganizationMapper.calculateActive(userEngagement.getOrganization().getValidFrom(), userEngagement.getOrganization().getValidTo())) {
                LOG.log(Level.INFO, "Attempt to login with active user, but on inactive organization");
                return false;
            }

            if(userEngagement.getRoles() == null || userEngagement.getRoles().isEmpty()) {
                LOG.log(Level.WARNING, "Attempt to login by user with no roles on the engagement");
                return false;
            }

            // Login
            try {
                authHandler.standardLogin(username, hashedPassword);
            } catch (Exception e) {
                // do not log exception here
                // the authhandler should be in charge of logging more detailed
                LOG.log(Level.INFO, "Failed to login");
                return false;
            }

            UserAPI userAPI = UserMapper.map(userEngagement, true);
            UserAccount userAccount = userEngagement.getUserAccount();
            userAPI.setPreviousLoginFound(userAccount.isHasLoggedIn());
            if( !userAccount.isHasLoggedIn() ) {
                userAccount.setHasLoggedIn(true);
                userController.updateUserAccount(userAccount);
            }
            authHandler.addToSession(AuthHandler.USER_API_SESSION_KEY, userAPI);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.USER, InternalAudit.ActionType.LOGIN, userEngagement.getUniqueId(), authHandler.getSessionId(), userEngagement.getUniqueId(), requestIp));
            return true;
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Unhandled error during login", e);
            return false;
        }
    }

    public boolean tokenLoginUser(String username, String token, String requestIp) {
        LOG.log(Level.FINEST, "tokenLoginUser(...)");
        try {
            if (username == null || username.isEmpty()) {
                LOG.log(Level.INFO, "Attempt to login without username");
                return false;
            }
            if (token == null || token.isEmpty()) {
                LOG.log(Level.INFO, "Attempt to login without token");
                return false;
            }
            UserEngagement userEngagement = userController.findByUsername(username);
            if (userEngagement == null) {
                LOG.log(Level.INFO, "Attempt to login using token by non-existing user");
                return false;
            }
            if( userEngagement.getUserAccount().getLoginType() != UserAccount.LoginType.TOKEN ) {
                LOG.log(Level.INFO, "Attempt to login using token by non-token user");
                return false;
            }
            String hashedToken = securityController.hashPassword(token, userEngagement.getUserAccount().getSalt(), userEngagement.getUserAccount().getIterations());
            if (!userEngagement.getUserAccount().getToken().equals(hashedToken)) {
                LOG.log(Level.INFO, "Attempt to login with wrong token");
                return false;
            }
            if (!UserMapper.calculateActive(userEngagement.getValidFrom(), userEngagement.getValidTo())) {
                LOG.log(Level.INFO, "Attempt to login with inactive token engagement");
                return false;
            }
            if (!OrganizationMapper.calculateActive(userEngagement.getOrganization().getValidFrom(), userEngagement.getOrganization().getValidTo())) {
                LOG.log(Level.INFO, "Attempt to login with active token user, but on inactive organization");
                return false;
            }
            if(userEngagement.getRoles() == null || userEngagement.getRoles().isEmpty()) {
                LOG.log(Level.WARNING, "Attempt to login by token user with no roles on the engagement");
                return false;
            }

            // Login
            try {
                authHandler.tokenLogin(username, hashedToken);
            } catch (Exception e) {
                // do not log exception here
                // the authhandler should be in charge of logging more detailed
                LOG.log(Level.INFO, "Failed to login");
                return false;
            }

            UserAPI userAPI = UserMapper.map(userEngagement, true);
            UserAccount userAccount = userEngagement.getUserAccount();
            userAPI.setPreviousLoginFound(userAccount.isHasLoggedIn());
            if( !userAccount.isHasLoggedIn() ) {
                userAccount.setHasLoggedIn(true);
                userController.updateUserAccount(userAccount);
            }
            authHandler.addToSession(AuthHandler.USER_API_SESSION_KEY, userAPI);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.USER, InternalAudit.ActionType.LOGIN, userEngagement.getUniqueId(), authHandler.getSessionId(), userEngagement.getUniqueId(), requestIp));
            return true;
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Unhandled error during login", e);
            return false;
        }
    }

}
