package se.inera.hjalpmedelstjansten.business.product.controller.exportimport;

import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.media.MediaImageAPI;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;

import java.util.List;

/**
 * Class for moving data between import product/article steps read and save
 * 
 */
public class ImportItem {

    private ProductAPI productAPI;
    private ArticleAPI articleAPI;
    private Category category;
    private List<CategorySpecificProperty> categorySpecificPropertys;
    private String sheetName;
    private int rowNumber;
    private MediaImageAPI mainMediaImageAPI;
    private boolean deleteMainImage;

    public boolean isDeleteMainImage() {
        return deleteMainImage;
    }

    public void setDeleteMainImage(boolean deleteMainImage) {
        this.deleteMainImage = deleteMainImage;
    }

    public ImportItem(ProductAPI productAPI, 
            Category category, 
            List<CategorySpecificProperty> categorySpecificPropertys, 
            int rowNumber,
            String sheetName) {
        this.productAPI = productAPI;
        this.rowNumber = rowNumber;
        this.category = category;
        this.categorySpecificPropertys = categorySpecificPropertys;
        this.sheetName = sheetName;
    }

    public ImportItem(ArticleAPI articleAPI, 
            Category category, 
            List<CategorySpecificProperty> categorySpecificPropertys, 
            int rowNumber, 
            String sheetName) {
        this.articleAPI = articleAPI;
        this.rowNumber = rowNumber;
        this.category = category;
        this.categorySpecificPropertys = categorySpecificPropertys;
        this.sheetName = sheetName;
    }

    public ProductAPI getProductAPI() {
        return productAPI;
    }

    public void setProductAPI(ProductAPI productAPI) {
        this.productAPI = productAPI;
    }

    public ArticleAPI getArticleAPI() {
        return articleAPI;
    }

    public void setArticleAPI(ArticleAPI articleAPI) {
        this.articleAPI = articleAPI;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<CategorySpecificProperty> getCategorySpecificPropertys() {
        return categorySpecificPropertys;
    }

    public void setCategorySpecificPropertys(List<CategorySpecificProperty> categorySpecificPropertys) {
        this.categorySpecificPropertys = categorySpecificPropertys;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public MediaImageAPI getMainMediaImageAPI() {
        return mainMediaImageAPI;
    }

    public void setMainMediaImageAPI(MediaImageAPI mainMediaImageAPI) {
        this.mainMediaImageAPI = mainMediaImageAPI;
    }

    
}
