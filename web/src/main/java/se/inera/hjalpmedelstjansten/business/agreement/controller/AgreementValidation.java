package se.inera.hjalpmedelstjansten.business.agreement.controller;

import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

/**
 * Validation methods for agreements
 *
 */
@Stateless
public class AgreementValidation {

    @Inject
    HjmtLogger LOG;

    @Inject
    ValidationMessageService validationMessageService;

    public void validateForCreate(AgreementAPI agreementAPI, long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForCreate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForCreate(organizationUniqueId, agreementAPI);
        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }
    }

    public void validateForUpdate(AgreementAPI agreementAPI, Agreement agreement, long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForUpdate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForUpdate(organizationUniqueId, agreementAPI, agreement);
        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }
    }

    /**
     * Generates a Set of <code>ErrorMessageAPI</code> in case the given agreementAPI
     * does not validate entirely for create
     *
     * @param organizationUniqueId
     * @param agreementAPI
     * @return
     */
    public Set<ErrorMessageAPI> tryForCreate(long organizationUniqueId, AgreementAPI agreementAPI) {
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        validateAgreementAPI(agreementAPI, errorMessageAPIs);
        if( !errorMessageAPIs.isEmpty() ) {
            return errorMessageAPIs;
        }
        if( !validFromDate(agreementAPI.getValidFrom()) ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("validFrom", validationMessageService.getMessage("agreement.validFrom.notFuture")));
        }
        return errorMessageAPIs;
    }

    /**
     * Generates a Set of <code>ErrorMessageAPI</code> in case the given agreementAPI
     * does not validate entirely for update
     *
     * @param organizationUniqueId
     * @param agreementAPI
     * @return
     */
    public Set<ErrorMessageAPI> tryForUpdate(long organizationUniqueId, AgreementAPI agreementAPI, Agreement agreement) {
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        validateAgreementAPI(agreementAPI, errorMessageAPIs);
        if( !errorMessageAPIs.isEmpty() ) {
            return errorMessageAPIs;
        }
        //if( agreement.getStatus() == Agreement.Status.DISCONTINUED ) {
        //    errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("status", validationMessageService.getMessage("agreement.status.discontinued")));
        //}

        //HJAL-2117.1
        /*
        if( !DateUtils.isSameDay(agreementAPI.getValidFrom(), agreement.getValidFrom().getTime()) &&
                !validFromDate(agreementAPI.getValidFrom()) ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("validFrom", validationMessageService.getMessage("agreement.validFrom.notFuture")));
        }
        */
        return errorMessageAPIs;
    }

    private void validateAgreementAPI(AgreementAPI agreementAPI, Set<ErrorMessageAPI> errorMessageAPIs) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<AgreementAPI>> constraintViolations = validator.validate(agreementAPI, Default.class);
        if( constraintViolations != null && !constraintViolations.isEmpty() ) {
            for( ConstraintViolation constraintViolation : constraintViolations ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage()));
            }
        }
    }

    private boolean validFromDate(Long dateAsMillis) {
        // valid from must be today or a future date when creating or updating validFrom
        Instant nowInstant = Instant.now();
        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime todayStartOfDay = ZonedDateTime.ofInstant(nowInstant, zoneId).toLocalDate().atStartOfDay(zoneId);
        Instant validFromInstant = Instant.ofEpochMilli(dateAsMillis);
        ZonedDateTime validFrom = ZonedDateTime.ofInstant(validFromInstant, zoneId);
        if( validFrom.isBefore(todayStartOfDay) ) {
            return false;
        }
        return true;
    }

}
