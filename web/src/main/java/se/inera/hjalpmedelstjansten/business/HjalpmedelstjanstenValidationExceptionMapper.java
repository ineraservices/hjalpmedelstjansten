package se.inera.hjalpmedelstjansten.business;

import se.inera.hjalpmedelstjansten.model.api.ErrorAPI;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * This exception causes a HTTP response code of 400 and lists error in the
 * response body.
 *
 * @see se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
 */
@Provider
public class HjalpmedelstjanstenValidationExceptionMapper implements ExceptionMapper<HjalpmedelstjanstenValidationException> {

    @Override
    public Response toResponse(HjalpmedelstjanstenValidationException validationException) {
        ErrorAPI errorAPI = new ErrorAPI();
        errorAPI.setErrors(validationException.getValidationMessages());
        return Response.status(Response.Status.BAD_REQUEST)
            .entity(errorAPI)
            .build();
    }

}
