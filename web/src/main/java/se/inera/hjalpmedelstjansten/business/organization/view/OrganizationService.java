package se.inera.hjalpmedelstjansten.business.organization.view;

import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.ResultExporterController;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementController;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.product.controller.CategoryController;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductController;
import se.inera.hjalpmedelstjansten.business.product.controller.SearchController;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.business.user.controller.EmailController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;


/**
 * REST API for Organization functionality.
 *
 */
@Stateless
@Path("organizations")
@Interceptors({ PerformanceLogInterceptor.class })
public class OrganizationService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private ResultExporterController resultExporterController;

    @Inject
    EmailController emailController;

    @EJB
    private OrganizationController organizationController;

    @EJB
    private UserController userController;

    @EJB
    private ProductController productController;

    @EJB
    private SearchController searchController;

    @EJB
    private ArticleController articleController;

    @EJB
    private CategoryController categoryController;

    @EJB
    private AgreementController agreementController;

    @EJB
    private GeneralPricelistController generalPricelistController;

    @EJB
    private AuthHandler authHandler;

    /**
     * Search organization by user query
     *
     * @param query the user query
     * @param offset for pagination purpose
     * @param limit
     * @param organizationTypes list of organization types to limit search for
     * @return a list of <code>OrganizationAPI</code> matching the query and a
     * header X-Total-Count giving the total number of organizations for pagination
     */
    @GET
    @SecuredService(permissions = {"organization:view"})
    public Response searchOrganizations(
            @DefaultValue(value = "") @QueryParam("query") String query,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @DefaultValue(value = "25") @QueryParam("limit") int limit,
            @QueryParam("type") List<Organization.OrganizationType> organizationTypes) {
        LOG.log(Level.FINEST, "searchOrganizations( offset: {0} )", new Object[] {offset});
        // db query expects at least one type, if none are specified, we return organizations
        // of all types
        if( organizationTypes == null || organizationTypes.isEmpty() ) {
            organizationTypes = new ArrayList<>(Arrays.asList(Organization.OrganizationType.values()));
        }
        SearchDTO searchDTO = organizationController.searchOrganizations(query, organizationTypes, offset, limit);
        return Response.ok(searchDTO.getItems()).
                header("X-Total-Count", searchDTO.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }

    @GET
    @Path("/valid")
    @SecuredService(permissions = {"organization:view"})
    public Response searchValidOrganizations(
            @DefaultValue(value = "") @QueryParam("query") String query,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @DefaultValue(value = "25") @QueryParam("limit") int limit,
            @QueryParam("type") List<Organization.OrganizationType> organizationTypes) {
        LOG.log(Level.FINEST, "searchValidOrganizations( offset: {0} )", new Object[] {offset});
        // db query expects at least one type, if none are specified, we return organizations
        // of all types
        if( organizationTypes == null || organizationTypes.isEmpty() ) {
            organizationTypes = new ArrayList<>(Arrays.asList(Organization.OrganizationType.values()));
        }
        SearchDTO searchDTO = organizationController.searchValidOrganizations(query, organizationTypes, offset, limit);
        return Response.ok(searchDTO.getItems()).
                header("X-Total-Count", searchDTO.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                        build();
    }

    /**
     * Export organizations
     *
     * @param httpServletRequest
     * @param query
     * @return an excel containing the list of search results
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"organization:view"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportOrganizations(
            @Context HttpServletRequest httpServletRequest,
            @DefaultValue(value = "") @QueryParam("query") String query) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "exportOrganizations...");
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if (!isSuperAdmin ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        List<Organization.OrganizationType> organizationTypes = new ArrayList<>(Arrays.asList(Organization.OrganizationType.values()));
        int offset = 0;
        int maximumNumberOfResults = 55000;

        SearchDTO searchDTO = organizationController.searchOrganizations(query,
                organizationTypes,
                offset,
                maximumNumberOfResults);

        if (searchDTO == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                String filename = "Export_Organisationer_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";
                List<OrganizationAPI> searchOrganizationsAPIs = (List<OrganizationAPI>) searchDTO.getItems();
                byte[] exportBytes = resultExporterController.generateOrganizationsList(searchOrganizationsAPIs);

                return Response.ok(
                        exportBytes,
                        jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                        .header("content-disposition", "attachment; filename = " + filename)
                        .build();
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to export articles for assortment to file", ex);
                return Response.serverError().build();
            }
        }
    }

    /**
     * Crete a new organization
     *
     * @param httpServletRequest
     * @param organizationAPI user supplied values
     * @return the created <code>OrganizationAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case validation fails
     */
    @POST
    @SecuredService(permissions = {"organization:create"})
    public Response createOrganization(@Context HttpServletRequest httpServletRequest, OrganizationAPI organizationAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createOrganization(...)");
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        organizationAPI = organizationController.createOrganization(organizationAPI, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        return Response.ok(organizationAPI).build();
    }

    /**
     * Update an existing <code>Organization</code>
     *
     * @param httpServletRequest
     * @param uniqueId the id of the organization to update
     * @param organizationAPI user supplied values
     * @return the updated <code>OrganizationAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case validation fails
     */
    @PUT
    @Path("{uniqueId}")
    @SecuredService(permissions = {"organization:update", "organization:update_contact"})
    @TransactionTimeout(value=60, unit = TimeUnit.MINUTES)
    public Response updateOrganization(@Context HttpServletRequest httpServletRequest,
                                       @PathParam("uniqueId") long uniqueId,
                                       OrganizationAPI organizationAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateOrganization( uniqueId: {0} )", new Object[] {uniqueId});
        boolean updateOrganization = authHandler.hasPermission("organization:update");
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( uniqueId, updateOrganization, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        organizationAPI = organizationController.updateOrganization(uniqueId, organizationAPI, updateOrganization, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( organizationAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(organizationAPI).build();
        }
    }

    /**
     * Get information about a specific organization
     *
     * @param uniqueId the id of the organization to return
     * @return the corresponding <code>OrganizationAPI</code>
     */
    @GET
    @Path("{uniqueId}")
    @SecuredService(permissions = {"organization:view"})
    public Response getOrganization(@Context HttpServletRequest httpServletRequest, @PathParam("uniqueId") long uniqueId ) {
        LOG.log(Level.FINEST, "getOrganization( uniqueId: {0} )", new Object[] {uniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        OrganizationAPI organizationAPI = organizationController.getOrganizationAPI( uniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest) );
        if( organizationAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(organizationAPI).build();
        }
    }

    /**
     * Delete a specific organization. Rare case since only organizations without
     * e.g. users can be deleted.
     *
     * @param uniqueId the id of the organization to return
     * @return HTTP 200 ok or HTTP 400 if validation fails
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case validation fails, e.g. the organization has users
     */
    @DELETE
    @Path("{uniqueId}")
    @SecuredService(permissions = {"organization:delete"})
    public Response deleteOrganization(@Context HttpServletRequest httpServletRequest, @PathParam("uniqueId") long uniqueId ) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteOrganization( uniqueId: {0} )", new Object[] {uniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        organizationController.deleteOrganization( uniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest) );
        return Response.ok().build();
    }

    /**
     * Authorize whether user can handle organization
     *
     * @param uniqueId unique id of organizatino
     * @param updateOrganization whether the user has the permission to handle organization info
     * @return true if handle is ok, otherwise false
     */
    private boolean authorizeHandleOrganization(long uniqueId, boolean updateOrganization, UserAPI userAPI) {
        if( updateOrganization ) {
            return true;
        } else {
            // if the user is not superadmin, only own organization can be handled
            return authorizeHandleOrganization(uniqueId, userAPI);
        }
    }

}
