package se.inera.hjalpmedelstjansten.business.assortment.view;

import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.ResultExporterController;
import se.inera.hjalpmedelstjansten.business.assortment.controller.AssortmentController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.AssortmentAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Assortment;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * REST API for all assortments.
 *
 */
@Stateless
@Path("assortments")
@Interceptors({ PerformanceLogInterceptor.class })
public class AssortmentAllService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    AssortmentController assortmentController;

    @Inject
    ResultExporterController resultExporterController;

    @EJB
    AuthHandler authHandler;

    /**
     * Search all assortments by user query
     *
     * @param query the user query
     * @param statuses
     * @param offset for pagination purpose
     * @return a list of <code>AgreementAPI</code> matching the query and a
     * header X-Total-Count giving the total number of agreements for pagination
     */
    @GET
    @SecuredService(permissions = {"assortment:view_all"})
    public Response searchAllAssortments(
            @DefaultValue(value = "") @QueryParam("query") String query,
            @QueryParam("status") List<Assortment.Status> statuses,
            @DefaultValue(value = "0") @QueryParam("offset") int offset) {
        LOG.log(Level.FINEST, "searchAllAssortments( offset: {0} )", new Object[] {offset});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( query != null && query.isEmpty() ) {
            query = null;
        }
        if( statuses != null && statuses.size() == 3 ) {
            // means we should include all statuses, meaning we don't have to filter on it
            statuses = null;
        }
        SearchDTO searchDTO = assortmentController.searchAssortments(
                null,
                query,
                statuses,
                false,
                userAPI,
                offset,
                25);
        return Response.ok(searchDTO.getItems()).
                header("X-Total-Count", searchDTO.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }

    /**
     * Export assortments
     *
     * @param query the user query
     * @param statuses
     * @return an excel containing the list of search results
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"assortment:view_all"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportAllAssortments(
            @DefaultValue(value = "") @QueryParam("query") String query,
            @QueryParam("status") List<Assortment.Status> statuses) {
        LOG.log(Level.FINEST, "exportAssortments...");
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( query != null && query.isEmpty() ) {
            query = null;
        }
        if( statuses != null && statuses.size() == 3 ) {
            // means we should include all statuses, meaning we don't have to filter on it
            statuses = null;
        }
        int offset = 0;
        int maximumNumberOfResults = 55000;

        SearchDTO searchDTO = assortmentController.searchAssortments(null, query, statuses, false, userAPI, offset, maximumNumberOfResults);

        if (searchDTO == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                String filename = "Export_Utbud_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";

                List<AssortmentAPI> searchAssortmentsAPIs = (List<AssortmentAPI>) searchDTO.getItems();
                byte[] exportBytes = resultExporterController.generateAssortmentsResultList(searchAssortmentsAPIs);

                return Response.ok(
                        exportBytes,
                        jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                        .header("content-disposition", "attachment; filename = " + filename)
                        .build();
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to export users to file", ex);
                return Response.serverError().build();
            }
        }
    }
}
