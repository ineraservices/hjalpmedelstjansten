package se.inera.hjalpmedelstjansten.business.assortment.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountyAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCounty;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVMunicipality;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Level;


@Stateless
public class CountyController {

    @Inject
    private HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;

    /**
     * Get all CE directives and standards
     *
     * @return a <code>CeAPI</code> containing all standards and directives
     */
    public List<CVCountyAPI> getAllCounties() {
        LOG.log(Level.FINEST, "getAllCounties()");
        return AssortmentMapper.mapCounties(findAllCounties(), true);
    }

    public List<CVCountyAPI> getCountiesForOrganization(long organizationId) {
        LOG.log(Level.FINEST, "getCountiesForOrganization()");
        return AssortmentMapper.mapCounties(findCountiesForOrganization(organizationId), true);
    }

    public List<CVCounty> findCountiesForOrganization(long organizationId) {
        LOG.log(Level.FINEST, "findCountiesForOrganization()");
        String sql = "SELECT c.* FROM hjmtj.CVCounty c INNER JOIN hjmtj.OrganizationCounty o ON c.uniqueId = o.countyId WHERE o.organizationId = " + organizationId + " ORDER BY c.name";
        return  em.createNativeQuery(sql, CVCounty.class).getResultList();
    }

    public List<CVCounty> findAllCounties() {
        LOG.log(Level.FINEST, "findAllCounties()");
        return em.createNamedQuery(CVCounty.FIND_ALL).
                getResultList();
    }

    public CVCounty findCountyById(long countyId) {
        return em.find(CVCounty.class, countyId);
    }

    public CVMunicipality findMunicipalityById(long municipalityId) {
        return em.find(CVMunicipality.class, municipalityId);
    }
}
