package se.inera.hjalpmedelstjansten.business.security.controller;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.LoggerService;

import java.util.logging.Level;

public class TokenUserJDBCRealm extends JdbcRealm {

    private static final HjmtLogger LOG = LoggerService.getLogger(TokenUserJDBCRealm.class.getName());
    
    @Override
    public boolean supports(AuthenticationToken token) {
        if( token instanceof TokenUserToken ) {
            return true;
        }
        LOG.log( Level.FINEST, "token is NOT of type TokenUserToken" );
        return false;
    }    
    
}
