package se.inera.hjalpmedelstjansten.business.product.controller;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import jakarta.ejb.DependsOn;
import jakarta.ejb.ScheduleExpression;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.ejb.Timeout;
import jakarta.ejb.TimerConfig;
import jakarta.ejb.TimerService;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.clustering.ClusterController;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.UserEngagementAPI;
import se.inera.hjalpmedelstjansten.model.entity.Product;

/**
 * Timer for discontinuing products where the replacementDate is passed.
 */
@Singleton
@Startup
@DependsOn("PropertyLoader")
public class ProductDiscontinuedTimer {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private boolean productDiscontinuedTimerEnabled;

    @Inject
    private String productDiscontinuedTimerHour;

    @Inject
    private String productDiscontinuedTimerMinute;

    @Inject
    private String productDiscontinuedTimerSecond;

    @Inject
    private ProductController productController;

    @Inject
    private ClusterController clusterController;

    @Resource
    private TimerService timerService;

    @PersistenceContext(unitName = "HjmtjPU")
    EntityManager em;

    @Timeout
    private void schedule() {
        LOG.log(Level.FINEST, "schedule");
        // attempt to get lock
        boolean lockReceived = clusterController.getLock(this.getClass().getName(), 30);
        if (lockReceived) {
            LOG.log(Level.FINEST, "Received lock!");
            long start = System.currentTimeMillis();

            try {
                discontinueProductsByDate();
            } catch(Exception ex) {
                LOG.log(Level.SEVERE, "Job to discontinue products failed!", ex);
            }

            long total = System.currentTimeMillis() - start;
            if (total > 60000) {
                LOG.log(Level.WARNING, "Scheduled job took: {0} ms which is more than 1 minute which is a long time, a developer should take a look at this.", new Object[]{total});
            } else {
                LOG.log(Level.INFO, "Scheduled job took: {0} ms.", new Object[]{total});
            }
            //release lock
            clusterController.releaseLock(this.getClass().getName());
        } else {
            LOG.log(Level.INFO, "Did not receive lock!");
        }
    }

    /**
     * Discontinue all products with replacement date set and has passed.
     */
    public void discontinueProductsByDate() {
        LOG.log(Level.FINEST, "discontinueProductsByDate()");
        final var publishedProductsWithReplacementDate = em.createNamedQuery(Product.FIND_BY_STATUS_AND_REPLACEMENT_DATE_PASSED, Product.class).
            setParameter("status", Product.Status.PUBLISHED).
            setParameter("replacementDate", new Date()).
            getResultList();
        if (publishedProductsWithReplacementDate != null && !publishedProductsWithReplacementDate.isEmpty()) {
            LOG.log(Level.FINEST, "Found {0} products to discontinue because of replacement date passed", new Object[]{publishedProductsWithReplacementDate.size()});
            for (Product product : publishedProductsWithReplacementDate) {
                try {
                    UserAPI userAPI = new UserAPI();
                    List<UserEngagementAPI> userEngagementAPIs = new LinkedList<>();
                    UserEngagementAPI userEngagementAPI = new UserEngagementAPI();
                    userEngagementAPI.setOrganizationId(product.getOrganization().getUniqueId());
                    userEngagementAPIs.add(userEngagementAPI);
                    userAPI.setUserEngagementAPIs(userEngagementAPIs);
                    productController.discontinueProduct(product, product.isInactivateRowsOnReplacement(), product.getOrganization().getUniqueId(), product.getUniqueId(), userAPI);
                } catch (HjalpmedelstjanstenValidationException ex) {
                    LOG.log(Level.WARNING, "Failed to discontinue product: " + product.getUniqueId() + " by date", ex);
                }
            }
        } else {
            LOG.log(Level.FINEST, "Found NO products to discontinue because of replacement date passed");
        }
    }

    @PostConstruct
    private void initialize() {
        LOG.log(Level.FINEST, "initialize()");
        if (productDiscontinuedTimerEnabled) {
            LOG.log(Level.FINEST, "Timer is ENABLED");
            if (timerService.getTimers().isEmpty()) {
                String name = this.getClass().getName();
                TimerConfig configuration = new TimerConfig();
                configuration.setPersistent(false);
                configuration.setInfo(name);
                ScheduleExpression scheduleExpression = new ScheduleExpression();
                scheduleExpression.hour(productDiscontinuedTimerHour).minute(productDiscontinuedTimerMinute).second(productDiscontinuedTimerSecond);
                timerService.createCalendarTimer(scheduleExpression, configuration);
            }
        } else {
            LOG.log(Level.INFO, "Timer is NOT ENABLED");
        }
    }
}
