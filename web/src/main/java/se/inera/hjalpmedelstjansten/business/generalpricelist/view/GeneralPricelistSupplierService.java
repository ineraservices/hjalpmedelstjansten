package se.inera.hjalpmedelstjansten.business.generalpricelist.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import java.util.logging.Level;

/**
 * REST API for general pricelists from a supplier point of view. Suppliers
 * can create/update a general pricelist on their own organization.
 *
 */
@Stateless
@Path("organizations/{organizationUniqueId}/generalpricelists")
@Interceptors({ PerformanceLogInterceptor.class })
public class GeneralPricelistSupplierService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @Inject
    GeneralPricelistController generalPricelistController;

    @Inject
    AuthHandler authHandler;

    /**
     * Get general pricelist on a given organization (supplier). Currently there can be only
     * one general pricelist per supplier.
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization who owns the General Pricelist
     * @return an <code>GeneralPricelistAPI</code> or null if none exist
     */
    @GET
    @SecuredService(permissions = {"generalpricelist:view", "generalpricelist:view_all"})
    public Response getGeneralPricelist(@Context HttpServletRequest httpServletRequest, @PathParam("organizationUniqueId") long organizationUniqueId) {
        LOG.log(Level.FINEST, "getGeneralPricelist( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authHandler.hasPermission("generalpricelist:view_all") && !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        GeneralPricelistAPI generalPricelistAPI = generalPricelistController.getGeneralPricelistAPI(organizationUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( generalPricelistAPI == null ) {
            return Response.noContent().build();
        } else {
            return Response.ok(generalPricelistAPI).build();
        }
    }

    /**
     * Create new <code>GeneralPricelist</code> on the given organization
     *
     * @param organizationUniqueId unique id of the organization to create general pricelist on
     * @param generalPricelistAPI user supplied values
     * @return the created <code>GeneralPricelistAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @SecuredService(permissions = {"generalpricelist:create_own"})
    public Response createGeneralPricelist(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            GeneralPricelistAPI generalPricelistAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createGeneralPricelist( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        generalPricelistAPI = generalPricelistController.createGeneralPricelist(organizationUniqueId, generalPricelistAPI, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        return Response.ok(generalPricelistAPI).build();
    }

    /**
     * Update an existing <code>GeneralPricelist</code>
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of organization
     * @param generalPricelistAPI user supplied values
     * @return the updated <code>GeneralPricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case validation fails
     */
    @PUT
    @SecuredService(permissions = {"generalpricelist:update_own"})
    public Response updateGeneralPricelist(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            GeneralPricelistAPI generalPricelistAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateGeneralPricelist( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        generalPricelistAPI = generalPricelistController.updateGeneralPricelist(organizationUniqueId, generalPricelistAPI, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( generalPricelistAPI == null ) {
            return Response.noContent().build();
        } else {
            return Response.ok(generalPricelistAPI).build();
        }
    }

}
