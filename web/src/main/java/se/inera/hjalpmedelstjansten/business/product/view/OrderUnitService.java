package se.inera.hjalpmedelstjansten.business.product.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.product.controller.OrderUnitController;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.cv.CVOrderUnitAPI;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import java.util.List;
import java.util.logging.Level;

@Stateless
@Path("orderunits")
@Interceptors({ PerformanceLogInterceptor.class })
public class OrderUnitService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    private OrderUnitController orderUnitController;

    /**
     * Return all order units in the database.
     *
     * @return a list of <code>OrderUnitAPI</code>
     */
    @GET
    @SecuredService(permissions = {"orderunit:view"})
    public List<CVOrderUnitAPI> getAllOrderUnits() {
        LOG.log(Level.FINEST, "getAllOrderUnits()");
        return orderUnitController.getAllUnits();
    }

}
