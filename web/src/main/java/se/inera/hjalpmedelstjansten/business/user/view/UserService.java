package se.inera.hjalpmedelstjansten.business.user.view;

import lombok.NoArgsConstructor;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.ResultExporterController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.business.user.controller.NotificationController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserValidation;
import se.inera.hjalpmedelstjansten.business.user.domain.NotificationRequest;
import se.inera.hjalpmedelstjansten.business.user.domain.NotificationResponse;
import se.inera.hjalpmedelstjansten.business.user.domain.mapper.NotificationMapper;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.NotificationDTO;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * REST API for User functionality.
 *
 */
@Stateless
@Path("organizations/{organizationUniqueId}/users")
@Interceptors({ PerformanceLogInterceptor.class })
@NoArgsConstructor
public class UserService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private UserValidation userValidation;

    @Inject
    ResultExporterController resultExporterController;

    @Inject
    private AuthHandler authHandler;

    private UserController userController;
    private NotificationController notificationController;
    private NotificationMapper notificationMapper;

    @Inject
    UserService(UserController userController,
                NotificationController notificationController,
                NotificationMapper notificationMapper) {

        this.userController = userController;
        this.notificationController = notificationController;
        this.notificationMapper = notificationMapper;
    }

    /**
     * Search users on a specific organization by query
     *
     * @param organizationUniqueId the unique id of the organization to search users on
     * @param query the user query
     * @param offset offset for pagination purpose
     * @param roleNames limit search to only users with the given role names
     * @return a list of <code>UserAPI</code> matching the query and a header
     * X-Total-Count giving the total number of users for pagination
     */
    @GET
    @SecuredService(permissions = {"user:view"})
    public Response searchUsers(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @QueryParam("roleName") List<UserRole.RoleName> roleNames) {
        LOG.log(Level.FINEST, "searchUsers( organizationUniqueId: {0}, offset: {1} )", new Object[] {organizationUniqueId, offset});
        long countSearch = userController.countSearchUsers(query, organizationUniqueId, roleNames);
        List<UserAPI> userAPIs = userController.searchUsers(false, query, organizationUniqueId, offset, 25, roleNames);
        return Response.ok(userAPIs).
                header("X-Total-Count", countSearch).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }

    /**
     * Export users
     *
     * @param httpServletRequest
     * @param query
     * @return an excel containing the list of search results
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("/{onlyActives}/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"user:view"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportUsers(
            @Context HttpServletRequest httpServletRequest,
            @PathParam("onlyActives") boolean onlyActives,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "exportUsers...");
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if (!isSuperAdmin ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        List<UserRole.RoleName> roleNames = new ArrayList<>(Arrays.asList(UserRole.RoleName.values()));
        int offset = 0;
        int maximumNumberOfResults = 55000;

        /*
        SearchDTO searchDTO = userController.searchUsers(query,
                organizationUniqueId,
                offset,
                maximumNumberOfResults,
                roleNames);

         */

        List<UserAPI> userAPIs = userController.searchUsers(onlyActives, query, organizationUniqueId, offset, maximumNumberOfResults, roleNames);


        if (userAPIs == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                String filename = "Export_Användare_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";
                //List<UserAPI> searchUsersAPIs = (List<UserAPI>) searchDTO.getItems();
                //byte[] exportBytes = resultExporterController.generateUsersList(searchUsersAPIs);
                byte[] exportBytes = resultExporterController.generateUsersResultList(userAPIs);

                return Response.ok(
                        exportBytes,
                        jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                        .header("content-disposition", "attachment; filename = " + filename)
                        .build();
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to export users to file", ex);
                return Response.serverError().build();
            }
        }
    }

    /**
     * Get all users on a specific organization in specic roles. This API is not
     * paginated like the search API.
     *
     * @param organizationUniqueId the unique id of the organization to search users on
     * @param roleNames
     * @return a list of <code>UserAPI</code> matching the query
     */
    @GET
    @Path("/roles")
    @SecuredService(permissions = {"user:view"})
    public Response getUsersInRoles(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @QueryParam("roleName") List<UserRole.RoleName> roleNames) {
        LOG.log(Level.FINEST, "getUsersInRoles( organizationUniqueId: {0}, roleNames: {1} )", new Object[] {organizationUniqueId, roleNames});
        List<UserAPI> userAPIs = null;
        if( roleNames == null || roleNames.isEmpty() ) {
            LOG.log( Level.WARNING, "No roleNames supplied in query parameter which is invalid use of API." );
        } else {
            userAPIs = userController.getUsersInRolesOnOrganization(organizationUniqueId, roleNames);
        }
        return Response.ok(userAPIs).build();
    }

    /**
     * Get users on a specific organization in specific business levels. This API is not
     * paginated like the search API.
     *
     * @param organizationUniqueId the unique id of the organization to search users on
     * @param query
     * @param businessLevelIds ids of the businesslevels to look for
     * @param offset
     * @return a list of <code>UserAPI</code> matching the query
     */
    @GET
    @Path("/businesslevels")
    @SecuredService(permissions = {"user:view"})
    public Response getUsersInBusinessLevels(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query,
            @QueryParam("businessLevel") List<Long> businessLevelIds,
            @DefaultValue(value = "0") @QueryParam("offset") int offset) {
        LOG.log(Level.FINEST, "getUsersInBusinessLevels( organizationUniqueId: {0}, businessLevelIds: {1} )", new Object[] {organizationUniqueId, businessLevelIds});
        long countSearch = userController.countSearchUsersInBusinessLevels(query, organizationUniqueId, businessLevelIds);
        List<UserAPI> userAPIs = userController.searchUsersInBusinessLevels(query, organizationUniqueId, businessLevelIds, offset, 25);
        return Response.ok(userAPIs).
                header("X-Total-Count", countSearch).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }

    /**
     * Get information about a specific user on an organization. regex och path is
     * to distinguish it from method above (getUserInRole) which has the same path,
     * but does not accept digits, otherwise server complains
     *
     * @param organizationUniqueId the unique id of the organization of the user
     * @param uniqueId the unique id of the user
     * @return the corresponding <code>UserAPI</code>
     */
    @GET
    @Path("{uniqueId : \\d+}")
    @SecuredService(permissions = {"user:view"})
    public Response getUser(@Context HttpServletRequest httpServletRequest,
                            @PathParam("organizationUniqueId") long organizationUniqueId,
                            @PathParam("uniqueId") long uniqueId ) {
        LOG.log(Level.FINEST, "getUser( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        UserAPI sessionUserAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        UserAPI userAPI = userController.getUserAPI(organizationUniqueId, uniqueId, sessionUserAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( userAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(userAPI).build();
        }
    }

    /**
     * Create a user on an organization.
     *
     * @param httpServletRequest
     * @param organizationUniqueId the unique id of the organization of the user
     * @param userAPI the user supplied values
     * @return the created <code>UserAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException
     */
    @POST
    @SecuredService(permissions = {"user:create", "user:create_own"})
    public Response createUser(@Context HttpServletRequest httpServletRequest,
                               @PathParam("organizationUniqueId") long organizationUniqueId,
                               UserAPI userAPI) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "createUser( ... )" );
        UserAPI sessionUserAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        boolean isUserCreate = authHandler.hasPermission("user:create");
        boolean isUserCreateOwn = authHandler.hasPermission("user:create_own");
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if( !authorizeHandleUsers(organizationUniqueId, null, isUserCreate, isUserCreateOwn, sessionUserAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        userAPI = userController.createUser(organizationUniqueId, userAPI, sessionUserAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest), isSuperAdmin);
        return Response.ok(userAPI).build();
    }

    /**
     * Update a user on an organization.
     *
     * @param organizationUniqueId the unique id of the organization of the user
     * @param uniqueId the unique id of the user
     * @param userAPI the user supplied values
     * @return the updated <code>UserAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case validation fails
     */
    @PUT
    @Path("{uniqueId}")
    @SecuredService(permissions = {"user:update", "user:update_own", "user:update_contact"})
    public Response updateUser(@Context HttpServletRequest httpServletRequest,
                               @PathParam("organizationUniqueId") long organizationUniqueId,
                               @PathParam("uniqueId") long uniqueId,
                               UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateUser( uniqueId: {0} )", new Object[] {uniqueId});
        UserAPI sessionUserAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        boolean isUpdateUser = authHandler.hasPermission("user:update");
        boolean isUpdateUserOwn = authHandler.hasPermission("user:update_own");
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if( !authorizeHandleUsers(organizationUniqueId, uniqueId, isUpdateUser, isUpdateUserOwn, sessionUserAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        userAPI = userController.updateUser(organizationUniqueId, uniqueId, userAPI, isUpdateUser || isUpdateUserOwn, sessionUserAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest), isSuperAdmin);
        if( uniqueId == sessionUserAPI.getId() ) {
            // must update session profile
            authHandler.addToSession(AuthHandler.USER_API_SESSION_KEY, userAPI);
        }
        if( userAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(userAPI).build();
        }
    }

    /**
     * REMOVED UNTIL WE KNOW EXACTLY HOW TO HANDLE USER REMOVE
     * Delete a user on an organization.
     *
     * @param organizationUniqueId the unique id of the organization of the user
     * @param uniqueId the unique id of the user
     * @return HTTP 200 or HTTP 404 if the user is not found fails
     **/
    @POST
    @Path("{uniqueId}")
    @Produces(MediaType.TEXT_PLAIN + ";charset=UTF-8")
    @SecuredService(permissions = {"user:delete","user:delete_own"})
    public Response deleteUser(@Context HttpServletRequest httpServletRequest, String reasonCode,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("uniqueId") long uniqueId) {

        LOG.log(Level.FINEST, "deleteUser( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        boolean isDelete = authHandler.hasPermission("user:delete");
        boolean isDeleteOwn = authHandler.hasPermission("user:update_own");
        UserAPI sessionUserAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleUsers(organizationUniqueId, uniqueId, isDelete, isDeleteOwn, sessionUserAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        if(reasonCode == null || reasonCode.isEmpty()){
            UserEngagement userEngagement = userController.deleteUser(organizationUniqueId, uniqueId, sessionUserAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
            if( userEngagement == null ) {
                return Response.status(Response.Status.NOT_FOUND).build();
            } else {
                return Response.ok().build();
            }
        }
        else{
            UserEngagement userEngagement = userController.deleteUserGDPR(organizationUniqueId, uniqueId, reasonCode, sessionUserAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
            if( userEngagement == null ) {
                return Response.status(Response.Status.NOT_FOUND).build();
            } else {
                return Response.ok().build();
            }
        }
    }


    /**
     * Get current users total available notification types in :all <code>Map<string, string></code>
     * Get current users actual chosen notification types in :user <code>Map<string, string></code>
     *
     *
     * /organizations/{organizationUniqueId}/users/{userUniqueId}/notifications
     *
     * @return  Object containing all available values and the user specific ones.
     *          { all: Map<string, string>, user: Map <string, string> }
     */
    @GET
    @Path("{uniqueId}/notifications")
    @Produces(value = MediaType.APPLICATION_JSON + ";charset=utf-8")
    @SecuredService(permissions = {"user:view"})
    public Response getNotificationTypes(@PathParam("organizationUniqueId") Long organizationId,
                                         @PathParam("uniqueId") Long userId) {

        LOG.log(Level.FINEST, "user:get:getNotificationTypes()");

        final NotificationDTO notificationDTO = notificationMapper.toDTO(new NotificationRequest());
        notificationDTO.setUserId(userId + 1); //wtf?
        notificationDTO.setOrganizationId(organizationId);

        final NotificationResponse notificationResponse = notificationMapper
                .toResponse(notificationController.getUserNotifications(notificationDTO));

        return Response.status(200).entity(notificationResponse).build();

    }


    /**
     *
     * @param organizationId organizationID
     * @param userId userId
     * @param body Array of Strings
     * @return Response with HttpStatus.
     */
    @PUT
    @Path("{uniqueId}/notifications")
    @Consumes(value = MediaType.APPLICATION_JSON + ";charset=utf-8")
    @SecuredService(permissions = {"user:view"})
    public Response setNotificationTypes(@PathParam("organizationUniqueId") long organizationId,
                                         @PathParam("uniqueId") long userId,
                                         Set<String> body) {

        LOG.log(Level.FINEST, "userService:setNotificationTypes()");

        NotificationRequest request = new NotificationRequest();
        request.setUserId(userId + 1);
        request.setOrganizationId(organizationId);
        request.setUserNotifications(body);

        final NotificationDTO notificationDTO = notificationMapper.toDTO(request);

        try {
            notificationController.updateUserNotifications(notificationDTO);
        } catch (HjalpmedelstjanstenException exc) {
            notificationDTO.getError().put("Exception occurred: ", exc.getMessage());
        }

        final NotificationResponse notificationResponse = notificationMapper.toResponse(notificationDTO);

        return Response.status(201)
            .entity(notificationResponse)
            .build();

    }


    private boolean authorizeHandleUsers(long organizationUniqueId, Long userUniqueId, boolean isHandleUser, boolean isHandlerUserOwn, UserAPI userAPI) {
        if( isHandleUser ) {
            return true;
        } else {
            if( !authorizeHandleOrganization(organizationUniqueId, userAPI)) {
                return false;
            }
            if( !isHandlerUserOwn ) {
                // users can only update themselves
                if( !userUniqueId.equals(userAPI.getId()) ) {
                    LOG.log( Level.WARNING, "Attempt to handle user not self" );
                    return false;
                }
            }
        }
        return true;
    }

}
