package se.inera.hjalpmedelstjansten.business.assortment.controller;

import se.inera.hjalpmedelstjansten.business.DateUtils;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.ExternalCategoryMapper;
import se.inera.hjalpmedelstjansten.business.user.controller.UserMapper;
import se.inera.hjalpmedelstjansten.model.api.AssortmentAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountyAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVMunicipalityAPI;
import se.inera.hjalpmedelstjansten.model.entity.Assortment;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCounty;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVMunicipality;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for mapping between API and Entity classes
 *
 */
public class AssortmentMapper {

    public static final List<AssortmentAPI> map(List<Assortment> assortments) {
        if( assortments == null ) {
            return null;
        }
        List<AssortmentAPI> assortmentAPIs = new ArrayList<>();
        for( Assortment assortment : assortments ) {
            assortmentAPIs.add(map(assortment));
        }
        return assortmentAPIs;
    }

    public static final AssortmentAPI map(Assortment assortment) {
        if( assortment == null ) {
            return null;
        }
        AssortmentAPI assortmentAPI = new AssortmentAPI();
        assortmentAPI.setId(assortment.getUniqueId());
        assortmentAPI.setName(assortment.getName());
        assortmentAPI.setCustomer(OrganizationMapper.map(assortment.getCustomer(), false));
        assortmentAPI.setValidFrom(assortment.getValidFrom().getTime());
        if( assortment.getValidTo() != null ) {
            assortmentAPI.setValidTo(assortment.getValidTo().getTime());
        }
        if( assortment.getCounty() != null ) {
            assortmentAPI.setCounty(mapCounty(assortment.getCounty(), false));
        }
        if( assortment.getMunicipalities() != null ) {
            assortmentAPI.setMunicipalities(mapMunicipalities(assortment.getMunicipalities()));
        }
        if( assortment.getManagers() != null && !assortment.getManagers().isEmpty() ) {
            assortmentAPI.setManagers(UserMapper.map(assortment.getManagers(), false));
        }

        if (assortment.getExternalCategoryGroupings() != null && !assortment.getExternalCategoryGroupings().isEmpty()) {
            assortmentAPI.setExternalCategoriesGroupings(ExternalCategoryMapper.mapExc(assortment.getExternalCategoryGroupings()));

        }
        return assortmentAPI;
    }

    public static final Assortment map(AssortmentAPI assortmentAPI) {
        if( assortmentAPI == null ) {
            return null;
        }
        Assortment assortment = new Assortment();
        assortment.setName(assortmentAPI.getName());
        assortment.setValidFrom(DateUtils.beginningOfDay(assortmentAPI.getValidFrom()));
        if( assortmentAPI.getValidTo() != null ) {
            assortment.setValidTo(DateUtils.endOfDay(assortmentAPI.getValidTo()));
        }
        return assortment;
    }

    public static List<CVCountyAPI> mapCounties(List<CVCounty> counties, boolean includeMunicipalities) {
        if( counties == null ) {
            return null;
        }
        List<CVCountyAPI> countyMunicipalityAPIs = new ArrayList<>();
        for( CVCounty county : counties ) {
            countyMunicipalityAPIs.add(mapCounty(county, includeMunicipalities));
        }
        return countyMunicipalityAPIs;
    }

    public static CVCountyAPI mapCounty(CVCounty county, boolean includeMunicipalities) {
        if( county == null ) {
            return null;
        }
        CVCountyAPI countyAPI = new CVCountyAPI();
        countyAPI.setId(county.getUniqueId());
        countyAPI.setCode(county.getCode());
        countyAPI.setName(county.getName());
        if( county.getShowMunicipalities() && includeMunicipalities ) {
            countyAPI.setMunicipalities(mapMunicipalities(county.getMunicipalities()));
        }
        return countyAPI;
    }

    public static List<CVMunicipalityAPI> mapMunicipalities(List<CVMunicipality> municipalities) {
        if( municipalities == null ) {
            return null;
        }
        List<CVMunicipalityAPI> municipalityAPIs = new ArrayList<>();
        for( CVMunicipality municipality : municipalities ) {
            municipalityAPIs.add(mapMunicipality(municipality));
        }
        return municipalityAPIs;
    }

    public static CVMunicipalityAPI mapMunicipality(CVMunicipality municipality) {
        if( municipality == null ) {
            return null;
        }
        CVMunicipalityAPI municipalityAPI = new CVMunicipalityAPI();
        municipalityAPI.setId(municipality.getUniqueId());
        municipalityAPI.setCode(municipality.getCode());
        municipalityAPI.setName(municipality.getName());
        return municipalityAPI;
    }

}
