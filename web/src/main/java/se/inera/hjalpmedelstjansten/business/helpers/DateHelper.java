package se.inera.hjalpmedelstjansten.business.helpers;

import java.time.Year;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateHelper {

    private Calendar calendar = new GregorianCalendar();

    public Date formatStringDateAfterYYMMDD(String date) {
        return this.constructDate(date, 0, 2, 2, 4, 4, 6, true);
    }

    public Date formatStringDateAfterYYYYMMDD(String date) {
        return this.constructDate(date, 0, 4, 4, 6, 6, 8, false);
    }

    private Date constructDate(String date, int startYear, int endYear, int startMonth, int endMonth, int startDay, int endDay, boolean includeCentury) {

        int year;
        int month;
        int day;

        if(includeCentury) {
            String currentYear = Year.now().toString();
            year = Integer.valueOf(currentYear.substring(0, 2)+date.substring(startYear, endYear));

        } else {
            year = Integer.valueOf(date.substring(startYear, endYear));
        }

           month = Integer.valueOf(date.substring(startMonth, endMonth)) - 1;
           day = Integer.valueOf(date.substring(startDay, endDay));

            if(month < 0) {
                month = 0;
            }

            if(year < 0 ) {
                year = 0;
            }

            if(day < 0 ) {
                day = 0;
            }

        calendar.set(year, month, day);
        return calendar.getTime();
    }

}
