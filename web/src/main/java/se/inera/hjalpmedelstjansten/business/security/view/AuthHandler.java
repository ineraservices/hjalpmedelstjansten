package se.inera.hjalpmedelstjansten.business.security.view;

public interface AuthHandler {

    String USER_API_SESSION_KEY = "HJMTJ_USER_API_SESSION_KEY";

    void standardLogin(String username, String password);

    void tokenLogin(String username, String password);

    void addToSession(Object key, Object value);

    Object getFromSession(Object key);

    boolean hasRole(String role);

    boolean hasPermission(String permission);

    String getSessionId();

    boolean isAthenticated();
}
