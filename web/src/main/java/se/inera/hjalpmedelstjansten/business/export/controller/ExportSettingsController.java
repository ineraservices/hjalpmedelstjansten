package se.inera.hjalpmedelstjansten.business.export.controller;

import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.organization.controller.BusinessLevelController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.clustering.ClusterController;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.api.ExportSettingsAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.UserEngagementAPI;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;
import se.inera.hjalpmedelstjansten.model.entity.ExportSettings;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelist;
import se.inera.hjalpmedelstjansten.model.entity.Organization;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Class for handling business logic of ExportSettings. This includes talking to the
 * database.
 *
 */
@Stateless
public class ExportSettingsController {

    @Inject
    HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    ExportSettingsValidation exportSettingsValidation;

    @Inject
    OrganizationController organizationController;

    @Inject
    BusinessLevelController businessLevelController;

    @Inject
    GeneralPricelistController generalPricelistController;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    private ClusterController clusterController;

    public static final String QUEUE_NAME = "export_queue";

    /**
     * Search all export settings
     * @return a list of <code>ExportSettingsAPI</code>
     */
    public List<ExportSettingsAPI> search() {
        LOG.log(Level.FINEST, "getExportSettings()");
        return ExportSettingsMapper.map(em.createNamedQuery(ExportSettings.SEARCH_ALL).
                getResultList());
    }

    public ExportSettings find(long organizationUniqueId, long exportSettingsUniqueId) {
        LOG.log(Level.FINEST, "find( organizationUniqueId: {0}, exportSettingsUniqueId: {1} )", new Object[] {organizationUniqueId, exportSettingsUniqueId});
        ExportSettings exportSettings = em.find(ExportSettings.class, exportSettingsUniqueId);
        if( exportSettings == null ) {
            return null;
        }
        if( !exportSettings.getOrganization().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user to export settings : {0} not on given organization: {1}. Returning null.", new Object[] {exportSettingsUniqueId, organizationUniqueId});
            return null;
        }
        return exportSettings;
    }

    public Boolean queueExport(long exportSettingsId)
    {
        boolean lockReceived = clusterController.getLock(this.getClass().getName(), 30);
        if( lockReceived ) {
            List<ExportSettings> exportSettings = em.createNamedQuery(ExportSettings.FIND_ALL).getResultList();
            for ( ExportSettings exportSetting : exportSettings )
            {
                if(exportSetting.getUniqueId() == exportSettingsId)
                {
                    LOG.log(Level.INFO, String.format("Added %s to the queue",exportSetting.getFilename()));
                    clusterController.addLongToQueue(QUEUE_NAME, exportSettingsId);
                    return true;
                }
            }
            //release lock
            clusterController.releaseLock(this.getClass().getName());
        }
        return false;
    }

    /**
     * Create an <code>ExportSettings</code>
     *
     * @param exportSettingsAPI user supplied values
     * @return the created <code>ExportSettingsAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public ExportSettingsAPI create(ExportSettingsAPI exportSettingsAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "create(...)");
        exportSettingsValidation.validateForCreate(exportSettingsAPI);
        // find any existing export settings
        if( exportSettingsAPI.getBusinessLevel() != null ) {
            List<Long> businessLevelIds = new ArrayList<>();
            businessLevelIds.add(exportSettingsAPI.getBusinessLevel().getId());
            List<ExportSettings> exportSettingses = findByOrganizationAndBusinessLevels(exportSettingsAPI.getOrganization().getId(), businessLevelIds);
            if( exportSettingses != null && !exportSettingses.isEmpty() ) {
                throw validationMessageService.generateValidationException("organization", "exportsettings.organizationandbusinesslevel.exists");
            }
        } else {
            List<ExportSettings> exportSettingses = findByOrganizationAndNoBusinessLevels(exportSettingsAPI.getOrganization().getId());
            if( exportSettingses != null && !exportSettingses.isEmpty() ) {
                throw validationMessageService.generateValidationException("organization", "exportsettings.organization.exists");
            }
        }

        List<ExportSettings> exportSettingsWithSameName = em.createNamedQuery(ExportSettings.FIND_BY_FILE_NAME).
                setParameter("filename", exportSettingsAPI.getFilename()).
                getResultList();
        if( exportSettingsWithSameName != null && !exportSettingsWithSameName.isEmpty() ) {
            throw validationMessageService.generateValidationException("organization", "exportsettings.filename.exists", exportSettingsAPI.getFilename());
        }

        // organization
        Organization organization = organizationController.getOrganization(exportSettingsAPI.getOrganization().getId());
        if( organization == null ) {
            throw validationMessageService.generateValidationException("organization", "exportsettings.organization.notExists");
        }
        if( organization.getOrganizationType() != Organization.OrganizationType.CUSTOMER ) {
            throw validationMessageService.generateValidationException("organization", "exportsettings.organization.invalidType");
        }

        ExportSettings exportSettings = ExportSettingsMapper.map(exportSettingsAPI);

        exportSettings.setOrganization(organization);

        // business level
        if( exportSettingsAPI.getBusinessLevel() != null ) {
            BusinessLevel businessLevel = businessLevelController.getBusinessLevel(organization.getUniqueId(), exportSettingsAPI.getBusinessLevel().getId());
            if( businessLevel == null ) {
                throw validationMessageService.generateValidationException("organization", "exportsettings.businessLevel.notExists");
            }
            exportSettings.setBusinessLevel(businessLevel);
        }

        exportSettings.setEnabled(exportSettingsAPI.isEnabled());
        em.persist(exportSettings);

        return ExportSettingsMapper.map(exportSettings, true);
    }

    /**
     * Update <code>ExportSettings</code>. Currently only updates enabled field.
     *
     * @param exportSettingsUniqueId id of the ExportSettings to update
     * @param exportSettingsAPI user supplied values
     * @return the updated <code>ExportSettingsAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public ExportSettingsAPI update(long exportSettingsUniqueId, ExportSettingsAPI exportSettingsAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "create(...)");
        exportSettingsValidation.validateForUpdate(exportSettingsAPI);
        ExportSettings exportSettings = em.find(ExportSettings.class, exportSettingsUniqueId);
        if( exportSettings == null ) {
            LOG.log(Level.FINEST, "unable to update export settings: {0} cause it doesn't exist", new Object[] {exportSettingsUniqueId});
            throw validationMessageService.generateValidationException("id", "exportsettings.update.notexists");
        }
        exportSettings.setEnabled(exportSettingsAPI.isEnabled());

        return ExportSettingsMapper.map(exportSettings, true);
    }

    /**
     * Find a specific <code>ExportSettings</code>
     *
     * @param exportSettingsId id of the export settings to find
     * @return the corresponding <code>ExportSettingsAPI</code>
     */
    public ExportSettingsAPI find(long exportSettingsId) {
        LOG.log(Level.FINEST, "find(exportSettingsId: {0})", new Object[] {exportSettingsId});
        return ExportSettingsMapper.map(em.find(ExportSettings.class, exportSettingsId), true);
    }

    /**
     * Find a specific <code>ExportSettingsAPI</code>
     *
     * @param organizationUniqueId unique id of the organization
     * @param exportSettingsId id of the export settings to find
     * @return the corresponding <code>ExportSettingsAPI</code>
     */
    public ExportSettingsAPI findAPI(long organizationUniqueId, long exportSettingsId) {
        LOG.log(Level.FINEST, "findAPI( exportSettingsId: {0}, exportSettingsId: {1} )", new Object[] {exportSettingsId, exportSettingsId});
        return ExportSettingsMapper.map(find(organizationUniqueId, exportSettingsId), true);
    }

    /**
     * Find the <code>ExportSettings</code> for the organization and user. The users
     * business level (or lack of) decides which object is returned
     *
     * @param organizationUniqueId unique id of the organization
     * @param userEngagementAPI information of the users' engagement to find export settings for
     * @return the corresponding list of <code>ExportSettingsAPI</code> or null if none exist
     */
    public List<ExportSettingsAPI> findByOrganizationAndUser(long organizationUniqueId, UserEngagementAPI userEngagementAPI) {
        LOG.log(Level.FINEST, "findByOrganizationAndUser( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        if( userEngagementAPI.getBusinessLevels() == null || userEngagementAPI.getBusinessLevels().isEmpty() ) {
            // return all export settings for organization
            return ExportSettingsMapper.map(em.createNamedQuery(ExportSettings.FIND_BY_ORGANIZATION).
                    setParameter("organizationUniqueId", organizationUniqueId).
                    getResultList());
        } else {
            // return export settings matching business levels
            List<Long> businessLevelIds = getBusinessLevelIds(userEngagementAPI.getBusinessLevels());
            return ExportSettingsMapper.map(findByOrganizationAndBusinessLevels(organizationUniqueId, businessLevelIds));
        }

    }

    /**
     * Find <code>ExportSettings</code> for the given organization and business levels
     *
     * @param organizationUniqueId
     * @param businessLevelIds
     * @return
     */
    public List<ExportSettings> findByOrganizationAndBusinessLevels(long organizationUniqueId, List<Long> businessLevelIds) {
        LOG.log(Level.FINEST, "findByOrganizationAndBusinessLevels( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        return em.createNamedQuery(ExportSettings.FIND_BY_ORGANIZATION_AND_BUSINESS_LEVELS).
                    setParameter("organizationUniqueId", organizationUniqueId).
                    setParameter("businessLevelIds", businessLevelIds).
                    getResultList();
    }

    /**
     * Find <code>ExportSettings</code> for the given organization (no business level)
     *
     * @param organizationUniqueId
     * @param organizationUniqueId
     * @return
     */
    public List<ExportSettings> findByOrganizationAndNoBusinessLevels(long organizationUniqueId) {
        LOG.log(Level.FINEST, "findByOrganizationAndNoBusinessLevels( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        return em.createNamedQuery(ExportSettings.FIND_BY_ORGANIZATION_AND_NO_BUSINESS_LEVELS).
                    setParameter("organizationUniqueId", organizationUniqueId).
                    getResultList();
    }

    /**
     * Generate a List of Long corresponding to the ids of the business levels.
     *
     * @param businessLevels the list of business levels
     * @return a List of <code>Long</code>
     */
    private List<Long> getBusinessLevelIds(List<BusinessLevelAPI> businessLevels) {
        if( businessLevels == null || businessLevels.isEmpty() ) {
            return null;
        }
        List<Long> businessLevelIds = new ArrayList<>();
        businessLevels.forEach((businessLevelAPI) -> {
            businessLevelIds.add(businessLevelAPI.getId());
        });
        return businessLevelIds;
    }

    public ExportSettingsAPI addGeneralPricelistsToExportSetting(long organizationUniqueId, long exportSettingsUniqueId, List<GeneralPricelistAPI> generalPricelistAPIs) throws HjalpmedelstjanstenValidationException {
        ExportSettings exportSettings = find(organizationUniqueId, exportSettingsUniqueId);
        if( exportSettings == null ) {
            return null;
        }
        if( generalPricelistAPIs != null && !generalPricelistAPIs.isEmpty() ) {
            for( GeneralPricelistAPI generalPricelistAPI : generalPricelistAPIs ) {
                GeneralPricelist generalPricelist = em.find(GeneralPricelist.class, generalPricelistAPI.getId());
                if( generalPricelist == null ) {
                    throw validationMessageService.generateValidationException("generalPricelists", "exportsettings.generalPricelist.notExists");
                }
                if( exportSettings.getGeneralPricelists().contains(generalPricelist) ) {
                    // already in list, ignore
                    continue;
                }
                exportSettings.getGeneralPricelists().add(generalPricelist);
            }
        }
        return ExportSettingsMapper.map(exportSettings, true);
    }

    public ExportSettingsAPI deleteGeneralPricelistsToExportSetting(long organizationUniqueId, long exportSettingsUniqueId, List<GeneralPricelistAPI> generalPricelistAPIs) throws HjalpmedelstjanstenValidationException {
        ExportSettings exportSettings = find(organizationUniqueId, exportSettingsUniqueId);
        if( exportSettings == null ) {
            return null;
        }
        if( generalPricelistAPIs != null && !generalPricelistAPIs.isEmpty() ) {
            for( GeneralPricelistAPI generalPricelistAPI : generalPricelistAPIs ) {
                GeneralPricelist generalPricelist = em.find(GeneralPricelist.class, generalPricelistAPI.getId());
                if( generalPricelist == null ) {
                    throw validationMessageService.generateValidationException("generalPricelists", "exportsettings.generalPricelist.notExists");
                }
                exportSettings.getGeneralPricelists().remove(generalPricelist);
            }
        }
        return ExportSettingsMapper.map(exportSettings, true);
    }



}
