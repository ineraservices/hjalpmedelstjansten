package se.inera.hjalpmedelstjansten.business.user.domain;

import lombok.Data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Data
public class NotificationResponse {

    private Map<String, String> allNotifications = new HashMap<>();
    private Set<String> userNotifications = new HashSet<>();
    private Map<String, String> errors = new HashMap<>();

}
