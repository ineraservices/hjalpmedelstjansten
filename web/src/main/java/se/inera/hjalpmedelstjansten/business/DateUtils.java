package se.inera.hjalpmedelstjansten.business;

import java.util.Calendar;
import java.util.Date;


public class DateUtils {
    
    public static Date endOfDay(Date date ) {        
        return endOfDay(date.getTime());
    }
    
    public static Date endOfDay(long time ) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        return c.getTime();
    }
    
    public static Date beginningOfDay(Date date ) {        
        return beginningOfDay(date.getTime());
    }
    
    public static Date beginningOfDay(long time ) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        return c.getTime();
    }
    
    public static boolean isSameDay( long time1, long time2 ) {
        Calendar c1 = Calendar.getInstance();
        c1.setTimeInMillis(time1);
        
        Calendar c2 = Calendar.getInstance();
        c2.setTimeInMillis(time2);
        
        if( c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) &&
                c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) &&
                c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH) ) {
            return true;
        }
        return false;
    }
    
}
