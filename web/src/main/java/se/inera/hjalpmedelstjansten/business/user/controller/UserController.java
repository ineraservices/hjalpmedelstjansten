package se.inera.hjalpmedelstjansten.business.user.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import jakarta.annotation.PostConstruct;
import jakarta.ejb.Asynchronous;
import jakarta.ejb.Stateless;
import jakarta.enterprise.event.Event;
import jakarta.inject.Inject;
import jakarta.mail.MessagingException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.BaseController;
import se.inera.hjalpmedelstjansten.business.DateUtils;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.logging.view.MdcToolkit;
import se.inera.hjalpmedelstjansten.business.organization.controller.BusinessLevelController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.security.controller.SecurityController;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.api.ChangeUserPasswordAPI;
import se.inera.hjalpmedelstjansten.model.api.DeletedUserAPI;
import se.inera.hjalpmedelstjansten.model.api.RequestChangeUserPasswordAPI;
import se.inera.hjalpmedelstjansten.model.api.RoleAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.UserEngagementAPI;
import se.inera.hjalpmedelstjansten.model.entity.ApprovedAgreementPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;
import se.inera.hjalpmedelstjansten.model.entity.DeletedUser;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.UserAccount;
import se.inera.hjalpmedelstjansten.model.entity.UserAccountToken;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

/**
 * Business methods for handling user accounts, tokens and engagements
 *
 */
@Stateless
public class UserController extends BaseController {

    @Inject
    private HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;

    @Inject
    private Event<InternalAuditEvent> internalAuditEvent;

    @Inject
    private UserValidation userValidation;

    @Inject
    private BusinessLevelController businessLevelController;

    @Inject
    private OrganizationController organizationController;

    @Inject
    private SecurityController securityController;

    @Inject
    private EmailController emailController;

    @Inject
    private ValidationMessageService validationMessageService;

    @Inject
    private RoleController roleController;

    @Inject
    private boolean welcomeEmailSendingEnabled;

    private String serviceBaseUrl;

    /**
     * Simple get UserAccount by Id.
     *
     * @param userId the unique Id of the user to get.
     * @return UserAccount or nothing.
     * @throws EntityNotFoundException In case of no match.
     */
    public UserAccount getUserById(long userId) throws EntityNotFoundException {
        final UserAccount userAccount = em.find(UserAccount.class, (userId));
        if(null == userAccount) throw new EntityNotFoundException("User with ID: " + userId + "not found.");
        return userAccount;
    }


    /**
     * Search users on a given organization for a given query.
     *
     * @param searchQuery user supplied query
     * @param organizationUniqueId the unique id of the organization
     * @param offset start returning from this search result
     * @param limit maximum number of search results to return
     * @param roleNames limit search to only users with the given role names
     * @return a list of <code>UserAPI</code> matching the query parameters
     */
    public List<UserAPI> searchUsers(boolean onlyActives, String searchQuery, long organizationUniqueId, int offset, int limit, List<UserRole.RoleName> roleNames) {
        LOG.log(Level.FINEST, "searchUsers( offset: {0}, limit: {1} )", new Object[] {offset, limit});
        // had to make this a little more complicated because of limitations to distinct
        // and order by
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT DISTINCT(ue.uniqueId), ue.validFrom, ue.validTo, ua.firstName, ua.lastName, ua.title, ua.username, ea.uniqueId, ea.email, ea.mobile, ea.telephone FROM UserEngagement ue ");
        queryBuilder.append("JOIN ue.userAccount ua ");
        queryBuilder.append("JOIN ua.electronicAddress ea ");
        if( roleNames != null && !roleNames.isEmpty() ) {
            queryBuilder.append("JOIN ue.roles uer ");
        }
        queryBuilder.append("WHERE ue.organization.uniqueId = :organizationUniqueId ");

        if(onlyActives){
            //queryBuilder.append("AND ue.validFrom <= current_date() AND (ue.validTo IS NULL OR ue.validTo > current_date()) ");
            queryBuilder.append("AND ue.validFrom <= current_timestamp() AND (ue.validTo IS NULL OR ue.validTo > current_timestamp()) ");
        }

        if( roleNames != null && !roleNames.isEmpty() ) {
            queryBuilder.append("AND uer.name IN :roleNames ");
        }
        if( searchQuery != null && !searchQuery.isEmpty() ) {
            queryBuilder.append("AND (ua.firstName LIKE :query OR ua.lastName LIKE :query OR ua.username LIKE :query OR ea.email LIKE :query) ");
        }
        queryBuilder.append("ORDER BY ua.lastName ASC");
        String queryString = queryBuilder.toString();
        LOG.log(Level.FINEST, "queryString: {0}", new Object[] {queryString});
        Query query = em.createQuery(queryString).
                setParameter("organizationUniqueId", organizationUniqueId).
                setFirstResult(offset).
                setMaxResults(limit);
        if( searchQuery != null && !searchQuery.isEmpty() ) {
            query.setParameter("query", "%" + searchQuery + "%");
        }
        if( roleNames != null && !roleNames.isEmpty() ) {
            query.setParameter("roleNames", roleNames);
        }
        List<UserEngagement> userEngagements = UserMapper.mapSearchToUserEngagements(query.getResultList());
        return UserMapper.map(userEngagements, false);
    }

    /**
     * Count the number of users on a given organization for a given query.
     *
     * @param searchQuery user supplied query
     * @param organizationUniqueId the unique id of the organization
     * @param roleNames limit search to only users with the given role names
     * @return the number of users found
     */
    public long countSearchUsers(String searchQuery, long organizationUniqueId, List<UserRole.RoleName> roleNames) {
        LOG.log(Level.FINEST, "countSearchUsers(...)");
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT COUNT(DISTINCT ue.uniqueId) FROM UserEngagement ue ");
        if( roleNames != null && !roleNames.isEmpty() ) {
            queryBuilder.append("JOIN ue.roles uer ");
        }
        queryBuilder.append("WHERE ue.organization.uniqueId = :organizationUniqueId ");
        if( roleNames != null && !roleNames.isEmpty() ) {
            queryBuilder.append("AND uer.name IN :roleNames ");
        }
        if( searchQuery != null && !searchQuery.isEmpty() ) {
            queryBuilder.append("AND (ue.userAccount.firstName LIKE :query OR ue.userAccount.lastName LIKE :query OR ue.userAccount.username LIKE :query OR ue.userAccount.electronicAddress.email LIKE :query) ");
        }
        String queryString = queryBuilder.toString();
        LOG.log(Level.FINEST, "queryString: {0}", new Object[] {queryString});
        Query query = em.createQuery(queryString).
                setParameter("organizationUniqueId", organizationUniqueId);
        if( searchQuery != null && !searchQuery.isEmpty() ) {
            query.setParameter("query", "%" + searchQuery + "%");
        }
        if( roleNames != null && !roleNames.isEmpty() ) {
            query.setParameter("roleNames", roleNames);
        }
        return (Long) query.getSingleResult();
    }

    /**
     * Get users on a given organization with specific roles
     *
     * @param organizationUniqueId the unique id of the organization
     * @param roleNames
     * @return a list of <code>UserAPI</code> matching the roleName
     */
    public List<UserAPI> getUsersInRolesOnOrganization(long organizationUniqueId, List<UserRole.RoleName> roleNames) {
        LOG.log(Level.FINEST, "getUsersInRolesOnOrganization( organizationUniqueId: {0}, roleNames: {1} )", new Object[] {organizationUniqueId, roleNames});
        return UserMapper.map(em.createNamedQuery(UserEngagement.GET_BY_ORGANIZATION_AND_ROLES).
                setParameter("organizationUniqueId", organizationUniqueId).
                setParameter("roleNames", roleNames).
                getResultList(), false);
    }

    /**
     * Count the number of engagements on the specified organization
     *
     * @param organizationUniqueId the unique id of the organization
     * @return the number of engagements found
     */
    public long countUserEngagementsOnOrganization(long organizationUniqueId) {
        LOG.log(Level.FINEST, "countUserEngagementsOnOrganization( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        return (Long) em.createNamedQuery(UserEngagement.COUNT_BY_ORGANIZATION).
                setParameter("organizationUniqueId", organizationUniqueId).
                getSingleResult();
    }

    /**
     * Get the <code>UserAPI</code> for the <code>UserEngagement</code>
     * with the given id
     *
     * @param organizationUniqueId unique id of the organization
     * @param uniqueId unique id of the user engagement API to get
     * @param userAPI
     * @param sessionId
     * @return the corresponding <code>UserAPI</code>
     */
    public UserAPI getUserAPI(long organizationUniqueId, long uniqueId, UserAPI userAPI, String sessionId, String requestIp) {
        LOG.log(Level.FINEST, "getUserAPI( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        UserEngagement userEngagement = getUserEngagement(organizationUniqueId, uniqueId);
        if( userEngagement != null ) {
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.USER, InternalAudit.ActionType.VIEW, userAPI.getId(), sessionId, uniqueId, requestIp));
            return UserMapper.map( userEngagement, true );
        }
        return null;
    }

    /**
     * Get the <code>UserEngagement</code> for the user on the specified organization
     *
     * @param organizationUniqueId unique id of the organization
     * @param uniqueId unique id of the user engagement to get
     * @return the corresponding <code>UserEngagement</code>
     */
    public UserEngagement getUserEngagement(long organizationUniqueId, long uniqueId) {
        LOG.log(Level.FINEST, "getUserEngagement( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        UserEngagement userEngagement = em.find(UserEngagement.class, uniqueId);
        if( userEngagement == null ) {
            return null;
        }
        if( !userEngagement.getOrganization().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user to access user engagement: {0} not on given organization: {1}. Returning 404.", new Object[] {uniqueId, organizationUniqueId});
            return null;
        }
        return userEngagement;
    }

    /**
     * Send a welcome email (same as when creating new user) to all users who have
     * not logged into the service or already has a account token.
     */
    @Asynchronous
    @TransactionTimeout(value = 2, unit = TimeUnit.HOURS)
    public void sendWelcomeAllNew(Map<String, String> mdc) {
        try {
            MdcToolkit.setMdc(mdc);
            sendWelcomeAllNew();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error sending welcome mail to all new", e);
        } finally {
            MdcToolkit.clearMdc();
        }
    }

    private void sendWelcomeAllNew() throws HjalpmedelstjanstenException, HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "sendWelcomeAllNew(welcomeEmailSendingEnabled: {0})", new Object[] {welcomeEmailSendingEnabled});
        if( !welcomeEmailSendingEnabled ) {
            throw validationMessageService.generateValidationException("email", "user.welcomeall.notenabled");
        }
        List<UserAccount> userAccounts = em.createQuery("SELECT ua FROM UserAccount ua").
                getResultList();
        if( userAccounts != null && !userAccounts.isEmpty() ) {
            for( UserAccount userAccount : userAccounts ) {
                List<UserAccountToken> userAccountTokens = em.createNamedQuery(UserAccountToken.FIND_BY_USERACCOUNT).
                        setParameter("userAccountUniqueId", userAccount.getUniqueId()).
                        getResultList();
                if( userAccountTokens != null && !userAccountTokens.isEmpty() ) {
                    // user already has token, do not send new
                    LOG.log(Level.INFO, "UserAccount: {0} already has account token, do not send welcome", new Object[] {userAccount.getUniqueId()});
                    continue;
                }
                if( userAccount.isHasLoggedIn() ) {
                    // user has been logged in before and does not need a welcome mail
                    LOG.log(Level.INFO, "UserAccount: {0} has previously logged in, do not send welcome", new Object[] {userAccount.getUniqueId()});
                    continue;
                }
                if( userAccount.getElectronicAddress() == null ||
                        userAccount.getElectronicAddress().getEmail() == null ||
                        userAccount.getElectronicAddress().getEmail().isEmpty() ) {
                    // no email on user
                    LOG.log(Level.WARNING, "UserAccount: {0} has no email, do not send welcome", new Object[] {userAccount.getUniqueId()});
                    continue;
                }
                createAndMailToken(userAccount);
            }
        }
    }

    /**
     * Create a user based on the information given. A <code>UserAccountTokenz</code>
     * is also created and an email with a link to create a new password is sent to
     * the email address of the created user.
     *
     * @param organizationUniqueId the organization
     * @param userAPI information from the ui
     * @param sessionUserAPI the logged in users information
     * @param sessionId id of the session
     * @param isSuperAdmin true if the current user is super admin
     * @return API of the created <code>UserEngagement</code>
     * @throws HjalpmedelstjanstenValidationException if a validation error occurs
     * @throws HjalpmedelstjanstenException if other type of error occurs to cause a rollback
     */
    public UserAPI createUser(long organizationUniqueId, UserAPI userAPI, UserAPI sessionUserAPI, String sessionId, String requestIp, boolean isSuperAdmin) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "createUser( organizationUniqueId: {0}, isSuperAdmin: {1} )", new Object[]{organizationUniqueId, isSuperAdmin});
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        if( organization == null ) {
            throw validationMessageService.generateValidationException("organizationId", "userEngagement.organization.notExists");
        }
        userValidation.validate( userAPI, false, null );
        UserEngagement userEngagement = UserMapper.map(userAPI);

        // login type, can only be set on create, not on update
        UserAccount.LoginType loginType = UserAccount.LoginType.PASSWORD;
        if( isSuperAdmin && userAPI.getLoginType() != null ) {
            loginType = UserAccount.LoginType.valueOf(userAPI.getLoginType());
        }
        userEngagement.getUserAccount().setLoginType(loginType);

        UserEngagementAPI userEngagementAPI = userAPI.getUserEngagements().get(0);

        // add organization
        userEngagement.setOrganization(organization);

        setUserRoles(userEngagementAPI, userEngagement, isSuperAdmin);

        // add business levels
        if( userEngagementAPI.getBusinessLevels() != null && !userEngagementAPI.getBusinessLevels().isEmpty() ) {
            if( organization.getOrganizationType() != Organization.OrganizationType.CUSTOMER ) {
                throw validationMessageService.generateValidationException("businessLevels", "userEngagement.businessLevel.notCustomer");
            }
            List<BusinessLevel> businessLevels = new ArrayList<>();
            for( BusinessLevelAPI businessLevelAPI : userEngagementAPI.getBusinessLevels() ) {
                BusinessLevel businessLevel = businessLevelController.getBusinessLevel(organizationUniqueId, businessLevelAPI.getId());
                if( businessLevel != null ) {
                    // make sure organization has this businesslevel
                    if( !organization.getBusinessLevels().contains(businessLevel) ) {
                        throw validationMessageService.generateValidationException("businessLevels", "userEngagement.businessLevel.notInOrganization");
                    }
                    businessLevels.add(businessLevel);
                }
            }
            userEngagement.setBusinessLevels(businessLevels);
        }

        String tokenLoginUrl = null;
        if( userEngagement.getUserAccount().getLoginType() == UserAccount.LoginType.TOKEN ) {
            String tokenSalt = securityController.generateSalt();
            int tokenIterations = securityController.generateIterations();
            String unhashedToken = UUID.randomUUID().toString();
            String hashedToken = securityController.hashPassword(unhashedToken, tokenSalt, tokenIterations);
            userEngagement.getUserAccount().setIterations(tokenIterations);
            userEngagement.getUserAccount().setSalt(tokenSalt);
            userEngagement.getUserAccount().setToken(hashedToken);
            tokenLoginUrl = serviceBaseUrl + "hjmtj/resources/v1/login?username=" + userEngagement.getUserAccount().getUsername() + "&token=" + unhashedToken;
        }

        // save
        em.persist(userEngagement);
        userAPI = UserMapper.map(userEngagement, true);

        if( userEngagement.getUserAccount().getLoginType() == UserAccount.LoginType.PASSWORD ) {
            createAndMailToken(userEngagement.getUserAccount());
        } else {
            userAPI.setLoginUrl(tokenLoginUrl);
        }

        // log audit
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.USER, InternalAudit.ActionType.CREATE, sessionUserAPI.getId(), sessionId, userEngagement.getUniqueId(), requestIp));
        return userAPI;
    }

    private void createAndMailToken(UserAccount userAccount) throws HjalpmedelstjanstenException {
        String tokenSalt = securityController.generateSalt();
        int tokenIterations = securityController.generateIterations();

        // create token for email verification
        String emailLinkToken = securityController.generateEmailLinkToken();
        String hashedEmailLinkToken = securityController.hashPassword(emailLinkToken, tokenSalt, tokenIterations);
        Calendar tokenExpiration = Calendar.getInstance();
        tokenExpiration.add(Calendar.DAY_OF_YEAR, 7); // valid 7 days exactly from now
        UserAccountToken userAccountToken = new UserAccountToken();
        userAccountToken.setIterations(tokenIterations);
        userAccountToken.setSalt(tokenSalt);
        userAccountToken.setToken(hashedEmailLinkToken);
        userAccountToken.setTokenExpires(tokenExpiration.getTime());
        userAccountToken.setUserAccount(userAccount);
        em.persist(userAccountToken);

        // send email
        try {
            emailController.sendAccountCreationEmail( userAccount.getElectronicAddress().getEmail(), emailLinkToken, serviceBaseUrl, userAccount.getUsername(), tokenExpiration.getTime() );
        } catch (UnsupportedEncodingException ex) {
            LOG.log(Level.WARNING, "Failed to create email verification link", ex);
            throw generateException("email", "user.accountcreate.sendmail.failed", ex, validationMessageService);
        } catch (MessagingException ex) {
            LOG.log(Level.WARNING, "Failed to send account creation email.", ex);
            throw generateException("email", "user.accountcreate.sendmail.failed", ex, validationMessageService);
        }
    }

    /**
     * Request to change password for user. Verify user supplied data and send
     * an email with a token link.
     *
     * @param requestChangeUserPasswordAPI
     * @throws HjalpmedelstjanstenValidationException
     * @throws HjalpmedelstjanstenException
     */
    public void requestChangePassword(RequestChangeUserPasswordAPI requestChangeUserPasswordAPI) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "requestChangePassword( ... )");
        String email = requestChangeUserPasswordAPI.getEmail();
        String username = "";
        try {
            username = this.findByUsername(requestChangeUserPasswordAPI.getUsername()).getUserAccount().getUsername();
        } catch(NullPointerException e) {
            LOG.log(Level.FINEST, "No such username can be found: " + username);
            throw validationMessageService.generateValidationException("requestResetPassword", "user.requestpassword.fail");
        }
        if( email == null || email.isEmpty() || username == null || username.isEmpty() ) {
            LOG.log( Level.FINEST, "Attempt to request change password, but missing email or username" );
            throw validationMessageService.generateValidationException("requestResetPassword", "user.requestpassword.fail");
        }
        UserEngagement userEngagement = findByEmail(email);
        if( userEngagement == null ) {
            LOG.log( Level.INFO, "Attempt to request change password with email not connected to user account" );
            throw validationMessageService.generateValidationException("requestResetPassword", "user.requestpassword.fail");
        }
        UserAccount userAccount = userEngagement.getUserAccount();
        if( !username.equals(userAccount.getUsername()) ) {
            LOG.log( Level.WARNING, "Attempt to request change password with username not connected to email" );
            throw validationMessageService.generateValidationException("requestResetPassword", "user.requestpassword.fail");
        }
        if( userEngagement.getUserAccount().getLoginType() == UserAccount.LoginType.TOKEN ) {
            LOG.log( Level.WARNING, "Attempt to request change password on account with token based login which is not valid" );
            throw validationMessageService.generateValidationException("requestResetPassword", "user.requestpassword.fail");
        }

        userAccount.setPassword(null);
        em.merge(userAccount);

        // remove all current tokens
        List<UserAccountToken> userAccountTokens = em.createNamedQuery(UserAccountToken.FIND_BY_USERACCOUNT).
                setParameter("userAccountUniqueId", userAccount.getUniqueId()).
                getResultList();
        if( userAccountTokens != null && !userAccountTokens.isEmpty() ) {
            for(UserAccountToken userAccountToken : userAccountTokens ) {
                em.remove(userAccountToken);
            }
        }

        String tokenSalt = securityController.generateSalt();
        int tokenIterations = securityController.generateIterations();
        // create token for email verification
        String emailLinkToken = securityController.generateEmailLinkToken();
        String hashedEmailLinkToken = securityController.hashPassword(emailLinkToken, tokenSalt, tokenIterations);
        Calendar tokenExpiration = Calendar.getInstance();
        tokenExpiration.add(Calendar.HOUR, 1); // valid 1 hour exactly from now
        UserAccountToken userAccountToken = new UserAccountToken();
        userAccountToken.setIterations(tokenIterations);
        userAccountToken.setSalt(tokenSalt);
        userAccountToken.setToken(hashedEmailLinkToken);
        userAccountToken.setTokenExpires(tokenExpiration.getTime());
        userAccountToken.setUserAccount(userEngagement.getUserAccount());
        em.persist(userAccountToken);

        // send email
        try {
            emailController.sendResetPasswordEmail(userEngagement.getUserAccount().getElectronicAddress().getEmail(), emailLinkToken, serviceBaseUrl, userEngagement.getUserAccount().getUsername() );
        } catch (UnsupportedEncodingException ex) {
            LOG.log(Level.WARNING, "Failed to create email verification link on reset password request", ex);
            throw generateException("requestResetPassword", "user.requestpassword.currentlyunavailable", ex, validationMessageService);
        } catch (MessagingException ex) {
            LOG.log(Level.WARNING, "Failed to create email verification link on reset password request", ex);
            throw generateException("requestResetPassword", "user.requestpassword.currentlyunavailable", ex, validationMessageService);
        }

    }

    /**
     * Find a <code>UserEngagement</code> with the given email address.
     *
     * @param email
     * @return a <code>UserEngagement</code> or null if none exist
     */
    public UserEngagement findByEmail(String email) {
        LOG.log(Level.FINEST, "findByEmail()");
        List<UserEngagement> userEngagements = em.createNamedQuery(UserEngagement.FIND_BY_EMAIL).
                setParameter("email", email).
                getResultList();
        UserEngagement userEngagement = null;
        if( userEngagements != null && !userEngagements.isEmpty() ) {
            userEngagement = userEngagements.get(0);
        }
        return userEngagement;
    }

    /**
     * Find a <code>UserEngagement</code> with the given username.
     *
     * @param username the username to look for
     * @return a <code>UserEngagement</code> or null if none exist
     */
    public UserEngagement findByUsername(String username) {
        LOG.log(Level.FINEST, "findByUsername()");
        List<UserEngagement> userEngagements = em.createNamedQuery(UserEngagement.FIND_BY_USERNAME).
                setParameter("username", username).
                getResultList();
        UserEngagement userEngagement = null;
        if( userEngagements != null && !userEngagements.isEmpty() ) {
            userEngagement = userEngagements.get(0);
        }
        return userEngagement;
    }

    /**
     * Update the user with the given id on the specified organization. The user
     * can only update adressses while the superadmin or local admin can update
     * more.
     *
     * @param organizationUniqueId the unique id of the organization
     * @param uniqueId the unique id of the user
     * @param userAPI the user supplied data
     * @param isAdmin decides whether user basic information (like title) can be updated or just electronic address
     * @param sessionUserAPI
     * @param sessionId
     * @return the updated user
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case of validation failure
     */
    public UserAPI updateUser(long organizationUniqueId, long uniqueId, UserAPI userAPI, boolean isAdmin, UserAPI sessionUserAPI, String sessionId, String requestIp, boolean isSuperAdmin) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateUser( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        UserEngagement dbUserEngagement = getUserEngagement(organizationUniqueId, uniqueId);
        if( dbUserEngagement == null ) {
            return null;
        }
        userValidation.validate( userAPI, true, dbUserEngagement );
        if( isAdmin ) {
            updateUserBasics(dbUserEngagement, userAPI);
            UserEngagementAPI userEngagementAPI = userAPI.getUserEngagements().get(0);
            setUserRoles(userEngagementAPI, dbUserEngagement, isSuperAdmin);
        }
        updateUserAddresses(dbUserEngagement, userAPI);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.USER, InternalAudit.ActionType.UPDATE, sessionUserAPI.getId(), sessionId, uniqueId, requestIp));
        return UserMapper.map( dbUserEngagement, true );
    }

    /**
     * Delete the user with the given id on the specified organization.
     *
     * @param organizationUniqueId the unique id of the organization
     * @param uniqueId the unique id of the user
     * @param sessionUserAPI
     * @param sessionId
     * @return the deleted <code>UserEngagement</code>
     */
    public UserEngagement deleteUser(long organizationUniqueId, long uniqueId, UserAPI sessionUserAPI, String sessionId, String requestIp) {
        LOG.log(Level.FINEST, "deleteUser( uniqueId: {0} )", new Object[] {uniqueId});
        UserEngagement userEngagement = getUserEngagement(organizationUniqueId, uniqueId);
        if( userEngagement == null ) {
            return null;
        }
        // first we need to remove any tokens
        UserAccountToken userAccountToken = getTokenForUserAccount(userEngagement.getUserAccount());
        if( userAccountToken != null ) {
            em.remove(userAccountToken);
        }
        // TODO: can a user be removed from db or just anonymized?
        em.remove(userEngagement);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.USER, InternalAudit.ActionType.DELETE, sessionUserAPI.getId(), sessionId, uniqueId, requestIp));
        return userEngagement;
    }

    public UserEngagement deleteUserGDPR(long organizationUniqueId, long uniqueId, String reasonCode ,UserAPI sessionUserAPI, String sessionId, String requestIp) {
        LOG.log(Level.FINEST, "deleteUser( uniqueId: {0} )", new Object[] {uniqueId});
        UserEngagement userEngagement = getUserEngagement(organizationUniqueId, uniqueId);
        UserAPI fullUserAPI = getUserAPI(organizationUniqueId, uniqueId, sessionUserAPI, "", null);
        //UserEngagement adminEngagement = getUserEngagement(organizationUniqueId, sessionUserAPI.getId());
        if( userEngagement == null ) {
            return null;
        }
        Date date = new Date();
        List<UserEngagement> organizationUsers = em.createNamedQuery(UserEngagement.GET_BY_ORGANIZATION).setParameter("organizationUniqueId",organizationUniqueId).getResultList();
        int inactiveCount = 0;
        for(UserEngagement ue : organizationUsers){
            if(ue.getValidTo() != null){
                if(ue.getValidTo().before(date)){
                    inactiveCount ++;
                }
            }
        }
        if(inactiveCount == organizationUsers.size()){
            //all users are inactive
            List<ApprovedAgreementPricelistRow> approvedRowsByUser = em.createNamedQuery(ApprovedAgreementPricelistRow.FIND_BY_USERID).setParameter("approvedBy",fullUserAPI.getFirstName() + " " + fullUserAPI.getLastName() + " (" + fullUserAPI.getUsername() + ")").getResultList();
            if(approvedRowsByUser.size() > 0 && approvedRowsByUser != null){
                for(ApprovedAgreementPricelistRow ar : approvedRowsByUser){
                    ar.setApprovedBy("deleted user");
                    em.persist(ar);
                }
            }
        }
        // first we need to remove any tokens
        UserAccountToken userAccountToken = getTokenForUserAccount(userEngagement.getUserAccount());
        if( userAccountToken != null ) {
            em.remove(userAccountToken);
        }
        // TODO: can a user be removed from db or just anonymized?

        String sql = "DELETE FROM hjmtj.AgreementUserEngagementCustomer WHERE userEngagementId = " + userEngagement.getUniqueId();
        em.createNativeQuery(sql).executeUpdate();

        em.remove(userEngagement);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.USER, InternalAudit.ActionType.DELETE, sessionUserAPI.getId(), sessionId, uniqueId, requestIp));

        try{

            Calendar calendar = Calendar.getInstance();
            DeletedUserAPI deletedUser = new DeletedUserAPI();
            deletedUser.setDeletedByUserId(sessionUserAPI.getId());
            deletedUser.setOldUniqueId(uniqueId);
            deletedUser.setDeletedUserOrg(organizationUniqueId);
            deletedUser.setDeletedDate(calendar.getTime());
            deletedUser.setReasonCode(reasonCode);
            writeDeletedUser(deletedUser);
        }
        catch(Exception e ){
            LOG.log(Level.INFO, e.getMessage());
        }

        return userEngagement;
    }

    public DeletedUserAPI writeDeletedUser(DeletedUserAPI du){
        if(du != null){
            DeletedUser dud = new DeletedUser();
            dud.setDeletedByUserId(du.getDeletedByUserId());
            dud.setOldUniqueId(du.getOldUniqueId());
            dud.setDeletedDate(du.getDeletedDate());
            dud.setReasonCode(du.getReasonCode());
            dud.setDeletedUserOrg(du.getDeletedUserOrg());
            em.persist(dud);
            du.setUniqueId(dud.getUniqueId());
            return du;
        }
        else{
            return null;
        }
    }


    /**
     * Update user information except address information.
     *
     * @param dbUserEngagement the userengagement to update
     * @param userAPI the user supplied data
     * @throws HjalpmedelstjanstenValidationException in case of validation failure
     */
    private void updateUserBasics(UserEngagement dbUserEngagement, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        dbUserEngagement.getUserAccount().setUsername(userAPI.getUsername());
        dbUserEngagement.getUserAccount().setFirstName(userAPI.getFirstName());
        dbUserEngagement.getUserAccount().setLastName(userAPI.getLastName());
        dbUserEngagement.getUserAccount().setTitle(userAPI.getTitle());
        UserEngagementAPI userEngagementAPI = userAPI.getUserEngagements().get(0);
        dbUserEngagement.setValidFrom(new Date(userEngagementAPI.getValidFrom()));
        if( userEngagementAPI.getValidTo() != null ) {
            dbUserEngagement.setValidTo(DateUtils.endOfDay(new Date(userEngagementAPI.getValidTo())));
        } else {
            dbUserEngagement.setValidTo(null);
        }

        // use organization from saved engagement since it's not possible to change organization
        Organization organization = organizationController.getOrganization(dbUserEngagement.getOrganization().getUniqueId());

        // add business levels
        List<BusinessLevel> businessLevels = new ArrayList<>();
        if( userEngagementAPI.getBusinessLevels() != null && !userEngagementAPI.getBusinessLevels().isEmpty() ) {
            if( organization.getOrganizationType() != Organization.OrganizationType.CUSTOMER ) {
                throw validationMessageService.generateValidationException("businessLevels", "userEngagement.businessLevel.notCustomer");
            }
            for( BusinessLevelAPI businessLevelAPI : userEngagementAPI.getBusinessLevels() ) {
                BusinessLevel businessLevel = businessLevelController.getBusinessLevel(organization.getUniqueId(), businessLevelAPI.getId());
                if( businessLevel != null ) {
                    // make sure organization has this businesslevel
                    if( !organization.getBusinessLevels().contains(businessLevel) ) {
                        throw validationMessageService.generateValidationException("businessLevels", "userEngagement.businessLevel.notInOrganization");
                    }
                    businessLevels.add(businessLevel);
                }
            }
        }
        dbUserEngagement.setBusinessLevels(businessLevels);
    }

    /**
     * Update user address information.
     *
     * @param dbUserEngagement the userengagement to update
     * @param userAPI the user supplied data
     */
    private void updateUserAddresses(UserEngagement dbUserEngagement, UserAPI userAPI) {
        LOG.log(Level.FINEST, "updateUserAddresses( ... )");
        if( userAPI.getElectronicAddress() != null ) {
            dbUserEngagement.getUserAccount().getElectronicAddress().setTelephone(userAPI.getElectronicAddress().getTelephone());
            dbUserEngagement.getUserAccount().getElectronicAddress().setMobile(userAPI.getElectronicAddress().getMobile());
            dbUserEngagement.getUserAccount().getElectronicAddress().setEmail(userAPI.getElectronicAddress().getEmail());
        }
    }

    /**
     * Validate the user supplied token
     *
     * @param changeUserPasswordAPI user supplied data
     * @param userAccountToken database token to compare user supplied data with
     * @return whether the token validates or not
     */
    private boolean validateEmailToken(ChangeUserPasswordAPI changeUserPasswordAPI, UserAccountToken userAccountToken) {
        LOG.log(Level.FINEST, "validateEmailToken( ... )");
        if( userAccountToken == null ) {
            LOG.log( Level.INFO, "Attempt to validate email token for a UserAccount where no token has been registered or no longer exists" );
            return false;
        }
        Date now = new Date();
        if( now.after(userAccountToken.getTokenExpires()) ) {
            LOG.log( Level.WARNING, "Attempt to validate email token for a UserAccount with expired token" );
            return false;
        }
        String hashedToken = securityController.hashPassword(changeUserPasswordAPI.getToken(), userAccountToken.getSalt(), userAccountToken.getIterations());
        if( !hashedToken.equals(userAccountToken.getToken()) ) {
            LOG.log( Level.WARNING, "Attempt to validate email token for a UserAccount with invalid token" );
            return false;
        }
        return true;
    }

    /**
     * Token validates if:
     * - user with email exists
     * - a token has been requested
     * - token is valid
     * - supplied token matches saved token for user with given email
     *
     * @param changeUserPasswordAPI the user supplied data
     * @return whether token validates or not
     */
    public boolean validateEmailToken(ChangeUserPasswordAPI changeUserPasswordAPI) {
        LOG.log(Level.FINEST, "validateEmailToken( ... )");
        String email = changeUserPasswordAPI.getEmail();
        UserEngagement userEngagement = findByEmail(email);
        if( userEngagement == null ) {
            LOG.log( Level.WARNING, "Attempt to validate email token with email that does not exist" );
            return false;
        }
        UserAccountToken userAccountToken = getTokenForUserAccount(userEngagement.getUserAccount());
        return validateEmailToken(changeUserPasswordAPI, userAccountToken);
    }

    /**
     * Change password of user based on token.
     *
     * @param changeUserPasswordAPI the user supplied data
     * @return whether the password was updated or not
     * @throws HjalpmedelstjanstenValidationException in case of validation failure
     */
    public boolean changePasswordWithEmailToken(ChangeUserPasswordAPI changeUserPasswordAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "changePasswordWithEmailToken( ... )");
        String email = changeUserPasswordAPI.getEmail();
        UserEngagement userEngagement = findByEmail(email);
        UserAccount userAccount = userEngagement.getUserAccount();
        UserAccountToken userAccountToken = getTokenForUserAccount(userAccount);
        if( userAccountToken == null ) {
            LOG.log( Level.WARNING, "Attempt to change password with email token for a UserAccount with token available" );
            return false;
        }
        if( !validateEmailToken(changeUserPasswordAPI, userAccountToken) ) {
            return false;
        }
        if( !securityController.validatePassword(changeUserPasswordAPI.getNewPassword(), userEngagement.getUserAccount().getUsername(), email) ) {
            throw validationMessageService.generateValidationException("newPassword", "user.password.invalid");
        }

        // ok, token is validated and password is ok, change it
        String salt = securityController.generateSalt();
        int iterations = securityController.generateIterations();
        String hashedPassword = securityController.hashPassword(changeUserPasswordAPI.getNewPassword(), salt, iterations);
        userAccount.setSalt(salt);
        userAccount.setIterations(iterations);
        userAccount.setPassword(hashedPassword);

        em.merge(userAccount);
        em.remove(userAccountToken); // remove token

        return true;
    }

    /**
     * Delete all expired tokens. Most likely called from some sort of time job.
     */
    public void deleteExpiredUserAccountTokens() {
        LOG.log(Level.FINEST, "deleteExpiredUserAccountTokens( ... )");
        Date date = new Date();
        List<UserAccountToken> userAccountTokens = em.createNamedQuery(UserAccountToken.FIND_BY_EXPIRED_TOKENS).
                setParameter("date", date).
                getResultList();
        if( userAccountTokens != null ) {
            LOG.log(Level.FINEST, "deleting: {0} expired tokens for date: {1}", new Object[] {userAccountTokens.size(), date});
            for( UserAccountToken userAccountToken : userAccountTokens ) {
                em.remove(userAccountToken);
            }
        }
    }

    /**
     * Find a token for the specified user account.
     *
     * @param userAccount the user account to look for tokens on
     * @return a token if any was found
     */
    private UserAccountToken getTokenForUserAccount(UserAccount userAccount) {
        List<UserAccountToken> userAccountTokens = em.createNamedQuery(UserAccountToken.FIND_BY_USERACCOUNT).
                setParameter("userAccountUniqueId", userAccount.getUniqueId()).
                getResultList();
        if( userAccountTokens != null && !userAccountTokens.isEmpty() ) {
            return userAccountTokens.get(0);
        }
        return null;
    }

    public List<UserEngagement> findByBusinessLevel(long businessLevelUniqueId) {
        LOG.log(Level.FINEST, "findByBusinessLevel( businessLevelUniqueId: {0} )", new Object[] {businessLevelUniqueId});
        return em.createNamedQuery(UserEngagement.FIND_BY_BUSINESS_LEVEL).
                setParameter("businessLevelUniqueId", businessLevelUniqueId).
                getResultList();
    }

    /**
     * Count users on a given organization by query and in specific business levels.
     * If param businessLevelIds is null or empty, users that has NO business level
     * setis returned.
     *
     * @param query
     * @param organizationUniqueId the unique id of the organization
     * @param businessLevelsIds ids of the businesslevels to look for
     * @return a list of <code>UserAPI</code> matching the roleName
     */
    public long countSearchUsersInBusinessLevels(String query, long organizationUniqueId, List<Long> businessLevelsIds) {
        LOG.log(Level.FINEST, "countSearchUsersInBusinessLevels( organizationUniqueId: {0}, businessLevelsIds: {1} )", new Object[] {organizationUniqueId, businessLevelsIds});
        if( businessLevelsIds != null && businessLevelsIds.isEmpty() ) {
            return (Long) em.createNamedQuery(UserEngagement.COUNT_BY_ORGANIZATION_AND_NO_BUSINESS_LEVELS).
                    setParameter("organizationUniqueId", organizationUniqueId).
                    setParameter("query", "%" + query + "%").
                    getSingleResult();
        } else {
            return (Long) em.createNamedQuery(UserEngagement.COUNT_BY_ORGANIZATION_AND_BUSINESS_LEVELS).
                    setParameter("organizationUniqueId", organizationUniqueId).
                    setParameter("businessLevelsIds", businessLevelsIds).
                    setParameter("query", "%" + query + "%").
                    getSingleResult();
        }
    }

    /**
     * Search users on a given organization by query and in specific business
     * levels. If param businessLevelIds is null or empty, users that has NO
     * business level set is returned.
     *
     * @param query
     * @param organizationUniqueId the unique id of the organization
     * @param businessLevelsIds ids of the businesslevels to look for
     * @param offset
     * @param limit
     * @return a list of <code>UserAPI</code> matching the roleName
     */
    public List<UserAPI> searchUsersInBusinessLevels(String query, long organizationUniqueId, List<Long> businessLevelsIds, int offset, int limit) {
        LOG.log(Level.FINEST, "searchUsersInBusinessLevels( organizationUniqueId: {0}, businessLevelsIds: {1} )", new Object[] {organizationUniqueId, businessLevelsIds});
        if( businessLevelsIds == null || businessLevelsIds.isEmpty() ) {
            return UserMapper.map(em.createNamedQuery(UserEngagement.SEARCH_BY_ORGANIZATION_AND_NO_BUSINESS_LEVELS).
                    setParameter("organizationUniqueId", organizationUniqueId).
                    setParameter("query", "%" + query + "%").
                    setMaxResults(limit).
                    setFirstResult(offset).
                    getResultList(), false);
        } else {
            return UserMapper.map(em.createNamedQuery(UserEngagement.SEARCH_BY_ORGANIZATION_AND_BUSINESS_LEVELS).
                    setParameter("organizationUniqueId", organizationUniqueId).
                    setParameter("businessLevelsIds", businessLevelsIds).
                    setParameter("query", "%" + query + "%").
                    setMaxResults(limit).
                    setFirstResult(offset).
                    getResultList(), false);
        }
    }

    /**
     * Find a <code>UserRole</code> for the specified name.
     *
     * @param userRoleName the role name to look for
     * @return the corresponding <code>UserRole</code>
     */
    public UserRole findRoleByName(UserRole.RoleName userRoleName) {
        LOG.log(Level.FINEST, "findRoleByName( userRoleName: {0} )", new Object[] {userRoleName});
        List<UserRole.RoleName> userRoleNames = new ArrayList<>();
        userRoleNames.add(userRoleName);
        List<UserRole> userRoles = em.createNamedQuery(UserRole.FIND_BY_NAMES).
                setParameter("names", userRoleNames).
                getResultList();
        UserRole userRole = null;
        if( userRoles != null && !userRoles.isEmpty() ) {
            userRole = userRoles.get(0);
        }
        return userRole;
    }

    public UserRole findRoleById(long userRoleUniqueId) {
        LOG.log(Level.FINEST, "findRoleById( userRoleUniqueId: {0} )", new Object[] {userRoleUniqueId});
        return em.find(UserRole.class, userRoleUniqueId);
    }

    public void updateUserAccount(UserAccount userAccount) {
        LOG.log(Level.FINEST, "updateUserAccount( ... )");
        em.merge(userAccount);
    }

    private void setUserRoles( UserEngagementAPI userEngagementAPI, UserEngagement userEngagement, boolean isSuperAdmin ) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setUserRoles( isSuperAdmin: {0} )", new Object[] {isSuperAdmin});
        // add base role
        List<UserRole> userRoles = new ArrayList<>();
        UserRole baseUserRole = findRoleByName(UserRole.RoleName.Baseuser);
        UserRole baseTokenUserRole = findRoleByName(UserRole.RoleName.BaseTokenuser);
        if( userEngagement.getUserAccount().getLoginType() == UserAccount.LoginType.PASSWORD ) {
            userRoles.add(baseUserRole);
        } else {
            userRoles.add(baseTokenUserRole);
        }
        if( !isSuperAdmin ) {
            UserRole existingRole = findExistingLocalAdminRole(userEngagement);
            if( existingRole != null ) {
                userRoles.add(existingRole);
            }
        }

        // add requested roles
        if( userEngagementAPI.getRoles() != null && !userEngagementAPI.getRoles().isEmpty() ) {
            for( RoleAPI roleAPI : userEngagementAPI.getRoles() ) {
                UserRole userRole = findRoleById(roleAPI.getId());
                if( userRole == null ) {
                    throw validationMessageService.generateValidationException("role", "userEngagement.role.notExists", userRole.getDescription());
                }
                if( userRole.getName() == UserRole.RoleName.Baseuser ) {
                    // ignore this, already added and everyone must have it
                    continue;
                }
                if( !roleController.validOrganizationRole(userRole, userEngagement.getOrganization()) ) {
                    throw validationMessageService.generateValidationException("role", "userEngagement.role.notInOrganization", userRole.getDescription());
                }
                if( userRole.getName() == UserRole.RoleName.Customeradmin || userRole.getName() == UserRole.RoleName.Supplieradmin ) {
                    // only the superadmin can changes this value
                    if( isSuperAdmin ) {
                        userRoles.add(userRole);
                    }
                } else {
                    if( userRole != baseUserRole && userRole != baseTokenUserRole ) {
                        // validate that the role is ok for t
                        userRoles.add(userRole);
                    }
                }
            }
        }
        userEngagement.setRoles(userRoles);
    }

    public static UserEngagementAPI getUserEngagementAPIByOrganizationId( long organizationUniqueId, List<UserEngagementAPI> userEngagementAPIs ) {
        if( userEngagementAPIs != null && !userEngagementAPIs.isEmpty() ) {
            for( UserEngagementAPI potentialUserEngagementAPI : userEngagementAPIs ) {
                if( potentialUserEngagementAPI.getOrganizationId().equals(organizationUniqueId) ) {
                    return potentialUserEngagementAPI;
                }
            }
        }
        return null;
    }

    private UserRole findExistingLocalAdminRole( UserEngagement userEngagement ) {
        List<UserRole> existingRoles = userEngagement.getRoles();
        UserRole.RoleName roleName = userEngagement.getOrganization().getOrganizationType() == Organization.OrganizationType.CUSTOMER ? UserRole.RoleName.Customeradmin: UserRole.RoleName.Supplieradmin;
        if( existingRoles != null && !existingRoles.isEmpty() ) {
            for( UserRole userRole : existingRoles ) {
                if( userRole.getName() == roleName ) {
                    return userRole;
                }
            }
        }
        return null;
    }

    @PostConstruct
    private void initialize() {
        serviceBaseUrl = System.getenv("HJMTJ_SERVICE_BASE_URL");
    }

}
