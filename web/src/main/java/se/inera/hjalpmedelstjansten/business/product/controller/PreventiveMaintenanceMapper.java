package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

import java.util.ArrayList;
import java.util.List;

public class PreventiveMaintenanceMapper {
    
    public static final List<CVPreventiveMaintenanceAPI> map(List<CVPreventiveMaintenance> preventiveMaintenances) {
        if( preventiveMaintenances == null ) {
            return null;
        }
        List<CVPreventiveMaintenanceAPI> preventiveMaintenanceAPIs = new ArrayList<>();
        for( CVPreventiveMaintenance preventiveMaintenance : preventiveMaintenances ) {
            preventiveMaintenanceAPIs.add(map(preventiveMaintenance));
        }
        return preventiveMaintenanceAPIs;
    }
    
    public static final CVPreventiveMaintenanceAPI map(CVPreventiveMaintenance preventiveMaintenance) {
        if( preventiveMaintenance == null ) {
            return null;
        }
        CVPreventiveMaintenanceAPI preventiveMaintenanceAPI = new CVPreventiveMaintenanceAPI();
        preventiveMaintenanceAPI.setId(preventiveMaintenance.getUniqueId());
        preventiveMaintenanceAPI.setCode(preventiveMaintenance.getCode());
        preventiveMaintenanceAPI.setName(preventiveMaintenance.getName());
        return preventiveMaintenanceAPI;
    }
    
}
