package se.inera.hjalpmedelstjansten.business.product.view;

import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.ResultExporterController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductController;
import se.inera.hjalpmedelstjansten.business.product.controller.SearchController;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.SearchAPI;
import se.inera.hjalpmedelstjansten.model.api.SearchProductsAndArticlesAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Product;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * REST API for searching articles and products. Some methods use an external search
 * engine (like elastic) for seach, other methods use basic sql queries
 *
 */
@Stateless
@Interceptors({ PerformanceLogInterceptor.class })
@Path("search")
public class SearchService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @Inject
    SearchController searchController;

    @Inject
    ProductController productController;

    @Inject
    ArticleController articleController;

    @Inject
    AuthHandler authHandler;

    @Inject
    ResultExporterController resultExporterController;

    @Inject
    OrganizationController organizationController;

    /**
     * The big search
     *
     * @param uriInfo query information
     * @return
     * @throws HjalpmedelstjanstenValidationException
     */
    @GET
    @SecuredService(permissions = {"product:view", "article:view"})
    public Response searchProductsAndArticles(@Context UriInfo uriInfo, @DefaultValue(value = "25") @QueryParam("limit") int limit,
                                              @DefaultValue(value = "") @QueryParam("sortType") String sortType,
                                              @DefaultValue(value = "0") @QueryParam("supplier") int supplier,
                                              @DefaultValue(value = "0") @QueryParam("assortmentId") long assortmentId,
                                              @DefaultValue("") @QueryParam("sortOrder") String sortOrder) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "searchProductsAndArticles( ... )");

        // search, default limit is 25 (hits per page) - Deprecated, setting default value of limit to 25 and letting the call query the limit variable in the head.
        SearchDTO searchDTO = searchController.searchProductsAndArticles(uriInfo.getQueryParameters(), limit, sortType, assortmentId, sortOrder);
        return Response.ok(searchDTO.getItems()).
                header("X-Total-Count", searchDTO.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }

    /**
     * Export big search
     *
     * @param uriInfo query information
     * @return an excel containing the requested products and articles
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
     */
    @POST
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"product:view", "article:view"})
    @Path("export")
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportProductsAndArticles(@Context UriInfo uriInfo) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "exportProductsAndArticles(...)");

        // search, we allow a maximum of 55000 hits in the excel, this is hard-coded since
        // this number should correspond to the setting index.max_result_window
        // in the file elasticsearch_index_source.json file
        int maximumNumberOfResults = 55000;
        SearchDTO searchDTO = searchController.searchProductsAndArticles(uriInfo.getQueryParameters(), maximumNumberOfResults, "NUMBER", 0, "ASC");
        if (searchDTO == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                String filename = "Export_ProdukterOchArtiklar_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";
                List<SearchProductsAndArticlesAPI> searchProductsAndArticlesAPIs = (List<SearchProductsAndArticlesAPI>) searchDTO.getItems();
                byte[] exportBytes = resultExporterController.generateProductsAndArticlesList(searchProductsAndArticlesAPIs);
                return Response.ok(
                        exportBytes,
                        jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                        .header("content-disposition", "attachment; filename = " + filename)
                        .build();
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to export organization product/article search to file", ex);
                return Response.serverError().build();
            }
        }
    }

    /**
     * Search products/articles for a specific organization.
     *
     * @param organizationUniqueId unique id of the organization
     * @param query                user supplied textual query string
     * @param offset               start returning search results from this point, for pagination
     * @param statuses             list of product/article status to include
     * @param articleTypes         list of article types to include in response, only affects
     *                             articles.
     * @param includeProducts      whether to include products in search results, if this is set
     *                             to false and includeArticles is also set to false, we assume both should be included
     * @param includeArticles      whether to include articles in search results, if this is set
     *                             to false and includeProducts is also set to false, we assume both should be included
     * @param searchAPI            if additional search data is supplied, like category specific properties
     * @return a list of search results
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
     */
    @POST
    @Path("organizations/{organizationUniqueId}")
    @SecuredService(permissions = {"product:view", "article:view"})
    public Response searchProductsAndArticlesForOrganization(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @DefaultValue(value = "25") @QueryParam("limit") int limit,
            @DefaultValue(value = "") @QueryParam("sortType") String sortType,
            @DefaultValue(value = "") @QueryParam("sortOrder") String sortOrder,
            @QueryParam("status") List<Product.Status> statuses,
            @QueryParam("type") List<Article.Type> articleTypes,
            @DefaultValue(value = "true") @QueryParam("includeProducts") boolean includeProducts,
            @DefaultValue(value = "true") @QueryParam("includeArticles") boolean includeArticles,
            @DefaultValue(value = "false") @QueryParam("includeOnlyTiso") boolean includeOnlyTiso,
            SearchAPI searchAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "searchProductsAndArticlesForOrganization( organizationUniqueId: {0}, offset: {1}, includeProducts: {2}, includeArticles: {3}, limit: {4})", new Object[]{organizationUniqueId, offset, includeProducts, includeArticles, limit});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if (!authorizeHandleOrganization(organizationUniqueId, userAPI)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        // if both includeProducts and includeArticles is false means both checkboxes are unchecked so we include both
        if (!includeProducts && !includeArticles) {
            includeProducts = includeArticles = true;
        }

        // search
        SearchDTO searchDTO = searchController.searchProductsAndArticlesByOrganization(organizationUniqueId,
                query,
                statuses,
                articleTypes,
                sortType,
                sortOrder,
                offset,
                limit,
                includeProducts,
                includeArticles,
                includeOnlyTiso,
                searchAPI);

        return Response.ok(searchDTO.getItems()).
                header("X-Total-Count", searchDTO.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }

    /**
     * Export products/articles for a specific organization.
     *
     * @param organizationUniqueId unique id of the organization
     * @param query                user supplied textual query string
     * @param statuses             list of product/article status to include
     * @param articleTypes         list of article types to include in response, only affects
     *                             articles.
     * @param includeProducts      whether to include products in search results, if this is set
     *                             to false and includeArticles is also set to false, we assume both should be included
     * @param includeArticles      whether to include articles in search results, if this is set
     *                             to false and includeProducts is also set to false, we assume both should be included
     * @param searchAPI            if additional search data is supplied, like category specific properties
     * @return an excel containing the list of search results
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
     */
    @POST
    @Path("organizations/{organizationUniqueId}/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"product:view", "article:view"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportProductsAndArticlesForOrganization(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query,
            @QueryParam("status") List<Product.Status> statuses,
            @QueryParam("type") List<Article.Type> articleTypes,
            @DefaultValue(value = "true") @QueryParam("includeProducts") boolean includeProducts,
            @DefaultValue(value = "true") @QueryParam("includeArticles") boolean includeArticles,
            SearchAPI searchAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "exportProductsAndArticlesForOrganization( organizationUniqueId: {0}, includeProducts: {1}, includeArticles: {2} )", new Object[]{organizationUniqueId, includeProducts, includeArticles});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if (!authorizeHandleOrganization(organizationUniqueId, userAPI)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        // if both includeProducts and includeArticles is false means both checkboxes are unchecked so we include both
        if (!includeProducts && !includeArticles) {
            includeProducts = includeArticles = true;
        }
        // we allow a maximum of 55000 hits in the excel, this is hard-coded since
        // this number should correspond to the setting index.max_result_window
        // in the file elasticsearch_index_source.json file
        int maximumNumberOfResults = 55000;
        SearchDTO searchDTO = searchController.searchProductsAndArticlesByOrganization(organizationUniqueId,
                query,
                statuses,
                articleTypes,
                "",
                "",
                0,
                maximumNumberOfResults,
                includeProducts,
                includeArticles,
                false,
                searchAPI);
        if (searchDTO == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                String filename = "Export_ProdukterOchArtiklar_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";
                List<SearchProductsAndArticlesAPI> searchProductsAndArticlesAPIs = (List<SearchProductsAndArticlesAPI>) searchDTO.getItems();
                byte[] exportBytes = resultExporterController.generateProductsAndArticlesList(searchProductsAndArticlesAPIs);
                return Response.ok(
                        exportBytes,
                        jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                        .header("content-disposition", "attachment; filename = " + filename)
                        .build();
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to export organization product/article search to file", ex);
                return Response.serverError().build();
            }
        }
    }

    @GET
    @Path("organizations/{organizationUniqueId}/articles")
    @SecuredService(permissions = {"article:view"})
    public Response searchArticles(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @DefaultValue(value = "25") @QueryParam("limit") int limit,
            @QueryParam("status") List<Product.Status> statuses,
            @QueryParam("type") List<Article.Type> articleTypes) {
        LOG.log(Level.FINEST, "searchArticles( organizationUniqueId: {0}, offset: {1}, limit: {2} )", new Object[]{organizationUniqueId, offset, limit});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if (!authorizeHandleOrganization(organizationUniqueId, userAPI)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        long countSearch = searchController.countArticlesByOrganization(organizationUniqueId, query, statuses, articleTypes);
        List<SearchProductsAndArticlesAPI> searchArticlesAPIs = searchController.searchArticlesByOrganization(organizationUniqueId, query, statuses, articleTypes, offset, limit);
        return Response.ok(searchArticlesAPIs).
                header("X-Total-Count", countSearch).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }

    /**
     * Search articles connected to a specific product
     *
     * @param organizationUniqueId unique id of the organization
     * @param productUniqueIdId    unique id of the product
     * @param query                user supplied textual query string
     * @param offset               start returning search results from this point, for pagination
     * @param statuses             list of article status to include
     * @param articleTypes         list of article types to include in response
     * @return a list of search results
     */
    @GET
    @Path("organizations/{organizationUniqueId}/products/{productUniqueIdId}")
    @SecuredService(permissions = {"product:view", "article:view"})
    public Response searchProductsAndArticlesForProduct(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("productUniqueIdId") long productUniqueIdId,
            @DefaultValue(value = "") @QueryParam("query") String query,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @DefaultValue(value = "25") @QueryParam("limit") int limit,
            @QueryParam("status") List<Product.Status> statuses,
            @QueryParam("type") List<Article.Type> articleTypes) {
        LOG.log(Level.FINEST, "searchProductsAndArticlesForProduct( organizationUniqueId: {0}, productUniqueIdId: {1}, offset: {2} )", new Object[]{organizationUniqueId, productUniqueIdId, offset, limit});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        Product product = productController.getProduct(organizationUniqueId, productUniqueIdId, userAPI);
        if (product == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            // if user is in own organization, show customer unique
            //Organization organization = organizationController.getOrganizationFromLoggedInUser(userAPI);
            boolean excludeCustomerUnique = UserController.getUserEngagementAPIByOrganizationId(organizationUniqueId, userAPI.getUserEngagements()) == null;

            /*
            boolean isServiceOwner = organization.getOrganizationType() == Organization.OrganizationType.SERVICE_OWNER;
            if (isServiceOwner)
                excludeCustomerUnique = false;

            LOG.log(Level.FINEST, "isServiceOwner: {0}, excludeCustomerUnique: {1}", new Object[]{isServiceOwner, excludeCustomerUnique});
            */

            long countSearch = searchController.countSearchArticlesByProduct(organizationUniqueId,
                    productUniqueIdId,
                    query,
                    statuses,
                    articleTypes,
                    excludeCustomerUnique);
            List<SearchProductsAndArticlesAPI> searchProductsAndArticlesAPIs = searchController.searchArticlesByProduct(organizationUniqueId,
                    productUniqueIdId,
                    query,
                    statuses,
                    articleTypes,
                    offset,
                    limit,
                    excludeCustomerUnique);
            return Response.ok(searchProductsAndArticlesAPIs).
                    header("X-Total-Count", countSearch).
                    header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                    build();
        }
    }

    /**
     * Search products/articles connected to a specific article
     *
     * @param organizationUniqueId unique id of the organization
     * @param articleUniqueId      unique id of the article
     * @param query                user supplied textual query string
     * @param offset               start returning search results from this point, for pagination
     * @param statuses             list of product/article status to include
     * @param articleTypes         list of article types to include in response, only affects
     *                             articles.
     * @return a list of search results
     */
    @GET
    @Path("organizations/{organizationUniqueId}/articles/{articleUniqueId}")
    @SecuredService(permissions = {"product:view", "article:view"})
    public Response searchProductsAndArticlesForArticle(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("articleUniqueId") long articleUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @DefaultValue("25") @QueryParam("limit") int limit,
            @QueryParam("status") List<Product.Status> statuses,
            @QueryParam("type") List<Article.Type> articleTypes) {
        LOG.log(Level.FINEST, "searchProductsAndArticlesForArticle( organizationUniqueId: {0}, articleUniqueId: {1}, offset: {2} )", new Object[]{organizationUniqueId, articleUniqueId, offset});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        Article article = articleController.getArticle(organizationUniqueId, articleUniqueId, userAPI);
        if (article == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            // if user is in own organization, show customer unique
            boolean excludeCustomerUnique = UserController.getUserEngagementAPIByOrganizationId(organizationUniqueId, userAPI.getUserEngagements()) == null;
            long countSearch = searchController.countSearchProductsAndArticlesForArticle(organizationUniqueId,
                    article,
                    query,
                    statuses,
                    articleTypes,
                    excludeCustomerUnique);
            List<SearchProductsAndArticlesAPI> searchProductsAndArticlesAPIs = searchController.searchProductsAndArticlesForArticle(organizationUniqueId,
                    article,
                    query,
                    statuses,
                    articleTypes,
                    offset,
                    limit,
                    excludeCustomerUnique);
            return Response.ok(searchProductsAndArticlesAPIs).
                    header("X-Total-Count", countSearch).
                    header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                    build();
        }
    }


    //PELLES HÄR NEDANFÖR

    /**
     * Export products/articles connected to a specific article
     *
     * @param organizationUniqueId unique id of the organization
     * @param query                user supplied textual query string
     * @param statuses             list of product/article status to include
     * @param articleTypes         list of article types to include in response, only affects
     *                             articles.
     * @param searchAPI            if additional search data is supplied, like category specific properties
     * @return an excel containing the list of search results
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
     */
    @POST
    @Path("organizations/{organizationUniqueId}/articles/{articleUniqueId}/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"product:view", "article:view"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportProductsAndArticlesForArticle(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("articleUniqueId") long articleUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query,
            @QueryParam("status") List<Product.Status> statuses,
            @QueryParam("type") List<Article.Type> articleTypes,
            SearchAPI searchAPI) {
        LOG.log(Level.FINEST, "exportProductsAndArticlesForArticle( organizationUniqueId: {0} )", new Object[]{organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        //if (!authorizeHandleOrganization(organizationUniqueId, userAPI)) {
        //    return Response.status(Response.Status.FORBIDDEN).build();
        //}

        Article article = articleController.getArticle(organizationUniqueId, articleUniqueId, userAPI);
        if (article == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            // if user is in own organization, show customer unique
            boolean excludeCustomerUnique = UserController.getUserEngagementAPIByOrganizationId(organizationUniqueId, userAPI.getUserEngagements()) == null;


            // we allow a maximum of 55000 hits in the excel, this is hard-coded since
            // this number should correspond to the setting index.max_result_window
            // in the file elasticsearch_index_source.json file
            int maximumNumberOfResults = 55000;

            List<SearchProductsAndArticlesAPI> searchProductsAndArticlesAPIs = searchController.searchProductsAndArticlesForArticle(organizationUniqueId,
                    article,
                    query,
                    statuses,
                    articleTypes,
                    0,
                    maximumNumberOfResults,
                    excludeCustomerUnique);

            if (searchProductsAndArticlesAPIs.size() == 0 || searchProductsAndArticlesAPIs == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            } else {
                try {
                    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                    String filename = "Export_ProdukterOchArtiklar_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";
                    //List<SearchProductsAndArticlesAPI> searchProductsAndArticlesAPIs = (List<SearchProductsAndArticlesAPI>) searchDTO.getItems();
                    byte[] exportBytes = resultExporterController.generateProductsAndArticlesList(searchProductsAndArticlesAPIs);
                    return Response.ok(
                            exportBytes,
                            jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                            .header("content-disposition", "attachment; filename = " + filename)
                            .build();
                } catch (IOException ex) {
                    LOG.log(Level.WARNING, "Failed to export organization product/article search to file", ex);
                    return Response.serverError().build();
                }
            }
        }
    }

    /**
     * Export articles connected to a specific product
     *
     * @param organizationUniqueId unique id of the organization
     * @param query                user supplied textual query string
     * @param statuses             list of product/article status to include
     * @param articleTypes         list of article types to include in response, only affects
     *                             articles.
     * @param searchAPI            if additional search data is supplied, like category specific properties
     * @return an excel containing the list of search results
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
     */
    @POST
    @Path("organizations/{organizationUniqueId}/products/{productUniqueId}/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"product:view", "article:view"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportArticlesForProduct(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("productUniqueId") long productUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query,
            @QueryParam("status") List<Product.Status> statuses,
            @QueryParam("type") List<Article.Type> articleTypes,
            SearchAPI searchAPI) {
        LOG.log(Level.FINEST, "exportArticlesForProduct( organizationUniqueId: {0} )", new Object[]{organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        //if (!authorizeHandleOrganization(organizationUniqueId, userAPI)) {
        //    return Response.status(Response.Status.FORBIDDEN).build();
        //}

        //Article article = articleController.getArticle(organizationUniqueId, articleUniqueId, userAPI);
        Product product = productController.getProduct(organizationUniqueId, productUniqueId, userAPI);
        if (product == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            // if user is in own organization, show customer unique
            boolean excludeCustomerUnique = UserController.getUserEngagementAPIByOrganizationId(organizationUniqueId, userAPI.getUserEngagements()) == null;


            // we allow a maximum of 55000 hits in the excel, this is hard-coded since
            // this number should correspond to the setting index.max_result_window
            // in the file elasticsearch_index_source.json file
            int maximumNumberOfResults = 55000;


            List<SearchProductsAndArticlesAPI> searchArticlesAPIs = searchController.searchArticlesByProduct(organizationUniqueId,
                    productUniqueId,
                    query,
                    statuses,
                    articleTypes,
                    0,
                    maximumNumberOfResults,
                    excludeCustomerUnique);

            if (searchArticlesAPIs.size() == 0 || searchArticlesAPIs == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            } else {
                try {
                    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                    String filename = "Export_Artiklar_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";
                    byte[] exportBytes = resultExporterController.generateProductsAndArticlesList(searchArticlesAPIs);
                    return Response.ok(
                            exportBytes,
                            jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                            .header("content-disposition", "attachment; filename = " + filename)
                            .build();
                } catch (IOException ex) {
                    LOG.log(Level.WARNING, "Failed to export product article search to file", ex);
                    return Response.serverError().build();
                }
            }
        }
    }

}
