package se.inera.hjalpmedelstjansten.business.user.controller;

import se.inera.hjalpmedelstjansten.business.DateUtils;
import se.inera.hjalpmedelstjansten.business.organization.controller.BusinessLevelMapper;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.PermissionAPI;
import se.inera.hjalpmedelstjansten.model.api.RoleAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.UserEngagementAPI;
import se.inera.hjalpmedelstjansten.model.entity.ElectronicAddress;
import se.inera.hjalpmedelstjansten.model.entity.UserAccount;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;
import se.inera.hjalpmedelstjansten.model.entity.UserPermission;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class UserMapper {

    public static List<UserAPI> map(List<UserEngagement> userEngagements, boolean includeEverything) {
        if( userEngagements == null ) {
            return null;
        }
        List<UserAPI> userAPIs = new ArrayList<>();
        for( UserEngagement userEngagement : userEngagements ) {
            userAPIs.add(map(userEngagement, includeEverything));
        }
        return userAPIs;
    }

    public static UserAPI map(UserEngagement userEngagement, boolean includeEverything) {
        if( userEngagement == null ) {
            return null;
        }
        UserAPI userAPI = new UserAPI();
        userAPI.setId(userEngagement.getUniqueId());
        
        UserAccount userAccount = userEngagement.getUserAccount();
        userAPI.setFirstName(userAccount.getFirstName());
        userAPI.setLastName(userAccount.getLastName());
        userAPI.setTitle(userAccount.getTitle());
        userAPI.setUsername(userAccount.getUsername());
        
        ElectronicAddress electronicAddress = userAccount.getElectronicAddress();        
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setId(electronicAddress.getUniqueId());
        electronicAddressAPI.setEmail(electronicAddress.getEmail());
        electronicAddressAPI.setMobile(electronicAddress.getMobile());
        electronicAddressAPI.setTelephone(electronicAddress.getTelephone());
        userAPI.setElectronicAddress(electronicAddressAPI);
        
        if( includeEverything ) {
            List<UserEngagementAPI> userEngagementAPIs = new ArrayList<>();
            UserEngagementAPI userEngagementAPI = new UserEngagementAPI();
            if( userEngagement.getBusinessLevels() != null && !userEngagement.getBusinessLevels().isEmpty() ) {
                userEngagementAPI.setBusinessLevels(BusinessLevelMapper.map(userEngagement.getBusinessLevels(), false));
            }
            userEngagementAPI.setId(userEngagement.getUniqueId());
            userEngagementAPI.setOrganizationId(userEngagement.getOrganization().getUniqueId());
            userEngagementAPI.setValidFrom(userEngagement.getValidFrom().getTime());
            if( userEngagement.getValidTo() != null ) {
                userEngagementAPI.setValidTo(userEngagement.getValidTo().getTime());
            }
            if( userEngagement.getRoles() != null ) {
                List<RoleAPI> roleAPIs = new ArrayList<>();
                for( UserRole userRole : userEngagement.getRoles() ) {
                    RoleAPI roleAPI = new RoleAPI();
                    roleAPI.setId(userRole.getUniqueId());
                    roleAPI.setDescription(userRole.getDescription());
                    roleAPI.setName(userRole.getName().toString());
                    if( userRole.getPermissions() != null ) {
                        List<PermissionAPI> permissionAPIs = new ArrayList<>();
                        for( UserPermission userPermission : userRole.getPermissions() ) {
                            PermissionAPI permissionAPI = new PermissionAPI();
                            permissionAPI.setName(userPermission.getName());
                            permissionAPIs.add(permissionAPI);
                        }
                        roleAPI.setPermissions(permissionAPIs); 
                    }
                    roleAPIs.add(roleAPI);
                }
                userEngagementAPI.setRoles(roleAPIs);
            }
            userEngagementAPIs.add(userEngagementAPI);
            userAPI.setUserEngagementAPIs(userEngagementAPIs);
        }
        userAPI.setActive(calculateActive(userEngagement.getValidFrom(), userEngagement.getValidTo()));
        return userAPI;
    }

    public static UserEngagement map(UserAPI userAPI) {
        if( userAPI == null ) {
            return null;
        }
        UserEngagement userEngagement = new UserEngagement();
        
        // Electronic Address
        ElectronicAddress electronicAddress = new ElectronicAddress();
        ElectronicAddressAPI electronicAddressAPI = userAPI.getElectronicAddress();
        electronicAddress.setEmail(electronicAddressAPI.getEmail());
        electronicAddress.setMobile(electronicAddressAPI.getMobile());
        electronicAddress.setTelephone(electronicAddressAPI.getTelephone());

        // User Account
        UserAccount userAccount = new UserAccount();
        userAccount.setFirstName(userAPI.getFirstName());
        userAccount.setLastName(userAPI.getLastName());
        userAccount.setTitle(userAPI.getTitle());
        userAccount.setUsername(userAPI.getUsername());
        userAccount.setElectronicAddress(electronicAddress);
        userEngagement.setUserAccount(userAccount);
        
        UserEngagementAPI userEngagementAPI = userAPI.getUserEngagements().get(0);
        userEngagement.setValidFrom(new Date(userEngagementAPI.getValidFrom()));
        if( userEngagementAPI.getValidTo() != null ) {
            userEngagement.setValidTo(DateUtils.endOfDay(new Date(userAPI.getUserEngagements().get(0).getValidTo())));
        }
        return userEngagement;
    }
    
    public static boolean calculateActive(Date validFrom, Date validTo) {
        Date now = new Date();
        if( now.before(validFrom) ) {
            return false;
        } else if( validTo == null ) {
            return true;
        } else {
            return now.before(validTo);
        }
    }

    public static List<UserEngagement> mapSearchToUserEngagements(List<Object[]> objectArrayList) {
        List<UserEngagement> userEngagements = new ArrayList<>();
        if( objectArrayList != null && !objectArrayList.isEmpty() ) {
            for( Object[] objectArray : objectArrayList ) {
                UserEngagement userEngagement = new UserEngagement();
                userEngagement.setUniqueId((Long) objectArray[0]);
                userEngagement.setValidFrom((Date) objectArray[1]);
                userEngagement.setValidTo((Date) objectArray[2]);
                UserAccount userAccount = new UserAccount();
                userAccount.setFirstName((String) objectArray[3]);
                userAccount.setLastName((String) objectArray[4]);
                userAccount.setTitle((String) objectArray[5]);
                userAccount.setUsername((String) objectArray[6]);
                ElectronicAddress electronicAddress = new ElectronicAddress();
                electronicAddress.setUniqueId((Long) objectArray[7]);
                electronicAddress.setEmail((String) objectArray[8]);
                electronicAddress.setMobile((String) objectArray[9]);
                electronicAddress.setTelephone((String) objectArray[10]);
                userAccount.setElectronicAddress(electronicAddress);
                userEngagement.setUserAccount(userAccount);    
                userEngagements.add(userEngagement);
            }
        }
        return userEngagements;
    }
    
}
