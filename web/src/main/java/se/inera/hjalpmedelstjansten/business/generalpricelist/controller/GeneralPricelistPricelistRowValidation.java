package se.inera.hjalpmedelstjansten.business.generalpricelist.controller;

import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelistRow;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

/**
 * Validation methods for general pricelist pricelist row
 *
 */
@Stateless
public class GeneralPricelistPricelistRowValidation {

    @Inject
    HjmtLogger LOG;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    GeneralPricelistPricelistController pricelistController;

    public Set<ErrorMessageAPI> tryForCreate(GeneralPricelistPricelistRowAPI pricelistRowAPI, GeneralPricelistPricelist pricelist, GeneralPricelistPricelist currentPricelist) {
        LOG.log( Level.FINEST, "tryForCreate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        GeneralPricelistPricelist.Status pricelistStatus = GeneralPricelistPricelistMapper.getPricelistStatus(pricelist, currentPricelist);
        if( pricelistStatus == GeneralPricelistPricelist.Status.PAST ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("status", validationMessageService.getMessage("pricelist.status.past")));
        }

        validatePricelistRowAPI(pricelistRowAPI, errorMessageAPIs);

        // validation makes sure status is set and valid
        GeneralPricelistPricelistRow.Status status = GeneralPricelistPricelistRow.Status.valueOf(pricelistRowAPI.getStatus());
        if( status != GeneralPricelistPricelistRow.Status.ACTIVE ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("status", validationMessageService.getMessage("pricelistrow.status.invalidCreate")));
        }

        return errorMessageAPIs;
    }

    public void validateForCreate(GeneralPricelistPricelistRowAPI pricelistRowAPI, GeneralPricelistPricelist pricelist, GeneralPricelistPricelist currentPricelist) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForCreate(...)" );

        Set<ErrorMessageAPI> errorMessageAPIs = tryForCreate(pricelistRowAPI, pricelist, currentPricelist);

        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }

    }

    public Set<ErrorMessageAPI> tryForUpdate(GeneralPricelistPricelistRowAPI pricelistRowAPI, GeneralPricelistPricelistRow pricelistRow, GeneralPricelistPricelist currentPricelist) {
        LOG.log( Level.FINEST, "tryForUpdate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();

        GeneralPricelistPricelist.Status pricelistStatus = GeneralPricelistPricelistMapper.getPricelistStatus(pricelistRow.getPricelist(), currentPricelist);
        if( pricelistStatus == GeneralPricelistPricelist.Status.PAST ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("status", validationMessageService.getMessage("pricelist.status.past")));
        }

        validatePricelistRowAPI(pricelistRowAPI, errorMessageAPIs);

        // on update, deliverytime may not be null
        if( pricelistRowAPI.getDeliveryTime() == null ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("deliveryTime", validationMessageService.getMessage("pricelistrow.deliveryTime.notNull")));
        }

        return errorMessageAPIs;
    }

    public void validateForUpdate(GeneralPricelistPricelistRowAPI pricelistRowAPI, GeneralPricelistPricelistRow pricelistRow, GeneralPricelistPricelist currentPricelist) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForUpdate(...)" );

        Set<ErrorMessageAPI> errorMessageAPIs = tryForUpdate(pricelistRowAPI, pricelistRow, currentPricelist);

        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }

    }

    private void validatePricelistRowAPI(GeneralPricelistPricelistRowAPI pricelistRowAPI, Set<ErrorMessageAPI> errorMessageAPIs) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<GeneralPricelistPricelistRowAPI>> constraintViolations = validator.validate(pricelistRowAPI, Default.class);
        if( constraintViolations != null && !constraintViolations.isEmpty() ) {
            for( ConstraintViolation constraintViolation : constraintViolations ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage()));
            }
        }
    }

}
