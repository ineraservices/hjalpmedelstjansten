package se.inera.hjalpmedelstjansten.business;

import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;

import jakarta.ejb.ApplicationException;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Custom service exception which causes a rollback.
 *
 * @see se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenExceptionMapper
 */
@ApplicationException(rollback = true)
public class HjalpmedelstjanstenException extends Exception {

    public enum Type {
        FORBIDDEN
    }

    private final Set<ErrorMessageAPI> validationMessages = new LinkedHashSet<>();
    private Type type = null;

    public HjalpmedelstjanstenException(String message) {
        super(message);
    }

    public HjalpmedelstjanstenException(String message, Throwable cause) {
        super(message, cause);
    }

    public HjalpmedelstjanstenException(String message, Type type) {
        super(message);
        this.type = type;
    }

    public Set<ErrorMessageAPI> getValidationMessages() {
        return validationMessages;
    }

    public Type getType() {
        return type;
    }

    public void addValidationMessage(String field, String message) {
        ErrorMessageAPI errorMessageAPI = new ErrorMessageAPI();
        errorMessageAPI.setField(field);
        errorMessageAPI.setMessage(message);
        validationMessages.add(errorMessageAPI);
    }

    public void addValidationMessage(String message) {
        addValidationMessage(null, message);
    }

}
