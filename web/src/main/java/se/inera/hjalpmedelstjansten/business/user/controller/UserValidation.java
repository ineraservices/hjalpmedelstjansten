package se.inera.hjalpmedelstjansten.business.user.controller;

import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.Set;
import java.util.logging.Level;


@Stateless
public class UserValidation {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private ValidationMessageService validationMessageService;

    @Inject
    private UserController userController;

    public void validate( UserAPI userAPI, boolean update, UserEngagement currentUser ) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validate(...)" );
        HjalpmedelstjanstenValidationException exception = null;
        // email is mandatory when it comes to users, but not organization, that's why the
        // validation is here and not in the API-class
        if( userAPI.getElectronicAddress().getEmail() == null || userAPI.getElectronicAddress().getEmail().isEmpty() ) {
            exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessage("electronicAddress.email", validationMessageService.getMessage("user.email.notNull"));
        }
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<UserAPI>> constraintViolations = validator.validate(userAPI, Default.class);
        if( constraintViolations != null && !constraintViolations.isEmpty() ) {
            if( exception == null ) {
                exception = new HjalpmedelstjanstenValidationException("Validation failed");
            }
            for( ConstraintViolation constraintViolation : constraintViolations ) {
                exception.addValidationMessage(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage());
            }
        }
        if( exception != null ) {
            throw exception;
        }
        if( update ) {
            validateForUpdate(userAPI, currentUser);
        } else {
            validateForCreate(userAPI);
        }
    }


    private void validateForCreate(UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForCreate(...)" );
        HjalpmedelstjanstenValidationException exception = null;
        // make sure username and email is unique
        UserEngagement userEngagement = userController.findByEmail(userAPI.getElectronicAddress().getEmail());
        if( userEngagement != null ) {
            exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessage("electronicAddress.email", validationMessageService.getMessage("user.email.notUnique"));
        }
        userEngagement = userController.findByUsername(userAPI.getUsername());
        if( userEngagement != null) {
            if( exception == null ) {
                exception = new HjalpmedelstjanstenValidationException("Validation failed");
            }
            exception.addValidationMessage("username", validationMessageService.getMessage("user.username.notUnique"));
        }

        if( exception != null ) {
            throw exception;
        }
    }

    private void validateForUpdate(UserAPI userAPI, UserEngagement currentUser) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForUpdate(...)" );
        UserEngagement userEngagement = userController.findByEmail(userAPI.getElectronicAddress().getEmail());
        // make sure username and email is unique
        if( userEngagement != null ) {
            if( !userEngagement.getUserAccount().getUniqueId().equals(currentUser.getUserAccount().getUniqueId()) ) {
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
                exception.addValidationMessage("electronicAddress.email", validationMessageService.getMessage("user.email.notUnique"));
                throw exception;
            }
        }
        userEngagement = userController.findByUsername(userAPI.getUsername());
        if( userEngagement != null ) {
            if( !userEngagement.getUserAccount().getUniqueId().equals(currentUser.getUserAccount().getUniqueId()) ) {
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
                exception.addValidationMessage("username", validationMessageService.getMessage("user.username.notUnique"));
                throw exception;
            }
        }
    }

    public String validateForRemoval(UserAPI userAPI, UserEngagement userEngagement, UserEngagement adminEngagement){
        if(userEngagement.getOrganization().getOrganizationType() == Organization.OrganizationType.CUSTOMER){
            String test = "testy";
            return test;
        }
        if(userEngagement.getOrganization().getOrganizationType() == Organization.OrganizationType.SUPPLIER){
            String test = "testy1";
            return test;
        }
        if(userEngagement.getOrganization().getOrganizationType() == Organization.OrganizationType.SERVICE_OWNER){
            String test = "testy2";
            return test;
        }
        return "testyu123";
    }

}
