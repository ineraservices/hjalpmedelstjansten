package se.inera.hjalpmedelstjansten.business.user.dao;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.io.Serializable;


public abstract class StandardAbstractDAO<T extends Serializable> implements StandardDAO<T> {

    @PersistenceContext(unitName = "HjmtjPU")
    EntityManager entityManager;

    Class<T> clazz;

}
