package se.inera.hjalpmedelstjansten.business.export.view;

import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.ResultExporterController;
import se.inera.hjalpmedelstjansten.business.export.controller.ExportSettingsController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.ExportSettingsAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.UserEngagementAPI;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;


@Stateless
@Path("organizations/{organizationUniqueId}/exportsettings")
@Interceptors({ PerformanceLogInterceptor.class })
public class ExportSettingsService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private ExportSettingsController exportSettingsController;

    @Inject
    ResultExporterController resultExporterController;

    @EJB
    AuthHandler authHandler;

    @GET
    @SecuredService(permissions = {"exportsettings:view_own"})
    public Response searchExportSettings(@PathParam("organizationUniqueId") long organizationUniqueId) {
        LOG.log(Level.FINEST, "searchExportSettings( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        UserEngagementAPI userEngagementAPI = UserController.getUserEngagementAPIByOrganizationId( organizationUniqueId, userAPI.getUserEngagements());
        List<ExportSettingsAPI> exportSettingsAPIs = exportSettingsController.findByOrganizationAndUser(organizationUniqueId, userEngagementAPI);
        if( exportSettingsAPIs == null ) {
            return Response.noContent().build();
        } else {
            return Response.ok(exportSettingsAPIs).build();
        }
    }

    @GET
    @Path("{exportSettingsUniqueId}")
    @SecuredService(permissions = {"exportsettings:view_own"})
    public Response getExportSettings(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("exportSettingsUniqueId") long exportSettingsUniqueId) {
        LOG.log(Level.FINEST, "getExportSettings( organizationUniqueId: {0}, exportSettingsUniqueId: {1} )", new Object[] {organizationUniqueId, exportSettingsUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        ExportSettingsAPI exportSettingsAPI = exportSettingsController.findAPI(organizationUniqueId, exportSettingsUniqueId);
        if( exportSettingsAPI == null ) {
            return Response.noContent().build();
        } else {
            return Response.ok(exportSettingsAPI).build();
        }
    }

    /**
     * Export GPs
     *
     * @param httpServletRequest
     * @param query
     * @return an excel containing the list of search results
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("{exportSettingsUniqueId}/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"exportsettings:view_own"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportGPs(
            @Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("exportSettingsUniqueId") long exportSettingsUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "exportGPs...");

        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        UserEngagementAPI userEngagementAPI = UserController.getUserEngagementAPIByOrganizationId( organizationUniqueId, userAPI.getUserEngagements());

        List<ExportSettingsAPI> exportSettingsAPIs = exportSettingsController.findByOrganizationAndUser(organizationUniqueId, userEngagementAPI);

        if( exportSettingsAPIs == null ) {
            return Response.noContent().build();
        } else {
            List<GeneralPricelistAPI> gpAPIs = exportSettingsController.findAPI(organizationUniqueId, exportSettingsUniqueId).getGeneralPricelists();
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                String filename = "Export_GPs_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";

                byte[] exportBytes = resultExporterController.generateGPsList(gpAPIs);

                return Response.ok(
                        exportBytes,
                        jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                        .header("content-disposition", "attachment; filename = " + filename)
                        .build();
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to export GPs for exportfile to file", ex);
                return Response.serverError().build();
            }
        }
    }


    /**
     * Add a list of general pricelists to the export settings
     *
     * @param organizationUniqueId
     * @param exportSettingsUniqueId
     * @param generalPricelistAPIs user supplied list of general pricelists
     * @return the updated <code>ExportSettingsAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("{exportSettingsUniqueId}/generalpricelists")
    @SecuredService(permissions = {"exportsettings:update_own"})
    public Response addGeneralPricelistsToExportSetting(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("exportSettingsUniqueId") long exportSettingsUniqueId,
            List<GeneralPricelistAPI> generalPricelistAPIs) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "addGeneralPricelistsToExportSetting( organizationUniqueId: {0}, exportSettingsUniqueId: {1} )", new Object[] {organizationUniqueId, exportSettingsUniqueId});
        ExportSettingsAPI exportSettingsAPI = exportSettingsController.addGeneralPricelistsToExportSetting(organizationUniqueId, exportSettingsUniqueId, generalPricelistAPIs);
        if( exportSettingsAPI == null ) {
            return Response.noContent().build();
        } else {
            return Response.ok(exportSettingsAPI).build();
        }
    }

    /**
     * Delete a list of general pricelists from the export settings
     *
     * @param organizationUniqueId
     * @param exportSettingsUniqueId
     * @param generalPricelistAPIs user supplied list of general pricelists
     * @return the updated <code>ExportSettingsAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @DELETE
    @Path("{exportSettingsUniqueId}/generalpricelists")
    @SecuredService(permissions = {"exportsettings:update_own"})
    public Response deleteGeneralPricelistsToExportSetting(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("exportSettingsUniqueId") long exportSettingsUniqueId,
            List<GeneralPricelistAPI> generalPricelistAPIs) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteGeneralPricelistsToExportSetting( organizationUniqueId: {0}, exportSettingsUniqueId: {1} )", new Object[] {organizationUniqueId, exportSettingsUniqueId});
        ExportSettingsAPI exportSettingsAPI = exportSettingsController.deleteGeneralPricelistsToExportSetting(organizationUniqueId, exportSettingsUniqueId, generalPricelistAPIs);
        if( exportSettingsAPI == null ) {
            return Response.noContent().build();
        } else {
            return Response.ok(exportSettingsAPI).build();
        }
    }

}
