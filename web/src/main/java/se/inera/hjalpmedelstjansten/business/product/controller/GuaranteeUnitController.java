package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

@Stateless
public class GuaranteeUnitController {

    @Inject
    private HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;

    public List<CVGuaranteeUnitAPI> getAllGuaranteeUnitAPIs() {
        LOG.log(Level.FINEST, "getAllGuaranteeUnitAPIs()");
        return GuaranteeUnitMapper.map(getAllGuaranteeUnits());
    }

    public CVGuaranteeUnit getGuaranteeUnit(long uniqueId) {
        LOG.log(Level.FINEST, "getGuaranteeUnit( uniqueId: {0} )");
        return em.find(CVGuaranteeUnit.class, uniqueId);
    }

    public List<CVGuaranteeUnit> getAllGuaranteeUnits() {
        LOG.log(Level.FINEST, "getAllGuaranteeUnits()");
        return em.createNamedQuery(CVGuaranteeUnit.FIND_ALL)
                .getResultList();
    }

    public CVGuaranteeUnit findByName(String name) {
        LOG.log(Level.FINEST, "findByName( name: {0} )", new Object[] {name});
        List<CVGuaranteeUnit> guaranteeUnits = em.createNamedQuery(CVGuaranteeUnit.FIND_BY_NAME).
                setParameter("name", name).
                getResultList();
        // there is a maximum of one
        if( guaranteeUnits != null && !guaranteeUnits.isEmpty() ) {
            return guaranteeUnits.get(0);
        }
        return null;
    }

    public Map<String, CVGuaranteeUnit> getGuaranteeUnitNamesMap() {
        Map<String, CVGuaranteeUnit> guaranteeUnitNamesMap = new HashMap<>();
        List<CVGuaranteeUnit> allGuaranteeUnits = getAllGuaranteeUnits();
        allGuaranteeUnits.forEach((guaranteeUnit) -> {
            guaranteeUnitNamesMap.put(guaranteeUnit.getName(), guaranteeUnit);
        });
        return guaranteeUnitNamesMap;
    }

}
