package se.inera.hjalpmedelstjansten.business.dynamictext.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.dynamictext.controller.DynamicTextController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.model.api.DynamicTextAPI;
import se.inera.hjalpmedelstjansten.model.entity.DynamicText;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import java.util.logging.Level;

/**
 * REST API for dynamic text.
 *
 */
@Stateless
@Path("dynamicText")
@Interceptors({ PerformanceLogInterceptor.class })
public class DynamicTextService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @Inject
    DynamicTextController dynamicTextController;


    @EJB
    AuthHandler authHandler;

    /**
     * Search dynamicText by uniqueId
     *
     * @param uniqueId
     * @return a string of <code>DynamicTextAPI</code>
     */

    @GET
    @Path("{uniqueId}")
    //@SecuredService(permissions = {"assortment:view"})
    public Response getDynamicText(@Context HttpServletRequest httpServletRequest,
            @PathParam("uniqueId") long uniqueId ) {
        LOG.log(Level.FINEST, "getDynamicText( uniqueId: {0} )", new Object[] {uniqueId});

        DynamicText dynamicText = dynamicTextController.getDynamicText(uniqueId);
        LOG.log(Level.FINEST, "dynamicText: {0}", new Object[] {dynamicText});

        if( dynamicText == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(dynamicText).build();
        }

    }


    /**
     * Update dynamicText
     *
     * @param uniqueId the id of the dynamictext to update
     * @param dynamicTextAPI
     */


    @PUT
    @Path("{uniqueId}")
    //@SecuredService(permissions = {"dynamicText:update"})
    public Response updateDynamicText(@Context HttpServletRequest httpServletRequest,
                                      @PathParam("uniqueId") long uniqueId, DynamicTextAPI dynamicTextAPI) throws HjalpmedelstjanstenValidationException
    {
        LOG.log(Level.FINEST, "updateDynamicText( uniqueId: {0}, dynamicText: {1} )", new Object[] {uniqueId, dynamicTextAPI});

        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if( !isSuperAdmin) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }


        dynamicTextAPI = dynamicTextController.updateDynamicTextX(uniqueId, dynamicTextAPI);


        if( dynamicTextAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(dynamicTextAPI).build();
        }

    }

}
