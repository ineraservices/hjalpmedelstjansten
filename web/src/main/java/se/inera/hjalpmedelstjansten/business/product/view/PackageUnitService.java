package se.inera.hjalpmedelstjansten.business.product.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.product.controller.PackageUnitController;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPackageUnitAPI;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import java.util.List;
import java.util.logging.Level;

@Stateless
@Path("packageunits")
@Interceptors({ PerformanceLogInterceptor.class })
public class PackageUnitService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    private PackageUnitController packageUnitController;

    /**
     * Return all order units in the database.
     *
     * @return a list of <code>OrderUnitAPI</code>
     */
    @GET
    @SecuredService(permissions = {"packageunit:view"})
    public List<CVPackageUnitAPI> getAllPackageUnits() {
        LOG.log(Level.FINEST, "getAllPackageUnits()");
        return packageUnitController.getAllUnits();
    }

}
