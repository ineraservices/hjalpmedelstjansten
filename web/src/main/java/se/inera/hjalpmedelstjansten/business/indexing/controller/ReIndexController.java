package se.inera.hjalpmedelstjansten.business.indexing.controller;

import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.entity.Organization;

import jakarta.ejb.Asynchronous;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * Drop/create elastic index and start reindexing of products and articles
 * for all supplier organizations.
 *
 */
@Stateless
public class ReIndexController {

    @Inject
    HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    ReIndexSupplierController reIndexSupplierController;

    @Inject
    ElasticSearchController elasticSearchController;

    @Asynchronous
    @TransactionTimeout(value = 8, unit = TimeUnit.HOURS)
    public void reIndexAllProductsAndArticles() {
        LOG.log( Level.FINEST, "reIndexAllProductsAndArticles");
        long start = System.currentTimeMillis();
        if( elasticSearchController.indexExists() ) {
            LOG.log( Level.INFO, "Delete elastic index");
            elasticSearchController.deleteIndex();
        }
        if( !elasticSearchController.createIndex()) {
            LOG.log( Level.WARNING, "Failed to create elastic search index. Search will not work properly" );
        } else {
            LOG.log( Level.INFO, "Successfully created elastic search index." );
        }

        List<Long> supplierIds = getAllSupplierIds();
        int i = 0;
        if( supplierIds != null ) {
            for( Long supplierId : supplierIds ) {
                String supplierName = getSupplierName(supplierId);
                LOG.log(Level.INFO, "Indexing supplier {0}, no {1} of {2}", new Object[]{supplierName, ++i, supplierIds.size()});
                reIndexSupplierController.reIndexSupplier(supplierId, supplierName);
            }
        }
        long total = System.currentTimeMillis() - start;
        LOG.log(Level.INFO, "Reindex completed" );
        LOG.logPerformance(this.getClass().getName(), "reIndexAllProductsAndArticles()", total, null);
    }


    private List<Long> getAllSupplierIds() {
        LOG.log(Level.FINEST, "getAllSupplierIds()");
        return em.createQuery("SELECT o.uniqueId FROM Organization o WHERE o.organizationType = :type ORDER BY o.organizationName").
                setParameter("type", Organization.OrganizationType.SUPPLIER).
                getResultList();
    }

    private String getSupplierName(long organizationUniqueId) {
        LOG.log(Level.FINEST, "getSupplierName()");
        return (String) em.createQuery("SELECT o.organizationName FROM Organization o WHERE o.uniqueId = :organizationUniqueId").
                setParameter("organizationUniqueId", organizationUniqueId).
                getSingleResult();
    }


}
