package se.inera.hjalpmedelstjansten.business.assortment.controller;

import se.inera.hjalpmedelstjansten.business.DateUtils;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.AssortmentAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.entity.Assortment;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

/**
 * Validation methods for assortments
 *
 */
@Stateless
public class AssortmentValidation {

    @Inject
    HjmtLogger LOG;

    @Inject
    ValidationMessageService validationMessageService;

    public void validateForCreate(AssortmentAPI assortmentAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForCreate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForCreate(assortmentAPI);
        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }
    }

    public void validateForUpdate(AssortmentAPI assortmentAPI, Assortment assortment, boolean isSuperAdmin) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForUpdate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForUpdate(assortmentAPI, assortment, isSuperAdmin);
        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }
    }

    /**
     * Generates a Set of <code>ErrorMessageAPI</code> in case the given assortmentAPI
     * does not validate entirely for create
     *
     * @param assortmentAPI
     * @return
     */
    public Set<ErrorMessageAPI> tryForCreate(AssortmentAPI assortmentAPI) {
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        validateAssortmentAPI(assortmentAPI, errorMessageAPIs);
        if( !errorMessageAPIs.isEmpty() ) {
            return errorMessageAPIs;
        }
        validateFromAndToDates(assortmentAPI.getValidFrom(), assortmentAPI.getValidTo(), errorMessageAPIs);
        return errorMessageAPIs;
    }

    /**
     * Generates a Set of <code>ErrorMessageAPI</code> in case the given assortmentAPI
     * does not validate entirely for update
     *
     * @param assortmentAPI
     * @param assortment
     * @param isSuperAdmin
     * @return
     */
    public Set<ErrorMessageAPI> tryForUpdate(AssortmentAPI assortmentAPI, Assortment assortment, boolean isSuperAdmin) {
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        validateAssortmentAPI(assortmentAPI, errorMessageAPIs);
        if( isSuperAdmin && assortmentAPI.getCounty() == null ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("county", validationMessageService.getMessage("assortment.county.notNullWhenSuperadmin")));
        }
        if( !errorMessageAPIs.isEmpty() ) {
            return errorMessageAPIs;
        }
        return errorMessageAPIs;
    }

    private void validateAssortmentAPI(AssortmentAPI assortmentAPI, Set<ErrorMessageAPI> errorMessageAPIs) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<AssortmentAPI>> constraintViolations = validator.validate(assortmentAPI, Default.class);
        if( constraintViolations != null && !constraintViolations.isEmpty() ) {
            for( ConstraintViolation constraintViolation : constraintViolations ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage()));
            }
        }
    }

    private void validateFromAndToDates(long fromDate, Long toDate, Set<ErrorMessageAPI> errorMessageAPIs) {
        if( toDate != null ) {
            if( toDate <= DateUtils.endOfDay(fromDate).getTime() ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("validTo", validationMessageService.getMessage("assortment.validTo.notAfterFrom")));
            }
        }
    }

}
