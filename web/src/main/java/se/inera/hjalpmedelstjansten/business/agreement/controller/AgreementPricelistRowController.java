package se.inera.hjalpmedelstjansten.business.agreement.controller;

import lombok.NoArgsConstructor;
import se.inera.hjalpmedelstjansten.business.BaseController;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.assortment.controller.AssortmentController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.*;
import se.inera.hjalpmedelstjansten.business.property.view.PropertyLoader;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.EmailController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.*;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.*;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

import jakarta.ejb.Stateless;
import jakarta.enterprise.event.Event;
import jakarta.inject.Inject;
import jakarta.mail.MessagingException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;
import java.util.logging.Level;

import java.math.BigInteger;
import java.nio.ByteBuffer;

import static java.util.stream.Collectors.joining;

/**
 * Class for handling business logic of Agreements. This includes talking to the
 * database.
 *
 */
@Stateless
@NoArgsConstructor
public class AgreementPricelistRowController extends BaseController {

    @Inject
    HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    AgreementPricelistRowValidation pricelistRowValidation;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    AgreementPricelistController pricelistController;

    @Inject
    ArticleController articleController;

    @Inject
    OrganizationController organizationController;

    @Inject
    GuaranteeUnitController guaranteeUnitController;

    @Inject
    CVPreventiveMaintenanceController cVPreventiveMaintenanceController;

    @Inject
    AgreementController agreementController;

    @Inject
    AgreementPricelistController agreementPricelistController;

    @Inject
    UserController userController;

    @Inject
    CategoryController categoryController;
    @Inject
    AssortmentController assortmentController;
    @Inject
    Event<InternalAuditEvent> internalAuditEvent;

    @Inject
    EmailController emailController;

    private String rowsSentForApprovalMailBody;
    private String rowsSentForApprovalMailSubject;
    private String rowsSentApprovedMailSubject;
    private String rowsSentApprovedMailBody;
    private String rowsSentApprovedMailSubject_single;
    private String rowsSentApprovedMailBody_single;
    private String rowsSentApprovedInactivateMailSubject;
    private String rowsSentApprovedInactivateMailSubject_single;
    private String rowsSentApprovedInactivateMailBody;
    private String rowsSentApprovedInactivateMailBody_single;
    private String rowsSentDeclinedInactivateMailSubject;
    private String rowsSentDeclinedInactivateMailSubject_single;
    private String rowsSentDeclinedInactivateMailBody;
    private String rowsSentDeclinedInactivateMailBody_single;
    private String rowsSentDeclinedMailSubject;
    private String rowsSentDeclinedMailSubject_single;
    private String rowsSentDeclinedMailBody;
    private String rowsSentDeclinedMailBody_single;
    private String pricelistToHandleMailSubject;
    private String pricelistToHandleMailBody;

    @Inject
    public AgreementPricelistRowController(PropertyLoader propertyLoader) {
        rowsSentForApprovalMailBody = propertyLoader.getMessage("rowsSentForApprovalMailBody");
        rowsSentForApprovalMailSubject = propertyLoader.getMessage("rowsSentForApprovalMailSubject");
        String pricelistToHandleMailSubjectSingle = propertyLoader.getMessage("pricelistToHandleMailSubjectSingle");
        String pricelistToHandleMailBodySingle = propertyLoader.getMessage("pricelistToHandleMailBodySingle");
        String pricelistToHandleMailSubjectMultiple = propertyLoader.getMessage("pricelistToHandleMailSubjectMultiple");
        String pricelistToHandleMailBodyMultiple = propertyLoader.getMessage("pricelistToHandleMailBodyMultiple");
        rowsSentApprovedMailSubject = propertyLoader.getMessage("rowsSentApprovedMailSubject");
        rowsSentApprovedMailBody = propertyLoader.getMessage("rowsSentApprovedMailBody");
        rowsSentApprovedMailSubject_single = propertyLoader.getMessage("rowsSentApprovedMailSubject_single");
        rowsSentApprovedMailBody_single = propertyLoader.getMessage("rowsSentApprovedMailBody_single");
        rowsSentApprovedInactivateMailSubject = propertyLoader.getMessage("rowsSentApprovedInactivateMailSubject");
        rowsSentApprovedInactivateMailSubject_single = propertyLoader.getMessage("rowsSentApprovedInactivateMailSubject_single");
        rowsSentApprovedInactivateMailBody = propertyLoader.getMessage("rowsSentApprovedInactivateMailBody");
        rowsSentApprovedInactivateMailBody_single = propertyLoader.getMessage("rowsSentApprovedInactivateMailBody_single");
        rowsSentDeclinedInactivateMailSubject = propertyLoader.getMessage("rowsSentDeclinedInactivateMailSubject");
        rowsSentDeclinedInactivateMailSubject_single = propertyLoader.getMessage("rowsSentDeclinedInactivateMailSubject_single");
        rowsSentDeclinedInactivateMailBody = propertyLoader.getMessage("rowsSentDeclinedInactivateMailBody");
        rowsSentDeclinedInactivateMailBody_single = propertyLoader.getMessage("rowsSentDeclinedInactivateMailBody_single");
        rowsSentDeclinedMailSubject = propertyLoader.getMessage("rowsSentDeclinedMailSubject");
        rowsSentDeclinedMailSubject_single = propertyLoader.getMessage("rowsSentDeclinedMailSubject_single");
        rowsSentDeclinedMailBody = propertyLoader.getMessage("rowsSentDeclinedMailBody");
        rowsSentDeclinedMailBody_single = propertyLoader.getMessage("rowsSentDeclinedMailBody_single");
        pricelistToHandleMailSubject = propertyLoader.getMessage("pricelistToHandleMailSubject");
        pricelistToHandleMailBody = propertyLoader.getMessage("pricelistToHandleMailBody");
    }



    /**
     * Search pricelist rows for the user supplied query. Search is paginated so only
     * results valid for given offset and limit is returned.
     *
     * @param queryString user supplied query
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param pricelistUniqueId unique id of the pricelist
     * @param assortmentId unique id of the assortment (if any)
     * @param userAPI user information, needed for business levels validation
     * @param offset start returning from this search result
     * @param limit maximum number of search results to return
     * @param statuses list of statuses to include in search, if null or empty,
     * all statuses are included
     * @param articleTypes list of article types to include in search, if null or empty,
     * all article types are included
     * @param showRowsWithPrices include rows where price is not null, if neither this
     * not showRowsWithoutPrices is set, then all rows are included
     * @param showRowsWithoutPrices include rows where price is null, if neither this
     * not showRowsWithPrices is set, then all rows are included
     * @param categoryUniqueId unique id of the category
     * @return a list of <code>AgreementPricelistRowAPI</code> matching the query parameters
     */
    public SearchDTO searchPricelistRows(String queryString,
                                         String sortOrder,
                                         String sortType,
                                         long organizationUniqueId,
                                         long agreementUniqueId,
                                         long pricelistUniqueId,
                                         long assortmentId,
                                         UserAPI userAPI,
                                         int offset,
                                         int limit,
                                         List<AgreementPricelistRow.Status> statuses,
                                         List<Article.Type> articleTypes,
                                         Boolean showRowsWithPrices,
                                         Boolean showRowsWithoutPrices,
                                         Long categoryUniqueId,
                                         List<Product.Status> articleStatuses) {
        LOG.log(Level.FINEST, "searchPricelistRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2}, statuses: {3}, articleTypes: {4}, showRowsWithPrices: {5}, showRowsWithoutPrices: {6}, categoryUniqueId: {6} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId, statuses, articleTypes, showRowsWithPrices, showRowsWithoutPrices, categoryUniqueId});
        AgreementPricelist pricelist = pricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to search rows on pricelist: {1} which is is not available. Returning 404.", new Object[] {userAPI.getId(), pricelistUniqueId});
            return null;
        }

        //HJAL-2413
        boolean showMore = statuses.contains(AgreementPricelistRow.Status.PENDING_APPROVAL_SHOW_MORE);

        // customers and suppliers should get rows in different statuses, suppliers get all
        // statuses which means we don't have to include it in the search so list is left null
        // or empty
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        if( organization.getOrganizationType() == Organization.OrganizationType.CUSTOMER ) {
            // if no statuses are included, include all statuses except CREATED and CHANGED
            if(statuses.isEmpty()) {
                statuses = new ArrayList<>(Arrays.asList(AgreementPricelistRow.Status.values()));
                statuses.remove(AgreementPricelistRow.Status.CREATED);
            }
            //moving this one inside the if above, a customer cannot have selected CREATED as filter
            //statuses.remove(AgreementPricelistRow.Status.CREATED);
            //2050
            //statuses.remove(AgreementPricelistRow.Status.CHANGED);
            //HJAL-2413 not really necessary to remove PENDING_APPROVAL_SHOW_MORE since it should never have been set
            //statuses.remove(AgreementPricelistRow.Status.PENDING_APPROVAL_SHOW_MORE);
        }

        // if both showRows... is null, it means, show everything, i.e. do not include in search
        boolean searchWithPriceLimitation = true;
        showRowsWithPrices = showRowsWithPrices != null && showRowsWithPrices;
        showRowsWithoutPrices = showRowsWithoutPrices == null ? false: showRowsWithoutPrices;
        if( (showRowsWithPrices && showRowsWithoutPrices) || (!showRowsWithPrices && !showRowsWithoutPrices) ) {
            searchWithPriceLimitation = false;
        }

        // articleStatuses: if none or both articleStatuses are selected (PUBLISHED and DISCONTINUED) do not include this filter in search
        boolean searchWithArticleStatusLimitation = (articleStatuses != null && articleStatuses.size() == 1) ? true : false;

        List<Long> articleIdsOnAssortment = new ArrayList<>();
        if (assortmentId != 0) {
            Assortment assortment = assortmentController.getAssortment(organizationUniqueId, assortmentId, userAPI);
            List<Article> articlesOnAssortment = assortment.getArticles();

            for (Article art : articlesOnAssortment){
                articleIdsOnAssortment.add(art.getUniqueId());
            }
        }

        // count
        StringBuilder countSqlBuilder = new StringBuilder();
        countSqlBuilder.append("SELECT COUNT(p.uniqueId) FROM AgreementPricelistRow p ");
        if( (articleTypes != null && !articleTypes.isEmpty()) || categoryUniqueId != null ) {
            countSqlBuilder.append("LEFT JOIN p.article.category ac ");
            countSqlBuilder.append("LEFT JOIN p.article.basedOnProduct.category apc ");
        }
        countSqlBuilder.append("WHERE p.pricelist.uniqueId = :pricelistUniqueId ");
        if (!articleIdsOnAssortment.isEmpty()){
            countSqlBuilder.append("AND p.article.uniqueId NOT IN :articleIdsOnAssortment ");
        }
        if(!statuses.isEmpty()) {
            countSqlBuilder.append("AND p.status IN :statuses ");
        }
        if( queryString != null && !queryString.isEmpty() ) {
            countSqlBuilder.append("AND (p.article.articleName LIKE :query OR p.article.articleNumber LIKE :query) ");
        }
        if( categoryUniqueId != null ) {
            countSqlBuilder.append("AND (ac.uniqueId = :categoryUniqueId OR apc.uniqueId = :categoryUniqueId) ");
        }
        if (assortmentId != 0) {
            countSqlBuilder.append("AND apc.code IS NOT NULL ");
        }
        if( articleTypes != null && !articleTypes.isEmpty() ) {
            countSqlBuilder.append("AND (");
            for( int i=0; i<articleTypes.size(); i++ ) {
                if( i!=0 ) {
                    countSqlBuilder.append("OR ");
                }
                countSqlBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(" ) ");
            }
            countSqlBuilder.append(") ");
        }
        if( searchWithPriceLimitation ) {
            // means one of showRows... is false and one is true
            if( showRowsWithPrices ) {
                countSqlBuilder.append("AND p.price IS NOT NULL ");
            } else if( showRowsWithoutPrices ) {
                countSqlBuilder.append("AND p.price IS NULL ");
            }
        }

        if (searchWithArticleStatusLimitation){
            countSqlBuilder.append("AND p.article.status IN :articleStatuses ");
        }

        String countQuerySql = countSqlBuilder.toString();
        LOG.log(Level.FINEST, "countQuerySql: {0}", new Object[] {countQuerySql});
        Query countQuery = em.createQuery(countQuerySql).
                setParameter("pricelistUniqueId", pricelistUniqueId);
        if (!articleIdsOnAssortment.isEmpty()){
            countQuery.setParameter("articleIdsOnAssortment", articleIdsOnAssortment);
        }
        if( categoryUniqueId != null ) {
            countQuery.setParameter("categoryUniqueId", categoryUniqueId);
        }
        if( articleTypes != null && !articleTypes.isEmpty() ) {
            for( int i=0; i<articleTypes.size(); i++ ) {
                countQuery.setParameter("type"+i, articleTypes.get(i));
            }
        }
        if(!statuses.isEmpty()) {
            countQuery.setParameter("statuses", statuses);
        }
        if(searchWithArticleStatusLimitation) {
            countQuery.setParameter("articleStatuses", articleStatuses);
        }
        if( queryString != null && !queryString.isEmpty() ) {
            countQuery.setParameter("query", "%" + queryString + "%");
        }
        Long count = (Long) countQuery.getSingleResult();

        // search
        StringBuilder searchSqlBuilder = new StringBuilder();
        //searchSqlBuilder.append("SELECT p, aa.price FROM AgreementPricelistRow p ");
        searchSqlBuilder.append("SELECT p FROM AgreementPricelistRow p ");
        if( (articleTypes != null && !articleTypes.isEmpty()) || categoryUniqueId != null ) {
            searchSqlBuilder.append("LEFT JOIN p.article.category ac ");
            searchSqlBuilder.append("LEFT JOIN p.article.basedOnProduct.category apc ");
        }

        searchSqlBuilder.append("WHERE p.pricelist.uniqueId = :pricelistUniqueId ");
        if (!articleIdsOnAssortment.isEmpty()){
            searchSqlBuilder.append("AND p.article.uniqueId NOT IN :articleIdsOnAssortment ");
        }
        if (!statuses.isEmpty()) {
            searchSqlBuilder.append("AND p.status IN :statuses ");
        }
        if (queryString != null && !queryString.isEmpty()) {
            searchSqlBuilder.append("AND (p.article.articleName LIKE :query OR p.article.articleNumber LIKE :query) ");
        }
        if (categoryUniqueId != null) {
            searchSqlBuilder.append("AND (ac.uniqueId = :categoryUniqueId OR apc.uniqueId = :categoryUniqueId) ");
        }
        if (assortmentId != 0) {
            searchSqlBuilder.append("AND apc.code IS NOT NULL ");
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            searchSqlBuilder.append("AND (");
            for (int i = 0; i < articleTypes.size(); i++) {
                if (i != 0) {
                    searchSqlBuilder.append("OR ");
                }
                searchSqlBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(" ) ");
            }
            searchSqlBuilder.append(") ");
        }
        if (searchWithPriceLimitation) {
            // means one of showRows... is false and one is true
            if (showRowsWithPrices) {
                searchSqlBuilder.append("AND p.price IS NOT NULL ");
            } else if (showRowsWithoutPrices) {
                searchSqlBuilder.append("AND p.price IS NULL ");
            }
        }

        if (searchWithArticleStatusLimitation){
            searchSqlBuilder.append("AND p.article.status IN :articleStatuses ");
        }


        if (!Objects.equals(sortType, "")) {
            sortType = sortType.toUpperCase();
            sortOrder = sortOrder.toUpperCase();

            switch (sortType) {
                case "NAME":
                    searchSqlBuilder.append("ORDER BY p.article.articleName ");
                    break;
                case "NUMBER":
                    searchSqlBuilder.append("ORDER BY p.article.articleNumber ");
                    break;
                case "ORGANIZATIONNAME":
                    searchSqlBuilder.append("ORDER BY p.article.organization.organizationName ");
                    break;
                case "BASEDONPRODUCT":
                    searchSqlBuilder.append("ORDER BY p.article.basedOnProduct.productName ");
                    break;
                case "ARTICLETYPE":
                    searchSqlBuilder.append("ORDER BY p.article.basedOnProduct.category.articleType ");
                    break;
                case "CODE":
                    searchSqlBuilder.append("ORDER BY p.article.basedOnProduct.category.code ");
                    break;
                case "STATUS":
                    searchSqlBuilder.append("ORDER BY p.article.status ");
                    break;
            }

            if (sortOrder.equals("DESC"))
                searchSqlBuilder.append("DESC ");
            else
                searchSqlBuilder.append("ASC ");
        } else {
            searchSqlBuilder.append("ORDER BY p.article.articleNumber ASC");
        }


        String searchQuerySql = searchSqlBuilder.toString();
        LOG.log(Level.FINEST, "searchQuerySql: {0}", new Object[]{searchQuerySql});
        Query searchQuery = em.createQuery(searchQuerySql).
                setParameter("pricelistUniqueId", pricelistUniqueId);
        if (!articleIdsOnAssortment.isEmpty()){
            searchQuery.setParameter("articleIdsOnAssortment", articleIdsOnAssortment);
        }
        if (!statuses.isEmpty()) {
            searchQuery.setParameter("statuses", statuses);
        }
        if(searchWithArticleStatusLimitation) {
            searchQuery.setParameter("articleStatuses", articleStatuses);
        }
        if( queryString != null && !queryString.isEmpty() ) {
            searchQuery.setParameter("query", "%" + queryString + "%");
        }
        if (categoryUniqueId != null) {
            searchQuery.setParameter("categoryUniqueId", categoryUniqueId);
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            for (int i = 0; i < articleTypes.size(); i++) {
                searchQuery.setParameter("type" + i, articleTypes.get(i));
            }
        }
        LOG.log(Level.FINEST, "searchQuery: {0}", new Object[] {searchQuery});
        List<AgreementPricelistRowAPI> pricelistRowAPIs = AgreementPricelistRowMapper.map(searchQuery.
                setMaxResults(limit).
                setFirstResult(offset).
                getResultList(), false, false);

        if (showMore) {
            //HJAL-2413 Hämta tidigare godkända värden
            List<AgreementPricelistRowAPI> combinedList = new ArrayList<>();
            for (AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs) {
                AgreementPricelistRowAPI approvedPricelistRowAPI = new AgreementPricelistRowAPI();

                approvedPricelistRowAPI.setId(pricelistRowAPI.getId());

                if (getApprovedPricelistRow(pricelistRowAPI.getId()) != null){
                    approvedPricelistRowAPI.setStatus("ACTIVE");

                    Date approvedValidFromDate = getApprovedPricelistRow(pricelistRowAPI.getId()) != null ? getApprovedPricelistRow(pricelistRowAPI.getId()).getValidFrom() : null;
                    assert approvedValidFromDate != null;
                    Long approvedValidFromLong = approvedValidFromDate.getTime();
                    approvedPricelistRowAPI.setValidFrom(approvedValidFromLong); //the approved value

                    approvedPricelistRowAPI.setPrice(getApprovedPricelistRow(pricelistRowAPI.getId()).getPrice()); //the approved value
                    approvedPricelistRowAPI.setLeastOrderQuantity(getApprovedPricelistRow(pricelistRowAPI.getId()).getLeastOrderQuantity()); //the approved value
                    approvedPricelistRowAPI.setDeliveryTime(getApprovedPricelistRow(pricelistRowAPI.getId()).getDeliveryTime()); //the approved value
                    approvedPricelistRowAPI.setWarrantyQuantity(getApprovedPricelistRow(pricelistRowAPI.getId()).getWarrantyQuantity()); //the approved value

                    CVGuaranteeUnitAPI approvedWarrantyQuantityUnit = new CVGuaranteeUnitAPI();
                    if (getApprovedPricelistRow(pricelistRowAPI.getId()).getWarrantyQuantityUnit() != null) {
                        approvedWarrantyQuantityUnit.setId(getApprovedPricelistRow(pricelistRowAPI.getId()).getWarrantyQuantityUnit().getUniqueId());
                        approvedWarrantyQuantityUnit.setCode(getApprovedPricelistRow(pricelistRowAPI.getId()).getWarrantyQuantityUnit().getCode());
                        approvedWarrantyQuantityUnit.setName(getApprovedPricelistRow(pricelistRowAPI.getId()).getWarrantyQuantityUnit().getName());
                        approvedPricelistRowAPI.setWarrantyQuantityUnit(approvedWarrantyQuantityUnit); //the approved value
                    }
                    else{
                        approvedPricelistRowAPI.setWarrantyQuantityUnit(null);
                    }

                    CVPreventiveMaintenanceAPI approvedWarrantyValidFrom = new CVPreventiveMaintenanceAPI();
                    if (getApprovedPricelistRow(pricelistRowAPI.getId()).getWarrantyValidFrom() != null) {
                        approvedWarrantyValidFrom.setId(getApprovedPricelistRow(pricelistRowAPI.getId()).getWarrantyValidFrom().getUniqueId());
                        approvedWarrantyValidFrom.setCode(getApprovedPricelistRow(pricelistRowAPI.getId()).getWarrantyValidFrom().getCode());
                        approvedWarrantyValidFrom.setName(getApprovedPricelistRow(pricelistRowAPI.getId()).getWarrantyValidFrom().getName());
                        approvedPricelistRowAPI.setWarrantyValidFrom(approvedWarrantyValidFrom); //the approved value
                    }
                    else{
                        approvedPricelistRowAPI.setWarrantyValidFrom(null);
                    }

                    approvedPricelistRowAPI.setWarrantyTerms(getApprovedPricelistRow(pricelistRowAPI.getId()).getWarrantyTerms()); //the approved value
                    approvedPricelistRowAPI.setComment("Godkänd " + getApprovedPricelistRow(pricelistRowAPI.getId()).getApprovedDate().toString().substring(0,10) + " av " + getApprovedPricelistRow(pricelistRowAPI.getId()).getApprovedBy());
                    approvedPricelistRowAPI.setPricelist(pricelistRowAPI.getPricelist());

                    Long categoryId = getApprovedPricelistRow(pricelistRowAPI.getId()).getCategoryId();
                    if (categoryId != null) {
                        ArticleAPI tempArticleAPI = new ArticleAPI();
                        Category tempCategory = categoryController.getById(categoryId);
                        tempArticleAPI.setCategory(CategoryMapper.map(tempCategory));
                        tempArticleAPI.setArticleNumber(pricelistRowAPI.getArticle().getArticleNumber());
                        tempArticleAPI.setArticleName(pricelistRowAPI.getArticle().getArticleName());
                        tempArticleAPI.setStatus(pricelistRowAPI.getArticle().getStatus());
                        tempArticleAPI.setOrderUnit(pricelistRowAPI.getArticle().getOrderUnit());

                        approvedPricelistRowAPI.setArticle(tempArticleAPI); //this is where we get the approved isoCode from
                    }
                    else {
                        approvedPricelistRowAPI.setArticle(pricelistRowAPI.getArticle());
                    }
                }
                else{ //never approved before
                    approvedPricelistRowAPI.setStatus("CREATED");
                    approvedPricelistRowAPI.setValidFrom(null);
                    approvedPricelistRowAPI.setPrice(null);
                    approvedPricelistRowAPI.setLeastOrderQuantity(null);
                    approvedPricelistRowAPI.setDeliveryTime(null);
                    approvedPricelistRowAPI.setWarrantyQuantity(null);
                    approvedPricelistRowAPI.setWarrantyQuantityUnit(null);
                    approvedPricelistRowAPI.setWarrantyValidFrom(null);
                    approvedPricelistRowAPI.setWarrantyTerms(null);
                    approvedPricelistRowAPI.setComment("Historik saknas");
                    approvedPricelistRowAPI.setPricelist(pricelistRowAPI.getPricelist());
                    approvedPricelistRowAPI.setArticle(pricelistRowAPI.getArticle());
                }

                combinedList.add(approvedPricelistRowAPI);
                combinedList.add(pricelistRowAPI);

            }
            pricelistRowAPIs = combinedList;
        }

        return new SearchDTO(count, pricelistRowAPIs);
    }

    //this one is for assortments, to be able to add all rows with one click
    //leaving a lot of filters that is not needed but might be in the future...
    public SearchDTO searchPricelistRowsAll(String queryString,
                                         long organizationUniqueId,
                                         long agreementUniqueId,
                                         long pricelistUniqueId,
                                         long assortmentId,
                                         UserAPI userAPI,
                                         List<AgreementPricelistRow.Status> statuses,
                                         List<Article.Type> articleTypes,
                                         Boolean showRowsWithPrices,
                                         Boolean showRowsWithoutPrices,
                                         Long categoryUniqueId,
                                         List<Product.Status> articleStatuses) {
        LOG.log(Level.FINEST, "searchPricelistRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2}, statuses: {3}, articleTypes: {4}, showRowsWithPrices: {5}, showRowsWithoutPrices: {6}, categoryUniqueId: {6} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId, statuses, articleTypes, showRowsWithPrices, showRowsWithoutPrices, categoryUniqueId});
        AgreementPricelist pricelist = pricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to search rows on pricelist: {1} which is is not available. Returning 404.", new Object[] {userAPI.getId(), pricelistUniqueId});
            return null;
        }


        // customers and suppliers should get rows in different statuses, suppliers get all
        // statuses which means we don't have to include it in the search so list is left null
        // or empty
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        //if( organization.getOrganizationType() == Organization.OrganizationType.CUSTOMER ) {
            // if no statuses are included, include all statuses except CREATED and CHANGED
            if(statuses.isEmpty()) {
                statuses = new ArrayList<>(Arrays.asList(AgreementPricelistRow.Status.values()));
                statuses.remove(AgreementPricelistRow.Status.CREATED);
            }
        //}

        // if both showRows... is null, it means, show everything, i.e. do not include in search
        boolean searchWithPriceLimitation = true;
        showRowsWithPrices = showRowsWithPrices != null && showRowsWithPrices;
        showRowsWithoutPrices = showRowsWithoutPrices == null ? false: showRowsWithoutPrices;
        if( (showRowsWithPrices && showRowsWithoutPrices) || (!showRowsWithPrices && !showRowsWithoutPrices) ) {
            searchWithPriceLimitation = false;
        }

        // articleStatuses: if none or both articleStatuses are selected (PUBLISHED and DISCONTINUED) do not include this filter in search
        boolean searchWithArticleStatusLimitation = (articleStatuses != null && articleStatuses.size() == 1) ? true : false;

        List<Long> articleIdsOnAssortment = new ArrayList<>();
        if (assortmentId != 0) {
            Assortment assortment = assortmentController.getAssortment(organizationUniqueId, assortmentId, userAPI);
            List<Article> articlesOnAssortment = assortment.getArticles();

            for (Article art : articlesOnAssortment){
                articleIdsOnAssortment.add(art.getUniqueId());
            }
        }


        // search
        StringBuilder searchSqlBuilder = new StringBuilder();
        searchSqlBuilder.append("SELECT p FROM AgreementPricelistRow p ");
        if( (articleTypes != null && !articleTypes.isEmpty()) || categoryUniqueId != null ) {
            searchSqlBuilder.append("LEFT JOIN p.article.category ac ");
            searchSqlBuilder.append("LEFT JOIN p.article.basedOnProduct.category apc ");
        }

        searchSqlBuilder.append("WHERE p.pricelist.uniqueId = :pricelistUniqueId ");
        if (!articleIdsOnAssortment.isEmpty()){
            searchSqlBuilder.append("AND p.article.uniqueId NOT IN :articleIdsOnAssortment ");
        }
        if (!statuses.isEmpty()) {
            searchSqlBuilder.append("AND p.status IN :statuses ");
        }
        if (queryString != null && !queryString.isEmpty()) {
            searchSqlBuilder.append("AND (p.article.articleName LIKE :query OR p.article.articleNumber LIKE :query) ");
        }
        if (categoryUniqueId != null) {
            searchSqlBuilder.append("AND (ac.uniqueId = :categoryUniqueId OR apc.uniqueId = :categoryUniqueId) ");
        }
        if (assortmentId != 0) {
            searchSqlBuilder.append("AND apc.code IS NOT NULL ");
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            searchSqlBuilder.append("AND (");
            for (int i = 0; i < articleTypes.size(); i++) {
                if (i != 0) {
                    searchSqlBuilder.append("OR ");
                }
                searchSqlBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(" ) ");
            }
            searchSqlBuilder.append(") ");
        }
        if (searchWithPriceLimitation) {
            // means one of showRows... is false and one is true
            if (showRowsWithPrices) {
                searchSqlBuilder.append("AND p.price IS NOT NULL ");
            } else if (showRowsWithoutPrices) {
                searchSqlBuilder.append("AND p.price IS NULL ");
            }
        }

        if (searchWithArticleStatusLimitation){
            searchSqlBuilder.append("AND p.article.status IN :articleStatuses ");
        }

        String searchQuerySql = searchSqlBuilder.toString();
        LOG.log(Level.FINEST, "searchQuerySql: {0}", new Object[]{searchQuerySql});
        Query searchQuery = em.createQuery(searchQuerySql).
            setParameter("pricelistUniqueId", pricelistUniqueId);
        if (!articleIdsOnAssortment.isEmpty()){
            searchQuery.setParameter("articleIdsOnAssortment", articleIdsOnAssortment);
        }
        if (!statuses.isEmpty()) {
            searchQuery.setParameter("statuses", statuses);
        }
        if(searchWithArticleStatusLimitation) {
            searchQuery.setParameter("articleStatuses", articleStatuses);
        }
        if( queryString != null && !queryString.isEmpty() ) {
            searchQuery.setParameter("query", "%" + queryString + "%");
        }
        if (categoryUniqueId != null) {
            searchQuery.setParameter("categoryUniqueId", categoryUniqueId);
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            for (int i = 0; i < articleTypes.size(); i++) {
                searchQuery.setParameter("type" + i, articleTypes.get(i));
            }
        }
        LOG.log(Level.FINEST, "searchQuery: {0}", new Object[] {searchQuery});
        List<AgreementPricelistRowAPI> pricelistRowAPIs = AgreementPricelistRowMapper.map(searchQuery.
            getResultList(), false, false);


        return new SearchDTO((long)pricelistRowAPIs.size(), pricelistRowAPIs);
    }

    public SearchDTO searchPricelistRowsByApprovementId(String queryString,
                                         long organizationUniqueId,
                                         long agreementUniqueId,
                                         long pricelistUniqueId,
                                         long approvementId,
                                         UserAPI userAPI,
                                         int offset,
                                         int limit,
                                         List<AgreementPricelistRow.Status> statuses,
                                         List<Article.Type> articleTypes,
                                         Boolean showRowsWithPrices,
                                         Boolean showRowsWithoutPrices,
                                         Long categoryUniqueId) {
        LOG.log(Level.FINEST, "searchPricelistRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2}, statuses: {3}, articleTypes: {4}, showRowsWithPrices: {5}, showRowsWithoutPrices: {6}, categoryUniqueId: {7} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId, statuses, articleTypes, showRowsWithPrices, showRowsWithoutPrices, categoryUniqueId});
        AgreementPricelist pricelist = pricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to search rows on pricelist: {1} which is is not available. Returning 404.", new Object[] {userAPI.getId(), pricelistUniqueId});
            return null;
        }

        // customers and suppliers should get rows in different statuses, suppliers get all
        // statuses which means we don't have to include it in the search so list is left null
        // or empty
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        if( organization.getOrganizationType() == Organization.OrganizationType.CUSTOMER ) {
            // if no statuses are included, include all statuses except CREATED and CHANGED
            if( statuses == null || statuses.isEmpty() ) {
                statuses = new ArrayList<>(Arrays.asList(AgreementPricelistRow.Status.values()));
            }
            statuses.remove(AgreementPricelistRow.Status.CREATED);
            //2050
            //statuses.remove(AgreementPricelistRow.Status.CHANGED);
        }

        // if both showRows... is null, it means, show everything, i.e. do not include in search
        boolean searchWithPriceLimitation = true;
        showRowsWithPrices = showRowsWithPrices != null && showRowsWithPrices;
        showRowsWithoutPrices = showRowsWithoutPrices != null && showRowsWithoutPrices;
        if( (showRowsWithPrices && showRowsWithoutPrices) || (!showRowsWithPrices && !showRowsWithoutPrices) ) {
            searchWithPriceLimitation = false;
        }

        // count
        StringBuilder countSqlBuilder = new StringBuilder();
        countSqlBuilder.append("SELECT COUNT(p.uniqueId) FROM AgreementPricelistRow p ");
        if( (articleTypes != null && !articleTypes.isEmpty()) || categoryUniqueId != null ) {
            countSqlBuilder.append("LEFT JOIN p.article.category ac ");
            countSqlBuilder.append("LEFT JOIN p.article.basedOnProduct.category apc ");
        }

        countSqlBuilder.append("LEFT JOIN p.approvedAgreementPricelistRow aapr ");

        countSqlBuilder.append("WHERE p.pricelist.uniqueId = :pricelistUniqueId ");
        countSqlBuilder.append("AND p.approvedAgreementPricelistRow.approvementId = :approvementId ");
        if( statuses != null && !statuses.isEmpty() ) {
            countSqlBuilder.append("AND p.status IN :statuses ");
        }
        if( queryString != null && !queryString.isEmpty() ) {
            countSqlBuilder.append("AND (p.article.articleName LIKE :query OR p.article.articleNumber LIKE :query) ");
        }
        if( categoryUniqueId != null ) {
            countSqlBuilder.append("AND (ac.uniqueId = :categoryUniqueId OR apc.uniqueId = :categoryUniqueId) ");
        }
        if( articleTypes != null && !articleTypes.isEmpty() ) {
            countSqlBuilder.append("AND (");
            for( int i=0; i<articleTypes.size(); i++ ) {
                if( i!=0 ) {
                    countSqlBuilder.append("OR ");
                }
                countSqlBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(" ) ");
            }
            countSqlBuilder.append(") ");
        }
        if( searchWithPriceLimitation ) {
            // means one of showRows... is false and one is true
            if( showRowsWithPrices ) {
                countSqlBuilder.append("AND p.price IS NOT NULL ");
            } else if( showRowsWithoutPrices ) {
                countSqlBuilder.append("AND p.price IS NULL ");
            }
        }
        String countQuerySql = countSqlBuilder.toString();
        LOG.log(Level.FINEST, "countQuerySql: {0}", new Object[] {countQuerySql});
        Query countQuery = em.createQuery(countQuerySql).
                setParameter("pricelistUniqueId", pricelistUniqueId);
        if( categoryUniqueId != null ) {
            countQuery.setParameter("categoryUniqueId", categoryUniqueId);
        }
        if( articleTypes != null && !articleTypes.isEmpty() ) {
            for( int i=0; i<articleTypes.size(); i++ ) {
                countQuery.setParameter("type"+i, articleTypes.get(i));
            }
        }
        if( statuses != null && !statuses.isEmpty() ) {
            countQuery.setParameter("statuses", statuses);
        }
        if( queryString != null && !queryString.isEmpty() ) {
            countQuery.setParameter("query", "%" + queryString + "%");
        }

        countQuery.setParameter("approvementId", approvementId);

        Long count = (Long) countQuery.getSingleResult();

        // search

        StringBuilder searchSqlBuilder = new StringBuilder();
        {
            searchSqlBuilder.append("SELECT p FROM AgreementPricelistRow p ");
            if ((articleTypes != null && !articleTypes.isEmpty()) || categoryUniqueId != null) {
                searchSqlBuilder.append("LEFT JOIN p.article.category ac ");
                searchSqlBuilder.append("LEFT JOIN p.article.basedOnProduct.category apc ");
            }


            searchSqlBuilder.append("LEFT JOIN p.approvedAgreementPricelistRow aapr ");



            searchSqlBuilder.append("WHERE p.pricelist.uniqueId = :pricelistUniqueId ");

            searchSqlBuilder.append("AND p.approvedAgreementPricelistRow.approvementId = :approvementId ");
            if (statuses != null && !statuses.isEmpty()) {
                searchSqlBuilder.append("AND p.status IN :statuses ");
            }
            if (queryString != null && !queryString.isEmpty()) {
                searchSqlBuilder.append("AND (p.article.articleName LIKE :query OR p.article.articleNumber LIKE :query) ");
            }
            if (categoryUniqueId != null) {
                searchSqlBuilder.append("AND (ac.uniqueId = :categoryUniqueId OR apc.uniqueId = :categoryUniqueId) ");
            }
            if (articleTypes != null && !articleTypes.isEmpty()) {
                searchSqlBuilder.append("AND (");
                for (int i = 0; i < articleTypes.size(); i++) {
                    if (i != 0) {
                        searchSqlBuilder.append("OR ");
                    }
                    searchSqlBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(" ) ");
                }
                searchSqlBuilder.append(") ");
            }
            if (searchWithPriceLimitation) {
                // means one of showRows... is false and one is true
                if (showRowsWithPrices) {
                    searchSqlBuilder.append("AND p.price IS NOT NULL ");
                } else if (showRowsWithoutPrices) {
                    searchSqlBuilder.append("AND p.price IS NULL ");
                }
            }



            searchSqlBuilder.append("ORDER BY p.article.articleNumber ASC");
            String searchQuerySql = searchSqlBuilder.toString();
            LOG.log(Level.FINEST, "searchQuerySql: {0}", new Object[]{searchQuerySql});
            Query searchQuery = em.createQuery(searchQuerySql).
                    setParameter("pricelistUniqueId", pricelistUniqueId);
            if (statuses != null && !statuses.isEmpty()) {
                searchQuery.setParameter("statuses", statuses);
            }
            if (queryString != null && !queryString.isEmpty()) {
                searchQuery.setParameter("query", "%" + queryString + "%");
            }
            if (categoryUniqueId != null) {
                searchQuery.setParameter("categoryUniqueId", categoryUniqueId);
            }
            if (articleTypes != null && !articleTypes.isEmpty()) {
                for (int i = 0; i < articleTypes.size(); i++) {
                    searchQuery.setParameter("type" + i, articleTypes.get(i));
                }
            }

            searchQuery.setParameter("approvementId", approvementId);

            LOG.log(Level.FINEST, "searchQuery: {0}", new Object[] {searchQuery});
            List<AgreementPricelistRowAPI> pricelistRowAPIs = AgreementPricelistRowMapper.map(searchQuery.
                    setMaxResults(limit).
                    setFirstResult(offset).
                    getResultList(), false, false);
            if (!pricelistRowAPIs.isEmpty())
                LOG.log(Level.FINEST, "pricelistRowAPIs(0): {0}", new Object[] {pricelistRowAPIs.get(0)});
            return new SearchDTO(count, pricelistRowAPIs);
        }

    }

    /**
     * Get the <code>AgreementPricelistRowAPI</code> for the given unique id on the
     * specified organization
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowUniqueId unique id of the pricelist row to get
     * @param userAPI user information
     * @param sessionId
     * @return the corresponding <code>AgreementPricelistRowAPI</code>
     */
    public AgreementPricelistRowAPI getPricelistRowAPI(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, long pricelistRowUniqueId, UserAPI userAPI, String sessionId, String requestIp) {
        LOG.log(Level.FINEST, "getPricelistRowAPI( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2}, pricelistRowUniqueId: {3} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowUniqueId});
        AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowUniqueId, userAPI);
        if( pricelistRow != null ) {
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.VIEW, userAPI.getId(), sessionId, pricelistRowUniqueId, requestIp));
            return AgreementPricelistRowMapper.map( pricelistRow, true, false );
        }
        return null;
    }

    /**
     * Get the <code>AgreementPricelistRow</code> with the given id on the specified organization.
     * The agreement can be read by the defined customer and the supplier and by
     * any organization (customer) that the agreement is shared with.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowUniqueId
     * @param userAPI user information
     * @return the corresponding <code>AgreementPricelist</code>
     */


    public AgreementPricelistRow getPricelistRow(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, long pricelistRowUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getAgreement( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        AgreementPricelistRow pricelistRow = em.find(AgreementPricelistRow.class, pricelistRowUniqueId);
        if( pricelistRow == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowUniqueId});
            return null;
        }

        // make sure user can fetch agreement connected to pricelist
        AgreementPricelist pricelist = pricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1}, but pricelist: {2} is not available. Returning null.", new Object[] {userAPI.getId(), pricelistRowUniqueId, pricelistUniqueId});
            return null;
        }

        if( !pricelistRow.getPricelist().getUniqueId().equals(pricelist.getUniqueId()) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} not on given pricelist: {2}. Returning null.", new Object[] {userAPI.getId(), pricelistRowUniqueId, pricelist.getUniqueId()});
            return null;
        }

        // customers cannot see rows in status CREATED, only limitation right now
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        if( organization.getOrganizationType() == Organization.OrganizationType.CUSTOMER && pricelistRow.getStatus() == AgreementPricelistRow.Status.CREATED ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} on given pricelist: {2} in status: {3} which is not allowed. Returning null.", new Object[] {userAPI.getId(), pricelistRowUniqueId, pricelist.getUniqueId(), pricelistRow.getStatus()});
            return null;
        }

        return pricelistRow;
    }

    public ApprovedAgreementPricelistRow getApprovedPricelistRow(long pricelistRowUniqueId) {
        ApprovedAgreementPricelistRow approvedPricelistRow = em.find(ApprovedAgreementPricelistRow.class, pricelistRowUniqueId);
        return approvedPricelistRow;
    }

    /**
     * Create a pricelist row based on the supplied data.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs user supplied data
     * @param userAPI logged in user's session information
     * @return the created <code>AgreementPricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public List<AgreementPricelistRowAPI> createPricelistRows( long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp ) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createPricelistRow( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});

        // pricelist
        AgreementPricelist pricelist = pricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to add articles to pricelist: {1} which does not exist or is not available to user", new Object[] {userAPI.getId(), pricelistUniqueId});
            throw validationMessageService.generateValidationException("pricelist", "pricelistrow.pricelist.notNull");
        }
        List<AgreementPricelistRowAPI> agreementPricelistRowAPIs = new ArrayList<>();
        List<Long> currentArticleUniqueIdsInPricelist = getAgreementPricelistArticleUniqueIds(pricelist.getUniqueId());
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = createPricelistRow(organizationUniqueId, agreementUniqueId, pricelistRowAPI, pricelist, currentArticleUniqueIdsInPricelist, userAPI, sessionId, requestIp);
            currentArticleUniqueIdsInPricelist.add(pricelistRow.getArticle().getUniqueId());
            agreementPricelistRowAPIs.add(AgreementPricelistRowMapper.map(pricelistRow, true, false));
        }
        return agreementPricelistRowAPIs;
    }

    public AgreementPricelistRow createPricelistRow(long organizationUniqueId, long agreementUniqueId, AgreementPricelistRowAPI pricelistRowAPI, AgreementPricelist pricelist, List<Long> currentArticleUniqueIdsInPricelist, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        AgreementPricelist currentPricelist = pricelistController.getCurrentPricelist(agreementUniqueId);
        return createPricelistRow(organizationUniqueId, pricelistRowAPI, pricelist, currentPricelist, currentArticleUniqueIdsInPricelist, userAPI, sessionId, requestIp);
    }

    public AgreementPricelistRowAPI deleteRow(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, long pricelistRowUniqueId, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteRow( organizationUniqueId: {0}, agreementUniqueId: {1} pricelistUniqueId: {2} pricelistRowUniqueId: {3})", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowUniqueId});
        AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowUniqueId, userAPI);
        //ApprovedAgreementPricelistRow approvedPricelistRow = getApprovedPricelistRow(pricelistRowUniqueId);

        em.remove(pricelistRow);
        LOG.log(Level.INFO, "Row in agreement pricelist deleted...");

        //em.remove(approvedPricelistRow);
        //LOG.log(Level.INFO, "Row in approved agreement pricelist deleted...");
        return AgreementPricelistRowMapper.map(pricelistRow, true, true); //???
    }

    public void removeDuplicatePricelistRows() {
        LOG.log(Level.FINEST, "removeDuplicatePricelistRows()");
        //avtal
        String sql = "SELECT * FROM hjmtj.AgreementPricelistRow";
        sql += " WHERE uniqueId NOT IN (SELECT * FROM (SELECT MAX(apr.uniqueId) FROM hjmtj.AgreementPricelistRow apr GROUP BY apr.articleId, apr.agreementPricelistId) x)";
        List<AgreementPricelistRow> agreementPricelistRows =  em.createNativeQuery(sql, AgreementPricelistRow.class).getResultList();

        for (AgreementPricelistRow pricelistRow : agreementPricelistRows) {
            em.remove(pricelistRow);
        }
        LOG.log(Level.INFO, "{0} duplicate agreement pricelist rows deleted...", new Object[] {agreementPricelistRows.size()});

        //GP
        sql = "SELECT * FROM hjmtj.GeneralPricelistPricelistRow";
        sql += " WHERE uniqueId NOT IN (SELECT * FROM (SELECT MAX(gppr.uniqueId) FROM hjmtj.GeneralPricelistPricelistRow gppr GROUP BY gppr.articleId, gppr.generalPricelistPricelistId) x)";
        List<GeneralPricelistPricelistRow> generalPricelistPricelistRows =  em.createNativeQuery(sql, GeneralPricelistPricelistRow.class).getResultList();

        for (GeneralPricelistPricelistRow pricelistRow : generalPricelistPricelistRows) {
            em.remove(pricelistRow);
        }
        LOG.log(Level.INFO, "{0} duplicate GP pricelist rows deleted...", new Object[] {generalPricelistPricelistRows.size()});
    }

    public AgreementPricelistRow createPricelistRow(long organizationUniqueId, AgreementPricelistRowAPI pricelistRowAPI, AgreementPricelist pricelist, AgreementPricelist currentPricelist, List<Long> currentArticleUniqueIdsInPricelist, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        pricelistRowValidation.validateForCreate(pricelistRowAPI, pricelist, currentPricelist);

        Date validFrom = new Date();
        AgreementPricelist.Status pricelistStatus = AgreementPricelistMapper.getPricelistStatus(pricelist, currentPricelist);
        if( pricelistStatus == AgreementPricelist.Status.FUTURE ) {
            validFrom = pricelist.getValidFrom();
        }
        AgreementPricelistRow pricelistRow = AgreementPricelistRowMapper.map(pricelistRowAPI, validFrom);
        pricelistRow.setPricelist(pricelist);

        // article
        Article article = articleController.getArticle(organizationUniqueId, pricelistRowAPI.getArticle().getId(), userAPI);
        if( article == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to add article: {1} which doesn't exist to pricelist: {2}", new Object[] {userAPI.getId(), pricelistRowAPI.getArticle().getId(), pricelist.getUniqueId()});
            throw validationMessageService.generateValidationException("article", "pricelistrow.article.notExist");
        }
        /*
        HJAL-1774
        if( article.getStatus() == Product.Status.DISCONTINUED ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to add article: {1} which is discontinued to pricelist: {2}", new Object[] {userAPI.getId(), article.getUniqueId(), pricelist.getUniqueId()});
            throw validationMessageService.generateValidationException("article", "pricelistrow.article.discontinued");
        }*/

        /*
        HJAL-2022
        if( currentArticleUniqueIdsInPricelist.contains(article.getUniqueId())) {
            throw validationMessageService.generateValidationException("article", "pricelistrow.article.alreadyExistsInPricelist", article.getArticleName());
        }
        */
        pricelistRow.setArticle(article);

        Article.Type articleType = pricelistRow.getArticle().getBasedOnProduct() == null ? pricelistRow.getArticle().getCategory().getArticleType(): pricelistRow.getArticle().getBasedOnProduct().getCategory().getArticleType();
        AgreementPricelistRowMapper.mapUpdatableFields(pricelistRowAPI, pricelistRow, articleType, true);
        setOverrideWarrantyValidFrom(pricelistRowAPI, pricelistRow, articleType, userAPI, true);

        // set overridden warranty quantity unit (if any)
        setOverriddenWarrantyQuantityUnit(pricelistRow, pricelistRowAPI, true);

        em.persist(pricelistRow);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        return pricelistRow;
    }

    /**
     * Update a pricelist based on the supplied data
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowUniqueId unique id of the pricelist row
     * @param pricelistRowAPI user supplied data
     * @param userAPI logged in user's session information
     * @param sessionId
     * @param requestIp
     * @return the updated <code>AgreementPricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public AgreementPricelistRowAPI updatePricelistRow(long organizationUniqueId,
            long agreementUniqueId,
            long pricelistUniqueId,
            long pricelistRowUniqueId,
            AgreementPricelistRowAPI pricelistRowAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updatePricelistRow( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2}, pricelistRowUniqueId: {3} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowUniqueId});
        AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowUniqueId, userAPI);
        if( pricelistRow == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning 404.", new Object[] {userAPI.getId(), pricelistRowUniqueId});
            return null;
        }
        AgreementPricelist currentPricelist = pricelistController.getCurrentPricelist(agreementUniqueId);
        return updatePricelistRow(pricelistRow, pricelistRowAPI, userAPI, sessionId, requestIp, currentPricelist);
    }

    public AgreementPricelistRowAPI updatePricelistRow(AgreementPricelistRow pricelistRow,
            AgreementPricelistRowAPI pricelistRowAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp,
            AgreementPricelist currentPricelist) throws HjalpmedelstjanstenValidationException {
        pricelistRowValidation.validateForUpdate(pricelistRowAPI, pricelistRow, currentPricelist);
        pricelistRow.setPrice(pricelistRowAPI.getPrice());
        pricelistRow.setLeastOrderQuantity(pricelistRowAPI.getLeastOrderQuantity());
        Article.Type articleType = pricelistRow.getArticle().getBasedOnProduct() == null ? pricelistRow.getArticle().getCategory().getArticleType(): pricelistRow.getArticle().getBasedOnProduct().getCategory().getArticleType();
        AgreementPricelistRowMapper.mapUpdatableFields(pricelistRowAPI, pricelistRow, articleType, false);
        setOverrideWarrantyValidFrom(pricelistRowAPI, pricelistRow, articleType, userAPI, false);

        // set overridden warranty quantity unit (if any)
        setOverriddenWarrantyQuantityUnit(pricelistRow, pricelistRowAPI, false);
        if( pricelistRow.getStatus() == AgreementPricelistRow.Status.ACTIVE || pricelistRow.getStatus() == AgreementPricelistRow.Status.DECLINED ) {
            pricelistRow.setStatus(AgreementPricelistRow.Status.CHANGED);
        }
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        return AgreementPricelistRowMapper.map(pricelistRow, true, false);
    }

    /**
     * Send the specified pricelist rows for approval by customer. The supplier
     * can do this and only if each row is either in status CREATED, CHANGED or DECLINED.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs list of rows to send to customer for approval
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> sendRowsForCustomerApproval(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "sendRowsForCustomerApproval( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }
        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to send pricelist row: {1} for customer approval where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
            }
            if( !AgreementPricelistRow.Status.CREATED.equals(pricelistRow.getStatus()) &&
                    !AgreementPricelistRow.Status.DECLINED.equals(pricelistRow.getStatus()) &&
                    !AgreementPricelistRow.Status.CHANGED.equals(pricelistRow.getStatus())) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to send pricelist row: {1} for customer approval which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.sendRowsForCustomerApproval.wrongStatus");
            }
            if( pricelistRow.getPrice() == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to send pricelist row: {1} for customer approval, but row has no price.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                throw validationMessageService.generateValidationException("price", "pricelistrow.sendRowsForCustomerApproval.noPrice");
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.PENDING_APPROVAL);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }

        try {
            // send message to customer
            Agreement agreement = pricelistRows.get(0).getPricelist().getAgreement();
            if( agreement.getCustomerPricelistApprovers() != null && !agreement.getCustomerPricelistApprovers().isEmpty() ) {
                LOG.log( Level.FINEST, "{0} rows sent for approval, send mail", new Object[] {pricelistRows.size()});
                String selectedSubject = rowsSentForApprovalMailSubject;
                String selectedBody = rowsSentForApprovalMailBody;
                for( UserEngagement userEngagement : agreement.getCustomerPricelistApprovers() ) {
                    emailController.send(userEngagement.getUserAccount().getElectronicAddress().getEmail(), selectedSubject, selectedBody);
                }
            }
        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on rows sent for approval", ex);
        }
        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    public List<AgreementPricelistRowAPI> sendRowForCustomerApprovalWithoutMail(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, AgreementPricelistRowAPI pricelistRowAPI, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "sendRowsForCustomerApproval( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        if( pricelistRowAPI == null) {
            return null;
        }
        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
        if( pricelistRow == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
            return null;
        }
        if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
            return null;
        }
        if( pricelistRow.getPrice() == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to send pricelist row: {1} for customer approval, but row has no price.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
            return null;
        }
        pricelistRow.setStatus(AgreementPricelistRow.Status.PENDING_APPROVAL);
        pricelistRows.add(pricelistRow);

        for( AgreementPricelistRow tempPricelistRow : pricelistRows ) {
            em.merge(tempPricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }
        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    /**
     * Send the specified pricelist rows for approval by customer. The supplier
     * can do this and only if each row is either in status CREATED, CHANGED or DECLINED.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs list of rows to send to customer for approval
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> sendRowsAndCommentForCustomerApproval(String comment, long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "sendRowsAndCommentForCustomerApproval( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2}, comment: {3} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId, comment});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }
        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();

        ApprovedAgreementPricelistRow approvedPricelistRow = new ApprovedAgreementPricelistRow();
        String changeMessage = "";
        int maxPricelistRowsInMail = 20;

        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to send pricelist row: {1} for customer approval where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
            }
            if( !AgreementPricelistRow.Status.CREATED.equals(pricelistRow.getStatus()) &&
                    !AgreementPricelistRow.Status.DECLINED.equals(pricelistRow.getStatus()) &&
                    !AgreementPricelistRow.Status.CHANGED.equals(pricelistRow.getStatus())) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to send pricelist row: {1} for customer approval which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.sendRowsForCustomerApproval.wrongStatus");
            }
            if( pricelistRow.getPrice() == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to send pricelist row: {1} for customer approval, but row has no price.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                throw validationMessageService.generateValidationException("price", "pricelistrow.sendRowsForCustomerApproval.noPrice");
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.PENDING_APPROVAL);
            pricelistRows.add(pricelistRow);
            pricelistRow.setComment(comment);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);

            //Skicka ett mindre utförligt mail om antalet ändrade rader är fler är maxPricelistRowsInMail
            if (pricelistRows.size() <= maxPricelistRowsInMail) {
                //jämför med värden i tabellen ApprovedAgreementPricelistRow
                approvedPricelistRow = getApprovedPricelistRow(pricelistRow.getUniqueId());
                if (approvedPricelistRow == null)
                    changeMessage += "<br>Prislisterad med artikelnummer " + pricelistRow.getArticle().getArticleNumber() + " saknar historik.<br>";
                else {
                    try {
                        changeMessage += "<br>Prislisterad med artikelnummer " + pricelistRow.getArticle().getArticleNumber() + " blev senast godkänd " + approvedPricelistRow.getApprovedDate().toString().substring(0, 10) + " av " + approvedPricelistRow.getApprovedBy() + ".<br>";
                        if (!pricelistRow.getPrice().equals(approvedPricelistRow.getPrice()))
                            changeMessage += "Priset är ändrat från " + approvedPricelistRow.getPrice() + " till " + pricelistRow.getPrice() + ".<br>";
                        if (pricelistRow.getValidFrom() != null && pricelistRow.getValidFrom() != approvedPricelistRow.getValidFrom())
                            changeMessage += "Giltig from är ändrat från " + approvedPricelistRow.getValidFrom() + " till " + pricelistRow.getValidFrom() + ".<br>";
                        if (pricelistRow.getLeastOrderQuantity() != approvedPricelistRow.getLeastOrderQuantity())
                            changeMessage += "Min best är ändrat från " + approvedPricelistRow.getLeastOrderQuantity() + " till " + pricelistRow.getLeastOrderQuantity() + ".<br>";
                        if (pricelistRow.getDeliveryTime() != null && !pricelistRow.getDeliveryTime().equals(approvedPricelistRow.getDeliveryTime()))
                            changeMessage += "Levtid är ändrat från " + approvedPricelistRow.getDeliveryTime() + " till " + pricelistRow.getDeliveryTime() + ".<br>";
                        if (pricelistRow.getWarrantyQuantity() != null && !pricelistRow.getWarrantyQuantity().equals(approvedPricelistRow.getWarrantyQuantity())) {
                            if (approvedPricelistRow.getWarrantyQuantity() == null) {
                                changeMessage += "Garanti (antal) är ändrat från inget till " + pricelistRow.getWarrantyQuantity() + ".<br>";
                            } else {
                                changeMessage += "Garanti (antal) är ändrat från " + approvedPricelistRow.getWarrantyQuantity() + " till " + pricelistRow.getWarrantyQuantity() + ".<br>";
                            }
                        }
                        if (pricelistRow.getWarrantyQuantityUnit() != null) {
                            if (approvedPricelistRow.getWarrantyQuantityUnit() == null) {
                                changeMessage += "Garanti (enhet) är ändrat från inget till " + pricelistRow.getWarrantyQuantityUnit().getName() + ".<br>";
                            } else if (!pricelistRow.getWarrantyQuantityUnit().getName().equals(approvedPricelistRow.getWarrantyQuantityUnit().getName()))
                                changeMessage += "Garanti (enhet) är ändrat från " + approvedPricelistRow.getWarrantyQuantityUnit().getName() + " till " + pricelistRow.getWarrantyQuantityUnit().getName() + ".<br>";
                        }
                        if (pricelistRow.getWarrantyValidFrom() != null) {
                            if (approvedPricelistRow.getWarrantyValidFrom() == null) {
                                changeMessage += "Garanti (gäller från) är ändrat från inget datum till " + pricelistRow.getWarrantyValidFrom().getName() + ".<br>";
                            } else if (!pricelistRow.getWarrantyValidFrom().getName().equals(approvedPricelistRow.getWarrantyValidFrom().getName())) {
                                changeMessage += "Garanti (gäller från) är ändrat från " + approvedPricelistRow.getWarrantyValidFrom().getName() + " till " + pricelistRow.getWarrantyValidFrom().getName() + ".<br>";
                            }
                        }
                        if (pricelistRow.getWarrantyTerms() != null && !pricelistRow.getWarrantyTerms().equals(approvedPricelistRow.getWarrantyTerms()))
                            if (approvedPricelistRow.getWarrantyTerms() == null) {
                                changeMessage += "Garanti (villkor) är ändrat från inga villkor till " + pricelistRow.getWarrantyTerms() + ".<br>";
                            } else {
                                changeMessage += "Garanti (villkor) är ändrat från " + approvedPricelistRow.getWarrantyTerms() + " till " + pricelistRow.getWarrantyTerms() + ".<br>";
                            }
                    } catch (Exception e) {
                        StringWriter sw = new StringWriter();
                        e.printStackTrace(new PrintWriter(sw));
                        String exceptionAsString = sw.toString();
                        LOG.log(Level.WARNING, "Failed to construct updatemessage error:" + e.getMessage() + "<br/>" + exceptionAsString.replaceAll("(\r\n|\n)", "<br />"));
                        changeMessage += " Uppdaterad prislisterad";
                    }
                }
            }

            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }
        if (pricelistRows.size() > maxPricelistRowsInMail) {
            changeMessage += "Det är för många ändringar för att visa i mailet.<br>";
        }

        try {
            // send mail m16_0 to customer
            Agreement agreement = pricelistRows.get(0).getPricelist().getAgreement();
            if( agreement.getCustomerPricelistApprovers() != null && !agreement.getCustomerPricelistApprovers().isEmpty() ) {

                //check if user has approved to receive mail
                List<?> usersWhoApproved = usersWhoApprovedToReceiveMail("AGREEMENT_PRICELISTROW_APPROVE");

                LOG.log( Level.FINEST, "{0} rows sent for approval, send mail", new Object[] {pricelistRows.size()});
                String selectedSubject = rowsSentForApprovalMailSubject;
                String selectedBody = rowsSentForApprovalMailBody;
                for( UserEngagement userEngagement : agreement.getCustomerPricelistApprovers() ) {
                    if (usersWhoApproved.contains(convertLongToBigInteger(userEngagement.getUniqueId()))) {
                        LOG.log(Level.FINEST, userEngagement.getUserAccount().getUsername() + " has chosen to receive mail regarding approvals");
                        emailController.send(userEngagement.getUserAccount().getElectronicAddress().getEmail(), selectedSubject, selectedBody);
                    }
                    else{
                        LOG.log(Level.FINEST, userEngagement.getUserAccount().getUsername() + " has chosen NOT to receive mail regarding approvals");
                    }
                }
            }
        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on rows sent for approval", ex);
        }
        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    private BigInteger convertLongToBigInteger(long value){
        return new BigInteger(ByteBuffer.allocate(Long.SIZE/Byte.SIZE).putLong(value).array());
    }

    private List<?> usersWhoApprovedToReceiveMail(String notificationType) {
        String sql = "SELECT ue.uniqueId FROM hjmtj.UserAccountNotification uan";
        sql +=" INNER JOIN hjmtj.Notification n ON uan.notificationId = n.uniqueId";
        sql +=" INNER JOIN hjmtj.UserEngagement ue ON ue.userId = uan.userId";
        sql +=" WHERE n.type = '" + notificationType + "'";
        return em.createNativeQuery(sql).getResultList();
    }

    /**
     * Send all valid rows on specified pricelist for approval by customer. Valid rows
     * are in status CREATED, CHANGED or DECLINED and has a price set.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * the agreement is discontinued
     */
    public List<AgreementPricelistRowAPI> sendAllRowsForCustomerApproval(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "sendAllRowsForCustomerApproval( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        AgreementPricelist pricelist = pricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to send all rows on pricelist: {1} for customer approval which is is not available. Returning null.", new Object[] {userAPI.getId(), pricelistUniqueId});
            return null;
        }
        if( pricelist.getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to send all pricelist rows for  pricelist: {1} for customer approval where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelist.getUniqueId()});
            throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
        }

        List<AgreementPricelistRow.Status> statuses = new ArrayList<>();
        statuses.add(AgreementPricelistRow.Status.CREATED);
        statuses.add(AgreementPricelistRow.Status.CHANGED);
        List<AgreementPricelistRow> pricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_PRICELIST_AND_STATUSES).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                setParameter("statuses", statuses).
                getResultList();
        if( pricelistRows != null && !pricelistRows.isEmpty() ) {
            for( AgreementPricelistRow pricelistRow : pricelistRows ) {
                if (pricelistRow.getPrice() != null) {
                    pricelistRow.setStatus(AgreementPricelistRow.Status.PENDING_APPROVAL);
                    em.merge(pricelistRow);
                    internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));

                }

            }

            try {
                // send message to customer
                Agreement agreement = pricelistRows.get(0).getPricelist().getAgreement();
                if( agreement.getCustomerPricelistApprovers() != null && !agreement.getCustomerPricelistApprovers().isEmpty() ) {
                    LOG.log( Level.FINEST, "{0} rows sent for approval, send mail", new Object[] {pricelistRows.size()});
                    String selectedSubject = rowsSentForApprovalMailSubject;
                    String selectedBody = rowsSentForApprovalMailBody;
                    for( UserEngagement userEngagement : agreement.getCustomerPricelistApprovers() ) {
                        emailController.send(userEngagement.getUserAccount().getElectronicAddress().getEmail(), selectedSubject, selectedBody);
                    }
                }
            } catch (MessagingException ex) {
                LOG.log(Level.SEVERE, "Failed to send message on rows sent for approval", ex);
            }
        }
        // don't need to include rows in reply, only return something not null
        return new ArrayList();
    }

    /**
     * Send all valid rows on specified pricelist for approval by customer. Valid rows
     * are in status CREATED, CHANGED or DECLINED and has a price set.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * the agreement is discontinued
     */
    public List<AgreementPricelistRowAPI> sendAllRowsAndCommentForCustomerApproval(String comment, long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "sendAllRowsAndCommentForCustomerApproval( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        AgreementPricelist pricelist = pricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to send all rows on pricelist: {1} for customer approval which is is not available. Returning null.", new Object[] {userAPI.getId(), pricelistUniqueId});
            return null;
        }
        if( pricelist.getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to send all pricelist rows for pricelist: {1} for customer approval where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelist.getUniqueId()});
            throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
        }

        List<AgreementPricelistRow.Status> statuses = new ArrayList<>();
        statuses.add(AgreementPricelistRow.Status.CREATED);
        statuses.add(AgreementPricelistRow.Status.CHANGED);
        List<AgreementPricelistRow> pricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_PRICELIST_AND_STATUSES).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                setParameter("statuses", statuses).
                getResultList();

        ApprovedAgreementPricelistRow approvedPricelistRow = new ApprovedAgreementPricelistRow();
        StringBuilder changeMessage = new StringBuilder();

        if( pricelistRows != null && !pricelistRows.isEmpty() ) {
            int maxPricelistRowsInMail = 20;

                int maxRows = 10000;
                int thisRow = 1;

                for( AgreementPricelistRow pricelistRow : pricelistRows ) {
                    if (thisRow > maxRows) {
                        break;
                    }
                    else {

                        if (pricelistRow.getPrice() != null) {
                            pricelistRow.setStatus(AgreementPricelistRow.Status.PENDING_APPROVAL);
                            em.merge(pricelistRow);
                            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));

                        }


                        //Skicka ett mindre utförligt mail om antalet ändrade rader är fler är maxPricelistRowsInMail


                        if (pricelistRows.size() <= maxPricelistRowsInMail || maxRows <= maxPricelistRowsInMail) {
                            //jämför med värden i tabellen ApprovedAgreementPricelistRow
                            approvedPricelistRow = getApprovedPricelistRow(pricelistRow.getUniqueId());
                            if (approvedPricelistRow == null)
                                changeMessage.append("<br>Prislisterad med artikelnummer ").append(pricelistRow.getArticle().getArticleNumber()).append(" saknar historik.<br>");
                            else {
                                try {
                                    changeMessage.append("<br>Prislisterad med artikelnummer ").append(pricelistRow.getArticle().getArticleNumber()).append(" blev senast godkänd ").append(approvedPricelistRow.getApprovedDate().toString().substring(0, 10)).append(" av ").append(approvedPricelistRow.getApprovedBy()).append(".<br>");
                                    if (!pricelistRow.getPrice().equals(approvedPricelistRow.getPrice()))
                                        changeMessage.append("Priset är ändrat från ").append(approvedPricelistRow.getPrice()).append(" till ").append(pricelistRow.getPrice()).append(".<br>");
                                    if (pricelistRow.getValidFrom() != null && pricelistRow.getValidFrom() != approvedPricelistRow.getValidFrom())
                                        changeMessage.append("Giltig from är ändrat från ").append(approvedPricelistRow.getValidFrom()).append(" till ").append(pricelistRow.getValidFrom()).append(".<br>");
                                    if (pricelistRow.getLeastOrderQuantity() != approvedPricelistRow.getLeastOrderQuantity())
                                        changeMessage.append("Min best är ändrat från ").append(approvedPricelistRow.getLeastOrderQuantity()).append(" till ").append(pricelistRow.getLeastOrderQuantity()).append(".<br>");
                                    if (pricelistRow.getDeliveryTime() != null && !pricelistRow.getDeliveryTime().equals(approvedPricelistRow.getDeliveryTime()))
                                        changeMessage.append("Levtid är ändrat från ").append(approvedPricelistRow.getDeliveryTime()).append(" till ").append(pricelistRow.getDeliveryTime()).append(".<br>");
                                    if (pricelistRow.getWarrantyQuantity() != null && !pricelistRow.getWarrantyQuantity().equals(approvedPricelistRow.getWarrantyQuantity())) {
                                        if (approvedPricelistRow.getWarrantyQuantity() == null) {
                                            changeMessage.append("Garanti (antal) är ändrat från inget till ").append(pricelistRow.getWarrantyQuantity()).append(".<br>");
                                        } else {
                                            changeMessage.append("Garanti (antal) är ändrat från ").append(approvedPricelistRow.getWarrantyQuantity()).append(" till ").append(pricelistRow.getWarrantyQuantity()).append(".<br>");
                                        }
                                    }
                                    if (pricelistRow.getWarrantyQuantityUnit() != null) {
                                        if (approvedPricelistRow.getWarrantyQuantityUnit() == null) {
                                            changeMessage.append("Garanti (enhet) är ändrat från inget till ").append(pricelistRow.getWarrantyQuantityUnit().getName()).append(".<br>");
                                        } else if (!pricelistRow.getWarrantyQuantityUnit().getName().equals(approvedPricelistRow.getWarrantyQuantityUnit().getName()))
                                            changeMessage.append("Garanti (enhet) är ändrat från ").append(approvedPricelistRow.getWarrantyQuantityUnit().getName()).append(" till ").append(pricelistRow.getWarrantyQuantityUnit().getName()).append(".<br>");
                                    }
                                    if (pricelistRow.getWarrantyValidFrom() != null) {
                                        if (approvedPricelistRow.getWarrantyValidFrom() == null) {
                                            changeMessage.append("Garanti (gäller från) är ändrat från inget datum till ").append(pricelistRow.getWarrantyValidFrom().getName()).append(".<br>");
                                        } else if (!pricelistRow.getWarrantyValidFrom().getName().equals(approvedPricelistRow.getWarrantyValidFrom().getName())) {
                                            changeMessage.append("Garanti (gäller från) är ändrat från ").append(approvedPricelistRow.getWarrantyValidFrom().getName()).append(" till ").append(pricelistRow.getWarrantyValidFrom().getName()).append(".<br>");
                                        }
                                    }
                                    if (pricelistRow.getWarrantyTerms() != null && !pricelistRow.getWarrantyTerms().equals(approvedPricelistRow.getWarrantyTerms()))
                                        if (approvedPricelistRow.getWarrantyTerms() == null) {
                                            changeMessage.append("Garanti (villkor) är ändrat från inga villkor till ").append(pricelistRow.getWarrantyTerms()).append(".<br>");
                                        } else {
                                            changeMessage.append("Garanti (villkor) är ändrat från ").append(approvedPricelistRow.getWarrantyTerms()).append(" till ").append(pricelistRow.getWarrantyTerms()).append(".<br>");
                                        }
                                } catch (Exception e) {
                                    StringWriter sw = new StringWriter();
                                    e.printStackTrace(new PrintWriter(sw));
                                    String exceptionAsString = sw.toString();
                                    LOG.log(Level.WARNING, "Failed to construct updatemessage error:" + e.getMessage() + "<br/>" + exceptionAsString.replaceAll("(\r\n|\n)", "<br />"));
                                    changeMessage.append(" Uppdaterad prislisterad");
                                }
                            }
                        }
                    }
                    thisRow++;
                }

            try {
                // send message to customer
                Agreement agreement = pricelistRows.get(0).getPricelist().getAgreement();
                if( agreement.getCustomerPricelistApprovers() != null && !agreement.getCustomerPricelistApprovers().isEmpty() ) {
                    if (pricelistRows.size() > maxRows)
                        LOG.log( Level.FINEST, "{0} rows sent for approval, send mail", new Object[] {maxRows});
                    else
                        LOG.log( Level.FINEST, "{0} rows sent for approval, send mail", new Object[] {pricelistRows.size()});
                    String selectedSubject = rowsSentForApprovalMailSubject;
                    String selectedBody = rowsSentForApprovalMailBody;

                    //check if user has approved to receive mail
                    List<?> usersWhoApproved = usersWhoApprovedToReceiveMail("AGREEMENT_PRICELISTROW_APPROVE");

                    for( UserEngagement userEngagement : agreement.getCustomerPricelistApprovers() ) {
                        if (usersWhoApproved.contains(convertLongToBigInteger(userEngagement.getUniqueId()))) {
                            LOG.log(Level.FINEST, userEngagement.getUserAccount().getUsername() + " has chosen to receive mail regarding approvals");
                            emailController.send(userEngagement.getUserAccount().getElectronicAddress().getEmail(), selectedSubject, selectedBody);
                        }
                        else{
                            LOG.log(Level.FINEST, userEngagement.getUserAccount().getUsername() + " has chosen NOT to receive mail regarding approvals");
                        }
                    }
                }
            } catch (MessagingException ex) {
                LOG.log(Level.SEVERE, "Failed to send message on rows sent for approval", ex);
            }
        }
        // don't need to include rows in reply, only return something not null
        return new ArrayList();
    }

    /**
     * Decline the specified pricelist rows. The customer can do this and only
     * if each row is either in status PENDING_APPROVAL or ACTIVE.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs list of rows to send to decline
     * @param userAPI user session information
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> declineRows(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "declineRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }

        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);

        // organizations/business levels (customers) that the agreement is shared
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline row on agreement: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }

        // the logged in user must be set as pricelist approver in order to decline rows
        if( !userIsPricelistApprover(organizationUniqueId, agreement, userAPI) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist rows on agreement: {1} but user is not pricelist approver", new Object[] {userAPI.getId(), agreement.getUniqueId()});
            throw generateForbiddenException();
        }

        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist row: {1} where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
            }
            if( !AgreementPricelistRow.Status.PENDING_APPROVAL.equals(pricelistRow.getStatus()) &&
                    !AgreementPricelistRow.Status.ACTIVE.equals(pricelistRow.getStatus())) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.declineRows.wrongStatus");
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.DECLINED);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }
        // TODO: send message to supplier, this may include a customizable message from the customer

        // HJAL-1701 - Send message to supplier...
        try{
            List<UserRole.RoleName> roleNames = new ArrayList<>();
            roleNames.add(UserRole.RoleName.SupplierAgreementManager);
            List<UserAPI> supplierAgreementManager = null;
            if(userController != null && agreement.getSupplier() != null && agreement.getSupplier().getUniqueId() != null) {
                supplierAgreementManager = userController.getUsersInRolesOnOrganization(agreement.getSupplier().getUniqueId(), roleNames);
            }



            if(supplierAgreementManager != null && !supplierAgreementManager.isEmpty()) {
                String selectedSubject = rowsSentDeclinedMailSubject;
                StringBuilder rowsFormatted = new StringBuilder();
                for (AgreementPricelistRow pricelistRow : pricelistRows)
                {
                    rowsFormatted.append("Artikelnummer: " + pricelistRow.getArticle().getArticleNumber() + "<br/>");
                }
                String priceListNumber = "-";
                if(!pricelistRows.isEmpty() && pricelistRows.get(0) != null) {
                    priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                }

                String mailBody  = String.format(rowsSentDeclinedMailBody,
                        pricelistRows.size(),
                        agreement.getAgreementName(),
                        agreement.getValidFrom(),
                        agreement.getAgreementNumber(),
                        priceListNumber,
                        agreement.getCustomer().getOrganizationName(),
                        rowsFormatted.toString());
                //check if user has approved to receive mail
                List<?> usersWhoApproved = usersWhoApprovedToReceiveMail("AGREEMENT_PRICELISTEROW_CUSTOMER_DECLINED");

                for (UserAPI agreementManager : supplierAgreementManager) {
                    if(agreementManager.getElectronicAddress() != null && agreementManager.getElectronicAddress().getEmail() != null && !agreementManager.getElectronicAddress().getEmail().isEmpty()) {
                        if (usersWhoApproved.contains(convertLongToBigInteger(agreementManager.getId()))) {
                            LOG.log(Level.FINEST, agreementManager.getUsername() + " has chosen to receive mail regarding declined pricelistrows");
                            emailController.send(agreementManager.getElectronicAddress().getEmail(), selectedSubject, mailBody);
                        }
                        else{
                            LOG.log(Level.FINEST, agreementManager.getUsername() + " has chosen NOT to receive mail regarding declined pricelistrows");
                        }

                    }
                }
            }
        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on declined rows on approval", ex);

        }

        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    /**
     * Decline the specified pricelist rows. The customer can do this and only
     * if each row is either in status PENDING_APPROVAL or ACTIVE.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs list of rows to send to decline
     * @param userAPI user session information
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> declineRowsWithComment(String comment, long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "declineRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }

        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);

        // organizations/business levels (customers) that the agreement is shared
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline row on agreement: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }

        // the logged in user must be set as pricelist approver in order to decline rows
        if( !userIsPricelistApprover(organizationUniqueId, agreement, userAPI) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist rows on agreement: {1} but user is not pricelist approver", new Object[] {userAPI.getId(), agreement.getUniqueId()});
            throw generateForbiddenException();
        }

        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist row: {1} where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
            }
            if( !AgreementPricelistRow.Status.PENDING_APPROVAL.equals(pricelistRow.getStatus()) &&
                    !AgreementPricelistRow.Status.ACTIVE.equals(pricelistRow.getStatus())) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.declineRows.wrongStatus");
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.DECLINED);
            pricelistRow.setComment(comment);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }

        // HJAL-1701 - Send message to supplier...
        try{
            List<UserRole.RoleName> roleNames = new ArrayList<>();
            roleNames.add(UserRole.RoleName.SupplierAgreementManager);
            List<UserAPI> supplierAgreementManager = null;
            if(userController != null && agreement.getSupplier() != null && agreement.getSupplier().getUniqueId() != null) {
                supplierAgreementManager = userController.getUsersInRolesOnOrganization(agreement.getSupplier().getUniqueId(), roleNames);
            }



            if(supplierAgreementManager != null && !supplierAgreementManager.isEmpty()) {
                String priceListNumber = "-";
                if(!pricelistRows.isEmpty() && pricelistRows.get(0) != null) {
                    priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                }
                String selectedSubject = pricelistRows.size() == 1 ? rowsSentDeclinedMailSubject_single : rowsSentDeclinedMailSubject;
                String mailBody  = String.format(
                    pricelistRows.size() == 1 ? rowsSentDeclinedMailBody_single: rowsSentDeclinedMailBody,
                        !comment.isEmpty() ? "Kommentar från kunden:<br/>&emsp;" + comment + "<br/><br/>": "",
                    pricelistRows.size(),
                    agreement.getAgreementName(),
                    agreement.getValidFrom(),
                    agreement.getAgreementNumber(),
                    priceListNumber,
                    agreement.getCustomer().getOrganizationName(),
                    pricelistRows.stream()
                        .map(AgreementPricelistRow::getArticle)
                        .map(article -> "&emsp;" + article.getArticleNumber() + " &emsp; " + article.getArticleName() + "<br/>")
                        .collect(joining(""))
                );

                //check if user has approved to receive mail
                List<?> usersWhoApproved = usersWhoApprovedToReceiveMail("AGREEMENT_PRICELISTEROW_CUSTOMER_DECLINED");

                for (UserAPI agreementManager : supplierAgreementManager) {
                    if(agreementManager.getElectronicAddress() != null && agreementManager.getElectronicAddress().getEmail() != null && !agreementManager.getElectronicAddress().getEmail().isEmpty()) {
                        if (usersWhoApproved.contains(convertLongToBigInteger(agreementManager.getId()))) {
                            LOG.log(Level.FINEST, agreementManager.getUsername() + " has chosen to receive mail regarding declined pricelistrows");
                            emailController.send(agreementManager.getElectronicAddress().getEmail(), selectedSubject, mailBody);
                        }
                        else{
                            LOG.log(Level.FINEST, agreementManager.getUsername() + " has chosen NOT to receive mail regarding declined pricelistrows");
                        }
                    }
                }
            }
        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on declined rows on approval", ex);

        }

        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }


    /**
     * Activate the specified pricelist rows. The customer can do this and only
     * if each row is either in status PENDING_APPROVAL.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs list of rows to send to activate
     * @param userAPI user session information
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> activateRows(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "activateRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }

        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);

        // organizations/business levels (customers) that the agreement is shared
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to activate row on agreement: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }

        // the logged in user must be set as pricelist approver in order to activate rows
        if( !userIsPricelistApprover(organizationUniqueId, agreement, userAPI) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to activate pricelist rows on agreement: {1} but user is not pricelist approver", new Object[] {userAPI.getId(), agreement.getUniqueId()});
            throw generateForbiddenException();
        }

        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }

            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to activate pricelist row: {1} where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
            }
            if( !AgreementPricelistRow.Status.PENDING_APPROVAL.equals(pricelistRow.getStatus()) ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to activate pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.activateRows.wrongStatus");
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.ACTIVE);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }

        try
        {
            // HJAL-1701 - Send message to supplier...
            List<UserRole.RoleName> roleNames = new ArrayList<>();
            roleNames.add(UserRole.RoleName.SupplierAgreementManager);
            List<UserAPI> supplierAgreementManager = null;
            if(userController != null && agreement.getSupplier() != null && agreement.getSupplier().getUniqueId() != null) {
                supplierAgreementManager = userController.getUsersInRolesOnOrganization(agreement.getSupplier().getUniqueId(), roleNames);
            }

            if(supplierAgreementManager != null && !supplierAgreementManager.isEmpty())
            {
                LOG.log( Level.FINEST, "{0} rows are accepted, send mail", new Object[] {pricelistRows.size()});
                String priceListNumber = "-";
                if(!pricelistRows.isEmpty() && pricelistRows.get(0) != null) {
                    priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                }
                String selectedSubject = rowsSentApprovedMailSubject;
                //String mailBody = rowsSentApprovedMailBody;
                String mailBody = String.format(rowsSentApprovedMailBody, agreement.getAgreementName(),  agreement.getValidFrom(), agreement.getAgreementNumber(),priceListNumber,agreement.getCustomer().getOrganizationName(),pricelistRows.size());
                if (pricelistRows.size() == 1){
                    selectedSubject = rowsSentApprovedMailSubject_single;
                    mailBody = String.format(rowsSentApprovedMailBody_single, agreement.getAgreementName(),  agreement.getValidFrom(), agreement.getAgreementNumber(),priceListNumber,agreement.getCustomer().getOrganizationName(),pricelistRows.size());
                }
                //check if user has approved to receive mail
                List<?> usersWhoApproved = usersWhoApprovedToReceiveMail("AGREEMENT_PRICELISTEROW_CUSTOMER_APPROVED");

                for (UserAPI agreementManager : supplierAgreementManager) {
                    if(agreementManager.getElectronicAddress() != null && agreementManager.getElectronicAddress().getEmail() != null && !agreementManager.getElectronicAddress().getEmail().isEmpty()) {
                        if (usersWhoApproved.contains(convertLongToBigInteger(agreementManager.getId()))) {
                            LOG.log(Level.FINEST, agreementManager.getUsername() + " has chosen to receive mail regarding approved pricelistrows");
                            emailController.send(agreementManager.getElectronicAddress().getEmail(), selectedSubject, mailBody);
                        }
                        else{
                            LOG.log(Level.FINEST, agreementManager.getUsername() + " has chosen NOT to receive mail regarding approved pricelistrows");
                        }
                    }
                }
            }

        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on rows, activate pricelist rows", ex);
        }


        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    /**
     * Activate the specified pricelist rows. The customer can do this and only
     * if each row is either in status PENDING_APPROVAL. Including comment.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs list of rows to send to activate
     * @param userAPI user session information
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> activateRowsWithComment(String comment, long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "activateRowsWithComment( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }

        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);

        // organizations/business levels (customers) that the agreement is shared
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to activate row on agreement: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }

        // the logged in user must be set as pricelist approver in order to activate rows
        if( !userIsPricelistApprover(organizationUniqueId, agreement, userAPI) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to activate pricelist rows on agreement: {1} but user is not pricelist approver", new Object[] {userAPI.getId(), agreement.getUniqueId()});
            throw generateForbiddenException();
        }

        //create a row in AgreementPricelistRowApprovement
        AgreementPricelistRowApprovement apra = new AgreementPricelistRowApprovement();
        apra.setComment(comment);
        em.persist(apra);

        //get the uniqueId of the row just created
        Long approvementId = apra.getUniqueId();

        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        List<ApprovedAgreementPricelistRow> approvedPricelistRows = new ArrayList<>();

        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            ApprovedAgreementPricelistRow approvedPricelistRow = getApprovedPricelistRow(pricelistRowAPI.getId());
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }

            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to activate pricelist row: {1} where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
            }
            if( !AgreementPricelistRow.Status.PENDING_APPROVAL.equals(pricelistRow.getStatus()) ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to activate pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.activateRows.wrongStatus");
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.ACTIVE);
            pricelistRows.add(pricelistRow);

            if (approvedPricelistRow == null){
                approvedPricelistRow = new ApprovedAgreementPricelistRow();
            }

            approvedPricelistRow.setUniqueId(pricelistRow.getUniqueId()); //OK
            approvedPricelistRow.setApprovedBy(userAPI.getFirstName() + " " + userAPI.getLastName() + " (" + userAPI.getUsername() + ")");
            approvedPricelistRow.setLeastOrderQuantity(pricelistRow.getLeastOrderQuantity()); //OK
            approvedPricelistRow.setPrice(pricelistRow.getPrice()); //OK
            approvedPricelistRow.setApprovementId(approvementId);

            if (pricelistRow.getValidFrom() != null)
                approvedPricelistRow.setValidFrom(pricelistRow.getValidFrom());
            else
                approvedPricelistRow.setValidFrom(agreement.getValidFrom());


            Article.Type mtype = pricelistRow.getArticle().getBasedOnProduct() == null ? pricelistRow.getArticle().getCategory().getArticleType() : pricelistRow.getArticle().getBasedOnProduct().getCategory().getArticleType();

            if (pricelistRow.getDeliveryTime() != null)
                approvedPricelistRow.setDeliveryTime(pricelistRow.getDeliveryTime());
            else {

                if (mtype == Article.Type.H)
                    approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeH());
                if (mtype == Article.Type.I)
                    approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeI());
                if (mtype == Article.Type.R)
                    approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeR());
                if (mtype == Article.Type.T)
                    approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeT());
                if (mtype == Article.Type.Tj)
                    approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeTJ());
            }

            if (pricelistRow.getWarrantyQuantity() != null)
                approvedPricelistRow.setWarrantyQuantity(pricelistRow.getWarrantyQuantity());
            else {
                if (mtype == Article.Type.H)
                    approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityH());
                if (mtype == Article.Type.I)
                    approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityI());
                if (mtype == Article.Type.R)
                    approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityR());
                if (mtype == Article.Type.T)
                    approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityT());
                if (mtype == Article.Type.Tj)
                    approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityTJ());
            }

            if (pricelistRow.getWarrantyQuantityUnit() != null)
                approvedPricelistRow.setWarrantyQuantityUnit(pricelistRow.getWarrantyQuantityUnit());
            else{
                if (mtype == Article.Type.H)
                    approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityHUnit());
                if (mtype == Article.Type.I)
                    approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityIUnit());
                if (mtype == Article.Type.R)
                    approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityRUnit());
                if (mtype == Article.Type.T)
                    approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityTUnit());
                if (mtype == Article.Type.Tj)
                    approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityTJUnit());
            }

            if (pricelistRow.getWarrantyTerms() != null)
                approvedPricelistRow.setWarrantyTerms(pricelistRow.getWarrantyTerms());
            else{
                if (mtype == Article.Type.H)
                    approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsH());
                if (mtype == Article.Type.I)
                    approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsI());
                if (mtype == Article.Type.R)
                    approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsR());
                if (mtype == Article.Type.T)
                    approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsT());
                if (mtype == Article.Type.Tj)
                    approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsTJ());
            }

            if (pricelistRow.getWarrantyValidFrom() != null)
                approvedPricelistRow.setWarrantyValidFrom(pricelistRow.getWarrantyValidFrom());
            else{
                if (mtype == Article.Type.H)
                    approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromH());
                if (mtype == Article.Type.I)
                    approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromI());
                if (mtype == Article.Type.R)
                    approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromR());
                if (mtype == Article.Type.T)
                    approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromT());
                if (mtype == Article.Type.Tj)
                    approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromTJ());
            }

            ArticleAPI artAPI = pricelistRowAPI.getArticle();

            if (artAPI.getCategory() != null)
                approvedPricelistRow.setCategoryId(artAPI.getCategory().getId());
            else
                approvedPricelistRow.setCategoryId(null);

            approvedPricelistRows.add(approvedPricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }

        for( ApprovedAgreementPricelistRow approvedPricelistRow : approvedPricelistRows ) {
            em.merge(approvedPricelistRow);
        }

        try
        {
            // HJAL-1701 - Send message to supplier...
            List<UserRole.RoleName> roleNames = new ArrayList<>();
            roleNames.add(UserRole.RoleName.SupplierAgreementManager);
            List<UserAPI> supplierAgreementManager = null;
            if(userController != null && agreement.getSupplier() != null && agreement.getSupplier().getUniqueId() != null) {
                supplierAgreementManager = userController.getUsersInRolesOnOrganization(agreement.getSupplier().getUniqueId(), roleNames);
            }

            if(supplierAgreementManager != null && !supplierAgreementManager.isEmpty())
            {
                LOG.log( Level.FINEST, "{0} rows are accepted, send mail", new Object[] {pricelistRows.size()});
                String priceListNumber = "-";
                if(!pricelistRows.isEmpty() && pricelistRows.get(0) != null) {
                    priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                }
                String selectedSubject = rowsSentApprovedMailSubject;
                //String mailBody = rowsSentApprovedMailBody;
                String mailBody = String.format(rowsSentApprovedMailBody, agreement.getAgreementName(),  agreement.getValidFrom(), agreement.getAgreementNumber(),priceListNumber,agreement.getCustomer().getOrganizationName(),pricelistRows.size());
                if (pricelistRows.size() == 1){
                    selectedSubject = rowsSentApprovedMailSubject_single;
                    mailBody = String.format(rowsSentApprovedMailBody_single, agreement.getAgreementName(),  agreement.getValidFrom(), agreement.getAgreementNumber(),priceListNumber,agreement.getCustomer().getOrganizationName(),pricelistRows.size());
                }
                if (!comment.isEmpty()) {
                    mailBody = "Kommentar från kunden:<br> " + comment + "<br><br>" + mailBody;
                }

                //check if user has approved to receive mail
                List<?> usersWhoApproved = usersWhoApprovedToReceiveMail("AGREEMENT_PRICELISTEROW_CUSTOMER_APPROVED");

                for (UserAPI agreementManager : supplierAgreementManager) {
                    if(agreementManager.getElectronicAddress() != null && agreementManager.getElectronicAddress().getEmail() != null && !agreementManager.getElectronicAddress().getEmail().isEmpty()) {
                        if (usersWhoApproved.contains(convertLongToBigInteger(agreementManager.getId()))) {
                            LOG.log(Level.FINEST, agreementManager.getUsername() + " has chosen to receive mail regarding approved pricelistrows");
                            emailController.send(agreementManager.getElectronicAddress().getEmail(), selectedSubject, mailBody);
                        }
                        else{
                            LOG.log(Level.FINEST, agreementManager.getUsername() + " has chosen NOT to receive mail regarding approved pricelistrows");
                        }
                    }
                    // HJAL-2402 - Add user to table ChangeChecker, one for each approved row
                    for( ApprovedAgreementPricelistRow approvedPricelistRow : approvedPricelistRows ) {
                        ChangeChecker cc = new ChangeChecker();
                        cc.setChangeId(approvedPricelistRow.getUniqueId());
                        cc.setType("APPROVEDPRICELISTROW");
                        cc.setUserId(agreementManager.getId());
                        em.persist(cc);
                    }

                }
            }

        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on rows, activate pricelist rows", ex);
        }


        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    /**
     * Activate all pricelist rows in status PENDING_APPROVAL. The customer can
     * do this
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param userAPI user session information
     * @return null or empty list
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * the agreement is discontinued
     */
    public List<AgreementPricelistRowAPI> activateAllRowsWithComment(String comment, long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "activateAllRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        long startTime = System.nanoTime();
        AgreementPricelist pricelist = pricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);

        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);

        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to send all rows on pricelist: {1} for customer approval which is is not available. Returning null.", new Object[] {userAPI.getId(), pricelistUniqueId});
            return null;
        }
        if( pricelist.getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to activate all pricelist rows for pricelist: {1} where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelist.getUniqueId()});
            throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
        }

        List<AgreementPricelistRow.Status> statuses = new ArrayList<>();
        statuses.add(AgreementPricelistRow.Status.PENDING_APPROVAL);
        List<AgreementPricelistRow> pricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_PRICELIST_AND_STATUSES).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                setParameter("statuses", statuses).
                getResultList();

        //create a row in AgreementPricelistRowApprovement
        AgreementPricelistRowApprovement apra = new AgreementPricelistRowApprovement();
        apra.setComment(comment);
        em.persist(apra);

        //get the uniqueId of the row just created
        Long approvementId = apra.getUniqueId();

        List<ApprovedAgreementPricelistRow> approvedPricelistRows = new ArrayList<>();
            int maxRows = 10000;
            int thisRow = 1;
            for( AgreementPricelistRow pricelistRow : pricelistRows ) {
                //if ((block - 1) * blockSize < thisRow && thisRow <= block * blockSize) {
                if (thisRow > maxRows) {
                    break;
                }
                else {
                    //LOG.log(Level.FINEST, "thisRow {0} belongs to block {1} and will be handled now", new Object[] {thisRow, block});
                    pricelistRow.setStatus(AgreementPricelistRow.Status.ACTIVE);

                    ApprovedAgreementPricelistRow approvedPricelistRow = getApprovedPricelistRow(pricelistRow.getUniqueId());

                    if (approvedPricelistRow == null) {
                        approvedPricelistRow = new ApprovedAgreementPricelistRow();
                    }

                    approvedPricelistRow.setUniqueId(pricelistRow.getUniqueId());
                    approvedPricelistRow.setApprovedBy(userAPI.getFirstName() + " " + userAPI.getLastName() + " (" + userAPI.getUsername() + ")");
                    approvedPricelistRow.setLeastOrderQuantity(pricelistRow.getLeastOrderQuantity());
                    approvedPricelistRow.setPrice(pricelistRow.getPrice());
                    approvedPricelistRow.setApprovementId(approvementId);

                    if (pricelistRow.getValidFrom() != null)
                        approvedPricelistRow.setValidFrom(pricelistRow.getValidFrom());
                    else
                        approvedPricelistRow.setValidFrom(agreement.getValidFrom());

                    Article.Type mtype = pricelistRow.getArticle().getBasedOnProduct() == null ? pricelistRow.getArticle().getCategory().getArticleType() : pricelistRow.getArticle().getBasedOnProduct().getCategory().getArticleType();

                    if (pricelistRow.getDeliveryTime() != null)
                        approvedPricelistRow.setDeliveryTime(pricelistRow.getDeliveryTime());
                    else {
                        if (mtype == Article.Type.H)
                            approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeH());
                        if (mtype == Article.Type.I)
                            approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeI());
                        if (mtype == Article.Type.R)
                            approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeR());
                        if (mtype == Article.Type.T)
                            approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeT());
                        if (mtype == Article.Type.Tj)
                            approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeTJ());
                    }

                    if (pricelistRow.getWarrantyQuantity() != null)
                        approvedPricelistRow.setWarrantyQuantity(pricelistRow.getWarrantyQuantity());
                    else {
                        if (mtype == Article.Type.H)
                            approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityH());
                        if (mtype == Article.Type.I)
                            approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityI());
                        if (mtype == Article.Type.R)
                            approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityR());
                        if (mtype == Article.Type.T)
                            approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityT());
                        if (mtype == Article.Type.Tj)
                            approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityTJ());
                    }

                    if (pricelistRow.getWarrantyQuantityUnit() != null)
                        approvedPricelistRow.setWarrantyQuantityUnit(pricelistRow.getWarrantyQuantityUnit());
                    else {
                        if (mtype == Article.Type.H)
                            approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityHUnit());
                        if (mtype == Article.Type.I)
                            approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityIUnit());
                        if (mtype == Article.Type.R)
                            approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityRUnit());
                        if (mtype == Article.Type.T)
                            approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityTUnit());
                        if (mtype == Article.Type.Tj)
                            approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityTJUnit());
                    }

                    if (pricelistRow.getWarrantyTerms() != null)
                        approvedPricelistRow.setWarrantyTerms(pricelistRow.getWarrantyTerms());
                    else {
                        if (mtype == Article.Type.H)
                            approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsH());
                        if (mtype == Article.Type.I)
                            approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsI());
                        if (mtype == Article.Type.R)
                            approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsR());
                        if (mtype == Article.Type.T)
                            approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsT());
                        if (mtype == Article.Type.Tj)
                            approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsTJ());
                    }

                    if (pricelistRow.getWarrantyValidFrom() != null)
                        approvedPricelistRow.setWarrantyValidFrom(pricelistRow.getWarrantyValidFrom());
                    else {
                        if (mtype == Article.Type.H)
                            approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromH());
                        if (mtype == Article.Type.I)
                            approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromI());
                        if (mtype == Article.Type.R)
                            approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromR());
                        if (mtype == Article.Type.T)
                            approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromT());
                        if (mtype == Article.Type.Tj)
                            approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromTJ());
                    }

                    if (pricelistRow.getArticle().getCategory() != null)
                        approvedPricelistRow.setCategoryId(pricelistRow.getArticle().getCategory().getUniqueId());
                    else
                        approvedPricelistRow.setCategoryId(null);

                    approvedPricelistRows.add(approvedPricelistRow);

                    em.merge(pricelistRow);
                    internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
                }
                thisRow++;
            }
            for (ApprovedAgreementPricelistRow approvedPricelistRow : approvedPricelistRows) {
                em.merge(approvedPricelistRow);
            }
            //LOG.log( Level.INFO, "*** Block {0} of {1} done ***", new Object[] {block, numberOfBlocks});
        //}

        try
        {
            // Send message to supplier...

            List<UserRole.RoleName> roleNames = new ArrayList<>();
            roleNames.add(UserRole.RoleName.SupplierAgreementManager);
            List<UserAPI> supplierAgreementManager = null;
            if(userController != null && agreement.getSupplier() != null && agreement.getSupplier().getUniqueId() != null) {
                supplierAgreementManager = userController.getUsersInRolesOnOrganization(agreement.getSupplier().getUniqueId(), roleNames);
            }

            if(supplierAgreementManager != null && !supplierAgreementManager.isEmpty())
            {
                if (pricelistRows.size() > maxRows)
                    LOG.log( Level.FINEST, "{0} rows are accepted, send mail", new Object[] {maxRows});
                else
                    LOG.log( Level.FINEST, "{0} rows are accepted, send mail", new Object[] {pricelistRows.size()});

                String priceListNumber = "-";
                if(!pricelistRows.isEmpty() && pricelistRows.get(0) != null) {
                    priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                }


                String selectedSubject = rowsSentApprovedMailSubject;
                //String mailBody = rowsSentApprovedMailBody;
                String mailBody = String.format(rowsSentApprovedMailBody, agreement.getAgreementName(),  agreement.getValidFrom(), agreement.getAgreementNumber(),priceListNumber,agreement.getCustomer().getOrganizationName(),pricelistRows.size());
                if (pricelistRows.size() == 1){
                    selectedSubject = rowsSentApprovedMailSubject_single;
                    mailBody = String.format(rowsSentApprovedMailBody_single, agreement.getAgreementName(),  agreement.getValidFrom(), agreement.getAgreementNumber(),priceListNumber,agreement.getCustomer().getOrganizationName(),pricelistRows.size());
                }

                if (!comment.isEmpty()) {
                    mailBody = "Kommentar från kunden:<br> " + comment + "<br><br>" + mailBody;
                }

                //check if user has approved to receive mail
                List<?> usersWhoApproved = usersWhoApprovedToReceiveMail("AGREEMENT_PRICELISTEROW_CUSTOMER_APPROVED");

                for (UserAPI agreementManager : supplierAgreementManager) {
                    if(agreementManager.getElectronicAddress() != null && agreementManager.getElectronicAddress().getEmail() != null && !agreementManager.getElectronicAddress().getEmail().isEmpty()) {
                        if (usersWhoApproved.contains(convertLongToBigInteger(agreementManager.getId()))) {
                            LOG.log(Level.FINEST, agreementManager.getUsername() + " has chosen to receive mail regarding approved pricelistrows");
                            emailController.send(agreementManager.getElectronicAddress().getEmail(), selectedSubject, mailBody);
                        }
                        else{
                            LOG.log(Level.FINEST, agreementManager.getUsername() + " has chosen NOT to receive mail regarding approved pricelistrows");
                        }

                        emailController.send(agreementManager.getElectronicAddress().getEmail(), selectedSubject, mailBody);
                    }
                    // HJAL-2402 - Add user to table ChangeChecker, one for each approved row
                    for( ApprovedAgreementPricelistRow approvedPricelistRow : approvedPricelistRows ) {
                        ChangeChecker cc = new ChangeChecker();
                        cc.setChangeId(approvedPricelistRow.getUniqueId());
                        cc.setType("APPROVEDPRICELISTROW");
                        cc.setUserId(agreementManager.getId());
                        em.persist(cc);
                    }
                }
            }

        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on rows, activate pricelist rows", ex);
        }

        long endTime = System.nanoTime();
        LOG.log(Level.INFO, "all approved/activated, took: {0} sec",  new Object[]{(System.nanoTime() - startTime)});
        // don't need to include rows in reply, only return something not null
        //return new ArrayList();

        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }


    /**
     * Activate all pricelist rows in status PENDING_APPROVAL. The customer can
     * do this
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param userAPI user session information
     * @return null or empty list
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * the agreement is discontinued
     */
    public List<AgreementPricelistRowAPI> activateAllRows(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "activateAllRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        AgreementPricelist pricelist = pricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to send all rows on pricelist: {1} for customer approval which is is not available. Returning null.", new Object[] {userAPI.getId(), pricelistUniqueId});
            return null;
        }
        if( pricelist.getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to activate all pricelist rows for pricelist: {1} where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelist.getUniqueId()});
            throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
        }

        List<AgreementPricelistRow.Status> statuses = new ArrayList<>();
        statuses.add(AgreementPricelistRow.Status.PENDING_APPROVAL);
        List<AgreementPricelistRow> pricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_PRICELIST_AND_STATUSES).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                setParameter("statuses", statuses).
                getResultList();
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            pricelistRow.setStatus(AgreementPricelistRow.Status.ACTIVE);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }

        // don't need to include rows in reply, only return something not null
        return new ArrayList();
    }

    /**
     * Inactivate the specified pricelist rows. The supplier can do this and only
     * if each row is either in status ACTIVE.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs list of rows to send to customer for inactivation
     * @param userAPI user session information
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> inactivateRows(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "inactivateRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }
        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;

            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to inactivate pricelist row: {1} where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
            }
            if( !AgreementPricelistRow.Status.ACTIVE.equals(pricelistRow.getStatus()) ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to inactivate pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.inactivateRows.wrongStatus");
            }

            pricelistRow.setStatus(AgreementPricelistRow.Status.PENDING_INACTIVATION);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));

        }

            Agreement agreement =  agreementController.getAgreement(organizationUniqueId,agreementUniqueId,userAPI);
            if( agreement.getCustomerPricelistApprovers() != null && !agreement.getCustomerPricelistApprovers().isEmpty() ) {
                //HJAL-2415 Do NOT send mail immediately, send later by timer
                LOG.log( Level.FINEST, "{0} rows (total) sent for inactivation", new Object[] {pricelistRowAPIs.size()});
            }

        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    public void sendEmailForRowInactivationRequest() {
        //M19-1

        String sql = "SELECT email, count(*) FROM";
        sql += " (SELECT ea.email FROM hjmtj.AgreementPricelistRow apr";
        sql += " INNER JOIN hjmtj.AgreementPricelist ap ON ap.uniqueId = apr.agreementPricelistId";
        sql += " INNER JOIN hjmtj.Agreement a ON a.uniqueId = ap.agreementId";
        sql += " INNER JOIN hjmtj.AgreementUserEngagementCustomer auec ON auec.agreementId = a.uniqueId";
        sql += " INNER JOIN hjmtj.UserEngagement ue ON ue.uniqueId = auec.userEngagementId";

        sql += " INNER JOIN hjmtj.UserAccountNotification uan ON uan.userId = ue.userId";
        sql += " INNER JOIN hjmtj.Notification n ON n.uniqueId = uan.notificationId";

        sql += " INNER JOIN hjmtj.UserAccount ua ON ua.uniqueId = ue.userId";
        sql += " INNER JOIN hjmtj.ElectronicAddress ea ON ea.uniqueId = ua.primaryElectronicAdressId";
        sql += " WHERE a.status != 'DISCONTINUED'";

        sql += " AND n.type = 'ARTICLES_EXPIRED'";

        sql += " AND apr.status = 'PENDING_INACTIVATION'";
        sql += " AND ap.uniqueId IN";
        sql += " (SELECT a1.uniqueId FROM hjmtj.AgreementPricelist a1";
        sql += " INNER JOIN hjmtj.AgreementPricelist a2 ON a1.uniqueID = a2.uniqueId";
        sql += " WHERE a1.validFrom > current_date()";
        sql += " OR a1.validFrom = (SELECT max(validFrom) FROM hjmtj.AgreementPricelist a1";
        sql += " WHERE a1.agreementId = a2.agreementId AND validFrom <= current_date()))";
        sql += " AND (ue.validTo IS NULL OR ue.validTo >= CURDATE())";
        sql += " GROUP BY ea.email, ap.uniqueId) tempTable";
        sql += " GROUP BY email";

        List<Object> emailAndCountList = em.createNativeQuery(sql).getResultList();

        Iterator iterator = emailAndCountList.iterator();

        LOG.log(Level.FINEST, "loop start...");
        while(iterator.hasNext()){
            try {
                Object[] object = (Object[]) iterator.next();
                LOG.log(Level.FINEST, "to: " + object[0].toString() + ", pricelists: " + object[1].toString() );
                emailController.send(object[0].toString(), pricelistToHandleMailSubject, pricelistToHandleMailBody);
            } catch (MessagingException ex) {
                LOG.log(Level.SEVERE, "Failed to send notification message to customer about request for inactivation of pricelistrows");
            }
        }
    }

    /**
     * Approve inactivation of the specified pricelist rows. The customer can do
     * this and only if each row is either in status PENDING_INACTIVATION. An
     * inactive pricelist row cannot change status
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs list of rows to approve inactivation
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> approveInactivateRows(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "approveInactivateRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }

        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);

        // organizations/business levels (customers) that the agreement is shared
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to approve inactivate row on agreement: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }

        // the logged in user must be set as pricelist approver in order to decline rows
        if( !userIsPricelistApprover(organizationUniqueId, agreement, userAPI) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist rows on agreement: {1} but user is not pricelist approver", new Object[] {userAPI.getId(), agreement.getUniqueId()});
            throw generateForbiddenException();
        }

        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to approve inactivate pricelist row: {1}  where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
            }
            if( !AgreementPricelistRow.Status.PENDING_INACTIVATION.equals(pricelistRow.getStatus()) ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to approve inactivate pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.approveInactivateRows.wrongStatus");
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.INACTIVE);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }
        // TODO: send message to supplier
        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    /**
     * Approve inactivation of the specified pricelist rows. The customer can do
     * this and only if each row is either in status PENDING_INACTIVATION. An
     * inactive pricelist row cannot change status
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs list of rows to approve inactivation
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> approveInactivateRowsComment(String comment, long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "approveInactivateRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }

        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);

        // organizations/business levels (customers) that the agreement is shared
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to approve inactivate row on agreement: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }

        // the logged in user must be set as pricelist approver in order to decline rows
        if( !userIsPricelistApprover(organizationUniqueId, agreement, userAPI) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist rows on agreement: {1} but user is not pricelist approver", new Object[] {userAPI.getId(), agreement.getUniqueId()});
            throw generateForbiddenException();
        }

        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to approve inactivate pricelist row: {1}  where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
            }
            if( !AgreementPricelistRow.Status.PENDING_INACTIVATION.equals(pricelistRow.getStatus()) ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to approve inactivate pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.approveInactivateRows.wrongStatus");
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.INACTIVE);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }

        // send mail to supplier
        try
        {
            List<UserRole.RoleName> roleNames = new ArrayList<>();
            roleNames.add(UserRole.RoleName.SupplierAgreementManager);
            List<UserAPI> supplierAgreementManager = null;
            if(userController != null && agreement.getSupplier() != null && agreement.getSupplier().getUniqueId() != null) {
                supplierAgreementManager = userController.getUsersInRolesOnOrganization(agreement.getSupplier().getUniqueId(), roleNames);
            }

            if(supplierAgreementManager != null && !supplierAgreementManager.isEmpty())
            {
                LOG.log( Level.FINEST, "{0} inactivated row(s) accepted, send mail", new Object[] {pricelistRows.size()});
                String priceListNumber = "-";
                if(!pricelistRows.isEmpty() && pricelistRows.get(0) != null) {
                    priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                }

                String selectedSubject = rowsSentApprovedInactivateMailSubject;
                String selectedBody = rowsSentApprovedInactivateMailBody;
                if (pricelistRows.size() == 1) {
                    selectedSubject = rowsSentApprovedInactivateMailSubject_single;
                    selectedBody = rowsSentApprovedInactivateMailBody_single;
                }

                String mailBody  = String.format(selectedBody, agreement.getAgreementName(), agreement.getValidFrom(), agreement.getAgreementNumber(), priceListNumber,agreement.getCustomer().getOrganizationName(), pricelistRows.size());

                if (!comment.isEmpty()) {
                    //mailBody = "Kommentar från kunden:<br> " + comment.replace("QQQ","<br>").replace("@@@","/").replace("£££","\\") + "<br><br>" + mailBody;
                    mailBody = "Kommentar från kunden:<br> " + comment + "<br><br>" + mailBody;
                }

                //check if user has approved to receive mail
                List<?> usersWhoApproved = usersWhoApprovedToReceiveMail("AGREEMENT_PRICELISTEROW_CUSTOMER_APPROVED_INACTIVATION");

                for (UserAPI agreementManager : supplierAgreementManager) {
                    if(agreementManager.getElectronicAddress() != null && agreementManager.getElectronicAddress().getEmail() != null && !agreementManager.getElectronicAddress().getEmail().isEmpty()) {
                        if (usersWhoApproved.contains(convertLongToBigInteger(agreementManager.getId()))) {
                            LOG.log(Level.FINEST, agreementManager.getUsername() + " has chosen to receive mail regarding approved inactivations");
                            emailController.send(agreementManager.getElectronicAddress().getEmail(), selectedSubject, mailBody);
                        }
                        else{
                            LOG.log(Level.FINEST, agreementManager.getUsername() + " has chosen NOT to receive mail regarding approved inactivations");
                        }
                    }
                }
            }

        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on rows, approve inactivated pricelist rows", ex);
        }


        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    /**
     * Decline inactivation of the specified pricelist rows. The customer can do
     * this and only if each row is in status PENDING_INACTIVATION. An
     * inactive pricelist row cannot change status
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs list of rows to send to decline inactivation
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> declineInactivateRows(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "declineInactivateRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }

        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);

        // organizations/business levels (customers) that the agreement is shared
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline inactivate row on agreement: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }

        // the logged in user must be set as pricelist approver in order to decline rows
        if( !userIsPricelistApprover(organizationUniqueId, agreement, userAPI) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist rows on agreement: {1} but user is not pricelist approver", new Object[] {userAPI.getId(), agreement.getUniqueId()});
            throw generateForbiddenException();
        }

        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to decline inactivate pricelist row: {1}  where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
            }
            if( !AgreementPricelistRow.Status.PENDING_INACTIVATION.equals(pricelistRow.getStatus()) ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to decline inactivate pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.declineInactivateRows.wrongStatus");
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.ACTIVE);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }
        // TODO: send message to supplier
        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    /**
     * Decline inactivation of the specified pricelist rows. The customer can do
     * this and only if each row is in status PENDING_INACTIVATION. An
     * inactive pricelist row cannot change status
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs list of rows to send to decline inactivation
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> declineInactivateRowsComment(String comment, long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "declineInactivateRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }

        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);

        // organizations/business levels (customers) that the agreement is shared
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline inactivate row on agreement: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }

        // the logged in user must be set as pricelist approver in order to decline rows
        if( !userIsPricelistApprover(organizationUniqueId, agreement, userAPI) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist rows on agreement: {1} but user is not pricelist approver", new Object[] {userAPI.getId(), agreement.getUniqueId()});
            throw generateForbiddenException();
        }

        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to decline inactivate pricelist row: {1}  where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
            }
            if( !AgreementPricelistRow.Status.PENDING_INACTIVATION.equals(pricelistRow.getStatus()) ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to decline inactivate pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.declineInactivateRows.wrongStatus");
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.ACTIVE);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }

        // send mail to supplier
        try
        {
            List<UserRole.RoleName> roleNames = new ArrayList<>();
            roleNames.add(UserRole.RoleName.SupplierAgreementManager);
            List<UserAPI> supplierAgreementManager = null;
            if(userController != null && agreement.getSupplier() != null && agreement.getSupplier().getUniqueId() != null) {
                supplierAgreementManager = userController.getUsersInRolesOnOrganization(agreement.getSupplier().getUniqueId(), roleNames);
            }

            if(supplierAgreementManager != null && !supplierAgreementManager.isEmpty())
            {
                LOG.log( Level.FINEST, "{0} inactivated row(s) declined, send mail", new Object[] {pricelistRows.size()});
                String priceListNumber = "-";
                if(!pricelistRows.isEmpty() && pricelistRows.get(0) != null) {
                    priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                }

                String selectedSubject = rowsSentDeclinedInactivateMailSubject;
                String selectedBody = rowsSentDeclinedInactivateMailBody;

                if (pricelistRows.size() == 1) {
                    selectedSubject = rowsSentDeclinedInactivateMailSubject_single;
                    selectedBody = rowsSentDeclinedInactivateMailBody_single;
                }

                StringBuilder rowsFormatted = new StringBuilder();
                for (AgreementPricelistRow pricelistRow : pricelistRows)
                {
                    rowsFormatted.append("Artikelnummer: " + pricelistRow.getArticle().getArticleNumber() + "<br/>");
                }

                String mailBody = String.format(selectedBody, pricelistRows.size(), agreement.getAgreementName(),  agreement.getValidFrom(), agreement.getAgreementNumber(), priceListNumber, agreement.getCustomer().getOrganizationName(), rowsFormatted);
                //String mailBody = selectedBody;

                if (!comment.isEmpty())
                    mailBody = "Kommentar från kunden:<br> " + comment + "<br><br>" + mailBody;

                //check if user has approved to receive mail
                List<?> usersWhoApproved = usersWhoApprovedToReceiveMail("AGREEMENT_PRICELISTEROW_CUSTOMER_DECLINED_INACTIVATION");

                for (UserAPI agreementManager : supplierAgreementManager) {
                    if(agreementManager.getElectronicAddress() != null && agreementManager.getElectronicAddress().getEmail() != null && !agreementManager.getElectronicAddress().getEmail().isEmpty()) {
                        if (usersWhoApproved.contains(convertLongToBigInteger(agreementManager.getId()))) {
                            LOG.log(Level.FINEST, agreementManager.getUsername() + " has chosen to receive mail regarding declined inactivations");
                            emailController.send(agreementManager.getElectronicAddress().getEmail(), selectedSubject, mailBody);
                        }
                        else{
                            LOG.log(Level.FINEST, agreementManager.getUsername() + " has chosen NOT to receive mail regarding declined inactivations");
                        }
                    }
                }
            }

        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on rows, decline inactivate pricelist rows", ex);
        }

        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    /**
     * Reopen the specified pricelist rows. Only if each row is in status INACTIVE.
     * An inactive pricelist row cannot change status
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs list of rows to reopen
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> reopenInactivatedRows(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "reopenInactivatedRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }
        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to reopen inactivated pricelist row: {1}  where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
            }
            if( !AgreementPricelistRow.Status.INACTIVE.equals(pricelistRow.getStatus()) ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to reopen inactivated pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.reopenInactivatedRows.wrongStatus");
            }
            if( pricelistRow.getArticle().getStatus() == Product.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to reopen pricelist row: {1} with discontinued article}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.reopenInactivatedRows.articleDiscontinued");
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.CREATED);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }
        // TODO: send message to supplier
        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    /**
     * When an article is discontinued, all rows on all agreements where the
     * given article is referred must be inactivated
     *
     * @param article the article to find rows to inactivate by
     */
    public void handleRowsWhereArticleIsDiscontinued( Article article ) {
        LOG.log(Level.FINEST, "handleRowsWhereArticleIsDiscontinued( article->uniqueId: {0} )", new Object[] {article.getUniqueId()});
        List<AgreementPricelistRow> agreementPricelistRows =
                em.createNamedQuery(AgreementPricelistRow.FIND_BY_ARTICLE).
                setParameter("articleUniqueId", article.getUniqueId()).
                getResultList();
        if( agreementPricelistRows != null && !agreementPricelistRows.isEmpty() ) {
            agreementPricelistRows.forEach((agreementPricelistRow) -> {
                // only update row if it's pricelist is current or future. this is not trivial to find out
                AgreementPricelist currentPricelist = agreementPricelistController.getCurrentPricelist(agreementPricelistRow.getPricelist().getAgreement().getUniqueId());
                AgreementPricelist.Status pricelistStatus = AgreementPricelistMapper.getPricelistStatus(agreementPricelistRow.getPricelist(), currentPricelist);
                AgreementPricelistRow.Status status = agreementPricelistRow.getStatus();
                if( pricelistStatus != AgreementPricelist.Status.PAST && status == AgreementPricelistRow.Status.ACTIVE ) {
                    agreementPricelistRow.setStatus(AgreementPricelistRow.Status.PENDING_INACTIVATION);
                    //agreementPricelistRow.setPrice(BigDecimal.ZERO); (1711)

                    /*
                    //skicka mail till kund
                    try{
                        Agreement agreement = agreementPricelistRow.getPricelist().getAgreement();
                        if( agreement.getCustomerPricelistApprovers() != null && !agreement.getCustomerPricelistApprovers().isEmpty() ) {
                            LOG.log( Level.FINEST, "{0} rows (total) sent for inactivation, send mail", new Object[] {agreementPricelistRows.size()});
                            String selectedSubject = rowsSentForInactivationMailSubject;
                            String selectedBody = rowsSentForInactivationMailBody;
                            //String priceListNumber = "-";
                            //if( agreementPricelistRows.size() > 0 && agreementPricelistRows.get(0) != null) {
                                //priceListNumber = agreementPricelistRows.get(0).getPricelist().getNumber();
                                //priceListNumber = agreementPricelistRow.getPricelist().getNumber();
                            //}
                            //String mailBody = String.format(selectedBody, agreementPricelistRows.size(), agreement.getAgreementName(), agreement.getAgreementNumber(),  priceListNumber, agreement.getSupplier().getOrganizationName());
                            //String mailBody = String.format(selectedBody, 1, agreement.getAgreementName(), agreement.getAgreementNumber(),  priceListNumber, agreement.getSupplier().getOrganizationName());
                            for( UserEngagement userEngagement : agreement.getCustomerPricelistApprovers() ) {
                                emailController.send(userEngagement.getUserAccount().getElectronicAddress().getEmail(), selectedSubject, selectedBody);
                            }
                        }

                    } catch (MessagingException ex) {
                        LOG.log(Level.SEVERE, "Failed to send message on rows sent for inactivation", ex);
                    }

                     */
                }
            });
        }
    }

    public boolean existRowsByArticle( List<Long> articleUniqueIds ) {
        Long count = (Long) em.createNamedQuery(AgreementPricelistRow.COUNT_BY_ARTICLES).
                setParameter("articleUniqueIds", articleUniqueIds).
                getSingleResult();
        return count != null && count > 0;
    }

    /**
     * When an article is changed in its "essence" (like switches based on product)
     * all references to that article must be removed.
     *
     * @param article the article to find rows for to remove
     */
    public void deleteRowsByArticle( Article article ) {
        LOG.log(Level.FINEST, "deleteRowsByArticle( article->uniqueId: {0} )", new Object[] {article.getUniqueId()});
        List<AgreementPricelistRow> agreementPricelistRows =
                em.createNamedQuery(AgreementPricelistRow.FIND_BY_ARTICLE).
                setParameter("articleUniqueId", article.getUniqueId()).
                getResultList();
        if( agreementPricelistRows != null && !agreementPricelistRows.isEmpty() ) {
            Iterator<AgreementPricelistRow> it = agreementPricelistRows.iterator();
            while( it.hasNext() ) {
                em.remove(it.next());
            }
        }
    }

    /**
     * Get customer pricelist rows by article
     *
     * @param organization
     * @param articleUniqueId
     * @return
     */
    public List<AgreementPricelistRow> getOrganizationPricelistRowsByArticle(Organization organization, long articleUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getOrganizationPricelistRowsByArticle( organization -> uniqueId: {0}, articleUniqueId: {1} )", new Object[] {organization.getUniqueId(), articleUniqueId});
        // customers and suppliers should get rows in different statuses
        List<AgreementPricelistRow> agreementPricelistRows = null;
        if( organization.getOrganizationType() == Organization.OrganizationType.CUSTOMER ) {
            // include all statuses except CREATED and CHANGED
            //HJAL-2050 include CHANGED...
            List<AgreementPricelistRow.Status> statuses = new ArrayList<>();
            statuses.add(AgreementPricelistRow.Status.CHANGED);
            statuses.add(AgreementPricelistRow.Status.ACTIVE);
            statuses.add(AgreementPricelistRow.Status.PENDING_APPROVAL);
            statuses.add(AgreementPricelistRow.Status.INACTIVE);
            List<Long> userBusinessLevelIds = agreementController.getUserBusinessLevelIds(userAPI, organization.getUniqueId());
            if( userBusinessLevelIds == null || userBusinessLevelIds.isEmpty() ) {
                // user IS NOT limited by business levels
                agreementPricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_CUSTOMER_AND_ARTICLE, AgreementPricelistRow.class).
                        setParameter("organizationUniqueId", organization.getUniqueId()).
                        setParameter("articleUniqueId", articleUniqueId).
                        setParameter("statuses", statuses).
                        getResultList();
            } else {
                // user IS limited by business levels
                agreementPricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_CUSTOMER_BUSINESS_LEVELS_AND_ARTICLE, AgreementPricelistRow.class).
                        setParameter("businessLevelIds", userBusinessLevelIds).
                        setParameter("articleUniqueId", articleUniqueId).
                        setParameter("statuses", statuses).
                        getResultList();
            }
        } else if( organization.getOrganizationType() == Organization.OrganizationType.SUPPLIER ) {
            agreementPricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_SUPPLIER_AND_ARTICLE, AgreementPricelistRow.class).
                    setParameter("organizationUniqueId", organization.getUniqueId()).
                    setParameter("articleUniqueId", articleUniqueId).
                    getResultList();
        } else if( organization.getOrganizationType() == Organization.OrganizationType.SERVICE_OWNER) {
            agreementPricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_ARTICLE, AgreementPricelistRow.class).
                    setParameter("articleUniqueId", articleUniqueId).
                    getResultList();
        }
        if( agreementPricelistRows != null ) {
            LOG.log(Level.FINEST, "agreementPricelistRows.size() {0}", new Object[] {agreementPricelistRows.size()});
            if (organization.getOrganizationType() != Organization.OrganizationType.SERVICE_OWNER) {
                Iterator<AgreementPricelistRow> agreementPricelistRowIterator = agreementPricelistRows.iterator();
                while (agreementPricelistRowIterator.hasNext()) {
                    AgreementPricelistRow agreementPricelistRow = agreementPricelistRowIterator.next();
                    AgreementPricelist currentPricelistForAgreement = agreementPricelistController.getCurrentPricelist(agreementPricelistRow.getPricelist().getAgreement().getUniqueId());
                    //LOG.log(Level.FINEST, "currentPricelistForAgreement: {0}, agreementPricelistRow.getPricelist(): {1} ", new Object[] {currentPricelistForAgreement.getNumber(), agreementPricelistRow.getPricelist().getNumber()});
                    //visa även för framtida prislista HJAL-2205
                    //if (currentPricelistForAgreement == null || !currentPricelistForAgreement.equals(agreementPricelistRow.getPricelist())) {
                    //    agreementPricelistRowIterator.remove();
                    //}
                }
            }
        }
        return agreementPricelistRows;
    }

    /**
     * Sets overridden warranty quantity unit if it is set by the user.
     *
     * @param pricelistRow the row to update
     * @param pricelistRowAPI user supplied data
     * @throws HjalpmedelstjanstenValidationException if unit is set but does not exist
     */
    void setOverriddenWarrantyQuantityUnit(AgreementPricelistRow pricelistRow, AgreementPricelistRowAPI pricelistRowAPI, boolean isCreate) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setOverriddenWarrantyQuantityUnit( ... )");
        if( isCreate && pricelistRowAPI.getWarrantyQuantityUnit() == null ) {
            // do nothing, default to inherit
        } else {
            if( pricelistRow.isWarrantyQuantityUnitOverridden() ) {
                if( pricelistRowAPI.getWarrantyQuantityUnit() != null ) {
                    CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(pricelistRowAPI.getWarrantyQuantityUnit().getId());
                    if( unit == null ) {
                        LOG.log(Level.FINEST, "cannot set warranty quantity unit: {0} for pricelist row since it does not exist", new Object[] {pricelistRowAPI.getWarrantyQuantityUnit().getId()});
                        throw validationMessageService.generateValidationException("warrantyQuantityUnit", "pricelistrow.warrantyQuantityUnit.notExist");
                    }
                    pricelistRow.setWarrantyQuantityUnit(unit);
                } else {
                    pricelistRow.setWarrantyQuantityUnit(null);
                }
            } else {
                Article article = pricelistRow.getArticle();
                Article.Type articleType = article.getBasedOnProduct() == null ? article.getCategory().getArticleType(): article.getBasedOnProduct().getCategory().getArticleType();

                Agreement agreement = em.find(Agreement.class, pricelistRow.getPricelist().getAgreement().getUniqueId());

                boolean overridden = false;
                if( pricelistRowAPI.getWarrantyQuantityUnit() == null || pricelistRowAPI.getWarrantyQuantityUnit().getId() == null) {
                    // no unit sent in API, check whether it is different from agreement
                    if( articleType == Article.Type.H && pricelistRow.getPricelist().getAgreement().getWarrantyQuantityHUnit() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.I && agreement.getWarrantyQuantityIUnit() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.R && agreement.getWarrantyQuantityRUnit() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.T && agreement.getWarrantyQuantityTUnit() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.Tj && agreement.getWarrantyQuantityTJUnit() != null ) {
                        overridden = true;
                    }
                    if( overridden ) {
                        pricelistRow.setWarrantyQuantityUnit(null);
                        pricelistRow.setWarrantyQuantityUnitOverridden(true);
                    }
                } else {
                    // unit sent in API
                    if( articleType == Article.Type.H ) {
                        if( agreement.getWarrantyQuantityHUnit() == null ||
                                !agreement.getWarrantyQuantityHUnit().getUniqueId().equals(pricelistRowAPI.getWarrantyQuantityUnit().getId()) ) {
                            overridden = true;
                        }
                    } else if( articleType == Article.Type.R ) {
                        if( agreement.getWarrantyQuantityRUnit() == null ||
                                !agreement.getWarrantyQuantityRUnit().getUniqueId().equals(pricelistRowAPI.getWarrantyQuantityUnit().getId()) ) {
                            overridden = true;
                        }
                    } else if( articleType == Article.Type.I ) {
                        if( agreement.getWarrantyQuantityIUnit() == null ||
                                !agreement.getWarrantyQuantityIUnit().getUniqueId().equals(pricelistRowAPI.getWarrantyQuantityUnit().getId()) ) {
                            overridden = true;
                        }
                    } else if( articleType == Article.Type.T ) {
                        if( agreement.getWarrantyQuantityTUnit() == null ||
                                !agreement.getWarrantyQuantityTUnit().getUniqueId().equals(pricelistRowAPI.getWarrantyQuantityUnit().getId()) ) {
                            overridden = true;
                        }
                    } else if( articleType == Article.Type.Tj ) {
                        if( agreement.getWarrantyQuantityTJUnit() == null ||
                                !agreement.getWarrantyQuantityTJUnit().getUniqueId().equals(pricelistRowAPI.getWarrantyQuantityUnit().getId()) ) {
                            overridden = true;
                        }
                    }
                    if( overridden ) {
                        CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(pricelistRowAPI.getWarrantyQuantityUnit().getId());
                        if( unit == null ) {
                            LOG.log(Level.FINEST, "cannot set warranty quantity unit: {0} for pricelist row since it does not exist", new Object[] {pricelistRowAPI.getWarrantyQuantityUnit().getId()});
                            throw validationMessageService.generateValidationException("warrantyQuantityUnit", "pricelistrow.warrantyQuantityUnit.notExist");
                        }
                        pricelistRow.setWarrantyQuantityUnit(unit);
                        pricelistRow.setWarrantyQuantityUnitOverridden(true);
                    }
                }
            }
        }
    }

    public List<Long> getAgreementPricelistArticleUniqueIds(Long pricelistUniqueId) {
        LOG.log(Level.FINEST, "getAgreementPricelistArticleUniqueIds( pricelistUniqueId: {0})", new Object[] {pricelistUniqueId});
        return (List<Long>) em.createNamedQuery(AgreementPricelistRow.GET_ARTICLE_UNIQUE_IDS).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                getResultList();
    }

    public List<String> getAgreementPricelistArticleUniqueNumbers(Long pricelistUniqueId) {
        LOG.log(Level.FINEST, "getAgreementPricelistArticleUniqueNumbers( pricelistUniqueId: {0})", new Object[] {pricelistUniqueId});
        return (List<String>) em.createNamedQuery(AgreementPricelistRow.GET_ARTICLE_UNIQUE_NUMBERS).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                getResultList();
    }

    public List<Long> getAgreementPricelistRowUniqueId(Long articleUniqueId, Long pricelistUniqueId) {
        LOG.log(Level.FINEST, "getAgreementPricelistRowUniqueId( articleUniqueId: {0}, pricelistUniqueId: {1})", new Object[] {articleUniqueId, pricelistUniqueId});
        return (List<Long>) em.createNamedQuery(AgreementPricelistRow.GET_PRICELISTROW_UNIQUE_ID_BY_PRICELIST_AND_ARTICLE).
                setParameter("articleUniqueId", articleUniqueId).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                getResultList();
    }


    /**
     * Get the total number of rows on all pricelists in agreement
     *
     * @param agreementUniqueId the unique id of the agreement
     * @return the total number of rows in the agreement
     */
    public long getNumberOfRowsOnAgreement(long agreementUniqueId) {
        Long count = (Long) em.createNamedQuery(AgreementPricelistRow.COUNT_BY_AGREEMENT).
                setParameter("agreementUniqueId", agreementUniqueId).
                getSingleResult();
        return count == null ? 0: count;
    }


    private void setOverrideWarrantyValidFrom(AgreementPricelistRowAPI pricelistRowAPI, AgreementPricelistRow pricelistRow, Article.Type articleType, UserAPI userAPI, boolean isCreate) throws HjalpmedelstjanstenValidationException {
        if( isCreate && pricelistRowAPI.getWarrantyValidFrom() == null ) {
            // do nothing, default to inherit
        } else {
            if( pricelistRow.isWarrantyValidFromOverridden() ) {
                if( pricelistRowAPI.getWarrantyValidFrom() != null ) {
                    CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(pricelistRowAPI.getWarrantyValidFrom().getCode());
                    if( preventiveMaintenance == null ) {
                        LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[] {userAPI.getId(), pricelistRowAPI.getWarrantyValidFrom()});
                        throw validationMessageService.generateValidationException("warrantyValidFrom", "pricelistrow.warrantyValidFrom.notExist");
                    }
                    pricelistRow.setWarrantyValidFrom(preventiveMaintenance);
                } else {
                    pricelistRow.setWarrantyValidFrom(null);
                }
            } else {
                boolean overridden = false;
                if( pricelistRowAPI.getWarrantyValidFrom() != null ) {
                    CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(pricelistRowAPI.getWarrantyValidFrom().getCode());
                    if( preventiveMaintenance == null ) {
                        LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[] {userAPI.getId(), pricelistRowAPI.getWarrantyValidFrom()});
                        throw validationMessageService.generateValidationException("warrantyValidFrom", "pricelistrow.warrantyValidFrom.notExist");
                    }

                    Agreement agreement = em.find(Agreement.class, pricelistRow.getPricelist().getAgreement().getUniqueId());
                    //agreement.getWarrantyQuantityHUnit().getUniqueId();

                    if( articleType == Article.Type.H ) {

                        if( agreement.getWarrantyValidFromH() == null ) {
                            overridden = true;
                        } else {

                            if( !preventiveMaintenance.getUniqueId().equals(agreement.getWarrantyValidFromH().getUniqueId()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.I ) {
                        if( agreement.getWarrantyValidFromI() == null ) {
                            overridden = true;
                        } else {
                            if( !preventiveMaintenance.getUniqueId().equals(agreement.getWarrantyValidFromI().getUniqueId()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.R ) {
                        if( agreement.getWarrantyValidFromR() == null ) {
                            overridden = true;
                        } else {
                            if( !preventiveMaintenance.getUniqueId().equals(agreement.getWarrantyValidFromR().getUniqueId()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.T ) {
                        if( agreement.getWarrantyValidFromT() == null ) {
                            overridden = true;
                        } else {
                            if( !preventiveMaintenance.getUniqueId().equals(agreement.getWarrantyValidFromT().getUniqueId()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.Tj ) {
                        if( agreement.getWarrantyValidFromTJ() == null ) {
                            overridden = true;
                        } else {
                            if( !preventiveMaintenance.getUniqueId().equals(agreement.getWarrantyValidFromTJ().getUniqueId()) ) {
                                overridden = true;
                            }
                        }
                    }
                    if( overridden ) {
                        pricelistRow.setWarrantyValidFrom(preventiveMaintenance);
                        pricelistRow.setWarrantyValidFromOverridden(true);
                    }
                } else {
                    if( articleType == Article.Type.H && pricelistRow.getPricelist().getAgreement().getWarrantyValidFromH() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.I && pricelistRow.getPricelist().getAgreement().getWarrantyValidFromI() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.R && pricelistRow.getPricelist().getAgreement().getWarrantyValidFromR() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.T && pricelistRow.getPricelist().getAgreement().getWarrantyValidFromT() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.Tj && pricelistRow.getPricelist().getAgreement().getWarrantyValidFromTJ() != null ) {
                        overridden = true;
                    }
                    if( overridden ) {
                        pricelistRow.setWarrantyValidFrom(null);
                        pricelistRow.setWarrantyValidFromOverridden(true);
                    }
                }
            }
        }
    }

    private boolean userIsPricelistApprover(long organizationUniqueId, Agreement agreement, UserAPI userAPI) {
        List<UserEngagement> userEngagements = agreement.getCustomerPricelistApprovers();
        UserEngagementAPI userEngagementAPI = UserController.getUserEngagementAPIByOrganizationId(organizationUniqueId, userAPI.getUserEngagements());
        for( UserEngagement userEngagement : userEngagements ) {
            if( userEngagement.getUniqueId().equals(userEngagementAPI.getId()) ) {
                return true;
            }
        }
        return false;
    }


}
