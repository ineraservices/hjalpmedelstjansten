package se.inera.hjalpmedelstjansten.business;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;
import jakarta.ejb.ApplicationException;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;

/**
 * Custom service validation exception which causes a rollback.
 *
 * @see se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationExceptionMapper
 */
@ApplicationException(rollback = true)
public class HjalpmedelstjanstenValidationException extends Exception {

    private final Set<ErrorMessageAPI> validationMessages = new LinkedHashSet<>();

    public HjalpmedelstjanstenValidationException(String message) {
        super(message);
    }

    public Set<ErrorMessageAPI> getValidationMessages() {
        return validationMessages;
    }

    public void addValidationMessages(Set<ErrorMessageAPI> validationMessages) {
        this.validationMessages.addAll(validationMessages);
    }

    public void addValidationMessage(String field, String message) {
        ErrorMessageAPI errorMessageAPI = new ErrorMessageAPI();
        errorMessageAPI.setField(field);
        errorMessageAPI.setMessage(message);
        validationMessages.add(errorMessageAPI);
    }

    public static ErrorMessageAPI generateValidationMessage(String field, String message) {
        ErrorMessageAPI errorMessageAPI = new ErrorMessageAPI();
        errorMessageAPI.setField(field);
        errorMessageAPI.setMessage(message);
        return errorMessageAPI;
    }

    public void addValidationMessage(String message) {
        addValidationMessage(null, message);
    }


    public boolean hasValidationMessages() {
        return !validationMessages.isEmpty();
    }

    public String validationMessagesAsString() {
        return validationMessagesAsString("\n");
    }

    public String validationMessagesAsString(String delimiter) {
        return validationMessages.stream().map(errorMessageAPI -> errorMessageAPI.getField() + " - " + errorMessageAPI.getMessage()).collect(Collectors.joining(delimiter));
    }

}
