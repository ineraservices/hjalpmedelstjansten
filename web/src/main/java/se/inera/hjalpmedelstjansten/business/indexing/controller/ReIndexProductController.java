package se.inera.hjalpmedelstjansten.business.indexing.controller;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.product.controller.CategoryController;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductMapper;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.Product;

import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 * Reindex a specific product.
 *
 */
@Stateless
public class ReIndexProductController {

    @Inject
    HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    ReIndexArticleController reIndexArticleController;

    @Inject
    CategoryController categoryController;

    @Inject
    ElasticSearchController elasticSearchController;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void reIndexProduct( long organizationUniqueId, long productId, Map<Long, List<CategorySpecificProperty>> categorySpecificPropertyMap ) {
        LOG.log( Level.FINEST, "Fetch product: {0}", new Object[] {productId});
        Session session = em.unwrap(Session.class);
        session.setDefaultReadOnly(true);
        session.setHibernateFlushMode(FlushMode.MANUAL);
        Product product = em.find(Product.class, productId);
        List<CategorySpecificProperty> categorySpecificPropertys = categorySpecificPropertyMap.get(product.getCategory().getUniqueId());
        if( categorySpecificPropertys == null ) {
            categorySpecificPropertys = em.createNamedQuery(CategorySpecificProperty.FIND_BY_CATEGORY).
                        setParameter("categoryUniqueId", product.getCategory().getUniqueId()).
                        getResultList();
            categorySpecificPropertyMap.put(product.getCategory().getUniqueId(), categorySpecificPropertys);
        }
        ProductAPI productAPI = ProductMapper.map(product, true, categorySpecificPropertys);
        elasticSearchController.bulkIndexProduct(productAPI);
    }


}
