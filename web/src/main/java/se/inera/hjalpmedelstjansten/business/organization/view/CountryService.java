package se.inera.hjalpmedelstjansten.business.organization.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.organization.controller.CountryController;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountryAPI;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import java.util.List;
import java.util.logging.Level;


@Stateless
@Path("country")
@Interceptors({ PerformanceLogInterceptor.class })
public class CountryService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    private CountryController countryController;

    /**
     * Return all countries in the database.
     *
     * @return a list of <code>CountryAPI</code>
     */
    @GET
    @SecuredService(permissions = {"organization:view"})
    public List<CVCountryAPI> getAllCountries() {
        LOG.log(Level.FINEST, "getAllCountries()");
        return countryController.getAllCountries();
    }

}
