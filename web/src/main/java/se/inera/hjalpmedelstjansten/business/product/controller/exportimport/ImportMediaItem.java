package se.inera.hjalpmedelstjansten.business.product.controller.exportimport;

import se.inera.hjalpmedelstjansten.model.api.media.MediaAPI;

/**
 * Class for moving media data between import product/article steps read and save
 * 
 */
public class ImportMediaItem {

    private MediaAPI mediaAPI;
    private Long productId;
    private Long articleId;
    private String sheetName;
    private int rowNumber;

    public ImportMediaItem(MediaAPI mediaAPI, 
            Long productId,
            Long articleId, 
            String sheetName, 
            int rowNumber) {
        this.mediaAPI = mediaAPI;
        this.productId = productId;
        this.articleId = articleId;
        this.sheetName = sheetName;
        this.rowNumber = rowNumber;
    }

    public MediaAPI getMediaAPI() {
        return mediaAPI;
    }

    public void setMediaAPI(MediaAPI mediaAPI) {
        this.mediaAPI = mediaAPI;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }
    
    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    
}
