package se.inera.hjalpmedelstjansten.business.media.controller;

import se.inera.hjalpmedelstjansten.model.api.cv.CVDocumentTypeAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVDocumentType;

import java.util.ArrayList;
import java.util.List;

public class DocumentTypeMapper {
    
    public static final List<CVDocumentTypeAPI> map(List<CVDocumentType> documentTypes) {
        if( documentTypes == null ) {
            return null;
        }
        List<CVDocumentTypeAPI> documentTypeAPIs = new ArrayList<>();
        for( CVDocumentType documentType : documentTypes ) {
            documentTypeAPIs.add(map(documentType));
        }
        return documentTypeAPIs;
    }
    
    public static final CVDocumentTypeAPI map(CVDocumentType documentType) {
        if( documentType == null ) {
            return null;
        }
        CVDocumentTypeAPI documentTypeAPI = new CVDocumentTypeAPI();
        documentTypeAPI.setId(documentType.getUniqueId());
        documentTypeAPI.setCode(documentType.getCode());
        documentTypeAPI.setValue(documentType.getValue());
        return documentTypeAPI;
    }
    
}
