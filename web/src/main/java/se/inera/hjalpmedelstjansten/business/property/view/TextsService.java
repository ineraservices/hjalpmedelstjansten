package se.inera.hjalpmedelstjansten.business.property.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.PropertyAPI;
import se.inera.hjalpmedelstjansten.property.TextController;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;

/**
 * REST API for text to frontend
 *
 */
@Stateless
@Path("texts")
@Interceptors({ PerformanceLogInterceptor.class })
public class TextsService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private TextController textController;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @SecuredService(permissions = {"texts:view"})
    public Response getTexts() {
        LOG.log(Level.FINEST, "getTexts()");
        PropertyAPI propertyAPI = new PropertyAPI();
        Map<Object, Object> values = new HashMap<>();
        Properties properties = textController.getTexts();
        if( properties != null ) {
            for( Object key : properties.keySet() ) {
                Object value = properties.get(key);
                values.put(key, value);
            }
        }
        propertyAPI.setValues(values);
        return Response.ok(propertyAPI).build();
    }

}
