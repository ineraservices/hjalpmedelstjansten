package se.inera.hjalpmedelstjansten.business.indexing.controller;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.stream.Collectors;
import jakarta.annotation.PostConstruct;
import jakarta.ejb.ConcurrencyManagement;
import jakarta.ejb.ConcurrencyManagementType;
import jakarta.ejb.DependsOn;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonArrayBuilder;
import jakarta.json.JsonBuilderFactory;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;
import jakarta.json.JsonWriter;
import jakarta.ws.rs.core.MultivaluedMap;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.message.BasicHeader;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.Requests;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.RestHighLevelClientBuilder;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.core.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.xcontent.XContentType;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.ResourceSpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.SearchProductsAndArticlesAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.Product;

/**
 * Contains methods for indexing and querying the elastic search. The following
 * environment variables are read:
 *
 * ELASTICSEARCH_URL: the url to the elastic search server
 *
 * ELASTICSEARCH_INDEX_NAME: the name of the elastic search index to use
 *
 * ELASTICSEARCH_MIN_SCORE: the minimum score for results to return by the elastic search
 * minimum score is only applied if the user enters a specific query, otherwise all
 * hits are returned.
 *
 * ELASTICSEARCH_FUZZINESS: basically means how many "wrong" characters the user can
 * enter in the query, but still receive similar results, e.g. querying "horss" will
 * match "horse". this is used together with the next variable called ELASTICSEARCH_PREFIX_LENGTH.
 * For more info on fuzziness, goto <a href="https://www.elastic.co/guide/en/elasticsearch/guide/current/fuzziness.html">...</a>
 *
 * ELASTICSEARCH_PREFIX_LENGTH: this determines the number of characters the user must
 * enter in a query before fuzziness is applied to the query. to apply fuzziness to a
 * short query generates a lot of false positives
 *
 * ELASTICSEARCH_FIELD_BOOST: this determines the amount of boost to apply to
 * the name, number and category fields. a boost of 2 means a field is twice as important
 * as a field that is not boosted
 *
 */
@Singleton
@Startup
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@DependsOn("PropertyLoader")
public class ElasticSearchController {

    private static final String INDEX_TYPE = "doc";

    private static final String FIELD_NAME_FULLTEXT = "fulltext";

    private static final String FIELD_NAME_TYPE = "type";
    private static final String FIELD_NAME_SUPPLIER_ID = "supplierId";
    private static final String FIELD_NAME_STATUS = "status";
    private static final String FIELD_NAME_BASED_ON_PRODUCT = "basedOnProduct";
    private static final String FIELD_NAME_NAME = "name";
    private static final String FIELD_NAME_NUMBER = "number";
    private static final String FIELD_NAME_CATEGORY_ID = "categoryId";
    private static final String FIELD_NAME_CATEGORY_NAME = "categoryName";
    private static final String FIELD_NAME_CATEGORY_TYPE = "categoryType";
    private static final String FIELD_NAME_CATEGORY_CODE = "categoryCode";
    private static final String FIELD_NAME_EXTENDED_CATEGORIES_PROPERTIES = "extendedCategories";
    private static final String FIELD_NAME_MANUFACTURER = "manufacturer";
    private static final String FIELD_NAME_MANUFACTURER_NUMBER = "manufacturerNumber";
    private static final String FIELD_NAME_COLOR = "color";
    private static final String FIELD_NAME_SUPPLEMENTED_INFORMATION = "supplementedInformation";
    private static final String FIELD_NAME_SUPPLIER_NAME = "supplierName";
    private static final String FIELD_NAME_GTIN_13 = "gtin13";
    private static final String FIELD_NAME_CUSTOMER_UNIQUE = "customerUnique";
    private static final String FIELD_NAME_RESOURCE_SPECIFIC_PROPERTY_ID = "id";
    private static final String FIELD_NAME_RESOURCE_SPECIFIC_PROPERTY_VALUE = "value";
    private static final String FIELD_NAME_RESOURCE_SPECIFIC_TEXT_PROPERTIES = "textProperties";
    private static final String FIELD_NAME_RESOURCE_SPECIFIC_DECIMAL_PROPERTIES = "decimalProperties";
    private static final String FIELD_NAME_RESOURCE_SPECIFIC_VALUELIST_SINGLE_PROPERTIES = "valuelistSingleProperties";
    private static final String FIELD_NAME_RESOURCE_SPECIFIC_VALUELIST_MULTIPLE_PROPERTIES = "valuelistMultipleProperties";
    private static final String FIELD_NAME_RESOURCE_SPECIFIC_INTERVAL_PROPERTIES = "intervalProperties";
    private static final String FIELD_NAME_RESOURCE_SPECIFIC_INTERVAL_RANGE_PROPERTIES = "interval";

    private static final String TYPE_PRODUCT = "PRODUCT";
    private static final String TYPE_ARTICLE = "ARTICLE";

    @Inject
    HjmtLogger LOG;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    OrganizationController organizationController;

    @Inject
    ArticleController articleController;

    @Inject
    ProductController productController;

    @Inject
    AuthHandler authHandler;

    private boolean enabled = false;
    private RestHighLevelClient restHighLevelClient;
    private BulkProcessor bulkProcessor;
    private String indexSource;
    private String indexName;
    private Float minScore;
    private int prefixLength;
    private int fuzziness;
    private float fieldBoost;

    public void indexProduct(ProductAPI productAPI) {
        LOG.log(Level.FINEST, "indexProduct( product->uniqueId: {0}, enabled: {1} )", new Object[] {productAPI.getId(), enabled});
        if( !enabled ) {
            LOG.log( Level.WARNING, "Elastic search is not enabled, search is not possible" );
            return;
        }
        try {
            byte[] source = toByteArray(toJsonObject(productAPI));
            restHighLevelClient.index(new IndexRequest(indexName, INDEX_TYPE, productAPI.getId().toString()).source(source, XContentType.JSON), RequestOptions.DEFAULT);
            //restHighLevelClient.index(new IndexRequest(indexName, INDEX_TYPE).source(source, XContentType.JSON), RequestOptions.DEFAULT);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Failed to index json", ex);
        }
    }

    public void indexArticle(ArticleAPI articleAPI) {
        LOG.log(Level.FINEST, "indexArticle( article->uniqueId: {0}, enabled: {1} )", new Object[] {articleAPI.getId(), enabled});
        if( !enabled ) {
            LOG.log( Level.WARNING, "Elastic search is not enabled, search is not possible" );
            return;
        }
        try {
            byte[] source = toByteArray(toJsonObject(articleAPI));
            restHighLevelClient.index(new IndexRequest(indexName, INDEX_TYPE, articleAPI.getId().toString()).source(source, XContentType.JSON), RequestOptions.DEFAULT);
            //restHighLevelClient.index(new IndexRequest(indexName, INDEX_TYPE).source(source, XContentType.JSON), RequestOptions.DEFAULT);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Failed to index json", ex);
        }
    }

    public void updateIndexProduct(ProductAPI productAPI) {
        LOG.log(Level.FINEST, "updateIndexProduct( product->uniqueId: {0}, enabled: {1} )", new Object[] {productAPI.getId(), enabled});
        if( !enabled ) {
            LOG.log( Level.WARNING, "Elastic search is not enabled, search is not possible" );
            return;
        }
        try {
            byte[] source = toByteArray(toJsonObject(productAPI));
            IndexRequest indexRequest = new IndexRequest(indexName, INDEX_TYPE, productAPI.getId().toString()).source(source, XContentType.JSON);
            //IndexRequest indexRequest = new IndexRequest(indexName).source(source, XContentType.JSON);
            restHighLevelClient.update(
                    new UpdateRequest(indexName, productAPI.getId().toString()).
                            doc(source, XContentType.JSON).
                            upsert(indexRequest),
                    RequestOptions.DEFAULT);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Failed to index json", ex);
        }
    }

    public void updateIndexArticle(ArticleAPI articleAPI) {
        LOG.log(Level.FINEST, "updateIndexArticle( article->uniqueId: {0}, enabled: {1} )", new Object[] {articleAPI.getId(), enabled});
        //LOG.log(Level.INFO, "updateIndexArticle( article->uniqueId: {0}, enabled: {1} )", new Object[] {articleAPI.getId(), enabled});
        if( !enabled ) {
            LOG.log( Level.WARNING, "Elastic search is not enabled, search is not possible" );
            return;
        }
        try {
            byte[] source = toByteArray(toJsonObject(articleAPI));
            IndexRequest indexRequest = new IndexRequest(indexName, INDEX_TYPE, articleAPI.getId().toString()).source(source, XContentType.JSON);
            //IndexRequest indexRequest = new IndexRequest(indexName, INDEX_TYPE).source(source, XContentType.JSON);
            restHighLevelClient.update(new UpdateRequest(indexName, articleAPI.getId().toString()).
                    doc(source, XContentType.JSON).upsert(indexRequest),
                    RequestOptions.DEFAULT);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Failed to index json", ex);
        }
    }

    public void bulkIndexProduct(ProductAPI productAPI) {
        LOG.log(Level.FINEST, "bulkProduct( productAPI->uniqueId: {0}, enabled: {1} )", new Object[] {productAPI.getId(), enabled});
        if( !enabled ) {
            LOG.log( Level.WARNING, "Elastic search is not enabled, search is not possible" );
            return;
        }
        try {
            byte[] source = toByteArray(toJsonObject(productAPI));
            IndexRequest req = Requests.indexRequest(indexName)
                .id(productAPI.getId().toString())
                .source(source, XContentType.JSON);
            bulkProcessor.add(req);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Failed to index json", ex);
        }   catch(NullPointerException e) {
            LOG.log(Level.FINEST, "Nullpointer exception in ProductAPI: " + productAPI.getProductName());
        }
    }

    public void bulkIndexArticle(ArticleAPI articleAPI) {
        LOG.log(Level.FINEST, "bulkArticle( articleAPI->uniqueId: {0}, enabled: {1} )", new Object[] {articleAPI.getId(), enabled});
        //LOG.log(Level.INFO, "bulkArticle( articleAPI->uniqueId: {0}, enabled: {1} )", new Object[] {articleAPI.getId(), enabled});
        if( !enabled ) {
            LOG.log( Level.WARNING, "Elastic search is not enabled, search is not possible" );
            LOG.log(Level.FINEST, "ArticleAPI: " + articleAPI.getArticleName() + articleAPI.getBasedOnProduct() + articleAPI.getStatus());
            return;
        }
        try {
            //LOG.log(Level.INFO, "bulkArticle: toByteArray...");
            byte[] source = toByteArray(toJsonObject(articleAPI));

            IndexRequest req = Requests.indexRequest(indexName).id(articleAPI.getId().toString()).source(source, XContentType.JSON);

            //LOG.log(Level.INFO, "bulkArticle: bulkProcessor.add...");
            bulkProcessor.add(req);
            //LOG.log(Level.INFO, "bulkArticle done!");
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Failed to index json", ex);
        } catch(NullPointerException e) {
            LOG.log(Level.FINEST, "Nullpointer exception in ArticleAPI: " + articleAPI.getArticleName());
        }
    }

    public void bulkUpdateIndexProduct(ProductAPI productAPI) {
        LOG.log(Level.FINEST, "bulkUpdateIndexProduct( productAPI->uniqueId: {0}, enabled: {1} )", new Object[] {productAPI.getId(), enabled});
        if( !enabled ) {
            LOG.log( Level.WARNING, "Elastic search is not enabled, search is not possible" );
            return;
        }
        try {
            byte[] source = toByteArray(toJsonObject(productAPI));
            IndexRequest req = Requests.indexRequest(indexName)
                .id(productAPI.getId().toString())
                .source(source, XContentType.JSON);
            bulkProcessor.add(req);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Failed to index json", ex);
        }
    }

    public void bulkUpdateIndexArticle(ArticleAPI articleAPI) {
        LOG.log(Level.FINEST, "bulkUpdateIndexArticle( articleAPI->uniqueId: {0}, enabled: {1} )", new Object[] {articleAPI.getId(), enabled});
        if( !enabled ) {
            LOG.log( Level.WARNING, "Elastic search is not enabled, search is not possible" );
            return;
        }
        try {
            byte[] source = toByteArray(toJsonObject(articleAPI));
            IndexRequest req = Requests.indexRequest(indexName)
                .id(articleAPI.getId().toString())
                .source(source, XContentType.JSON);
            bulkProcessor.add(req);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Failed to index json", ex);
        }
    }

    public void removeIndexProduct(ProductAPI productAPI) {
        LOG.log(Level.FINEST, "removeIndexProduct( productAPI->uniqueId: {0}, enabled: {1} )", new Object[] {productAPI.getId(), enabled});
        if( !enabled ) {
            LOG.log( Level.WARNING, "Elastic search is not enabled, search is not possible" );
            return;
        }
        try {
            byte[] source = toByteArray(toJsonObject(productAPI));
            restHighLevelClient.delete(new DeleteRequest(indexName, INDEX_TYPE, productAPI.getId().toString()), RequestOptions.DEFAULT);
            //restHighLevelClient.delete(new DeleteRequest(indexName,  productAPI.getId().toString()), RequestOptions.DEFAULT);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Failed to index json", ex);
        }
    }

    public void removeIndexArticle(ArticleAPI articleAPI) {
        LOG.log(Level.FINEST, "removeIndexArticle( articleAPI->uniqueId: {0}, enabled: {1} )", new Object[] {articleAPI.getId(), enabled});
        if( !enabled ) {
            LOG.log( Level.WARNING, "Elastic search is not enabled, search is not possible" );
            return;
        }
        try {
            byte[] source = toByteArray(toJsonObject(articleAPI));
            restHighLevelClient.delete(new DeleteRequest(indexName, INDEX_TYPE, articleAPI.getId().toString()), RequestOptions.DEFAULT);
            //restHighLevelClient.delete(new DeleteRequest(indexName, articleAPI.getId().toString()), RequestOptions.DEFAULT);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Failed to index json", ex);
        }
    }

//    public void bulkRemoveIndexProduct(ProductAPI productAPI) {
//        LOG.log(Level.FINEST, "bulkRemoveIndexProduct( productAPI->uniqueId: {0}, enabled: {1} )", new Object[] {productAPI.getId(), enabled});
//        if( !enabled ) {
//            LOG.log( Level.WARNING, "Elastic search is not enabled, search is not possible" );
//            return;
//        }
//        try {
//            byte[] source = toByteArray(toJsonObject(productAPI));
//            bulkProcessor.add(new DeleteRequest(indexName, INDEX_TYPE, productAPI.getId().toString()), RequestOptions.DEFAULT);
//        } catch (IOException ex) {
//            LOG.log(Level.SEVERE, "Failed to index json", ex);
//        }
//    }
//
//    public void bulkRemoveIndexArticle(ArticleAPI articleAPI) {
//        LOG.log(Level.FINEST, "bulkRemoveIndexArticle( articleAPI->uniqueId: {0}, enabled: {1} )", new Object[] {articleAPI.getId(), enabled});
//        if( !enabled ) {
//            LOG.log( Level.WARNING, "Elastic search is not enabled, search is not possible" );
//            return;
//        }
//        try {
//            byte[] source = toByteArray(toJsonObject(articleAPI));
//            bulkProcessor.add(new DeleteRequest(indexName, INDEX_TYPE, articleAPI.getId().toString()), RequestOptions.DEFAULT);
//        } catch (IOException ex) {
//            LOG.log(Level.SEVERE, "Failed to index json", ex);
//        }
//    }

    public SearchDTO searchOrganization(String query,
                                        long organizationId,
                                        boolean includeProducts,
                                        boolean includeArticles,
                                        String sortOrder,
                                        String sortType,
                                        List<Article.Type> articleTypes,
                                        List<Long> excludeIds,
                                        Product.Status status,
                                        int limit,
                                        int offset,
                                        boolean includeOnlyTiso) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "search( limit: {0}, offset: {1}, enabled: {2} )", new Object[]{limit, offset, enabled});
        if (!enabled) {
            LOG.log(Level.WARNING, "Elastic search is not enabled, search is not possible");
            return new SearchDTO(0L, new ArrayList<>());
        }
        try {
            BoolQueryBuilder boolQueryBuilder = QueryBuilders.
                    boolQuery();

            // must match organization (supplier)
            QueryBuilder supplierIdBuilder = QueryBuilders.
                        termQuery(FIELD_NAME_SUPPLIER_ID, organizationId);
            boolQueryBuilder.must(supplierIdBuilder);

            // include both articles and products?
            if( includeArticles != includeProducts ) {
                boolQueryBuilder.must(QueryBuilders.
                        termQuery(FIELD_NAME_TYPE, includeArticles ? TYPE_ARTICLE: TYPE_PRODUCT));
            }

            if(includeOnlyTiso)
            {
                boolQueryBuilder.must(QueryBuilders.wildcardQuery(FIELD_NAME_CATEGORY_CODE, "*"));
            }

            if( excludeIds != null && !excludeIds.isEmpty() ) {
                boolQueryBuilder.mustNot(QueryBuilders.termsQuery("_id", excludeIds));
            }

            if( status != null ) {
                boolQueryBuilder.must(QueryBuilders.
                        termQuery(FIELD_NAME_STATUS, status.toString()));
            }

            // if search query, then it must match that, otherwise match all
            if( query != null && !query.isEmpty() ) {
                QueryBuilder fulltextQueryBuilder = QueryBuilders.
                    multiMatchQuery(query).
                    field(FIELD_NAME_FULLTEXT). // no specific boost since all matches in fulltext are equal
                    field(FIELD_NAME_NAME, fieldBoost). // boost article/product name field
                    field(FIELD_NAME_NUMBER, fieldBoost). // boost article/product number field
                    field(FIELD_NAME_CATEGORY_CODE, fieldBoost). // boost category code field
                    fuzziness(fuzziness).
                    prefixLength(prefixLength).
                    type(MultiMatchQueryBuilder.Type.MOST_FIELDS).
                    operator(Operator.AND);
                boolQueryBuilder.must(fulltextQueryBuilder);
            }

            // if article types, it must match one of
            if( includeArticles && articleTypes != null && !articleTypes.isEmpty() ) {
                BoolQueryBuilder mustArticletypesBoolQueryBuilder = QueryBuilders.boolQuery();
                BoolQueryBuilder shouldArticletypesBoolQueryBuilder = QueryBuilders.boolQuery();
                for( Article.Type articleType : articleTypes ) {
                    shouldArticletypesBoolQueryBuilder.should(QueryBuilders.termQuery(FIELD_NAME_CATEGORY_TYPE, articleType.toString()));
                }
                shouldArticletypesBoolQueryBuilder.minimumShouldMatch(1);
                //HJAL-2183
                if (includeProducts){
                    shouldArticletypesBoolQueryBuilder.should(QueryBuilders.termQuery(FIELD_NAME_TYPE, TYPE_PRODUCT));
                }
                mustArticletypesBoolQueryBuilder.must(shouldArticletypesBoolQueryBuilder);
                boolQueryBuilder.must(mustArticletypesBoolQueryBuilder);
            }

            //HJAL-2247 inga artikeltyper valda == alla artikeltyper valda
            if( includeArticles && (articleTypes == null || articleTypes.isEmpty()) ) {
                BoolQueryBuilder mustArticletypesBoolQueryBuilder = QueryBuilders.boolQuery();
                BoolQueryBuilder shouldArticletypesBoolQueryBuilder = QueryBuilders.boolQuery();

                Article.Type[] allArticleTypes = Article.Type.values();
                for( Article.Type articleType : allArticleTypes ) {
                    shouldArticletypesBoolQueryBuilder.should(QueryBuilders.termQuery(FIELD_NAME_CATEGORY_TYPE, articleType.toString()));
                }
                shouldArticletypesBoolQueryBuilder.minimumShouldMatch(1);
                //HJAL-2183
                if (includeProducts){
                    shouldArticletypesBoolQueryBuilder.should(QueryBuilders.termQuery(FIELD_NAME_TYPE, TYPE_PRODUCT));
                }
                mustArticletypesBoolQueryBuilder.must(shouldArticletypesBoolQueryBuilder);
                boolQueryBuilder.must(mustArticletypesBoolQueryBuilder);
            }


            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().
                    query(boolQueryBuilder).
                    from(offset).
                    trackTotalHits(true).
                    size(limit);
            // only apply minimum score if user entered anything in the query field
            if( query != null && !query.isEmpty() ) {
                searchSourceBuilder.minScore(minScore);
            }
            // if user enters something in the query field we default to order by score, otherwise
            // we sort by article/product number
            if( query == null || query.isEmpty()) {
                //searchSourceBuilder.sort(new FieldSortBuilder(FIELD_NAME_NAME + ".keyword").order(SortOrder.ASC));
                searchSourceBuilder.sort(new FieldSortBuilder(FIELD_NAME_NUMBER + ".keyword").order(SortOrder.ASC));
            }
            SearchRequest searchRequest = new SearchRequest(indexName).source(searchSourceBuilder);
            SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            return mapSearchResponse(searchResponse);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Failed to search", ex);
            throw validationMessageService.generateValidationException("category", "search.productsandarticles.generalFailure");
        }
    }

    public SearchDTO search(String query,
                            Category category,
                            Product.Status status,
                            List<CategorySpecificProperty> categorySpecificPropertys,
                            MultivaluedMap<String, String> queryParameters,
                            String sortType,
                            String sortOrder,
                            boolean includeProducts,
                            boolean includeArticles,
                            List<Article.Type> articleTypes,
                            int supplier,
                            int limit,
                            int offset,
                            List<Article> excludeTheseArticles,
                            boolean includeOnlyTiso) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "search( limit: {0}, offset: {1}, enabled: {2}, sortType: {3}, sortOrder: {4} )", new Object[]{limit, offset, enabled, sortType, sortOrder});
        if (!enabled) {
            LOG.log(Level.WARNING, "Elastic search is not enabled, search is not possible");
            return new SearchDTO(0L, new ArrayList<>());
        }
        try {
            SearchResponse searchResponse;
            if( isEmptyQuery(query, category, includeProducts, includeArticles) ) {
                return new SearchDTO(0L, new ArrayList<>());
            } else {
                if( (query == null || query.length() < 3) ) {
                    // must be at least 3 characters for search
                    query = null;
                }
                searchResponse = getAllProductsAndArticlesComplexResults(query, category, status, categorySpecificPropertys, queryParameters, sortType, sortOrder, includeProducts, includeArticles, articleTypes, supplier, offset, limit, excludeTheseArticles, includeOnlyTiso);
            }
            UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
            Organization organization = organizationController.getOrganizationFromLoggedInUser(userAPI);
            boolean isCustomer = organization.getOrganizationType() == Organization.OrganizationType.CUSTOMER;
            if(isCustomer)
            {
                return mapSearchResponseCustomer(searchResponse);
            }
            return mapSearchResponse(searchResponse);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Failed to search", ex);
            throw validationMessageService.generateValidationException("category", "search.productsandarticles.generalFailure");
        }
    }

    private boolean isEmptyQuery(String query, Category category, boolean includeProducts, boolean includeArticles ) {
        return (query == null || query.isEmpty()) && category == null && !includeProducts && !includeArticles;
    }

    public boolean indexExists() {
        LOG.log(Level.FINEST, "indexExists( indexName: {0}, enabled: {1} )", new Object[] {indexName, enabled});
        if( !enabled ) {
            LOG.log( Level.WARNING, "Elastic search is not enabled, search is not possible" );
            return false;
        }
        try {
            // create index
            GetIndexRequest getIndexRequest = new GetIndexRequest().
                    indices(indexName);
            return restHighLevelClient.indices().exists(getIndexRequest, RequestOptions.DEFAULT);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Failed to find index", ex);
            return false;
        }
    }

    public boolean deleteIndex() {
        LOG.log(Level.FINEST, "deleteIndex( indexName: {0}, enabled: {1} )", new Object[] {indexName, enabled});
        if( !enabled ) {
            LOG.log( Level.WARNING, "Elastic search is not enabled, search is not possible" );
            return false;
        }
        try {
            // create index
            DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest(indexName);
            restHighLevelClient.indices().delete(deleteIndexRequest, RequestOptions.DEFAULT);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Failed to delete index", ex);
            return false;
        }
        return true;
    }

    public boolean createIndex() {
        LOG.log(Level.FINEST, "createIndex( indexName: {0}, enabled: {1} )", new Object[] {indexName, enabled});
        if( !enabled ) {
            LOG.log( Level.WARNING, "Elastic search is not enabled, search is not possible" );
            return false;
        }
        try {
            // create index
            CreateIndexRequest createIndexRequest = new CreateIndexRequest(indexName).
                    source(indexSource, XContentType.JSON);
            restHighLevelClient.
                    indices().
                    create(createIndexRequest, RequestOptions.DEFAULT);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Failed to create index", ex);
            return false;
        }
        return true;
    }

    private byte[] toByteArray(JsonObject jsonObject) throws IOException {
        try(ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JsonWriter jsonWriter = Json.createWriter(byteArrayOutputStream) ) {
            jsonWriter.writeObject(jsonObject);
            jsonWriter.close();
            byteArrayOutputStream.flush();
            return byteArrayOutputStream.toByteArray();
        }
    }

    private JsonObject toJsonObject( ProductAPI productAPI ) {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder().
                add(FIELD_NAME_TYPE, TYPE_PRODUCT).
                add(FIELD_NAME_SUPPLIER_ID, productAPI.getOrganizationId()).
                add(FIELD_NAME_SUPPLIER_NAME, productAPI.getOrganizationName()).
                add(FIELD_NAME_STATUS, productAPI.getStatus()).
                add(FIELD_NAME_NAME, productAPI.getProductName()).
                add(FIELD_NAME_NUMBER, productAPI.getProductNumber()).
                add(FIELD_NAME_SUPPLIER_NAME, productAPI.getOrganizationName()).
                add(FIELD_NAME_CATEGORY_ID, productAPI.getCategory().getId()).
                add(FIELD_NAME_CATEGORY_NAME, productAPI.getCategory().getName()).
                add(FIELD_NAME_CATEGORY_TYPE, productAPI.getCategory().getArticleType().toString()).
                add(FIELD_NAME_CUSTOMER_UNIQUE, productAPI.isCustomerUnique());
        if( productAPI.getCategory().getCode() != null ) {
            jsonObjectBuilder.add(FIELD_NAME_CATEGORY_CODE, productAPI.getCategory().getCode());
        }
        if( productAPI.getManufacturer() != null && !productAPI.getManufacturer().isEmpty() ) {
            jsonObjectBuilder.add(FIELD_NAME_MANUFACTURER, productAPI.getManufacturer());
        }
        if( productAPI.getManufacturerProductNumber() != null && !productAPI.getManufacturerProductNumber().isEmpty() ) {
            jsonObjectBuilder.add(FIELD_NAME_MANUFACTURER_NUMBER, productAPI.getManufacturerProductNumber());
        }
        if( productAPI.getColor() != null && !productAPI.getColor().isEmpty() ) {
            jsonObjectBuilder.add(FIELD_NAME_COLOR, productAPI.getColor());
        }
        if( productAPI.getSupplementedInformation() != null && !productAPI.getSupplementedInformation().isEmpty() ) {
            jsonObjectBuilder.add(FIELD_NAME_SUPPLEMENTED_INFORMATION, productAPI.getSupplementedInformation());
        }
        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = productAPI.getCategoryPropertys();
        addResourceSpecificPropertiesJsonObjects(jsonObjectBuilder, resourceSpecificPropertyAPIs);

        addExtendedCategories(jsonObjectBuilder, productAPI.getExtendedCategories());

        return jsonObjectBuilder.build();
    }

    private JsonObject toJsonObject( ArticleAPI articleAPI ) {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder().
                add(FIELD_NAME_TYPE, TYPE_ARTICLE).
                add(FIELD_NAME_SUPPLIER_ID, articleAPI.getOrganizationId()).
                add(FIELD_NAME_SUPPLIER_NAME, articleAPI.getOrganizationName()).
                add(FIELD_NAME_STATUS, articleAPI.getStatus()).
                add(FIELD_NAME_NAME, articleAPI.getArticleName()).
                add(FIELD_NAME_NUMBER, articleAPI.getArticleNumber()).
                add(FIELD_NAME_SUPPLIER_NAME, articleAPI.getOrganizationName()).
                add(FIELD_NAME_CATEGORY_ID, articleAPI.getCategory().getId()).
                add(FIELD_NAME_CATEGORY_NAME, articleAPI.getCategory().getName()).
                add(FIELD_NAME_CATEGORY_TYPE, articleAPI.getCategory().getArticleType().toString()).
                add(FIELD_NAME_CUSTOMER_UNIQUE, articleAPI.isCustomerUnique());
        if( articleAPI.getCategory().getCode() != null ) {
            jsonObjectBuilder.add(FIELD_NAME_CATEGORY_CODE, articleAPI.getCategory().getCode());
        }
        if( articleAPI.getManufacturer() != null && !articleAPI.getManufacturer().isEmpty() ) {
            jsonObjectBuilder.add(FIELD_NAME_MANUFACTURER, articleAPI.getManufacturer());
        }
        if( articleAPI.getManufacturerArticleNumber() != null && !articleAPI.getManufacturerArticleNumber().isEmpty() ) {
            jsonObjectBuilder.add(FIELD_NAME_MANUFACTURER_NUMBER, articleAPI.getManufacturerArticleNumber());
        }
        if( articleAPI.getColor() != null && !articleAPI.getColor().isEmpty() ) {
            jsonObjectBuilder.add(FIELD_NAME_COLOR, articleAPI.getColor());
        }
        if( articleAPI.getSupplementedInformation() != null && !articleAPI.getSupplementedInformation().isEmpty() ) {
            jsonObjectBuilder.add(FIELD_NAME_SUPPLEMENTED_INFORMATION, articleAPI.getSupplementedInformation());
        }
        if( articleAPI.getGtin() != null && !articleAPI.getGtin().isEmpty() ) {
            jsonObjectBuilder.add(FIELD_NAME_GTIN_13, articleAPI.getGtin());
        }
        if (articleAPI.getBasedOnProduct() != null){
            jsonObjectBuilder.add(FIELD_NAME_BASED_ON_PRODUCT, articleAPI.getBasedOnProduct().getProductName());
        }
        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = articleAPI.getCategoryPropertys();
        addResourceSpecificPropertiesJsonObjects(jsonObjectBuilder, resourceSpecificPropertyAPIs);

        addExtendedCategories(jsonObjectBuilder, articleAPI.getExtendedCategories());

        return jsonObjectBuilder.build();
    }

        private void addExtendedCategories(JsonObjectBuilder jsonObjectBuilder, List<CategoryAPI> extendedCategories) {
        if( extendedCategories != null && !extendedCategories.isEmpty() ) {
            JsonBuilderFactory jsonBuilderFactory = Json.createBuilderFactory(null);
            JsonArrayBuilder jsonExtendedCategoriesArrayBuilder = jsonBuilderFactory.createArrayBuilder();
            for( CategoryAPI categoryAPI : extendedCategories ) {
                JsonObjectBuilder jsonExtendedCategoryObjectBuilder = jsonBuilderFactory.createObjectBuilder().
                                add(FIELD_NAME_CATEGORY_NAME, categoryAPI.getName());
                if( categoryAPI.getCode() != null ) {
                    jsonExtendedCategoryObjectBuilder.add(FIELD_NAME_RESOURCE_SPECIFIC_PROPERTY_ID, categoryAPI.getCode());
                }
                jsonExtendedCategoriesArrayBuilder.add(jsonExtendedCategoryObjectBuilder.build());
            }
            jsonObjectBuilder.add(FIELD_NAME_EXTENDED_CATEGORIES_PROPERTIES, jsonExtendedCategoriesArrayBuilder.build());
        }
    }

    private void addResourceSpecificPropertiesJsonObjects(JsonObjectBuilder jsonObjectBuilder, List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs) {
        if( resourceSpecificPropertyAPIs != null && !resourceSpecificPropertyAPIs.isEmpty() ) {
            JsonBuilderFactory jsonBuilderFactory = Json.createBuilderFactory(null);
            JsonArrayBuilder jsonTextPropertiesArrayBuilder = jsonBuilderFactory.createArrayBuilder();
            JsonArrayBuilder jsonDecimalPropertiesArrayBuilder = jsonBuilderFactory.createArrayBuilder();
            JsonArrayBuilder jsonIntervalPropertiesArrayBuilder = jsonBuilderFactory.createArrayBuilder();
            JsonArrayBuilder jsonValuelistSinglePropertiesArrayBuilder = jsonBuilderFactory.createArrayBuilder();
            JsonArrayBuilder jsonValuelistMultiplePropertiesArrayBuilder = jsonBuilderFactory.createArrayBuilder();
            for( ResourceSpecificPropertyAPI resourceSpecificPropertyAPI : resourceSpecificPropertyAPIs ) {
                if( CategorySpecificProperty.Type.TEXTFIELD.toString().equals(resourceSpecificPropertyAPI.getProperty().getType()) && resourceSpecificPropertyAPI.getTextValue() != null  ) {
                    jsonTextPropertiesArrayBuilder.add(
                            jsonBuilderFactory.createObjectBuilder().
                                    add(FIELD_NAME_RESOURCE_SPECIFIC_PROPERTY_ID, resourceSpecificPropertyAPI.getProperty().getId()).
                                    add(FIELD_NAME_RESOURCE_SPECIFIC_PROPERTY_VALUE, resourceSpecificPropertyAPI.getTextValue())
                    );
                } else if( CategorySpecificProperty.Type.DECIMAL.toString().equals(resourceSpecificPropertyAPI.getProperty().getType()) && resourceSpecificPropertyAPI.getDecimalValue()!= null  ) {
                    jsonDecimalPropertiesArrayBuilder.add(
                            jsonBuilderFactory.createObjectBuilder().
                                    add(FIELD_NAME_RESOURCE_SPECIFIC_PROPERTY_ID, resourceSpecificPropertyAPI.getProperty().getId()).
                                    add(FIELD_NAME_RESOURCE_SPECIFIC_PROPERTY_VALUE, resourceSpecificPropertyAPI.getDecimalValue())
                    );
                } else if( CategorySpecificProperty.Type.VALUELIST_SINGLE.toString().equals(resourceSpecificPropertyAPI.getProperty().getType()) && resourceSpecificPropertyAPI.getSingleListValue() != null  ) {
                    jsonValuelistSinglePropertiesArrayBuilder.add(
                            jsonBuilderFactory.createObjectBuilder().
                                    add(FIELD_NAME_RESOURCE_SPECIFIC_PROPERTY_ID, resourceSpecificPropertyAPI.getProperty().getId()).
                                    add(FIELD_NAME_RESOURCE_SPECIFIC_PROPERTY_VALUE, resourceSpecificPropertyAPI.getSingleListValue())
                    );
                } else if( CategorySpecificProperty.Type.VALUELIST_MULTIPLE.toString().equals(resourceSpecificPropertyAPI.getProperty().getType()) && resourceSpecificPropertyAPI.getMultipleListValue() != null && !resourceSpecificPropertyAPI.getMultipleListValue().isEmpty()  ) {
                    JsonArrayBuilder multipleValues = jsonBuilderFactory.createArrayBuilder();
                    JsonObjectBuilder valuelistMultipleJsonObjectBuilder = jsonBuilderFactory.createObjectBuilder().
                                    add(FIELD_NAME_RESOURCE_SPECIFIC_PROPERTY_ID, resourceSpecificPropertyAPI.getProperty().getId());
                    for( Long value : resourceSpecificPropertyAPI.getMultipleListValue() ) {
                        multipleValues.add(value);
                    }
                    valuelistMultipleJsonObjectBuilder.add(FIELD_NAME_RESOURCE_SPECIFIC_PROPERTY_VALUE + "s", multipleValues);
                    jsonValuelistMultiplePropertiesArrayBuilder.add(valuelistMultipleJsonObjectBuilder);
                } else if( CategorySpecificProperty.Type.INTERVAL.toString().equals(resourceSpecificPropertyAPI.getProperty().getType()) && (resourceSpecificPropertyAPI.getIntervalFromValue() != null ||resourceSpecificPropertyAPI.getIntervalToValue() != null) ) {
                    JsonObjectBuilder intervalJsonObjectBuilder = jsonBuilderFactory.createObjectBuilder().
                            add(FIELD_NAME_RESOURCE_SPECIFIC_PROPERTY_ID, resourceSpecificPropertyAPI.getProperty().getId());
                    if( resourceSpecificPropertyAPI.getIntervalFromValue() != null || resourceSpecificPropertyAPI.getIntervalToValue() != null ) {
                        JsonObjectBuilder intervalRangeBuilder = jsonBuilderFactory.createObjectBuilder();
                        if( resourceSpecificPropertyAPI.getIntervalFromValue() != null ) {
                            intervalRangeBuilder.add("gte", resourceSpecificPropertyAPI.getIntervalFromValue());
                        }
                        if( resourceSpecificPropertyAPI.getIntervalToValue() != null ) {
                            intervalRangeBuilder.add("lte", resourceSpecificPropertyAPI.getIntervalToValue());
                        }
                        intervalJsonObjectBuilder.add(FIELD_NAME_RESOURCE_SPECIFIC_INTERVAL_RANGE_PROPERTIES, intervalRangeBuilder);
                    }
                    jsonIntervalPropertiesArrayBuilder.add(intervalJsonObjectBuilder);
                }
            }
            jsonObjectBuilder.add(FIELD_NAME_RESOURCE_SPECIFIC_TEXT_PROPERTIES, jsonTextPropertiesArrayBuilder);
            jsonObjectBuilder.add(FIELD_NAME_RESOURCE_SPECIFIC_DECIMAL_PROPERTIES, jsonDecimalPropertiesArrayBuilder);
            jsonObjectBuilder.add(FIELD_NAME_RESOURCE_SPECIFIC_VALUELIST_SINGLE_PROPERTIES, jsonValuelistSinglePropertiesArrayBuilder);
            jsonObjectBuilder.add(FIELD_NAME_RESOURCE_SPECIFIC_VALUELIST_MULTIPLE_PROPERTIES, jsonValuelistMultiplePropertiesArrayBuilder);
            jsonObjectBuilder.add(FIELD_NAME_RESOURCE_SPECIFIC_INTERVAL_PROPERTIES, jsonIntervalPropertiesArrayBuilder);
        }
    }

    private SearchResponse getAllProductsAndArticlesComplexResults(String query,
                                                                   Category category,
                                                                   Product.Status status,
                                                                   List<CategorySpecificProperty> categorySpecificPropertys,
                                                                   MultivaluedMap<String, String> queryParameters,
                                                                   String sortType,
                                                                   String sortOrder,
                                                                   boolean includeProducts,
                                                                   boolean includeArticles,
                                                                   List<Article.Type> articleTypes,
                                                                   int supplier,
                                                                   int offset,
                                                                   int limit,
                                                                   List<Article> excludeTheseArticles,
                                                                   boolean includeOnlyTiso) throws IOException {
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.
                boolQuery();
        if( query != null && !query.isEmpty() ) {
            QueryBuilder fulltextQueryBuilder = QueryBuilders.
                    multiMatchQuery(query).
                    field(FIELD_NAME_FULLTEXT). // no specific boost since all matches in fulltext are equal
                    field(FIELD_NAME_NAME, fieldBoost). // boost article/product name field
                    field(FIELD_NAME_NUMBER, fieldBoost). // boost article/product number field
                    field(FIELD_NAME_CATEGORY_CODE, fieldBoost). // boost category code field
                    fuzziness(fuzziness).
                    prefixLength(prefixLength).
                    type(MultiMatchQueryBuilder.Type.MOST_FIELDS).
                    operator(Operator.AND);
            boolQueryBuilder.must(fulltextQueryBuilder);
        } else {
            QueryBuilder allQueryBuilder = QueryBuilders.matchAllQuery();
            boolQueryBuilder.must(allQueryBuilder);
        }

        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        Organization organization = organizationController.getOrganizationFromLoggedInUser(userAPI);
        boolean isServiceOwner = organization.getOrganizationType() == Organization.OrganizationType.SERVICE_OWNER;
        boolean isSupplier = organization.getOrganizationType() == Organization.OrganizationType.SUPPLIER;

        // do not include customer unique in this query
        //PELLE
        // unless serviceOwner fixt!
        // or own organization fixt!
        // or on agreement...
        // or shared agreement...

        if (!isServiceOwner) {
            //    QueryBuilder customerUniqueQueryBuilder = QueryBuilders.
            //            termQuery(FIELD_NAME_CUSTOMER_UNIQUE, false);
            //    boolQueryBuilder.must(customerUniqueQueryBuilder);

           if (isSupplier) {

                QueryBuilder testyQueryBuilder_1 = QueryBuilders.termQuery(FIELD_NAME_SUPPLIER_ID, organization.getUniqueId());
                QueryBuilder testyQueryBuilder_2 = QueryBuilders.termQuery(FIELD_NAME_CUSTOMER_UNIQUE, true);
                QueryBuilder testyQueryBuilder_3 = QueryBuilders.termQuery(FIELD_NAME_CUSTOMER_UNIQUE, false);

                BoolQueryBuilder boolQueryBuilder_1 = QueryBuilders.boolQuery();
                boolQueryBuilder_1.must(testyQueryBuilder_1);
                boolQueryBuilder_1.must(testyQueryBuilder_2);

                BoolQueryBuilder boolQueryBuilder_2 = QueryBuilders.boolQuery();
                boolQueryBuilder_2.should(testyQueryBuilder_3);
                boolQueryBuilder_2.should(boolQueryBuilder_1);

                boolQueryBuilder.must(boolQueryBuilder_2);

           } else { // customer

               QueryBuilder testyQueryBuilder_2 = QueryBuilders.termQuery(FIELD_NAME_CUSTOMER_UNIQUE, true);
               QueryBuilder testyQueryBuilder_3 = QueryBuilders.termQuery(FIELD_NAME_CUSTOMER_UNIQUE, false);

               BoolQueryBuilder boolQueryBuilder_1 = QueryBuilders.boolQuery();
               boolQueryBuilder_1.must(testyQueryBuilder_2);

               BoolQueryBuilder boolQueryBuilder_2 = QueryBuilders.boolQuery();
               boolQueryBuilder_2.should(testyQueryBuilder_3);
               boolQueryBuilder_2.should(boolQueryBuilder_1);

               boolQueryBuilder.must(boolQueryBuilder_2);
           }

        }

        if(excludeTheseArticles != null){
            for (Article excludedArticle : excludeTheseArticles){
                boolQueryBuilder.mustNot(QueryBuilders.termQuery("_id", excludedArticle.getUniqueId() ));
            }
        }

        if(includeOnlyTiso)
        {
            boolQueryBuilder.must(QueryBuilders.wildcardQuery(FIELD_NAME_CATEGORY_CODE, "*"));
        }

        // include both articles and products?
        if( includeArticles != includeProducts ) {
            boolQueryBuilder.must(QueryBuilders.
                    termQuery(FIELD_NAME_TYPE, includeArticles ? TYPE_ARTICLE: TYPE_PRODUCT));
        }

        if( status != null ) {
            boolQueryBuilder.must(QueryBuilders.
                    termQuery(FIELD_NAME_STATUS, status.toString()));
        }

        //if supplier filter
        if( supplier != 0){
            boolQueryBuilder.must(QueryBuilders.termQuery(FIELD_NAME_SUPPLIER_ID, supplier));
        }

        // if article types, it must match one of
        if( includeArticles && articleTypes != null && !articleTypes.isEmpty() ) {
            BoolQueryBuilder mustArticletypesBoolQueryBuilder = QueryBuilders.boolQuery();
            BoolQueryBuilder shouldArticletypesBoolQueryBuilder = QueryBuilders.boolQuery();
            for( Article.Type articleType : articleTypes ) {
                shouldArticletypesBoolQueryBuilder.should(QueryBuilders.termQuery(FIELD_NAME_CATEGORY_TYPE, articleType.toString()));
            }
            shouldArticletypesBoolQueryBuilder.minimumShouldMatch(1);
            //HJAL-2183
            if (includeProducts){
                shouldArticletypesBoolQueryBuilder.should(QueryBuilders.termQuery(FIELD_NAME_TYPE, TYPE_PRODUCT));
            }
            mustArticletypesBoolQueryBuilder.must(shouldArticletypesBoolQueryBuilder);
            boolQueryBuilder.must(mustArticletypesBoolQueryBuilder);
        }
        //HJAL-2247 inga artikeltyper valda == alla artikeltyper valda
        if( includeArticles && (articleTypes == null || articleTypes.isEmpty()) ) {
            BoolQueryBuilder mustArticletypesBoolQueryBuilder = QueryBuilders.boolQuery();
            BoolQueryBuilder shouldArticletypesBoolQueryBuilder = QueryBuilders.boolQuery();

            Article.Type[] allArticleTypes = Article.Type.values();
            for( Article.Type articleType : allArticleTypes ) {
                shouldArticletypesBoolQueryBuilder.should(QueryBuilders.termQuery(FIELD_NAME_CATEGORY_TYPE, articleType.toString()));
            }
            shouldArticletypesBoolQueryBuilder.minimumShouldMatch(1);
            //HJAL-2183
            if (includeProducts){
                shouldArticletypesBoolQueryBuilder.should(QueryBuilders.termQuery(FIELD_NAME_TYPE, TYPE_PRODUCT));
            }
            mustArticletypesBoolQueryBuilder.must(shouldArticletypesBoolQueryBuilder);
            boolQueryBuilder.must(mustArticletypesBoolQueryBuilder);
        }

        if( category != null ) {
            QueryBuilder categoryQueryBuilder = QueryBuilders.
                    termQuery(FIELD_NAME_CATEGORY_ID, category.getUniqueId());
            boolQueryBuilder.filter(categoryQueryBuilder);
            if( categorySpecificPropertys != null && !categorySpecificPropertys.isEmpty() ) {
                for( CategorySpecificProperty categorySpecificProperty : categorySpecificPropertys ) {
                    if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.TEXTFIELD ) {
                        String userSuppliedValue = queryParameters.getFirst("csp_" + categorySpecificProperty.getUniqueId() );
                        if( userSuppliedValue != null && !userSuppliedValue.isEmpty() ) {
                            LOG.log( Level.FINEST, "Found user supplied value for category {0} of type TEXTFIELD", new Object[] {categorySpecificProperty.getUniqueId()});

                            QueryBuilder valueFullTextQueryBuilder = QueryBuilders.multiMatchQuery(userSuppliedValue).
                                field(FIELD_NAME_RESOURCE_SPECIFIC_TEXT_PROPERTIES + ".value");

                            // match the id of the property
                            QueryBuilder idQueryBuilder = QueryBuilders.termQuery(FIELD_NAME_RESOURCE_SPECIFIC_TEXT_PROPERTIES + ".id", categorySpecificProperty.getUniqueId());

                            BoolQueryBuilder textfieldBoolQueryBuilder = QueryBuilders.boolQuery().
                                    must(idQueryBuilder).
                                    must(valueFullTextQueryBuilder);
                            QueryBuilder nestedPropertyIdBuilder = QueryBuilders.
                                    nestedQuery(FIELD_NAME_RESOURCE_SPECIFIC_TEXT_PROPERTIES,  textfieldBoolQueryBuilder, ScoreMode.None);
                            boolQueryBuilder.filter(nestedPropertyIdBuilder);
                        }
                    } else if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.DECIMAL ) {
                        String userSuppliedValueFrom = queryParameters.getFirst("csp_" + categorySpecificProperty.getUniqueId() + "_from" );
                        String userSuppliedValueTo = queryParameters.getFirst("csp_" + categorySpecificProperty.getUniqueId() + "_to");
                        if( userSuppliedValueFrom != null && !userSuppliedValueFrom.isEmpty() && userSuppliedValueTo != null && !userSuppliedValueTo.isEmpty()) {
                            LOG.log( Level.FINEST, "Found user supplied value for category {0} of type DECIMAL", new Object[] {categorySpecificProperty.getUniqueId()});
                            double from = Double.parseDouble(userSuppliedValueFrom);
                            double to = Double.parseDouble(userSuppliedValueTo);
                            QueryBuilder valueRangeQueryBuilder = QueryBuilders.
                                rangeQuery(FIELD_NAME_RESOURCE_SPECIFIC_DECIMAL_PROPERTIES + ".value").
                                            from(from).
                                            to(to).
                                            includeLower(true).
                                            includeUpper(true);

                            // match the id of the property
                            QueryBuilder idQueryBuilder = QueryBuilders.termQuery(FIELD_NAME_RESOURCE_SPECIFIC_DECIMAL_PROPERTIES + ".id", categorySpecificProperty.getUniqueId());
                            BoolQueryBuilder decimalBoolQueryBuilder = QueryBuilders.boolQuery().
                                    must(idQueryBuilder).
                                    must(valueRangeQueryBuilder);
                            QueryBuilder categorySpecificPropertyIdBuilder = QueryBuilders.
                                    nestedQuery(FIELD_NAME_RESOURCE_SPECIFIC_DECIMAL_PROPERTIES,  decimalBoolQueryBuilder, ScoreMode.None);
                            boolQueryBuilder.filter(categorySpecificPropertyIdBuilder);
                        }
                    } else if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.INTERVAL ) {
                        String userSuppliedValueFrom = queryParameters.getFirst("csp_" + categorySpecificProperty.getUniqueId() + "_from" );
                        String userSuppliedValueTo = queryParameters.getFirst("csp_" + categorySpecificProperty.getUniqueId() + "_to");
                        if( (userSuppliedValueFrom != null && !userSuppliedValueFrom.isEmpty()) || (userSuppliedValueTo != null && !userSuppliedValueTo.isEmpty())) {
                            LOG.log( Level.FINEST, "Found user supplied value for category {0} of type INTERVAL", new Object[] {categorySpecificProperty.getUniqueId()});
                            double from = 0; // default from if user didn't specify
                            if( userSuppliedValueFrom != null && !userSuppliedValueFrom.isEmpty() ) {
                                from = Double.parseDouble(userSuppliedValueFrom);
                            }
                            double to = Double.MAX_VALUE; // default to value if user didn't specify
                            if( userSuppliedValueTo != null && !userSuppliedValueTo.isEmpty() ) {
                                to = Double.parseDouble(userSuppliedValueTo);
                            }

                            BoolQueryBuilder valueFromToQueryBuilder = QueryBuilders.boolQuery();
                            // range
                            QueryBuilder valueFromRangeQueryBuilder = QueryBuilders.
                                rangeQuery(FIELD_NAME_RESOURCE_SPECIFIC_INTERVAL_PROPERTIES + "." + FIELD_NAME_RESOURCE_SPECIFIC_INTERVAL_RANGE_PROPERTIES).
                                            from(from).
                                            to(to).
                                            includeLower(true).
                                            includeUpper(true);
                            valueFromToQueryBuilder.should(valueFromRangeQueryBuilder);

                            // match the id of the property
                            QueryBuilder idQueryBuilder = QueryBuilders.termQuery(FIELD_NAME_RESOURCE_SPECIFIC_INTERVAL_PROPERTIES + ".id", categorySpecificProperty.getUniqueId());

                            BoolQueryBuilder intervalBoolQueryBuilder = QueryBuilders.boolQuery().
                                    must(idQueryBuilder).
                                    must(valueFromToQueryBuilder);
                            QueryBuilder nestedPropertyIdBuilder = QueryBuilders.
                                    nestedQuery(FIELD_NAME_RESOURCE_SPECIFIC_INTERVAL_PROPERTIES,  intervalBoolQueryBuilder, ScoreMode.None);
                            boolQueryBuilder.filter(nestedPropertyIdBuilder);
                        }
                    } else if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_SINGLE ) {
                        String userSuppliedValue = queryParameters.getFirst("csp_" + categorySpecificProperty.getUniqueId() );
                        if( userSuppliedValue != null && !userSuppliedValue.isEmpty() ) {
                            LOG.log( Level.FINEST, "Found user supplied value for category {0} of type VALUELIST_SINGLE", new Object[] {categorySpecificProperty.getUniqueId()});
                            long userSuppliedValueId = Long.parseLong(userSuppliedValue);

                            QueryBuilder valueQueryBuilder = QueryBuilders.termQuery(FIELD_NAME_RESOURCE_SPECIFIC_VALUELIST_SINGLE_PROPERTIES + ".value",
                                                    userSuppliedValueId);
                            // match the id of the property
                            QueryBuilder idQueryBuilder = QueryBuilders.termQuery(FIELD_NAME_RESOURCE_SPECIFIC_VALUELIST_SINGLE_PROPERTIES + ".id", categorySpecificProperty.getUniqueId());

                            BoolQueryBuilder valuelistSingleQueryBuilder = QueryBuilders.boolQuery().
                                    must(idQueryBuilder).
                                    must(valueQueryBuilder);
                            QueryBuilder nestedPropertyIdBuilder = QueryBuilders.
                                    nestedQuery(FIELD_NAME_RESOURCE_SPECIFIC_VALUELIST_SINGLE_PROPERTIES,
                                            valuelistSingleQueryBuilder,
                                            ScoreMode.None);
                            boolQueryBuilder.filter(nestedPropertyIdBuilder);
                        }
                    }  else if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_MULTIPLE ) {
                        List<String> userSuppliedValues = queryParameters.get("csp_" + categorySpecificProperty.getUniqueId() );
                        if( userSuppliedValues != null && !userSuppliedValues.isEmpty() ) {
                            LOG.log( Level.FINEST, "Found user supplied value for category {0} of type VALUELIST_MULTIPLE", new Object[] {categorySpecificProperty.getUniqueId()});

                            BoolQueryBuilder multipleListBoolQueryBuilder = QueryBuilders.boolQuery();
                            for( String valueAsString : userSuppliedValues ) {
                                long value = Long.parseLong(valueAsString);
                                multipleListBoolQueryBuilder.must(QueryBuilders.termQuery(FIELD_NAME_RESOURCE_SPECIFIC_VALUELIST_MULTIPLE_PROPERTIES + ".values", value));
                            }

                            // match the id of the property
                            QueryBuilder idQueryBuilder = QueryBuilders.termQuery(FIELD_NAME_RESOURCE_SPECIFIC_VALUELIST_MULTIPLE_PROPERTIES + ".id", categorySpecificProperty.getUniqueId());

                            BoolQueryBuilder valuelistMultipleQueryBuilder = QueryBuilders.boolQuery().
                                    must(idQueryBuilder).
                                    must(multipleListBoolQueryBuilder);
                            QueryBuilder nestedPropertyIdBuilder = QueryBuilders.
                                    nestedQuery(FIELD_NAME_RESOURCE_SPECIFIC_VALUELIST_MULTIPLE_PROPERTIES,
                                            valuelistMultipleQueryBuilder,
                                            ScoreMode.None);
                            boolQueryBuilder.filter(nestedPropertyIdBuilder);
                        }
                    }
                }
            }
        }
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().
                query(boolQueryBuilder).
                from(offset).
                trackTotalHits(true).
                size(limit);
        // only apply minimum score if user entered anything in the query field
        if( query != null && !query.isEmpty() ) {
            searchSourceBuilder.minScore(minScore);
        }

        this.determineSortTypeAndOrder(searchSourceBuilder, sortType, sortOrder);

        SearchRequest searchRequest = new SearchRequest(indexName).source(searchSourceBuilder);
        return restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
    }

    private void determineSortTypeAndOrder(SearchSourceBuilder searchSourceBuilder, String sortType, String sortOrder) {

        sortOrder = sortOrder.toUpperCase();
        sortType = sortType.toUpperCase();

        switch (sortType) {

            case "ARTICLETYPE":
                searchSourceBuilder.sort(new FieldSortBuilder(FIELD_NAME_CATEGORY_TYPE).order(SortOrder.fromString(sortOrder)));
                return;
            case "NAME":
                searchSourceBuilder.sort(new FieldSortBuilder(FIELD_NAME_NAME + ".keyword").order(SortOrder.fromString(sortOrder)));
                return;
            case "NUMBER":
                searchSourceBuilder.sort(new FieldSortBuilder(FIELD_NAME_NUMBER + ".keyword").order(SortOrder.fromString(sortOrder)));
                return;
            case "ORGANIZATIONNAME":
                searchSourceBuilder.sort(new FieldSortBuilder(FIELD_NAME_SUPPLIER_NAME + ".keyword").order(SortOrder.fromString(sortOrder)));
                return;
            case "CODE":
                searchSourceBuilder.sort(new FieldSortBuilder(FIELD_NAME_CATEGORY_CODE + ".keyword").order(SortOrder.fromString(sortOrder)));
                return;
            case "STATUS":
                searchSourceBuilder.sort(new FieldSortBuilder(FIELD_NAME_STATUS + ".keyword").order(SortOrder.fromString(sortOrder)));
                return;
            case "BASEDONPRODUCT":
                searchSourceBuilder.sort(new FieldSortBuilder(FIELD_NAME_BASED_ON_PRODUCT + ".keyword").order(SortOrder.fromString(sortOrder)));
                return;
            default:
        }
    }

    private SearchDTO mapSearchResponseCustomer(SearchResponse searchResponse) {
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        List<SearchProductsAndArticlesAPI> searchProductsAndArticlesAPIs = new ArrayList<>();
        long count = 0L;
        if( searchResponse != null ) {
            SearchHits hits = searchResponse.getHits();
            count = hits.getTotalHits().value;
            SearchHit[] searchHits = hits.getHits();
            for( SearchHit searchHit : searchHits ) {
                Map<String, Object> sourceAsMap = searchHit.getSourceAsMap();
                String type = (String) sourceAsMap.get(FIELD_NAME_TYPE);
                String status = (String) sourceAsMap.get(FIELD_NAME_STATUS);
                String name = (String) sourceAsMap.get(FIELD_NAME_NAME);
                String number = (String) sourceAsMap.get(FIELD_NAME_NUMBER);
                String categoryCode = (String) sourceAsMap.get(FIELD_NAME_CATEGORY_CODE);
                String categoryType = (String) sourceAsMap.get(FIELD_NAME_CATEGORY_TYPE);
                Integer organizationId = (Integer) sourceAsMap.get(FIELD_NAME_SUPPLIER_ID);
                String organizationName = (String) sourceAsMap.get(FIELD_NAME_SUPPLIER_NAME);
                boolean isCustomerUnique = (boolean) sourceAsMap.get(FIELD_NAME_CUSTOMER_UNIQUE);
                SearchProductsAndArticlesAPI searchProductsAndArticlesAPI = new SearchProductsAndArticlesAPI();
                searchProductsAndArticlesAPI.setId(Long.valueOf(searchHit.getId()));
                searchProductsAndArticlesAPI.setStatus(Product.Status.valueOf(status));
                searchProductsAndArticlesAPI.setType(SearchProductsAndArticlesAPI.Type.valueOf(type));
                searchProductsAndArticlesAPI.setNumber(number);
                searchProductsAndArticlesAPI.setName(name);
                searchProductsAndArticlesAPI.setCode(categoryCode);
                searchProductsAndArticlesAPI.setArticleType(Article.Type.valueOf(categoryType));
                searchProductsAndArticlesAPI.setOrganizationId(organizationId.longValue());
                searchProductsAndArticlesAPI.setOrganizationName(organizationName);

                if (type.equals(TYPE_ARTICLE)) {
                    Article article = articleController.getArticle(Long.parseLong(searchHit.getId()), userAPI);
                    ArticleAPI articleAPI = ArticleMapper.map(article,true, null);
                    ProductAPI basedOnProduct = null;
                    if (articleAPI != null) {
                        basedOnProduct = articleAPI.getBasedOnProduct();
                    }
                    searchProductsAndArticlesAPI.setBasedOnProduct(basedOnProduct);
                }

                if (categoryCode == null && type.equals(TYPE_ARTICLE)) {
                    Article article = articleController.getArticle(Long.parseLong(searchHit.getId()), userAPI);
                    ArticleAPI articleAPI = ArticleMapper.map(article,true, null);

                    CategoryAPI category;

                    if (articleAPI != null) {
                        //case 1
                        if( articleAPI.getBasedOnProduct() == null ) {
                            category = articleAPI.getCategory();
                        } else {
                            category = articleAPI.getBasedOnProduct().getCategory();
                        }

                        //case 2
                        if (category.getCode() == null){
                            if (articleAPI.getFitsToProducts() != null && !articleAPI.getFitsToProducts().isEmpty()) {
                                category = articleAPI.getFitsToProducts().get(0).getCategory();
                            }
                        }

                        //case 3
                        if (category.getCode() == null){
                            if (articleAPI.getFitsToArticles() != null && !articleAPI.getFitsToArticles().isEmpty()) {
                                if (articleAPI.getFitsToArticles().get(0).getBasedOnProduct() != null) {
                                    category = articleAPI.getFitsToArticles().get(0).getBasedOnProduct().getCategory();
                                }
                                // kan tänkas att detta ska med? PA 2020-10-05
                                else{
                                    category = articleAPI.getFitsToArticles().get(0).getCategory();
                                }
                            }
                        }
                        if (category.getCode() == null){
                            if (articleAPI.getFitsToArticles() != null && !articleAPI.getFitsToArticles().isEmpty()) {
                                if (articleAPI.getFitsToArticles().get(0).getFitsToProducts() != null && !articleAPI.getFitsToArticles().get(0).getFitsToProducts().isEmpty()) {
                                    category = articleAPI.getFitsToArticles().get(0).getFitsToProducts().get(0).getCategory();
                                }
                            }
                        }

                        searchProductsAndArticlesAPI.setCode("(" + category.getCode() + ")");

//                        if( category.getCode() != null && category.getCode().length() > 6 ) {
//                            // we do not want to include categories longer than 6 digits
//                            // let's find the parent category with six digits
//                            category = category.getParent();
//                        }
                    }

                }

                if(isCustomerUnique) {
                    long id = Long.parseLong(searchHit.getId());
                    Article article = null;
                    Product product = null;
                    if (type.equals(TYPE_ARTICLE)){
                        article = articleController.getArticle(id, userAPI);
                    } else if (type.equals(TYPE_PRODUCT)) {
                        product = productController.getProductForElasticSearch(id, userAPI);
                    }

                    if (article != null) {
                        searchProductsAndArticlesAPIs.add(searchProductsAndArticlesAPI);
                    } else if (product != null) {
                        searchProductsAndArticlesAPIs.add(searchProductsAndArticlesAPI);
                    } else {
                        count--;
                    }
                }
                else {
                    searchProductsAndArticlesAPIs.add(searchProductsAndArticlesAPI);
                }
            }
        }
        return new SearchDTO(count, searchProductsAndArticlesAPIs);
    }

    private SearchDTO mapSearchResponse(SearchResponse searchResponse) {
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        List<SearchProductsAndArticlesAPI> searchProductsAndArticlesAPIs = new ArrayList<>();
        long count = 0L;
        if( searchResponse != null ) {
            SearchHits hits = searchResponse.getHits();
            count = hits.getTotalHits().value;
            SearchHit[] searchHits = hits.getHits();
            for( SearchHit searchHit : searchHits ) {
                Map<String, Object> sourceAsMap = searchHit.getSourceAsMap();
                String type = (String) sourceAsMap.get(FIELD_NAME_TYPE);
                String status = (String) sourceAsMap.get(FIELD_NAME_STATUS);
                String name = (String) sourceAsMap.get(FIELD_NAME_NAME);
                String number = (String) sourceAsMap.get(FIELD_NAME_NUMBER);
                String categoryCode = (String) sourceAsMap.get(FIELD_NAME_CATEGORY_CODE);
                String categoryType = (String) sourceAsMap.get(FIELD_NAME_CATEGORY_TYPE);
                Integer organizationId = (Integer) sourceAsMap.get(FIELD_NAME_SUPPLIER_ID);
                String organizationName = (String) sourceAsMap.get(FIELD_NAME_SUPPLIER_NAME);
                SearchProductsAndArticlesAPI searchProductsAndArticlesAPI = new SearchProductsAndArticlesAPI();
                searchProductsAndArticlesAPI.setId(Long.valueOf(searchHit.getId()));
                searchProductsAndArticlesAPI.setStatus(Product.Status.valueOf(status));
                searchProductsAndArticlesAPI.setType(SearchProductsAndArticlesAPI.Type.valueOf(type));
                searchProductsAndArticlesAPI.setNumber(number);
                searchProductsAndArticlesAPI.setName(name);
                searchProductsAndArticlesAPI.setCode(categoryCode);
                searchProductsAndArticlesAPI.setArticleType(Article.Type.valueOf(categoryType));
                searchProductsAndArticlesAPI.setOrganizationId(organizationId.longValue());
                searchProductsAndArticlesAPI.setOrganizationName(organizationName);
                searchProductsAndArticlesAPIs.add(searchProductsAndArticlesAPI);

                if (categoryCode == null && type.equals(TYPE_ARTICLE)) {
                    Article article = articleController.getArticle(Long.parseLong(searchHit.getId()), userAPI);
                    ArticleAPI articleAPI = ArticleMapper.map(article,true, null);

                    CategoryAPI category;

                    if (articleAPI != null) {
                        //case 1
                        if( articleAPI.getBasedOnProduct() == null ) {
                            category = articleAPI.getCategory();
                        } else {
                            category = articleAPI.getBasedOnProduct().getCategory();
                        }

                        //case 2
                        if (category.getCode() == null){
                            if (articleAPI.getFitsToProducts() != null && !articleAPI.getFitsToProducts().isEmpty()) {
                                category = articleAPI.getFitsToProducts().get(0).getCategory();
                            }
                        }

                        //case 3
                        if (category.getCode() == null){
                            if (articleAPI.getFitsToArticles() != null && !articleAPI.getFitsToArticles().isEmpty()) {
                                if (articleAPI.getFitsToArticles().get(0).getBasedOnProduct() != null) {
                                    category = articleAPI.getFitsToArticles().get(0).getBasedOnProduct().getCategory();
                                }
                                // kan tänkas att detta ska med? PA 2020-10-05
                                else{
                                    category = articleAPI.getFitsToArticles().get(0).getCategory();
                                }
                            }
                        }
                        if (category.getCode() == null){
                            if (articleAPI.getFitsToArticles() != null && !articleAPI.getFitsToArticles().isEmpty()) {
                                if (articleAPI.getFitsToArticles().get(0).getFitsToProducts() != null && !articleAPI.getFitsToArticles().get(0).getFitsToProducts().isEmpty()) {
                                    category = articleAPI.getFitsToArticles().get(0).getFitsToProducts().get(0).getCategory();
                                }
                            }
                        }

                        searchProductsAndArticlesAPI.setCode("(" + category.getCode() + ")");

//                        if( category.getCode() != null && category.getCode().length() > 6 ) {
//                            // we do not want to include categories longer than 6 digits
//                            // let's find the parent category with six digits
//                            category = category.getParent();
//                        }
                    }

                }
            }
        }
        return new SearchDTO(count, searchProductsAndArticlesAPIs);
    }

    @PostConstruct
    private void initialize() {
        String elasticSearchUrl = System.getenv("ELASTICSEARCH_URL");
        String elasticToken = System.getenv("ELASTICSEARCH_TOKEN");
        indexName = System.getenv("ELASTICSEARCH_INDEX_NAME");
        String minScoreString = System.getenv("ELASTICSEARCH_MIN_SCORE");
        minScore = minScoreString == null ? 10: Float.parseFloat(minScoreString);
        String fuzzinessString = System.getenv("ELASTICSEARCH_FUZZINESS");
        fuzziness = fuzzinessString == null ? 1: Integer.parseInt(fuzzinessString);
        String prefixLengthString = System.getenv("ELASTICSEARCH_PREFIX_LENGTH");
        prefixLength = prefixLengthString == null ? 3: Integer.parseInt(prefixLengthString);
        String fieldBoostString = System.getenv("ELASTICSEARCH_FIELD_BOOST");
        fieldBoost = fieldBoostString == null ? 10: Float.parseFloat(fieldBoostString);
        // url and name is the minimum that must be configured
        if( elasticSearchUrl != null && !elasticSearchUrl.isEmpty() && indexName != null && !indexName.isEmpty()) {

            // First, set up the low-level client.
            RestClientBuilder lowLevelClient = RestClient.builder(HttpHost.create(elasticSearchUrl));

            // If there is an elastic token available, add it to request header. (create/refresh with base64(user:pass))
            if(elasticToken != null) {
                lowLevelClient.setDefaultHeaders(new Header[] {
                    new BasicHeader("Authorization", "Basic " + elasticToken)
                });
            }

            // Construct the high-level client on top of the low level client.
            // TODO: Move the low level client so that it can be accessed by the api-client and gradually shift to newer functionality.
            this.restHighLevelClient = new RestHighLevelClientBuilder(lowLevelClient.build())
                .setApiCompatibilityMode(true)
                .build();

            enabled = true;

            BulkProcessor.Listener listener = new BulkProcessor.Listener() {
                @Override
                public void beforeBulk(long executionId, BulkRequest request) {
                    LOG.log(Level.FINEST, "Before bulk job, hlrc bulk processor: BulkRequest: {0}", new Object[] { request.getDescription() });
                }

                @Override
                public void afterBulk(long executionId, BulkRequest request, BulkResponse response) {
                    LOG.log(Level.FINEST, "After bulk job, hlrc bulk processor: BulkRequest: {0}, BulkResponse is Fragment: {1}", new Object[] { request.getDescription(), response.isFragment() });
                }

                @Override
                public void afterBulk(long executionId, BulkRequest request, Throwable failure) {
                    LOG.log(Level.FINEST, "After failed bulk job, hlrc bulk processor: BulkRequest: {0}", new Object[] { request.getDescription() });
                }
            };

            // BulkProcessor
            this.bulkProcessor = BulkProcessor.builder((request, bulkListener) -> restHighLevelClient.bulkAsync(request, RequestOptions.DEFAULT, bulkListener),
                    listener,
                    "hlrc-bulk-processor"
                ).setBulkActions(2000)
                //).setBulkActions(20000) //increase this?
                .setBulkSize(new ByteSizeValue(5, ByteSizeUnit.MB)) //5MB is default, increase this?
                .setFlushInterval(TimeValue.timeValueSeconds(5))
                .build();

            // get the elastic search index configuration json
            InputStream inputStreamMappings = this.getClass().getClassLoader().getResourceAsStream("elasticsearch_index_source.json");
            assert inputStreamMappings != null;
            indexSource = new BufferedReader(new InputStreamReader(inputStreamMappings)).lines().collect(Collectors.joining("\n"));
        }
    }

}
