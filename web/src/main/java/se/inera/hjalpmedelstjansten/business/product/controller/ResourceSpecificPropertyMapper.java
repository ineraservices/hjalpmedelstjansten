package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.model.api.ResourceSpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificPropertyListValue;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValue;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueDecimal;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueInterval;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueTextField;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueValueListMultiple;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueValueListSingle;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for mapping between API and Entity classes
 * 
 */
public class ResourceSpecificPropertyMapper {
    
    public static final List<ResourceSpecificPropertyAPI> map(List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues) {
        if( resourceSpecificPropertyValues == null || resourceSpecificPropertyValues.isEmpty() ) {
            return null;
        }
        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        for( ResourceSpecificPropertyValue resourceSpecificPropertyValue : resourceSpecificPropertyValues ) {
            resourceSpecificPropertyAPIs.add(map(resourceSpecificPropertyValue));
        }
        return resourceSpecificPropertyAPIs;
    }
    
    public static final ResourceSpecificPropertyAPI map(ResourceSpecificPropertyValue resourceSpecificPropertyValue) {
        if( resourceSpecificPropertyValue == null ) {
            return null;
        }
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        resourceSpecificPropertyAPI.setId(resourceSpecificPropertyValue.getUniqueId());
        resourceSpecificPropertyAPI.setProperty(CategorySpecificPropertyMapper.map(resourceSpecificPropertyValue.getCategorySpecificProperty()));
        if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueTextField ) {
            ResourceSpecificPropertyValueTextField resourceSpecificPropertyValueTextfield = ( ResourceSpecificPropertyValueTextField ) resourceSpecificPropertyValue;
            resourceSpecificPropertyAPI.setTextValue(resourceSpecificPropertyValueTextfield.getValue());
        } else if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueDecimal ) {
            ResourceSpecificPropertyValueDecimal resourceSpecificPropertyValueDecimal = ( ResourceSpecificPropertyValueDecimal ) resourceSpecificPropertyValue;
            resourceSpecificPropertyAPI.setDecimalValue(resourceSpecificPropertyValueDecimal.getValue());
        } else if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueInterval ) {
            ResourceSpecificPropertyValueInterval resourceSpecificPropertyValueInterval = ( ResourceSpecificPropertyValueInterval ) resourceSpecificPropertyValue;
            resourceSpecificPropertyAPI.setIntervalFromValue(resourceSpecificPropertyValueInterval.getFromValue());
            resourceSpecificPropertyAPI.setIntervalToValue(resourceSpecificPropertyValueInterval.getToValue());
        } else if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueValueListSingle ) {
            ResourceSpecificPropertyValueValueListSingle resourceSpecificPropertyValueValueListSingle = ( ResourceSpecificPropertyValueValueListSingle ) resourceSpecificPropertyValue;
            resourceSpecificPropertyAPI.setSingleListValue(resourceSpecificPropertyValueValueListSingle.getValue() == null ? null: resourceSpecificPropertyValueValueListSingle.getValue().getUniqueId());            
        } else if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueValueListMultiple ) {
            ResourceSpecificPropertyValueValueListMultiple resourceSpecificPropertyValueValueListMultiple = ( ResourceSpecificPropertyValueValueListMultiple ) resourceSpecificPropertyValue;
            if( resourceSpecificPropertyValueValueListMultiple.getValues() != null && !resourceSpecificPropertyValueValueListMultiple.getValues().isEmpty() ) {
                List<Long> multiListValues = new ArrayList<>();
                for( CategorySpecificPropertyListValue categorySpecificPropertyListValue : resourceSpecificPropertyValueValueListMultiple.getValues() ) {
                    multiListValues.add(categorySpecificPropertyListValue.getUniqueId());
                }
                resourceSpecificPropertyAPI.setMultipleListValue(multiListValues);                
            }
        }
        return resourceSpecificPropertyAPI;
    }
    
}
