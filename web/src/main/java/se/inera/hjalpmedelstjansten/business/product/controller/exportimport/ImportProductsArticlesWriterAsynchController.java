package se.inera.hjalpmedelstjansten.business.product.controller.exportimport;

import jakarta.ejb.Asynchronous;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.mail.MessagingException;
import jakarta.transaction.Transactional;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import lombok.NoArgsConstructor;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.helpers.ImportExcelHelper;
import se.inera.hjalpmedelstjansten.business.importstatus.controller.ImportStatusController;
import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.MdcToolkit;
import se.inera.hjalpmedelstjansten.business.media.controller.MediaMapper;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleValidation;
import se.inera.hjalpmedelstjansten.business.product.controller.CategoryMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.OrderUnitMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.PackageUnitMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductValidation;
import se.inera.hjalpmedelstjansten.business.property.view.PropertyLoader;
import se.inera.hjalpmedelstjansten.business.user.controller.EmailController;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.ImportStatusAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.ResourceSpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEDirectiveAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEStandardAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVDocumentTypeAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVOrderUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPackageUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;
import se.inera.hjalpmedelstjansten.model.api.media.MediaAPI;
import se.inera.hjalpmedelstjansten.model.api.media.MediaDocumentAPI;
import se.inera.hjalpmedelstjansten.model.api.media.MediaImageAPI;
import se.inera.hjalpmedelstjansten.model.api.media.MediaVideoAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificPropertyListValue;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEDirective;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEStandard;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;
import se.inera.hjalpmedelstjansten.model.entity.media.ArticleMediaDocument;
import se.inera.hjalpmedelstjansten.model.entity.media.ArticleMediaImage;
import se.inera.hjalpmedelstjansten.model.entity.media.ArticleMediaVideo;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaDocument;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaImage;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaVideo;

/**
 * Asynchronous import of read agreement pricelist rows
 *
 */
@Stateless
@NoArgsConstructor
public class ImportProductsArticlesWriterAsynchController extends ExportImportProductsArticlesController {
    private int importInProgress = 0;
    private int importComplete = 1;
    private int importFailed = 2;

    @Inject
    HjmtLogger LOG;

    @Inject
    ProductValidation productValidation;

    @Inject
    ArticleValidation articleValidation;

    @Inject
    EmailController emailController;

    @Inject
    OrganizationController organizationController;

    @Inject
    ElasticSearchController elasticSearchController;


    @Inject
    ImportExcelHelper importExcelHelper;

    @Inject
    ImportStatusController importStatusController;

    String importProductsArticlesMailSubjectSuccessful;
    String importProductsArticlesMailBodySuccessful;
    String importProductsArticlesMailSubjectFailed;
    String importProductsArticlesMailBodyFailed;

    @Inject
    public ImportProductsArticlesWriterAsynchController(PropertyLoader propertyLoader) {
        importProductsArticlesMailSubjectSuccessful = propertyLoader.getMessage("importProductsArticlesMailSubjectSuccessful");
        importProductsArticlesMailBodySuccessful = propertyLoader.getMessage("importProductsArticlesMailBodySuccessful");
        importProductsArticlesMailSubjectFailed = propertyLoader.getMessage("importProductsArticlesMailSubjectFailed");
        importProductsArticlesMailBodyFailed = propertyLoader.getMessage("importProductsArticlesMailBodyFailed");
    }

    /**
     *
     */
    @Asynchronous
    @Transactional
    @TransactionTimeout(7200)
    public void readAndSaveItems(long organizationUniqueId,
                                 XSSFWorkbook workbook,
                                 UserAPI userAPI,
                                 String sessionId,
                                 String requestIp,
                                 String fileName, Map<String, String> mdc) {
        try {
            MdcToolkit.setMdc(mdc);
            readAndSaveItems(organizationUniqueId, workbook, userAPI, sessionId, requestIp, fileName);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error reading and saving products and articles", e);
        } finally {
            MdcToolkit.clearMdc();
        }
    }

    private void readAndSaveItems(long organizationUniqueId,
        XSSFWorkbook workbook,
        UserAPI userAPI,
        String sessionId,
        String requestIp,
        String fileName) {

        LOG.log(Level.FINEST, "readAndSaveItems( organizationUniqueId: {0} )", new Object[]{organizationUniqueId});

        // read items
        HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validering misslyckades");
        Organization organization = organizationController.getOrganization(organizationUniqueId);

        ImportStatusAPI importStatusAPI = createImportStatusForFile(fileName,importInProgress,userAPI);

        List<CVOrderUnit> allOrderUnits = orderUnitController.findAll();
        List<CVPackageUnit> allPackageUnits = packageUnitController.findAll();
        Map<Long, CVOrderUnit> orderUnitsIdMap = orderUnitController.getAllAsIdMap(allOrderUnits);
        Map<Long, CVPackageUnit> packageUnitsIdMap = packageUnitController.getAllAsIdMap(allPackageUnits);

        List<ImportItem> importItems = null;
        List<ImportMediaItem> importMediaImageItems = null;
        List<ImportMediaItem> importMediaDocumentItems = null;
        List<ImportMediaItem> importMediaVideoItems = null;

        List<ProductAPI> addedProducts = new ArrayList<>();
        List<ProductAPI> updatedProducts = new ArrayList<>();
        List<ArticleAPI> addedArticles = new ArrayList<>();
        List<ArticleAPI> updatedArticles = new ArrayList<>();

        MediaIndexTracker mediaIndexTracker =  new MediaIndexTracker();

        try {
            // first read and validate items from excel to list of products and articles

            //kategorier
            importItems = readItems(organizationUniqueId,
                    workbook,
                    organization,
                    allOrderUnits,
                    allPackageUnits,
                    exception,
                    userAPI,
                    sessionId,
                    requestIp, updatedArticles);

            XSSFSheet imagesSheet = workbook.getSheet(IMAGES_SHEET_NAME);
            importMediaImageItems = readImages(imagesSheet, organization, userAPI, exception);

            XSSFSheet documentsSheet = workbook.getSheet(DOCUMENTS_SHEET_NAME);
            importMediaDocumentItems = readDocuments(documentsSheet, organization, userAPI, exception);

            XSSFSheet videosSheet = workbook.getSheet(VIDEOS_SHEET_NAME);
            importMediaVideoItems = readVideos(videosSheet, organization, userAPI, exception);
        } catch (Exception ex) {
            LOG.log(Level.WARNING, "Failed to handle product article import read phase", ex);

            exception.addValidationMessage(UNKNOWN, validationMessageService.getMessage("import.pricelist.unhandledException"));
        }

        // we stop if there are validation errors already
        LOG.log(Level.FINEST, "Finished reading items. Found: {0} validation errors", new Object[]{exception.getValidationMessages().size()});
        if (exception.getValidationMessages().isEmpty()) {

            // save items
            for (ImportItem importItem : importItems) {
                try {
                    if (importItem.getProductAPI() != null) { //product
                        ProductAPI productAPI = importItem.getProductAPI();
                        if (productAPI.getId() == null) {
                            // create product
                            createProduct(organizationUniqueId, userAPI, sessionId, requestIp, orderUnitsIdMap, packageUnitsIdMap, addedProducts, importItem, productAPI);
                        } else {
                            // update product
                            updateProduct(organizationUniqueId, userAPI, sessionId, requestIp, orderUnitsIdMap, packageUnitsIdMap, updatedProducts, importItem, productAPI);
                        }
                    } else { //article
                        ArticleAPI articleAPI = importItem.getArticleAPI();
                        if (articleAPI.getId() == null) {
                            // create article
                            createArticle(organizationUniqueId, userAPI, sessionId, requestIp, orderUnitsIdMap, addedArticles, importItem, articleAPI);
                        } else {
                            // update article
                            updateArticle(organizationUniqueId, userAPI, sessionId, requestIp, orderUnitsIdMap, updatedArticles, importItem, articleAPI);
                        }
                    }
                } catch (HjalpmedelstjanstenValidationException ex) {

                    for (ErrorMessageAPI errorMessageAPI : ex.getValidationMessages()) {
                        addRowErrorMessage(exception, importItem.getRowNumber(), importItem.getSheetName(), errorMessageAPI.getField(), errorMessageAPI.getMessage());
                    }
                    // we break immediately since the transaction is done, errors should
                    // already have been handled before we get to this stage. if validation
                    // errors still exist, they should be handled in the read-phase
                    // in ImportPricelistController
                    break;
                } catch (Exception ex) {
                    LOG.log(Level.WARNING, "Failed to handle product article import", ex);
                    addRowErrorMessage(exception, importItem.getRowNumber(), importItem.getSheetName(), UNKNOWN, validationMessageService.getMessage("import.pricelist.unhandledException"));

                    // we break immediately since this is an "unhandled" error
                    break;
                }
            }

            handleUploadAndRemovalOfMedia(organizationUniqueId, userAPI, sessionId, requestIp, exception, importMediaImageItems, importMediaDocumentItems, importMediaVideoItems, mediaIndexTracker);
        }

        try {
            if (!exception.getValidationMessages().isEmpty()) {

                sendErrorEmail(userAPI, exception, fileName);

                updateImportStatusForFile(importStatusAPI,importFailed);
            } else {
                // update indexes
                updateIndexes(organizationUniqueId, addedProducts, updatedProducts, addedArticles, updatedArticles);
                // update import status table
                updateImportStatusForFile(importStatusAPI,importComplete);
                sendSuccessEmail(userAPI, importItems, addedProducts, updatedProducts, addedArticles, updatedArticles, fileName, mediaIndexTracker);



            }
        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on import pricelist", ex);
        }

        if (exception.hasValidationMessages()) {
            LOG.log(Level.WARNING, "Product and article import failed due to validation errors: {0}", new Object[]{exception.validationMessagesAsString()});
        }
    }

    private void sendSuccessEmail(UserAPI userAPI, List<ImportItem> importItems,
                                  List<ProductAPI> addedProducts,
                                  List<ProductAPI> updatedProducts,
                                  List<ArticleAPI> addedArticles,
                                  List<ArticleAPI> updatedArticles,
                                  String fileName,
                                  MediaIndexTracker mediaIndexTracker) throws MessagingException {
        LOG.log(Level.FINEST, "Import successful of {0} items, send success message " + mediaIndexTracker.toString(), new Object[]{importItems.size()});
        String mailBody = String.format(importProductsArticlesMailBodySuccessful,
                addedProducts.size(),
                updatedProducts.size(),
                addedArticles.size(),
                updatedArticles.size(),
                mediaIndexTracker.newImages,
                mediaIndexTracker.removedImages,
                mediaIndexTracker.newDocuments,
                mediaIndexTracker.removedDocuments,
                mediaIndexTracker.newVideos,
                mediaIndexTracker.removedVideos);
        emailController.send(userAPI.getElectronicAddress().getEmail(), fileName + ":" + importProductsArticlesMailSubjectSuccessful, mailBody);
    }

    private void sendErrorEmail(UserAPI userAPI, HjalpmedelstjanstenValidationException exception, String fileName) throws MessagingException {
        LOG.log(Level.FINEST, "Import failed, send fail message");
        StringBuilder allErrors = new StringBuilder();
        for (ErrorMessageAPI errorMessageAPI : exception.getValidationMessages()) {
            allErrors.append(errorMessageAPI.getMessage()).append("<br />");
        }
        String mailBody = String.format(importProductsArticlesMailBodyFailed, allErrors.toString());
        emailController.send(userAPI.getElectronicAddress().getEmail(), fileName + ":" + importProductsArticlesMailSubjectFailed, mailBody);
    }

    private void handleUploadAndRemovalOfMedia(long organizationUniqueId,
                                               UserAPI userAPI,
                                               String sessionId,
                                               String requestIp,
                                               HjalpmedelstjanstenValidationException exception,
                                               List<ImportMediaItem> importMediaImageItems,
                                               List<ImportMediaItem> importMediaDocumentItems,
                                               List<ImportMediaItem> importMediaVideoItems,
                                               MediaIndexTracker mediaIndexTracker) {
        if (exception.getValidationMessages().isEmpty()) {
            uploadOrDeleteImages(organizationUniqueId, userAPI, sessionId, requestIp, exception, importMediaImageItems, mediaIndexTracker);
        }

        if (exception.getValidationMessages().isEmpty()) {
            uploadOrDeleteDocuments(organizationUniqueId, userAPI, sessionId, requestIp, exception, importMediaDocumentItems, mediaIndexTracker);
        }

        if (exception.getValidationMessages().isEmpty()) {
            uploadOrDeleteVideos(organizationUniqueId, userAPI, sessionId, requestIp, exception, importMediaVideoItems, mediaIndexTracker);
        }
    }

    private void updateArticle(long organizationUniqueId, UserAPI userAPI, String sessionId, String requestIp, Map<Long, CVOrderUnit> orderUnitsIdMap, List<ArticleAPI> updatedArticles, ImportItem importItem, ArticleAPI articleAPI) throws HjalpmedelstjanstenValidationException {
        if(importItem.getMainMediaImageAPI() != null && importItem.getMainMediaImageAPI().getUrl() != null && importItem.getMainMediaImageAPI().getUrl().equalsIgnoreCase(CLEAR_VALUE_STRING))
            importItem.setDeleteMainImage(true);

        Article article = articleController.updateArticle(organizationUniqueId,
                articleAPI.getId(),
                articleAPI,
                orderUnitsIdMap,
                importItem.getCategorySpecificPropertys(),
                userAPI,
                sessionId,
                requestIp,
                false);
        if (importItem.isDeleteMainImage()) {
            mediaController.deleteArticleMainImage(article, userAPI, sessionId, requestIp);
        } else if (importItem.getMainMediaImageAPI() != null && importItem.getMainMediaImageAPI().getUrl() != null) {
            mediaController.handleMainImageUpload(article, importItem.getMainMediaImageAPI(), userAPI, sessionId, requestIp);
        }
        articleAPI = ArticleMapper.map(article, true, importItem.getCategorySpecificPropertys());
        updatedArticles.add(articleAPI);
    }

    private ImportStatusAPI createImportStatusForFile(String fileName, int status, UserAPI userAPI){
        ImportStatusAPI importStatusAPI = new ImportStatusAPI();
//        long id = 1;
//        status.setUniqueId(id);
        importStatusAPI.setFileName(fileName);
        importStatusAPI.setReadStatus(status);
        importStatusAPI.setChangedBy(userAPI.getId().toString());
        Calendar calendar = Calendar.getInstance();
        importStatusAPI.setImportStartDate(calendar.getTime());
        ImportStatusAPI returnValue = importStatusController.createImportStatusForFile(importStatusAPI);
        return returnValue;
    }

    private void updateImportStatusForFile(ImportStatusAPI importStatusAPI, int status){
        importStatusAPI.setReadStatus(status);

        importStatusController.updateImportStatus(importStatusAPI);
    }

    private void createArticle(long organizationUniqueId, UserAPI userAPI, String sessionId, String requestIp, Map<Long, CVOrderUnit> orderUnitsIdMap, List<ArticleAPI> addedArticles, ImportItem importItem, ArticleAPI articleAPI) throws HjalpmedelstjanstenValidationException {
        Article article;
        if (articleAPI.getBasedOnProduct() == null) {
            article = articleController.createArticle(organizationUniqueId,
                    articleAPI,
                    importItem.getCategory(),
                    null,
                    importItem.getCategorySpecificPropertys(),
                    orderUnitsIdMap,
                    userAPI,
                    sessionId,
                    requestIp,
                    false);
        } else {
            article = articleController.createArticle(organizationUniqueId,
                    articleAPI,
                    importItem.getCategorySpecificPropertys(),
                    orderUnitsIdMap,
                    userAPI,
                    sessionId,
                    requestIp,
                    false);
        }
        if (importItem.getMainMediaImageAPI() != null && importItem.getMainMediaImageAPI().getUrl() != null) {
            mediaController.handleMainImageUpload(article, importItem.getMainMediaImageAPI(), userAPI, sessionId, requestIp);
        }
        articleAPI = ArticleMapper.map(article, true, importItem.getCategorySpecificPropertys());
        addedArticles.add(articleAPI);
    }

    private void updateProduct(long organizationUniqueId, UserAPI userAPI, String sessionId, String requestIp, Map<Long, CVOrderUnit> orderUnitsIdMap, Map<Long, CVPackageUnit> packageUnitsIdMap, List<ProductAPI> updatedProducts, ImportItem importItem, ProductAPI productAPI) throws HjalpmedelstjanstenValidationException {
        if(importItem.getMainMediaImageAPI() != null && importItem.getMainMediaImageAPI().getUrl().equalsIgnoreCase(CLEAR_VALUE_STRING))
            importItem.setDeleteMainImage(true);

        // update
        Product product = productController.updateProduct(organizationUniqueId,
                productAPI.getId(),
                productAPI,
                orderUnitsIdMap,
                packageUnitsIdMap,
                importItem.getCategorySpecificPropertys(),
                userAPI,
                sessionId,
                requestIp,
                false);

        if (importItem.getMainMediaImageAPI() != null) {
            mediaController.handleMainImageUpload(product, importItem.getMainMediaImageAPI(), userAPI, sessionId, requestIp);
        }
        productAPI = ProductMapper.map(product, true, importItem.getCategorySpecificPropertys());
        updatedProducts.add(productAPI);
    }

    //this is only for excel
    private void createProduct(long organizationUniqueId, UserAPI userAPI, String sessionId, String requestIp, Map<Long, CVOrderUnit> orderUnitsIdMap, Map<Long, CVPackageUnit> packageUnitsIdMap, List<ProductAPI> addedProducts, ImportItem importItem, ProductAPI productAPI) throws HjalpmedelstjanstenValidationException {
        Product product = productController.createProduct(organizationUniqueId,
                productAPI,
                importItem.getCategory(),
                orderUnitsIdMap,
                packageUnitsIdMap,
                importItem.getCategorySpecificPropertys(),
                userAPI,
                sessionId,
                requestIp);
        if (importItem.getMainMediaImageAPI() != null && importItem.getMainMediaImageAPI().getUrl() != null && importItem.getMainMediaImageAPI().getAlternativeText() != null) {
            mediaController.handleMainImageUpload(product, importItem.getMainMediaImageAPI(), userAPI, sessionId, requestIp);
        }
        else {
            throw validationMessageService.generateValidationException("mainImage", "import.productarticle.product.errorUploadMainImage");
        }
        productAPI = ProductMapper.map(product, true, importItem.getCategorySpecificPropertys());
        addedProducts.add(productAPI);
    }

    private void uploadOrDeleteVideos(long organizationUniqueId, UserAPI userAPI, String sessionId, String requestIp, HjalpmedelstjanstenValidationException exception, List<ImportMediaItem> importMediaVideoItems, MediaIndexTracker mediaIndexTracker) {
        LOG.log(Level.FINEST, "videos to import: {0}", new Object[]{importMediaVideoItems == null ? 0 : importMediaVideoItems.size()});
        if (importMediaVideoItems != null && !importMediaVideoItems.isEmpty()) {
            for (ImportMediaItem importMediaItem : importMediaVideoItems) {
                try {

                    if(importMediaItem.getMediaAPI() != null && importMediaItem.getMediaAPI().getUrl().equalsIgnoreCase(CLEAR_VALUE_STRING)) {
                        removeMedia(organizationUniqueId, userAPI, sessionId, requestIp, importMediaItem);
                        mediaIndexTracker.removedVideos++;
                    }  else {
                        mediaController.handleMediaUpload(organizationUniqueId, importMediaItem.getProductId(), importMediaItem.getArticleId(), importMediaItem.getMediaAPI(), userAPI, sessionId, requestIp);
                        mediaIndexTracker.newVideos++;
                    }

                } catch (HjalpmedelstjanstenValidationException ex) {
                    for (ErrorMessageAPI errorMessageAPI : ex.getValidationMessages()) {
                        addRowErrorMessage(exception, importMediaItem.getRowNumber(), importMediaItem.getSheetName(), errorMessageAPI.getField(), errorMessageAPI.getMessage());
                    }
                    // we break immediately since the transaction is done, errors should
                    // already have been handled before we get to this stage. if validation
                    // errors still exist, they should be handled in the read-phase
                    // in ImportPricelistController
                    break;
                } catch (Exception ex) {
                    LOG.log(Level.WARNING, "Failed to handle product article import", ex);
                    //Quite obviously not an unknown exception, not sure what to do about this one - noting that this always will be a media exception. Leaving it here for now until we know what to do with it.
                    addRowErrorMessage(exception, importMediaItem.getRowNumber(), importMediaItem.getSheetName(), UNKNOWN, validationMessageService.getMessage("import.pricelist.unhandledException"));
                    // we break immediately since this is an "unhandled" error
                    break;
                }
            }
        }
    }

    private void uploadOrDeleteDocuments(long organizationUniqueId, UserAPI userAPI, String sessionId, String requestIp, HjalpmedelstjanstenValidationException exception, List<ImportMediaItem> importMediaDocumentItems, MediaIndexTracker mediaIndexTracker) {
        LOG.log(Level.FINEST, "documents to import: {0}", new Object[]{importMediaDocumentItems == null ? 0 : importMediaDocumentItems.size()});
        if (importMediaDocumentItems != null && !importMediaDocumentItems.isEmpty()) {
            for (ImportMediaItem importMediaItem : importMediaDocumentItems) {
                try {

                    if(importMediaItem.getMediaAPI() != null && importMediaItem.getMediaAPI().getUrl().equalsIgnoreCase(CLEAR_VALUE_STRING)) {
                        removeMedia(organizationUniqueId, userAPI, sessionId, requestIp, importMediaItem);
                        mediaIndexTracker.removedDocuments++;
                    } else {
                        mediaController.handleMediaUpload(organizationUniqueId, importMediaItem.getProductId(), importMediaItem.getArticleId(), importMediaItem.getMediaAPI(), userAPI, sessionId, requestIp);
                        mediaIndexTracker.newDocuments++;
                    }

                } catch (HjalpmedelstjanstenValidationException ex) {
                    for (ErrorMessageAPI errorMessageAPI : ex.getValidationMessages()) {
                        addRowErrorMessage(exception, importMediaItem.getRowNumber(), importMediaItem.getSheetName(), errorMessageAPI.getField(), errorMessageAPI.getMessage());
                    }
                    // we break immediately since the transaction is done, errors should
                    // already have been handled before we get to this stage. if validation
                    // errors still exist, they should be handled in the read-phase
                    // in ImportPricelistController
                    break;
                } catch (Exception ex) {
                    LOG.log(Level.WARNING, "Failed to handle product article import", ex);
                    //Quite obviously not an unknown exception, not sure what to do about this one - noting that this always will be a media exception. Leaving it here for now until we know what to do with it.
                    addRowErrorMessage(exception, importMediaItem.getRowNumber(), importMediaItem.getSheetName(), UNKNOWN, validationMessageService.getMessage("import.pricelist.unhandledException"));
                    // we break immediately since this is an "unhandled" error
                    break;
                }
            }
        }
    }

    private void uploadOrDeleteImages(long organizationUniqueId, UserAPI userAPI, String sessionId, String requestIp, HjalpmedelstjanstenValidationException exception, List<ImportMediaItem> importMediaImageItems, MediaIndexTracker mediaIndexTracker) {
        LOG.log(Level.FINEST, "images to import: {0}", new Object[]{importMediaImageItems == null ? 0 : importMediaImageItems.size()});
        if (importMediaImageItems != null && !importMediaImageItems.isEmpty()) {
            for (ImportMediaItem importMediaItem : importMediaImageItems) {
                try {
                    if(importMediaItem.getMediaAPI() != null && importMediaItem.getMediaAPI().getUrl().equalsIgnoreCase(CLEAR_VALUE_STRING)) {
                        removeMedia(organizationUniqueId, userAPI, sessionId, requestIp, importMediaItem);
                        mediaIndexTracker.removedImages++;
                    } else {
                        mediaController.handleMediaUpload(organizationUniqueId, importMediaItem.getProductId(), importMediaItem.getArticleId(), importMediaItem.getMediaAPI(), userAPI, sessionId, requestIp);
                        mediaIndexTracker.newImages++;
                    }

                } catch (HjalpmedelstjanstenValidationException ex) {
                    for (ErrorMessageAPI errorMessageAPI : ex.getValidationMessages()) {
                        addRowErrorMessage(exception, importMediaItem.getRowNumber(), importMediaItem.getSheetName(), errorMessageAPI.getField(), errorMessageAPI.getMessage());
                    }
                    // we break immediately since the transaction is done, errors should
                    // already have been handled before we get to this stage. if validation
                    // errors still exist, they should be handled in the read-phase
                    // in ImportPricelistController
                    break;
                } catch (Exception ex) {
                    LOG.log(Level.WARNING, "Failed to handle product article import", ex);
                    //Quite obviously not an unknown exception, not sure what to do about this one - noting that this always will be a media exception. Leaving it here for now until we know what to do with it.
                    addRowErrorMessage(exception, importMediaItem.getRowNumber(), importMediaItem.getSheetName(), UNKNOWN, validationMessageService.getMessage("import.pricelist.unhandledException"));
                    // we break immediately since this is an "unhandled" error
                    break;
                }
            }
        }
    }

    private void removeMedia(long organizationUniqueId, UserAPI userAPI, String sessionId, String requestIp, ImportMediaItem importMediaItem) throws HjalpmedelstjanstenValidationException {
        //long organizationUniqueId, long articleUniqueId, long mediaUniqueId, UserAPI userAPI, String sessionId, String requestIp
        if( importMediaItem.getArticleId() != null ) {
            mediaController.deleteArticleMedia(organizationUniqueId, importMediaItem.getArticleId(), importMediaItem.getMediaAPI().getId(), userAPI, sessionId, requestIp);
        } else {
            mediaController.deleteProductMedia(organizationUniqueId, importMediaItem.getProductId(), importMediaItem.getMediaAPI().getId(), userAPI, sessionId, requestIp);
        }
    }

    private List<ImportItem> readItems(long organizationUniqueId,
                                       XSSFWorkbook workbook,
                                       Organization organization,
                                       List<CVOrderUnit> allOrderUnits,
                                       List<CVPackageUnit> allPackageUnits,
                                       HjalpmedelstjanstenValidationException exception,
                                       UserAPI userAPI,
                                       String sessionId,
                                       String requestIp,
                                       List<ArticleAPI> updatedArticles) {
        List<ImportItem> importItems = new ArrayList<>();
        XSSFSheet categoriesSheet = workbook.getSheet(CATEGORIES_SHEET_NAME);
        int numberOfCategories = categoriesSheet.getLastRowNum();
        List<CVOrderUnitAPI> allOrderUnitAPIs = OrderUnitMapper.map(allOrderUnits);
        List<CVPackageUnitAPI> allPackageUnitAPIs = PackageUnitMapper.map(allPackageUnits);
        Map<String, CVOrderUnitAPI> orderUnitsNameMap = orderUnitListToNameMap(allOrderUnitAPIs);
        Map<String, CVPackageUnitAPI> packageUnitsNameMap = packageUnitListToNameMap(allPackageUnitAPIs);
        for (int i = 0; i <= numberOfCategories; i++) {
            XSSFRow row = categoriesSheet.getRow(i);
            XSSFCell categoryIdCell = row.getCell(0);
            long categoryUniqueId = (long) categoryIdCell.getNumericCellValue();
            XSSFCell categoryNameCell = row.getCell(1);
            String categoryName = this.importExcelHelper.parseCellDataToStringType(categoryNameCell);


            XSSFCell categoryCodeCell = row.getCell(2);
            String categoryCode = null;
            if (categoryCodeCell != null) {
                categoryCode = this.importExcelHelper.parseCellDataToStringType(categoryCodeCell);
            }
            XSSFCell sheetNameCell = row.getCell(3);
            String sheetName = this.importExcelHelper.parseCellDataToStringType(sheetNameCell);
            Category category = categoryController.getById(categoryUniqueId);
            if (category == null) {
                addRowErrorMessage(exception, row.getRowNum(), categoriesSheet.getSheetName(), "file", validationMessageService.getMessage("import.productarticle.category.notExist", categoryName));
                continue;
            }
            List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(categoryUniqueId);
            LOG.log(Level.FINEST, "Found categoryUniqueId: {0} name: {1}, code: {2}, sheetName: {3}", new Object[]{categoryUniqueId, categoryName, categoryCode, sheetName});
            XSSFSheet categorySheet = workbook.getSheet(sheetName);
            if (categorySheet != null) {
                LOG.log(Level.FINEST, "Found sheet for sheetName: {0}", new Object[]{sheetName});
                XSSFRow fieldNamesRow = categorySheet.getRow(0);
                List<XSSFRow> productRows = new ArrayList<>();
                List<XSSFRow> articleRows = new ArrayList<>();
                int lastRowNumber = categorySheet.getLastRowNum();
                Set<String> productArticleNumbersAlreadyHandled = new HashSet<>();
                // first two rows are just headers and field info
                for (int rowNumber = startFromColumnTwo(); rowNumber <= lastRowNumber; rowNumber++) {
                    XSSFRow productArticleRow = categorySheet.getRow(rowNumber);
                    if (isNewOrChanged(productArticleRow)) {

                        XSSFCell customerUniqueCell = this.importExcelHelper.getCellByName(fieldNamesRow, productArticleRow, FIELD_CUSTOMERUNIQUE_NAME);
                        if(customerUniqueCell != null){
                            if (customerUniqueCell.getStringCellValue().equalsIgnoreCase(YES)){
                                customerUniqueCell.setCellValue(YES);
                            }
                            if (customerUniqueCell.getStringCellValue().equalsIgnoreCase(NO)){
                                customerUniqueCell.setCellValue(NO);
                            }
                        }

                        XSSFCell ceMarkedCell = this.importExcelHelper.getCellByName(fieldNamesRow, productArticleRow, FIELD_CEMARKED_NAME);
                        if(ceMarkedCell != null){
                            if (ceMarkedCell.getStringCellValue().equalsIgnoreCase(YES)){
                                ceMarkedCell.setCellValue(YES);
                            }
                            if (ceMarkedCell.getStringCellValue().equalsIgnoreCase(NO)){
                                ceMarkedCell.setCellValue(NO);
                            }
                        }

                        XSSFCell orderUnitCell = this.importExcelHelper.getCellByName(fieldNamesRow, productArticleRow, FIELD_ORDERUNIT_NAME);
                        if(orderUnitCell != null){
                            if (orderUnitCell.getStringCellValue().equalsIgnoreCase(STYCK))
                                orderUnitCell.setCellValue(STYCK);
                            if (orderUnitCell.getStringCellValue().equalsIgnoreCase(MILLIGRAM))
                                orderUnitCell.setCellValue(MILLIGRAM);
                            if (orderUnitCell.getStringCellValue().equalsIgnoreCase(DYGN))
                                orderUnitCell.setCellValue(DYGN);
                            if (orderUnitCell.getStringCellValue().equalsIgnoreCase(TIMME))
                                orderUnitCell.setCellValue(TIMME);
                            if (orderUnitCell.getStringCellValue().equalsIgnoreCase(KILOGRAM))
                                orderUnitCell.setCellValue(KILOGRAM);
                            if (orderUnitCell.getStringCellValue().equalsIgnoreCase(GRAM))
                                orderUnitCell.setCellValue(GRAM);
                            if (orderUnitCell.getStringCellValue().equalsIgnoreCase(METER))
                                orderUnitCell.setCellValue(METER);
                            if (orderUnitCell.getStringCellValue().equalsIgnoreCase(LITER))
                                orderUnitCell.setCellValue(LITER);
                            if (orderUnitCell.getStringCellValue().equalsIgnoreCase(MILLILITER))
                                orderUnitCell.setCellValue(MILLILITER);
                        }

                        XSSFCell articleQuantityInOuterPackageUnit = this.importExcelHelper.getCellByName(fieldNamesRow, productArticleRow, FIELD_ARTICLEQUANTITYINOUTERPACKAGEUNIT_NAME);
                        if(articleQuantityInOuterPackageUnit != null){
                            if (articleQuantityInOuterPackageUnit.getStringCellValue().equalsIgnoreCase(STYCK))
                                articleQuantityInOuterPackageUnit.setCellValue(STYCK);
                            if (articleQuantityInOuterPackageUnit.getStringCellValue().equalsIgnoreCase(MILLIGRAM))
                                articleQuantityInOuterPackageUnit.setCellValue(MILLIGRAM);
                            if (articleQuantityInOuterPackageUnit.getStringCellValue().equalsIgnoreCase(DYGN))
                                articleQuantityInOuterPackageUnit.setCellValue(DYGN);
                            if (articleQuantityInOuterPackageUnit.getStringCellValue().equalsIgnoreCase(TIMME))
                                articleQuantityInOuterPackageUnit.setCellValue(TIMME);
                            if (articleQuantityInOuterPackageUnit.getStringCellValue().equalsIgnoreCase(KILOGRAM))
                                articleQuantityInOuterPackageUnit.setCellValue(KILOGRAM);
                            if (articleQuantityInOuterPackageUnit.getStringCellValue().equalsIgnoreCase(GRAM))
                                articleQuantityInOuterPackageUnit.setCellValue(GRAM);
                            if (articleQuantityInOuterPackageUnit.getStringCellValue().equalsIgnoreCase(METER))
                                articleQuantityInOuterPackageUnit.setCellValue(METER);
                            if (articleQuantityInOuterPackageUnit.getStringCellValue().equalsIgnoreCase(LITER))
                                articleQuantityInOuterPackageUnit.setCellValue(LITER);
                            if (articleQuantityInOuterPackageUnit.getStringCellValue().equalsIgnoreCase(MILLILITER))
                                articleQuantityInOuterPackageUnit.setCellValue(MILLILITER);
                        }

                        XSSFCell packageContentUnit = this.importExcelHelper.getCellByName(fieldNamesRow, productArticleRow, FIELD_PACKAGECONTENTUNIT_NAME);
                        if(packageContentUnit != null){
                            if (packageContentUnit.getStringCellValue().equalsIgnoreCase(STYCK))
                                packageContentUnit.setCellValue(STYCK);
                            if (packageContentUnit.getStringCellValue().equalsIgnoreCase(MILLIGRAM))
                                packageContentUnit.setCellValue(MILLIGRAM);
                            if (packageContentUnit.getStringCellValue().equalsIgnoreCase(DYGN))
                                packageContentUnit.setCellValue(DYGN);
                            if (packageContentUnit.getStringCellValue().equalsIgnoreCase(TIMME))
                                packageContentUnit.setCellValue(TIMME);
                            if (packageContentUnit.getStringCellValue().equalsIgnoreCase(KILOGRAM))
                                packageContentUnit.setCellValue(KILOGRAM);
                            if (packageContentUnit.getStringCellValue().equalsIgnoreCase(GRAM))
                                packageContentUnit.setCellValue(GRAM);
                            if (packageContentUnit.getStringCellValue().equalsIgnoreCase(METER))
                                packageContentUnit.setCellValue(METER);
                            if (packageContentUnit.getStringCellValue().equalsIgnoreCase(LITER))
                                packageContentUnit.setCellValue(LITER);
                            if (packageContentUnit.getStringCellValue().equalsIgnoreCase(MILLILITER))
                                packageContentUnit.setCellValue(MILLILITER);
                            if (packageContentUnit.getStringCellValue().equalsIgnoreCase(PAR))
                                packageContentUnit.setCellValue(PAR);
                            if (packageContentUnit.getStringCellValue().equalsIgnoreCase(SATS))
                                packageContentUnit.setCellValue(SATS);
                        }



                        // get type
                        XSSFCell typeCell = this.importExcelHelper.getCellByName(fieldNamesRow, productArticleRow, FIELD_TYPE_NAME);
                        if (typeCell == null) {
                            //If the typeCell is null it means that the user deleted a field in excel, we need to accept this happening. We can't see a valid reason to keep this in, but that might change.
                            //LOG.log(Level.FINEST, "Row {0} contains a deleted cell, ignoring.", new Object[]{rowNumber + 1});
                            //HJAL-2180
                            addRowErrorMessage(exception, rowNumber, sheetName, "file", validationMessageService.getMessage("import.productarticle.invalidfile.typeNotFound"));
                        } else {
                            String productArticleNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productArticleRow, FIELD_NUMBER_NAME);
                            String type = typeCell.getStringCellValue();
                            if (!productArticleNumbersAlreadyHandled.contains(productArticleNumber)) {
                                productArticleNumbersAlreadyHandled.add(productArticleNumber);
                            }
                            else { //rad med samma produkt-/artikelnummer redan behandlad
                                //ta bort den existerande raden innan vi lägger till den nya
                                if(type.equals(PRODUCT_TYPE_NAME))
                                {
                                    for (int rowNr = 0; rowNr < productRows.size(); rowNr++ )
                                    {
                                        if( productRows.get(rowNr).getCell(2).toString().equals(productArticleNumber))
                                        {
                                            productRows.remove(productRows.get(rowNr));
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    for (int rowNr = 0; rowNr < articleRows.size(); rowNr++ ) {
                                        if( articleRows.get(rowNr).getCell(2).toString().equals(productArticleNumber)) {
                                            articleRows.remove(articleRows.get(rowNr));
                                            break;
                                        }
                                    }
                                }

                               /* Bugg vid flera artiklar.
                               for (int rowNr = 0; rowNr < productArticleNumbersAlreadyHandled.size(); rowNr++) {
                                    if (type.equals(PRODUCT_TYPE_NAME) && productRows.get(rowNr).getCell(2).toString() == productArticleNumber)
                                        productRows.remove(productRows.get(rowNr));
                                    if (type.equals(ARTICLE_TYPE_NAME) && articleRows.get(rowNr).getCell(2).toString() == productArticleNumber)
                                        articleRows.remove(articleRows.get(rowNr));
                                }*/
                            }
                            if (type.equals(PRODUCT_TYPE_NAME)) {
                                productRows.add(productArticleRow);
                            } else {
                                articleRows.add(productArticleRow);
                            }
                        }
                    } else {
                        LOG.log(Level.FINEST, "excel Row {0} is not new and not changed, ignoring.", new Object[]{rowNumber + 1});
                    }
                }
                Map<String, ProductAPI> productNumbersAdded = new HashMap<>();
                if (!productRows.isEmpty()) {
                    for (XSSFRow productRow : productRows) {
                        // id
                        XSSFCell uniqueIdCell = productRow.getCell(0);
                        Long uniqueId = uniqueIdCell == null ? null : (long) uniqueIdCell.getNumericCellValue();
                        LOG.log(Level.FINEST, "Found product unique id: {0}", new Object[]{uniqueId});

                        //if uniqueId == null, try to get it from existing
                        String productNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_NUMBER_NAME);
                        if (uniqueId == null) {
                            uniqueId = productController.findByProductNumberAndOrganization(productNumber, organization.getUniqueId()) == null ? null : productController.findByProductNumberAndOrganization(productNumber, organization.getUniqueId()).getUniqueId();
                        }

                        ProductAPI productAPI = uniqueId == null ? new ProductAPI() : productController.getProductAPI(organizationUniqueId, uniqueId, userAPI, sessionId, requestIp);
                        productAPI = fillProductAPI(organization, productRow, fieldNamesRow, productAPI, updatedArticles, categorySpecificPropertys, category, orderUnitsNameMap, packageUnitsNameMap, categorySheet.getSheetName(), exception, userAPI);
                        Set<ErrorMessageAPI> errorMessageAPIs = null;
                        if (productAPI.getId() == null) {
                            // create
                            errorMessageAPIs = productValidation.tryForCreate(organizationUniqueId, productAPI);
                        } else {
                            // update
                            Product product = productController.getProduct(organizationUniqueId, productAPI.getId(), userAPI);
                            //if (product.getStatus() != Product.Status.DISCONTINUED) {
                            errorMessageAPIs = productValidation.tryForUpdate(organizationUniqueId, productAPI, product, userAPI, false);
                            /*} else {
                                LOG.log(Level.FINEST, "Product {0} is in status DISCONTINUED and cannot be updated", new Object[]{product.getUniqueId()});
                            } */
                        }
//                        if(productAPI.getStatus().equals(Product.Status.DISCONTINUED)){
//                            addRowErrorMessage(exception, productRow.getRowNum(), categorySheet.getSheetName(), "1", "asd");
//                        }

                        if (errorMessageAPIs != null && !errorMessageAPIs.isEmpty()) {
                            for (ErrorMessageAPI errorMessageAPI : errorMessageAPIs) {
                                addRowErrorMessage(exception, productRow.getRowNum(), categorySheet.getSheetName(), errorMessageAPI.getField(), errorMessageAPI.getMessage());
                            }
                        }
                        ImportItem importItem = new ImportItem(productAPI, category, categorySpecificPropertys, productRow.getRowNum(), sheetName);
                        fillMainImageMediaAPI(productRow, fieldNamesRow, productAPI.getId(), null, importItem, exception, sheetName);
                        importItems.add(importItem);
                        if (productAPI.getProductNumber() != null) {
                            productNumbersAdded.put(productAPI.getProductNumber(), productAPI);
                        }
                    }
                }
                if (!articleRows.isEmpty()) {
                    for (XSSFRow articleRow : articleRows) {
                        XSSFCell uniqueIdCell = articleRow.getCell(0);
                        Long uniqueId = uniqueIdCell == null ? null : (long) uniqueIdCell.getNumericCellValue();
                        LOG.log(Level.FINEST, "Found article unique id: {0}", new Object[]{uniqueId});

                        //if uniqueId == null, try to get it from existing
                        String articleNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_NUMBER_NAME);
                        if (uniqueId == null) {
                            uniqueId = articleController.findByArticleNumberAndOrganization(articleNumber, organization.getUniqueId()) == null ? null : articleController.findByArticleNumberAndOrganization(articleNumber, organization.getUniqueId()).getUniqueId();
                        }

                        ArticleAPI articleAPI = uniqueId == null ? new ArticleAPI() : articleController.getArticleAPI(organizationUniqueId, uniqueId, userAPI, sessionId, requestIp);
                        try {
                            fillArticleAPI(organization, articleRow, fieldNamesRow, articleAPI, categorySpecificPropertys, category, orderUnitsNameMap, packageUnitsNameMap, productNumbersAdded, categorySheet.getSheetName(), exception);
                            Set<ErrorMessageAPI> errorMessageAPIs = null;

                            //HJAL-2105 ska inte gå att sätta en artikel till EJ kundunik om den baseras på en kundunik produkt
                            if (articleAPI.getBasedOnProduct() != null) {
                                if (articleAPI.getBasedOnProduct().isCustomerUnique() && !articleAPI.isCustomerUnique()) {
                                    errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("customerUnique", validationMessageService.getMessage("article.customerUnique.cannotOverride")));
                                }
                            }

                            if (articleAPI.getId() == null) {
                                // create
                                errorMessageAPIs = articleValidation.tryForCreate(organizationUniqueId, articleAPI);
                            } else {
                                // update
                                Article article = articleController.getArticle(organizationUniqueId, articleAPI.getId(), userAPI);
                                /*if (article.getStatus() != Product.Status.DISCONTINUED) { */
                                errorMessageAPIs = articleValidation.tryForUpdate(organizationUniqueId, articleAPI, article,userAPI);
                                /*} else {
                                    LOG.log(Level.FINEST, "Article {0} is in status DISCONTINUED and cannot be updated", new Object[]{article.getUniqueId()});
                                } */
                            }
                            if (errorMessageAPIs != null && !errorMessageAPIs.isEmpty()) {
                                for (ErrorMessageAPI errorMessageAPI : errorMessageAPIs) {
                                    addRowErrorMessage(exception, articleRow.getRowNum(), categorySheet.getSheetName(), errorMessageAPI.getField(), errorMessageAPI.getMessage());
                                }
                            }
                        } catch (HjalpmedelstjanstenValidationException ex) {
                            for (ErrorMessageAPI errorMessageAPI : ex.getValidationMessages()) {
                                addRowErrorMessage(exception, articleRow.getRowNum(), categorySheet.getSheetName(), errorMessageAPI.getField(), errorMessageAPI.getMessage());
                            }
                        }
                        ImportItem importItem = new ImportItem(articleAPI, category, categorySpecificPropertys, articleRow.getRowNum(), sheetName);
                        fillMainImageMediaAPI(articleRow, fieldNamesRow, null, articleAPI.getId(), importItem, exception, sheetName);
                        importItems.add(importItem);
                    }
                }
            } else {
                LOG.log(Level.WARNING, "No sheet found for name: {0}", new Object[]{sheetName});
            }
        }
        return importItems;
    }

    private List<ImportMediaItem> readImages(XSSFSheet imagesSheet,
                                             Organization organization,
                                             UserAPI userAPI,
                                             HjalpmedelstjanstenValidationException exception
    ) {
        List<ImportMediaItem> importMediaItems = new ArrayList<>();
        XSSFRow fieldNamesRow = imagesSheet.getRow(0);
        // first two rows are just headers and field info
        int i = startFromColumnTwo();
        XSSFRow row;
        while (!isEmptyMediaRow(row = imagesSheet.getRow(i), fieldNamesRow)) {
            LOG.log(Level.FINEST, "Images row {0} is not empty", new Object[]{i});
            i++;
            XSSFCell uniqueIdCell = row.getCell(0);
            Long uniqueId = uniqueIdCell == null ? null : (long) uniqueIdCell.getNumericCellValue();
            Long productId = null;
            Long articleId = null;
            MediaImageAPI mediaImageAPI;
            // if we don't get media unique id from hidden excel field this is a new
            // row and we must from number determine whether it's product or article
            if (uniqueId == null) {
                mediaImageAPI = new MediaImageAPI();
                String articleOrProductNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_NUMBER_NAME);
                Product product = productController.findByProductNumberAndOrganization(articleOrProductNumber, organization.getUniqueId());
                Article article = null;
                if (product == null) {
                    article = articleController.findByArticleNumberAndOrganization(articleOrProductNumber, organization.getUniqueId());
                    if (article == null) {
                        String message = validationMessageService.getMessage("media.article.notExists");
                        addRowErrorMessage(exception, row.getRowNum(), imagesSheet.getSheetName(), "article", message);
                    } else {
                        articleId = article.getUniqueId();
                    }
                } else {
                    productId = product.getUniqueId();
                }
            } else {
                // get existing product or article media
                ArticleMediaImage articleMediaImage = mediaController.getArticleMediaImage(organization.getUniqueId(), uniqueId, userAPI);
                if (articleMediaImage != null) {
                    mediaImageAPI = MediaMapper.mapArticleImage(articleMediaImage);
                    articleId = articleMediaImage.getArticle().getUniqueId();
                } else {
                    MediaImage mediaImage = mediaController.getProductMediaImage(organization.getUniqueId(), uniqueId, userAPI);
                    mediaImageAPI = MediaMapper.mapImage(mediaImage);
                    productId = mediaImage.getProduct().getUniqueId();
                }
            }
            boolean isNewOrChanged = fillMediaAPI(row, fieldNamesRow, mediaImageAPI, organization);
            if (isNewOrChanged) {
                ImportMediaItem importMediaItem = new ImportMediaItem(mediaImageAPI, productId, articleId, imagesSheet.getSheetName(), row.getRowNum());
                importMediaItems.add(importMediaItem);
            }
        }
        return importMediaItems;
    }

    //dumbest ever
    private int startFromColumnTwo() {
        return 2;
    }


    private List<ImportMediaItem> readDocuments(XSSFSheet documentsSheet,
                                                Organization organization,
                                                UserAPI userAPI,
                                                HjalpmedelstjanstenValidationException exception
    ) {
        List<ImportMediaItem> importMediaItems = new ArrayList<>();
        XSSFRow fieldNamesRow = documentsSheet.getRow(0);
        // first two rows are just headers and field info
        int i = startFromColumnTwo();
        XSSFRow row;
        while (!isEmptyMediaRow(row = documentsSheet.getRow(i), fieldNamesRow)) {
            i++;
            XSSFCell uniqueIdCell = row.getCell(0);
            Long uniqueId = uniqueIdCell == null ? null : (long) uniqueIdCell.getNumericCellValue();
            Long productId = null;
            Long articleId = null;
            MediaDocumentAPI mediaDocumentAPI;
            // if we don't get media unique id from hidden excel field this is a new
            // row and we must from number determine whether it's product or article
            if (uniqueId == null) {
                mediaDocumentAPI = new MediaDocumentAPI();
                String articleOrProductNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_NUMBER_NAME);
                Product product = productController.findByProductNumberAndOrganization(articleOrProductNumber, organization.getUniqueId());
                Article article = null;
                if (product == null) {
                    article = articleController.findByArticleNumberAndOrganization(articleOrProductNumber, organization.getUniqueId());
                    if (article == null) {
                        String message = validationMessageService.getMessage("media.article.notExists");
                        addRowErrorMessage(exception, row.getRowNum(), documentsSheet.getSheetName(), "article", message);
                    } else {
                        articleId = article.getUniqueId();
                    }
                } else {
                    productId = product.getUniqueId();
                }
            } else {
                // get existing product or article media
                ArticleMediaDocument articleMediaDocument = mediaController.getArticleMediaDocument(organization.getUniqueId(), uniqueId, userAPI);
                if (articleMediaDocument != null) {
                    mediaDocumentAPI = MediaMapper.mapArticleDocument(articleMediaDocument);
                    articleId = articleMediaDocument.getArticle().getUniqueId();
                } else {
                    MediaDocument mediaDocument = mediaController.getProductMediaDocument(organization.getUniqueId(), uniqueId, userAPI);
                    mediaDocumentAPI = MediaMapper.mapDocument(mediaDocument);
                    productId = mediaDocument.getProduct().getUniqueId();
                }
            }

            boolean isNewOrChanged = fillMediaAPI(row, fieldNamesRow, mediaDocumentAPI, organization);
            if (isNewOrChanged) {
                ImportMediaItem importMediaItem = new ImportMediaItem(mediaDocumentAPI, productId, articleId, documentsSheet.getSheetName(), row.getRowNum());
                importMediaItems.add(importMediaItem);
            }
        }
        return importMediaItems;
    }

    private List<ImportMediaItem> readVideos(XSSFSheet videosSheet,
                                             Organization organization,
                                             UserAPI userAPI,
                                             HjalpmedelstjanstenValidationException exception
    ) {
        List<ImportMediaItem> importMediaItems = new ArrayList<>();
        XSSFRow fieldNamesRow = videosSheet.getRow(0);
        // first two rows are just headers and field info
        int i = startFromColumnTwo();
        XSSFRow row;
        while (!isEmptyMediaRow(row = videosSheet.getRow(i), fieldNamesRow)) {
            i++;
            XSSFCell uniqueIdCell = row.getCell(0);
            Long uniqueId = uniqueIdCell == null ? null : (long) uniqueIdCell.getNumericCellValue();
            Long productId = null;
            Long articleId = null;
            MediaVideoAPI mediaVideoAPI;
            // if we don't get media unique id from hidden excel field this is a new
            // row and we must from number determine whether it's product or article
            if (uniqueId == null) {
                mediaVideoAPI = new MediaVideoAPI();
                String articleOrProductNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_NUMBER_NAME);
                Product product = productController.findByProductNumberAndOrganization(articleOrProductNumber, organization.getUniqueId());
                Article article = null;
                if (product == null) {
                    article = articleController.findByArticleNumberAndOrganization(articleOrProductNumber, organization.getUniqueId());
                    if (article == null) {
                        String message = validationMessageService.getMessage("media.article.notExists");
                        addRowErrorMessage(exception, row.getRowNum(), videosSheet.getSheetName(), "article", message);
                    } else {
                        articleId = article.getUniqueId();
                    }
                } else {
                    productId = product.getUniqueId();
                }
            } else {
                // get existing product or article media
                ArticleMediaVideo articleMediaVideo = mediaController.getArticleMediaVideo(organization.getUniqueId(), uniqueId, userAPI);
                if (articleMediaVideo != null) {
                    mediaVideoAPI = MediaMapper.mapArticleVideo(articleMediaVideo);
                    articleId = articleMediaVideo.getArticle().getUniqueId();
                } else {
                    MediaVideo mediaVideo = mediaController.getProductMediaVideo(organization.getUniqueId(), uniqueId, userAPI);
                    mediaVideoAPI = MediaMapper.mapVideo(mediaVideo);
                    productId = mediaVideo.getProduct().getUniqueId();
                }
            }

            boolean isNewOrChanged = fillMediaAPI(row, fieldNamesRow, mediaVideoAPI, organization);
            if (isNewOrChanged) {
                ImportMediaItem importMediaItem = new ImportMediaItem(mediaVideoAPI, productId, articleId, videosSheet.getSheetName(), row.getRowNum());
                importMediaItems.add(importMediaItem);
            }
        }
        return importMediaItems;
    }

    private boolean fillMediaAPI(XSSFRow row,
                                 XSSFRow fieldNamesRow,
                                 MediaAPI mediaAPI,
                                 Organization organization) {
        LOG.log(Level.FINEST, "fillMediaAPI( organization->uniqueId: {0}, mediaAPI->id: {1} )", new Object[]{organization.getUniqueId(), mediaAPI.getId()});
        boolean isNewOrChanged = false;
        if (mediaAPI.getId() == null) {
            isNewOrChanged = true;
        }

        // url
        String url = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_MEDIA_URL_NAME);
        if (url != null) {
            mediaAPI.setUrl(url);
            isNewOrChanged = true;
        }

        // url
        String description = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_MEDIA_DESCRIPTION_NAME);
        if (description != null) {
            mediaAPI.setDescription(description);
            isNewOrChanged = true;
        }

        // url
        String altText = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_MEDIA_ALTTEXT_NAME);
        if (altText != null) {
            mediaAPI.setAlternativeText(altText);
            isNewOrChanged = true;
        }

        // document type
        String documentType = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_MEDIA_DOCUMENTTYPE_NAME);
        if (documentType != null) {
            CVDocumentTypeAPI documentTypeAPI = new CVDocumentTypeAPI();
            documentTypeAPI.setValue(documentType);
            MediaDocumentAPI mediaDocumentAPI = (MediaDocumentAPI) mediaAPI;
            mediaDocumentAPI.setDocumentType(documentTypeAPI);
            isNewOrChanged = true;
        }

        return isNewOrChanged;
    }

    private boolean isEmptyMediaRow(XSSFRow row, XSSFRow fieldNamesRow) {
        LOG.log(Level.FINEST, "Check if row {0} is empty", new Object[]{row == null ? null : row.getRowNum()});
        if (row == null) {
            return true;
        }
        XSSFCell uniqueIdCell = row.getCell(0);
        Long uniqueId = uniqueIdCell == null ? null : (long) uniqueIdCell.getNumericCellValue();
        if (uniqueId == null) {
            String articleOrProductNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_NUMBER_NAME);
            if (articleOrProductNumber == null || articleOrProductNumber.isEmpty()) {
                return true;
            }
        }
        return false;
    }



    private ProductAPI fillProductAPI(
            Organization organization,
            XSSFRow productRow,
            XSSFRow fieldNamesRow,
            ProductAPI productAPI,
            List<ArticleAPI> updatedArticles,
            List<CategorySpecificProperty> categorySpecificPropertys,
            Category category,
            Map<String, CVOrderUnitAPI> orderUnitsNameMap,
            Map<String, CVPackageUnitAPI> packageUnitsNameMap,
            String sheetName,
            HjalpmedelstjanstenValidationException exception,
            UserAPI userAPI) {
        LOG.log(Level.FINEST, "validateProductImport( organization->uniqueId: {0} )", new Object[]{organization.getUniqueId()});

        /*if (productAPI.getId() == null) {
            productAPI.setStatus(Product.Status.PUBLISHED.toString());
        } */

        productAPI.setOrganizationId(organization.getUniqueId());
        productAPI.setOrganizationName(organization.getOrganizationName());

        // name
        String productName = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_NAME_NAME);
        if (productName != null && !productName.isEmpty()) {
            productAPI.setProductName(productName);
        }

        // number
        String productNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_NUMBER_NAME);
        if (productNumber != null && !productNumber.isEmpty()) {
            productAPI.setProductNumber(productNumber);
        }

        // order unit
        String orderUnitCode = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_ORDERUNIT_NAME);
        if (orderUnitCode != null && !orderUnitCode.isEmpty()) {
            CVOrderUnitAPI orderUnitAPI = getOrderUnitAPI(orderUnitCode, orderUnitsNameMap, productRow.getRowNum(), sheetName, exception);
            productAPI.setOrderUnit(orderUnitAPI);
        }
        List<ArticleAPI> articles = null;
        if (productAPI.getId() != null)
        {
            articles = productController.getArticleAPIsBasedOnProduct(productAPI.getOrganizationId(), productAPI.getId(), userAPI);
        }

        // discontinued date
        Date discontinueDate = this.importExcelHelper.getCellDateValueByName(fieldNamesRow, productRow, FIELD_DISCONTINUED_NAME);

        if (discontinueDate != null) {
            productAPI.setReplacementDate(discontinueDate.getTime());

            if (this.importExcelHelper.isProductOrArticleDiscontinued(discontinueDate)) {

                LOG.log(Level.FINEST, "CategoryID is: " + productAPI.getCategory());

                productAPI.setStatus(Product.Status.DISCONTINUED.toString());

                if (articles != null) {

                    for (ArticleAPI articleAPI : articles) {

                        articleAPI.setStatus(Product.Status.DISCONTINUED.toString());
                        updatedArticles.add(articleAPI);

                        LOG.log(Level.FINEST, "ArticleName: " + articleAPI.getArticleName() + " ArticleNumber: " + articleAPI.getArticleNumber());
                    }
                }

            } else {
                productAPI.setStatus(Product.Status.PUBLISHED.toString());
            }
        }
        else
        {
            productAPI.setStatus(Product.Status.PUBLISHED.toString());
        }

        // inactivate rows on discontinued date
        String inactivateRowsOnDiscontinueDate = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_INACTIVATE_ROWS_ON_DISCONTINUED_NAME);
        if (inactivateRowsOnDiscontinueDate != null) {
            productAPI.setInactivateRowsOnReplacement(YES.equals(inactivateRowsOnDiscontinueDate));

            if(articles != null) {

                for (ArticleAPI article : articles) {
                    article.setInactivateRowsOnReplacement(YES.equals(inactivateRowsOnDiscontinueDate));
                }
            }
        }


        String replacedByProduct = this.importExcelHelper.getCellStringValueByName(fieldNamesRow,productRow, FIELD_REPLACEDBY_NAME);
        if (replacedByProduct != null) {
            String productNumbers[] = replacedByProduct.split(",");
            List<ProductAPI> listOfReplacedProducts = new ArrayList<>();
            for(String replacedBy : productNumbers)
            {
                ProductAPI replacedByProductAPI = productController.getProductAPIByProductNumber(organization.getUniqueId(), replacedBy);
                listOfReplacedProducts.add(replacedByProductAPI);
            }
            productAPI.setReplacedByProducts(listOfReplacedProducts);
        }




        // main category
        CategoryAPI categoryAPI = CategoryMapper.map(category);
        if (categoryAPI != null) {
            productAPI.setCategory(categoryAPI);
        }

        // extended categories
        String extendedCategories = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_EXTENDED_CATEGORIES_NAME);
        if (extendedCategories != null) {
            List<CategoryAPI> categoryAPIs = getExtendedCategories(extendedCategories, productRow.getRowNum(), sheetName, exception);
            productAPI.setExtendedCategories(categoryAPIs);
        }

        // supplementary information
        String supplementaryInformation = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_SUPPLEMENTARYINFORMATION_NAME);
        if (supplementaryInformation != null && !supplementaryInformation.isEmpty()) {
            productAPI.setSupplementedInformation(possiblyClearValue(supplementaryInformation));
        }

        // description for 1177
        String descriptionForElvasjuttiosju = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_DESCRIPTION_FOR_ELVASJUTTIOSJU);
        if (descriptionForElvasjuttiosju != null && !descriptionForElvasjuttiosju.isEmpty()) {
            productAPI.setDescriptionForElvasjuttiosju(possiblyClearValue(descriptionForElvasjuttiosju));
        }

        // mainImage url
        String mainImageUrl = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_MEDIA_URL_NAME);
        if (mainImageUrl != null && !mainImageUrl.isEmpty()) {
            productAPI.setMainImageUrl(mainImageUrl);
        }

        // mainImage altText
        String mainImageAltText = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_MEDIA_ALTTEXT_NAME);
        if (mainImageAltText != null && !mainImageAltText.isEmpty()) {
            productAPI.setMainImageAltText(mainImageAltText);
        }

        //mainImage description
        String mainImageDescription = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_MEDIA_DESCRIPTION_NAME);
        if (mainImageDescription != null && !mainImageDescription.isEmpty()) {
            productAPI.setMainImageDescription(mainImageDescription);
        }

        // manufacturer
        String manufacturer = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_MANUFACTURER_NAME);
        if (manufacturer != null && !manufacturer.isEmpty()) {
            productAPI.setManufacturer(possiblyClearValue(manufacturer));
        }

        // trademark
        String trademark = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_TRADEMARK_NAME);
        if (trademark != null && !trademark.isEmpty()) {
            productAPI.setTrademark(possiblyClearValue(trademark));
        }

        // manufacturer web page
        String manufacturerWebPage = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_MANUFACTURERWEB_NAME);
        if (manufacturerWebPage != null && !manufacturerWebPage.isEmpty()) {
            if (CLEAR_VALUE_STRING.equals(manufacturerWebPage)) {
                productAPI.setManufacturerElectronicAddress(null);
            } else {
                ElectronicAddressAPI electronicAddressAPI = productAPI.getManufacturerElectronicAddress() == null ? new ElectronicAddressAPI() : productAPI.getManufacturerElectronicAddress();
                electronicAddressAPI.setWeb(manufacturerWebPage);
                productAPI.setManufacturerElectronicAddress(electronicAddressAPI);
            }
        }

        // customer unique
        String customerUnique = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_CUSTOMERUNIQUE_NAME);
        if (customerUnique != null) {
            if (YES.equalsIgnoreCase(customerUnique)) {
                productAPI.setCustomerUnique(true);
            } else {
                productAPI.setCustomerUnique(false);
            }
        }

        // article quantity in outer package || Antal förpackningar
        Double articleQuantityInOuterPackage = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, productRow, FIELD_ARTICLEQUANTITYINOUTERPACKAGE_NAME);
        if (articleQuantityInOuterPackage != null) {
            productAPI.setArticleQuantityInOuterPackage(possiblyClearValue(articleQuantityInOuterPackage));
        }

        // article quantity in outer package unit
        String articleQuantityInOuterPackageUnit = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_ARTICLEQUANTITYINOUTERPACKAGEUNIT_NAME);
        if (articleQuantityInOuterPackageUnit != null) {
            if (CLEAR_VALUE_STRING.equals(articleQuantityInOuterPackageUnit)) {
                productAPI.setArticleQuantityInOuterPackageUnit(null);
            } else {
                CVOrderUnitAPI articleQuantityInOuterPackageOrderUnitAPI = getOrderUnitAPI(articleQuantityInOuterPackageUnit, orderUnitsNameMap, productRow.getRowNum(), sheetName, exception);
                productAPI.setArticleQuantityInOuterPackageUnit(articleQuantityInOuterPackageOrderUnitAPI);
            }
        }

        // package content
        Double packageContent = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, productRow, FIELD_PACKAGECONTENT_NAME);
        if (packageContent != null) {
            productAPI.setPackageContent(possiblyClearValue(packageContent));
        }

        // package content unit
        String packageContentUnit = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_PACKAGECONTENTUNIT_NAME);
        if (packageContentUnit != null) {
            if (CLEAR_VALUE_STRING.equals(packageContentUnit)) {
                productAPI.setPackageContentUnit(null);
            } else {
                CVPackageUnitAPI packageContentUnitAPI = getPackageUnitAPI(packageContentUnit, packageUnitsNameMap, productRow.getRowNum(), sheetName, exception);
                productAPI.setPackageContentUnit(packageContentUnitAPI);
            }
        }

        // package level base
        Double packageLevelBase = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, productRow, FIELD_PACKAGELEVELBASE_NAME);
        if (packageLevelBase != null) {
            packageLevelBase = possiblyClearValue(packageLevelBase);
            productAPI.setPackageLevelBase(packageLevelBase == null ? null : packageLevelBase.intValue());
        }

        // package level middle
        Double packageLevelMiddle = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, productRow, FIELD_PACKAGELEVELMIDDLE_NAME);
        if (packageLevelMiddle != null) {
            packageLevelMiddle = possiblyClearValue(packageLevelMiddle);
            productAPI.setPackageLevelMiddle(packageLevelMiddle == null ? null : packageLevelMiddle.intValue());
        }

        // package level top
        Double packageLevelTop = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, productRow, FIELD_PACKAGELEVELTOP_NAME);
        if (packageLevelTop != null) {
            packageLevelTop = possiblyClearValue(packageLevelTop);
            productAPI.setPackageLevelTop(packageLevelTop == null ? null : packageLevelTop.intValue());
        }

        // manufacturer product number
        String manufacturerNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_MANUFACTURERNUMBER_NAME);
        if (manufacturerNumber != null && !manufacturerNumber.isEmpty()) {
            productAPI.setManufacturerProductNumber(possiblyClearValue(manufacturerNumber));
        }

        // ce marked
        String ceMarked = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_CEMARKED_NAME);
        if (ceMarked != null) {
            productAPI.setCeMarked(YES.equalsIgnoreCase(ceMarked));
        }

        // directive
        String directive = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_DIRECTIVE_NAME);
        if (directive != null && !directive.isEmpty()) {
            if (CLEAR_VALUE_STRING.equals(directive)) {
                productAPI.setCeDirective(null);
            } else {
                productAPI.setCeDirective(getDirectiveAPI(directive));
            }
        }

        // standard
        String standard = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_STANDARD_NAME);
        if (standard != null && !standard.isEmpty()) {
            if (CLEAR_VALUE_STRING.equals(standard)) {
                productAPI.setCeStandard(null);
            } else {
                productAPI.setCeStandard(getStandardAPI(standard));
            }
        }

        // preventive maintenance valid from
        String preventiveMaintenanceValidFrom = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_PREVENTIVEMAINTENANCE_VALIDFROM_NAME);
        if (preventiveMaintenanceValidFrom != null && !preventiveMaintenanceValidFrom.isEmpty()) {
            if (CLEAR_VALUE_STRING.equals(preventiveMaintenanceValidFrom)) {
                productAPI.setPreventiveMaintenanceValidFrom(null);
            } else {
                productAPI.setPreventiveMaintenanceValidFrom(getPreventiveMaintenanceAPI(preventiveMaintenanceValidFrom));
            }
        }

        // preventive maintenance interval
        Double preventiveMaintenanceInterval = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, productRow, FIELD_PREVENTIVEMAINTENANCE_INTERVAL_NAME);
        if (preventiveMaintenanceInterval != null) {
            preventiveMaintenanceInterval = possiblyClearValue(preventiveMaintenanceInterval);
            productAPI.setPreventiveMaintenanceNumberOfDays(preventiveMaintenanceInterval == null ? null : preventiveMaintenanceInterval.intValue());
        }

        // color
        String color = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productRow, FIELD_COLOR_NAME);
        if (color != null) {
            productAPI.setColor(possiblyClearValue(color));
        }

        if (productAPI.getCategoryPropertys() == null) {
            productAPI.setCategoryPropertys(new ArrayList<>());
        }
        setResourceSpecificPropertys(categorySpecificPropertys, productAPI.getCategoryPropertys(), fieldNamesRow, productRow);

        return productAPI;
    }

    private void fillMainImageMediaAPI(XSSFRow row,
                                       XSSFRow fieldNamesRow,
                                       Long productUniqueId,
                                       Long articleUniqueId,
                                       ImportItem importItem,
                                       HjalpmedelstjanstenValidationException exception,
                                       String sheetName) {
        LOG.log(Level.FINEST, "fillMainImageMediaAPI( ... )");

        MediaImageAPI mediaImageAPI = new MediaImageAPI();
        mediaImageAPI.setMainImage(true);

        String mainImageUrl_new = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_MEDIA_URL_NAME);
        String mainImageAltText_new = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_MEDIA_ALTTEXT_NAME);
        String mainImageDescription_new = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_MEDIA_DESCRIPTION_NAME);

        if (articleUniqueId != null) {
            if ((mainImageUrl_new != null && !mainImageUrl_new.isEmpty()) ||
                (mainImageDescription_new != null && !mainImageDescription_new.isEmpty()) ||
                (mainImageAltText_new != null && !mainImageAltText_new.isEmpty())) {
                // the excel includes new values


                // check if the article already has a main image
                ArticleMediaImage articleMediaImage = mediaController.getArticleMainImage(articleUniqueId);
                if (articleMediaImage != null) { //article has a main image
                    mediaImageAPI = MediaMapper.mapArticleImage(articleMediaImage);
                } /*else { //article has NO main image
                // url must be entered (really?) todo
                if (mainImageUrl_new == null || mainImageUrl_new.isEmpty()) {
                    addRowErrorMessage(exception, row.getRowNum(), sheetName, "mainImage", validationMessageService.getMessage("import.productarticle.mainImage.descriptionOrAltTextButNoUrl"));
                    return;
                }
            }
            */
            }
        } else if (productUniqueId != null) {
            // check if the product already has a main image
            MediaImage mediaImage_current = mediaController.getProductMainImage(productUniqueId);
            if (mediaImage_current != null) { //product has a main image
                mediaImageAPI = MediaMapper.mapImage(mediaImage_current);
            } else { //product has NO main image
                // url and alt text must be entered
                if (mainImageUrl_new == null || mainImageUrl_new.isEmpty() || mainImageAltText_new == null || mainImageAltText_new.isEmpty()) {
                    if (mainImageUrl_new == null || mainImageUrl_new.isEmpty()) {
                        addRowErrorMessage(exception, row.getRowNum(), sheetName, "mainImage", validationMessageService.getMessage("import.productarticle.product.missingMainImage"));
                    }
                    if (mainImageAltText_new == null || mainImageAltText_new.isEmpty()){
                        addRowErrorMessage(exception, row.getRowNum(), sheetName, "mainImage", validationMessageService.getMessage("import.productarticle.product.missingAltText"));
                    }
                    return;
                }
            }
        }

        // url
        // main image url is required for products (1177) so we can not delete it
        if (mainImageUrl_new != null && CLEAR_VALUE_STRING.equals(mainImageUrl_new)) {
            if (articleUniqueId != null) {
                importItem.setDeleteMainImage(true);
                return;
            }
            else {//this is a product
                addRowErrorMessage(exception, row.getRowNum(), sheetName, "mainImage", validationMessageService.getMessage("import.productarticle.product.deletingUrl"));
                return;
            }
        }
        // main image url is required for products (1177)
        if (mainImageUrl_new != null && !mainImageUrl_new.isEmpty()) {
            mediaImageAPI.setUrl(mainImageUrl_new);
        }
        else {
            if (productUniqueId != null && (mediaImageAPI.getUrl() == null || mediaImageAPI.getUrl().isEmpty())) {
                addRowErrorMessage(exception, row.getRowNum(), sheetName, "mainImage", validationMessageService.getMessage("import.productarticle.product.missingMainImage"));
                return;
            }
        }


        // alt text
        // main image alt-text is required for products (1177) so we can not delete it
        if (mainImageAltText_new != null && !mainImageAltText_new.isEmpty()) {
            if (CLEAR_VALUE_STRING.equals(mainImageAltText_new)) {
                if (articleUniqueId != null) {
                    mediaImageAPI.setAlternativeText(null);
                } else { //this is a product
                    addRowErrorMessage(exception, row.getRowNum(), sheetName, "mainImage", validationMessageService.getMessage("import.productarticle.product.deletingAltText"));
                }
            } else {
                mediaImageAPI.setAlternativeText(mainImageAltText_new);
            }
        }
        else{
            if (productUniqueId != null && (mediaImageAPI.getAlternativeText() == null || mediaImageAPI.getAlternativeText().isEmpty())) {
                addRowErrorMessage(exception, row.getRowNum(), sheetName, "mainImage", validationMessageService.getMessage("import.productarticle.product.missingAltText"));
                return;
            }
        }

        // description
        if (mainImageDescription_new != null && !mainImageDescription_new.isEmpty()) {
            if (CLEAR_VALUE_STRING.equals(mainImageDescription_new)) {
                mediaImageAPI.setDescription(null);
            } else {
                mediaImageAPI.setDescription(mainImageDescription_new);
            }
        }

        // main image is required if alt-text and/or description is given
        //if (mainImageUrl_new == null && ((mainImageDescription_new != null && !mainImageDescription_new.isEmpty()) || (mainImageAltText_new != null && !mainImageAltText_new.isEmpty()))) {
        if ((mediaImageAPI.getUrl() == null || mediaImageAPI.getUrl().isEmpty()) && (mediaImageAPI.getAlternativeText() != null || mediaImageAPI.getDescription() != null))  {
            addRowErrorMessage(exception, row.getRowNum(), sheetName, "mainImage", validationMessageService.getMessage("import.productarticle.mainImage.descriptionOrAltTextButNoUrl"));
            return;
        }

        importItem.setMainMediaImageAPI(mediaImageAPI);
    }


    private ArticleAPI fillArticleAPI(
            Organization organization,
            XSSFRow articleRow,
            XSSFRow fieldNamesRow,
            ArticleAPI articleAPI,
            List<CategorySpecificProperty> categorySpecificPropertys,
            Category category,
            Map<String, CVOrderUnitAPI> orderUnitsNameMap,
            Map<String, CVPackageUnitAPI> packageUnitsNameMap,
            Map<String, ProductAPI> productAPINumbersAdded,
            String sheetName,
            HjalpmedelstjanstenValidationException exception) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "fillArticleAPI( organization->uniqueId: {0} )", new Object[]{organization.getUniqueId()});

        /*if (articleAPI.getId() == null) {
            articleAPI.setStatus(Product.Status.PUBLISHED.toString());
        } */

        articleAPI.setOrganizationId(organization.getUniqueId());
        articleAPI.setOrganizationName(organization.getOrganizationName());

        // based on
        String basedOnProductNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_BASEDONPRODUCT_NAME);
        if (basedOnProductNumber != null && !basedOnProductNumber.isEmpty()) {
            // ok, the article is based on a product, find it, first look
            // among the products we already have added/updated in the document
            ProductAPI basedOnProductAPI = productAPINumbersAdded.get(basedOnProductNumber);
            if (basedOnProductAPI == null) {
                // product was not updated or added in document, try to find it in the db
                Product basedOnProduct = productController.findByProductNumberAndOrganization(basedOnProductNumber, organization.getUniqueId());
                basedOnProductAPI = ProductMapper.map(basedOnProduct, true, categorySpecificPropertys);
            }
            if (basedOnProductAPI == null) {
                // product was not found anywhere, mark as error
                addRowErrorMessage(exception, articleRow.getRowNum(), sheetName, "basedOnProduct", validationMessageService.getMessage("import.productarticle.basedOnProduct.notFound", basedOnProductNumber));
            } else {
                articleAPI.setBasedOnProduct(basedOnProductAPI);
                if (articleAPI.getId() == null) {
                    // new article based on product, we must copy all values to be inherited to article from product
                    copyInheritanceValuesFromProductToArticle(articleAPI);
                }
            }
        }

        // name
        String articleName = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_NAME_NAME);
        if (articleName != null && !articleName.isEmpty()) {
            articleAPI.setArticleName(articleName);
        }

        // number
        String articleNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_NUMBER_NAME);
        if (articleNumber != null && !articleNumber.isEmpty()) {
            articleAPI.setArticleNumber(articleNumber);
        }

        // order unit
        String orderUnitCode = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_ORDERUNIT_NAME);
        if (orderUnitCode != null && !orderUnitCode.isEmpty()) {
            CVOrderUnitAPI orderUnitAPI = getOrderUnitAPI(orderUnitCode, orderUnitsNameMap, articleRow.getRowNum(), sheetName, exception);
            articleAPI.setOrderUnit(orderUnitAPI);
        }

        // discontinued date
        Date discontinueDate = this.importExcelHelper.getCellDateValueByName(fieldNamesRow, articleRow, FIELD_DISCONTINUED_NAME);
        if (discontinueDate != null) {
            articleAPI.setReplacementDate(discontinueDate.getTime());

            if (this.importExcelHelper.isProductOrArticleDiscontinued(discontinueDate)) {
                articleAPI.setStatus(Product.Status.DISCONTINUED.toString());

            } else {
                articleAPI.setStatus(Product.Status.PUBLISHED.toString());
            }
        }
        else
        {
            articleAPI.setStatus(Product.Status.PUBLISHED.toString());
        }

        // inactivate rows on discontinued date
        String inactivateRowsOnDiscontinueDate = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_INACTIVATE_ROWS_ON_DISCONTINUED_NAME);
        if (inactivateRowsOnDiscontinueDate != null) {
            articleAPI.setInactivateRowsOnReplacement(YES.equalsIgnoreCase(inactivateRowsOnDiscontinueDate));
        }

        String replacedByArticle = this.importExcelHelper.getCellStringValueByName(fieldNamesRow,articleRow, FIELD_REPLACEDBY_NAME);
        if (replacedByArticle != null) {
            String articleNumbers[] = replacedByArticle.split(",");
            List<ArticleAPI> listOfReplacedArticles = new ArrayList<>();
            for(String replacedBy : articleNumbers)
            {
                ArticleAPI replacedByArticleAPI = articleController.getArticleAPIByArticleNumber(organization.getUniqueId(), replacedBy);
                listOfReplacedArticles.add(replacedByArticleAPI);
            }
            articleAPI.setReplacedByArticles(listOfReplacedArticles);
        }

        // main category
        CategoryAPI categoryAPI = CategoryMapper.map(category);
        if (categoryAPI != null) {
            articleAPI.setCategory(categoryAPI);
        }

        // extended categories
        String extendedCategories = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_EXTENDED_CATEGORIES_NAME);
        if (extendedCategories != null) {
            List<CategoryAPI> categoryAPIs = getExtendedCategories(extendedCategories, articleRow.getRowNum(), sheetName, exception);
            articleAPI.setExtendedCategories(categoryAPIs);
        }

        // supplementary information
        String supplementaryInformation = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_SUPPLEMENTARYINFORMATION_NAME);
        if (supplementaryInformation != null && !supplementaryInformation.isEmpty()) {
            articleAPI.setSupplementedInformation(possiblyClearValue(supplementaryInformation));
        }

        // manufacturer
        String manufacturer = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_MANUFACTURER_NAME);
        if (manufacturer != null && !manufacturer.isEmpty()) {
            articleAPI.setManufacturer(possiblyClearValue(manufacturer));
        }

        // trademark
        String trademark = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_TRADEMARK_NAME);
        if (trademark != null && !trademark.isEmpty()) {
            articleAPI.setTrademark(possiblyClearValue(trademark));
        }

        // gtin 13
        String gtin13 = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_GTIN13_NAME);
        if (gtin13 != null && !gtin13.isEmpty()) {
            articleAPI.setGtin(possiblyClearValue(gtin13));
        }

        // manufacturer web page
        String manufacturerWebPage = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_MANUFACTURERWEB_NAME);
        if (manufacturerWebPage != null && !manufacturerWebPage.isEmpty()) {
            if (CLEAR_VALUE_STRING.equals(manufacturerWebPage)) {
                articleAPI.setManufacturerElectronicAddress(null);
            } else {
                ElectronicAddressAPI electronicAddressAPI = articleAPI.getManufacturerElectronicAddress() == null ? new ElectronicAddressAPI() : articleAPI.getManufacturerElectronicAddress();
                electronicAddressAPI.setWeb(manufacturerWebPage);
                articleAPI.setManufacturerElectronicAddress(electronicAddressAPI);
            }
        }

        // customer unique
        String customerUnique = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_CUSTOMERUNIQUE_NAME);
        if (customerUnique != null && YES.equalsIgnoreCase(customerUnique)) {
            articleAPI.setCustomerUnique(true);
        }
        if (customerUnique != null && NO.equalsIgnoreCase(customerUnique)) {
            articleAPI.setCustomerUnique(false);
        }

        // article quantity in outer package
        Double articleQuantityInOuterPackage = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, articleRow, FIELD_ARTICLEQUANTITYINOUTERPACKAGE_NAME);
        if (articleQuantityInOuterPackage != null) {
            articleAPI.setArticleQuantityInOuterPackage(possiblyClearValue(articleQuantityInOuterPackage));
        }

        // article quantity in outer package unit
        String articleQuantityInOuterPackageUnit = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_ARTICLEQUANTITYINOUTERPACKAGEUNIT_NAME);
        if (articleQuantityInOuterPackageUnit != null) {
            if (CLEAR_VALUE_STRING.equals(articleQuantityInOuterPackageUnit)) {
                articleAPI.setArticleQuantityInOuterPackageUnit(null);
            } else {
                CVOrderUnitAPI articleQuantityInOuterPackageOrderUnitAPI = getOrderUnitAPI(articleQuantityInOuterPackageUnit, orderUnitsNameMap, articleRow.getRowNum(), sheetName, exception);
                articleAPI.setArticleQuantityInOuterPackageUnit(articleQuantityInOuterPackageOrderUnitAPI);
            }
        }

        // package content
        Double packageContent = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, articleRow, FIELD_PACKAGECONTENT_NAME);
        if (packageContent != null) {
            articleAPI.setPackageContent(possiblyClearValue(packageContent));
        }

        // package content unit
        String packageContentUnit = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_PACKAGECONTENTUNIT_NAME);
        if (packageContentUnit != null) {
            if (CLEAR_VALUE_STRING.equals(packageContentUnit)) {
                articleAPI.setPackageContentUnit(null);
            } else {
                CVPackageUnitAPI packageContentUnitAPI = getPackageUnitAPI(packageContentUnit, packageUnitsNameMap, articleRow.getRowNum(), sheetName, exception);
                articleAPI.setPackageContentUnit(packageContentUnitAPI);
            }
        }

        // package level base
        Double packageLevelBase = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, articleRow, FIELD_PACKAGELEVELBASE_NAME);
        if (packageLevelBase != null) {
            packageLevelBase = possiblyClearValue(packageLevelBase);
            articleAPI.setPackageLevelBase(packageLevelBase == null ? null : packageLevelBase.intValue());
        }

        // package level middle
        Double packageLevelMiddle = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, articleRow, FIELD_PACKAGELEVELMIDDLE_NAME);
        if (packageLevelMiddle != null) {
            packageLevelMiddle = possiblyClearValue(packageLevelMiddle);
            articleAPI.setPackageLevelMiddle(packageLevelMiddle == null ? null : packageLevelMiddle.intValue());
        }

        // package level top
        Double packageLevelTop = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, articleRow, FIELD_PACKAGELEVELTOP_NAME);
        if (packageLevelTop != null) {
            packageLevelTop = possiblyClearValue(packageLevelTop);
            articleAPI.setPackageLevelTop(packageLevelTop == null ? null : packageLevelTop.intValue());
        }

        // manufacturer product number
        String manufacturerNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_MANUFACTURERNUMBER_NAME);
        if (manufacturerNumber != null && !manufacturerNumber.isEmpty()) {
            articleAPI.setManufacturerArticleNumber(possiblyClearValue(manufacturerNumber));
        }

        // ce marked
        String ceMarked = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_CEMARKED_NAME);
        if (ceMarked != null && !ceMarked.isEmpty()) {
            articleAPI.setCeMarked(YES.equalsIgnoreCase(ceMarked));
        }

        // directive
        String directive = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_DIRECTIVE_NAME);
        if (directive != null && !directive.isEmpty()) {
            if (CLEAR_VALUE_STRING.equals(directive)) {
                articleAPI.setCeDirective(null);
            } else {
                articleAPI.setCeDirective(getDirectiveAPI(directive));
            }
        }

        // standard
        String standard = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_STANDARD_NAME);
        if (standard != null && !standard.isEmpty()) {
            if (CLEAR_VALUE_STRING.equals(standard)) {
                articleAPI.setCeStandard(null);
            } else {
                articleAPI.setCeStandard(getStandardAPI(standard));
            }
        }

        // preventive maintenance valid from
        String preventiveMaintenanceValidFrom = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_PREVENTIVEMAINTENANCE_VALIDFROM_NAME);
        if (preventiveMaintenanceValidFrom != null && !preventiveMaintenanceValidFrom.isEmpty()) {
            if (CLEAR_VALUE_STRING.equals(preventiveMaintenanceValidFrom)) {
                articleAPI.setPreventiveMaintenanceValidFrom(null);
            } else {
                articleAPI.setPreventiveMaintenanceValidFrom(getPreventiveMaintenanceAPI(preventiveMaintenanceValidFrom));
            }
        }

        // preventive maintenance interval
        Double preventiveMaintenanceInterval = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, articleRow, FIELD_PREVENTIVEMAINTENANCE_INTERVAL_NAME);
        if (preventiveMaintenanceInterval != null) {
            preventiveMaintenanceInterval = possiblyClearValue(preventiveMaintenanceInterval);
            articleAPI.setPreventiveMaintenanceNumberOfDays(preventiveMaintenanceInterval == null ? null : preventiveMaintenanceInterval.intValue());
        }

        // color
        String color = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, articleRow, FIELD_COLOR_NAME);
        if (color != null) {
            articleAPI.setColor(possiblyClearValue(color));
        }

        if (articleAPI.getCategoryPropertys() == null) {
            articleAPI.setCategoryPropertys(new ArrayList<>());
        }
        setResourceSpecificPropertys(categorySpecificPropertys, articleAPI.getCategoryPropertys(), fieldNamesRow, articleRow);

        if (articleAPI.getCategory().getArticleType() != Article.Type.H) {
            setConnections(articleAPI, fieldNamesRow, articleRow, organization.getUniqueId());
        }

        return articleAPI;
    }

    /**
     * Add an error message for a specific row on a specific sheet. Instead of having
     * to correct the row number for each call, we add 1 here.
     *
     * @param exception
     * @param rowNumber
     * @param sheetName
     * @param field
     * @param message
     */
    public void addRowErrorMessage(HjalpmedelstjanstenValidationException exception, int rowNumber, String sheetName, String field, String message) {
        String errorOnRowMessage = validationMessageService.getMessage("import.productarticle.errorOnRow", rowNumber + 1, sheetName);
        exception.addValidationMessage(field, errorOnRowMessage + " " + message);
    }

    private CVOrderUnitAPI getOrderUnitAPI(String orderUnitName, Map<String, CVOrderUnitAPI> allOrderUnits, int rowNumber, String sheetName, HjalpmedelstjanstenValidationException exception) {
        if (orderUnitName != null && !orderUnitName.trim().isEmpty()) {
            CVOrderUnitAPI orderUnitAPI = allOrderUnits.get(orderUnitName);
            if (orderUnitAPI == null) {
                addRowErrorMessage(exception, rowNumber, sheetName, "orderUnit", validationMessageService.getMessage("import.productarticle.orderunit.notFound", orderUnitName));
            } else {
                return orderUnitAPI;
            }
        }
        return null;
    }

    private CVPackageUnitAPI getPackageUnitAPI(String packageUnitName, Map<String, CVPackageUnitAPI> allPackageUnits, int rowNumber, String sheetName, HjalpmedelstjanstenValidationException exception) {
        if (packageUnitName != null && !packageUnitName.trim().isEmpty()) {
            CVPackageUnitAPI packageUnitAPI = allPackageUnits.get(packageUnitName);
            if (packageUnitAPI == null) {
                addRowErrorMessage(exception, rowNumber, sheetName, "packageUnit", validationMessageService.getMessage("import.productarticle.orderunit.notFound", packageUnitName));
            } else {
                return packageUnitAPI;
            }
        }
        return null;
    }

    private CVCEDirectiveAPI getDirectiveAPI(String directiveName) {
        if (directiveName != null && !directiveName.trim().isEmpty()) {
            CVCEDirective cEDirective = ceController.findCEDirectiveByName(directiveName);
            CVCEDirectiveAPI directiveAPI = new CVCEDirectiveAPI();
            directiveAPI.setId(cEDirective.getUniqueId());
            return directiveAPI;
        }
        return null;
    }

    private CVCEStandardAPI getStandardAPI(String standardName) {
        if (standardName != null && !standardName.trim().isEmpty()) {
            CVCEStandard cEStandard = ceController.findCEStandardByName(standardName);
            CVCEStandardAPI standardAPI = new CVCEStandardAPI();
            standardAPI.setId(cEStandard.getUniqueId());
            return standardAPI;
        }
        return null;
    }

    private CVPreventiveMaintenanceAPI getPreventiveMaintenanceAPI(String preventiveMaintenanceName) {
        if (preventiveMaintenanceName != null && !preventiveMaintenanceName.trim().isEmpty()) {
            CVPreventiveMaintenance preventiveMaintenance = preventiveMaintenanceUnitController.findByName(preventiveMaintenanceName);
            CVPreventiveMaintenanceAPI preventiveMaintenanceAPI = new CVPreventiveMaintenanceAPI();
            preventiveMaintenanceAPI.setId(preventiveMaintenance.getUniqueId());
            preventiveMaintenanceAPI.setCode(preventiveMaintenance.getCode());
            return preventiveMaintenanceAPI;
        }
        return null;
    }

    private void setResourceSpecificPropertys(List<CategorySpecificProperty> categorySpecificPropertys, List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs, XSSFRow fieldNamesRow, XSSFRow productOrArticleRow) {
        for (CategorySpecificProperty categorySpecificProperty : categorySpecificPropertys) {
            // we do not handle multi value lists in Excel
            if (categorySpecificProperty.getType() != CategorySpecificProperty.Type.VALUELIST_MULTIPLE) {
                if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.INTERVAL) {
                    // two values, min and max
                    Double categorySpecificPropertyValueMin = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, productOrArticleRow, FIELD_CATEGORYSPECIFICPROPERTY_PREFIX + categorySpecificProperty.getUniqueId() + "-min");
                    Double categorySpecificPropertyValueMax = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, productOrArticleRow, FIELD_CATEGORYSPECIFICPROPERTY_PREFIX + categorySpecificProperty.getUniqueId() + "-max");
                    if (categorySpecificPropertyValueMin != null &&
                            categorySpecificPropertyValueMax != null) {
                        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = findCurrentValue(categorySpecificProperty, resourceSpecificPropertyAPIs);
                        if (resourceSpecificPropertyAPI == null) {
                            resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
                            resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
                        }
                        setCategorySpecificPropertyValueInterval(categorySpecificProperty, categorySpecificPropertyValueMin, categorySpecificPropertyValueMax, resourceSpecificPropertyAPI);
                    }
                } else if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.DECIMAL) {
                    // two values, min and max
                    Double categorySpecificPropertyValue = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, productOrArticleRow, FIELD_CATEGORYSPECIFICPROPERTY_PREFIX + categorySpecificProperty.getUniqueId());
                    if (categorySpecificPropertyValue != null) {
                        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = findCurrentValue(categorySpecificProperty, resourceSpecificPropertyAPIs);
                        if (resourceSpecificPropertyAPI == null) {
                            resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
                            resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
                        }
                        setCategorySpecificPropertyValueDecimal(categorySpecificProperty, categorySpecificPropertyValue, resourceSpecificPropertyAPI);
                    }
                } else {
                    String categorySpecificPropertyValue = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productOrArticleRow, FIELD_CATEGORYSPECIFICPROPERTY_PREFIX + categorySpecificProperty.getUniqueId());
                    if (categorySpecificPropertyValue != null && !categorySpecificPropertyValue.isEmpty()) {
                        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = findCurrentValue(categorySpecificProperty, resourceSpecificPropertyAPIs);
                        if (resourceSpecificPropertyAPI == null) {
                            resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
                            resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
                        }
                        setCategorySpecificPropertyValue(categorySpecificProperty, categorySpecificPropertyValue, resourceSpecificPropertyAPI);
                    }
                }
            }
        }
    }


    private List<CategoryAPI> getExtendedCategories(String extendedCategoriesString, int rowNumber, String sheetName, HjalpmedelstjanstenValidationException exception) {
        List<CategoryAPI> categoryAPIs = new ArrayList<>();
        if (extendedCategoriesString != null && !extendedCategoriesString.isEmpty()) {
            String[] extendedCategoriesArray = extendedCategoriesString.split(CONNECTIONS_SEPARATOR);
            for (String extendedCategoryCode : extendedCategoriesArray) {
                Category category = categoryController.getChildlessByCode(extendedCategoryCode);
                if (category == null) {
                    addRowErrorMessage(exception, rowNumber, sheetName, "extendedCategories", validationMessageService.getMessage("import.productarticle.extendedCategory.notFound", extendedCategoryCode));
                } else {
                    CategoryAPI categoryAPI = new CategoryAPI();
                    categoryAPI.setId(category.getUniqueId());
                    categoryAPIs.add(categoryAPI);
                }
            }
        }
        return categoryAPIs;
    }

    private void setConnections(ArticleAPI articleAPI, XSSFRow fieldNamesRow, XSSFRow productOrArticleRow, long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        String connectionsString = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, productOrArticleRow, FIELD_CONNECTED_TO_NAME);
        List<ArticleAPI> fitsToArticles = new ArrayList<>();
        List<ProductAPI> fitsToProducts = new ArrayList<>();
        if (connectionsString != null && !connectionsString.isEmpty()) {
            String[] connectionsArray = connectionsString.split(CONNECTIONS_SEPARATOR);
            for (String connectionNumber : connectionsArray) {
                // could be product or article
                ProductAPI connectedToProductAPI = productController.getProductAPIByProductNumber(organizationUniqueId, connectionNumber);
                if (connectedToProductAPI != null) {
                    fitsToProducts.add(connectedToProductAPI);
                } else {
                    ArticleAPI connectedToArticleAPI = articleController.getArticleAPIByArticleNumber(organizationUniqueId, connectionNumber);
                    if (connectedToArticleAPI != null) {
                        fitsToArticles.add(connectedToArticleAPI);
                    } else {
                        throw validationMessageService.generateValidationException("file", "import.productarticle.import.connection.doesNotExist", connectionNumber);
                    }
                }
            }
            articleAPI.setFitsToArticles(fitsToArticles);
            articleAPI.setFitsToProducts(fitsToProducts);
        }
    }

    private ResourceSpecificPropertyAPI findCurrentValue(CategorySpecificProperty categorySpecificProperty, List<ResourceSpecificPropertyAPI> categoryPropertys) {
        for (ResourceSpecificPropertyAPI resourceSpecificPropertyAPI : categoryPropertys) {
            if (resourceSpecificPropertyAPI.getProperty().getId().equals(categorySpecificProperty.getUniqueId())) {
                return resourceSpecificPropertyAPI;
            }
        }
        return null;
    }

    private void setCategorySpecificPropertyValue(CategorySpecificProperty categorySpecificProperty, String categorySpecificPropertyValue, ResourceSpecificPropertyAPI resourceSpecificPropertyAPI) {
        if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.TEXTFIELD) {
            resourceSpecificPropertyAPI.setTextValue(possiblyClearValue(categorySpecificPropertyValue));
        } else if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_SINGLE) {
            if (CLEAR_VALUE_STRING.equals(categorySpecificPropertyValue)) {
                resourceSpecificPropertyAPI.setSingleListValue(null);
            } else {
                CategorySpecificPropertyListValue categorySpecificPropertyListValue = findCategoryListValueFromUserSelection(categorySpecificPropertyValue, categorySpecificProperty.getCategorySpecificPropertyListValues());
                resourceSpecificPropertyAPI.setSingleListValue(categorySpecificPropertyListValue.getUniqueId());
            }
        }
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificProperty.getUniqueId());
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
    }

    private void setCategorySpecificPropertyValueDecimal(CategorySpecificProperty categorySpecificProperty, Double categorySpecificPropertyValue, ResourceSpecificPropertyAPI resourceSpecificPropertyAPI) {
        if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.DECIMAL) {
            resourceSpecificPropertyAPI.setDecimalValue(possiblyClearValue(categorySpecificPropertyValue));
        }
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificProperty.getUniqueId());
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
    }

    private void setCategorySpecificPropertyValueInterval(CategorySpecificProperty categorySpecificProperty, Double categorySpecificPropertyValueMin, Double categorySpecificPropertyValueMax, ResourceSpecificPropertyAPI resourceSpecificPropertyAPI) {
        resourceSpecificPropertyAPI.setIntervalFromValue(possiblyClearValue(categorySpecificPropertyValueMin));
        resourceSpecificPropertyAPI.setIntervalToValue(possiblyClearValue(categorySpecificPropertyValueMax));
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificProperty.getUniqueId());
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
    }

    private CategorySpecificPropertyListValue findCategoryListValueFromUserSelection(String categorySpecificPropertyValue, List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues) {
        if (CLEAR_VALUE_STRING.equals(categorySpecificPropertyValue)) {
            return null;
        }
        for (CategorySpecificPropertyListValue categorySpecificPropertyListValue : categorySpecificPropertyListValues) {
            if (categorySpecificPropertyListValue.getValue().equals(categorySpecificPropertyValue)) {
                return categorySpecificPropertyListValue;
            }
        }
        return null;
    }

    private String possiblyClearValue(String value) {
        if (value != null && !value.isEmpty() && CLEAR_VALUE_STRING.equals(value)) {
            return null;
        }
        return value;
    }

    private Double possiblyClearValue(Double value) {
        if (value != null && CLEAR_VALUE_DOUBLE.equals(value)) {
            return null;
        }
        return value;
    }

    private Map<String, CVOrderUnitAPI> orderUnitListToNameMap(List<CVOrderUnitAPI> allOrderUnits) {
        Map<String, CVOrderUnitAPI> orderUnitsNameMap = new HashMap<>();
        allOrderUnits.forEach((orderUnitAPI) -> {
            orderUnitsNameMap.put(orderUnitAPI.getName(), orderUnitAPI);
        });
        return orderUnitsNameMap;
    }

    private Map<String, CVPackageUnitAPI> packageUnitListToNameMap(List<CVPackageUnitAPI> allPackageUnits) {
        Map<String, CVPackageUnitAPI> packageUnitsNameMap = new HashMap<>();
        allPackageUnits.forEach((packageUnitAPI) -> {
            packageUnitsNameMap.put(packageUnitAPI.getName(), packageUnitAPI);
        });
        return packageUnitsNameMap;
    }

    private Map<Long, CVOrderUnitAPI> orderUnitListToIdMap(List<CVOrderUnitAPI> allOrderUnits) {
        Map<Long, CVOrderUnitAPI> orderUnitsIdMap = new HashMap<>();
        allOrderUnits.forEach((orderUnitAPI) -> {
            orderUnitsIdMap.put(orderUnitAPI.getId(), orderUnitAPI);
        });
        return orderUnitsIdMap;
    }

    private boolean isNewOrChanged(XSSFRow productArticleRow) {
        int rowNumberForLogging = productArticleRow.getRowNum() + 1; // this is the number as seen in excel file
        XSSFCell uniqueIdCell = productArticleRow.getCell(0);
        if (uniqueIdCell == null) {
            LOG.log(Level.FINEST, "Row {0} is new", new Object[]{rowNumberForLogging});
            return true;
        } else {
            // ok, the row is not new, check if any updates are made
            short lastCellNum = productArticleRow.getLastCellNum();
            LOG.log(Level.FINEST, "Row {0} is not new, check for updates, lastCellNum: {1}", new Object[]{rowNumberForLogging, lastCellNum});
            // we already checked cell 0
            for (int cellNumber = 1; cellNumber <= lastCellNum; cellNumber++) {
                XSSFCell cell = productArticleRow.getCell(cellNumber);
                if (cell != null && !cell.getCellStyle().getLocked() && cell.getCellType() != CellType.BLANK && cell.getCellType() != null) {

                    LOG.log(Level.WARNING, cell.getCellStyle() + " " + cell.getCellType() + " " + cell.getRawValue());

                    String columnLetter = CellReference.convertNumToColString(cellNumber);
                    LOG.log(Level.FINEST, "Row {0} Cellcolumn {1} is NOT null, NOT blank and NOT locked", new Object[]{rowNumberForLogging, columnLetter});
                    return true;
                }
            }
        }
        return false;
    }

    private void copyInheritanceValuesFromProductToArticle(ArticleAPI articleAPI) {
        LOG.log(Level.FINEST, "copyInheritanceValuesFromProductToArticle( articleAPI->id: {0} )", new Object[]{articleAPI.getId()});

        // extended categories
        articleAPI.setExtendedCategories(articleAPI.getBasedOnProduct().getExtendedCategories());

        // customer unique
        articleAPI.setCustomerUnique(articleAPI.getBasedOnProduct().isCustomerUnique());

        // copy order information
        articleAPI.setOrderUnit(articleAPI.getBasedOnProduct().getOrderUnit());
        articleAPI.setArticleQuantityInOuterPackage(articleAPI.getBasedOnProduct().getArticleQuantityInOuterPackage());
        articleAPI.setArticleQuantityInOuterPackageUnit(articleAPI.getBasedOnProduct().getArticleQuantityInOuterPackageUnit());
        articleAPI.setPackageContent(articleAPI.getBasedOnProduct().getPackageContent());
        articleAPI.setPackageContentUnit(articleAPI.getBasedOnProduct().getPackageContentUnit());
        articleAPI.setPackageLevelBase(articleAPI.getBasedOnProduct().getPackageLevelBase());
        articleAPI.setPackageLevelMiddle(articleAPI.getBasedOnProduct().getPackageLevelMiddle());
        articleAPI.setPackageLevelTop(articleAPI.getBasedOnProduct().getPackageLevelTop());

        // copy ce information
        articleAPI.setCeMarked(articleAPI.getBasedOnProduct().isCeMarked());
        articleAPI.setCeStandard(articleAPI.getBasedOnProduct().getCeStandard());
        articleAPI.setCeDirective(articleAPI.getBasedOnProduct().getCeDirective());

        // copy preventive maintenance
        articleAPI.setPreventiveMaintenanceValidFrom(articleAPI.getBasedOnProduct().getPreventiveMaintenanceValidFrom());
        articleAPI.setPreventiveMaintenanceNumberOfDays(articleAPI.getBasedOnProduct().getPreventiveMaintenanceNumberOfDays());

        // copy manufacturer
        articleAPI.setManufacturer(articleAPI.getBasedOnProduct().getManufacturer());
        articleAPI.setManufacturerArticleNumber(articleAPI.getBasedOnProduct().getManufacturerProductNumber());
        articleAPI.setManufacturerElectronicAddress(articleAPI.getBasedOnProduct().getManufacturerElectronicAddress());
        articleAPI.setTrademark(articleAPI.getBasedOnProduct().getTrademark());

        // copy extra info
        articleAPI.setColor(articleAPI.getBasedOnProduct().getColor());
        articleAPI.setSupplementedInformation(articleAPI.getBasedOnProduct().getSupplementedInformation());

        // category specific properties
        articleAPI.setCategoryPropertys(articleAPI.getBasedOnProduct().getCategoryPropertys());
    }

    private void updateIndexes(long organizationUniqueId,
                               List<ProductAPI> addedProducts,
                               List<ProductAPI> updatedProducts,
                               List<ArticleAPI> addedArticles,
                               List<ArticleAPI> updatedArticles) {
        addedProducts.forEach((productAPI) -> {
            elasticSearchController.bulkIndexProduct(productAPI);
        });
        updatedProducts.forEach((productAPI) -> {
            elasticSearchController.bulkUpdateIndexProduct(productAPI);
            // must also reindex articles based on
            List<Article> articles = productController.getArticlesBasedOnProduct(organizationUniqueId, productAPI.getId());
            if (articles != null && !articles.isEmpty()) {
                // all articles based on product will have the same category
                Category category = articles.get(0).getBasedOnProduct().getCategory();
                List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(category.getUniqueId());
                for (Article article : articles) {
                    ArticleAPI articleAPI = ArticleMapper.map(article, true, categorySpecificPropertys);
                    elasticSearchController.bulkUpdateIndexArticle(articleAPI);
                }
            }
        });
        addedArticles.forEach((articleAPI) -> {
            elasticSearchController.bulkIndexArticle(articleAPI);
        });
        updatedArticles.forEach((articleAPI) -> {
            elasticSearchController.bulkUpdateIndexArticle(articleAPI);
        });
    }

    private class MediaIndexTracker {
        Integer replacedImages = 0;
        Integer removedImages = 0;
        Integer newImages = 0;
        Integer replacedDocuments = 0;
        Integer removedDocuments = 0;
        Integer newDocuments = 0;
        Integer replacedVideos = 0;
        Integer removedVideos = 0;
        Integer newVideos = 0;

        @Override
        public String toString() {
            return "Replaced images: " + replacedImages +
                    " New images: " + newImages +
                    " Removed images: " + removedImages +
                    " Replaced documents: " +  replacedDocuments +
                    " New documents:  " + newImages +
                    " Removed documents: " +  removedDocuments +
                    " Replaced videos: " +  replacedVideos +
                    " New videos: " +  newVideos +
                    " Removed videos: " +  removedVideos;
        }
    }
}
