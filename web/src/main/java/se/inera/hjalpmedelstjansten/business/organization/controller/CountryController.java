package se.inera.hjalpmedelstjansten.business.organization.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountryAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCountry;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Level;


@Stateless
public class CountryController {

    @Inject
    private HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;

    public List<CVCountryAPI> getAllCountries() {
        LOG.log(Level.FINEST, "getAllCountries()");
        return CountryMapper.map(em.createNamedQuery(CVCountry.FIND_ALL)
                .getResultList());
    }

    public CVCountry getCountry(long uniqueId) {
        LOG.log(Level.FINEST, "getCountry( uniqueId: {0} )");
        return em.find(CVCountry.class, uniqueId);
    }

}
