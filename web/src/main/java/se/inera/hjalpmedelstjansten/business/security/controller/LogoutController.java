package se.inera.hjalpmedelstjansten.business.security.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;

import jakarta.ejb.Stateless;
import jakarta.enterprise.event.Event;
import jakarta.inject.Inject;
import java.util.Date;
import java.util.logging.Level;


@Stateless
public class LogoutController {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private Event<InternalAuditEvent> internalAuditEvent;

    public void logoutUser(UserAPI userAPI, String requestIp) {
        LOG.log(Level.FINEST, "logoutUser(...)");
        Subject currentUser = SecurityUtils.getSubject();
        String sessionId = currentUser.getSession().getId().toString();
        currentUser.logout();
        internalAuditEvent.fire(new InternalAuditEvent(
                new Date(),
                InternalAudit.EntityType.USER,
                InternalAudit.ActionType.LOGOUT,
                userAPI == null ? null:userAPI.getId(),
                sessionId,
                userAPI == null ? null:userAPI.getId(),
                requestIp)
        );
    }

}
