package se.inera.hjalpmedelstjansten.business.product.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.product.controller.PreventiveMaintenanceUnitController;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import java.util.List;
import java.util.logging.Level;

/**
 * REST API for Guarantee Unit functionality.
 *
 */
@Stateless
@Path("preventivemaintenances")
@Interceptors({ PerformanceLogInterceptor.class })
public class PreventiveMaintenanceService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    private PreventiveMaintenanceUnitController preventiveMaintenanceUnitController;

    /**
     * Return all guarantee units in the database.
     *
     * @return a list of <code>GuaranteeUnitAPI</code>
     */
    @GET
    @SecuredService(permissions = {"preventivemaintenance:view"})
    public List<CVPreventiveMaintenanceAPI> getAllPreventiveMaintenances() {
        LOG.log(Level.FINEST, "getAllPreventiveMaintenances()");
        return preventiveMaintenanceUnitController.getAllPreventiveMaintenanceAPIs();
    }

}
