package se.inera.hjalpmedelstjansten.business;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementController;
import se.inera.hjalpmedelstjansten.business.helpers.ExportHelper;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.AssortmentAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.SearchProductsAndArticlesAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.Organization;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.logging.Level;

@Stateless
public class ResultExporterController {

    @Inject
    HjmtLogger LOG;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    OrganizationController organizationController;

    @Inject
    AgreementController agreementController;

    @Inject
    ExportHelper exportHelper;

    private final static int rowToFreeze = 1;

    public byte[] generateAgreementPricelistRowResultList(SearchDTO searchDTO, long organizationUniqueId, long agreementUniqueId, UserAPI userAPI) throws IOException {
        LOG.log(Level.FINEST, "generateAgreementPricelistRowResultList( ... )" );
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Sökresultat");
        int rowNumber = 0;
        Row headerRow = sheet.createRow(rowNumber++);
        headerRow.createCell(0).setCellValue("Typ");
        headerRow.createCell(1).setCellValue("Artnr");
        headerRow.createCell(2).setCellValue("Benämning");
        headerRow.createCell(3).setCellValue("Kategori");
        headerRow.createCell(4).setCellValue("Beställningsenhet");
        //headerRow.createCell(5).setCellValue("Status");
        headerRow.createCell(5).setCellValue("Utgått");
        headerRow.createCell(6).setCellValue("Pris");
        headerRow.createCell(7).setCellValue("Gäller från");
        headerRow.createCell(8).setCellValue("Min best");
        headerRow.createCell(9).setCellValue("Levtid (dagar)");
        headerRow.createCell(10).setCellValue("Garanti (antal)");
        headerRow.createCell(11).setCellValue("(enhet)");
        headerRow.createCell(12).setCellValue("(gäller från)");
        headerRow.createCell(13).setCellValue("(villkor)");
        headerRow.createCell(14).setCellValue("Status");
        headerRow.createCell(15).setCellValue("Kund");
        headerRow.createCell(16).setCellValue("Leverantör");
        Agreement agreement = agreementController.getAgreement(organizationUniqueId,agreementUniqueId,userAPI);
        Organization supplier = agreement.getSupplier();
        Organization customer = agreement.getCustomer();
        if( searchDTO != null && searchDTO.getItems() != null ) {
            List<AgreementPricelistRowAPI> agreementPricelistRowAPIs = ( List<AgreementPricelistRowAPI> ) searchDTO.getItems();
            for( AgreementPricelistRowAPI agreementPricelistRowAPI : agreementPricelistRowAPIs ) {
                int columnNumber = 0;
                Row row = sheet.createRow(rowNumber++);
                row.createCell(columnNumber++).setCellValue(agreementPricelistRowAPI.getArticle().getCategory().getArticleType().toString());
                row.createCell(columnNumber++).setCellValue(agreementPricelistRowAPI.getArticle().getArticleNumber());
                row.createCell(columnNumber++).setCellValue(agreementPricelistRowAPI.getArticle().getArticleName());
                // to get the code if article is not H or Tiso
                String categoryString;
                //case 1 type H or Tiso
                if( agreementPricelistRowAPI.getArticle().getBasedOnProduct() == null ) {
                    categoryString = agreementPricelistRowAPI.getArticle().getCategory().getCode();
                } else {
                    categoryString = agreementPricelistRowAPI.getArticle().getBasedOnProduct().getCategory().getCode();
                }
                //case 2
                if (categoryString == null){
                    if (agreementPricelistRowAPI.getArticle().getFitsToProducts() != null ) {
                        if (agreementPricelistRowAPI.getArticle().getFitsToProducts().size() > 0) {
                            categoryString = agreementPricelistRowAPI.getArticle().getFitsToProducts().get(0).getCategory().getCode();
                            categoryString = "(" + categoryString + ")";
                        }
                    }
                }
                //case 3
                if (categoryString == null){
                    if (agreementPricelistRowAPI.getArticle().getFitsToArticles() != null && agreementPricelistRowAPI.getArticle().getFitsToArticles().size() > 0) {
                        if (agreementPricelistRowAPI.getArticle().getFitsToArticles().get(0).getBasedOnProduct() != null) {
                            categoryString = agreementPricelistRowAPI.getArticle().getFitsToArticles().get(0).getBasedOnProduct().getCategory().getCode();
                            categoryString = "(" + categoryString + ")";
                        }
                        // kan tänkas att detta ska med? PA 2020-10-05
                        else{
                            categoryString = agreementPricelistRowAPI.getArticle().getFitsToArticles().get(0).getCategory().getCode();
                            categoryString = "(" + categoryString + ")";
                        }
                    }
                }
                if (categoryString == null){
                    if (agreementPricelistRowAPI.getArticle().getFitsToArticles() != null && agreementPricelistRowAPI.getArticle().getFitsToArticles().size() > 0) {
                        if (agreementPricelistRowAPI.getArticle().getFitsToArticles().get(0).getFitsToProducts() != null && agreementPricelistRowAPI.getArticle().getFitsToArticles().get(0).getFitsToProducts().size() > 0) {
                            categoryString = agreementPricelistRowAPI.getArticle().getFitsToArticles().get(0).getFitsToProducts().get(0).getCategory().getCode();
                            categoryString = "(" + categoryString + ")";
                        }
                    }
                }
                row.createCell(columnNumber++).setCellValue(categoryString);
                row.createCell(columnNumber++).setCellValue(agreementPricelistRowAPI.getArticle().getOrderUnit().getName());
                //String pricelistRowStatus = validationMessageService.getMessage("pricelistrow.status." + agreementPricelistRowAPI.getStatus());
                //row.createCell(columnNumber++).setCellValue(pricelistRowStatus);
                String articleStatus = agreementPricelistRowAPI.getArticle().getStatus().equalsIgnoreCase("DISCONTINUED") ? "Utgått" : "";
                row.createCell(columnNumber++).setCellValue(articleStatus);
                row.createCell(columnNumber++).setCellValue(agreementPricelistRowAPI.getPrice() == null ? "": exportHelper.parseNumberValueToStringWithCorrectDecimalFormatting(agreementPricelistRowAPI.getPrice()));
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                ZonedDateTime validFrom = Instant.ofEpochMilli(agreementPricelistRowAPI.getValidFrom()).atZone(ZoneId.systemDefault());
                String date = dateTimeFormatter.format(validFrom);
                row.createCell(columnNumber++).setCellValue(date);
                row.createCell(columnNumber++).setCellValue(agreementPricelistRowAPI.getLeastOrderQuantity() == null ? "": agreementPricelistRowAPI.getLeastOrderQuantity().toString());
                row.createCell(columnNumber++).setCellValue(agreementPricelistRowAPI.getDeliveryTime() == null ? "": agreementPricelistRowAPI.getDeliveryTime().toString());
                row.createCell(columnNumber++).setCellValue(agreementPricelistRowAPI.getWarrantyQuantity() == null ? "": agreementPricelistRowAPI.getWarrantyQuantity().toString());
                row.createCell(columnNumber++).setCellValue(agreementPricelistRowAPI.getWarrantyQuantityUnit() == null ? "": agreementPricelistRowAPI.getWarrantyQuantityUnit().getName());
                row.createCell(columnNumber++).setCellValue(agreementPricelistRowAPI.getWarrantyValidFrom() == null ? "": agreementPricelistRowAPI.getWarrantyValidFrom().getName());
                row.createCell(columnNumber++).setCellValue(agreementPricelistRowAPI.getWarrantyTerms() == null ? "": agreementPricelistRowAPI.getWarrantyTerms());
                String pricelistRowStatus = validationMessageService.getMessage("pricelistrow.status." + agreementPricelistRowAPI.getStatus());
                row.createCell(columnNumber++).setCellValue(pricelistRowStatus);
                //HJAL 1393
                row.createCell(columnNumber++).setCellValue(customer.getOrganizationName() == null ? "": customer.getOrganizationName());
                row.createCell(columnNumber++).setCellValue(supplier.getOrganizationName() == null ? "": supplier.getOrganizationName());
            }
        }
        // size columns
        for( int i=0; i<=headerRow.getLastCellNum(); i++ ) {
            sheet.autoSizeColumn(i);
        }

        sheet.createFreezePane(0, rowToFreeze);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        workbook.write(baos);
        return baos.toByteArray();
    }

    public byte[] generateProductsAndArticlesList(List<SearchProductsAndArticlesAPI> searchProductsAndArticlesAPIs) throws IOException {
        LOG.log(Level.FINEST, "generateProductsAndArticlesList( ... )" );
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Sökresultat");
        int rowNumber = 0;
        Row headerRow = sheet.createRow(rowNumber++);
        headerRow.createCell(0).setCellValue("Typ");
        headerRow.createCell(1).setCellValue("Benämning");
        headerRow.createCell(2).setCellValue("Produkt/artnr");
        headerRow.createCell(3).setCellValue("Status");
        headerRow.createCell(4).setCellValue("Kategori");
        headerRow.createCell(5).setCellValue("Leverantör");
        if( searchProductsAndArticlesAPIs != null && !searchProductsAndArticlesAPIs.isEmpty() ) {
            for( SearchProductsAndArticlesAPI searchProductsAndArticlesAPI : searchProductsAndArticlesAPIs ) {
                int columnNumber = 0;
                Row row = sheet.createRow(rowNumber++);
                String type = searchProductsAndArticlesAPI.getType().equals(SearchProductsAndArticlesAPI.Type.PRODUCT) ? "P": searchProductsAndArticlesAPI.getArticleType().toString();
                row.createCell(columnNumber++).setCellValue(type);
                row.createCell(columnNumber++).setCellValue(searchProductsAndArticlesAPI.getName());
                row.createCell(columnNumber++).setCellValue(searchProductsAndArticlesAPI.getNumber());
                String productStatus = validationMessageService.getMessage("product.status." + searchProductsAndArticlesAPI.getStatus().toString());
                row.createCell(columnNumber++).setCellValue(productStatus);
                row.createCell(columnNumber++).setCellValue(searchProductsAndArticlesAPI.getCode());
                row.createCell(columnNumber++).setCellValue(searchProductsAndArticlesAPI.getOrganizationName());
            }
        }
        // size columns
        for( int i=0; i<=headerRow.getLastCellNum(); i++ ) {
            sheet.autoSizeColumn(i);
        }

        sheet.createFreezePane(0, rowToFreeze);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        workbook.write(baos);
        return baos.toByteArray();
    }

    public byte[] generateArticlesList(List<ArticleAPI> searchArticlesAPIs) throws IOException {
        LOG.log(Level.FINEST, "generateArticlesList( ... )" );
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Sökresultat");
        int rowNumber = 0;
        Row headerRow = sheet.createRow(rowNumber++);
        headerRow.createCell(0).setCellValue("Typ");
        headerRow.createCell(1).setCellValue("Artikelnummer");
        headerRow.createCell(2).setCellValue("Artikelbenämning");
        headerRow.createCell(3).setCellValue("Baseras på produkt");
        headerRow.createCell(4).setCellValue("Kategori");
        headerRow.createCell(5).setCellValue("Organisation");

        if( searchArticlesAPIs != null && !searchArticlesAPIs.isEmpty() ) {
            for( ArticleAPI searchArticlesAPI : searchArticlesAPIs ) {
                int columnNumber = 0;
                Row row = sheet.createRow(rowNumber++);
                row.createCell(columnNumber++).setCellValue(searchArticlesAPI.getCategory().getArticleType().toString());
                row.createCell(columnNumber++).setCellValue(searchArticlesAPI.getArticleNumber());
                row.createCell(columnNumber++).setCellValue(searchArticlesAPI.getArticleName());
                row.createCell(columnNumber++).setCellValue(searchArticlesAPI.getBasedOnProduct().getProductNumber());
                row.createCell(columnNumber++).setCellValue(searchArticlesAPI.getBasedOnProduct().getCategory().getCode());
                row.createCell(columnNumber++).setCellValue(searchArticlesAPI.getOrganizationName());
            }
        }
        // size columns
        for( int i=0; i<=headerRow.getLastCellNum(); i++ ) {
            sheet.autoSizeColumn(i);
        }

        sheet.createFreezePane(0, rowToFreeze);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        workbook.write(baos);
        return baos.toByteArray();
    }

    public byte[] generateUsersResultList(List<UserAPI> searchUsersAPIs) throws IOException {
        LOG.log(Level.FINEST, "generateUsersResultList( ... )" );
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Sökresultat");
        int rowNumber = 0;
        Row headerRow = sheet.createRow(rowNumber++);
        headerRow.createCell(0).setCellValue("Namn");
        headerRow.createCell(1).setCellValue("Användarnamn");
        headerRow.createCell(2).setCellValue("Titel");
        headerRow.createCell(3).setCellValue("E-post");
        headerRow.createCell(4).setCellValue("Status");

        if( searchUsersAPIs != null && !searchUsersAPIs.isEmpty() ) {
            for( UserAPI searchUsersAPI : searchUsersAPIs ) {
                int columnNumber = 0;
                Row row = sheet.createRow(rowNumber++);
                String fullName = searchUsersAPI.getFirstName() + " " + searchUsersAPI.getLastName();
                row.createCell(columnNumber++).setCellValue(fullName);
                row.createCell(columnNumber++).setCellValue(searchUsersAPI.getUsername());
                row.createCell(columnNumber++).setCellValue(searchUsersAPI.getTitle());
                row.createCell(columnNumber++).setCellValue(searchUsersAPI.getElectronicAddress().getEmail());
                String isActive = searchUsersAPI.isActive() ? "Aktiv" : "Inaktiv";
                row.createCell(columnNumber++).setCellValue(isActive);

            }
        }
        // size columns
        for( int i=0; i<=headerRow.getLastCellNum(); i++ ) {
            sheet.autoSizeColumn(i);
        }

        sheet.createFreezePane(0, rowToFreeze);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        workbook.write(baos);
        return baos.toByteArray();
    }

    public byte[] generateAssortmentsResultList(List<AssortmentAPI> searchAssortmentsAPIs) throws IOException {
        LOG.log(Level.FINEST, "generateAssortmentsResultList( ... )" );
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Sökresultat");
        int rowNumber = 0;
        Row headerRow = sheet.createRow(rowNumber++);
        headerRow.createCell(0).setCellValue("Namn");
        headerRow.createCell(1).setCellValue("Organisation");
        headerRow.createCell(2).setCellValue("Län");
        headerRow.createCell(3).setCellValue("Giltigt från");
        headerRow.createCell(4).setCellValue("Giltigt till");
        headerRow.createCell(5).setCellValue("Utbudsansvarig");

        if( searchAssortmentsAPIs != null && !searchAssortmentsAPIs.isEmpty() ) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            for( AssortmentAPI searchAssortmentsAPI : searchAssortmentsAPIs ) {
                int columnNumber = 0;
                Row row = sheet.createRow(rowNumber++);
                row.createCell(columnNumber++).setCellValue(searchAssortmentsAPI.getName());
                row.createCell(columnNumber++).setCellValue(searchAssortmentsAPI.getCustomer().getOrganizationName());
                row.createCell(columnNumber++).setCellValue(searchAssortmentsAPI.getCounty() != null ? searchAssortmentsAPI.getCounty().getName() : "");
                ZonedDateTime validFrom = Instant.ofEpochMilli(searchAssortmentsAPI.getValidFrom()).atZone(ZoneId.systemDefault());
                row.createCell(columnNumber++).setCellValue(dateTimeFormatter.format(validFrom));
                if (searchAssortmentsAPI.getValidTo() != null) {
                    ZonedDateTime validTo = Instant.ofEpochMilli(searchAssortmentsAPI.getValidTo()).atZone(ZoneId.systemDefault());
                    row.createCell(columnNumber++).setCellValue(dateTimeFormatter.format(validTo));
                }
                else
                    row.createCell(columnNumber++).setCellValue("");


                List<UserAPI> managers = searchAssortmentsAPI.getManagers();
                StringBuilder managerFullName = new StringBuilder();
                int i = 0;
                if (managers != null) {
                    for (UserAPI manager : managers) {
                        if (i == 0)
                            managerFullName.append(manager.getFirstName()).append(" ").append(manager.getLastName());
                         else
                            managerFullName.append(", ").append(manager.getFirstName()).append(" ").append(manager.getLastName());
                        i++;
                    }
                }
                row.createCell(columnNumber++).setCellValue(managerFullName.toString());
            }
        }
        // size columns
        for( int i=0; i<=headerRow.getLastCellNum(); i++ ) {
            sheet.autoSizeColumn(i);
        }

        sheet.createFreezePane(0, rowToFreeze);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        workbook.write(baos);
        return baos.toByteArray();
    }

    public byte[] generateGPsList(List<GeneralPricelistAPI> gpAPIs) throws IOException {
        LOG.log(Level.FINEST, "generateGPsList( ... )" );
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Sökresultat");
        int rowNumber = 0;
        Row headerRow = sheet.createRow(rowNumber++);
        headerRow.createCell(0).setCellValue("Leverantör");
        headerRow.createCell(1).setCellValue("Avtalsnummer");
        headerRow.createCell(2).setCellValue("Giltigt from");
        headerRow.createCell(3).setCellValue("Giltigt tom");

        if( gpAPIs != null && !gpAPIs.isEmpty() ) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            for( GeneralPricelistAPI gpAPI : gpAPIs ) {
                int columnNumber = 0;
                Row row = sheet.createRow(rowNumber++);
                row.createCell(columnNumber++).setCellValue(gpAPI.getOwnerOrganization().getOrganizationName());
                row.createCell(columnNumber++).setCellValue(gpAPI.getGeneralPricelistNumber());
                ZonedDateTime validFrom = Instant.ofEpochMilli(gpAPI.getValidFrom()).atZone(ZoneId.systemDefault());
                row.createCell(columnNumber++).setCellValue(dateTimeFormatter.format(validFrom));
                if (gpAPI.getValidTo() != null) {
                    ZonedDateTime validTo = Instant.ofEpochMilli(gpAPI.getValidTo()).atZone(ZoneId.systemDefault());
                    row.createCell(columnNumber++).setCellValue(dateTimeFormatter.format(validTo));
                }
                else
                    row.createCell(columnNumber++).setCellValue("");
            }
        }
        // size columns
        for( int i=0; i<=headerRow.getLastCellNum(); i++ ) {
            sheet.autoSizeColumn(i);
        }

        sheet.createFreezePane(0, rowToFreeze);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        workbook.write(baos);
        return baos.toByteArray();
    }

    public byte[] generateOrganizationsList(List<OrganizationAPI> searchOrganizationsAPIs) throws IOException {
        LOG.log(Level.FINEST, "generateOrganizationsList( ... )" );
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Sökresultat");
        int rowNumber = 0;
        Row headerRow = sheet.createRow(rowNumber++);
        headerRow.createCell(0).setCellValue("Organisation");
        headerRow.createCell(1).setCellValue("Org.grupp");
        headerRow.createCell(2).setCellValue("Org.nr");
        headerRow.createCell(3).setCellValue("GLN");
        headerRow.createCell(4).setCellValue("Land");
        headerRow.createCell(5).setCellValue("Status");

        if( searchOrganizationsAPIs != null && !searchOrganizationsAPIs.isEmpty() ) {
            for( OrganizationAPI searchOrganizationsAPI : searchOrganizationsAPIs ) {
                int columnNumber = 0;
                Row row = sheet.createRow(rowNumber++);
                row.createCell(columnNumber++).setCellValue(searchOrganizationsAPI.getOrganizationName());
                String orgType = searchOrganizationsAPI.getOrganizationType().equalsIgnoreCase("CUSTOMER") ? "Kund" : searchOrganizationsAPI.getOrganizationType().equalsIgnoreCase("SUPPLIER") ? "Leverantör" : "Tjänsteägare";
                row.createCell(columnNumber++).setCellValue(orgType);
                row.createCell(columnNumber++).setCellValue(searchOrganizationsAPI.getOrganizationNumber());
                row.createCell(columnNumber++).setCellValue(searchOrganizationsAPI.getGln());
                row.createCell(columnNumber++).setCellValue(searchOrganizationsAPI.getCountry().getName());
                String isActive = searchOrganizationsAPI.isActive() ? "Aktiv" : "Inaktiv";
                row.createCell(columnNumber++).setCellValue(isActive);
            }
        }
        // size columns
        for( int i=0; i<=headerRow.getLastCellNum(); i++ ) {
            sheet.autoSizeColumn(i);
        }

        sheet.createFreezePane(0, rowToFreeze);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        workbook.write(baos);
        return baos.toByteArray();
    }

    public byte[] generatePricelistRowList(List<GeneralPricelistPricelistRowAPI> searchGeneralPricelistPricelistRowAPIs, long organizationUniqueId) throws IOException {
        LOG.log(Level.FINEST, "generatePricelistRowList( ... )" );
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        boolean isCustomer;

        isCustomer = organization.getOrganizationType().equals(Organization.OrganizationType.CUSTOMER);

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Sökresultat");
        int rowNumber = 0;
        Row headerRow = sheet.createRow(rowNumber++);
        headerRow.createCell(0).setCellValue("Typ");
        headerRow.createCell(1).setCellValue("Artikelnummer");
        headerRow.createCell(2).setCellValue("Artikelbenämning");
        headerRow.createCell(3).setCellValue("Kategori");
        headerRow.createCell(4).setCellValue("Beställningsenhet");
        headerRow.createCell(5).setCellValue("Utgått");
        headerRow.createCell(6).setCellValue("Pris (SEK)");
        headerRow.createCell(7).setCellValue("Gäller från");
        headerRow.createCell(8).setCellValue("Min. best.");
        headerRow.createCell(9).setCellValue("Levtid (dagar)");
        headerRow.createCell(10).setCellValue("Garanti (antal)");
        headerRow.createCell(11).setCellValue("Garanti (enhet)");
        headerRow.createCell(12).setCellValue("Garanti (gäller från)");
        headerRow.createCell(13).setCellValue("Garanti (villkor)");
        headerRow.createCell(14).setCellValue("Status");

        if( searchGeneralPricelistPricelistRowAPIs != null && !searchGeneralPricelistPricelistRowAPIs.isEmpty() ) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            for( GeneralPricelistPricelistRowAPI generalPricelistPricelistRowAPI : searchGeneralPricelistPricelistRowAPIs ) {
                int columnNumber = 0;
                Row row = sheet.createRow(rowNumber++);
                row.createCell(columnNumber++).setCellValue(generalPricelistPricelistRowAPI.getArticle().getCategory().getArticleType().toString());
                row.createCell(columnNumber++).setCellValue(generalPricelistPricelistRowAPI.getArticle().getArticleNumber());
                row.createCell(columnNumber++).setCellValue(generalPricelistPricelistRowAPI.getArticle().getArticleName());

                // to get the code if article is not H or Tiso
                String categoryString;
                //case 1 type H or Tiso
                if( generalPricelistPricelistRowAPI.getArticle().getBasedOnProduct() == null ) {
                    categoryString = generalPricelistPricelistRowAPI.getArticle().getCategory().getCode();
                } else {
                    categoryString = generalPricelistPricelistRowAPI.getArticle().getBasedOnProduct().getCategory().getCode();
                }
                //case 2
                if (categoryString == null){
                    if (generalPricelistPricelistRowAPI.getArticle().getFitsToProducts() != null ) {
                        if (!generalPricelistPricelistRowAPI.getArticle().getFitsToProducts().isEmpty()) {
                            categoryString = generalPricelistPricelistRowAPI.getArticle().getFitsToProducts().get(0).getCategory().getCode();
                            categoryString = "(" + categoryString + ")";
                        }
                    }
                }
                //case 3
                if (categoryString == null){
                    if (generalPricelistPricelistRowAPI.getArticle().getFitsToArticles() != null && !generalPricelistPricelistRowAPI.getArticle().getFitsToArticles().isEmpty()) {
                        if (generalPricelistPricelistRowAPI.getArticle().getFitsToArticles().get(0).getBasedOnProduct() != null) {
                            categoryString = generalPricelistPricelistRowAPI.getArticle().getFitsToArticles().get(0).getBasedOnProduct().getCategory().getCode();
                            categoryString = "(" + categoryString + ")";
                        }
                        // kan tänkas att detta ska med? PA 2020-10-05
                        else{
                            categoryString = generalPricelistPricelistRowAPI.getArticle().getFitsToArticles().get(0).getCategory().getCode();
                            categoryString = "(" + categoryString + ")";
                        }
                    }
                }
                if (categoryString == null){
                    if (generalPricelistPricelistRowAPI.getArticle().getFitsToArticles() != null && !generalPricelistPricelistRowAPI.getArticle().getFitsToArticles().isEmpty()) {
                        if (generalPricelistPricelistRowAPI.getArticle().getFitsToArticles().get(0).getFitsToProducts() != null && !generalPricelistPricelistRowAPI.getArticle().getFitsToArticles().get(0).getFitsToProducts().isEmpty()) {
                            categoryString = generalPricelistPricelistRowAPI.getArticle().getFitsToArticles().get(0).getFitsToProducts().get(0).getCategory().getCode();
                            categoryString = "(" + categoryString + ")";
                        }
                    }
                }

                row.createCell(columnNumber++).setCellValue(categoryString);
                row.createCell(columnNumber++).setCellValue(generalPricelistPricelistRowAPI.getArticle().getOrderUnit().getName());
                String articleStatus = generalPricelistPricelistRowAPI.getArticle().getStatus().equalsIgnoreCase("DISCONTINUED") ? "Utgått" : "";//validationMessageService.getMessage("product.status." + generalPricelistPricelistRowAPI.getArticle().getStatus());
                row.createCell(columnNumber++).setCellValue(articleStatus);

                if(isCustomer && generalPricelistPricelistRowAPI.getArticle().getStatus().equalsIgnoreCase("DISCONTINUED")) {
                    row.createCell(columnNumber++);
                } else {
                    row.createCell(columnNumber++).setCellValue(generalPricelistPricelistRowAPI.getPrice() != null ? exportHelper.parseNumberValueToStringWithCorrectDecimalFormatting(generalPricelistPricelistRowAPI.getPrice()) : "");
                }

                ZonedDateTime validFrom = Instant.ofEpochMilli(generalPricelistPricelistRowAPI.getValidFrom()).atZone(ZoneId.systemDefault());
                row.createCell(columnNumber++).setCellValue(dateTimeFormatter.format(validFrom));
                row.createCell(columnNumber++).setCellValue(generalPricelistPricelistRowAPI.getLeastOrderQuantity() != null ? generalPricelistPricelistRowAPI.getLeastOrderQuantity().toString() : "");
                row.createCell(columnNumber++).setCellValue(generalPricelistPricelistRowAPI.getDeliveryTime() != null ? generalPricelistPricelistRowAPI.getDeliveryTime().toString() : "");
                row.createCell(columnNumber++).setCellValue(generalPricelistPricelistRowAPI.getWarrantyQuantity() != null ? generalPricelistPricelistRowAPI.getWarrantyQuantity().toString() : "");
                row.createCell(columnNumber++).setCellValue(generalPricelistPricelistRowAPI.getWarrantyQuantityUnit() != null ? generalPricelistPricelistRowAPI.getWarrantyQuantityUnit().getName() : "");
                row.createCell(columnNumber++).setCellValue(generalPricelistPricelistRowAPI.getWarrantyValidFrom() != null ? generalPricelistPricelistRowAPI.getWarrantyValidFrom().getName() : "" );
                row.createCell(columnNumber++).setCellValue(generalPricelistPricelistRowAPI.getWarrantyTerms() != null ? generalPricelistPricelistRowAPI.getWarrantyTerms() : "");
                String pricelistRowStatus = validationMessageService.getMessage("pricelistrow.status." + generalPricelistPricelistRowAPI.getStatus());
                row.createCell(columnNumber++).setCellValue(pricelistRowStatus);
            }
        }
        // size columns
        for( int i=0; i<=headerRow.getLastCellNum(); i++ ) {
            sheet.autoSizeColumn(i);
        }

        sheet.createFreezePane(0, rowToFreeze);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        workbook.write(baos);
        return baos.toByteArray();
    }

    public byte[] generateAgreementResultList(List<AgreementAPI> agreementAPIs) throws IOException {
        LOG.log(Level.FINEST, "generateAgreementResultList( ... )" );
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Sökresultat");
        int rowNumber = 0;
        Row headerRow = sheet.createRow(rowNumber++);
        headerRow.createCell(0).setCellValue("Avtalsnummer");
        headerRow.createCell(1).setCellValue("Avtalsnamn");
        headerRow.createCell(2).setCellValue("Giltigt från");
        headerRow.createCell(3).setCellValue("Giltigt till");
        headerRow.createCell(4).setCellValue("Status");
        headerRow.createCell(5).setCellValue("Leverantör");
        headerRow.createCell(6).setCellValue("Kund");

        if( agreementAPIs != null && !agreementAPIs.isEmpty() ) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            for( AgreementAPI agreementAPI : agreementAPIs ) {
                int columnNumber = 0;
                Row row = sheet.createRow(rowNumber++);
                row.createCell(columnNumber++).setCellValue(agreementAPI.getAgreementNumber());
                row.createCell(columnNumber++).setCellValue(agreementAPI.getAgreementName());

                ZonedDateTime validFrom = Instant.ofEpochMilli(agreementAPI.getValidFrom()).atZone(ZoneId.systemDefault());
                row.createCell(columnNumber++).setCellValue(dateTimeFormatter.format(validFrom));
                ZonedDateTime validTo = Instant.ofEpochMilli(agreementAPI.getValidTo()).atZone(ZoneId.systemDefault());
                row.createCell(columnNumber++).setCellValue(dateTimeFormatter.format(validTo));
                String agreementStatus = validationMessageService.getMessage("agreement.status." + agreementAPI.getStatus());
                row.createCell(columnNumber++).setCellValue(agreementStatus);
                row.createCell(columnNumber++).setCellValue(agreementAPI.getSupplierOrganization().getOrganizationName());
                row.createCell(columnNumber++).setCellValue(agreementAPI.getCustomerOrganization().getOrganizationName());

            }
        }
        // size columns
        for( int i=0; i<=headerRow.getLastCellNum(); i++ ) {
            sheet.autoSizeColumn(i);
        }

        sheet.createFreezePane(0, rowToFreeze);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        workbook.write(baos);
        return baos.toByteArray();
    }

}
