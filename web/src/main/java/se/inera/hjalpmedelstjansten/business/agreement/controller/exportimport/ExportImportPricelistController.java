package se.inera.hjalpmedelstjansten.business.agreement.controller.exportimport;

import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistController;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowController;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistController;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistController;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistRowController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.product.controller.CategoryController;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitController;
import se.inera.hjalpmedelstjansten.business.product.controller.PreventiveMaintenanceUnitController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

/**
 * Base class for handling business logic of exporting and importing products and
 * articles.
 *
 * There is a notion of versions for excel export and import. Every times changes
 * are made that may cause old excel-files not to work, the version-field of this class
 * should be updated which causes a prompt to user using an old version of the excel
 * export to be forced to get a new one.
 *
 */
public abstract class ExportImportPricelistController {

    public static final String EXPORT_VERSION = "1.3";
    public static final String EXPORT_TYPE_AGREEMENT = "AgreementPricelists";
    public static final String EXPORT_TYPE_GENERAL_PRICELIST = "GeneralPricelists";

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    CategoryController categoryController;

    @Inject
    GeneralPricelistController generalPricelistController;

    @Inject
    GeneralPricelistPricelistController generalPricelistPricelistController;

    @Inject
    GeneralPricelistPricelistRowController generalPricelistPricelistRowController;

    @Inject
    AgreementPricelistController agreementPricelistController;

    @Inject
    AgreementPricelistRowController agreementPricelistRowController;

    @Inject
    GuaranteeUnitController guaranteeUnitController;

    @Inject
    ArticleController articleController;

    @Inject
    PreventiveMaintenanceUnitController preventiveMaintenanceUnitController;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    static final String FIELD_ARTICLE_NUMBER = "articleNumber";
    static final String FIELD_PRICE_NAME = "price";
    static final String FIELD_ORDERUNIT_NAME = "orderUnit";
    static final String FIELD_VALID_FROM_NAME = "validFrom";
    static final String FIELD_LEAST_ORDER_QUANTITY_NAME = "leastOrderQuantity";
    static final String FIELD_DELIVERY_TIME_NAME = "deliveryTime";
    static final String FIELD_WARRANTY_QUANTITY_NAME = "warrantyQuantity";
    static final String FIELD_WARRANTY_QUANTITY_UNIT_NAME = "warrantyQuantityUnit";
    static final String FIELD_WARRANTY_VALID_FROM_NAME = "warrantyValidFrom";
    static final String FIELD_WARRANTY_TERMS_NAME = "warrantyTerms";
    static final String FIELD_REQUEST_INACTIVE = "requestInactive";

    static final String STATUS_NAME_CREATED = "Ny";
    static final String STATUS_NAME_INACTIVE = "Inaktiv";
    static final String STATUS_NAME_DECLINED = "Avböjd";
    static final String STATUS_NAME_ACTIVE = "Aktiv";
    static final String STATUS_NAME_CHANGED = "Ändrad";
    static final String STATUS_NAME_PENDING_APPROVAL = "Skickad";
    static final String STATUS_NAME_PENDING_INACTIVATION = "Begärd";

    static final String DAGAR = "Dagar";
    static final String KILOMETER = "Kilometer";

    static final String PRICELISTROW_SHEET_NAME = "Rader Avtal";
    static final String GENERAL_PRICELISTROW_SHEET_NAME = "Rader Generell Prislista";
    static final String VALUELIST_SHEET_NAME = "Värdelistor";

    static final String DATE_PATTERN = "yyyy-MM-dd";
    static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    static final String CLEAR_VALUE_STRING = "*radera*";
    static final Double CLEAR_VALUE_DOUBLE = -1d;

}
