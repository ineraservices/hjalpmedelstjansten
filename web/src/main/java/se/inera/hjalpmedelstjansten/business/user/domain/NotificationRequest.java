package se.inera.hjalpmedelstjansten.business.user.domain;

import lombok.Data;

import java.util.Set;

@Data
public class NotificationRequest {

    private long userId;
    private long organizationId;

    // To be able to update we need at least an empty Array/Set.
    private Set<String> userNotifications;

}
