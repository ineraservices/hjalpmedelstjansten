package se.inera.hjalpmedelstjansten.business.indexing.controller;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.CategoryController;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;

import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 * Reindex a specific article.
 *
 */
@Stateless
public class ReIndexArticleController {

    @Inject
    HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    CategoryController categoryController;

    @Inject
    ElasticSearchController elasticSearchController;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void reIndexArticle( long organizationUniqueId, long articleId, Map<Long, List<CategorySpecificProperty>> categorySpecificPropertyMap ) {
        Session session = em.unwrap(Session.class);
        session.setDefaultReadOnly(true);
        session.setHibernateFlushMode(FlushMode.MANUAL);
        Article article = em.find(Article.class, articleId);
        long categoryUniqueId = article.getBasedOnProduct() == null ? article.getCategory().getUniqueId(): article.getBasedOnProduct().getCategory().getUniqueId();
        List<CategorySpecificProperty> categorySpecificPropertys = categorySpecificPropertyMap.get(categoryUniqueId);
        if( categorySpecificPropertys == null ) {
            LOG.log( Level.INFO, "categorySpecificPropertys is null, get them from category...");
            LOG.log( Level.INFO, "categoryUniqueId: {0}",  new Object[] {categoryUniqueId});
            categorySpecificPropertys = em.createNamedQuery(CategorySpecificProperty.FIND_BY_CATEGORY).
                        setParameter("categoryUniqueId", categoryUniqueId).
                        getResultList();
            LOG.log( Level.INFO, "categoryUniqueId: {0}, added {1} categorySpecificPropertys ",  new Object[] {categoryUniqueId, categorySpecificPropertys.size()});
            categorySpecificPropertyMap.put(categoryUniqueId, categorySpecificPropertys);
            LOG.log( Level.INFO, "mapping done...");
        }
        ArticleAPI articleAPI = ArticleMapper.map(article, true, categorySpecificPropertys);
        LOG.log( Level.INFO, "goto bulkIndexArticle...");
        elasticSearchController.bulkIndexArticle(articleAPI);
    }


}
