package se.inera.hjalpmedelstjansten.business.assortment.view;

import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.ResultExporterController;
import se.inera.hjalpmedelstjansten.business.assortment.controller.AssortmentController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.AssortmentAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.*;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * REST API for assortments on organization.
 *
 */
@Stateless
@Path("organizations/{organizationUniqueId}/assortments")
@Interceptors({ PerformanceLogInterceptor.class })
public class AssortmentOrganizationService extends BaseService {

    @Inject
    private HjmtLogger LOG;

    @EJB
    AssortmentController assortmentController;

    @Inject
    ResultExporterController resultExporterController;

    @EJB
    AuthHandler authHandler;

    /**
     * Search assortments by user query
     *
     * @param query the user query
     * @param statuses
     * @param include
     * @param organizationUniqueId
     * @param offset for pagination purpose
     * @return a list of <code>AgreementAPI</code> matching the query and a
     * header X-Total-Count giving the total number of agreements for pagination
     */
    @GET
    @SecuredService(permissions = {"assortment:view"})
    public Response searchAssortments(
            @DefaultValue(value = "") @QueryParam("query") String query,
            @QueryParam("status") List<Assortment.Status> statuses,
            @QueryParam("include") AssortmentAPI.Include include,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @DefaultValue(value = "0") @QueryParam("offset") int offset) {
        LOG.log(Level.FINEST, "searchAssortments( organizationUniqueId: {0}, offset: {1} )", new Object[] {organizationUniqueId, offset});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        if( query != null && query.isEmpty() ) {
            query = null;
        }
        boolean onlyWhereManager = include == AssortmentAPI.Include.MINE;
        if( statuses != null && statuses.size() == 3 ) {
            // means we should include all statuses, meaning we don't have to filter on it
            statuses = null;
        }
        SearchDTO searchDTO = assortmentController.searchAssortments(
                organizationUniqueId,
                query,
                statuses,
                onlyWhereManager,
                userAPI,
                offset,
                25);
        return Response.ok(searchDTO.getItems()).
                header("X-Total-Count", searchDTO.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }

    /**
     * Export assortments
     *
     * @param query the user query
     * @param statuses
     * @param include
     * @param organizationUniqueId
     * @return an excel containing the list of search results
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"assortment:view"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportAssortments(
            @DefaultValue(value = "") @QueryParam("query") String query,
            @QueryParam("status") List<Assortment.Status> statuses,
            @QueryParam("include") AssortmentAPI.Include include,
            @PathParam("organizationUniqueId") long organizationUniqueId) {
        LOG.log(Level.FINEST, "exportAssortments...");
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        if( query != null && query.isEmpty() ) {
            query = null;
        }

        boolean onlyWhereManager = include == AssortmentAPI.Include.MINE;

        if( statuses != null && statuses.size() == 3 ) {
            // means we should include all statuses, meaning we don't have to filter on it
            statuses = null;
        }

        int offset = 0;
        int maximumNumberOfResults = 55000;

        SearchDTO searchDTO = assortmentController.searchAssortments(organizationUniqueId, query, statuses, onlyWhereManager, userAPI, offset, maximumNumberOfResults);

        if (searchDTO == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                String filename = "Export_Utbud_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";

                List<AssortmentAPI> searchAssortmentsAPIs = (List<AssortmentAPI>) searchDTO.getItems();
                byte[] exportBytes = resultExporterController.generateAssortmentsResultList(searchAssortmentsAPIs);

                return Response.ok(
                        exportBytes,
                        jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                        .header("content-disposition", "attachment; filename = " + filename)
                        .build();
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to export users to file", ex);
                return Response.serverError().build();
            }
        }
    }

    /**
     * Get information about a specific assortment
     *
     * @param httpServletRequest
     * @param organizationUniqueId the unique id of the organization to get assortment from
     * @param assortmentUniqueId the id of the assortment to return
     * @return the corresponding <code>AssortmentAPI</code>
     */
    @GET
    @Path("{assortmentUniqueId}")
    @SecuredService(permissions = {"assortment:view"})
    public Response getAssortment(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("assortmentUniqueId") long assortmentUniqueId ) {
        LOG.log(Level.FINEST, "getAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if( !isSuperAdmin && !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        AssortmentAPI assortmentAPI = assortmentController.getAssortmentAPI(
                organizationUniqueId,
                assortmentUniqueId,
                userAPI,
                authHandler.getSessionId(),
                getRequestIp(httpServletRequest));
        if( assortmentAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(assortmentAPI).build();
        }
    }

    /**
     * Create new assortment
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to create assortment on
     * @param assortmentAPI user supplied values
     * @return the created <code>AssortmentAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @SecuredService(permissions = {"assortment:create"})
    public Response createAssortment(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            AssortmentAPI assortmentAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createAssortment( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        assortmentAPI = assortmentController.createAssortment(
                organizationUniqueId,
                assortmentAPI,
                userAPI,
                authHandler.getSessionId(),
                getRequestIp(httpServletRequest));
        return Response.
                ok(assortmentAPI).
                build();
    }

    /**
     * Update assortment
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to update assortment on
     * @param assortmentUniqueId
     * @param assortmentAPI user supplied values
     * @return the created <code>AssortmentAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @PUT
    @Path("{assortmentUniqueId}")
    @SecuredService(permissions = {"assortment:update"})
    public Response updateAssortment(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("assortmentUniqueId") long assortmentUniqueId,
            AssortmentAPI assortmentAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if( !isSuperAdmin && !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        assortmentAPI = assortmentController.updateAssortment(
                organizationUniqueId,
                assortmentUniqueId,
                assortmentAPI,
                userAPI,
                authHandler.getSessionId(),
                getRequestIp(httpServletRequest),
                isSuperAdmin);
        return Response.
                ok(assortmentAPI).
                build();
    }

    /**
     * Read articles on assortment
     *
     * @param httpServletRequest
     * @param query
     * @param offset
     * @param organizationUniqueId unique id of the organization to update assortment on
     * @param assortmentUniqueId
     * @param statuses list of statuses to include in search, if null or empty,
     * all statuses are included
     * @param articleTypes list of article types to include in search, if null or empty,
     * all article types are included
     * @param categoryId
     * @return a list of <code>ArticleAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @GET
    @Path("{assortmentUniqueId}/articles")
    @SecuredService(permissions = {"assortmentarticle:view"})
    public Response searchArticlesOnAssortment(@Context HttpServletRequest httpServletRequest,
            @DefaultValue(value = "") @QueryParam("query") String query,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @DefaultValue(value = "PUBLISHED") @QueryParam("status") List<Product.Status> statuses,
            @DefaultValue(value = "0") @QueryParam("selectedAssortmentUniqueId") long selectedAssortmentUniqueId,
            @DefaultValue(value = "") @QueryParam("sortOrder") String sortOrder,
            @DefaultValue(value = "") @QueryParam("sortType") String sortType,
            @QueryParam("type") List<Article.Type> articleTypes,
            @QueryParam("category") Long categoryId,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("assortmentUniqueId") long assortmentUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "searchArticlesOnAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if( !isSuperAdmin && !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        LinkedList<Article.Type> types = new LinkedList<>(articleTypes);
        if( types.isEmpty() ) {
            types.addAll(Arrays.asList(Article.Type.values()));
        }
        SearchDTO searchDTO = assortmentController.searchArticlesOnAssortment(
                query,
                offset,
                25,
                organizationUniqueId,
                assortmentUniqueId,
                selectedAssortmentUniqueId,
                userAPI,
                authHandler.getSessionId(),
                getRequestIp(httpServletRequest), statuses, articleTypes, categoryId, sortOrder, sortType);
        if( searchDTO == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(searchDTO.getItems()).
                    header("X-Total-Count", searchDTO.getCount()).
                    header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                    build();
        }
    }

    @GET
    @Path("{assortmentUniqueId}/articles/all")
    @SecuredService(permissions = {"assortmentarticle:view"})
    public Response searchArticlesOnAssortmentAll(@Context HttpServletRequest httpServletRequest,
                                               @DefaultValue(value = "") @QueryParam("query") String query,
                                               @DefaultValue(value = "0") @QueryParam("offset") int offset,
                                               @DefaultValue(value = "PUBLISHED") @QueryParam("status") List<Product.Status> statuses,
                                               @DefaultValue(value = "0") @QueryParam("selectedAssortmentUniqueId") long selectedAssortmentUniqueId,
                                               @QueryParam("type") List<Article.Type> articleTypes,
                                               @QueryParam("category") Long categoryId,
                                               @PathParam("organizationUniqueId") long organizationUniqueId,
                                               @PathParam("assortmentUniqueId") long assortmentUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "addArticlesToAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if( !isSuperAdmin && !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        LinkedList<Article.Type> types = new LinkedList<>(articleTypes);
        if( types.isEmpty() ) {
            types.addAll(Arrays.asList(Article.Type.values()));
        }
        SearchDTO searchDTO = assortmentController.searchArticlesOnAssortmentAll(
            query,
            organizationUniqueId,
            assortmentUniqueId,
            selectedAssortmentUniqueId,
            userAPI,
            authHandler.getSessionId(),
            getRequestIp(httpServletRequest), statuses, articleTypes, categoryId);
        if( searchDTO == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(searchDTO.getItems()).
                header("X-Total-Count", searchDTO.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                    build();
        }
    }

    /**
     * Read external categories on assortment
     *
     * @param httpServletRequest

     * @param organizationUniqueId unique id of the organization to update assortment on
     * @param assortmentUniqueId
     * @return a list <code>ExternalCategoryGrouping</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @GET
    @Path("{assortmentUniqueId}/externalCategories")
    @SecuredService(permissions = {"assortmentarticle:view"})
    public Response getExternalCategoriesOnAssortment(@Context HttpServletRequest httpServletRequest,
                                                      @DefaultValue(value = "") @QueryParam("query") String query,
                                                      @PathParam("organizationUniqueId") long organizationUniqueId,
                                                      @PathParam("assortmentUniqueId") long assortmentUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "getExternalCategoriesOnAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if( !isSuperAdmin && !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        List<ExternalCategoryGrouping> ecg = assortmentController.getExternalCategories(
            query,
            organizationUniqueId,
            assortmentUniqueId);



        if( ecg == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(ecg).build();
        }
    }

    /**
     * Read external categories on assortment
     *
     * @param httpServletRequest

     * @param organizationUniqueId unique id of the organization to update assortment on
     * @param assortmentUniqueId
     * @return a list <code>ExternalCategoryGrouping</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @GET
    @Path("{assortmentUniqueId}/allOtherexternalCategories")
    @SecuredService(permissions = {"assortmentarticle:view"})
    public Response getAllOtherExternalCategoriesOnAssortment(@Context HttpServletRequest httpServletRequest,
                                                      @DefaultValue(value = "") @QueryParam("query") String query,
                                                      @PathParam("organizationUniqueId") long organizationUniqueId,
                                                      @PathParam("assortmentUniqueId") long assortmentUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "getExternalCategoriesOnAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if( !isSuperAdmin && !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        List<ExternalCategoryGrouping> ecg = assortmentController.getAllOtherExternalCategories(
            query,
            organizationUniqueId,
            assortmentUniqueId);



        if( ecg == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(ecg).build();
        }
    }


    /**
     * Read external category exceptions on assortment
     *
     * @param httpServletRequest

     * @param organizationUniqueId unique id of the organization to update category exceptions on
     * @param assortmentUniqueId
     * @return a list <code>ExternalCategoryGrouping</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @GET
    @Path("{assortmentUniqueId}/categoryExceptions")
    @SecuredService(permissions = {"assortmentarticle:view"})
    public Response getCategoryExceptionsOnAssortment(@Context HttpServletRequest httpServletRequest,
                                                      @PathParam("organizationUniqueId") long organizationUniqueId,
                                                      @PathParam("assortmentUniqueId") long assortmentUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "getCategoryExceptionsOnAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if( !isSuperAdmin && !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        List<Long> categoryExceptions = assortmentController.getCategoryExceptions(
            organizationUniqueId,
            assortmentUniqueId);

        if( categoryExceptions == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(categoryExceptions).build();
        }
    }



    /**
     * Export articles for a specific assortment
     *
     * @param httpServletRequest
     * @param query
     * @param organizationUniqueId unique id of the organization
     * @param assortmentUniqueId
     * @return an excel containing the list of search results
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("{assortmentUniqueId}/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"assortmentarticle:view"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportArticlesForAssortment(
            @Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("assortmentUniqueId") long assortmentUniqueId,
            @DefaultValue(value = "PUBLISHED") @QueryParam("status") List<Product.Status> statuses,
            @QueryParam("type") List<Article.Type> articleTypes,
            @QueryParam("category") Long categoryId,
            @DefaultValue(value = "") @QueryParam("query") String query) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "exportArticlesOnAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[]{organizationUniqueId, assortmentUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if (!isSuperAdmin && !authorizeHandleOrganization(organizationUniqueId, userAPI)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        int maximumNumberOfResults = 55000;

        SearchDTO searchDTO = assortmentController.searchArticlesOnAssortment(query,
                0,
                maximumNumberOfResults,
                organizationUniqueId,
                assortmentUniqueId,
                0,
                userAPI,
                authHandler.getSessionId(),
                getRequestIp(httpServletRequest), statuses, articleTypes, categoryId, "", "");

        if (searchDTO == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                String filename = "Export_ArtiklarUtbud_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";

                List<ArticleAPI> searchArticlesAPIs = (List<ArticleAPI>) searchDTO.getItems();

                byte[] exportBytes = resultExporterController.generateArticlesList(searchArticlesAPIs);

                return Response.ok(
                        exportBytes,
                        jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                        .header("content-disposition", "attachment; filename = " + filename)
                        .build();
                } catch (IOException ex) {
                    LOG.log(Level.WARNING, "Failed to export articles for assortment to file", ex);
                    return Response.serverError().build();
                }
            }
        }


    /**
     * Add article to assortment
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to update assortment on
     * @param assortmentUniqueId
     * @param articleAPIs articles to add
     * @return 200 ok or 400 if validation fails
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("{assortmentUniqueId}/articles")
    @SecuredService(permissions = {"assortmentarticle:create_all", "assortmentarticle:create_own"})
    public Response addArticlesToAssortment(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("assortmentUniqueId") long assortmentUniqueId,
            List<ArticleAPI> articleAPIs) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "addArticlesToAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        boolean hasAllPermissions = authHandler.hasPermission("assortmentarticle:create_all");
        AssortmentAPI assortmentAPI = assortmentController.addArticlesToAssortment(
                organizationUniqueId,
                assortmentUniqueId,
                articleAPIs,
                userAPI,
                authHandler.getSessionId(),
                getRequestIp(httpServletRequest),
                hasAllPermissions);
        if( assortmentAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.
                    ok().
                    build();
        }
    }

    /**
     * Delete assortment and all articles
     *
     * @param organizationUniqueId unique id of organization
     * @param assortmentUniqueId the id of the assortment to delete
     * @return response code 200 if delete is successful, otherwise 404 if the user
     * cannot read the assortment or 400 if there is a validation error
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case validation fails
     */
    @DELETE
    @Path("{assortmentUniqueId}/delete")
    @SecuredService(permissions = {"assortmentarticle:delete_all", "assortmentarticle:delete_own"})
    public Response deleteAssortment(@Context HttpServletRequest httpServletRequest,
                                    @PathParam("organizationUniqueId") long organizationUniqueId,
                                    @PathParam("assortmentUniqueId") long assortmentUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteAssortment( assortmentUniqueId: {0} )", new Object[]{assortmentUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if (!authorizeHandleOrganization(organizationUniqueId, userAPI)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        boolean hasAllPermissions = authHandler.hasPermission("assortmentarticle:delete_all");
        assortmentController.deleteAssortment(organizationUniqueId, assortmentUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest), hasAllPermissions);
        return Response.ok().build();
    }

    /**
     * Delete articles from assortment
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to update assortment on
     * @param assortmentUniqueId
     * @param articleUniqueId unique id of the article to remove from assortment
     * @return 200 ok or 400 if validation fails
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @DELETE
    @Path("{assortmentUniqueId}/articles/{articleUniqueId}")
    @SecuredService(permissions = {"assortmentarticle:delete_all", "assortmentarticle:delete_own"})
    public Response deleteArticleFromAssortment(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("assortmentUniqueId") long assortmentUniqueId,
            @PathParam("articleUniqueId") long articleUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteArticleFromAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1}, articleUniqueId: {2} )", new Object[] {organizationUniqueId, assortmentUniqueId, articleUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        boolean hasAllPermissions = authHandler.hasPermission("assortmentarticle:delete_all");
        AssortmentAPI assortmentAPI = assortmentController.deleteArticleFromAssortment(
                organizationUniqueId,
                assortmentUniqueId,
                articleUniqueId,
                userAPI,
                authHandler.getSessionId(),
                getRequestIp(httpServletRequest),
                hasAllPermissions);
        if( assortmentAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.
                    ok().
                    build();
        }
    }

    /**
     * Get information about discontinued articles on assortments where logged in user is CustomerAssortmentManager, CustomerAssignedAssortmentManager or Superadmin
     *
     * @param organizationUniqueId the unique id of the organization
     * @return a list of <code>Article</code> and <code>Assortment</code>
     */
    @GET
    @Path("getArticlesDiscontinued")
    @SecuredService(permissions = {"assortmentarticle:view"})
    public Response getArticlesDiscontinued(
        @PathParam("organizationUniqueId") long organizationUniqueId) {
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        List<Object> discontinuedArticles = assortmentController.getArticlesDiscontinued(organizationUniqueId, userAPI);
        if(discontinuedArticles.isEmpty()) {
            return null;
        } else {
            return Response.ok(discontinuedArticles).build();
        }

    }

    /**
     * Delete from changechecker
     *
     * @param organizationUniqueId the unique id of the organization
     * @param userId userId
     * @param articleId article unique Id which is saved as changeId in ChangeChecker
     */
    @DELETE
    @Path("removeArticleDiscontinuedNotice/{articleId}/{userId}/{assortmentId}/{remove}")
    @SecuredService(permissions = {"agreement:view_own"})
    public Response removeArticleDiscontinuedNotice(
        @PathParam("assortmentId") long assortmentId,
        @PathParam("articleId") long articleId,
        @PathParam("organizationUniqueId") long organizationUniqueId,
        @PathParam("userId") long userId,
        @PathParam("remove") String remove
    ) {

        List<ChangeChecker> cc = assortmentController.removeArticleDiscontinuedNotice(assortmentId, articleId, userId, remove);

        return Response.ok().build();

    }
}
