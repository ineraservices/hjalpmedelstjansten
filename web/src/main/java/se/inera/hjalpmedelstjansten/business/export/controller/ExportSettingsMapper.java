package se.inera.hjalpmedelstjansten.business.export.controller;

import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistMapper;
import se.inera.hjalpmedelstjansten.business.organization.controller.BusinessLevelMapper;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationMapper;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.ExportSettingsAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistAPI;
import se.inera.hjalpmedelstjansten.model.entity.ExportSettings;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelist;
import se.inera.hjalpmedelstjansten.model.entity.Organization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Class for mapping between API and Entity classes
 *
 */
public class ExportSettingsMapper {

    public static final List<ExportSettingsAPI> map(List<ExportSettings> exportSettingses) {
        if( exportSettingses == null ) {
            return null;
        }
        List<ExportSettingsAPI> exportSettingsAPIs = new ArrayList<>();
        for( ExportSettings exportSettings : exportSettingses ) {
            exportSettingsAPIs.add(map(exportSettings, false));
        }
        return exportSettingsAPIs;
    }

    public static final ExportSettingsAPI map(ExportSettings exportSettings, boolean includeEverything) {
        if( exportSettings == null ) {
            return null;
        }
        ExportSettingsAPI exportSettingsAPI = new ExportSettingsAPI();
        exportSettingsAPI.setId(exportSettings.getUniqueId());
        exportSettingsAPI.setFilename(exportSettings.getFilename());
        exportSettingsAPI.setNumberOfExports(exportSettings.getNumberOfExports());
        //??? PELLE
        //exportSettingsAPI.setLastExported(exportSettings.getLastExported() == null ? null : exportSettings.getLastUpdated().getTime());
        exportSettingsAPI.setLastExported(exportSettings.getLastExported() == null ? null : exportSettings.getLastExported().getTime());

        exportSettingsAPI.setOrganization(OrganizationMapper.map(exportSettings.getOrganization(), false));
        exportSettingsAPI.setBusinessLevel(BusinessLevelMapper.map(exportSettings.getBusinessLevel(), false));
        exportSettingsAPI.setEnabled(exportSettings.isEnabled());
        if( includeEverything ) {
            if( exportSettings.getGeneralPricelists() != null && !exportSettings.getGeneralPricelists().isEmpty() ) {
                List<GeneralPricelistAPI> generalPricelistAPIs = new ArrayList<>();
                for( GeneralPricelist generalPricelist : exportSettings.getGeneralPricelists() ) {
                    generalPricelistAPIs.add(GeneralPricelistMapper.map(generalPricelist, false));
                }
                Collections.sort(generalPricelistAPIs, new Comparator<GeneralPricelistAPI>() {
                        public int compare(GeneralPricelistAPI s1, GeneralPricelistAPI s2) {
                            return s1.getOwnerOrganization().getOrganizationName().compareToIgnoreCase(s2.getOwnerOrganization().getOrganizationName());
                        }
                    });

                exportSettingsAPI.setGeneralPricelists(generalPricelistAPIs);
            }
        }
        return exportSettingsAPI;
    }

    public static final ExportSettings map(ExportSettingsAPI exportSettingsAPI) {
        if( exportSettingsAPI == null ) {
            return null;
        }
        ExportSettings exportSettings = new ExportSettings();
        exportSettings.setFilename(exportSettingsAPI.getFilename());
        exportSettings.setNumberOfExports(0);
        return exportSettings;
    }

}
