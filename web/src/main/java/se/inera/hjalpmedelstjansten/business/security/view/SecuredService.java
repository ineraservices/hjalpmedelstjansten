package se.inera.hjalpmedelstjansten.business.security.view;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for specifying required permissions for the annotated method. Actual 
 * check if permission exists is done in referenced classes 
 * <code>AuthenticationFeature</code> and <code>AuthenticationFilter</code>
 * 
 * @see se.inera.hjalpmedelstjansten.business.security.view.AuthenticationFeature
 * @see se.inera.hjalpmedelstjansten.business.security.view.AuthenticationFilter
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SecuredService {

    String[] permissions();

}
