package se.inera.hjalpmedelstjansten.business.security.view;

import java.util.logging.Level;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.security.controller.StandardUserToken;
import se.inera.hjalpmedelstjansten.business.security.controller.TokenUserToken;

/**
 * Interface used to be able to substitute authentication framework without
 * having to rewrite all code.
 *
 */
@Stateless
public class ShiroAuthHandler implements AuthHandler {

    @Inject
    private HjmtLogger LOG;

    @Override
    public void standardLogin(String username, String password) {
        try {
            Subject currentUser = SecurityUtils.getSubject();
            StandardUserToken standardUserToken = new StandardUserToken(username, password);
            currentUser.login(standardUserToken);
        } catch( UnknownAccountException e ) {
            LOG.log(Level.INFO, "Attempt to login with unknown account");
            throw e;
        } catch( Exception e ) {
            LOG.log(Level.WARNING, "Attempt to login of unhandled reason", e);
            throw e;
        }
    }

    @Override
    public void tokenLogin(String organizationNumber, String token) {
        try {
            Subject currentUser = SecurityUtils.getSubject();
            TokenUserToken tokenUserToken = new TokenUserToken(organizationNumber, token);
            currentUser.login(tokenUserToken);
        } catch( UnknownAccountException e ) {
            LOG.log(Level.INFO, "Attempt to login with unknown account");
            throw e;
        } catch( Exception e ) {
            LOG.log(Level.WARNING, "Attempt to login of unhandled reason", e);
            throw e;
        }
    }

    @Override
    public void addToSession( Object key, Object value ) {
        Subject currentUser = SecurityUtils.getSubject();
        currentUser.getSession().setAttribute(key, value);
    }

    @Override
    public Object getFromSession( Object key ) {
        Subject currentUser = SecurityUtils.getSubject();
        return currentUser.getSession().getAttribute(key);
    }

    @Override
    public boolean hasRole(String role) {
        Subject currentUser = SecurityUtils.getSubject();
        return currentUser.hasRole(role);
    }

    @Override
    public boolean hasPermission(String permission) {
        Subject currentUser = SecurityUtils.getSubject();
        return currentUser.isPermitted(permission);
    }

    @Override
    public String getSessionId() {
        return SecurityUtils.getSubject().getSession().getId().toString();
    }

    @Override
    public boolean isAthenticated() {
        return SecurityUtils.getSubject() != null && SecurityUtils.getSubject().isAuthenticated();
    }
}
