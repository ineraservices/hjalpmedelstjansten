package se.inera.hjalpmedelstjansten.business.agreement.controller.exportimport;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import jakarta.ejb.Asynchronous;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.mail.MessagingException;
import jakarta.transaction.Transactional;
import lombok.NoArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistRowMapper;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistRowValidation;
import se.inera.hjalpmedelstjansten.business.helpers.ImportExcelHelper;
import se.inera.hjalpmedelstjansten.business.importstatus.controller.ImportStatusController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.MdcToolkit;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitController;
import se.inera.hjalpmedelstjansten.business.property.view.PropertyLoader;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.EmailController;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.ImportStatusAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

/**
 * Asynchronous import of read agreement pricelist rows
 *
 */
@Stateless
@NoArgsConstructor
public class ImportGeneralPricelistPricelistWriterAsynchController extends ExportImportPricelistController {
    private int importInProgress = 0;
    private int importComplete = 1;
    private int importFailed = 2;

    @Inject
    HjmtLogger LOG;

    @Inject
    ImportExcelHelper importExcelHelper;

    @Inject
    ImportAgreementPricelistController importPricelistController;

    @Inject
    EmailController emailController;

    @Inject
    GeneralPricelistPricelistRowValidation generalPricelistPricelistRowValidation;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    GuaranteeUnitController guaranteeUnitController;

    @Inject
    ImportStatusController importStatusController;

    @Inject
    OrganizationController organizationController;


    String importPricelistRowsMailSubjectSuccessful;
    String importPricelistRowsMailBodySuccessfulOnlyRows;
    String importPricelistRowsMailSubjectFailed;
    String importPricelistRowsMailBodyFailed;

    @Inject
    public ImportGeneralPricelistPricelistWriterAsynchController(PropertyLoader propertyLoader) {
        importPricelistRowsMailSubjectSuccessful = propertyLoader.getMessage("importPricelistRowsMailSubjectSuccessful");
        importPricelistRowsMailBodySuccessfulOnlyRows = propertyLoader.getMessage("importPricelistRowsMailBodySuccessfulOnlyRows");
        importPricelistRowsMailSubjectFailed = propertyLoader.getMessage("importPricelistRowsMailSubjectFailed");
        importPricelistRowsMailBodyFailed = propertyLoader.getMessage("importPricelistRowsMailBodyFailed");
    }

    @Asynchronous
    @Transactional
    @TransactionTimeout(14400)
    public void readAndSaveItems(GeneralPricelistPricelist generalPricelistPricelist,
                                 long organizationUniqueId,
                                 long pricelistUniqueId,
                                 XSSFWorkbook workbook,
                                 UserAPI userAPI,
                                 String sessionId,
                                 String requestIp,
                                 String filename, Map<String, String> mdc) {
        try {
            MdcToolkit.setMdc(mdc);
            readAndSaveItems(generalPricelistPricelist, organizationUniqueId, pricelistUniqueId, workbook, userAPI, sessionId, requestIp, filename);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error reading and saving general pricelist", e);
        } finally {
            MdcToolkit.clearMdc();
        }
    }

    private void readAndSaveItems(GeneralPricelistPricelist generalPricelistPricelist,
        long organizationUniqueId,
        long pricelistUniqueId,
        XSSFWorkbook workbook,
        UserAPI userAPI,
        String sessionId,
        String requestIp,
        String filename) {
        LOG.log(Level.FINEST, "importWorkbook( organizationUniqueId: {0} )", new Object[]{organizationUniqueId});
        HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validering misslyckades");
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        GeneralPricelistPricelist currentPricelist = generalPricelistPricelistController.getCurrentPricelist(organizationUniqueId);
        List<String> pricelistArticleUniqueNumbers = generalPricelistPricelistRowController.getGeneralPricelistPricelistArticleUniqueNumbers(pricelistUniqueId);
        ImportStatusAPI importStatusAPI = createImportStatusForFile(filename,importInProgress,userAPI);
        // first read items from excel to list of rows and validate them
        List<ImportItem> importItems = readItems(organizationUniqueId,
                pricelistUniqueId,
                generalPricelistPricelist,
                currentPricelist,
                pricelistArticleUniqueNumbers,
                workbook,
                organization,
                exception,
                userAPI
                );
        // we stop if there are validation errors already
        LOG.log(Level.FINEST, "Finished reading items. Found: {0} validation errors", new Object[]{exception.getValidationMessages().size()});

        try {
            if (exception.getValidationMessages().isEmpty()) {
                // now it's time to save rows
                List<Long> pricelistArticleUniqueIds = agreementPricelistRowController.getAgreementPricelistArticleUniqueIds(pricelistUniqueId);
                LOG.log(Level.FINEST, "Number of article unique ids: {0}", new Object[]{pricelistArticleUniqueIds.size()});
                saveItems(organizationUniqueId,
                        pricelistUniqueId,
                        importItems,
                        generalPricelistPricelist,
                        currentPricelist,
                        pricelistArticleUniqueIds,
                        userAPI,
                        sessionId,
                        requestIp);

            }
        } catch (Exception ex) {
            try {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                String exceptionAsString = sw.toString();
                updateImportStatusForFile(importStatusAPI,importFailed);
                String mailText = "Okänt fel vid validering. Är kolumnerna rätt formaterade? Detaljer: <br/><br/>" + ex.getMessage() + "<br/> <br/><b>Stacktrace att skicka till support</b><br/>" + exceptionAsString;
                String mailBody = String.format(importPricelistRowsMailBodyFailed, mailText);
                emailController.send(userAPI.getElectronicAddress().getEmail(), filename + ":" + importPricelistRowsMailSubjectFailed, mailBody);
            } catch (MessagingException ex2) {
                LOG.log(Level.SEVERE, "Failed to send message on import General pricelist", ex2);
            }
            LOG.log(Level.SEVERE, "Failed to import General pricelist", ex);
            return;
        }

        try {
            if (!exception.getValidationMessages().isEmpty()) {
                LOG.log(Level.FINEST, "Import failed, send fail message");
                updateImportStatusForFile(importStatusAPI,importFailed);
                StringBuilder allErrors = new StringBuilder();
                for (ErrorMessageAPI errorMessageAPI : exception.getValidationMessages()) {
                    allErrors.append(errorMessageAPI.getMessage()).append("<br />");
                }
                String mailBody = String.format(importPricelistRowsMailBodyFailed, allErrors.toString());
                emailController.send(userAPI.getElectronicAddress().getEmail(), filename + ":" + importPricelistRowsMailSubjectFailed, mailBody);
            } else {
                LOG.log(Level.FINEST, "Import successful of {0} items, send success message", new Object[]{importItems.size()});
                updateImportStatusForFile(importStatusAPI,importComplete);
                String mailBody = String.format(importPricelistRowsMailBodySuccessfulOnlyRows, importItems.size());
                emailController.send(userAPI.getElectronicAddress().getEmail(), filename + ":" + importPricelistRowsMailSubjectSuccessful, mailBody);
            }
        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on import pricelist", ex);
        }

        if (exception.hasValidationMessages()) {
            LOG.log(Level.WARNING, "General pricelist import failed due to validation errors: {0}", new Object[]{exception.validationMessagesAsString()});
        }

    }


    public void saveItems(long organizationUniqueId,
                          long pricelistUniqueId,
                          List<ImportItem> importItems,
                          GeneralPricelistPricelist generalPricelistPricelist,
                          GeneralPricelistPricelist currentPricelist,
                          List<Long> pricelistArticleUniqueIds,
                          UserAPI userAPI,
                          String sessionId,
                          String requestIp) {
        LOG.log(Level.FINEST, "saveItems( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[]{organizationUniqueId, pricelistUniqueId});
        HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
        for (ImportItem importItem : importItems) {
            GeneralPricelistPricelistRowAPI generalPricelistPricelistRowAPI = importItem.getGeneralPricelistPricelistRowAPI();
            try {
                // id
                if (generalPricelistPricelistRowAPI.getId() == null) {
                    generalPricelistPricelistRowController.createPricelistRow(organizationUniqueId,
                            generalPricelistPricelistRowAPI,
                            generalPricelistPricelist,
                            currentPricelist,
                            pricelistArticleUniqueIds,
                            userAPI,
                            sessionId,
                            requestIp);
                    pricelistArticleUniqueIds.add(generalPricelistPricelistRowAPI.getArticle().getId());
                } else {
                    generalPricelistPricelistRowController.updatePricelistRow(organizationUniqueId,
                            pricelistUniqueId,
                            generalPricelistPricelistRowAPI.getId(),
                            generalPricelistPricelistRowAPI,
                            userAPI,
                            sessionId,
                            requestIp);
                }
            } catch (HjalpmedelstjanstenValidationException ex) {
                for (ErrorMessageAPI errorMessageAPI : ex.getValidationMessages()) {
                    importPricelistController.addRowErrorMessage(exception, importItem.getRowNumber(), errorMessageAPI.getField(), errorMessageAPI.getMessage());
                }
                // we break immediately since the transaction is done, errors should
                // already have been handled before we get to this stage. if validation
                // errors still exist, they should be handled in the read-phase
                // in ImportPricelistController
                break;
            } catch (Exception ex) {
                LOG.log(Level.WARNING, "Failed to handle pricelist row import", ex);
                importPricelistController.addRowErrorMessage(exception, importItem.getRowNumber(), "uknown", validationMessageService.getMessage("import.pricelist.unhandledException"));
                // we break immediately since this is an "unhandled" error
                break;
            }
        }

    }

    private List<ImportItem> readItems(long organizationUniqueId,
                                       long pricelistUniqueId,
                                       GeneralPricelistPricelist generalPricelistPricelist,
                                       GeneralPricelistPricelist currentPricelist,
                                       List<String> pricelistArticleUniqueNumbers,
                                       XSSFWorkbook workbook,
                                       Organization organization,
                                       HjalpmedelstjanstenValidationException exception,
                                       UserAPI userAPI) {
        LOG.log(Level.FINEST, "readItems( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[]{organizationUniqueId, pricelistUniqueId});
        List<ImportItem> importItems = new ArrayList<>();
        Map<String, CVGuaranteeUnit> guaranteeUnitNamesMap = guaranteeUnitController.getGuaranteeUnitNamesMap();
        XSSFSheet sheet = workbook.getSheet(GENERAL_PRICELISTROW_SHEET_NAME);
        XSSFRow fieldNamesRow = sheet.getRow(0);
        int i = 2;
        XSSFRow row;
        Long artUniqueId;
        while (!isEmptyRow(row = sheet.getRow(i), fieldNamesRow)) {
            i++;
            // id
            XSSFCell uniqueIdCell = row.getCell(0);
            Long uniqueId = uniqueIdCell == null ? null : (long) uniqueIdCell.getNumericCellValue();
            LOG.log(Level.FINEST, "Found pricelist row unique id: {0}", new Object[]{uniqueId});

            //if uniqueId == null, try to get it from existing
            String articleNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_ARTICLE_NUMBER);
            if (uniqueId == null) {
                artUniqueId = articleController.findByArticleNumberAndOrganization(articleNumber, organization.getUniqueId()) == null ? null : articleController.findByArticleNumberAndOrganization(articleNumber, organization.getUniqueId()).getUniqueId();
                //uniqueId = artUniqueId == null ? null : generalPricelistPricelistRowController.getGeneralPricelistPricelistRowUniqueId(artUniqueId, pricelistUniqueId).get(0);
                //nedanstående fixar TFF-605859
                uniqueId = artUniqueId == null ? null : generalPricelistPricelistRowController.getGeneralPricelistPricelistRowUniqueId(artUniqueId, pricelistUniqueId).size() == 0 ? null : generalPricelistPricelistRowController.getGeneralPricelistPricelistRowUniqueId(artUniqueId, pricelistUniqueId).get(0);
            }

            //convert daGAr and kiLOmeTEr etc to correct format: Dagar and Kilometer
            XSSFCell warrantyQuantityUnitCell = this.importExcelHelper.getCellByName(fieldNamesRow, row, FIELD_WARRANTY_QUANTITY_UNIT_NAME);
            if(warrantyQuantityUnitCell != null){
                if (warrantyQuantityUnitCell.getStringCellValue().equalsIgnoreCase(DAGAR)){
                    warrantyQuantityUnitCell.setCellValue(DAGAR);
                }
                if (warrantyQuantityUnitCell.getStringCellValue().equalsIgnoreCase(KILOMETER)){
                    warrantyQuantityUnitCell.setCellValue(KILOMETER);
                }
            }

            GeneralPricelistPricelistRow generalPricelistPricelistRow = uniqueId == null ? null : generalPricelistPricelistRowController.getPricelistRow(organizationUniqueId, pricelistUniqueId, uniqueId, userAPI);
            GeneralPricelistPricelistRowAPI generalPricelistPricelistRowAPI = generalPricelistPricelistRow == null ? new GeneralPricelistPricelistRowAPI() : GeneralPricelistPricelistRowMapper.map(generalPricelistPricelistRow, true);
            //String articleNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_ARTICLE_NUMBER);
            boolean getIdFromExisting = false;
            if (generalPricelistPricelistRowAPI.getId() == null) {
                // this is an add, check article number already exists
                // HJAL-2022
                // this is an update if article number exists
                // get the id from the existing
                if (pricelistArticleUniqueNumbers.contains(articleNumber)) {
                    getIdFromExisting = true;
                    //addRowErrorMessage(exception, row.getRowNum(), "articleNumber", validationMessageService.getMessage("pricelistrow.article.alreadyExistsInPricelist", articleNumber));
                    // no need to continue
                    //continue;
                }
            }
            boolean isNewOrChanged = fillGeneralPricelistPricelistRowAPI(getIdFromExisting, organization, guaranteeUnitNamesMap, row, fieldNamesRow, generalPricelistPricelistRowAPI, exception);
            // if not success, it means something caused the fill not to complete
            // in that case we don't have to validate
            if (isNewOrChanged) {
                Set<ErrorMessageAPI> errorMessageAPIs;
                if (generalPricelistPricelistRowAPI.getId() == null) {
                    errorMessageAPIs = generalPricelistPricelistRowValidation.tryForCreate(generalPricelistPricelistRowAPI, generalPricelistPricelist, currentPricelist);
                } else {
                    errorMessageAPIs = generalPricelistPricelistRowValidation.tryForUpdate(generalPricelistPricelistRowAPI, generalPricelistPricelistRow, currentPricelist);
                }
                for (ErrorMessageAPI errorMessageAPI : errorMessageAPIs) {
                    addRowErrorMessage(exception, row.getRowNum(), errorMessageAPI.getField(), errorMessageAPI.getMessage());
                }
                importItems.add(new ImportItem(generalPricelistPricelistRowAPI, row.getRowNum()));
            }
        }
        return importItems;
    }

    private ImportStatusAPI createImportStatusForFile(String fileName, int status, UserAPI userAPI){
        ImportStatusAPI importStatusAPI = new ImportStatusAPI();
//        long id = 1;
//        status.setUniqueId(id);
        importStatusAPI.setFileName(fileName);
        importStatusAPI.setReadStatus(status);
        importStatusAPI.setChangedBy(userAPI.getId().toString());
        Calendar calendar = Calendar.getInstance();
        importStatusAPI.setImportStartDate(calendar.getTime());
        ImportStatusAPI returnValue = importStatusController.createImportStatusForFile(importStatusAPI);
        return returnValue;
    }

    private void updateImportStatusForFile(ImportStatusAPI importStatusAPI, int status){
        importStatusAPI.setReadStatus(status);

        importStatusController.updateImportStatus(importStatusAPI);
    }


    public void addRowErrorMessage(HjalpmedelstjanstenValidationException exception, int rowNumber, String field, String message) {
        String errorOnRowMessage = validationMessageService.getMessage("import.pricelist.errorOnRow", rowNumber + 1);
        exception.addValidationMessage(field, errorOnRowMessage + " " + message);
    }

    private boolean isEmptyRow(XSSFRow row, XSSFRow fieldNamesRow) {
        if (row == null) {
            return true;
        }
        String articleNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_ARTICLE_NUMBER);
        if (articleNumber == null || articleNumber.isEmpty()) {
            return true;
        }
        return false;
    }

    private boolean fillGeneralPricelistPricelistRowAPI(
            boolean getIdFromExisting,
            Organization organization,
            Map<String, CVGuaranteeUnit> guaranteeUnitNamesMap,
            XSSFRow row,
            XSSFRow fieldNamesRow,
            GeneralPricelistPricelistRowAPI generalPricelistPricelistRowAPI,
            HjalpmedelstjanstenValidationException exception) {
        LOG.log(Level.FINEST, "fillGeneralPricelistPricelistRowAPI( organization->uniqueId: {0}, generalPricelistPricelistRowAPI->id: {1} )", new Object[]{organization.getUniqueId(), generalPricelistPricelistRowAPI.getId()});
        boolean isNewOrChanged = false;
        Long gpprId;
        if (generalPricelistPricelistRowAPI.getId() == null) {
            if (!getIdFromExisting) {
                generalPricelistPricelistRowAPI.setStatus(GeneralPricelistPricelistRow.Status.ACTIVE.toString());
                isNewOrChanged = true;
                String articleNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_ARTICLE_NUMBER);
                ArticleAPI articleAPI = articleController.getArticleAPIByArticleNumber(organization.getUniqueId(), articleNumber);
                generalPricelistPricelistRowAPI.setArticle(articleAPI);
            }
            else {
                generalPricelistPricelistRowAPI.setStatus(GeneralPricelistPricelistRow.Status.ACTIVE.toString());
                isNewOrChanged = true;
                String articleNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_ARTICLE_NUMBER);
                ArticleAPI articleAPI = articleController.getArticleAPIByArticleNumber(organization.getUniqueId(), articleNumber);
                gpprId = articleController.findByArticleNumberAndOrganization(articleNumber, organization.getUniqueId()).getUniqueId();
                generalPricelistPricelistRowAPI.setId(gpprId);
                generalPricelistPricelistRowAPI.setArticle(articleAPI);
            }
        }

        // price
        Double price = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, row, FIELD_PRICE_NAME);
        if (price != null) {
            generalPricelistPricelistRowAPI.setPrice(BigDecimal.valueOf(price));
            isNewOrChanged = true;
        }

        // valid from
        Date validFromDate = this.importExcelHelper.getCellDateValueByName(fieldNamesRow, row, FIELD_VALID_FROM_NAME);
        if (validFromDate != null) {
            generalPricelistPricelistRowAPI.setValidFrom(validFromDate.getTime());
            isNewOrChanged = true;
        }

        // least order quantity
        Double leastOrderQuantity = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, row, FIELD_LEAST_ORDER_QUANTITY_NAME);
        if (leastOrderQuantity != null) {
            generalPricelistPricelistRowAPI.setLeastOrderQuantity(leastOrderQuantity.intValue());
            isNewOrChanged = true;
        }

        // delivery time
        Double deliveryTime = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, row, FIELD_DELIVERY_TIME_NAME);
        if (deliveryTime != null) {
            generalPricelistPricelistRowAPI.setDeliveryTime(deliveryTime.intValue());
            isNewOrChanged = true;
        }

        // warranty quantity
        Double warrantyQuantity = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, row, FIELD_WARRANTY_QUANTITY_NAME);
        if (warrantyQuantity != null) {
            if (CLEAR_VALUE_DOUBLE.equals(warrantyQuantity)) {
                generalPricelistPricelistRowAPI.setWarrantyQuantity(null);
            } else {
                generalPricelistPricelistRowAPI.setWarrantyQuantity(warrantyQuantity.intValue());
            }
            isNewOrChanged = true;
        }

        // warranty quantity unit
        String warrantyQuantityUnitName = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_WARRANTY_QUANTITY_UNIT_NAME);
        if (warrantyQuantityUnitName != null && !warrantyQuantityUnitName.isEmpty()) {
            CVGuaranteeUnitAPI warrantyQuantityUnitAPI = getGuaranteeUnitAPI(warrantyQuantityUnitName, row.getRowNum(), exception, guaranteeUnitNamesMap);
            generalPricelistPricelistRowAPI.setWarrantyQuantityUnit(warrantyQuantityUnitAPI);
            isNewOrChanged = true;
        }

        // warranty valid from
        String warrantyValidFromName = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_WARRANTY_VALID_FROM_NAME);
        if (warrantyValidFromName != null && !warrantyValidFromName.isEmpty()) {
            generalPricelistPricelistRowAPI.setWarrantyValidFrom(getPreventiveMaintenanceAPI(warrantyValidFromName));
            isNewOrChanged = true;
        }

        // warranty terms
        String warrantyTerms = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_WARRANTY_TERMS_NAME);
        if (warrantyTerms != null && !warrantyTerms.isEmpty()) {
            if (CLEAR_VALUE_STRING.equals(warrantyTerms)) {
                generalPricelistPricelistRowAPI.setWarrantyTerms(null);
            } else {
                generalPricelistPricelistRowAPI.setWarrantyTerms(warrantyTerms);
            }
            isNewOrChanged = true;
        }
        return isNewOrChanged;
    }

    private CVGuaranteeUnitAPI getGuaranteeUnitAPI(String guaranteeUnitName, int rowNumber, HjalpmedelstjanstenValidationException exception, Map<String, CVGuaranteeUnit> guaranteeUnitNamesMap) {
        if (guaranteeUnitName != null && !guaranteeUnitName.trim().isEmpty()) {
            CVGuaranteeUnit guaranteeUnit = guaranteeUnitNamesMap.get(guaranteeUnitName);
            if (guaranteeUnit == null) {
                addRowErrorMessage(exception, rowNumber, "orderUnit", validationMessageService.getMessage("import.pricelist.guaranteeunit.notFound", guaranteeUnitName));
            } else {
                CVGuaranteeUnitAPI guaranteeUnitAPI = new CVGuaranteeUnitAPI();
                guaranteeUnitAPI.setId(guaranteeUnit.getUniqueId());
                return guaranteeUnitAPI;
            }
        }
        return null;
    }

    private CVPreventiveMaintenanceAPI getPreventiveMaintenanceAPI(String preventiveMaintenanceName) {
        if (preventiveMaintenanceName != null && !preventiveMaintenanceName.trim().isEmpty()) {
            CVPreventiveMaintenance preventiveMaintenance = preventiveMaintenanceUnitController.findByName(preventiveMaintenanceName);
            CVPreventiveMaintenanceAPI preventiveMaintenanceAPI = new CVPreventiveMaintenanceAPI();
            preventiveMaintenanceAPI.setId(preventiveMaintenance.getUniqueId());
            preventiveMaintenanceAPI.setCode(preventiveMaintenance.getCode());
            return preventiveMaintenanceAPI;
        }
        return null;
    }
}
