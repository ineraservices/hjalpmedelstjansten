package se.inera.hjalpmedelstjansten.model.api.validation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistRowControllerTest;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelistRow;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Set;


public class GeneralPricelistPricelistRowAPITest {

    private Validator beanValidator;

    static final int pricelistRowLeastOrderQuantity = 1;
    static final BigDecimal pricelistRowPrice = BigDecimal.TEN;
    static final long pricelistRowId = 1;
    static final long articleId = 2;

    @Before
    public void init() {
        beanValidator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    public void testAllEmpty() {
        GeneralPricelistPricelistRowAPI pricelistRowAPI = new GeneralPricelistPricelistRowAPI();
        Set<ConstraintViolation<GeneralPricelistPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistRowAPI, Default.class);
        Assert.assertEquals(3, constraintViolations.size());
        boolean leastOrderQuantityFound = false;
        boolean articleFound = false;
        boolean statusFound = false;
        Iterator<ConstraintViolation<GeneralPricelistPricelistRowAPI>> it = constraintViolations.iterator();
        while( it.hasNext() ) {
            ConstraintViolation<GeneralPricelistPricelistRowAPI> constraintViolation = it.next();
            if( "{pricelistrow.leastOrderQuantity.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                leastOrderQuantityFound = true;
            } else if( "{pricelistrow.article.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                articleFound = true;
            } else if( "{pricelistrow.status.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                statusFound = true;
            }
        }
        Assert.assertTrue(leastOrderQuantityFound);
        Assert.assertTrue(articleFound);
        Assert.assertTrue(statusFound);
    }

    @Test
    public void testAllRight() {
        GeneralPricelistPricelistRowAPI pricelistAPI = GeneralPricelistPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowPrice, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        Set<ConstraintViolation<GeneralPricelistPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    @Test
    public void testPriceMinusValid() {
        GeneralPricelistPricelistRowAPI pricelistAPI = GeneralPricelistPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowPrice, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        pricelistAPI.setPrice(new BigDecimal(-10));
        Set<ConstraintViolation<GeneralPricelistPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    @Test
    public void testInvalidValidFrom() {
        GeneralPricelistPricelistRowAPI pricelistAPI = GeneralPricelistPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowPrice, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        pricelistAPI.setValidFrom(-1l);
        Set<ConstraintViolation<GeneralPricelistPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelistrow.validFrom.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testNullLeastOrderQuantity() {
        GeneralPricelistPricelistRowAPI pricelistAPI = GeneralPricelistPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowPrice, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        pricelistAPI.setLeastOrderQuantity(null);
        Set<ConstraintViolation<GeneralPricelistPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelistrow.leastOrderQuantity.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testInvalidLeastOrderQuantity() {
        GeneralPricelistPricelistRowAPI pricelistAPI = GeneralPricelistPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowPrice, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        pricelistAPI.setLeastOrderQuantity(-1);
        Set<ConstraintViolation<GeneralPricelistPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelistrow.leastOrderQuantity.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testInvalidDeliveryTime() {
        GeneralPricelistPricelistRowAPI pricelistAPI = GeneralPricelistPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowPrice, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        pricelistAPI.setDeliveryTime(-1);
        Set<ConstraintViolation<GeneralPricelistPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelistrow.deliveryTime.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testInvalidWarrantyQuantity() {
        GeneralPricelistPricelistRowAPI pricelistAPI = GeneralPricelistPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowPrice, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        pricelistAPI.setWarrantyQuantity(-1);
        Set<ConstraintViolation<GeneralPricelistPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelistrow.warrantyQuantity.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testInvalidWarrantyTermsTooLong() {
        GeneralPricelistPricelistRowAPI pricelistAPI = GeneralPricelistPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowPrice, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        pricelistAPI.setWarrantyTerms("abcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyz");
        Set<ConstraintViolation<GeneralPricelistPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelistrow.warrantyTerms.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testNullArticle() {
        GeneralPricelistPricelistRowAPI pricelistAPI = GeneralPricelistPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowPrice, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        pricelistAPI.setArticle(null);
        Set<ConstraintViolation<GeneralPricelistPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelistrow.article.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }

}
