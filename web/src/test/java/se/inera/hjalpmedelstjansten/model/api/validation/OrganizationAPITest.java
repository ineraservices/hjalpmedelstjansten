package se.inera.hjalpmedelstjansten.model.api.validation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.PostAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountryAPI;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class OrganizationAPITest {

    private Validator validator;

    @Before
    public void init() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    public void testAllEmpty() {
        OrganizationAPI organizationAPI = new OrganizationAPI();
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(8, constraintViolations.size());
        boolean typeFound = false;
        boolean nameFound = false;
        boolean countryFound = false;
        boolean numberFound = false;
        boolean glnFound = false;
        boolean validFromFound = false;
        boolean electronicAddressFound = false;
        boolean postAddressFound = false;
        Iterator<ConstraintViolation<OrganizationAPI>> it = constraintViolations.iterator();
        while( it.hasNext() ) {
            ConstraintViolation<OrganizationAPI> constraintViolation = it.next();
            if( "{organization.organizationType.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                typeFound = true;
            } else if( "{organization.name.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                nameFound = true;
            } else if( "{organization.country.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                countryFound = true;
            } else if( "{organization.number.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                numberFound = true;
            } else if( "{organization.gln.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                glnFound = true;
            } else if( "{organization.validFrom.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                validFromFound = true;
            } else if( "{organization.electronicAddress.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                electronicAddressFound = true;
            } else if( "{organization.postAddresses.wrongSize}".equals(constraintViolation.getMessageTemplate()) ) {
                postAddressFound = true;
            }
        }
        Assert.assertTrue(typeFound);
        Assert.assertTrue(nameFound);
        Assert.assertTrue(countryFound);
        Assert.assertTrue(numberFound);
        Assert.assertTrue(glnFound);
        Assert.assertTrue(validFromFound);
        Assert.assertTrue(electronicAddressFound);
        Assert.assertTrue(postAddressFound);
    }

    @Test
    public void testAllRight() {
        OrganizationAPI organizationAPI = createValidOrganization();
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    @Test
    public void testTooLongName() {
        OrganizationAPI organizationAPI = createValidOrganization();
        organizationAPI.setOrganizationName("abcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyz");
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{organization.name.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testTooLongNumber() {
        OrganizationAPI organizationAPI = createValidOrganization();
        organizationAPI.setOrganizationNumber("abcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyz");
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{organization.number.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testTooShortGLN() {
        OrganizationAPI organizationAPI = createValidOrganization();
        organizationAPI.setGln("100000000000");
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{organization.gln.notValid.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testInvalidGLN() {
        OrganizationAPI organizationAPI = createValidOrganization();
        organizationAPI.setGln("1000000000000");
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{organization.gln.notValid.checkSum}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testTooManyPostAddresses() {
        OrganizationAPI organizationAPI = createValidOrganization();
        List<PostAddressAPI> postAddressAPIs = new ArrayList<>();
        PostAddressAPI visit = new PostAddressAPI();
        postAddressAPIs.add(visit);
        PostAddressAPI delivery = new PostAddressAPI();
        postAddressAPIs.add(delivery);
        PostAddressAPI oneTooMany = new PostAddressAPI();
        postAddressAPIs.add(oneTooMany);
        organizationAPI.setPostAddresses(postAddressAPIs);
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{organization.postAddresses.wrongSize}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testTooFewPostAddresses() {
        OrganizationAPI organizationAPI = createValidOrganization();
        List<PostAddressAPI> postAddressAPIs = new ArrayList<>();
        PostAddressAPI visit = new PostAddressAPI();
        postAddressAPIs.add(visit);
        organizationAPI.setPostAddresses(postAddressAPIs);
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{organization.postAddresses.wrongSize}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testWrongValidFrom() {
        OrganizationAPI organizationAPI = createValidOrganization();
        organizationAPI.setValidFrom(-1l);
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{organization.validFrom.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testWrongValidToMinusValue() {
        OrganizationAPI organizationAPI = createValidOrganization();
        organizationAPI.setValidTo(-1l);
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{organization.validTo.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testValidToPlusValueButNotFuture() {
        OrganizationAPI organizationAPI = createValidOrganization();
        organizationAPI.setValidTo(100l);
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    @Test
    public void testTooLongPostCode() {
        OrganizationAPI organizationAPI = createValidOrganization();
        List<PostAddressAPI> postAddressAPIs = new ArrayList<>();
        PostAddressAPI visit = new PostAddressAPI();
        visit.setPostCode("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
        postAddressAPIs.add(visit);
        PostAddressAPI delivery = new PostAddressAPI();
        postAddressAPIs.add(delivery);
        organizationAPI.setPostAddresses(postAddressAPIs);
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{postAddress.postCode.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testTooLongStreetAddress() {
        OrganizationAPI organizationAPI = createValidOrganization();
        List<PostAddressAPI> postAddressAPIs = new ArrayList<>();
        PostAddressAPI visit = new PostAddressAPI();
        visit.setStreetAddress("abcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyz");
        postAddressAPIs.add(visit);
        PostAddressAPI delivery = new PostAddressAPI();
        postAddressAPIs.add(delivery);
        organizationAPI.setPostAddresses(postAddressAPIs);
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{postAddress.streetAddress.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testTooLongCity() {
        OrganizationAPI organizationAPI = createValidOrganization();
        List<PostAddressAPI> postAddressAPIs = new ArrayList<>();
        PostAddressAPI visit = new PostAddressAPI();
        visit.setCity("abcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyz");
        postAddressAPIs.add(visit);
        PostAddressAPI delivery = new PostAddressAPI();
        postAddressAPIs.add(delivery);
        organizationAPI.setPostAddresses(postAddressAPIs);
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{postAddress.city.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testInvalidEmail() {
        OrganizationAPI organizationAPI = createValidOrganization();
        organizationAPI.getElectronicAddress().setEmail("test");
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.email.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testInvalidWeb() {
        OrganizationAPI organizationAPI = createValidOrganization();
        organizationAPI.getElectronicAddress().setWeb("test");
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.web.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testTooLongEmail() {
        OrganizationAPI organizationAPI = createValidOrganization();
        organizationAPI.getElectronicAddress().setEmail("abcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrst@uvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyz.com");
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.email.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testTooLongWeb() {
        OrganizationAPI organizationAPI = createValidOrganization();
        organizationAPI.getElectronicAddress().setWeb("http://abcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyz.com");
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.web.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testTooLongMobile() {
        OrganizationAPI organizationAPI = createValidOrganization();
        organizationAPI.getElectronicAddress().setMobile("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.mobile.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testTooLongTelephone() {
        OrganizationAPI organizationAPI = createValidOrganization();
        organizationAPI.getElectronicAddress().setTelephone("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.telephone.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testTooLongFax() {
        OrganizationAPI organizationAPI = createValidOrganization();
        organizationAPI.getElectronicAddress().setFax("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.fax.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testTooShortCountry() {
        OrganizationAPI organizationAPI = createValidOrganization();
        organizationAPI.getCountry().setCode("s");
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{organization.country.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testTooLongCountry() {
        OrganizationAPI organizationAPI = createValidOrganization();
        organizationAPI.getCountry().setCode("see");
        Set<ConstraintViolation<OrganizationAPI>> constraintViolations = validator.validate(organizationAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{organization.country.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    private OrganizationAPI createValidOrganization() {
        OrganizationAPI organizationAPI = new OrganizationAPI();
        organizationAPI.setOrganizationName("test");
        CVCountryAPI countryAPI = new CVCountryAPI();
        countryAPI.setCode("se");
        countryAPI.setName("Sverige");
        organizationAPI.setCountry(countryAPI);
        organizationAPI.setOrganisationType("CUSTOMER");
        organizationAPI.setGln("1000000000009");
        List<PostAddressAPI> postAddressAPIs = new ArrayList<>();
        PostAddressAPI visit = new PostAddressAPI();
        postAddressAPIs.add(visit);
        PostAddressAPI delivery = new PostAddressAPI();
        postAddressAPIs.add(delivery);
        organizationAPI.setPostAddresses(postAddressAPIs);
        organizationAPI.setOrganizationNumber("12345");
        organizationAPI.setElectronicAddress(new ElectronicAddressAPI());
        organizationAPI.setValidFrom(System.currentTimeMillis());
        return organizationAPI;
    }

}
