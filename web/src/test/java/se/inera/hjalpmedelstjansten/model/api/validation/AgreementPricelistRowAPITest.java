package se.inera.hjalpmedelstjansten.model.api.validation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowControllerTest;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelistRow;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Set;


public class AgreementPricelistRowAPITest {

    private Validator beanValidator;

    static final int pricelistRowLeastOrderQuantity = 1;
    static final String pricelistRowStatus = AgreementPricelistRow.Status.ACTIVE.toString();
    static final BigDecimal pricelistRowPrice = BigDecimal.TEN;
    static final long pricelistRowId = 1;
    static final long articleId = 2;

    @Before
    public void init() {
        beanValidator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    public void testAllEmpty() {
        AgreementPricelistRowAPI pricelistRowAPI = new AgreementPricelistRowAPI();
        Set<ConstraintViolation<AgreementPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistRowAPI, Default.class);
        Assert.assertEquals(3, constraintViolations.size());
        boolean statusFound = false;
        boolean leastOrderQuantityFound = false;
        boolean articleFound = false;
        Iterator<ConstraintViolation<AgreementPricelistRowAPI>> it = constraintViolations.iterator();
        while( it.hasNext() ) {
            ConstraintViolation<AgreementPricelistRowAPI> constraintViolation = it.next();
            if( "{pricelistrow.status.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                statusFound = true;
            } else if( "{pricelistrow.leastOrderQuantity.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                leastOrderQuantityFound = true;
            } else if( "{pricelistrow.article.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                articleFound = true;
            }
        }
        Assert.assertTrue(statusFound);
        Assert.assertTrue(leastOrderQuantityFound);
        Assert.assertTrue(articleFound);
    }

    @Test
    public void testAllRight() {
        AgreementPricelistRowAPI pricelistAPI = AgreementPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowStatus, pricelistRowPrice, articleId);
        Set<ConstraintViolation<AgreementPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    @Test
    public void testMinusPriceValid() {
        AgreementPricelistRowAPI pricelistAPI = AgreementPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowStatus, pricelistRowPrice, articleId);
        pricelistAPI.setPrice(new BigDecimal(-10));
        Set<ConstraintViolation<AgreementPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    @Test
    public void testNullStatus() {
        AgreementPricelistRowAPI pricelistAPI = AgreementPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowStatus, pricelistRowPrice, articleId);
        pricelistAPI.setStatus(null);
        Set<ConstraintViolation<AgreementPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelistrow.status.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testInvalidStatus() {
        AgreementPricelistRowAPI pricelistAPI = AgreementPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowStatus, pricelistRowPrice, articleId);
        pricelistAPI.setStatus("WRONG");
        Set<ConstraintViolation<AgreementPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelistrow.status.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testInvalidValidFrom() {
        AgreementPricelistRowAPI pricelistAPI = AgreementPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowStatus, pricelistRowPrice, articleId);
        pricelistAPI.setValidFrom(-1l);
        Set<ConstraintViolation<AgreementPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelistrow.validFrom.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testNullLeastOrderQuantity() {
        AgreementPricelistRowAPI pricelistAPI = AgreementPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowStatus, pricelistRowPrice, articleId);
        pricelistAPI.setLeastOrderQuantity(null);
        Set<ConstraintViolation<AgreementPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelistrow.leastOrderQuantity.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testInvalidLeastOrderQuantity() {
        AgreementPricelistRowAPI pricelistAPI = AgreementPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowStatus, pricelistRowPrice, articleId);
        pricelistAPI.setLeastOrderQuantity(-1);
        Set<ConstraintViolation<AgreementPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelistrow.leastOrderQuantity.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testInvalidDeliveryTime() {
        AgreementPricelistRowAPI pricelistAPI = AgreementPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowStatus, pricelistRowPrice, articleId);
        pricelistAPI.setDeliveryTime(-1);
        Set<ConstraintViolation<AgreementPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelistrow.deliveryTime.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testInvalidWarrantyQuantity() {
        AgreementPricelistRowAPI pricelistAPI = AgreementPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowStatus, pricelistRowPrice, articleId);
        pricelistAPI.setWarrantyQuantity(-1);
        Set<ConstraintViolation<AgreementPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelistrow.warrantyQuantity.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testInvalidWarrantyTermsTooLong() {
        AgreementPricelistRowAPI pricelistAPI = AgreementPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowStatus, pricelistRowPrice, articleId);
        pricelistAPI.setWarrantyTerms("abcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyz");
        Set<ConstraintViolation<AgreementPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelistrow.warrantyTerms.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testNullArticle() {
        AgreementPricelistRowAPI pricelistAPI = AgreementPricelistRowControllerTest.createValidPricelistRowAPI(pricelistRowId, pricelistRowLeastOrderQuantity, pricelistRowStatus, pricelistRowPrice, articleId);
        pricelistAPI.setArticle(null);
        Set<ConstraintViolation<AgreementPricelistRowAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelistrow.article.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }

}
