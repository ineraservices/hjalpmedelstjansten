package se.inera.hjalpmedelstjansten.model.api.validation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistControllerTest;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistAPI;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.Iterator;
import java.util.Set;


public class GeneralPricelistAPITest {

    private Validator beanValidator;

    @Before
    public void init() {
        beanValidator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    public void testAllEmpty() {
        GeneralPricelistAPI generalPricelistAPI = new GeneralPricelistAPI();
        Set<ConstraintViolation<GeneralPricelistAPI>> constraintViolations = beanValidator.validate(generalPricelistAPI, Default.class);
        Assert.assertEquals(8, constraintViolations.size());
        boolean numberFound = false;
        boolean validFromFound = false;
        boolean validToFound = false;
        boolean deliveryTimeHFound = false;
        boolean deliveryTimeTFound = false;
        boolean deliveryTimeIFound = false;
        boolean deliveryTimeRFound = false;
        boolean deliveryTimeTJFound = false;
        Iterator<ConstraintViolation<GeneralPricelistAPI>> it = constraintViolations.iterator();
        while( it.hasNext() ) {
            ConstraintViolation<GeneralPricelistAPI> constraintViolation = it.next();
            if( "{generalPricelist.number.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                numberFound = true;
            } else if( "{generalPricelist.validFrom.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                validFromFound = true;
            } else if( "{generalPricelist.validTo.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                validToFound = true;
            } else if( "{generalPricelist.deliveryTimeH.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                deliveryTimeHFound = true;
            } else if( "{generalPricelist.deliveryTimeT.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                deliveryTimeTFound = true;
            } else if( "{generalPricelist.deliveryTimeI.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                deliveryTimeIFound = true;
            } else if( "{generalPricelist.deliveryTimeR.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                deliveryTimeRFound = true;
            } else if( "{generalPricelist.deliveryTimeTJ.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                deliveryTimeTJFound = true;
            }
        }
        Assert.assertTrue(numberFound);
        Assert.assertTrue(validFromFound);
        Assert.assertTrue(validToFound);
        Assert.assertTrue(deliveryTimeHFound);
        Assert.assertTrue(deliveryTimeTFound);
        Assert.assertTrue(deliveryTimeIFound);
        Assert.assertTrue(deliveryTimeRFound);
        Assert.assertTrue(deliveryTimeTJFound);
    }

    @Test
    public void testAllRight() {
        GeneralPricelistAPI generalPricelistAPI = GeneralPricelistControllerTest.createValidGeneralPricelistAPI();
        Set<ConstraintViolation<GeneralPricelistAPI>> constraintViolations = beanValidator.validate(generalPricelistAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    @Test
    public void beanTestLongAgreementName() {
        GeneralPricelistAPI generalPricelistAPI = GeneralPricelistControllerTest.createValidGeneralPricelistAPI();
        generalPricelistAPI.setGeneralPricelistName("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdef");
        Set<ConstraintViolation<GeneralPricelistAPI>> constraintViolations = beanValidator.validate(generalPricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{generalPricelist.name.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestNoAgreementNumber() {
        GeneralPricelistAPI generalPricelistAPI = GeneralPricelistControllerTest.createValidGeneralPricelistAPI();
        generalPricelistAPI.setGeneralPricelistNumber(null);
        Set<ConstraintViolation<GeneralPricelistAPI>> constraintViolations = beanValidator.validate(generalPricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{generalPricelist.number.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestLongAgreementNumber() {
        GeneralPricelistAPI generalPricelistAPI = GeneralPricelistControllerTest.createValidGeneralPricelistAPI();
        generalPricelistAPI.setGeneralPricelistNumber("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdef");
        Set<ConstraintViolation<GeneralPricelistAPI>> constraintViolations = beanValidator.validate(generalPricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{generalPricelist.number.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestNoValidFrom() {
        GeneralPricelistAPI generalPricelistAPI = GeneralPricelistControllerTest.createValidGeneralPricelistAPI();
        generalPricelistAPI.setValidFrom(null);
        Set<ConstraintViolation<GeneralPricelistAPI>> constraintViolations = beanValidator.validate(generalPricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{generalPricelist.validFrom.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestInvalidValidFrom() {
        GeneralPricelistAPI generalPricelistAPI = GeneralPricelistControllerTest.createValidGeneralPricelistAPI();
        generalPricelistAPI.setValidFrom(-10l);
        Set<ConstraintViolation<GeneralPricelistAPI>> constraintViolations = beanValidator.validate(generalPricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{generalPricelist.validFrom.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestNoValidTo() {
        GeneralPricelistAPI generalPricelistAPI = GeneralPricelistControllerTest.createValidGeneralPricelistAPI();
        generalPricelistAPI.setValidTo(null);
        Set<ConstraintViolation<GeneralPricelistAPI>> constraintViolations = beanValidator.validate(generalPricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{generalPricelist.validTo.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestInvalidValidTo() {
        GeneralPricelistAPI generalPricelistAPI = GeneralPricelistControllerTest.createValidGeneralPricelistAPI();
        generalPricelistAPI.setValidTo(-10l);
        Set<ConstraintViolation<GeneralPricelistAPI>> constraintViolations = beanValidator.validate(generalPricelistAPI, Default.class);
        Assert.assertEquals(2, constraintViolations.size());
        boolean validToInvalidFound = false;
        boolean validToBeforeValidFromFound = false;
        for( Iterator<ConstraintViolation<GeneralPricelistAPI>> it = constraintViolations.iterator(); it.hasNext(); ) {
            ConstraintViolation<GeneralPricelistAPI> constraintViolation = it.next();
            if( "{generalPricelist.validTo.invalid}".equals(constraintViolation.getMessageTemplate()) ) {
                validToInvalidFound = true;
            } else if( "{generalPricelist.validToBeforeValidFrom}".equals(constraintViolation.getMessageTemplate()) ) {
                validToBeforeValidFromFound = true;
            }
        }
        Assert.assertTrue(validToInvalidFound);
        Assert.assertTrue(validToBeforeValidFromFound);
    }

}
