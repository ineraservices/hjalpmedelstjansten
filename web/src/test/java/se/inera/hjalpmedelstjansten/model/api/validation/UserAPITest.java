package se.inera.hjalpmedelstjansten.model.api.validation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.RoleAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.UserEngagementAPI;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class UserAPITest {

    private Validator validator;
    static final long organizationId = 0;
    static final long userId = 1;

    @Before
    public void init() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    public void testAllEmpty() {
        UserAPI userAPI = new UserAPI();
        Set<ConstraintViolation<UserAPI>> constraintViolations = validator.validate(userAPI, Default.class);
        Assert.assertEquals(5, constraintViolations.size());
        boolean firstNameFound = false;
        boolean lastNameFound = false;
        boolean userNameFound = false;
        boolean electronicAddressFound = false;
        boolean engagementFound = false;
        Iterator<ConstraintViolation<UserAPI>> it = constraintViolations.iterator();
        while( it.hasNext() ) {
            ConstraintViolation<UserAPI> constraintViolation = it.next();
            if( "{user.firstname.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                firstNameFound = true;
            } else if( "{user.lastname.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                lastNameFound = true;
            } else if( "{user.username.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                userNameFound = true;
            } else if( "{user.electronicAddress.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                electronicAddressFound = true;
            } else if( "{user.engagements.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                engagementFound = true;
            }
        }
        Assert.assertTrue(firstNameFound);
        Assert.assertTrue(lastNameFound);
        Assert.assertTrue(userNameFound);
        Assert.assertTrue(electronicAddressFound);
        Assert.assertTrue(engagementFound);
    }

    @Test
    public void testAllRight() {
        UserAPI userAPI = createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Baseuser, null);
        Set<ConstraintViolation<UserAPI>> constraintViolations = validator.validate(userAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    @Test
    public void testInvalidEmail() {
        UserAPI userAPI = createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Baseuser, null);
        userAPI.getElectronicAddress().setEmail("test");
        Set<ConstraintViolation<UserAPI>> constraintViolations = validator.validate(userAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.email.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testEmptyEmail() {
        UserAPI userAPI = createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Baseuser, null);
        userAPI.getElectronicAddress().setEmail("");
        Set<ConstraintViolation<UserAPI>> constraintViolations = validator.validate(userAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    @Test
    public void testInvalidValidFrom() {
        UserAPI userAPI = createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Baseuser, null);
        UserEngagementAPI userEngagementAPI = userAPI.getUserEngagements().get(0);
        userEngagementAPI.setOrganizationId(0l);
        userEngagementAPI.setValidFrom(-1l);
        Set<ConstraintViolation<UserAPI>> constraintViolations = validator.validate(userAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{userEngagement.validFrom.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testInvalidValidToMinusValue() {
        UserAPI userAPI = createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Baseuser, null);
        UserEngagementAPI userEngagementAPI = userAPI.getUserEngagements().get(0);
        userEngagementAPI.setOrganizationId(0l);
        userEngagementAPI.setValidFrom(System.currentTimeMillis());
        userEngagementAPI.setValidTo(-1l);
        Set<ConstraintViolation<UserAPI>> constraintViolations = validator.validate(userAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{userEngagement.validTo.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testValidToPlusValueButNotFuture() {
        UserAPI userAPI = createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Baseuser, null);
        UserEngagementAPI userEngagementAPI = userAPI.getUserEngagements().get(0);
        userEngagementAPI.setOrganizationId(0l);
        userEngagementAPI.setValidFrom(System.currentTimeMillis());
        userEngagementAPI.setValidTo(100l);
        Set<ConstraintViolation<UserAPI>> constraintViolations = validator.validate(userAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    public static UserAPI createValidUserAPI(long userId, long organizationId, Long businessLevelId, UserRole.RoleName roleName, Long userEngagementApiId) {
        UserAPI userAPI = new UserAPI();
        userAPI.setId(userId);
        userAPI.setFirstName("test");
        userAPI.setLastName("test");
        userAPI.setUsername("test");
        userAPI.setElectronicAddress(new ElectronicAddressAPI());
        List<RoleAPI> roleAPIs = new ArrayList<>();
        RoleAPI roleAPI = new RoleAPI();
        roleAPI.setName(roleName.toString());
        roleAPI.setName("description");
        roleAPIs.add(roleAPI);
        List<UserEngagementAPI> userEngagementAPIs = new ArrayList<>();
        UserEngagementAPI userEngagementAPI = new UserEngagementAPI();
        userEngagementAPI.setId(userEngagementApiId);
        userEngagementAPI.setOrganizationId(organizationId);
        userEngagementAPI.setValidFrom(System.currentTimeMillis());
        userEngagementAPI.setRoles(roleAPIs);
        if( businessLevelId != null ) {
            List<BusinessLevelAPI> businessLevelAPIs = new ArrayList<>();
            BusinessLevelAPI businessLevelAPI = new BusinessLevelAPI();
            businessLevelAPI.setId(businessLevelId);
            businessLevelAPIs.add(businessLevelAPI);
            userEngagementAPI.setBusinessLevels(businessLevelAPIs);
        }
        userEngagementAPIs.add(userEngagementAPI);
        userAPI.setUserEngagementAPIs(userEngagementAPIs);
        return userAPI;
    }

}
