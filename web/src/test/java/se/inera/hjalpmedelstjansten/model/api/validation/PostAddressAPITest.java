package se.inera.hjalpmedelstjansten.model.api.validation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.model.api.PostAddressAPI;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.Set;


public class PostAddressAPITest {

    private Validator validator;

    @Before
    public void init() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    public void testAllEmpty() {
        PostAddressAPI postAddressAPI = new PostAddressAPI();
        Set<ConstraintViolation<PostAddressAPI>> constraintViolations = validator.validate(postAddressAPI, Default.class);
        Assert.assertTrue(constraintViolations.isEmpty());
    }

    // TEST POSTCODE


    @Test
    public void testValidPostCode() {
        PostAddressAPI postAddressAPI = new PostAddressAPI();
        postAddressAPI.setPostCode("11111");
        Set<ConstraintViolation<PostAddressAPI>> constraintViolations = validator.validate(postAddressAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

}
