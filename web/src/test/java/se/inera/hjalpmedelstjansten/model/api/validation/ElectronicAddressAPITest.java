package se.inera.hjalpmedelstjansten.model.api.validation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.Iterator;
import java.util.Set;


public class ElectronicAddressAPITest {

    private Validator validator;

    @Before
    public void init() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    public void testAllEmpty() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertTrue(constraintViolations.isEmpty());
    }

    // TEST EMAIL

    @Test
    public void testInvalidEmail1() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setEmail("a");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.email.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testInvalidEmail2() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setEmail("a@");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.email.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testValidEmail() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setEmail("a@b.c");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    @Test
    public void testOddButValidEmail() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setEmail("a@b");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    // TEST WEB
    @Test
    public void testInvalidWeb1() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setWeb("a");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.web.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testInvalidWeb2() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setWeb("http:/test");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.web.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testInvalidWeb3() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setWeb("test://test");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.web.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void testValidWeb1() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setWeb("http://test");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    @Test
    public void testValidWeb2() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setWeb("https://test");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    // EMAIL AND WEB
    @Test
    public void testInvalidEmailAndInvalidWeb() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setEmail("a");
        electronicAddressAPI.setWeb("test://test");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(2, constraintViolations.size());
        boolean emailValidationFound = false;
        boolean webValidationFound = false;
        Iterator<ConstraintViolation<ElectronicAddressAPI>> it = constraintViolations.iterator();
        while( it.hasNext() ) {
            ConstraintViolation<ElectronicAddressAPI> constraintViolation = it.next();
            if( "{electronicAddress.email.invalid}".equals(constraintViolation.getMessageTemplate()) ) {
                emailValidationFound = true;
            } else if( "{electronicAddress.web.invalid}".equals(constraintViolation.getMessageTemplate()) ) {
                webValidationFound = true;
            }
        }
        Assert.assertTrue(emailValidationFound);
        Assert.assertTrue(webValidationFound);
    }

}
