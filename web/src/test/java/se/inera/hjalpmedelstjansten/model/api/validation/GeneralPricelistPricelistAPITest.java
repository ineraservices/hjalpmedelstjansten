package se.inera.hjalpmedelstjansten.model.api.validation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistControllerTest;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistAPI;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Set;


public class GeneralPricelistPricelistAPITest {

    private Validator beanValidator;

    static final long pricelistId = 1;
    static final String pricelistNumber = "001";

    @Before
    public void init() {
        beanValidator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    public void testAllEmpty() {
        GeneralPricelistPricelistAPI pricelistAPI = new GeneralPricelistPricelistAPI();
        Set<ConstraintViolation<GeneralPricelistPricelistAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(2, constraintViolations.size());
        boolean numberFound = false;
        boolean validFromFound = false;
        Iterator<ConstraintViolation<GeneralPricelistPricelistAPI>> it = constraintViolations.iterator();
        while( it.hasNext() ) {
            ConstraintViolation<GeneralPricelistPricelistAPI> constraintViolation = it.next();
            if( "{pricelist.number.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                numberFound = true;
            } else if( "{pricelist.validFrom.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                validFromFound = true;
            }
        }
        Assert.assertTrue(numberFound);
        Assert.assertTrue(validFromFound);
    }

    @Test
    public void testAllRight() {
        GeneralPricelistPricelistAPI pricelistAPI = GeneralPricelistPricelistControllerTest.createValidPricelistAPI(pricelistId, pricelistNumber, getValidFrom());
        Set<ConstraintViolation<GeneralPricelistPricelistAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    @Test
    public void beanTestNoAgreementNumber() {
        GeneralPricelistPricelistAPI pricelistAPI = GeneralPricelistPricelistControllerTest.createValidPricelistAPI(pricelistId, pricelistNumber, getValidFrom());
        pricelistAPI.setNumber(null);
        Set<ConstraintViolation<GeneralPricelistPricelistAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelist.number.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestLongAgreementNumber() {
        GeneralPricelistPricelistAPI pricelistAPI = GeneralPricelistPricelistControllerTest.createValidPricelistAPI(pricelistId, pricelistNumber, getValidFrom());
        pricelistAPI.setNumber("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdef");
        Set<ConstraintViolation<GeneralPricelistPricelistAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelist.number.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestNoValidFrom() {
        GeneralPricelistPricelistAPI pricelistAPI = GeneralPricelistPricelistControllerTest.createValidPricelistAPI(pricelistId, pricelistNumber, getValidFrom());
        pricelistAPI.setValidFrom(null);
        Set<ConstraintViolation<GeneralPricelistPricelistAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelist.validFrom.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestInvalidValidFrom() {
        GeneralPricelistPricelistAPI pricelistAPI = GeneralPricelistPricelistControllerTest.createValidPricelistAPI(pricelistId, pricelistNumber, getValidFrom());
        pricelistAPI.setValidFrom(-10l);
        Set<ConstraintViolation<GeneralPricelistPricelistAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelist.validFrom.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestInvalidValidFromNotFuture() {
        GeneralPricelistPricelistAPI pricelistAPI = GeneralPricelistPricelistControllerTest.createValidPricelistAPI(pricelistId, pricelistNumber, getValidFrom());
        Calendar validFrom = Calendar.getInstance();
        validFrom.add(Calendar.DAY_OF_YEAR, -1);
        pricelistAPI.setValidFrom(validFrom.getTimeInMillis());
        Set<ConstraintViolation<GeneralPricelistPricelistAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelist.validFrom.notFuture}", constraintViolations.iterator().next().getMessageTemplate());
    }

    private static long getValidFrom() {
        Calendar validFrom = Calendar.getInstance();
        return validFrom.getTimeInMillis();
    }

}
