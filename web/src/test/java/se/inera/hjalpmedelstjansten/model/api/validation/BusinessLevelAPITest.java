package se.inera.hjalpmedelstjansten.model.api.validation;

import org.junit.Before;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;

import jakarta.validation.Validation;
import jakarta.validation.Validator;


public class BusinessLevelAPITest {

    private Validator validator;

    @Before
    public void init() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    public static BusinessLevelAPI createValidBusinessLevelAPI(long id, BusinessLevel.Status status) {
        BusinessLevelAPI businessLevelAPI = new BusinessLevelAPI();
        businessLevelAPI.setId(id);
        businessLevelAPI.setStatus(status.toString());
        return businessLevelAPI;
    }

}
