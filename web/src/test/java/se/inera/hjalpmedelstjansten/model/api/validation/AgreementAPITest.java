package se.inera.hjalpmedelstjansten.model.api.validation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementControllerTest;
import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;


public class AgreementAPITest {

    private Validator beanValidator;

    @Before
    public void init() {
        beanValidator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    public void testAllEmpty() {
        AgreementAPI agreementAPI = new AgreementAPI();
        Set<ConstraintViolation<AgreementAPI>> constraintViolations = beanValidator.validate(agreementAPI, Default.class);
        Assert.assertEquals(10, constraintViolations.size());
        boolean numberFound = false;
        boolean validFromFound = false;
        boolean validToFound = false;
        boolean supplierFound = false;
        boolean pricelistApproversFound = false;
        boolean deliveryTimeHFound = false;
        boolean deliveryTimeTFound = false;
        boolean deliveryTimeIFound = false;
        boolean deliveryTimeRFound = false;
        boolean deliveryTimeTJFound = false;
        Iterator<ConstraintViolation<AgreementAPI>> it = constraintViolations.iterator();
        while( it.hasNext() ) {
            ConstraintViolation<AgreementAPI> constraintViolation = it.next();
            if( "{agreement.number.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                numberFound = true;
            } else if( "{agreement.validFrom.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                validFromFound = true;
            } else if( "{agreement.validTo.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                validToFound = true;
            } else if( "{agreement.supplierOrganization.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                supplierFound = true;
            } else if( "{agreement.customerPricelistApprovers.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                pricelistApproversFound = true;
            } else if( "{agreement.deliveryTimeH.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                deliveryTimeHFound = true;
            } else if( "{agreement.deliveryTimeT.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                deliveryTimeTFound = true;
            } else if( "{agreement.deliveryTimeI.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                deliveryTimeIFound = true;
            } else if( "{agreement.deliveryTimeR.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                deliveryTimeRFound = true;
            } else if( "{agreement.deliveryTimeTJ.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                deliveryTimeTJFound = true;
            }
        }
        Assert.assertTrue(numberFound);
        Assert.assertTrue(validFromFound);
        Assert.assertTrue(validToFound);
        Assert.assertTrue(supplierFound);
        Assert.assertTrue(pricelistApproversFound);
        Assert.assertTrue(deliveryTimeHFound);
        Assert.assertTrue(deliveryTimeTFound);
        Assert.assertTrue(deliveryTimeIFound);
        Assert.assertTrue(deliveryTimeRFound);
        Assert.assertTrue(deliveryTimeTJFound);
    }

    @Test
    public void testAllRight() {
        AgreementAPI agreementAPI = AgreementControllerTest.createValidAgreementAPI();
        Set<ConstraintViolation<AgreementAPI>> constraintViolations = beanValidator.validate(agreementAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    @Test
    public void beanTestLongAgreementName() {
        AgreementAPI agreementAPI = AgreementControllerTest.createValidAgreementAPI();
        agreementAPI.setAgreementName("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdef");
        Set<ConstraintViolation<AgreementAPI>> constraintViolations = beanValidator.validate(agreementAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{agreement.name.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestNoAgreementNumber() {
        AgreementAPI agreementAPI = AgreementControllerTest.createValidAgreementAPI();
        agreementAPI.setAgreementNumber(null);
        Set<ConstraintViolation<AgreementAPI>> constraintViolations = beanValidator.validate(agreementAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{agreement.number.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestLongAgreementNumber() {
        AgreementAPI agreementAPI = AgreementControllerTest.createValidAgreementAPI();
        agreementAPI.setAgreementNumber("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdef");
        Set<ConstraintViolation<AgreementAPI>> constraintViolations = beanValidator.validate(agreementAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{agreement.number.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestEmptyCustomerPricelistApprovers() {
        AgreementAPI agreementAPI = AgreementControllerTest.createValidAgreementAPI();
        agreementAPI.setCustomerPricelistApprovers(new ArrayList<>());
        Set<ConstraintViolation<AgreementAPI>> constraintViolations = beanValidator.validate(agreementAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{agreement.customerPricelistApprovers.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestNoValidFrom() {
        AgreementAPI agreementAPI = AgreementControllerTest.createValidAgreementAPI();
        agreementAPI.setValidFrom(null);
        Set<ConstraintViolation<AgreementAPI>> constraintViolations = beanValidator.validate(agreementAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{agreement.validFrom.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestInvalidValidFrom() {
        AgreementAPI agreementAPI = AgreementControllerTest.createValidAgreementAPI();
        agreementAPI.setValidFrom(-10l);
        Set<ConstraintViolation<AgreementAPI>> constraintViolations = beanValidator.validate(agreementAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{agreement.validFrom.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestNoValidTo() {
        AgreementAPI agreementAPI = AgreementControllerTest.createValidAgreementAPI();
        agreementAPI.setValidTo(null);
        Set<ConstraintViolation<AgreementAPI>> constraintViolations = beanValidator.validate(agreementAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{agreement.validTo.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestInvalidValidTo() {
        AgreementAPI agreementAPI = AgreementControllerTest.createValidAgreementAPI();
        agreementAPI.setValidTo(-10l);
        Set<ConstraintViolation<AgreementAPI>> constraintViolations = beanValidator.validate(agreementAPI, Default.class);
        Assert.assertEquals(2, constraintViolations.size());
        boolean validToInvalidFound = false;
        boolean validToBeforeValidFromFound = false;
        for( Iterator<ConstraintViolation<AgreementAPI>> it = constraintViolations.iterator(); it.hasNext(); ) {
            ConstraintViolation<AgreementAPI> constraintViolation = it.next();
            if( "{agreement.validTo.invalid}".equals(constraintViolation.getMessageTemplate()) ) {
                validToInvalidFound = true;
            } else if( "{agreement.validToBeforeValidFrom}".equals(constraintViolation.getMessageTemplate()) ) {
                validToBeforeValidFromFound = true;
            }
        }
        Assert.assertTrue(validToInvalidFound);
        Assert.assertTrue(validToBeforeValidFromFound);
    }

    @Test
    public void beanTestInvalidExtensionOptionTo() {
        AgreementAPI agreementAPI = AgreementControllerTest.createValidAgreementAPI();
        agreementAPI.setExtensionOptionTo(-10l);
        Set<ConstraintViolation<AgreementAPI>> constraintViolations = beanValidator.validate(agreementAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{agreement.extensionOptionTo.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestInvalidSendReminderOn() {
        AgreementAPI agreementAPI = AgreementControllerTest.createValidAgreementAPI();
        agreementAPI.setSendReminderOn(-10l);
        Set<ConstraintViolation<AgreementAPI>> constraintViolations = beanValidator.validate(agreementAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{agreement.sendReminderOn.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestInvalidEndDate() {
        AgreementAPI agreementAPI = AgreementControllerTest.createValidAgreementAPI();
        agreementAPI.setEndDate(-10l);
        Set<ConstraintViolation<AgreementAPI>> constraintViolations = beanValidator.validate(agreementAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{agreement.endDate.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestNoSupplierOrganization() {
        AgreementAPI agreementAPI = AgreementControllerTest.createValidAgreementAPI();
        agreementAPI.setSupplierOrganization(null);
        Set<ConstraintViolation<AgreementAPI>> constraintViolations = beanValidator.validate(agreementAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{agreement.supplierOrganization.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }

}
