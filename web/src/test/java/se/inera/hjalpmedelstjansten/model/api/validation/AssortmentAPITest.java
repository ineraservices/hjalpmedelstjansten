package se.inera.hjalpmedelstjansten.model.api.validation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.business.assortment.controller.AssortmentControllerTest;
import se.inera.hjalpmedelstjansten.model.api.AssortmentAPI;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class AssortmentAPITest {

    private Validator beanValidator;
    static final long assortmentId = 1;
    static final String assortmentName = "assortmentName";

    @Before
    public void init() {
        beanValidator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    public void testAllEmpty() {
        AssortmentAPI assortmentAPI = new AssortmentAPI();
        Set<ConstraintViolation<AssortmentAPI>> constraintViolations = beanValidator.validate(assortmentAPI, Default.class);
        Assert.assertEquals(2, constraintViolations.size());
        boolean nameFound = false;
        boolean validFromFound = false;
        Iterator<ConstraintViolation<AssortmentAPI>> it = constraintViolations.iterator();
        while( it.hasNext() ) {
            ConstraintViolation<AssortmentAPI> constraintViolation = it.next();
            if( "{assortment.name.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                nameFound = true;
            } else if( "{assortment.validFrom.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                validFromFound = true;
            }
        }
        Assert.assertTrue(nameFound);
        Assert.assertTrue(validFromFound);
    }

    @Test
    public void testAllRight() {
        AssortmentAPI assortmentAPI = AssortmentControllerTest.createValidAssortmentAPI(assortmentId, assortmentName);
        Set<ConstraintViolation<AssortmentAPI>> constraintViolations = beanValidator.validate(assortmentAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    @Test
    public void beanTestLongName() {
        AssortmentAPI assortmentAPI = AssortmentControllerTest.createValidAssortmentAPI(assortmentId, assortmentName);
        assortmentAPI.setName("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdef");
        Set<ConstraintViolation<AssortmentAPI>> constraintViolations = beanValidator.validate(assortmentAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{assortment.name.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestNoValidFrom() {
        AssortmentAPI assortmentAPI = AssortmentControllerTest.createValidAssortmentAPI(assortmentId, assortmentName);
        assortmentAPI.setValidFrom(null);
        Set<ConstraintViolation<AssortmentAPI>> constraintViolations = beanValidator.validate(assortmentAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{assortment.validFrom.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestInvalidValidFrom() {
        AssortmentAPI assortmentAPI = AssortmentControllerTest.createValidAssortmentAPI(assortmentId, assortmentName);
        assortmentAPI.setValidFrom(-10l);
        Set<ConstraintViolation<AssortmentAPI>> constraintViolations = beanValidator.validate(assortmentAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{assortment.validFrom.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }

    @Test
    public void beanTestInvalidValidTo() {
        AssortmentAPI assortmentAPI = AssortmentControllerTest.createValidAssortmentAPI(assortmentId, assortmentName);
        assortmentAPI.setValidTo(-10l);
        Set<ConstraintViolation<AssortmentAPI>> constraintViolations = beanValidator.validate(assortmentAPI, Default.class);
        Assert.assertEquals(2, constraintViolations.size());
        Iterator<ConstraintViolation<AssortmentAPI>> constraintViolationIterator = constraintViolations.iterator();
        List<String> violationMessages = new ArrayList();
        while( constraintViolationIterator.hasNext() ) {
            ConstraintViolation<AssortmentAPI> constraintViolation = constraintViolationIterator.next();
            violationMessages.add(constraintViolation.getMessageTemplate());
        }
        Assert.assertTrue(violationMessages.contains("{assortment.validToBeforeValidFrom}"));
        Assert.assertTrue(violationMessages.contains("{assortment.validTo.invalid}"));
    }

}
