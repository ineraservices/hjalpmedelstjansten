package se.inera.hjalpmedelstjansten.business;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class HjalpmedelstjanstenValidationExceptionTest {

    @Test
    void shallReturnFalseIfNoValidationMessages() {
        final var error = new HjalpmedelstjanstenValidationException("error");
        assertFalse(error.hasValidationMessages());
    }

    @Test
    void shallReturnTrueIfAllValidationMessages() {
        final var error = new HjalpmedelstjanstenValidationException("error");
        error.addValidationMessage("message");
        assertTrue(error.hasValidationMessages());
    }

    @Test
    void shallReturnEmptyStringIfNoValidationMessages() {
        final var expectedMessage = "";
        final var error = new HjalpmedelstjanstenValidationException("error");
        assertEquals(expectedMessage, error.validationMessagesAsString());
    }

    @Test
    void shallReturnConcatStringWithDefaultDelimiterIfNoValidationMessages() {
        final var expectedMessage = "field1 - message1"
            +"\n"
            + "field2 - message2";
        final var error = new HjalpmedelstjanstenValidationException("error");
        error.addValidationMessage("field1", "message1");
        error.addValidationMessage("field2", "message2");
        assertEquals(expectedMessage, error.validationMessagesAsString());
    }

    @Test
    void shallReturnConcatStringWithCommaDelimiterIfNoValidationMessages() {
        final var expectedMessage = "field1 - message1"
            +","
            + "field2 - message2";
        final var error = new HjalpmedelstjanstenValidationException("error");
        error.addValidationMessage("field1", "message1");
        error.addValidationMessage("field2", "message2");
        assertEquals(expectedMessage, error.validationMessagesAsString(","));
    }
}
