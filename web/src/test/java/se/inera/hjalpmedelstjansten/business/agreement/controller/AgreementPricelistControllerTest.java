package se.inera.hjalpmedelstjansten.business.agreement.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.UserAPITest;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelist;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import jakarta.enterprise.event.Event;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class AgreementPricelistControllerTest {

    UserAPI userAPI = UserAPITest.createValidUserAPI(userId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
    AgreementPricelistController pricelistController;
    AgreementController agreementController;
    EntityManager myEntityManager;
    Event<InternalAuditEvent> event;

    static final long pricelistId = 1;
    static final long userId = 2;
    static final long customerOrganizationId = 3;
    static final long agreementId = 4;
    static final long supplierOrganizationId = 3;
    static final String pricelistNumber = "001";

    @Before
    public void init() {
        pricelistController = new AgreementPricelistController();
        pricelistController.LOG = new NoLogger();
        pricelistController.defaultPricelistNumber = pricelistNumber;

        event = Mockito.mock(Event.class);
        pricelistController.internalAuditEvent = event;

        myEntityManager = Mockito.mock(EntityManager.class);
        Mockito.when(myEntityManager.find(AgreementPricelist.class, pricelistId)).thenReturn(createValidPricelist(pricelistId, pricelistNumber, new Date(), new Date(), agreementId, customerOrganizationId));
        Query mockQuery = Mockito.mock(Query.class);
        Mockito.when(mockQuery.setParameter(Mockito.anyString(), Mockito.any())).thenReturn(mockQuery);
        List<AgreementPricelist> currentPricelists = new ArrayList<>();
        AgreementPricelist currentPricelist = createValidPricelist(pricelistId, pricelistNumber, new Date(), new Date(), agreementId, customerOrganizationId);
        currentPricelists.add(currentPricelist);
        Mockito.when(mockQuery.getResultList()).thenReturn(currentPricelists);
        Mockito.when(myEntityManager.createNamedQuery(AgreementPricelist.FIND_BY_PASSED_VALID_FROM_AND_AGREEMENT)).thenReturn(mockQuery);
        pricelistController.em = myEntityManager;

        agreementController = Mockito.mock(AgreementController.class);
        Mockito.when(agreementController.getAgreement(customerOrganizationId, agreementId, userAPI)).thenReturn(createValidAgreement(agreementId, customerOrganizationId, supplierOrganizationId, new Date(), new Date()));
        pricelistController.agreementController = agreementController;

        AgreementPricelistController mockedPricelistController = Mockito.mock(AgreementPricelistController.class);

        AgreementPricelistValidation pricelistValidation = new AgreementPricelistValidation();
        pricelistValidation.LOG = new NoLogger();
        pricelistValidation.pricelistController = mockedPricelistController;
        pricelistController.pricelistValidation = pricelistValidation;
    }

    @Test
    public void testGetPricelist() {
        AgreementPricelist pricelist = pricelistController.getPricelist(customerOrganizationId, agreementId, pricelistId, userAPI);
        Assert.assertNotNull(pricelist);
    }

    @Test
    public void testGetPricelistWrongAgreementUniqueId() {
        AgreementPricelist pricelist = pricelistController.getPricelist(customerOrganizationId, 1000000, pricelistId, userAPI);
        Assert.assertNull(pricelist);
    }

    @Test
    public void testGetPricelistWrongCustomerOrganizationUniqueId() {
        AgreementPricelist pricelist = pricelistController.getPricelist(100000, agreementId, pricelistId, userAPI);
        Assert.assertNull(pricelist);
    }

    @Test
    public void testGetPricelistWrongPricelistUniqueId() {
        AgreementPricelist pricelist = pricelistController.getPricelist(customerOrganizationId, agreementId, 100000, userAPI);
        Assert.assertNull(pricelist);
    }

    @Test
    public void testGetPricelistAPI() {
        AgreementPricelistAPI pricelistAPI = pricelistController.getPricelistAPI(customerOrganizationId, agreementId, pricelistId, userAPI, null, null);
        Assert.assertNotNull(pricelistAPI);
    }

    @Test
    public void testGetPricelistAPIWrongAgreementUniqueId() {
        AgreementPricelistAPI pricelistAPI = pricelistController.getPricelistAPI(customerOrganizationId, 1000000, pricelistId, userAPI, null, null);
        Assert.assertNull(pricelistAPI);
    }

    @Test
    public void testGetPricelistAPIWrongCustomerOrganizationUniqueId() {
        AgreementPricelistAPI pricelistAPI = pricelistController.getPricelistAPI(100000, agreementId, pricelistId, userAPI, null, null);
        Assert.assertNull(pricelistAPI);
    }

    @Test
    public void testGetPricelistAPIWrongPricelistUniqueId() {
        AgreementPricelistAPI pricelistAPI = pricelistController.getPricelistAPI(customerOrganizationId, agreementId, 100000, userAPI, null, null);
        Assert.assertNull(pricelistAPI);
    }

    @Test
    public void testCreateFirstPricelistForAgreement() {
        Calendar validFrom = Calendar.getInstance();
        validFrom.add(Calendar.DAY_OF_YEAR, 1);
        Calendar validTo = Calendar.getInstance();
        validTo.add(Calendar.YEAR, 1);
        Agreement agreement = createValidAgreement(agreementId, customerOrganizationId, supplierOrganizationId, validFrom.getTime(), validTo.getTime());
        AgreementPricelistAPI pricelistAPI = null;

        try {
            pricelistAPI = pricelistController.createFirstPricelistForAgreement(customerOrganizationId, agreement, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
        Assert.assertNotNull(pricelistAPI);
        Assert.assertEquals(pricelistNumber, pricelistAPI.getNumber());
    }

    @Test
    public void testCreatePricelist() {
        Calendar validFrom = Calendar.getInstance();
        validFrom.add(Calendar.DAY_OF_YEAR, 1);
        AgreementPricelistAPI pricelistAPI = createValidPricelistAPI(agreementId, pricelistNumber, validFrom.getTimeInMillis());
        try {
            pricelistAPI = pricelistController.createPricelist(customerOrganizationId, agreementId, pricelistAPI, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
        Assert.assertNotNull(pricelistAPI);
        Assert.assertEquals(pricelistNumber, pricelistAPI.getNumber());
    }

    /** Decision to not allow update of pricelists so functionality removed.
    @Test
    public void testUpdatePricelist() {
        Calendar validFrom = Calendar.getInstance();
        validFrom.add(Calendar.DAY_OF_YEAR, 1);
        String updatedPricelistNumber = "UP-1";
        PricelistAPI pricelistAPI = createValidPricelistAPI(agreementId, updatedPricelistNumber, validFrom.getTimeInMillis());
        try {
            pricelistAPI = pricelistController.updatePricelist(organizationId, agreementId, pricelistId ,pricelistAPI, userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
        Assert.assertNotNull(pricelistAPI);
        Assert.assertEquals(updatedPricelistNumber, pricelistAPI.getNumber());
    }
    */

    public static AgreementPricelistAPI createValidPricelistAPI(long id, String number, long validFrom) {
        AgreementPricelistAPI pricelistAPI = new AgreementPricelistAPI();
        pricelistAPI.setId(id);
        pricelistAPI.setNumber(number);
        pricelistAPI.setValidFrom(validFrom);
        return pricelistAPI;
    }

    public static AgreementPricelist createValidPricelist( long id, String number, Date validFrom, Date validTo, Long agreementId, Long organizationId ) {
        AgreementPricelist pricelist = new AgreementPricelist();
        pricelist.setUniqueId(id);
        pricelist.setNumber(number);
        pricelist.setValidFrom(validFrom);
        if( agreementId != null ) {
            pricelist.setAgreement(createValidAgreement(agreementId, customerOrganizationId, supplierOrganizationId, validFrom, validTo));
        }
        return pricelist;
    }

    public static Agreement createValidAgreement(long agreementId, Long customerOrganizationId, Long supplierOrganizationId, Date validFrom, Date validTo) {
        Agreement agreement = new Agreement();
        agreement.setUniqueId(agreementId);
        agreement.setValidFrom(validFrom);
        agreement.setValidTo(validTo);
        agreement.setStatus(Agreement.Status.CURRENT);
        if( customerOrganizationId != null ) {
            Organization organization = new Organization();
            organization.setUniqueId(customerOrganizationId);
            organization.setOrganizationType(Organization.OrganizationType.CUSTOMER);
            organization.setValidFrom(new Date());
            agreement.setCustomer(organization);
        }
        if( supplierOrganizationId != null ) {
            Organization organization = new Organization();
            organization.setUniqueId(supplierOrganizationId);
            organization.setOrganizationType(Organization.OrganizationType.SUPPLIER);
            organization.setValidFrom(new Date());
            agreement.setSupplier(organization);
        }
        return agreement;
    }

}
