package se.inera.hjalpmedelstjansten.business.product.controller.exportimport;


import static org.junit.Assert.assertEquals;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleControllerTest;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductControllerTest;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;

import java.util.ArrayList;
import java.util.List;



public class ExportControllerTest {

    ExportProductsArticlesController exportController;

    static final String article1ArticleNumber = "AN1";
    static final String article2ArticleNumber = "AN2";
    static final String product1ProductNumber = "PN1";
    static final String product2ProductNumber = "PN2";

    @Before
    public void init() {

        exportController = new ExportProductsArticlesController();
    }

    @Test
    public void testGetConnectionsAsStringOnlyArticles() {
        List<ArticleAPI> fitsToArticleAPIs = new ArrayList<>();

        ArticleAPI fitsToArticle1 = ArticleControllerTest.createValidArticleAPI();
        fitsToArticle1.setArticleNumber(article1ArticleNumber);
        fitsToArticleAPIs.add(fitsToArticle1);

        ArticleAPI fitsToArticle2 = ArticleControllerTest.createValidArticleAPI();
        fitsToArticle2.setArticleNumber(article2ArticleNumber);
        fitsToArticleAPIs.add(fitsToArticle2);

        ArticleAPI articleAPI = new ArticleAPI();
        articleAPI.setFitsToArticles(fitsToArticleAPIs);

        String connectionString = exportController.getConnectionsAsStringArticle(articleAPI);
        String expected = article1ArticleNumber + ";" + article2ArticleNumber;
        assertEquals(expected, connectionString);
    }

    @Test
    public void testGetConnectionsAsStringOnlyProducts() {
        List<ProductAPI> fitsToProductAPIs = new ArrayList<>();

        ProductAPI fitsToProduct1 = ProductControllerTest.createValidProductAPI();
        fitsToProduct1.setProductNumber(product1ProductNumber);
        fitsToProductAPIs.add(fitsToProduct1);

        ProductAPI fitsToProduct2 = ProductControllerTest.createValidProductAPI();
        fitsToProduct2.setProductNumber(product2ProductNumber);
        fitsToProductAPIs.add(fitsToProduct2);

        ArticleAPI articleAPI = new ArticleAPI();
        articleAPI.setFitsToProducts(fitsToProductAPIs);

        String connectionString = exportController.getConnectionsAsStringArticle(articleAPI);
        String expected = product1ProductNumber + ";" + product2ProductNumber;
        assertEquals(expected, connectionString);
    }

    @Test
    public void testGetConnectionsAsStringBothArticlesAndProducts() {
        List<ArticleAPI> fitsToArticleAPIs = new ArrayList<>();

        ArticleAPI fitsToArticle1 = ArticleControllerTest.createValidArticleAPI();
        fitsToArticle1.setArticleNumber(article1ArticleNumber);
        fitsToArticleAPIs.add(fitsToArticle1);

        ArticleAPI fitsToArticle2 = ArticleControllerTest.createValidArticleAPI();
        fitsToArticle2.setArticleNumber(article2ArticleNumber);
        fitsToArticleAPIs.add(fitsToArticle2);

        ArticleAPI articleAPI = new ArticleAPI();
        articleAPI.setFitsToArticles(fitsToArticleAPIs);

        List<ProductAPI> fitsToProductAPIs = new ArrayList<>();

        ProductAPI fitsToProduct1 = ProductControllerTest.createValidProductAPI();
        fitsToProduct1.setProductNumber(product1ProductNumber);
        fitsToProductAPIs.add(fitsToProduct1);

        ProductAPI fitsToProduct2 = ProductControllerTest.createValidProductAPI();
        fitsToProduct2.setProductNumber(product2ProductNumber);
        fitsToProductAPIs.add(fitsToProduct2);

        articleAPI.setFitsToProducts(fitsToProductAPIs);

        String connectionString = exportController.getConnectionsAsStringArticle(articleAPI);
        String expected = product1ProductNumber + ";" + product2ProductNumber + ";" + article1ArticleNumber + ";" + article2ArticleNumber;
        assertEquals(expected, connectionString);
    }

   /*  @Test
   public void testAllConnectionExport() {

        List<Long> productIds = new ArrayList<>();
        long number = 717;
        productIds.add(number);
        exportController.tryToFindAdditionalConnections(396, productIds);

    } */

}
