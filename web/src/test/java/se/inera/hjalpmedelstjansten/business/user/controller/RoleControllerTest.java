package se.inera.hjalpmedelstjansten.business.user.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import java.util.List;


public class RoleControllerTest {
    
    RoleController roleController;
    
    @Before
    public void init() {
        roleController = new RoleController();
    }
        
    @Test
    public void testGetRoleNamesSupplier() {
        List<UserRole.RoleName> userRoleNames = roleController.getRoleNames(Organization.OrganizationType.SUPPLIER);
        Assert.assertEquals(4, userRoleNames.size());
        Assert.assertTrue(userRoleNames.contains(UserRole.RoleName.SupplierAgreementManager));
        Assert.assertTrue(userRoleNames.contains(UserRole.RoleName.SupplierAgreementViewer));
        Assert.assertTrue(userRoleNames.contains(UserRole.RoleName.SupplierProductAndArticleHandler));
        Assert.assertTrue(userRoleNames.contains(UserRole.RoleName.Supplieradmin));
    }
     
    @Test
    public void testGetRoleNamesCustomer() {
        List<UserRole.RoleName> userRoleNames = roleController.getRoleNames(Organization.OrganizationType.CUSTOMER);
        Assert.assertEquals(5, userRoleNames.size());
        Assert.assertTrue(userRoleNames.contains(UserRole.RoleName.CustomerAgreementManager));
        Assert.assertTrue(userRoleNames.contains(UserRole.RoleName.CustomerAgreementViewer));
        Assert.assertTrue(userRoleNames.contains(UserRole.RoleName.Customeradmin));
        Assert.assertTrue(userRoleNames.contains(UserRole.RoleName.CustomerAssortmentManager));
        Assert.assertTrue(userRoleNames.contains(UserRole.RoleName.CustomerAssignedAssortmentManager));
    }
    
    @Test
    public void testGetRoleNamesServiceOwner() {
        List<UserRole.RoleName> userRoleNames = roleController.getRoleNames(Organization.OrganizationType.SERVICE_OWNER);
        Assert.assertEquals(1, userRoleNames.size());
        Assert.assertTrue(userRoleNames.contains(UserRole.RoleName.Superadmin));
    }
    
    @Test
    public void testValidOrganizationRoleSupplier() {
        Organization organization = new Organization();
        organization.setOrganizationType(Organization.OrganizationType.SUPPLIER);
        
        UserRole supplierAgreementManagerRole = new UserRole();
        supplierAgreementManagerRole.setName(UserRole.RoleName.SupplierAgreementManager);
        Assert.assertTrue(roleController.validOrganizationRole(supplierAgreementManagerRole, organization));
        
        UserRole supplierAgreementViewerRole = new UserRole();
        supplierAgreementViewerRole.setName(UserRole.RoleName.SupplierAgreementViewer);
        Assert.assertTrue(roleController.validOrganizationRole(supplierAgreementViewerRole, organization));
        
        UserRole supplierProductAndArticleHandlerRole = new UserRole();
        supplierProductAndArticleHandlerRole.setName(UserRole.RoleName.SupplierProductAndArticleHandler);
        Assert.assertTrue(roleController.validOrganizationRole(supplierProductAndArticleHandlerRole, organization));
        
        UserRole supplierAdminRole = new UserRole();
        supplierAdminRole.setName(UserRole.RoleName.Supplieradmin);
        Assert.assertTrue(roleController.validOrganizationRole(supplierAdminRole, organization));
    }
    
    @Test
    public void testValidOrganizationRoleCustomer() {
        Organization organization = new Organization();
        organization.setOrganizationType(Organization.OrganizationType.CUSTOMER);
        
        UserRole customerAgreementManagerRole = new UserRole();
        customerAgreementManagerRole.setName(UserRole.RoleName.CustomerAgreementManager);
        Assert.assertTrue(roleController.validOrganizationRole(customerAgreementManagerRole, organization));
        
        UserRole customerAgreementViewerRole = new UserRole();
        customerAgreementViewerRole.setName(UserRole.RoleName.CustomerAgreementViewer);
        Assert.assertTrue(roleController.validOrganizationRole(customerAgreementViewerRole, organization));
        
        UserRole customerAdminRole = new UserRole();
        customerAdminRole.setName(UserRole.RoleName.Customeradmin);
        Assert.assertTrue(roleController.validOrganizationRole(customerAdminRole, organization));
    }
    
    @Test
    public void testValidOrganizationRoleServiceOwner() {
        Organization organization = new Organization();
        organization.setOrganizationType(Organization.OrganizationType.SERVICE_OWNER);
        
        UserRole superadminRole = new UserRole();
        superadminRole.setName(UserRole.RoleName.Superadmin);
        Assert.assertTrue(roleController.validOrganizationRole(superadminRole, organization));        
    }
    
    @Test
    public void testInvalidOrganizationRoles() {
        UserRole customerAdminRole = new UserRole();
        customerAdminRole.setName(UserRole.RoleName.Customeradmin);
        
        UserRole supplierAgreementViewerRole = new UserRole();
        supplierAgreementViewerRole.setName(UserRole.RoleName.SupplierAgreementViewer);
        
        UserRole superadminRole = new UserRole();
        superadminRole.setName(UserRole.RoleName.Superadmin);
        
        Organization supplierOrganization = new Organization();
        supplierOrganization.setOrganizationType(Organization.OrganizationType.SUPPLIER);
        Assert.assertFalse(roleController.validOrganizationRole(superadminRole, supplierOrganization));
        Assert.assertFalse(roleController.validOrganizationRole(customerAdminRole, supplierOrganization));

        Organization customerOrganization = new Organization();
        customerOrganization.setOrganizationType(Organization.OrganizationType.CUSTOMER);
        Assert.assertFalse(roleController.validOrganizationRole(superadminRole, customerOrganization));
        Assert.assertFalse(roleController.validOrganizationRole(supplierAgreementViewerRole, customerOrganization));

        Organization serviceOwnerOrganization = new Organization();
        serviceOwnerOrganization.setOrganizationType(Organization.OrganizationType.SERVICE_OWNER);
        Assert.assertFalse(roleController.validOrganizationRole(supplierAgreementViewerRole, serviceOwnerOrganization));
        Assert.assertFalse(roleController.validOrganizationRole(customerAdminRole, serviceOwnerOrganization));
        
    }
    
}
