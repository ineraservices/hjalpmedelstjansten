package se.inera.hjalpmedelstjansten.business.product.controller;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowController;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistRowController;
import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationControllerTest;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEDirectiveAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEStandardAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVOrderUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPackageUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.ProductAPITest;
import se.inera.hjalpmedelstjansten.model.api.validation.UserAPITest;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificPropertyListValue;
import se.inera.hjalpmedelstjansten.model.entity.ElectronicAddress;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEDirective;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEStandard;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

import jakarta.enterprise.event.Event;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ArticleControllerFitsToTest {

    ArticleController articleController;
    OrderUnitController unitController;
    PackageUnitController packageUnitController;
    OrganizationController organizationController;
    CategoryController categoryController;
    CeController ceController;
    EntityManager myEntityManager;
    Event<InternalAuditEvent> event;
    ProductController productController;
    AgreementPricelistRowController pricelistRowController;
    GeneralPricelistPricelistRowController generalPricelistPricelistRowController;
    CVPreventiveMaintenanceController cVPreventiveMaintenanceController;
    UserController userController;
    ElasticSearchController elasticSearchController;
    UserAPI userAPI;

    static String articleName = "Article name";
    static String articleArticleNumber = "Article number";
    static Product.Status articleStatus = Product.Status.PUBLISHED;
    static String articleSupplementedInformation = "Article supplemented information";
    static String articleColor = "Article color";
    static String articleTrademark = "Article trademark";
    static String articleManufacturer = "Article manufacturer";
    static String articleManufacturerProductNumber = "Article manufacturer article number";
    static String articleManufacturerWeb = "https://www.inera.se";
    static boolean articleCeMarked = true;
    static String articleCeDirectiveName = "Article Ce Directive name";
    static String articleCeStandardName = "Article Ce Standard name";
    static String articlePreventiveMaintenanceDescription = "Article Preventive maintenance description";
    static int articlePreventiveMaintenanceNumberOfDays = 3;
    static String articleGtin = "1000000000009";
    static String articleOrderUnitName = "Product order unit name";
    static String articleOrderUnitCode = "Product order unit code";
    static String articleGuaranteeUnitName = "Product guarantee unit name";
    static String articleGuaranteeUnitCode = "Product guarantee unit code";
    static Double articleArticleQuantityInOuterPackage = 112.0;
    static Double articlePackageContent = 113.0;
    static Integer articlePackageLevelBase = 114;
    static Integer articlePackageLevelMiddle = 115;
    static Integer articlePackageLevelTop = 116;
    static boolean articleCustomerUnique = false;
    static String organizationName = "organization name";
    static String resourceSpecificPropertyValueTextField = "text data";
    static double resourceSpecificPropertyValueDecimal = 1.0;
    static double resourceSpecificPropertyValueIntervalFrom = 1.0;
    static double resourceSpecificPropertyValueIntervalTo = 10.0;
    static String preventiveMaintenanceUnitCode = "code";
    static String preventiveMaintenanceUnitName = "name";
    static CVPreventiveMaintenance articlePreventiveMaintenanceValidFrom = PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenance(preventiveMaintenanceUnitCode, preventiveMaintenanceUnitName);
    static Map<Long, CVOrderUnit> orderUnitIdMap;
    static Map<Long, CVPackageUnit> packageUnitIdMap;

    static long articleId = 100;
    static long organizationId = 102;
    static long categoryId = 103;
    static long articleCeDirectiveId = 104;
    static long articleCeStandardId = 105;
    static long fitsToProductId = 106;
    static long articleOrderUnitId = 107;
    static long fitsToNonExistingArticleId = 108;
    static long fitsToProductTId = 109;
    static long fitsToProductIId = 110;
    static long fitsToProductTjId = 111;
    static long fitsToProductRId = 112;
    static long fitsToArticleIId = 113;
    static long fitsToArticleTId = 114;
    static long fitsToArticleTjId = 115;
    static long fitsToArticleRId = 116;
    static long fitsToArticleHId = 117;
    static long fitsToProductTNoCodeId = 118;
    static long fitsToArticleTNoCodeId = 119;
    static long userId = 120;
    static long articleBasedOnProductTypeHId = 121;
    static long articleBasedOnProductTypeTId = 122;
    static long categorySpecificPropertyIdTextField = 123;
    static long categorySpecificPropertyIdDecimal = 124;
    static long categorySpecificPropertyIdInterval = 125;
    static long categorySpecificPropertyIdValueListSingle = 126;
    static long categorySpecificPropertyIdValueListMultiple = 127;
    static long resourceSpecificPropertyListValue1 = 128;
    static long resourceSpecificPropertyListValue2 = 129;
    static long resourceSpecificPropertyListValue3 = 130;
    static long resourceSpecificPropertyValueId = 131;

    @Before
    public void init() {
        userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.SupplierProductAndArticleHandler, null);
        myEntityManager = Mockito.mock(EntityManager.class);
        Mockito.when(myEntityManager.find(Article.class, articleId)).thenReturn(createValidArticle(articleId, organizationId));
        Article articleBasedOnProductTypeH = createValidArticle(articleBasedOnProductTypeHId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        basedOnProduct.getCategory().setArticleType(Article.Type.H);
        articleBasedOnProductTypeH.setCategory(null);
        articleBasedOnProductTypeH.setBasedOnProduct(basedOnProduct);
        Mockito.when(myEntityManager.find(Article.class, articleBasedOnProductTypeHId)).thenReturn(articleBasedOnProductTypeH);

        Article articleBasedOnProductTypeT = createValidArticle(articleBasedOnProductTypeTId, organizationId);
        Product basedOnProductTypeT = createBasedOnProductSameValues();
        basedOnProductTypeT.getCategory().setArticleType(Article.Type.T);
        articleBasedOnProductTypeT.setCategory(null);
        articleBasedOnProductTypeT.setBasedOnProduct(basedOnProductTypeT);
        Mockito.when(myEntityManager.find(Article.class, articleBasedOnProductTypeTId)).thenReturn(articleBasedOnProductTypeT);

        Query mockQuery = Mockito.mock(Query.class);
        Mockito.when(mockQuery.setParameter(Mockito.anyString(), Mockito.any())).thenReturn(mockQuery);
        Mockito.when(mockQuery.getResultList()).thenReturn(null);
        Mockito.when(myEntityManager.createNamedQuery(Article.FIND_BY_GTIN)).thenReturn(mockQuery);
        Mockito.when(myEntityManager.createNamedQuery(Article.FIND_BY_ARTICLE_NUMBER_AND_ORGANIZATION)).thenReturn(mockQuery);
        articleController = new ArticleController();
        articleController.LOG = new NoLogger();
        articleController.em = myEntityManager;
        organizationController = Mockito.mock(OrganizationController.class);
        Mockito.when(organizationController.getOrganization(organizationId)).thenReturn(OrganizationControllerTest.createValidOrganization(organizationId, Organization.OrganizationType.SUPPLIER, new Date(), null, organizationName));
        articleController.organizationController = organizationController;
        categoryController = Mockito.mock(CategoryController.class);
        List<Category> categorys = new ArrayList<>();
        Category category = CategoryControllerTest.createValidCategory(categoryId);
        category.setArticleType(Article.Type.T);
        category.setCode(null);
        categorys.add(category);
        Mockito.when(categoryController.getChildlessById(categoryId)).thenReturn(categorys);
        CategorySpecificPropertyListValue categorySpecificPropertyListValue1 = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue1.setUniqueId(resourceSpecificPropertyListValue1);
        Mockito.when(categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyListValue1)).thenReturn(categorySpecificPropertyListValue1);
        CategorySpecificPropertyListValue categorySpecificPropertyListValue2 = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue2.setUniqueId(resourceSpecificPropertyListValue2);
        Mockito.when(categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyListValue2)).thenReturn(categorySpecificPropertyListValue2);
        CategorySpecificPropertyListValue categorySpecificPropertyListValue3 = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue3.setUniqueId(resourceSpecificPropertyListValue3);
        Mockito.when(categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyListValue3)).thenReturn(categorySpecificPropertyListValue3);
        articleController.categoryController = categoryController;
        event = Mockito.mock(Event.class);
        articleController.internalAuditEvent = event;
        ValidationMessageService validationMessageService = Mockito.mock(ValidationMessageService.class);
        Mockito.when(validationMessageService.generateValidationException(Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer<HjalpmedelstjanstenValidationException>() {
            @Override
            public HjalpmedelstjanstenValidationException answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException((String) args[0] + "->" + (String) args[1]);
                exception.addValidationMessage((String) args[0], (String) args[1]);
                return exception;
            }
        });
        Mockito.when(validationMessageService.generateValidationException(Mockito.anyString(), Mockito.anyString(), Mockito.any())).thenAnswer(new Answer<HjalpmedelstjanstenValidationException>() {
            @Override
            public HjalpmedelstjanstenValidationException answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException((String) args[0] + "->" + (String) args[1]);
                exception.addValidationMessage((String) args[0], (String) args[1]);
                return exception;
            }
        });
        articleController.validationMessageService = validationMessageService;

        productController = Mockito.mock(ProductController.class);
        Product connectedToProduct = ProductControllerTest.createValidProduct(fitsToProductId, organizationId);
        connectedToProduct.getCategory().setArticleType(Article.Type.H);
        Mockito.when(productController.getProduct(organizationId, fitsToProductId, userAPI)).thenReturn(connectedToProduct);
        Product connectedToProductI = ProductControllerTest.createValidProduct(fitsToProductIId, organizationId);
        connectedToProductI.getCategory().setArticleType(Article.Type.I);
        Mockito.when(productController.getProduct(organizationId, fitsToProductIId, userAPI)).thenReturn(connectedToProductI);
        Product connectedToProductT = ProductControllerTest.createValidProduct(fitsToProductTId, organizationId);
        connectedToProductT.getCategory().setArticleType(Article.Type.T);
        Mockito.when(productController.getProduct(organizationId, fitsToProductTId, userAPI)).thenReturn(connectedToProductT);
        Product connectedToProductTj = ProductControllerTest.createValidProduct(fitsToProductTjId, organizationId);
        connectedToProductTj.getCategory().setArticleType(Article.Type.Tj);
        Mockito.when(productController.getProduct(organizationId, fitsToProductTjId, userAPI)).thenReturn(connectedToProductTj);
        Product connectedToProductR = ProductControllerTest.createValidProduct(fitsToProductRId, organizationId);
        connectedToProductR.getCategory().setArticleType(Article.Type.R);
        Mockito.when(productController.getProduct(organizationId, fitsToProductRId, userAPI)).thenReturn(connectedToProductR);
        Product connectedToProductTNoCode = ProductControllerTest.createValidProduct(fitsToProductTNoCodeId, organizationId);
        connectedToProductTNoCode.getCategory().setArticleType(Article.Type.T);
        connectedToProductTNoCode.getCategory().setCode(null);
        Mockito.when(productController.getProduct(organizationId, fitsToProductTNoCodeId, userAPI)).thenReturn(connectedToProductTNoCode);
        articleController.productController = productController;

        ArticleValidation articleValidation = new ArticleValidation();
        articleValidation.LOG = new NoLogger();
        articleValidation.validationMessageService = validationMessageService;
        articleValidation.articleController = articleController;
        articleValidation.productController = productController;
        Mockito.when(articleController.findByGtin(articleGtin)).thenReturn(null);
        articleController.articleValidation = articleValidation;

        ceController = Mockito.mock(CeController.class);
        articleController.ceController = ceController;
        CVCEStandard standard = new CVCEStandard();
        standard.setUniqueId(articleCeStandardId);
        CVCEDirective directive = new CVCEDirective();
        directive.setUniqueId(articleCeDirectiveId);
        Mockito.when(ceController.findCEDirectiveById(articleCeDirectiveId)).thenReturn(directive);
        Mockito.when(ceController.findCEStandardById(articleCeStandardId)).thenReturn(standard);

        Article articleTypeI = createValidArticle(fitsToArticleIId, organizationId);
        articleTypeI.getCategory().setArticleType(Article.Type.I);
        Mockito.when(articleController.getArticle(organizationId, fitsToArticleIId, userAPI)).thenReturn(articleTypeI);
        Article articleTypeTj = createValidArticle(fitsToArticleTjId, organizationId);
        articleTypeTj.getCategory().setArticleType(Article.Type.Tj);
        Mockito.when(articleController.getArticle(organizationId, fitsToArticleTjId, userAPI)).thenReturn(articleTypeTj);
        Article articleTypeT = createValidArticle(fitsToArticleTId, organizationId);
        articleTypeT.getCategory().setArticleType(Article.Type.T);
        Mockito.when(articleController.getArticle(organizationId, fitsToArticleTId, userAPI)).thenReturn(articleTypeT);
        Article articleTypeR = createValidArticle(fitsToArticleRId, organizationId);
        articleTypeR.getCategory().setArticleType(Article.Type.R);
        Mockito.when(articleController.getArticle(organizationId, fitsToArticleRId, userAPI)).thenReturn(articleTypeR);
        Article articleTypeTNoCode = createValidArticle(fitsToArticleTNoCodeId, organizationId);
        articleTypeTNoCode.getCategory().setArticleType(Article.Type.T);
        articleTypeTNoCode.getCategory().setCode(null);
        Mockito.when(articleController.getArticle(organizationId, fitsToArticleTNoCodeId, userAPI)).thenReturn(articleTypeTNoCode);
        Article articleTypeH = createValidArticle(fitsToArticleHId, organizationId);
        articleTypeH.getCategory().setArticleType(Article.Type.H);
        Mockito.when(articleController.getArticle(organizationId, fitsToArticleHId, userAPI)).thenReturn(articleTypeH);

        unitController = Mockito.mock(OrderUnitController.class);
        articleController.orderUnitController = unitController;
        CVOrderUnit unit = createValidUnit(articleOrderUnitId);
        Mockito.when(unitController.getUnit(articleOrderUnitId)).thenReturn(unit);
        orderUnitIdMap = new HashMap<>();
        orderUnitIdMap.put(unit.getUniqueId(), unit);
        Mockito.when(unitController.findAllAsIdMap()).thenReturn(orderUnitIdMap);

        packageUnitController = Mockito.mock(PackageUnitController.class);
        articleController.packageUnitController = packageUnitController;
        CVPackageUnit packageUnit = createValidPackageUnit(articleOrderUnitId);
        Mockito.when(packageUnitController.getUnit(articleOrderUnitId)).thenReturn(packageUnit);
        packageUnitIdMap = new HashMap<>();
        packageUnitIdMap.put(unit.getUniqueId(), packageUnit);
        Mockito.when(packageUnitController.findAllAsIdMap()).thenReturn(packageUnitIdMap);

        pricelistRowController = Mockito.mock(AgreementPricelistRowController.class);
        articleController.agreementPricelistRowController = pricelistRowController;

        generalPricelistPricelistRowController = Mockito.mock(GeneralPricelistPricelistRowController.class);
        articleController.generalPricelistPricelistRowController = generalPricelistPricelistRowController;

        cVPreventiveMaintenanceController = Mockito.mock(CVPreventiveMaintenanceController.class);
        Mockito.when(cVPreventiveMaintenanceController.findByCode(preventiveMaintenanceUnitCode)).thenReturn(PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenance(preventiveMaintenanceUnitCode, preventiveMaintenanceUnitName));
        Mockito.when(cVPreventiveMaintenanceController.findByCode(preventiveMaintenanceUnitCode + "2")).thenReturn(PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenance(preventiveMaintenanceUnitCode + "2", preventiveMaintenanceUnitName + "2"));
        articleController.cVPreventiveMaintenanceController = cVPreventiveMaintenanceController;

        userController = Mockito.mock(UserController.class);
        articleController.userController = userController;

        elasticSearchController = Mockito.mock(ElasticSearchController.class);
        articleController.elasticSearchController = elasticSearchController;
    }

    @Test
    public void testSetFitsToArticleTypeHAndFitsToProducts() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setArticleType(Article.Type.H);
        try {
            articleController.setFitsTo(article.getOrganization().getUniqueId(), articleAPI, article, userAPI);
            Assert.fail("Exception not thrown but should have been");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
        }
    }

    @Test
    public void testSetFitsToArticleTypeHAndFitsToArticles() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setArticleType(Article.Type.H);
        List<ArticleAPI> fitsToArticles = new ArrayList<>();
        ArticleAPI fitsToArticle = createValidArticleAPI();
        fitsToArticles.add(fitsToArticle);
        articleAPI.setFitsToArticles(fitsToArticles);
        articleAPI.setFitsToProducts(null);
        try {
            articleController.setFitsTo(article.getOrganization().getUniqueId(), articleAPI, article, userAPI);
            Assert.fail("Exception not thrown but should have been");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
        }
    }


    @Test
    public void testSetFitsToArticleTypeHAndFitsToArticlesFitsToProducts() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setArticleType(Article.Type.H);
        List<ArticleAPI> fitsToArticles = new ArrayList<>();
        ArticleAPI fitsToArticle = createValidArticleAPI();
        fitsToArticles.add(fitsToArticle);
        articleAPI.setFitsToArticles(fitsToArticles);
        try {
            articleController.setFitsTo(article.getOrganization().getUniqueId(), articleAPI, article, userAPI);
            Assert.fail("Exception not thrown but should have been");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
        }
    }

    @Test
    public void testSetFitsToArticleTypeHAndNoFitsTo() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setArticleType(Article.Type.H);
        articleAPI.setFitsToArticles(null);
        articleAPI.setFitsToProducts(null);
        try {
            articleController.setFitsTo(article.getOrganization().getUniqueId(), articleAPI, article, userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown but should not have been");
        }
    }

    @Test
    public void testSetFitsToArticleTypeRAndFitsToArticleWithoutCategoryBasedOnProductWithCategoryT() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setArticleType(Article.Type.R);
        article.getCategory().setCode(null);
        List<ArticleAPI> fitsToArticles = new ArrayList<>();
        ArticleAPI fitsToArticle = createValidArticleAPI();
        fitsToArticle.setId(articleBasedOnProductTypeTId);
        fitsToArticles.add(fitsToArticle);
        articleAPI.setFitsToArticles(fitsToArticles);
        try {
            articleController.setFitsTo(article.getOrganization().getUniqueId(), articleAPI, article, userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown but should not have been");
        }
    }

    @Test
    public void testSetFitsToNullCodeNotFitsTo() {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setFitsToProducts(null);
        articleAPI.setFitsToArticles(null);
        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setCode(null);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    }

    @Test
    public void testSetFitsToNotNullCodeAndFitsToProducts() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.assertTrue(ex.getMessage().contains("article.product.isoCode.fitsTo.notNull"));
        }
    }

    @Test
    public void testSetFitsToNotNullCodeAndFitsToArticles() {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setFitsToProducts(null);
        List<ArticleAPI> fitsToArticleAPIs = new ArrayList<>();
        ArticleAPI fitsToArticleAPI = new ArticleAPI();
        fitsToArticleAPI.setId(fitsToNonExistingArticleId);
        fitsToArticleAPIs.add(fitsToArticleAPI);
        articleAPI.setFitsToArticles(fitsToArticleAPIs);

        Article article = createValidArticle(articleId, organizationId);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.assertTrue(ex.getMessage().contains("article.article.isoCode.fitsTo.notNull"));
        }
    }

    @Test
    public void testSetFitsToNotArticleNotExists() {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setFitsToProducts(null);
        List<ArticleAPI> fitsToArticleAPIs = new ArrayList<>();
        ArticleAPI fitsToArticleAPI = new ArticleAPI();
        fitsToArticleAPI.setId(fitsToNonExistingArticleId);
        fitsToArticleAPIs.add(fitsToArticleAPI);
        articleAPI.setFitsToArticles(fitsToArticleAPIs);

        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setArticleType(Article.Type.R);
        article.getCategory().setCode(null);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.assertTrue(ex.getMessage().contains("article.article.fitsTo.notExists"));
        }
    }

    @Test
    public void testSetFitsToNullCodeAndFitsToProductRightArticleTypeTNoCode() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ProductAPI> fitsToProductAPIs = new ArrayList<>();
        ProductAPI productAPI = new ProductAPI();
        productAPI.setId(fitsToProductTNoCodeId);
        fitsToProductAPIs.add(productAPI);
        articleAPI.setFitsToProducts(fitsToProductAPIs);

        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setArticleType(Article.Type.R);
        article.getCategory().setCode(null);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.assertTrue(ex.getMessage().contains("article.product.fitsTo.noCode"));
        }
    }

    @Test
    public void testSetFitsToNullCodeTypeTAndFitsToProductArticleTypeT() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ProductAPI> fitsToProductAPIs = new ArrayList<>();
        ProductAPI productAPI = new ProductAPI();
        productAPI.setId(fitsToProductTId);
        fitsToProductAPIs.add(productAPI);
        articleAPI.setFitsToProducts(fitsToProductAPIs);

        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setArticleType(Article.Type.T);
        article.getCategory().setCode(null);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    }

    @Test
    public void testSetFitsToNullCodeAndFitsToProductRightArticleTypeT() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ProductAPI> fitsToProductAPIs = new ArrayList<>();
        ProductAPI productAPI = new ProductAPI();
        productAPI.setId(fitsToProductTId);
        fitsToProductAPIs.add(productAPI);
        articleAPI.setFitsToProducts(fitsToProductAPIs);

        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setArticleType(Article.Type.R);
        article.getCategory().setCode(null);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    }

    @Test
    public void testSetFitsToNullCodeAndFitsToProductWrongArticleTypeI() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ProductAPI> fitsToProductAPIs = new ArrayList<>();
        ProductAPI productAPI = new ProductAPI();
        productAPI.setId(fitsToProductIId);
        fitsToProductAPIs.add(productAPI);
        articleAPI.setFitsToProducts(fitsToProductAPIs);

        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setCode(null);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.assertTrue(ex.getMessage().contains("article.product.fitsTo.productWrongType"));
        }
    }

    @Test
    public void testSetFitsToNullCodeAndFitsToProductWrongArticleTypeTj() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ProductAPI> fitsToProductAPIs = new ArrayList<>();
        ProductAPI productAPI = new ProductAPI();
        productAPI.setId(fitsToProductTjId);
        fitsToProductAPIs.add(productAPI);
        articleAPI.setFitsToProducts(fitsToProductAPIs);

        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setCode(null);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.assertTrue(ex.getMessage().contains("article.product.fitsTo.productWrongType"));
        }
    }

    @Test
    public void testSetFitsToNullCodeAndFitsToProductWrongArticleTypeR() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ProductAPI> fitsToProductAPIs = new ArrayList<>();
        ProductAPI productAPI = new ProductAPI();
        productAPI.setId(fitsToProductRId);
        fitsToProductAPIs.add(productAPI);
        articleAPI.setFitsToProducts(fitsToProductAPIs);

        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setCode(null);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.assertTrue(ex.getMessage().contains("article.product.fitsTo.productWrongType"));
        }
    }

    @Test
    public void testSetFitsToNullCodeAndFitsToArticleWrongArticleTypeI() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ArticleAPI> fitsToArticleAPIs = new ArrayList<>();
        ArticleAPI fitsToArticleAPI = new ArticleAPI();
        fitsToArticleAPI.setId(fitsToArticleIId);
        fitsToArticleAPIs.add(fitsToArticleAPI);
        articleAPI.setFitsToArticles(fitsToArticleAPIs);

        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setCode(null);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.assertTrue(ex.getMessage().contains("article.article.fitsTo.articleWrongType"));
        }
    }

    @Test
    public void testSetArticleWithArticleTypeTNoCodeFitsToArticleTypeTNoCode() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ArticleAPI> fitsToArticleAPIs = new ArrayList<>();
        ArticleAPI fitsToArticleAPI = new ArticleAPI();
        fitsToArticleAPI.setId(fitsToArticleTNoCodeId);
        fitsToArticleAPIs.add(fitsToArticleAPI);
        articleAPI.setFitsToArticles(fitsToArticleAPIs);

        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setArticleType(Article.Type.T);
        article.getCategory().setCode(null);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    }

    @Test
    public void testSetArticleWithArticleTypeTNoCodeFitsToArticleTypeH() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ArticleAPI> fitsToArticleAPIs = new ArrayList<>();
        ArticleAPI fitsToArticleAPI = new ArticleAPI();
        fitsToArticleAPI.setId(fitsToArticleHId);
        fitsToArticleAPIs.add(fitsToArticleAPI);
        articleAPI.setFitsToArticles(fitsToArticleAPIs);

        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setArticleType(Article.Type.T);
        article.getCategory().setCode(null);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    }

    @Test
    public void testSetArticleWithArticleTypeINoCodeFitsToArticleTypeT() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ArticleAPI> fitsToArticleAPIs = new ArrayList<>();
        ArticleAPI fitsToArticleAPI = new ArticleAPI();
        fitsToArticleAPI.setId(fitsToArticleTId);
        fitsToArticleAPIs.add(fitsToArticleAPI);
        articleAPI.setFitsToArticles(fitsToArticleAPIs);

        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setArticleType(Article.Type.I);
        article.getCategory().setCode(null);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    }

    @Test
    public void testSetArticleWithArticleTypeHNoCodeFitsToAnything() {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setFitsToProducts(null);
        List<ArticleAPI> fitsToArticleAPIs = new ArrayList<>();
        ArticleAPI fitsToArticleAPI = new ArticleAPI();
        fitsToArticleAPI.setId(fitsToArticleTId);
        fitsToArticleAPIs.add(fitsToArticleAPI);
        articleAPI.setFitsToArticles(fitsToArticleAPIs);

        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setArticleType(Article.Type.H);
        article.getCategory().setCode(null);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.assertTrue(ex.getMessage().contains("article.article.fitsTo.articleTypeH"));
        }
    }

    @Test
    public void testSetArticleFitsToSame() {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setFitsToProducts(null);
        List<ArticleAPI> fitsToArticleAPIs = new ArrayList<>();
        ArticleAPI fitsToArticleAPI = new ArticleAPI();
        fitsToArticleAPI.setId(articleId);
        fitsToArticleAPIs.add(fitsToArticleAPI);
        articleAPI.setFitsToArticles(fitsToArticleAPIs);

        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setArticleType(Article.Type.R);
        article.getCategory().setCode(null);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.assertTrue(ex.getMessage().contains("article.article.fitsTo.same"));
        }
    }

    @Test
    public void testSetFitsToNullCodeAndFitsToArticleRightArticleTypeTCode() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ArticleAPI> fitsToArticleAPIs = new ArrayList<>();
        ArticleAPI fitsToArticleAPI = new ArticleAPI();
        fitsToArticleAPI.setId(fitsToArticleTId);
        fitsToArticleAPIs.add(fitsToArticleAPI);
        articleAPI.setFitsToArticles(fitsToArticleAPIs);

        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setArticleType(Article.Type.R);
        article.getCategory().setCode(null);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    }

    @Test
    public void testSetFitsToNullCodeAndFitsToArticleWrongArticleTypeTj() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ArticleAPI> fitsToArticleAPIs = new ArrayList<>();
        ArticleAPI fitsToArticleAPI = new ArticleAPI();
        fitsToArticleAPI.setId(fitsToArticleTjId);
        fitsToArticleAPIs.add(fitsToArticleAPI);
        articleAPI.setFitsToArticles(fitsToArticleAPIs);

        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setCode(null);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.assertTrue(ex.getMessage().contains("article.article.fitsTo.articleWrongType"));
        }
    }

    @Test
    public void testSetFitsToNullCodeAndFitsToArticleWrongArticleTypeR() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ArticleAPI> fitsToArticleAPIs = new ArrayList<>();
        ArticleAPI fitsToArticleAPI = new ArticleAPI();
        fitsToArticleAPI.setId(fitsToArticleRId);
        fitsToArticleAPIs.add(fitsToArticleAPI);
        articleAPI.setFitsToArticles(fitsToArticleAPIs);

        Article article = createValidArticle(articleId, organizationId);
        article.getCategory().setCode(null);
        try {
            articleController.setFitsTo(organizationId, articleAPI, article, userAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.assertTrue(ex.getMessage().contains("article.article.fitsTo.articleWrongType"));
        }
    }

    private static Product createBasedOnProductSameValues() {
        Product basedOnProduct = new Product();
        CVPackageUnit packageUnit = createValidPackageUnit(articleOrderUnitId);
        CVOrderUnit unit = createValidUnit(articleOrderUnitId);
        basedOnProduct.setOrderUnit(unit);
        basedOnProduct.setArticleQuantityInOuterPackage(articleArticleQuantityInOuterPackage);
        basedOnProduct.setArticleQuantityInOuterPackageUnit(unit);
        basedOnProduct.setPackageContent(articlePackageContent);
        basedOnProduct.setPackageContentUnit(packageUnit);
        basedOnProduct.setPackageLevelBase(articlePackageLevelBase);
        basedOnProduct.setPackageLevelMiddle(articlePackageLevelMiddle);
        basedOnProduct.setPackageLevelTop(articlePackageLevelTop);

        basedOnProduct.setCustomerUnique(articleCustomerUnique);

        // create category
        basedOnProduct.setCategory(CategoryControllerTest.createValidCategory(null));

        // set ce
        basedOnProduct.setCeMarked(articleCeMarked);

        // create ce standard
        CVCEStandard cEStandard = new CVCEStandard();
        cEStandard.setUniqueId(articleCeStandardId);
        cEStandard.setName(articleCeStandardName);
        basedOnProduct.setCeStandard(cEStandard);

        // create ce directive
        CVCEDirective cEDirective = new CVCEDirective();
        cEDirective.setUniqueId(articleCeDirectiveId);
        cEDirective.setName(articleCeDirectiveName);
        basedOnProduct.setCeDirective(cEDirective);

        // set manufacturer
        basedOnProduct.setTrademark(articleTrademark);
        basedOnProduct.setManufacturer(articleManufacturer);
        basedOnProduct.setManufacturerProductNumber(articleManufacturerProductNumber);
        ElectronicAddress manufacturerElectronicAddress = new ElectronicAddress();
        manufacturerElectronicAddress.setWeb(articleManufacturerWeb);
        basedOnProduct.setManufacturerElectronicAddress(manufacturerElectronicAddress);

        // preventive maintenance
        basedOnProduct.setPreventiveMaintenanceDescription(articlePreventiveMaintenanceDescription);
        basedOnProduct.setPreventiveMaintenanceNumberOfDays(articlePreventiveMaintenanceNumberOfDays);
        basedOnProduct.setPreventiveMaintenanceValidFrom(articlePreventiveMaintenanceValidFrom);

        // supplemental information
        basedOnProduct.setSupplementedInformation(articleSupplementedInformation);

        basedOnProduct.setOrganization(OrganizationControllerTest.createValidOrganization(organizationId, null, null, null, null));

        return basedOnProduct;
    }

    public static Article createValidArticle(long articleId, long organizationId) {
        return createValidArticle(articleId, organizationId, Product.Status.PUBLISHED);
    }

    public static Article createValidArticle(long articleId, long organizationId, Product.Status status) {
        Article article = new Article();
        article.setArticleNumber(articleArticleNumber);
        article.setUniqueId(articleId);
        article.setArticleName(articleName);
        article.setStatus(status);
        article.setSupplementedInformation(articleSupplementedInformation);
        article.setGtin(articleGtin);
        article.setCustomerUnique(false);

        // set manufacturer
        article.setTrademark(articleTrademark);
        article.setManufacturer(articleManufacturer);
        article.setManufacturerArticleNumber(articleManufacturerProductNumber);
        ElectronicAddress manufacturerElectronicAddress = new ElectronicAddress();
        manufacturerElectronicAddress.setWeb(articleManufacturerWeb);
        article.setManufacturerElectronicAddress(manufacturerElectronicAddress);

        // preventive maintenance
        article.setPreventiveMaintenanceDescription(articlePreventiveMaintenanceDescription);
        article.setPreventiveMaintenanceNumberOfDays(articlePreventiveMaintenanceNumberOfDays);
        article.setPreventiveMaintenanceValidFrom(articlePreventiveMaintenanceValidFrom);

        // create category
        article.setCategory(CategoryControllerTest.createValidCategory(null));

        article.setOrganization(OrganizationControllerTest.createValidOrganization(organizationId, Organization.OrganizationType.SUPPLIER, new Date(), null, organizationName));

        // set ce
        article.setCeMarked(articleCeMarked);

        // create ce standard
        CVCEStandard cEStandard = new CVCEStandard();
        cEStandard.setUniqueId(articleCeStandardId);
        cEStandard.setName(articleCeStandardName);
        article.setCeStandard(cEStandard);

        // create ce directive
        CVCEDirective cEDirective = new CVCEDirective();
        cEDirective.setUniqueId(articleCeDirectiveId);
        cEDirective.setName(articleCeDirectiveName);
        article.setCeDirective(cEDirective);

        CVOrderUnit unit = createValidUnit(articleOrderUnitId);
        CVPackageUnit packgeUnit = createValidPackageUnit(articleOrderUnitId);
        article.setOrderUnit(unit);
        article.setArticleQuantityInOuterPackage(articleArticleQuantityInOuterPackage);
        article.setArticleQuantityInOuterPackageUnit(unit);
        article.setPackageContent(articlePackageContent);
        article.setPackageContentUnit(packgeUnit);
        article.setPackageLevelBase(articlePackageLevelBase);
        article.setPackageLevelMiddle(articlePackageLevelMiddle);
        article.setPackageLevelTop(articlePackageLevelTop);

        return article;
    }

    public static CVOrderUnit createValidUnit( long id ) {
        CVOrderUnit unit = new CVOrderUnit();
        unit.setUniqueId(id);
        unit.setCode(articleOrderUnitCode);
        unit.setName(articleOrderUnitName);
        return unit;
    }

    public static CVPackageUnit createValidPackageUnit( long id ) {
        CVPackageUnit unit = new CVPackageUnit();
        unit.setUniqueId(id);
        unit.setCode(articleOrderUnitCode);
        unit.setName(articleOrderUnitName);
        return unit;
    }

    public static ArticleAPI createValidArticleAPI() {
        ArticleAPI articleAPI = new ArticleAPI();
        articleAPI.setId(articleId);
        articleAPI.setArticleName("test");
        articleAPI.setArticleNumber(articleArticleNumber);
        articleAPI.setCustomerUnique(articleCustomerUnique);
        articleAPI.setStatus(Product.Status.PUBLISHED.toString());
        articleAPI.setCategory(ProductAPITest.createValidCategoryAPI());

        // preventive maintenance
        articleAPI.setPreventiveMaintenanceDescription(articlePreventiveMaintenanceDescription);
        articleAPI.setPreventiveMaintenanceNumberOfDays(articlePreventiveMaintenanceNumberOfDays);
        articleAPI.setPreventiveMaintenanceValidFrom(PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenanceAPI(preventiveMaintenanceUnitCode, preventiveMaintenanceUnitName));

        // set manufacturer
        articleAPI.setTrademark(articleTrademark);
        articleAPI.setSupplementedInformation(articleSupplementedInformation);
        articleAPI.setManufacturer(articleManufacturer);
        articleAPI.setManufacturerArticleNumber(articleManufacturerProductNumber);
        ElectronicAddressAPI manufacturerElectronicAddress = new ElectronicAddressAPI();
        manufacturerElectronicAddress.setWeb(articleManufacturerWeb);
        articleAPI.setManufacturerElectronicAddress(manufacturerElectronicAddress);

        CategoryAPI categoryAPI = new CategoryAPI();
        categoryAPI.setId(categoryId);
        categoryAPI.setArticleType(Article.Type.T);
        articleAPI.setCategory(categoryAPI);

        articleAPI.setCeMarked(articleCeMarked);
        articleAPI.setCeDirective(createValidCEDirectiveAPI());
        articleAPI.setCeStandard(createValidCEStandardAPI());

        List<ProductAPI> fitsToProductAPIs = new ArrayList<>();
        ProductAPI productAPI = new ProductAPI();
        productAPI.setId(fitsToProductId);
        fitsToProductAPIs.add(productAPI);
        articleAPI.setFitsToProducts(fitsToProductAPIs);

        CVOrderUnitAPI unitAPI = createValidUnitAPI(articleOrderUnitId);
        CVPackageUnitAPI packageUnitAPI = createValidPackageUnitAPI(articleOrderUnitId);
        articleAPI.setOrderUnit(unitAPI);
        articleAPI.setArticleQuantityInOuterPackage(articleArticleQuantityInOuterPackage);
        articleAPI.setArticleQuantityInOuterPackageUnit(unitAPI);
        articleAPI.setPackageContent(articlePackageContent);
        articleAPI.setPackageContentUnit(packageUnitAPI);
        articleAPI.setPackageLevelBase(articlePackageLevelBase);
        articleAPI.setPackageLevelMiddle(articlePackageLevelMiddle);
        articleAPI.setPackageLevelTop(articlePackageLevelTop);

       return articleAPI;
    }

    private static CVCEDirectiveAPI createValidCEDirectiveAPI() {
        CVCEDirectiveAPI directiveAPI = new CVCEDirectiveAPI();
        directiveAPI.setId(articleCeDirectiveId);
        return directiveAPI;
    }

    private static CVCEStandardAPI createValidCEStandardAPI() {
        CVCEStandardAPI standardAPI = new CVCEStandardAPI();
        standardAPI.setId(articleCeStandardId);
        return standardAPI;
    }

    public static CVOrderUnitAPI createValidUnitAPI( long id ) {
        CVOrderUnitAPI unitAPI = new CVOrderUnitAPI();
        unitAPI.setId(id);
        unitAPI.setCode(articleOrderUnitCode);
        unitAPI.setName(articleOrderUnitName);
        return unitAPI;
    }

    public static CVPackageUnitAPI createValidPackageUnitAPI( long id ) {
        CVPackageUnitAPI unitAPI = new CVPackageUnitAPI();
        unitAPI.setId(id);
        unitAPI.setCode(articleOrderUnitCode);
        unitAPI.setName(articleOrderUnitName);
        return unitAPI;
    }

    public static CVGuaranteeUnitAPI createValidGuaranteeUnitAPI( long id ) {
        CVGuaranteeUnitAPI unitAPI = new CVGuaranteeUnitAPI();
        unitAPI.setId(id);
        unitAPI.setCode(articleGuaranteeUnitCode);
        unitAPI.setName(articleGuaranteeUnitName);
        return unitAPI;
    }

    public static CVGuaranteeUnit createValidGuaranteeUnit( long id ) {
        CVGuaranteeUnit unit = new CVGuaranteeUnit();
        unit.setUniqueId(id);
        unit.setCode(articleGuaranteeUnitCode);
        unit.setName(articleGuaranteeUnitName);
        return unit;
    }

}
