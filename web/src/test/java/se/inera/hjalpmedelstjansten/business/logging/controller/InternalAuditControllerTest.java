package se.inera.hjalpmedelstjansten.business.logging.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

import java.util.Date;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit.ActionType;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit.EntityType;

@ExtendWith(MockitoExtension.class)
class InternalAuditControllerTest {

    @Mock
    private HjmtLogger LOG;
    @Mock
    private EntityManager em;
    @InjectMocks
    private InternalAuditController internalAuditController;

    @Test
    void shallSaveInternalAuditWithAuditTime() {
        final var expected = new Date();

        final var internalAuditEventArgumentCaptor = ArgumentCaptor.forClass(InternalAudit.class);

        final var internalAuditEvent = new InternalAuditEvent(
            expected, null, null, null, null, null, null
        );
        internalAuditController.handleInternalAuditEvent(internalAuditEvent);

        verify(em).persist(internalAuditEventArgumentCaptor.capture());

        assertEquals(expected, internalAuditEventArgumentCaptor.getValue().getAuditTime());
    }

    @Test
    void shallSaveInternalAuditWithEntityType() {
        final var expected = EntityType.ORGANIZATION;

        final var internalAuditEventArgumentCaptor = ArgumentCaptor.forClass(InternalAudit.class);

        final var internalAuditEvent = new InternalAuditEvent(
            null, expected, null, null, null, null, null
        );
        internalAuditController.handleInternalAuditEvent(internalAuditEvent);

        verify(em).persist(internalAuditEventArgumentCaptor.capture());

        assertEquals(expected, internalAuditEventArgumentCaptor.getValue().getEntityType());
    }

    @Test
    void shallSaveInternalAuditWithActionType() {
        final var expected = ActionType.CREATE;

        final var internalAuditEventArgumentCaptor = ArgumentCaptor.forClass(InternalAudit.class);

        final var internalAuditEvent = new InternalAuditEvent(
            null, null, expected, null, null, null, null
        );
        internalAuditController.handleInternalAuditEvent(internalAuditEvent);

        verify(em).persist(internalAuditEventArgumentCaptor.capture());

        assertEquals(expected, internalAuditEventArgumentCaptor.getValue().getActionType());
    }

    @Test
    void shallSaveInternalAuditWithUserEngagementId() {
        final var expected = 9889L;

        final var internalAuditEventArgumentCaptor = ArgumentCaptor.forClass(InternalAudit.class);

        final var internalAuditEvent = new InternalAuditEvent(
            null, null, null, expected, null, null, null
        );
        internalAuditController.handleInternalAuditEvent(internalAuditEvent);

        verify(em).persist(internalAuditEventArgumentCaptor.capture());

        assertEquals(expected, internalAuditEventArgumentCaptor.getValue().getUserEngagementId());
    }

    @Test
    void shallSaveInternalAuditWithUserSessionId() {
        final var expected = "sessions-id";

        final var internalAuditEventArgumentCaptor = ArgumentCaptor.forClass(InternalAudit.class);

        final var internalAuditEvent = new InternalAuditEvent(
            null, null, null, null, expected, null, null
        );
        internalAuditController.handleInternalAuditEvent(internalAuditEvent);

        verify(em).persist(internalAuditEventArgumentCaptor.capture());

        assertEquals(expected, internalAuditEventArgumentCaptor.getValue().getSessionId());
    }

    @Test
    void shallSaveInternalAuditWithEntityId() {
        final var expected = 99L;

        final var internalAuditEventArgumentCaptor = ArgumentCaptor.forClass(InternalAudit.class);

        final var internalAuditEvent = new InternalAuditEvent(
            null, null, null, null, null, expected, null
        );
        internalAuditController.handleInternalAuditEvent(internalAuditEvent);

        verify(em).persist(internalAuditEventArgumentCaptor.capture());

        assertEquals(expected, internalAuditEventArgumentCaptor.getValue().getEntityId());
    }

    @Test
    void shallSaveInternalAuditWithRequestIp() {
        final var expected = "99.88.77.66";

        final var internalAuditEventArgumentCaptor = ArgumentCaptor.forClass(InternalAudit.class);

        final var internalAuditEvent = new InternalAuditEvent(
            null, null, null, null, null, null, expected
        );
        internalAuditController.handleInternalAuditEvent(internalAuditEvent);

        verify(em).persist(internalAuditEventArgumentCaptor.capture());

        assertEquals(expected, internalAuditEventArgumentCaptor.getValue().getRequestIp());
    }
}
