package se.inera.hjalpmedelstjansten.business.generalpricelist.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationControllerTest;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleControllerTest;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.UserAPITest;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;

import jakarta.enterprise.event.Event;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class GeneralPricelistControllerTest {

    GeneralPricelistController generalPricelistController;
    EntityManager myEntityManager;
    OrganizationController organizationController;
    UserController userController;
    GuaranteeUnitController guaranteeUnitController;
    Organization customerOrganization;
    Organization supplierOrganization;
    GeneralPricelistValidation generalPricelistValidation;

    static final long supplierOrganizationId = 1;
    static final long generalPricelistId = 2;
    static final long guaranteeUnitId = 3;
    static final long userId = 4;
    static final long customerOrganizationId = 5;
    static final String generalPricelistName = "generalPricelistName";
    static final String generalPricelistNumber = "generalPricelistNumber";

    @Before
    public void init() {
        generalPricelistController = new GeneralPricelistController();
        generalPricelistController.LOG = new NoLogger();

        myEntityManager = Mockito.mock(EntityManager.class);
        Query mockQuery = Mockito.mock(Query.class);
        Mockito.when(mockQuery.setParameter(Mockito.anyString(), Mockito.any())).thenReturn(mockQuery);
        GeneralPricelist generalPricelist = createValidGeneralPricelist();
        List<GeneralPricelist> generalPricelists = new ArrayList<>();
        generalPricelists.add(generalPricelist);
        Mockito.when(mockQuery.getResultList()).thenReturn(generalPricelists);
        Mockito.when(myEntityManager.createNamedQuery(GeneralPricelist.FIND_BY_ORGANIZATION)).thenReturn(mockQuery);
        Query mockRowsQuery = Mockito.mock(Query.class);
        Mockito.when(myEntityManager.createNamedQuery(GeneralPricelistPricelistRow.COUNT_BY_GENERAL_PRICELIST)).thenReturn(mockRowsQuery);
        Mockito.when(mockRowsQuery.setParameter(Mockito.anyString(), Mockito.any())).thenReturn(mockRowsQuery);
        Mockito.when(mockRowsQuery.getSingleResult()).thenReturn(null);
        generalPricelistController.em = myEntityManager;

        organizationController = Mockito.mock(OrganizationController.class);
        supplierOrganization = OrganizationControllerTest.createValidOrganization(supplierOrganizationId, Organization.OrganizationType.SUPPLIER, new Date(), null, null);
        customerOrganization = OrganizationControllerTest.createValidOrganization(customerOrganizationId, Organization.OrganizationType.CUSTOMER, new Date(), null, null);
        Mockito.when(organizationController.getOrganization(supplierOrganizationId)).thenReturn(supplierOrganization);
        Mockito.when(organizationController.getOrganization(customerOrganizationId)).thenReturn(customerOrganization);
        generalPricelistController.organizationController = organizationController;

        ValidationMessageService validationMessageService = Mockito.mock(ValidationMessageService.class);
        Mockito.when(validationMessageService.generateValidationException(Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer<HjalpmedelstjanstenValidationException>() {
            @Override
            public HjalpmedelstjanstenValidationException answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException((String) args[0] + "->" + (String) args[1]);
                exception.addValidationMessage((String) args[0], (String) args[1]);
                return exception;
            }
        });
        generalPricelistController.validationMessageService = validationMessageService;

        guaranteeUnitController = Mockito.mock(GuaranteeUnitController.class);
        CVGuaranteeUnit unit = ArticleControllerTest.createValidGuaranteeUnit(guaranteeUnitId);
        Mockito.when(guaranteeUnitController.getGuaranteeUnit(guaranteeUnitId)).thenReturn(unit);
        generalPricelistController.guaranteeUnitController = guaranteeUnitController;

        Event<InternalAuditEvent> internalAuditEvent = Mockito.mock(Event.class);
        generalPricelistController.internalAuditEvent = internalAuditEvent;

        generalPricelistValidation = new GeneralPricelistValidation();
        generalPricelistValidation.LOG = new NoLogger();
        generalPricelistValidation.validationMessageService = validationMessageService;
        generalPricelistController.generalPricelistValidation = generalPricelistValidation;

        userController = Mockito.mock(UserController.class);
        generalPricelistController.userController = userController;
    }

    @Test
    public void testGetAgreement() {
        GeneralPricelist generalPricelist = generalPricelistController.getGeneralPricelist(supplierOrganizationId);
        Assert.assertNotNull(generalPricelist);
    }


    @Test
    public void testGetAgreementAPI() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, supplierOrganizationId, null, UserRole.RoleName.SupplierAgreementManager, null);
        GeneralPricelistAPI generalPricelistAPI = generalPricelistController.getGeneralPricelistAPI(supplierOrganizationId, userAPI, null, null);
        Assert.assertNotNull(generalPricelistAPI);
    }

    @Test
    public void testSetOwnerOrganization() {
        GeneralPricelist generalPricelist = createValidGeneralPricelist();
        try {
            generalPricelistController.setOwnerOrganization(generalPricelist, supplierOrganizationId);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    }

    @Test
    public void testSetSupplierWrongSupplierOrganizationId() {
        GeneralPricelist generalPricelist = createValidGeneralPricelist();
        try {
            generalPricelistController.setOwnerOrganization(generalPricelist, supplierOrganizationId + 1000);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("ownerOrganization", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetSupplierWrongSupplierOrganizationType() {
        GeneralPricelist generalPricelist = createValidGeneralPricelist();
        try {
            generalPricelistController.setOwnerOrganization(generalPricelist, customerOrganizationId);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("ownerOrganization", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetWarrantyQuantityUnits() throws HjalpmedelstjanstenValidationException {
        GeneralPricelist generalPricelist = createValidGeneralPricelist();
        GeneralPricelistAPI generalPricelistAPI = createValidGeneralPricelistAPI();
        CVGuaranteeUnitAPI warrantyQuantityUnit = ArticleControllerTest.createValidGuaranteeUnitAPI(guaranteeUnitId);
        generalPricelistAPI.setWarrantyQuantityHUnit(warrantyQuantityUnit);
        generalPricelistAPI.setWarrantyQuantityIUnit(warrantyQuantityUnit);
        generalPricelistAPI.setWarrantyQuantityRUnit(warrantyQuantityUnit);
        generalPricelistAPI.setWarrantyQuantityTUnit(warrantyQuantityUnit);
        generalPricelistAPI.setWarrantyQuantityTJUnit(warrantyQuantityUnit);
        generalPricelistController.setWarrantyQuantityUnits(generalPricelist, generalPricelistAPI);

        Assert.assertNotNull(generalPricelist.getWarrantyQuantityHUnit());
        Assert.assertNotNull(generalPricelist.getWarrantyQuantityIUnit());
        Assert.assertNotNull(generalPricelist.getWarrantyQuantityRUnit());
        Assert.assertNotNull(generalPricelist.getWarrantyQuantityTUnit());
        Assert.assertNotNull(generalPricelist.getWarrantyQuantityTJUnit());
    }

    @Test
    public void testSetWarrantyQuantityUnitsInvalidHUnit() {
        GeneralPricelist generalPricelist = createValidGeneralPricelist();
        GeneralPricelistAPI generalPricelistAPI = createValidGeneralPricelistAPI();
        CVGuaranteeUnitAPI warrantyQuantityUnit = ArticleControllerTest.createValidGuaranteeUnitAPI(guaranteeUnitId+1);
        generalPricelistAPI.setWarrantyQuantityHUnit(warrantyQuantityUnit);
        try {
            generalPricelistController.setWarrantyQuantityUnits(generalPricelist, generalPricelistAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("warrantyQuantityHUnit", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetWarrantyQuantityUnitsInvalidIUnit() {
        GeneralPricelist generalPricelist = createValidGeneralPricelist();
        GeneralPricelistAPI generalPricelistAPI = createValidGeneralPricelistAPI();
        CVGuaranteeUnitAPI warrantyQuantityUnit = ArticleControllerTest.createValidGuaranteeUnitAPI(guaranteeUnitId+1);
        generalPricelistAPI.setWarrantyQuantityIUnit(warrantyQuantityUnit);
        try {
            generalPricelistController.setWarrantyQuantityUnits(generalPricelist, generalPricelistAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("warrantyQuantityIUnit", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetWarrantyQuantityUnitsInvalidRUnit() {
        GeneralPricelist generalPricelist = createValidGeneralPricelist();
        GeneralPricelistAPI generalPricelistAPI = createValidGeneralPricelistAPI();
        CVGuaranteeUnitAPI warrantyQuantityUnit = ArticleControllerTest.createValidGuaranteeUnitAPI(guaranteeUnitId+1);
        generalPricelistAPI.setWarrantyQuantityRUnit(warrantyQuantityUnit);
        try {
            generalPricelistController.setWarrantyQuantityUnits(generalPricelist, generalPricelistAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("warrantyQuantityRUnit", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetWarrantyQuantityUnitsInvalidTUnit() {
        GeneralPricelist generalPricelist = createValidGeneralPricelist();
        GeneralPricelistAPI generalPricelistAPI = createValidGeneralPricelistAPI();
        CVGuaranteeUnitAPI warrantyQuantityUnit = ArticleControllerTest.createValidGuaranteeUnitAPI(guaranteeUnitId+1);
        generalPricelistAPI.setWarrantyQuantityTUnit(warrantyQuantityUnit);
        try {
            generalPricelistController.setWarrantyQuantityUnits(generalPricelist, generalPricelistAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("warrantyQuantityTUnit", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetWarrantyQuantityUnitsInvalidTJUnit() {
        GeneralPricelist generalPricelist = createValidGeneralPricelist();
        GeneralPricelistAPI generalPricelistAPI = createValidGeneralPricelistAPI();
        CVGuaranteeUnitAPI warrantyQuantityUnit = ArticleControllerTest.createValidGuaranteeUnitAPI(guaranteeUnitId+1);
        generalPricelistAPI.setWarrantyQuantityTJUnit(warrantyQuantityUnit);
        try {
            generalPricelistController.setWarrantyQuantityUnits(generalPricelist, generalPricelistAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("warrantyQuantityTJUnit", errorMessageAPI.getField());
        }
    }

    @Test
    public void testUpdateAgreement() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        GeneralPricelistAPI generalPricelistAPI = createValidGeneralPricelistAPI();
        try {
            generalPricelistController.updateGeneralPricelist(customerOrganizationId, generalPricelistAPI, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    }

    @Test
    public void testUpdateAgreementNODeliveryTimes() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        GeneralPricelistAPI generalPricelistAPI = createValidGeneralPricelistAPI();
        generalPricelistAPI.setDeliveryTimeH(null);
        try {
            generalPricelistController.updateGeneralPricelist(customerOrganizationId, generalPricelistAPI, userAPI, null, null);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("deliveryTimeH", errorMessageAPI.getField());
        }
    }

    public static GeneralPricelistAPI createValidGeneralPricelistAPI() {
        GeneralPricelistAPI generalPricelistAPI = new GeneralPricelistAPI();
        generalPricelistAPI.setId(generalPricelistId);
        generalPricelistAPI.setGeneralPricelistName(generalPricelistName);
        generalPricelistAPI.setGeneralPricelistNumber(generalPricelistNumber);
        Calendar validFrom = Calendar.getInstance();
        generalPricelistAPI.setValidFrom(validFrom.getTimeInMillis());
        generalPricelistAPI.setValidTo(validFrom.getTimeInMillis() + 1000000);
        generalPricelistAPI.setOwnerOrganization(OrganizationControllerTest.createValidOrganizationAPI(supplierOrganizationId, Organization.OrganizationType.SUPPLIER, System.currentTimeMillis()));
        generalPricelistAPI.setDeliveryTimeH(1);
        generalPricelistAPI.setDeliveryTimeI(1);
        generalPricelistAPI.setDeliveryTimeR(1);
        generalPricelistAPI.setDeliveryTimeT(1);
        generalPricelistAPI.setDeliveryTimeTJ(1);
        return generalPricelistAPI;
    }

    public static GeneralPricelist createValidGeneralPricelist() {
        GeneralPricelist generalPricelist = new GeneralPricelist();
        generalPricelist.setUniqueId(generalPricelistId);
        generalPricelist.setGeneralPricelistName(generalPricelistName);
        generalPricelist.setGeneralPricelistNumber(generalPricelistNumber);
        generalPricelist.setValidFrom(new Date(System.currentTimeMillis()));
        generalPricelist.setValidTo(new Date(System.currentTimeMillis() + 1000000));
        generalPricelist.setOwnerOrganization(OrganizationControllerTest.createValidOrganization(supplierOrganizationId, Organization.OrganizationType.CUSTOMER, new Date(), null, null));
        return generalPricelist;
    }

}
