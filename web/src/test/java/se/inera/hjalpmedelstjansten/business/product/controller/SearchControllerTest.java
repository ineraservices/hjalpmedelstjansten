package se.inera.hjalpmedelstjansten.business.product.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Category;

import jakarta.ws.rs.core.MultivaluedMap;

public class SearchControllerTest {

    SearchController searchController;
    ElasticSearchController elasticSearchController;
    CategoryController categoryController;

    final long validCategoryId = 1l;
    final long invalidCategoryId = 2l;

    @Before
    public void init() {
        searchController = new SearchController();

        elasticSearchController = Mockito.mock(ElasticSearchController.class);
        searchController.elasticSearchController = elasticSearchController;

        categoryController = Mockito.mock(CategoryController.class);
        Category category = CategoryControllerTest.createValidCategory(validCategoryId);
        Mockito.when(categoryController.getById(validCategoryId)).thenReturn(category);
        searchController.categoryController = categoryController;

        ValidationMessageService validationMessageService = Mockito.mock(ValidationMessageService.class);
        Mockito.when(validationMessageService.generateValidationException(Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer<HjalpmedelstjanstenValidationException>() {
            @Override
            public HjalpmedelstjanstenValidationException answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException((String) args[0] + "->" + (String) args[1]);
                exception.addValidationMessage((String) args[0], (String) args[1]);
                return exception;
            }
        });
        Mockito.when(validationMessageService.generateValidationException(Mockito.anyString(), Mockito.anyString(), Mockito.any())).thenAnswer(new Answer<HjalpmedelstjanstenValidationException>() {
            @Override
            public HjalpmedelstjanstenValidationException answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException((String) args[0] + "->" + (String) args[1]);
                exception.addValidationMessage((String) args[0], (String) args[1]);
                return exception;
            }
        });
        searchController.validationMessageService = validationMessageService;

        searchController.LOG = new NoLogger();

    }

    @Test
    public void testSearchProductsAndArticlesWithNothing() {
        MultivaluedMap multivaluedMap = Mockito.mock(MultivaluedMap.class);
        try {
            SearchDTO searchDTO = searchController.searchProductsAndArticles(multivaluedMap, 25, "", 0, "");
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Search failed but should be successful");
        }
    }

    @Test
    public void testSearchProductsAndArticlesWithOkCategory() {
        MultivaluedMap multivaluedMap = Mockito.mock(MultivaluedMap.class);
        Mockito.when(multivaluedMap.getFirst("category")).thenReturn(""+validCategoryId);
        try {
            SearchDTO searchDTO = searchController.searchProductsAndArticles(multivaluedMap, 25, "", 0, "");
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Search failed but should be successful");
        }
    }

    @Test
    public void testSearchProductsAndArticlesWithInvalidCategory() {
        MultivaluedMap multivaluedMap = Mockito.mock(MultivaluedMap.class);
        Mockito.when(multivaluedMap.getFirst("category")).thenReturn(""+invalidCategoryId);
        try {
            SearchDTO searchDTO = searchController.searchProductsAndArticles(multivaluedMap, 25, "", 0, "");
            Assert.fail("Search successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("category", errorMessageAPI.getField());
        }
    }

}
