package se.inera.hjalpmedelstjansten.business.agreement.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationControllerTest;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleControllerTest;
import se.inera.hjalpmedelstjansten.business.product.controller.CVPreventiveMaintenanceController;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.UserAPITest;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelist;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import jakarta.enterprise.event.Event;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class AgreementPricelistRowControllerTest {

    UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationIdSupplier, null, UserRole.RoleName.SupplierAgreementManager, userEngagementId);
    AgreementPricelistController pricelistController;
    OrganizationController organizationController;
    EntityManager myEntityManager;
    AgreementPricelistRowValidation pricelistRowValidation;
    ArticleController articleController;
    GuaranteeUnitController guaranteeUnitController;
    Event<InternalAuditEvent> event;
    CVPreventiveMaintenanceController maintenanceController = new CVPreventiveMaintenanceController();
    AgreementController agreementController;
    AgreementPricelistRowController pricelistRowController;


    static final long pricelistRowId = 1;
    static final long pricelistId = 2;
    static final long userId = 3;
    static final long organizationIdSupplier = 4;
    static final long agreementId = 5;
    static final long articleId = 6;
    static final long discontinuedArticleId = 7;
    static final long pricelistRowWithoutPriceId = 8;
    static final long pricelistRowWithStatusPendingApprovalId = 9;
    static final long pricelistRowWithAgreementStatusDiscontinuedId = 10;
    static final long pricelistRowWithStatusDeclinedId = 11;
    static final long pricelistRowWithStatusActiveId = 12;
    static final long pricelistRowPendingApprovalWithAgreementStatusDiscontinuedId = 13;
    static final long pricelistRowActiveWithAgreementStatusDiscontinuedId = 14;
    static final long pricelistRowWithStatusPendingInactivationId = 15;
    static final long pricelistRowPendingInactivationWithAgreementStatusDiscontinuedId = 16;
    static final long guaranteeUnitId = 17;
    static final long userEngagementId = 18;
    static final long organizationIdCustomer = 19;

    static final int leastOrderQuantity = 1;
    static final String status = AgreementPricelistRow.Status.CREATED.toString();
    static final BigDecimal price = BigDecimal.ONE;

    @Before
    public void init() {


        //Mockito.when(myEntityManager.find(Agreement.class, anyLong())).thenReturn(AgreementControllerTest.createValidAgreement(100L));

        pricelistRowController = new AgreementPricelistRowController();
        pricelistRowController.LOG = new NoLogger();

        myEntityManager = Mockito.mock(EntityManager.class);

        event = Mockito.mock(Event.class);
        pricelistRowController.internalAuditEvent = event;

        // row with status created
        Mockito.when(myEntityManager.find(AgreementPricelistRow.class, pricelistRowId)).thenReturn(createValidPricelistRow(pricelistRowId, pricelistId, agreementId, organizationIdSupplier, AgreementPricelistRow.Status.CREATED));

        // row with status declined
        Mockito.when(myEntityManager.find(AgreementPricelistRow.class, pricelistRowWithStatusDeclinedId)).thenReturn(createValidPricelistRow(pricelistRowWithStatusDeclinedId, pricelistId, agreementId, organizationIdSupplier, AgreementPricelistRow.Status.DECLINED));

        Mockito.when(myEntityManager.find(Agreement.class, agreementId)).thenReturn(AgreementControllerTest.createValidAgreement(100L));

        // row with no price
        AgreementPricelistRow pricelistRowWithoutPrice = createValidPricelistRow(pricelistRowId, pricelistId, agreementId, organizationIdSupplier, AgreementPricelistRow.Status.CREATED);
        pricelistRowWithoutPrice.setPrice(null);
        Mockito.when(myEntityManager.find(AgreementPricelistRow.class, pricelistRowWithoutPriceId)).thenReturn(pricelistRowWithoutPrice);

        // row with status pending approval
        AgreementPricelistRow pricelistRowWithStatusPendingApproval = createValidPricelistRow(pricelistRowWithStatusPendingApprovalId, pricelistId, agreementId, organizationIdSupplier, AgreementPricelistRow.Status.PENDING_APPROVAL);
        Mockito.when(myEntityManager.find(AgreementPricelistRow.class, pricelistRowWithStatusPendingApprovalId)).thenReturn(pricelistRowWithStatusPendingApproval);

        // row with status active
        AgreementPricelistRow pricelistRowWithStatusActive = createValidPricelistRow(pricelistRowWithStatusActiveId, pricelistId, agreementId, organizationIdSupplier, AgreementPricelistRow.Status.ACTIVE);
        Mockito.when(myEntityManager.find(AgreementPricelistRow.class, pricelistRowWithStatusActiveId)).thenReturn(pricelistRowWithStatusActive);

        // row with status pending inactivation
        AgreementPricelistRow pricelistRowWithStatusPendingInactivation = createValidPricelistRow(pricelistRowWithStatusPendingInactivationId, pricelistId, agreementId, organizationIdSupplier, AgreementPricelistRow.Status.PENDING_INACTIVATION);
        Mockito.when(myEntityManager.find(AgreementPricelistRow.class, pricelistRowWithStatusPendingInactivationId)).thenReturn(pricelistRowWithStatusPendingInactivation);


        // row with status created agreement status discontinued
        AgreementPricelistRow pricelistRowWithAgreementStatusDiscontinued = createValidPricelistRow(pricelistRowWithStatusPendingApprovalId, pricelistId, agreementId, organizationIdSupplier, AgreementPricelistRow.Status.CREATED);
        pricelistRowWithAgreementStatusDiscontinued.getPricelist().getAgreement().setStatus(Agreement.Status.DISCONTINUED);
        Mockito.when(myEntityManager.find(AgreementPricelistRow.class, pricelistRowWithAgreementStatusDiscontinuedId)).thenReturn(pricelistRowWithAgreementStatusDiscontinued);

        // row with status pending approval agreement status discontinued
        AgreementPricelistRow pricelistRowPendingApprovalWithAgreementStatusDiscontinued = createValidPricelistRow(pricelistRowPendingApprovalWithAgreementStatusDiscontinuedId, pricelistId, agreementId, organizationIdSupplier, AgreementPricelistRow.Status.PENDING_APPROVAL);
        pricelistRowPendingApprovalWithAgreementStatusDiscontinued.getPricelist().getAgreement().setStatus(Agreement.Status.DISCONTINUED);
        Mockito.when(myEntityManager.find(AgreementPricelistRow.class, pricelistRowPendingApprovalWithAgreementStatusDiscontinuedId)).thenReturn(pricelistRowPendingApprovalWithAgreementStatusDiscontinued);

        // row with status active agreement status discontinued
        AgreementPricelistRow pricelistRowActiveWithAgreementStatusDiscontinued = createValidPricelistRow(pricelistRowActiveWithAgreementStatusDiscontinuedId, pricelistId, agreementId, organizationIdSupplier, AgreementPricelistRow.Status.ACTIVE);
        pricelistRowActiveWithAgreementStatusDiscontinued.getPricelist().getAgreement().setStatus(Agreement.Status.DISCONTINUED);
        Mockito.when(myEntityManager.find(AgreementPricelistRow.class, pricelistRowActiveWithAgreementStatusDiscontinuedId)).thenReturn(pricelistRowActiveWithAgreementStatusDiscontinued);

        // row with status pending inactivation agreement status discontinued
        AgreementPricelistRow pricelistRowPendingInactivationWithAgreementStatusDiscontinued = createValidPricelistRow(pricelistRowPendingInactivationWithAgreementStatusDiscontinuedId, pricelistId, agreementId, organizationIdSupplier, AgreementPricelistRow.Status.PENDING_INACTIVATION);
        pricelistRowPendingInactivationWithAgreementStatusDiscontinued.getPricelist().getAgreement().setStatus(Agreement.Status.DISCONTINUED);
        Mockito.when(myEntityManager.find(AgreementPricelistRow.class, pricelistRowPendingInactivationWithAgreementStatusDiscontinuedId)).thenReturn(pricelistRowPendingInactivationWithAgreementStatusDiscontinued);


        Query mockQuery = Mockito.mock(Query.class);
        Mockito.when(mockQuery.setParameter(Mockito.anyString(), Mockito.any())).thenReturn(mockQuery);
        Mockito.when(myEntityManager.createNamedQuery(AgreementPricelistRow.FIND_BY_PRICELIST_AND_ARTICLE)).thenReturn(mockQuery);
        Query mockQueryArticleIds = Mockito.mock(Query.class);
        Mockito.when(myEntityManager.createNamedQuery(AgreementPricelistRow.GET_ARTICLE_UNIQUE_IDS)).thenReturn(mockQueryArticleIds);
        Mockito.when(mockQueryArticleIds.setParameter(Mockito.anyString(), Mockito.any())).thenReturn(mockQueryArticleIds);
        Mockito.when(mockQueryArticleIds.getResultList()).thenReturn(new ArrayList());
        pricelistRowController.em = myEntityManager;

        pricelistController = Mockito.mock(AgreementPricelistController.class);
        AgreementPricelist currentPricelist = AgreementPricelistControllerTest.createValidPricelist(pricelistId, null, new Date(), new Date(), agreementId, organizationIdSupplier);
        Mockito.when(pricelistController.getCurrentPricelist(agreementId)).thenReturn(currentPricelist);
        Mockito.when(pricelistController.getPricelist(organizationIdSupplier, agreementId, pricelistId, userAPI)).thenReturn(currentPricelist);
        pricelistRowController.pricelistController = pricelistController;

        organizationController = Mockito.mock(OrganizationController.class);
        Mockito.when(organizationController.getOrganization(organizationIdSupplier)).thenReturn(OrganizationControllerTest.createValidOrganization(organizationIdSupplier, Organization.OrganizationType.SUPPLIER, null, null, null));
        pricelistRowController.organizationController = organizationController;

        pricelistRowValidation = Mockito.mock(AgreementPricelistRowValidation.class);
        pricelistRowController.pricelistRowValidation = pricelistRowValidation;

        articleController = Mockito.mock(ArticleController.class);
        Mockito.when(articleController.getArticle(organizationIdSupplier, articleId, userAPI)).thenReturn(ArticleControllerTest.createValidArticle(articleId, organizationIdSupplier));
        Mockito.when(articleController.getArticle(organizationIdSupplier, discontinuedArticleId, userAPI)).thenReturn(ArticleControllerTest.createValidArticle(articleId, organizationIdSupplier, Product.Status.DISCONTINUED));
        pricelistRowController.articleController = articleController;

        ValidationMessageService validationMessageService = Mockito.mock(ValidationMessageService.class);
        Mockito.when(validationMessageService.generateValidationException(Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer<HjalpmedelstjanstenValidationException>() {
            @Override
            public HjalpmedelstjanstenValidationException answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException((String) args[0] + "->" + (String) args[1]);
                exception.addValidationMessage((String) args[0], (String) args[1]);
                return exception;
            }
        });
        pricelistRowController.validationMessageService = validationMessageService;

        guaranteeUnitController = Mockito.mock(GuaranteeUnitController.class);
        Mockito.when(guaranteeUnitController.getGuaranteeUnit(guaranteeUnitId)).thenReturn(ArticleControllerTest.createValidGuaranteeUnit(guaranteeUnitId));
        pricelistRowController.guaranteeUnitController = guaranteeUnitController;

        agreementController = Mockito.mock(AgreementController.class);
        Agreement agreement = AgreementControllerTest.createValidAgreement(null, userEngagementId, organizationIdSupplier, organizationIdCustomer);
        Mockito.when(agreementController.getAgreement(organizationIdSupplier, agreementId, userAPI)).thenReturn(agreement);
        pricelistRowController.agreementController = agreementController;
    }

    @Test
    public void testGetPricelistRow() {
        AgreementPricelistRow pricelistRow = pricelistRowController.getPricelistRow(organizationIdSupplier, agreementId, pricelistId, pricelistRowId, userAPI);
        Assert.assertNotNull(pricelistRow);
    }

    @Test
    public void testGetPricelistRowWrongAgreementUniqueId() {
        AgreementPricelistRow pricelistRow = pricelistRowController.getPricelistRow(organizationIdSupplier, 1000000, pricelistId, pricelistRowId, userAPI);
        Assert.assertNull(pricelistRow);
    }

    @Test
    public void testGetPricelistWrongPricelistUniqueId() {
        AgreementPricelistRow pricelistRow = pricelistRowController.getPricelistRow(organizationIdSupplier, agreementId, 1000000, pricelistRowId, userAPI);
        Assert.assertNull(pricelistRow);
    }

    @Test
    public void testGetPricelistWrongPricelistRowUniqueId() {
        AgreementPricelistRow pricelistRow = pricelistRowController.getPricelistRow(organizationIdSupplier, agreementId, pricelistId, 1000000, userAPI);
        Assert.assertNull(pricelistRow);
    }

    @Test
    public void testCreateValidPricelistRow() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> agreementPricelistRowAPIs = new ArrayList<>();
        agreementPricelistRowAPIs.add(agreementPricelistRowAPI);
        try {
            agreementPricelistRowAPIs = pricelistRowController.createPricelistRows(organizationIdSupplier, agreementId, pricelistId, agreementPricelistRowAPIs, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
        Assert.assertNotNull(agreementPricelistRowAPIs);
    }

    //@Test
    public void testCreateInvalidPricelistRowWrongArticleId() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, status, price, 1000000);
        List<AgreementPricelistRowAPI> agreementPricelistRowAPIs = new ArrayList<>();
        agreementPricelistRowAPIs.add(agreementPricelistRowAPI);
        try {
            agreementPricelistRowAPIs = pricelistRowController.createPricelistRows(organizationIdSupplier, agreementId, pricelistId, agreementPricelistRowAPIs, userAPI, null, null);
            Assert.fail("Exception not thrown, but should have been");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("article", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("pricelistrow.article.notExist"));
        }
    }

    @Test
    public void testCreateInvalidPricelistRowWrongPricelistId() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> agreementPricelistRowAPIs = new ArrayList<>();
        agreementPricelistRowAPIs.add(agreementPricelistRowAPI);
        try {
            agreementPricelistRowAPIs = pricelistRowController.createPricelistRows(organizationIdSupplier, agreementId, 1000000, agreementPricelistRowAPIs, userAPI, null, null);
            Assert.fail("Exception not thrown, but should have been");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("pricelist", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("pricelistrow.pricelist.notNull"));
        }
    }
    /*
    @Test
    public void testCreateInvalidPricelistRowDiscontinuedArticle() { // This test is no longer relevant as of 20190225 - the story has changed.
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, status, price, discontinuedArticleId);
        List<AgreementPricelistRowAPI> agreementPricelistRowAPIs = new ArrayList<>();
        agreementPricelistRowAPIs.add(agreementPricelistRowAPI);
        try {
            agreementPricelistRowAPIs = pricelistRowController.createPricelistRows(organizationIdSupplier, agreementId, pricelistId, agreementPricelistRowAPIs, userAPI, null, null);
            //Assert.fail("Exception not thrown, but should have been");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("article", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("pricelistrow.article.discontinued"));
        }
    }
    */

    //@Test
    public void testUpdateValidPricelistRow() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, status, price, articleId);
        try {
            agreementPricelistRowAPI = pricelistRowController.updatePricelistRow(organizationIdSupplier, agreementId, pricelistId, pricelistRowId, agreementPricelistRowAPI, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
        Assert.assertNotNull(agreementPricelistRowAPI);
    }

    @Test
    public void testUpdateInalidPricelistRowWrongUniqueId() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(1000000, leastOrderQuantity, status, price, articleId);
        try {
            agreementPricelistRowAPI = pricelistRowController.updatePricelistRow(organizationIdSupplier, agreementId, pricelistId, 1000000, agreementPricelistRowAPI, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
        Assert.assertNull(agreementPricelistRowAPI);
    }

    @Test
    public void testSendRowsForCustomerApprovalCreated() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.sendRowsForCustomerApproval(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
        Assert.assertNotNull(rows);
    }

    @Test
    public void testSendRowsForCustomerApprovalDeclined() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowWithStatusDeclinedId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.sendRowsForCustomerApproval(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
        Assert.assertNotNull(rows);
    }

    @Test
    public void testSendRowsForCustomerApprovalNoPrice() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowWithoutPriceId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.sendRowsForCustomerApproval(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
            Assert.fail();
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("price", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("pricelistrow.sendRowsForCustomerApproval.noPrice"));
        }
    }

    @Test
    public void testSendRowsForCustomerApprovalWrongRowStatus() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowWithStatusPendingApprovalId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.sendRowsForCustomerApproval(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
            Assert.fail();
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("status", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("pricelistrow.sendRowsForCustomerApproval.wrongStatus"));
        }
    }

    @Test
    public void testSendRowsForCustomerApprovalWrongAgreementStatus() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowWithAgreementStatusDiscontinuedId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.sendRowsForCustomerApproval(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
            Assert.fail();
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("status", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("agreement.status.discontinued"));
        }
    }

    @Test
    public void testDeclineRowsPendingApproval() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowWithStatusPendingApprovalId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.declineRows(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        } catch (HjalpmedelstjanstenException ex) {
            Assert.fail();
        }
        Assert.assertNotNull(rows);
    }

    @Test
    public void testDeclineRowsActive() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowWithStatusActiveId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.declineRows(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        } catch (HjalpmedelstjanstenException ex) {
            Assert.fail();
        }
        Assert.assertNotNull(rows);
    }

    @Test
    public void testDeclineRowsWrongStatus() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.declineRows(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
            Assert.fail();
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("status", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("pricelistrow.declineRows.wrongStatus"));
        } catch (HjalpmedelstjanstenException ex) {
            Assert.fail();
        }
    }

    @Test
    public void testDeclineRowsWrongAgreementStatus() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowPendingApprovalWithAgreementStatusDiscontinuedId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.sendRowsForCustomerApproval(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
            Assert.fail();
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("status", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("agreement.status.discontinued"));
        }
    }

    @Test
    public void testActivateRowsPendingApproval() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowWithStatusPendingApprovalId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.activateRows(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        } catch (HjalpmedelstjanstenException ex) {
            Assert.fail();
        }
        Assert.assertNotNull(rows);
    }

    @Test
    public void testActivateRowsWrongStatus() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.activateRows(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
            Assert.fail();
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("status", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("pricelistrow.activateRows.wrongStatus"));
        } catch (HjalpmedelstjanstenException ex) {
            Assert.fail();
        }
    }

    @Test
    public void testActivateRowsWrongAgreementStatus() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowPendingApprovalWithAgreementStatusDiscontinuedId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.activateRows(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
            Assert.fail();
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("status", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("agreement.status.discontinued"));
        } catch (HjalpmedelstjanstenException ex) {
            Assert.fail();
        }
    }


    /*
    @Test
    public void testInactivateRowsActive() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowWithStatusActiveId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.inactivateRows(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
        Assert.assertNotNull(rows);
    }

     */









    @Test
    public void testInactivateRowsWrongStatus() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.inactivateRows(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
            Assert.fail();
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("status", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("pricelistrow.inactivateRows.wrongStatus"));
        }
    }

    @Test
    public void testInactivateRowsWrongAgreementStatus() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowActiveWithAgreementStatusDiscontinuedId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.inactivateRows(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
            Assert.fail();
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("status", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("agreement.status.discontinued"));
        }
    }

    @Test
    public void testApproveInactivateRowsPendingInactivation() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowWithStatusPendingInactivationId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.approveInactivateRows(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        } catch (HjalpmedelstjanstenException ex) {
            Assert.fail();
        }
        Assert.assertNotNull(rows);
    }

    @Test
    public void testApproveInactivateRowsWrongStatus() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.approveInactivateRows(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
            Assert.fail();
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("status", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("pricelistrow.approveInactivateRows.wrongStatus"));
        } catch (HjalpmedelstjanstenException ex) {
            Assert.fail();
        }
    }

    @Test
    public void testApproveInactivateRowsWrongAgreementStatus() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowPendingInactivationWithAgreementStatusDiscontinuedId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.approveInactivateRows(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
            Assert.fail();
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("status", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("agreement.status.discontinued"));
        } catch (HjalpmedelstjanstenException ex) {
            Assert.fail();
        }
    }

    @Test
    public void testDeclineInactivateRowsPendingInactivation() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowWithStatusPendingInactivationId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.declineInactivateRows(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        } catch (HjalpmedelstjanstenException ex) {
            Assert.fail();
        }
        Assert.assertNotNull(rows);
    }

    @Test
    public void testDeclineInactivateRowsWrongStatus() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.declineInactivateRows(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
            Assert.fail();
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("status", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("pricelistrow.declineInactivateRows.wrongStatus"));
        } catch (HjalpmedelstjanstenException ex) {
            Assert.fail();
        }
    }

    @Test
    public void testDeclineInactivateRowsWrongAgreementStatus() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowPendingInactivationWithAgreementStatusDiscontinuedId, leastOrderQuantity, status, price, articleId);
        List<AgreementPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.declineInactivateRows(organizationIdSupplier, agreementId, pricelistId, rows, userAPI, null, null);
            Assert.fail();
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("status", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("agreement.status.discontinued"));
        } catch (HjalpmedelstjanstenException ex) {
            Assert.fail();
        }
    }

    @Test
    public void testSetOverriddenWarrantyQuantityUnitOverride() {
        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, status, price, articleId);
        agreementPricelistRowAPI.setWarrantyQuantityUnit(ArticleControllerTest.createValidGuaranteeUnitAPI(guaranteeUnitId));
        AgreementPricelistRow agreementPricelistRow = createValidPricelistRow(pricelistRowId, pricelistId, agreementId, organizationIdSupplier, AgreementPricelistRow.Status.CREATED);
        try {
            pricelistRowController.setOverriddenWarrantyQuantityUnit(agreementPricelistRow, agreementPricelistRowAPI, false);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
        Assert.assertNotNull(agreementPricelistRow.getWarrantyQuantityUnit());
        Assert.assertTrue(agreementPricelistRow.isWarrantyQuantityUnitOverridden());
    }

    @Test
    public void testSetOverriddenWarrantyQuantityUnitNotOverride() {

        Agreement agreement = AgreementControllerTest.createValidAgreement(agreementId);
        agreement.setWarrantyQuantityTUnit(ArticleControllerTest.createValidGuaranteeUnit(guaranteeUnitId));

        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, status, price, articleId);
        agreementPricelistRowAPI.setWarrantyQuantityUnit(ArticleControllerTest.createValidGuaranteeUnitAPI(guaranteeUnitId));
        AgreementPricelistRow agreementPricelistRow = createValidPricelistRow(pricelistRowId, pricelistId, agreementId, organizationIdSupplier, AgreementPricelistRow.Status.CREATED);
        agreementPricelistRow.getPricelist().getAgreement().setWarrantyQuantityTUnit(ArticleControllerTest.createValidGuaranteeUnit(guaranteeUnitId));

        Mockito.when(myEntityManager.find(Agreement.class, agreementId)).thenReturn(agreement);

        try {
            pricelistRowController.setOverriddenWarrantyQuantityUnit(agreementPricelistRow, agreementPricelistRowAPI, false);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
        Assert.assertNull(agreementPricelistRow.getWarrantyQuantityUnit());
        Assert.assertFalse(agreementPricelistRow.isWarrantyQuantityUnitOverridden());
    }

    @Test
    public void testSetOverriddenWarrantyQuantityUnitOverrideWithNull() {

        Agreement agreement = AgreementControllerTest.createValidAgreement(agreementId);
        agreement.setWarrantyQuantityTUnit(ArticleControllerTest.createValidGuaranteeUnit(guaranteeUnitId));
        //agreement.setWarrantyQuantityT(null);
        //Override previous mock. Making sure that the Agreement returning will contain no warrantyQuantity object
        Mockito.when(myEntityManager.find(Agreement.class, agreementId)).thenReturn(agreement);

        AgreementPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, status, price, articleId);
        AgreementPricelistRow agreementPricelistRow = createValidPricelistRow(pricelistRowId, pricelistId, agreementId, organizationIdSupplier, AgreementPricelistRow.Status.CREATED);
        agreementPricelistRow.getPricelist().getAgreement().setWarrantyQuantityTUnit(null);
        try {
            pricelistRowController.setOverriddenWarrantyQuantityUnit(agreementPricelistRow, agreementPricelistRowAPI, false);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }

        Assert.assertNull(agreementPricelistRow.getWarrantyQuantityUnit());
        Assert.assertTrue(agreementPricelistRow.isWarrantyQuantityUnitOverridden());
    }

    public static AgreementPricelistRowAPI createValidPricelistRowAPI(long id, int leastOrderQuantity, String status, BigDecimal price, long articleId) {
        AgreementPricelistRowAPI pricelistRowAPI = new AgreementPricelistRowAPI();
        pricelistRowAPI.setId(id);
        pricelistRowAPI.setLeastOrderQuantity(leastOrderQuantity);
        pricelistRowAPI.setStatus(status);
        pricelistRowAPI.setPrice(price);
        pricelistRowAPI.setDeliveryTime(1);
        ArticleAPI articleAPI = new ArticleAPI();
        articleAPI.setId(articleId);
        pricelistRowAPI.setArticle(articleAPI);
        return pricelistRowAPI;
    }

    public static AgreementPricelistRow createValidPricelistRow(long pricelistRowId, long pricelistId, long agreementId, long organizationId, AgreementPricelistRow.Status status) {
        AgreementPricelistRow agreementPricelistRow = new AgreementPricelistRow();
        agreementPricelistRow.setPrice(price);
        agreementPricelistRow.setUniqueId(pricelistRowId);
        agreementPricelistRow.setStatus(status);
        agreementPricelistRow.setPricelist(AgreementPricelistControllerTest.createValidPricelist(pricelistId, null, new Date(), null, agreementId, organizationId));
        agreementPricelistRow.setArticle(ArticleControllerTest.createValidArticle(articleId, organizationId));
        return agreementPricelistRow;
    }

}
