package se.inera.hjalpmedelstjansten.business.product.controller;

import org.junit.Assert;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;


public class ArticleMapperTest {
    
    static long articleId = 1;
    static long productId = 2;
    static long organizationId = 3;
    
    static String preventiveMaintenanceUnitCode = "code";
    static String preventiveMaintenanceUnitName = "name";
    static String articlePreventiveMaintenanceDescription = "articlePreventiveMaintenanceDescription";
    static int articlePreventiveMaintenanceNumberOfDays = 1;
    static CVPreventiveMaintenance articlePreventiveMaintenanceValidFrom = PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenance(preventiveMaintenanceUnitCode, preventiveMaintenanceUnitName);
    static String articleCeDirectiveName = "articleCeDirectiveName";
    static String articleCeStandardName = "articleCeStandardName";
    static boolean articleCeMarked = true;
    static String articleManufacturer = "articleManufacturer";
    static String articleManufacturerArticleNumber = "articleManufacturerArticleNumber";
    static String articleManufacturerWeb = "articleManufacturerWeb";
    static String articleTrademark = "articleTrademark";
    static String articleSupplementedInformation = "articleSupplementedInformation";
    static Article.Type articleCategoryArticleType = Article.Type.H;
    static String articleCategoryCode = "articleCategoryCode";
    static String articleCategoryName = "articleCategoryName";
    
    static String productPreventiveMaintenanceDescription = "productPreventiveMaintenanceDescription";
    static int productPreventiveMaintenanceNumberOfDays = 2;
    static CVPreventiveMaintenance productPreventiveMaintenanceValidFrom = PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenance(preventiveMaintenanceUnitCode, preventiveMaintenanceUnitName);
    static String productCeDirectiveName = "productCeDirectiveName";
    static String productCeStandardName = "productCeStandardName";
    static boolean productCeMarked = false;
    static String productManufacturer = "productManufacturer";
    static String productManufacturerProductNumber = "productManufacturerProductNumber";
    static String productManufacturerWeb = "productManufacturerWeb";
    static String productTrademark = "productTrademark";
    static String productSupplementedInformation = "productSupplementedInformation";
    static Article.Type productCategoryArticleType = Article.Type.H;
    static String productCategoryCode = "productCategoryCode";
    static String productCategoryName = "productCategoryName";
    
    /**
     * A published article uses inherited values
     */
    @Test
    public void testMapPublishedArticleWithOverridden() {
        Article article = ArticleControllerTest.createValidArticle(articleId, organizationId);
        article.setStatus(Product.Status.PUBLISHED);
        article.setPreventiveMaintenanceDescription(articlePreventiveMaintenanceDescription);
        article.setPreventiveMaintenanceDescriptionOverridden(true);
        article.setPreventiveMaintenanceNumberOfDays(articlePreventiveMaintenanceNumberOfDays);
        article.setPreventiveMaintenanceNumberOfDaysOverridden(true);
        article.setPreventiveMaintenanceValidFrom(articlePreventiveMaintenanceValidFrom);
        article.setPreventiveMaintenanceValidFromOverridden(true);
        article.getCeDirective().setName(articleCeDirectiveName);
        article.setCeDirectiveOverridden(true);
        article.getCeStandard().setName(articleCeStandardName);
        article.setCeStandardOverridden(true);
        article.setCeMarked(articleCeMarked);
        article.setCeMarkedOverridden(true);
        article.setManufacturer(articleManufacturer);
        article.setManufacturerOverridden(true);
        article.setManufacturerArticleNumber(articleManufacturerArticleNumber);
        article.setManufacturerArticleNumberOverridden(true);
        article.getManufacturerElectronicAddress().setWeb(articleManufacturerWeb);
        article.setManufacturerElectronicAddressOverridden(true);
        article.setTrademark(articleTrademark);
        article.setTrademarkOverridden(true);
        article.setOrderInformationOverridden(true);
        article.setSupplementedInformation(articleSupplementedInformation);
        article.setSupplementedInformationOverridden(true);

        Product basedOnProduct = ProductControllerTest.createValidProduct(productId, organizationId);
        basedOnProduct.setPreventiveMaintenanceDescription(productPreventiveMaintenanceDescription);
        basedOnProduct.setPreventiveMaintenanceNumberOfDays(productPreventiveMaintenanceNumberOfDays);
        basedOnProduct.setPreventiveMaintenanceValidFrom(productPreventiveMaintenanceValidFrom);
        basedOnProduct.getCeDirective().setName(productCeDirectiveName);
        basedOnProduct.getCeStandard().setName(productCeStandardName);
        basedOnProduct.setCeMarked(productCeMarked);
        basedOnProduct.setManufacturer(productManufacturer);
        basedOnProduct.setManufacturerProductNumber(productManufacturerProductNumber);
        basedOnProduct.getManufacturerElectronicAddress().setWeb(productManufacturerWeb);
        basedOnProduct.setTrademark(productTrademark);
        basedOnProduct.setSupplementedInformation(productSupplementedInformation);
        article.setBasedOnProduct(basedOnProduct);

        
        ArticleAPI articleAPI = ArticleMapper.map(article, true, null);
        Assert.assertEquals(articleId, articleAPI.getId().longValue());
        Assert.assertEquals(articlePreventiveMaintenanceDescription, articleAPI.getPreventiveMaintenanceDescription());
        Assert.assertEquals(articlePreventiveMaintenanceNumberOfDays, articleAPI.getPreventiveMaintenanceNumberOfDays().longValue());
        Assert.assertEquals(articlePreventiveMaintenanceValidFrom.getCode(), articleAPI.getPreventiveMaintenanceValidFrom().getCode());
        Assert.assertEquals(articleCeMarked, articleAPI.isCeMarked());
        Assert.assertEquals(articleCeDirectiveName, articleAPI.getCeDirective().getName());
        Assert.assertEquals(articleCeStandardName, articleAPI.getCeStandard().getName());
        Assert.assertEquals(articleManufacturer, articleAPI.getManufacturer());
        Assert.assertEquals(articleManufacturerArticleNumber, articleAPI.getManufacturerArticleNumber());
        Assert.assertEquals(articleManufacturerWeb, articleAPI.getManufacturerElectronicAddress().getWeb());
        Assert.assertEquals(articleTrademark, articleAPI.getTrademark());
        Assert.assertEquals(articleSupplementedInformation, articleAPI.getSupplementedInformation());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitId, articleAPI.getOrderUnit().getId().longValue());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitCode, articleAPI.getOrderUnit().getCode());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitName, articleAPI.getOrderUnit().getName());
        Assert.assertEquals(ArticleControllerTest.articleArticleQuantityInOuterPackage, articleAPI.getArticleQuantityInOuterPackage());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitId, articleAPI.getArticleQuantityInOuterPackageUnit().getId().longValue());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitCode, articleAPI.getArticleQuantityInOuterPackageUnit().getCode());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitName, articleAPI.getArticleQuantityInOuterPackageUnit().getName());
        Assert.assertEquals(ArticleControllerTest.articlePackageContent, articleAPI.getPackageContent());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitId, articleAPI.getPackageContentUnit().getId().longValue());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitCode, articleAPI.getPackageContentUnit().getCode());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitName, articleAPI.getPackageContentUnit().getName());
        Assert.assertEquals(ArticleControllerTest.articlePackageLevelBase, articleAPI.getPackageLevelBase());
        Assert.assertEquals(ArticleControllerTest.articlePackageLevelMiddle, articleAPI.getPackageLevelMiddle());
        Assert.assertEquals(ArticleControllerTest.articlePackageLevelTop, articleAPI.getPackageLevelTop());
    }
    
    /**
     * A published article uses inherited values
     */
    @Test
    public void testMapPublishedArticleWithoutOverridden() {
        Article article = ArticleControllerTest.createValidArticle(articleId, organizationId);
        article.setStatus(Product.Status.PUBLISHED);
        article.setPreventiveMaintenanceDescription(null);
        article.setPreventiveMaintenanceDescriptionOverridden(false);
        article.setPreventiveMaintenanceNumberOfDays(null);
        article.setPreventiveMaintenanceNumberOfDaysOverridden(false);
        article.setPreventiveMaintenanceValidFrom(null);
        article.setPreventiveMaintenanceValidFromOverridden(false);
        article.setCeDirective(null);
        article.setCeDirectiveOverridden(false);
        article.setCeStandard(null);
        article.setCeStandardOverridden(false);
        article.setCeMarked(false);
        article.setCeMarkedOverridden(false);
        article.setManufacturer(null);
        article.setManufacturerOverridden(false);
        article.setManufacturerArticleNumber(null);
        article.setManufacturerArticleNumberOverridden(false);
        article.setManufacturerElectronicAddress(null);
        article.setManufacturerElectronicAddressOverridden(false);
        article.setTrademark(null);
        article.setTrademarkOverridden(false);
        article.setOrderInformationOverridden(false);
        article.setSupplementedInformation(null);
        article.setSupplementedInformationOverridden(false);
        
        Product basedOnProduct = ProductControllerTest.createValidProduct(productId, organizationId);
        basedOnProduct.setPreventiveMaintenanceDescription(productPreventiveMaintenanceDescription);
        basedOnProduct.setPreventiveMaintenanceNumberOfDays(productPreventiveMaintenanceNumberOfDays);
        basedOnProduct.setPreventiveMaintenanceValidFrom(productPreventiveMaintenanceValidFrom);
        basedOnProduct.getCeDirective().setName(productCeDirectiveName);
        basedOnProduct.getCeStandard().setName(productCeStandardName);
        basedOnProduct.setCeMarked(productCeMarked);
        basedOnProduct.setManufacturer(productManufacturer);
        basedOnProduct.setManufacturerProductNumber(productManufacturerProductNumber);
        basedOnProduct.getManufacturerElectronicAddress().setWeb(productManufacturerWeb);
        basedOnProduct.setTrademark(productTrademark);
        basedOnProduct.setSupplementedInformation(productSupplementedInformation);
        article.setBasedOnProduct(basedOnProduct);

        ArticleAPI articleAPI = ArticleMapper.map(article, true, null);
        Assert.assertEquals(articleId, articleAPI.getId().longValue());
        Assert.assertEquals(productPreventiveMaintenanceDescription, articleAPI.getPreventiveMaintenanceDescription());
        Assert.assertEquals(productPreventiveMaintenanceNumberOfDays, articleAPI.getPreventiveMaintenanceNumberOfDays().longValue());
        Assert.assertEquals(productPreventiveMaintenanceValidFrom.getCode(), articleAPI.getPreventiveMaintenanceValidFrom().getCode());
        Assert.assertEquals(productCeMarked, articleAPI.isCeMarked());
        Assert.assertEquals(productCeDirectiveName, articleAPI.getCeDirective().getName());
        Assert.assertEquals(productCeStandardName, articleAPI.getCeStandard().getName());
        Assert.assertEquals(productManufacturer, articleAPI.getManufacturer());
        Assert.assertEquals(productManufacturerProductNumber, articleAPI.getManufacturerArticleNumber());
        Assert.assertEquals(productManufacturerWeb, articleAPI.getManufacturerElectronicAddress().getWeb());
        Assert.assertEquals(productTrademark, articleAPI.getTrademark());
        Assert.assertEquals(productSupplementedInformation,articleAPI.getSupplementedInformation());
        Assert.assertEquals(ProductControllerTest.productOrderUnitId, articleAPI.getOrderUnit().getId().longValue());
        Assert.assertEquals(ProductControllerTest.productOrderUnitCode, articleAPI.getOrderUnit().getCode());
        Assert.assertEquals(ProductControllerTest.productOrderUnitName, articleAPI.getOrderUnit().getName());
        Assert.assertEquals(ProductControllerTest.productArticleQuantityInOuterPackage, articleAPI.getArticleQuantityInOuterPackage());
        Assert.assertEquals(ProductControllerTest.productOrderUnitId, articleAPI.getArticleQuantityInOuterPackageUnit().getId().longValue());
        Assert.assertEquals(ProductControllerTest.productOrderUnitCode, articleAPI.getArticleQuantityInOuterPackageUnit().getCode());
        Assert.assertEquals(ProductControllerTest.productOrderUnitName, articleAPI.getArticleQuantityInOuterPackageUnit().getName());
        Assert.assertEquals(ProductControllerTest.productPackageContent, articleAPI.getPackageContent());
        Assert.assertEquals(ProductControllerTest.productOrderUnitId, articleAPI.getPackageContentUnit().getId().longValue());
        Assert.assertEquals(ProductControllerTest.productOrderUnitCode, articleAPI.getPackageContentUnit().getCode());
        Assert.assertEquals(ProductControllerTest.productOrderUnitName, articleAPI.getPackageContentUnit().getName());
        Assert.assertEquals(ProductControllerTest.productPackageLevelBase, articleAPI.getPackageLevelBase());
        Assert.assertEquals(ProductControllerTest.productPackageLevelMiddle, articleAPI.getPackageLevelMiddle());
        Assert.assertEquals(ProductControllerTest.productPackageLevelTop, articleAPI.getPackageLevelTop());
    }
    
    /**
     * A discontinued article does no longer use inherited values
     */
    @Test
    public void testMapDiscontinuedArticleWithProductModifiedValues() {
        Article article = ArticleControllerTest.createValidArticle(articleId, organizationId);
        article.setStatus(Product.Status.DISCONTINUED);
        article.setPreventiveMaintenanceDescription(articlePreventiveMaintenanceDescription);
        article.setPreventiveMaintenanceDescriptionOverridden(false);
        article.setPreventiveMaintenanceNumberOfDays(articlePreventiveMaintenanceNumberOfDays);
        article.setPreventiveMaintenanceNumberOfDaysOverridden(false);
        article.setPreventiveMaintenanceValidFrom(articlePreventiveMaintenanceValidFrom);
        article.setPreventiveMaintenanceValidFromOverridden(false);
        article.getCeDirective().setName(articleCeDirectiveName);
        article.setCeDirectiveOverridden(false);
        article.getCeStandard().setName(articleCeStandardName);
        article.setCeStandardOverridden(false);
        article.setCeMarked(articleCeMarked);
        article.setCeMarkedOverridden(false);
        article.setManufacturer(articleManufacturer);
        article.setManufacturerOverridden(false);
        article.setManufacturerArticleNumber(articleManufacturerArticleNumber);
        article.setManufacturerArticleNumberOverridden(false);
        article.getManufacturerElectronicAddress().setWeb(articleManufacturerWeb);
        article.setManufacturerElectronicAddressOverridden(false);
        article.setTrademark(articleTrademark);
        article.setTrademarkOverridden(false);
        Category articleCategory = new Category();
        articleCategory.setArticleType(articleCategoryArticleType);
        articleCategory.setCode(articleCategoryCode);
        articleCategory.setName(articleCategoryName);
        article.setCategory(articleCategory);
        article.setOrderInformationOverridden(false);
        article.setSupplementedInformation(articleSupplementedInformation);
        article.setSupplementedInformationOverridden(false);
        
        Product basedOnProduct = ProductControllerTest.createValidProduct(productId, organizationId);
        basedOnProduct.setPreventiveMaintenanceDescription(productPreventiveMaintenanceDescription);
        basedOnProduct.setPreventiveMaintenanceNumberOfDays(productPreventiveMaintenanceNumberOfDays);
        basedOnProduct.setPreventiveMaintenanceValidFrom(productPreventiveMaintenanceValidFrom);
        basedOnProduct.getCeDirective().setName(productCeDirectiveName);
        basedOnProduct.getCeStandard().setName(productCeStandardName);
        basedOnProduct.setCeMarked(productCeMarked);
        basedOnProduct.setManufacturer(productManufacturer);
        basedOnProduct.setManufacturerProductNumber(productManufacturerProductNumber);
        basedOnProduct.getManufacturerElectronicAddress().setWeb(productManufacturerWeb);
        basedOnProduct.setTrademark(productTrademark);
        basedOnProduct.setSupplementedInformation(productSupplementedInformation);
        Category productCategory = new Category();
        productCategory.setArticleType(productCategoryArticleType);
        productCategory.setCode(productCategoryCode);
        productCategory.setName(productCategoryName);
        basedOnProduct.setCategory(productCategory);
        article.setBasedOnProduct(basedOnProduct);

        ArticleAPI articleAPI = ArticleMapper.map(article, true, null);
        Assert.assertEquals(articleId, articleAPI.getId().longValue());
        Assert.assertEquals(articlePreventiveMaintenanceDescription, articleAPI.getPreventiveMaintenanceDescription());
        Assert.assertEquals(articlePreventiveMaintenanceNumberOfDays, articleAPI.getPreventiveMaintenanceNumberOfDays().longValue());
        Assert.assertEquals(articlePreventiveMaintenanceValidFrom.getCode(), articleAPI.getPreventiveMaintenanceValidFrom().getCode());
        Assert.assertEquals(articleCeMarked, articleAPI.isCeMarked());
        Assert.assertEquals(articleCeDirectiveName, articleAPI.getCeDirective().getName());
        Assert.assertEquals(articleCeStandardName, articleAPI.getCeStandard().getName());
        Assert.assertEquals(articleManufacturer, articleAPI.getManufacturer());
        Assert.assertEquals(articleManufacturerArticleNumber, articleAPI.getManufacturerArticleNumber());
        Assert.assertEquals(articleManufacturerWeb, articleAPI.getManufacturerElectronicAddress().getWeb());
        Assert.assertEquals(articleTrademark, articleAPI.getTrademark());
        Assert.assertEquals(articleCategoryCode, article.getCategory().getCode());
        Assert.assertEquals(articleCategoryName, article.getCategory().getName());
        Assert.assertEquals(articleCategoryArticleType, article.getCategory().getArticleType());
        Assert.assertEquals(articleSupplementedInformation,article.getSupplementedInformation());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitId, articleAPI.getOrderUnit().getId().longValue());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitCode, articleAPI.getOrderUnit().getCode());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitName, articleAPI.getOrderUnit().getName());
        Assert.assertEquals(ArticleControllerTest.articleArticleQuantityInOuterPackage, articleAPI.getArticleQuantityInOuterPackage());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitId, articleAPI.getArticleQuantityInOuterPackageUnit().getId().longValue());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitCode, articleAPI.getArticleQuantityInOuterPackageUnit().getCode());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitName, articleAPI.getArticleQuantityInOuterPackageUnit().getName());
        Assert.assertEquals(ArticleControllerTest.articlePackageContent, articleAPI.getPackageContent());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitId, articleAPI.getPackageContentUnit().getId().longValue());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitCode, articleAPI.getPackageContentUnit().getCode());
        Assert.assertEquals(ArticleControllerTest.articleOrderUnitName, articleAPI.getPackageContentUnit().getName());
        Assert.assertEquals(ArticleControllerTest.articlePackageLevelBase, articleAPI.getPackageLevelBase());
        Assert.assertEquals(ArticleControllerTest.articlePackageLevelMiddle, articleAPI.getPackageLevelMiddle());
        Assert.assertEquals(ArticleControllerTest.articlePackageLevelTop, articleAPI.getPackageLevelTop());
    }
    
}
