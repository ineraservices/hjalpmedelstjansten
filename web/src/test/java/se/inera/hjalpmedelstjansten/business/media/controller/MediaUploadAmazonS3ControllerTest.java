package se.inera.hjalpmedelstjansten.business.media.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class MediaUploadAmazonS3ControllerTest {
    private final MediaUploadAmazonS3Controller mediaUploadAmazonS3Controller = new MediaUploadAmazonS3Controller();

    @BeforeEach
    public void init() {
        mediaUploadAmazonS3Controller.LOG = new NoLogger();
    }


    @Test
    void downloadMedia_Success() throws Exception {
        final var getMediaStream = MediaUploadAmazonS3Controller.class.getDeclaredMethod("getMediaStream", String.class);
        getMediaStream.setAccessible(true);

        final var mockHttpClient = mock(HttpClient.class);
        mediaUploadAmazonS3Controller.setHttpClient(mockHttpClient);

        final var mediaUrl = "https://example.com/image.jpg";

        final var mockResponse = mock(HttpResponse.class);
        when(mockHttpClient.send(any(HttpRequest.class), any())).thenReturn(mockResponse);
        when(mockResponse.statusCode()).thenReturn(200);
        when(mockResponse.body()).thenReturn(mock(InputStream.class));

        final var response = (HttpResponse<InputStream>) getMediaStream.invoke(mediaUploadAmazonS3Controller, mediaUrl);
        assertEquals(200, response.statusCode());
    }

    @Test
    void downloadMedia_Failure() throws Exception {
        final var getMediaStream = MediaUploadAmazonS3Controller.class.getDeclaredMethod("getMediaStream", String.class);
        getMediaStream.setAccessible(true);

        final var mockHttpClient = mock(HttpClient.class);
        mediaUploadAmazonS3Controller.setHttpClient(mockHttpClient);

        final var mediaUrl = "https://example.com/nonexistent.jpg";

        final var mockResponse = mock(HttpResponse.class);
        when(mockHttpClient.send(any(HttpRequest.class), any())).thenReturn(mockResponse);
        when(mockResponse.statusCode()).thenReturn(404);

        final var response = (HttpResponse<InputStream>) getMediaStream.invoke(mediaUploadAmazonS3Controller, mediaUrl);
        assertEquals(404, response.statusCode());
    }

    @Test
    void correctFilename() throws Exception {
        final Method correctFilename;
        correctFilename = MediaUploadAmazonS3Controller.class.getDeclaredMethod("correctFilename", String.class, String.class);
        correctFilename.setAccessible(true);

        assertEquals("file.pdf", correctFilename.invoke(mediaUploadAmazonS3Controller, "application/pdf", "file.jpg"));
        assertEquals("file.xlsx", correctFilename.invoke(mediaUploadAmazonS3Controller, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "file.jpg"));
        assertEquals("file.xls", correctFilename.invoke(mediaUploadAmazonS3Controller, "application/vnd.ms-excel", "file.jpg"));
        assertEquals("file.docx", correctFilename.invoke(mediaUploadAmazonS3Controller, "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "file.jpg"));
        assertEquals("file.doc", correctFilename.invoke(mediaUploadAmazonS3Controller, "application/msword", "file.jpg"));
        assertEquals("file.xls", correctFilename.invoke(mediaUploadAmazonS3Controller, "application/vnd.ms-excel", "file.jpg"));
        assertEquals("file.bin", correctFilename.invoke(mediaUploadAmazonS3Controller, "application/octet-stream", "file.unknown"));
        assertEquals("file", correctFilename.invoke(mediaUploadAmazonS3Controller, "binary/octet-stream", "file.unknown"));
    }

    @Test
    void shouldNotThrowOnSupportedContentType() throws Exception {
        final var validateSupportedContentType = MediaUploadAmazonS3Controller.class.getDeclaredMethod("validateSupportedContentType", String.class);
        validateSupportedContentType.setAccessible(true);

        assertDoesNotThrow(() -> {
            validateSupportedContentType.invoke(mediaUploadAmazonS3Controller, "application/vnd.ms-excel");
        });
        assertDoesNotThrow(() -> {
            validateSupportedContentType.invoke(mediaUploadAmazonS3Controller, "image/jpg");
        });
        assertDoesNotThrow(() -> {
            validateSupportedContentType.invoke(mediaUploadAmazonS3Controller, "binary/octet-stream");
        });
    }

    @Test
    void shouldThrowOnUnsupportedContentType() throws Exception {
        final var validateSupportedContentType = MediaUploadAmazonS3Controller.class.getDeclaredMethod("validateSupportedContentType", String.class);
        validateSupportedContentType.setAccessible(true);
        assertThrows(HjalpmedelstjanstenValidationException.class, () -> {
            try {
                validateSupportedContentType.invoke(mediaUploadAmazonS3Controller, "audio/aac");
            } catch (InvocationTargetException e) {
                throw e.getCause();
            }
        });
        assertThrows(HjalpmedelstjanstenValidationException.class, () -> {
            try {
                validateSupportedContentType.invoke(mediaUploadAmazonS3Controller, "video/x-msvideo");
            } catch (InvocationTargetException e) {
                throw e.getCause();
            }
        });
        assertThrows(HjalpmedelstjanstenValidationException.class, () -> {
            try {
                validateSupportedContentType.invoke(mediaUploadAmazonS3Controller, "binary/other");
            } catch (InvocationTargetException e) {
                throw e.getCause();
            }
        });
    }
}
