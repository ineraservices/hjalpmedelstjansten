package se.inera.hjalpmedelstjansten.business.assortment.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationControllerTest;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserControllerTest;
import se.inera.hjalpmedelstjansten.model.api.AssortmentAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountyAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVMunicipalityAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.UserAPITest;
import se.inera.hjalpmedelstjansten.model.entity.Assortment;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCounty;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVMunicipality;

import jakarta.enterprise.event.Event;
import jakarta.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class AssortmentControllerTest {

    AssortmentController assortmentController;
    EntityManager myEntityManager;
    OrganizationController organizationController;
    UserController userController;
    CountyController countyController;
    Event<InternalAuditEvent> event;
    AssortmentValidation assortmentValidation;

    static UserAPI customerAssortmentUserAPI;
    static UserAPI customerAssignedAssortmentUserAPI;
    static UserAPI superAdminUserAPI;
    static CVMunicipality municipality;
    static CVMunicipality municipalityOtherCounty;

    static final long assortmentId = 1;
    static final long customerOrganizationId = 2;
    static final long customerAssortmentUserId = 3;
    static final long supplierOrganizationId = 4;
    static final long customerAssignedAssortmentUserId = 5;
    static final long superAdminUserId = 6;
    static final long superAdminOrganizationId = 7;
    static final long countyId = 8;
    static final long municipalityId = 9;
    static final long municipalityOtherCountyId = 10;
    static final String assortmentName = "assortmentName";

    @Before
    public void init() {
        customerAssortmentUserAPI = UserAPITest.createValidUserAPI(customerAssortmentUserId, customerOrganizationId, null, UserRole.RoleName.CustomerAssortmentManager, null);
        customerAssignedAssortmentUserAPI = UserAPITest.createValidUserAPI(customerAssignedAssortmentUserId, customerOrganizationId, null, UserRole.RoleName.CustomerAssignedAssortmentManager, null);
        superAdminUserAPI = UserAPITest.createValidUserAPI(superAdminUserId, superAdminOrganizationId, null, UserRole.RoleName.Superadmin, null);

        assortmentController = new AssortmentController();
        assortmentController.LOG = new NoLogger();

        event = Mockito.mock(Event.class);
        assortmentController.internalAuditEvent = event;

        myEntityManager = Mockito.mock(EntityManager.class);
        Mockito.when(myEntityManager.find(Assortment.class, assortmentId)).thenReturn(createValidAssortment(customerOrganizationId, assortmentId, assortmentName));
        assortmentController.em = myEntityManager;

        assortmentValidation = Mockito.mock(AssortmentValidation.class);
        assortmentController.assortmentValidation = assortmentValidation;

        organizationController = Mockito.mock(OrganizationController.class);

        Mockito.when(organizationController.getOrganization(customerOrganizationId)).thenReturn(OrganizationControllerTest.createValidOrganization(customerOrganizationId, Organization.OrganizationType.CUSTOMER, new Date(), null, null));
        Mockito.when(organizationController.getOrganization(supplierOrganizationId)).thenReturn(OrganizationControllerTest.createValidOrganization(supplierOrganizationId, Organization.OrganizationType.SUPPLIER, new Date(), null, null));
        assortmentController.organizationController = organizationController;

        userController = Mockito.mock(UserController.class);
        Mockito.when(userController.getUserEngagement(customerOrganizationId, customerAssortmentUserId)).thenReturn(UserControllerTest.createValidUserEngagement(customerOrganizationId, customerAssortmentUserId, Organization.OrganizationType.CUSTOMER, UserRole.RoleName.CustomerAssortmentManager, null));
        Mockito.when(userController.getUserEngagement(customerOrganizationId, customerAssignedAssortmentUserId)).thenReturn(UserControllerTest.createValidUserEngagement(customerOrganizationId, customerAssignedAssortmentUserId, Organization.OrganizationType.CUSTOMER, UserRole.RoleName.CustomerAssignedAssortmentManager, null));
        assortmentController.userController = userController;

        ValidationMessageService validationMessageService = Mockito.mock(ValidationMessageService.class);
        Mockito.when(validationMessageService.generateValidationException(Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer<HjalpmedelstjanstenValidationException>() {
            @Override
            public HjalpmedelstjanstenValidationException answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException((String) args[0] + "->" + (String) args[1]);
                exception.addValidationMessage((String) args[0], (String) args[1]);
                return exception;
            }
        });
        Mockito.when(validationMessageService.generateValidationException(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer<HjalpmedelstjanstenValidationException>() {
            @Override
            public HjalpmedelstjanstenValidationException answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException((String) args[0] + "->" + (String) args[1]);
                exception.addValidationMessage((String) args[0], (String) args[1]);
                return exception;
            }
        });
        assortmentController.validationMessageService = validationMessageService;

        countyController = Mockito.mock(CountyController.class);
        municipality = createValidMunicipality(municipalityId);
        municipalityOtherCounty = createValidMunicipality(municipalityOtherCountyId);
        Mockito.when(countyController.findCountyById(countyId)).thenReturn(createValidCounty(countyId));
        Mockito.when(countyController.findMunicipalityById(municipalityId)).thenReturn(municipality);
        Mockito.when(countyController.findMunicipalityById(municipalityOtherCountyId)).thenReturn(municipalityOtherCounty);
        assortmentController.countyController = countyController;
    }

    @Test
    public void testGetAssortment() {
        Assortment assortment = assortmentController.getAssortment(customerOrganizationId, assortmentId, customerAssortmentUserAPI);
        Assert.assertNotNull(assortment);
    }

    @Test
    public void testGetAssortmentWrongAssortmentId() {
        Assortment assortment = assortmentController.getAssortment(customerOrganizationId, assortmentId+1, customerAssortmentUserAPI);
        Assert.assertNull(assortment);
    }

    @Test
    public void testGetAssortmentWrongOrganizationId() {
        Assortment assortment = assortmentController.getAssortment(customerOrganizationId+1, assortmentId, customerAssortmentUserAPI);
        Assert.assertNull(assortment);
    }

    @Test
    public void testGetAssortmentAPI() {
        AssortmentAPI assortmentAPI = assortmentController.getAssortmentAPI(customerOrganizationId, assortmentId, customerAssortmentUserAPI, null, null);
        Assert.assertNotNull(assortmentAPI);
    }

    @Test
    public void testGetAssortmentAPIWrongAssortmentId() {
        AssortmentAPI assortmentAPI = assortmentController.getAssortmentAPI(customerOrganizationId, assortmentId+1, customerAssortmentUserAPI, null, null);
        Assert.assertNull(assortmentAPI);
    }

    @Test
    public void testGetAssortmentAPIWrongOrganizationId() {
        AssortmentAPI assortmentAPI = assortmentController.getAssortmentAPI(customerOrganizationId+1, assortmentId, customerAssortmentUserAPI, null, null);
        Assert.assertNull(assortmentAPI);
    }

    @Test
    public void testCreateAssortment() {
        AssortmentAPI assortmentAPI = createValidAssortmentAPI(assortmentId, assortmentName);
        try {
            assortmentController.createAssortment(customerOrganizationId, assortmentAPI, customerAssortmentUserAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    }

    @Test
    public void testCreateAssortmentWrongOrganizationType() {
        AssortmentAPI assortmentAPI = createValidAssortmentAPI(assortmentId, assortmentName);
        try {
            assortmentController.createAssortment(supplierOrganizationId, assortmentAPI, customerAssortmentUserAPI, null, null);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertTrue(errorMessageAPI.getMessage().equals("assortment.customerOrganization.invalidType"));
        }
    }

    @Test
    public void testCreateAssortmentNonExistingOrganization() {
        AssortmentAPI assortmentAPI = createValidAssortmentAPI(assortmentId, assortmentName);
        try {
            assortmentController.createAssortment(customerOrganizationId+1000, assortmentAPI, customerAssortmentUserAPI, null, null);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertTrue(errorMessageAPI.getMessage().equals("assortment.customerOrganization.notExists"));
        }
    }

    @Test
    public void testCreateAssortmentWrongManagerRole() {
        AssortmentAPI assortmentAPI = createValidAssortmentAPI(assortmentId, assortmentName);
        List<UserAPI> managers = new ArrayList<>();
        managers.add(customerAssortmentUserAPI);
        assortmentAPI.setManagers(managers);
        try {
            assortmentController.createAssortment(customerOrganizationId, assortmentAPI, customerAssortmentUserAPI, null, null);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertTrue(errorMessageAPI.getMessage().equals("assortment.managers.wrongRole"));
        }
    }

    @Test
    public void testCreateAssortmentNonExistingManager() {
        AssortmentAPI assortmentAPI = createValidAssortmentAPI(assortmentId, assortmentName);
        List<UserAPI> managers = new ArrayList<>();
        UserAPI userAPI = new UserAPI();
        userAPI.setId(customerAssortmentUserId+1000);
        managers.add(userAPI);
        assortmentAPI.setManagers(managers);
        try {
            assortmentController.createAssortment(customerOrganizationId, assortmentAPI, customerAssortmentUserAPI, null, null);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertTrue(errorMessageAPI.getMessage().equals("agreement.customerPricelistApprovers.notExist"));
        }
    }

    @Test
    public void testUpdateAssortment() {
        AssortmentAPI assortmentAPI = createValidAssortmentAPI(assortmentId, assortmentName);
        try {
            assortmentController.updateAssortment(customerOrganizationId, assortmentId, assortmentAPI, superAdminUserAPI, null, null, true);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    }

    @Test
    public void testUpdateAssortmentNonExistingCounty() {
        AssortmentAPI assortmentAPI = createValidAssortmentAPI(assortmentId, assortmentName);
        CVCountyAPI countyAPI = createValidCVCountyAPI(countyId+1000);
        assortmentAPI.setCounty(countyAPI);
        try {
            assortmentController.updateAssortment(customerOrganizationId, assortmentId, assortmentAPI, superAdminUserAPI, null, null, true);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertTrue(errorMessageAPI.getMessage().equals("assortment.county.notExists"));
        }
    }

    @Test
    public void testUpdateAssortmentMunicipalityOtherCounty() {
        AssortmentAPI assortmentAPI = createValidAssortmentAPI(assortmentId, assortmentName);
        List<CVMunicipalityAPI> municipalityAPIs = new ArrayList<>();
        municipalityAPIs.add(createValidCVMunicipalityAPI(municipalityOtherCountyId));
        assortmentAPI.setMunicipalities(municipalityAPIs);
        try {
            assortmentController.updateAssortment(customerOrganizationId, assortmentId, assortmentAPI, superAdminUserAPI, null, null, true);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertTrue(errorMessageAPI.getMessage().equals("assortment.municipalities.notInCounty"));
        }
    }

    public static AssortmentAPI createValidAssortmentAPI(long id, String name) {
        AssortmentAPI assortmentAPI = new AssortmentAPI();
        assortmentAPI.setId(id);
        assortmentAPI.setName(name);
        Calendar validFrom = Calendar.getInstance();
        assortmentAPI.setValidFrom(validFrom.getTimeInMillis());
        List<UserAPI> managers = new ArrayList<>();
        managers.add(customerAssignedAssortmentUserAPI);
        assortmentAPI.setManagers(managers);
        assortmentAPI.setCounty(createValidCVCountyAPI(countyId));
        List<CVMunicipalityAPI> municipalityAPIs = new ArrayList<>();
        municipalityAPIs.add(createValidCVMunicipalityAPI(municipalityId));
        assortmentAPI.setMunicipalities(municipalityAPIs);
        return assortmentAPI;
    }

    public static CVCountyAPI createValidCVCountyAPI(long id) {
        CVCountyAPI countyAPI = new CVCountyAPI();
        countyAPI.setId(id);
        return countyAPI;
    }

    public static CVMunicipalityAPI createValidCVMunicipalityAPI(long id) {
        CVMunicipalityAPI municipalityAPI = new CVMunicipalityAPI();
        municipalityAPI.setId(id);
        return municipalityAPI;
    }

    public static Assortment createValidAssortment(long customerId, long id, String name) {
        Assortment assortment = new Assortment();
        assortment.setUniqueId(id);
        assortment.setName(name);
        assortment.setValidFrom(new Date(System.currentTimeMillis()));
        assortment.setCustomer(OrganizationControllerTest.createValidOrganization(customerId, Organization.OrganizationType.CUSTOMER, new Date(), null, null));
        return assortment;
    }

    private CVCounty createValidCounty(long id) {
        CVCounty county = new CVCounty();
        county.setUniqueId(id);
        county.setShowMunicipalities(true);
        List<CVMunicipality> municipalitys = new ArrayList<>();
        municipalitys.add(municipality);
        county.setMunicipalities(municipalitys);
        return county;
    }

    private CVMunicipality createValidMunicipality(long id) {
        CVMunicipality cvMunicipality = new CVMunicipality();
        cvMunicipality.setUniqueId(id);
        return cvMunicipality;
    }

}
