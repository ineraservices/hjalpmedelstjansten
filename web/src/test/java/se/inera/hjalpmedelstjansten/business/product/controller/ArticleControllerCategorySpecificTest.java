package se.inera.hjalpmedelstjansten.business.product.controller;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowController;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistRowController;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationControllerTest;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.ResourceSpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEDirectiveAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEStandardAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVOrderUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPackageUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.ProductAPITest;
import se.inera.hjalpmedelstjansten.model.api.validation.UserAPITest;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificPropertyListValue;
import se.inera.hjalpmedelstjansten.model.entity.ElectronicAddress;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValue;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueDecimal;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueInterval;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueTextField;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueValueListMultiple;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueValueListSingle;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEDirective;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEStandard;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

import jakarta.enterprise.event.Event;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ArticleControllerCategorySpecificTest {

    ArticleController articleController;
    CategoryController categoryController;
    CeController ceController;
    EntityManager myEntityManager;
    Event<InternalAuditEvent> event;
    ProductController productController;
    UserAPI userAPI;
    AgreementPricelistRowController agreementPricelistRowController;
    GeneralPricelistPricelistRowController generalPricelistPricelistRowController;

    static String articleName = "Article name";
    static String articleArticleNumber = "Article number";
    static Product.Status articleStatus = Product.Status.PUBLISHED;
    static String articleSupplementedInformation = "Article supplemented information";
    static String articleColor = "Article color";
    static String articleTrademark = "Article trademark";
    static String articleManufacturer = "Article manufacturer";
    static String articleManufacturerProductNumber = "Article manufacturer article number";
    static String articleManufacturerWeb = "https://www.inera.se";
    static boolean articleCeMarked = true;
    static String articleCeDirectiveName = "Article Ce Directive name";
    static String articleCeStandardName = "Article Ce Standard name";
    static String articlePreventiveMaintenanceDescription = "Article Preventive maintenance description";
    static int articlePreventiveMaintenanceNumberOfDays = 3;
    static String articleGtin = "1000000000009";
    static String articleOrderUnitName = "Product order unit name";
    static String articleOrderUnitCode = "Product order unit code";
    static String articleGuaranteeUnitName = "Product guarantee unit name";
    static String articleGuaranteeUnitCode = "Product guarantee unit code";
    static Double articleArticleQuantityInOuterPackage = 112.0;
    static Double articlePackageContent = 113.0;
    static Integer articlePackageLevelBase = 114;
    static Integer articlePackageLevelMiddle = 115;
    static Integer articlePackageLevelTop = 116;
    static boolean articleCustomerUnique = false;
    static String organizationName = "organization name";
    static String resourceSpecificPropertyValueTextField = "text data";
    static double resourceSpecificPropertyValueDecimal = 1.0;
    static double resourceSpecificPropertyValueIntervalFrom = 1.0;
    static double resourceSpecificPropertyValueIntervalTo = 10.0;
    static String preventiveMaintenanceUnitCode = "code";
    static String preventiveMaintenanceUnitName = "name";
    static CVPreventiveMaintenance articlePreventiveMaintenanceValidFrom = PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenance(preventiveMaintenanceUnitCode, preventiveMaintenanceUnitName);

    static long articleId = 100;
    static long organizationId = 102;
    static long categoryId = 103;
    static long articleCeDirectiveId = 104;
    static long articleCeStandardId = 105;
    static long fitsToProductId = 106;
    static long articleOrderUnitId = 107;
    static long fitsToNonExistingArticleId = 108;
    static long fitsToProductTId = 109;
    static long fitsToProductIId = 110;
    static long fitsToProductTjId = 111;
    static long fitsToProductRId = 112;
    static long fitsToArticleIId = 113;
    static long fitsToArticleTId = 114;
    static long fitsToArticleTjId = 115;
    static long fitsToArticleRId = 116;
    static long fitsToArticleHId = 117;
    static long fitsToProductTNoCodeId = 118;
    static long fitsToArticleTNoCodeId = 119;
    static long userId = 120;
    static long articleBasedOnProductTypeHId = 121;
    static long articleBasedOnProductTypeTId = 122;
    static long categorySpecificPropertyIdTextField = 123;
    static long categorySpecificPropertyIdDecimal = 124;
    static long categorySpecificPropertyIdInterval = 125;
    static long categorySpecificPropertyIdValueListSingle = 126;
    static long categorySpecificPropertyIdValueListMultiple = 127;
    static long resourceSpecificPropertyListValue1 = 128;
    static long resourceSpecificPropertyListValue2 = 129;
    static long resourceSpecificPropertyListValue3 = 130;
    static long resourceSpecificPropertyValueId = 131;

    @Before
    public void init() {
        myEntityManager = Mockito.mock(EntityManager.class);
        Mockito.when(myEntityManager.find(Article.class, articleId)).thenReturn(createValidArticle(articleId, organizationId));

        Query mockQuery = Mockito.mock(Query.class);
        Mockito.when(mockQuery.setParameter(Mockito.anyString(), Mockito.any())).thenReturn(mockQuery);
        Mockito.when(mockQuery.getResultList()).thenReturn(new ArrayList());
        Mockito.when(myEntityManager.createNamedQuery(Article.FIND_BY_GTIN)).thenReturn(mockQuery);
        Mockito.when(myEntityManager.createNamedQuery(Article.GET_IDS_FITS_TO_ARTICLE)).thenReturn(mockQuery);
        articleController = new ArticleController();
        articleController.LOG = new NoLogger();
        articleController.em = myEntityManager;
        categoryController = Mockito.mock(CategoryController.class);
        List<Category> categorys = new ArrayList<>();
        Category category = CategoryControllerTest.createValidCategory(categoryId);
        category.setArticleType(Article.Type.T);
        category.setCode(null);
        categorys.add(category);
        Mockito.when(categoryController.getChildlessById(categoryId)).thenReturn(categorys);
        CategorySpecificPropertyListValue categorySpecificPropertyListValue1 = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue1.setUniqueId(resourceSpecificPropertyListValue1);
        Mockito.when(categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyListValue1)).thenReturn(categorySpecificPropertyListValue1);
        CategorySpecificPropertyListValue categorySpecificPropertyListValue2 = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue2.setUniqueId(resourceSpecificPropertyListValue2);
        Mockito.when(categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyListValue2)).thenReturn(categorySpecificPropertyListValue2);
        CategorySpecificPropertyListValue categorySpecificPropertyListValue3 = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue3.setUniqueId(resourceSpecificPropertyListValue3);
        Mockito.when(categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyListValue3)).thenReturn(categorySpecificPropertyListValue3);
        articleController.categoryController = categoryController;
        event = Mockito.mock(Event.class);
        articleController.internalAuditEvent = event;
        ValidationMessageService validationMessageService = Mockito.mock(ValidationMessageService.class);
        Mockito.when(validationMessageService.generateValidationException(Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer<HjalpmedelstjanstenValidationException>() {
            @Override
            public HjalpmedelstjanstenValidationException answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException((String) args[0] + "->" + (String) args[1]);
                exception.addValidationMessage((String) args[0], (String) args[1]);
                return exception;
            }
        });
        Mockito.when(validationMessageService.generateValidationException(Mockito.anyString(), Mockito.anyString(), Mockito.any())).thenAnswer(new Answer<HjalpmedelstjanstenValidationException>() {
            @Override
            public HjalpmedelstjanstenValidationException answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException((String) args[0] + "->" + (String) args[1]);
                exception.addValidationMessage((String) args[0], (String) args[1]);
                return exception;
            }
        });
        articleController.validationMessageService = validationMessageService;

        productController = Mockito.mock(ProductController.class);
        Product connectedToProduct = ProductControllerTest.createValidProduct(fitsToProductId, organizationId);
        connectedToProduct.getCategory().setArticleType(Article.Type.H);
        Mockito.when(productController.getProduct(organizationId, fitsToProductId, userAPI)).thenReturn(connectedToProduct);
        Product connectedToProductI = ProductControllerTest.createValidProduct(fitsToProductIId, organizationId);
        connectedToProductI.getCategory().setArticleType(Article.Type.I);
        Mockito.when(productController.getProduct(organizationId, fitsToProductIId, userAPI)).thenReturn(connectedToProductI);
        Product connectedToProductT = ProductControllerTest.createValidProduct(fitsToProductTId, organizationId);
        connectedToProductT.getCategory().setArticleType(Article.Type.T);
        Mockito.when(productController.getProduct(organizationId, fitsToProductTId, userAPI)).thenReturn(connectedToProductT);
        Product connectedToProductTj = ProductControllerTest.createValidProduct(fitsToProductTjId, organizationId);
        connectedToProductTj.getCategory().setArticleType(Article.Type.Tj);
        Mockito.when(productController.getProduct(organizationId, fitsToProductTjId, userAPI)).thenReturn(connectedToProductTj);
        Product connectedToProductR = ProductControllerTest.createValidProduct(fitsToProductRId, organizationId);
        connectedToProductR.getCategory().setArticleType(Article.Type.R);
        Mockito.when(productController.getProduct(organizationId, fitsToProductRId, userAPI)).thenReturn(connectedToProductR);
        Product connectedToProductTNoCode = ProductControllerTest.createValidProduct(fitsToProductTNoCodeId, organizationId);
        connectedToProductTNoCode.getCategory().setArticleType(Article.Type.T);
        connectedToProductTNoCode.getCategory().setCode(null);
        Mockito.when(productController.getProduct(organizationId, fitsToProductTNoCodeId, userAPI)).thenReturn(connectedToProductTNoCode);
        articleController.productController = productController;

        ArticleValidation articleValidation = new ArticleValidation();
        articleValidation.LOG = new NoLogger();
        articleValidation.validationMessageService = validationMessageService;
        articleValidation.articleController = articleController;
        articleValidation.productController = productController;
        Mockito.when(articleController.findByGtin(articleGtin)).thenReturn(null);
        articleController.articleValidation = articleValidation;

        ceController = Mockito.mock(CeController.class);
        articleController.ceController = ceController;
        CVCEStandard standard = new CVCEStandard();
        standard.setUniqueId(articleCeStandardId);
        CVCEDirective directive = new CVCEDirective();
        directive.setUniqueId(articleCeDirectiveId);
        Mockito.when(ceController.findCEDirectiveById(articleCeDirectiveId)).thenReturn(directive);
        Mockito.when(ceController.findCEStandardById(articleCeStandardId)).thenReturn(standard);

        agreementPricelistRowController = Mockito.mock(AgreementPricelistRowController.class);
        articleController.agreementPricelistRowController = agreementPricelistRowController;
        generalPricelistPricelistRowController = Mockito.mock(GeneralPricelistPricelistRowController.class);
        articleController.generalPricelistPricelistRowController = generalPricelistPricelistRowController;
    }

    @Test
    public void testGetArticleValidOrganization() {
        Article article = articleController.getArticle(organizationId, articleId, userAPI);
        Assert.assertNotNull(article);
    }

    @Test
    public void testGetArticleWrongUniqueId() {
        Article article = articleController.getArticle(organizationId, -1l, userAPI);
        Assert.assertNull(article);
    }

    @Test
    public void testGetArticleInvalidOrganization() {
        long invalidOrganizationUniqueId = 3l;
        Article validArticle = createValidArticle(articleId, organizationId);
        validArticle.getOrganization().setUniqueId(invalidOrganizationUniqueId);
        Mockito.when(myEntityManager.find(Article.class, articleId)).thenReturn(validArticle);
        Article article = articleController.getArticle(organizationId, articleId, userAPI);
        Assert.assertNull(article);
    }

    @Test
    public void testGetArticleAPI() {
        Article validArticle = createValidArticle(articleId, organizationId);
        validArticle.getCategory().setUniqueId(categoryId);
        List<Category> categorys = new ArrayList<>();
        Category category = CategoryControllerTest.createValidCategory(categoryId);
        categorys.add(category);
        validArticle.setExtendedCategories(categorys);
        Mockito.when(myEntityManager.find(Article.class, articleId)).thenReturn(validArticle);
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ArticleAPI articleAPI = articleController.getArticleAPI(organizationId, articleId, userAPI, null, null);
        Assert.assertNotNull(articleAPI);
        Assert.assertEquals(articleId, articleAPI.getId().longValue());
        Assert.assertEquals(articleStatus.toString(), articleAPI.getStatus());
        Assert.assertEquals(organizationName, articleAPI.getOrganizationName());
        Assert.assertEquals(articleSupplementedInformation, articleAPI.getSupplementedInformation());

        // validate category
        Assert.assertNotNull(articleAPI.getCategory());
        Assert.assertEquals(CategoryControllerTest.PRODUCT_CATEGORY_ARTICLE_TYPE, articleAPI.getCategory().getArticleType());
        Assert.assertEquals(CategoryControllerTest.PRODUCT_CATEGORY_NAME, articleAPI.getCategory().getName());
        Assert.assertEquals(CategoryControllerTest.PRODUCT_CATEGORY_CODE, articleAPI.getCategory().getCode());

        // validate extended category
        Assert.assertNotNull(articleAPI.getExtendedCategories());
        Assert.assertEquals(1, articleAPI.getExtendedCategories().size());
        CategoryAPI extendedCategoryAPI = articleAPI.getExtendedCategories().get(0);
        Assert.assertNotNull(extendedCategoryAPI);
        Assert.assertEquals(CategoryControllerTest.PRODUCT_CATEGORY_ARTICLE_TYPE, extendedCategoryAPI.getArticleType());
        Assert.assertEquals(CategoryControllerTest.PRODUCT_CATEGORY_NAME, extendedCategoryAPI.getName());
        Assert.assertEquals(CategoryControllerTest.PRODUCT_CATEGORY_CODE, extendedCategoryAPI.getCode());

        // validate manufacturer
        Assert.assertEquals(articleTrademark, articleAPI.getTrademark());
        Assert.assertEquals(articleManufacturer, articleAPI.getManufacturer());
        Assert.assertEquals(articleManufacturerProductNumber, articleAPI.getManufacturerArticleNumber());
        Assert.assertEquals(articleManufacturerWeb ,articleAPI.getManufacturerElectronicAddress().getWeb());

        // validate ce
        Assert.assertTrue(articleAPI.isCeMarked());

        // validate ce directive
        Assert.assertNotNull(articleAPI.getCeDirective());
        Assert.assertEquals(articleCeDirectiveId, articleAPI.getCeDirective().getId());
        Assert.assertEquals(articleCeDirectiveName, articleAPI.getCeDirective().getName());

        // validate ce standard
        Assert.assertNotNull(articleAPI.getCeStandard());
        Assert.assertEquals(articleCeStandardId, articleAPI.getCeStandard().getId());
        Assert.assertEquals(articleCeStandardName, articleAPI.getCeStandard().getName());

        // validate preventive maintenance
        Assert.assertEquals(articlePreventiveMaintenanceDescription, articleAPI.getPreventiveMaintenanceDescription());
        Assert.assertEquals(articlePreventiveMaintenanceNumberOfDays, articleAPI.getPreventiveMaintenanceNumberOfDays().intValue());
        Assert.assertEquals(articlePreventiveMaintenanceValidFrom.getCode(), articleAPI.getPreventiveMaintenanceValidFrom().getCode());

        Assert.assertEquals(articleOrderUnitId, articleAPI.getOrderUnit().getId().longValue());
        Assert.assertEquals(articleOrderUnitCode, articleAPI.getOrderUnit().getCode());
        Assert.assertEquals(articleOrderUnitName, articleAPI.getOrderUnit().getName());
        Assert.assertEquals(articleArticleQuantityInOuterPackage, articleAPI.getArticleQuantityInOuterPackage());
        Assert.assertEquals(articleOrderUnitId, articleAPI.getArticleQuantityInOuterPackageUnit().getId().longValue());
        Assert.assertEquals(articleOrderUnitCode, articleAPI.getArticleQuantityInOuterPackageUnit().getCode());
        Assert.assertEquals(articleOrderUnitName, articleAPI.getArticleQuantityInOuterPackageUnit().getName());
        Assert.assertEquals(articlePackageContent, articleAPI.getPackageContent());
        Assert.assertEquals(articleOrderUnitId, articleAPI.getPackageContentUnit().getId().longValue());
        Assert.assertEquals(articleOrderUnitCode, articleAPI.getPackageContentUnit().getCode());
        Assert.assertEquals(articleOrderUnitName, articleAPI.getPackageContentUnit().getName());
        Assert.assertEquals(articlePackageLevelBase, articleAPI.getPackageLevelBase());
        Assert.assertEquals(articlePackageLevelMiddle, articleAPI.getPackageLevelMiddle());
        Assert.assertEquals(articlePackageLevelTop, articleAPI.getPackageLevelTop());
    }

    @Test
    public void testSetCategorySpecificPropertyAllNull() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        try {
            articleController.setResourceSpecificProperties(articleAPI, article, article.getCategory(), null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testSetCategorySpecificPropertyOnePropertyInPossible() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setType(CategorySpecificProperty.Type.TEXTFIELD);
        categorySpecificPropertys.add(categorySpecificProperty);

        try {
            articleController.setResourceSpecificProperties(articleAPI, article, article.getCategory(), categorySpecificPropertys);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testSetCategorySpecificPropertyOneValidInAPI() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdTextField);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        articleAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);

        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);

        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdTextField);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.TEXTFIELD);
        categorySpecificPropertys.add(categorySpecificProperty);

        Mockito.when(productController.categorySpecificPropertysContains(categorySpecificPropertyIdTextField, categorySpecificPropertys)).thenReturn(categorySpecificProperty);

        try {
            articleController.setResourceSpecificProperties(articleAPI, article, article.getCategory(), categorySpecificPropertys);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testSetCategorySpecificPropertyTextfield() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdTextField);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPI.setTextValue(resourceSpecificPropertyValueTextField);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        articleAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);

        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);

        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdTextField);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.TEXTFIELD);
        categorySpecificPropertys.add(categorySpecificProperty);

        Mockito.when(productController.categorySpecificPropertysContains(categorySpecificPropertyIdTextField, categorySpecificPropertys)).thenReturn(categorySpecificProperty);

        try {
            articleController.setResourceSpecificProperties(articleAPI, article, article.getCategory(), categorySpecificPropertys);
            Assert.assertNotNull(article.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, article.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(article.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueTextField);
            ResourceSpecificPropertyValueTextField textfield = ( ResourceSpecificPropertyValueTextField ) article.getResourceSpecificPropertyValues().get(0);
            Assert.assertEquals(resourceSpecificPropertyValueTextField, textfield.getValue());
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testSetCategorySpecificPropertyDecimal() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdDecimal);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPI.setDecimalValue(resourceSpecificPropertyValueDecimal);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        articleAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);

        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);

        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdDecimal);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.DECIMAL);
        categorySpecificPropertys.add(categorySpecificProperty);

        Mockito.when(productController.categorySpecificPropertysContains(categorySpecificPropertyIdDecimal, categorySpecificPropertys)).thenReturn(categorySpecificProperty);

        try {
            articleController.setResourceSpecificProperties(articleAPI, article, article.getCategory(), categorySpecificPropertys);
            Assert.assertNotNull(article.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, article.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(article.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueDecimal);
            ResourceSpecificPropertyValueDecimal decimalValue = ( ResourceSpecificPropertyValueDecimal ) article.getResourceSpecificPropertyValues().get(0);
            Assert.assertTrue(resourceSpecificPropertyValueDecimal == decimalValue.getValue());
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testSetCategorySpecificPropertyInterval() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdInterval);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPI.setIntervalFromValue(resourceSpecificPropertyValueIntervalFrom);
        resourceSpecificPropertyAPI.setIntervalToValue(resourceSpecificPropertyValueIntervalTo);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        articleAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);

        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);

        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdInterval);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.INTERVAL);
        categorySpecificPropertys.add(categorySpecificProperty);

        Mockito.when(productController.categorySpecificPropertysContains(categorySpecificPropertyIdInterval, categorySpecificPropertys)).thenReturn(categorySpecificProperty);

        try {
            articleController.setResourceSpecificProperties(articleAPI, article, article.getCategory(), categorySpecificPropertys);
            Assert.assertNotNull(article.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, article.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(article.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueInterval);
            ResourceSpecificPropertyValueInterval intervalValue = ( ResourceSpecificPropertyValueInterval ) article.getResourceSpecificPropertyValues().get(0);
            Assert.assertTrue(resourceSpecificPropertyValueIntervalFrom == intervalValue.getFromValue());
            Assert.assertTrue(resourceSpecificPropertyValueIntervalTo == intervalValue.getToValue());
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testSetCategorySpecificPropertySingleList() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdValueListSingle);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPI.setSingleListValue(resourceSpecificPropertyListValue1);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        articleAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);

        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);

        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdValueListSingle);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.VALUELIST_SINGLE);
        categorySpecificPropertys.add(categorySpecificProperty);

        Mockito.when(productController.categorySpecificPropertysContains(categorySpecificPropertyIdValueListSingle, categorySpecificPropertys)).thenReturn(categorySpecificProperty);

        try {
            articleController.setResourceSpecificProperties(articleAPI, article, article.getCategory(), categorySpecificPropertys);
            Assert.assertNotNull(article.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, article.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(article.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueValueListSingle);
            ResourceSpecificPropertyValueValueListSingle singleValue = ( ResourceSpecificPropertyValueValueListSingle ) article.getResourceSpecificPropertyValues().get(0);
            Assert.assertTrue(resourceSpecificPropertyListValue1 == singleValue.getValue().getUniqueId());
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testSetCategorySpecificPropertyMultipleList() {
        ArticleAPI articleAPI = createValidArticleAPI();
        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdValueListMultiple);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        List<Long> multiListValues = new ArrayList<>();
        multiListValues.add(resourceSpecificPropertyListValue2);
        multiListValues.add(resourceSpecificPropertyListValue3);
        resourceSpecificPropertyAPI.setMultipleListValue(multiListValues);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        articleAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);

        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);

        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdValueListMultiple);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.VALUELIST_MULTIPLE);
        categorySpecificPropertys.add(categorySpecificProperty);

        Mockito.when(productController.categorySpecificPropertysContains(categorySpecificPropertyIdValueListMultiple, categorySpecificPropertys)).thenReturn(categorySpecificProperty);

        try {
            articleController.setResourceSpecificProperties(articleAPI, article, article.getCategory(), categorySpecificPropertys);
            Assert.assertNotNull(article.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, article.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(article.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueValueListMultiple);
            ResourceSpecificPropertyValueValueListMultiple multipleValue = ( ResourceSpecificPropertyValueValueListMultiple ) article.getResourceSpecificPropertyValues().get(0);
            Assert.assertEquals(2, multipleValue.getValues().size());
            Assert.assertTrue(multiListValues.contains(multipleValue.getValues().get(0).getUniqueId()));
            Assert.assertTrue(multiListValues.contains(multipleValue.getValues().get(1).getUniqueId()));
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testUpdateCategorySpecificPropertyTextfield() {
        ArticleAPI articleAPI = createValidArticleAPI();

        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        resourceSpecificPropertyAPI.setId(resourceSpecificPropertyValueId);
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdTextField);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPI.setTextValue(resourceSpecificPropertyValueTextField+resourceSpecificPropertyValueTextField);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        articleAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);


        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdTextField);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.TEXTFIELD);
        categorySpecificPropertys.add(categorySpecificProperty);

        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);

        List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues = new ArrayList<>();
        ResourceSpecificPropertyValueTextField textfield = new ResourceSpecificPropertyValueTextField();
        textfield.setUniqueId(resourceSpecificPropertyValueId);
        textfield.setValue(resourceSpecificPropertyValueTextField);
        textfield.setCategorySpecificProperty(categorySpecificProperty);
        resourceSpecificPropertyValues.add(textfield);
        article.setResourceSpecificPropertyValues(resourceSpecificPropertyValues);

        Mockito.when(productController.categorySpecificPropertysContains(categorySpecificPropertyIdTextField, categorySpecificPropertys)).thenReturn(categorySpecificProperty);
        Mockito.when(productController.getResourceSpecificPropertyValue(article.getResourceSpecificPropertyValues(), resourceSpecificPropertyValueId)).thenReturn(textfield);

        try {
            articleController.setResourceSpecificProperties(articleAPI, article, article.getCategory(), categorySpecificPropertys);
            Assert.assertNotNull(article.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, article.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(article.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueTextField);
            ResourceSpecificPropertyValueTextField updatedTextfield = ( ResourceSpecificPropertyValueTextField ) article.getResourceSpecificPropertyValues().get(0);
            Assert.assertEquals(resourceSpecificPropertyValueTextField+resourceSpecificPropertyValueTextField, updatedTextfield.getValue());
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testUpdateCategorySpecificPropertyDecimal() {
        ArticleAPI articleAPI = createValidArticleAPI();

        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        resourceSpecificPropertyAPI.setId(resourceSpecificPropertyValueId);
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdDecimal);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPI.setDecimalValue(resourceSpecificPropertyValueDecimal+resourceSpecificPropertyValueDecimal);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        articleAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);


        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdDecimal);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.DECIMAL);
        categorySpecificPropertys.add(categorySpecificProperty);

        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);

        List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues = new ArrayList<>();
        ResourceSpecificPropertyValueDecimal decimalfield = new ResourceSpecificPropertyValueDecimal();
        decimalfield.setUniqueId(resourceSpecificPropertyValueId);
        decimalfield.setValue(resourceSpecificPropertyValueDecimal);
        decimalfield.setCategorySpecificProperty(categorySpecificProperty);
        resourceSpecificPropertyValues.add(decimalfield);
        article.setResourceSpecificPropertyValues(resourceSpecificPropertyValues);

        Mockito.when(productController.categorySpecificPropertysContains(categorySpecificPropertyIdDecimal, categorySpecificPropertys)).thenReturn(categorySpecificProperty);
        Mockito.when(productController.getResourceSpecificPropertyValue(article.getResourceSpecificPropertyValues(), resourceSpecificPropertyValueId)).thenReturn(decimalfield);

        try {
            articleController.setResourceSpecificProperties(articleAPI, article, article.getCategory(), categorySpecificPropertys);
            Assert.assertNotNull(article.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, article.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(article.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueDecimal);
            ResourceSpecificPropertyValueDecimal updatedDecimal = ( ResourceSpecificPropertyValueDecimal ) article.getResourceSpecificPropertyValues().get(0);
            Assert.assertTrue((resourceSpecificPropertyValueDecimal+resourceSpecificPropertyValueDecimal) == updatedDecimal.getValue());
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testUpdateCategorySpecificPropertyInterval() {
        ArticleAPI articleAPI = createValidArticleAPI();

        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        resourceSpecificPropertyAPI.setId(resourceSpecificPropertyValueId);
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdInterval);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPI.setIntervalFromValue(resourceSpecificPropertyValueIntervalFrom+resourceSpecificPropertyValueIntervalFrom);
        resourceSpecificPropertyAPI.setIntervalToValue(resourceSpecificPropertyValueIntervalTo+resourceSpecificPropertyValueIntervalTo);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        articleAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);


        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdInterval);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.INTERVAL);
        categorySpecificPropertys.add(categorySpecificProperty);

        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);

        List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues = new ArrayList<>();
        ResourceSpecificPropertyValueInterval intervalField = new ResourceSpecificPropertyValueInterval();
        intervalField.setUniqueId(resourceSpecificPropertyValueId);
        intervalField.setFromValue(resourceSpecificPropertyValueIntervalFrom);
        intervalField.setToValue(resourceSpecificPropertyValueIntervalTo);
        intervalField.setCategorySpecificProperty(categorySpecificProperty);
        resourceSpecificPropertyValues.add(intervalField);
        article.setResourceSpecificPropertyValues(resourceSpecificPropertyValues);

        Mockito.when(productController.categorySpecificPropertysContains(categorySpecificPropertyIdInterval, categorySpecificPropertys)).thenReturn(categorySpecificProperty);
        Mockito.when(productController.getResourceSpecificPropertyValue(article.getResourceSpecificPropertyValues(), resourceSpecificPropertyValueId)).thenReturn(intervalField);

        try {
            articleController.setResourceSpecificProperties(articleAPI, article, article.getCategory(), categorySpecificPropertys);
            Assert.assertNotNull(article.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, article.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(article.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueInterval);
            ResourceSpecificPropertyValueInterval updatedInterval = ( ResourceSpecificPropertyValueInterval ) article.getResourceSpecificPropertyValues().get(0);
            Assert.assertTrue((resourceSpecificPropertyValueIntervalFrom+resourceSpecificPropertyValueIntervalFrom) == updatedInterval.getFromValue());
            Assert.assertTrue((resourceSpecificPropertyValueIntervalTo+resourceSpecificPropertyValueIntervalTo) == updatedInterval.getToValue());
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testUpdateCategorySpecificPropertySingleValue() {
        ArticleAPI articleAPI = createValidArticleAPI();

        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        resourceSpecificPropertyAPI.setId(resourceSpecificPropertyValueId);
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdValueListSingle);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPI.setSingleListValue(resourceSpecificPropertyListValue2);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        articleAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);


        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdValueListSingle);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.VALUELIST_SINGLE);
        categorySpecificPropertys.add(categorySpecificProperty);

        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);

        List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues = new ArrayList<>();
        ResourceSpecificPropertyValueValueListSingle singleList = new ResourceSpecificPropertyValueValueListSingle();
        singleList.setUniqueId(resourceSpecificPropertyValueId);
        CategorySpecificPropertyListValue categorySpecificPropertyListValue = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue.setCategorySpecificProperty(categorySpecificProperty);
        categorySpecificPropertyListValue.setUniqueId(resourceSpecificPropertyListValue1);
        singleList.setValue(categorySpecificPropertyListValue);
        singleList.setCategorySpecificProperty(categorySpecificProperty);
        resourceSpecificPropertyValues.add(singleList);
        article.setResourceSpecificPropertyValues(resourceSpecificPropertyValues);

        Mockito.when(productController.categorySpecificPropertysContains(categorySpecificPropertyIdValueListSingle, categorySpecificPropertys)).thenReturn(categorySpecificProperty);
        Mockito.when(productController.getResourceSpecificPropertyValue(article.getResourceSpecificPropertyValues(), resourceSpecificPropertyValueId)).thenReturn(singleList);

        try {
            articleController.setResourceSpecificProperties(articleAPI, article, article.getCategory(), categorySpecificPropertys);
            Assert.assertNotNull(article.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, article.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(article.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueValueListSingle);
            ResourceSpecificPropertyValueValueListSingle singleValue = ( ResourceSpecificPropertyValueValueListSingle ) article.getResourceSpecificPropertyValues().get(0);
            Assert.assertTrue(singleValue.getValue().getUniqueId() == resourceSpecificPropertyListValue2);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testUpdateCategorySpecificPropertyMultipleValue() {
        ArticleAPI articleAPI = createValidArticleAPI();

        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        resourceSpecificPropertyAPI.setId(resourceSpecificPropertyValueId);
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdValueListMultiple);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        List<Long> multipleListValue = new ArrayList<>();
        multipleListValue.add(resourceSpecificPropertyListValue2);
        multipleListValue.add(resourceSpecificPropertyListValue3);
        resourceSpecificPropertyAPI.setMultipleListValue(multipleListValue);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        articleAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);


        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdValueListMultiple);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.VALUELIST_MULTIPLE);
        categorySpecificPropertys.add(categorySpecificProperty);

        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);

        List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues = new ArrayList<>();
        ResourceSpecificPropertyValueValueListMultiple multiList = new ResourceSpecificPropertyValueValueListMultiple();
        multiList.setUniqueId(resourceSpecificPropertyValueId);
        List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues = new ArrayList<>();
        CategorySpecificPropertyListValue categorySpecificPropertyListValue = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue.setCategorySpecificProperty(categorySpecificProperty);
        categorySpecificPropertyListValue.setUniqueId(resourceSpecificPropertyListValue1);
        categorySpecificPropertyListValues.add(categorySpecificPropertyListValue);
        CategorySpecificPropertyListValue categorySpecificPropertyListValue2 = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue2.setCategorySpecificProperty(categorySpecificProperty);
        categorySpecificPropertyListValue2.setUniqueId(resourceSpecificPropertyListValue2);
        categorySpecificPropertyListValues.add(categorySpecificPropertyListValue2);
        multiList.setValues(categorySpecificPropertyListValues);
        multiList.setCategorySpecificProperty(categorySpecificProperty);
        resourceSpecificPropertyValues.add(multiList);
        article.setResourceSpecificPropertyValues(resourceSpecificPropertyValues);

        Mockito.when(productController.categorySpecificPropertysContains(categorySpecificPropertyIdValueListMultiple, categorySpecificPropertys)).thenReturn(categorySpecificProperty);
        Mockito.when(productController.getResourceSpecificPropertyValue(article.getResourceSpecificPropertyValues(), resourceSpecificPropertyValueId)).thenReturn(multiList);

        try {
            articleController.setResourceSpecificProperties(articleAPI, article, article.getCategory(), categorySpecificPropertys);
            Assert.assertNotNull(article.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, article.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(article.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueValueListMultiple);
            ResourceSpecificPropertyValueValueListMultiple multipleValue = ( ResourceSpecificPropertyValueValueListMultiple ) article.getResourceSpecificPropertyValues().get(0);
            Assert.assertTrue(multipleListValue.contains(multipleValue.getValues().get(0).getUniqueId()));
            Assert.assertTrue(multipleListValue.contains(multipleValue.getValues().get(1).getUniqueId()));
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    private static Product createBasedOnProductSameValues() {
        Product basedOnProduct = new Product();
        CVPackageUnit packageUnit = createValidPackageUnit(articleOrderUnitId);
        CVOrderUnit unit = createValidUnit(articleOrderUnitId);
        basedOnProduct.setOrderUnit(unit);
        basedOnProduct.setArticleQuantityInOuterPackage(articleArticleQuantityInOuterPackage);
        basedOnProduct.setArticleQuantityInOuterPackageUnit(unit);
        basedOnProduct.setPackageContent(articlePackageContent);
        basedOnProduct.setPackageContentUnit(packageUnit);
        basedOnProduct.setPackageLevelBase(articlePackageLevelBase);
        basedOnProduct.setPackageLevelMiddle(articlePackageLevelMiddle);
        basedOnProduct.setPackageLevelTop(articlePackageLevelTop);

        basedOnProduct.setCustomerUnique(articleCustomerUnique);

        // create category
        basedOnProduct.setCategory(CategoryControllerTest.createValidCategory(null));

        // set ce
        basedOnProduct.setCeMarked(articleCeMarked);

        // create ce standard
        CVCEStandard cEStandard = new CVCEStandard();
        cEStandard.setUniqueId(articleCeStandardId);
        cEStandard.setName(articleCeStandardName);
        basedOnProduct.setCeStandard(cEStandard);

        // create ce directive
        CVCEDirective cEDirective = new CVCEDirective();
        cEDirective.setUniqueId(articleCeDirectiveId);
        cEDirective.setName(articleCeDirectiveName);
        basedOnProduct.setCeDirective(cEDirective);

        // set manufacturer
        basedOnProduct.setTrademark(articleTrademark);
        basedOnProduct.setManufacturer(articleManufacturer);
        basedOnProduct.setManufacturerProductNumber(articleManufacturerProductNumber);
        ElectronicAddress manufacturerElectronicAddress = new ElectronicAddress();
        manufacturerElectronicAddress.setWeb(articleManufacturerWeb);
        basedOnProduct.setManufacturerElectronicAddress(manufacturerElectronicAddress);

        // preventive maintenance
        basedOnProduct.setPreventiveMaintenanceDescription(articlePreventiveMaintenanceDescription);
        basedOnProduct.setPreventiveMaintenanceNumberOfDays(articlePreventiveMaintenanceNumberOfDays);
        basedOnProduct.setPreventiveMaintenanceValidFrom(articlePreventiveMaintenanceValidFrom);

        // supplemental information
        basedOnProduct.setSupplementedInformation(articleSupplementedInformation);

        basedOnProduct.setOrganization(OrganizationControllerTest.createValidOrganization(organizationId, null, null, null, null));

        return basedOnProduct;
    }

    public static Article createValidArticle(long articleId, long organizationId) {
        return createValidArticle(articleId, organizationId, Product.Status.PUBLISHED);
    }

    public static Article createValidArticle(long articleId, long organizationId, Product.Status status) {
        Article article = new Article();
        article.setArticleNumber(articleArticleNumber);
        article.setUniqueId(articleId);
        article.setArticleName(articleName);
        article.setStatus(status);
        article.setSupplementedInformation(articleSupplementedInformation);
        article.setGtin(articleGtin);
        article.setCustomerUnique(false);

        // set manufacturer
        article.setTrademark(articleTrademark);
        article.setManufacturer(articleManufacturer);
        article.setManufacturerArticleNumber(articleManufacturerProductNumber);
        ElectronicAddress manufacturerElectronicAddress = new ElectronicAddress();
        manufacturerElectronicAddress.setWeb(articleManufacturerWeb);
        article.setManufacturerElectronicAddress(manufacturerElectronicAddress);

        // preventive maintenance
        article.setPreventiveMaintenanceDescription(articlePreventiveMaintenanceDescription);
        article.setPreventiveMaintenanceNumberOfDays(articlePreventiveMaintenanceNumberOfDays);
        article.setPreventiveMaintenanceValidFrom(articlePreventiveMaintenanceValidFrom);

        // create category
        article.setCategory(CategoryControllerTest.createValidCategory(null));

        article.setOrganization(OrganizationControllerTest.createValidOrganization(organizationId, Organization.OrganizationType.SUPPLIER, new Date(), null, organizationName));

        // set ce
        article.setCeMarked(articleCeMarked);

        // create ce standard
        CVCEStandard cEStandard = new CVCEStandard();
        cEStandard.setUniqueId(articleCeStandardId);
        cEStandard.setName(articleCeStandardName);
        article.setCeStandard(cEStandard);

        // create ce directive
        CVCEDirective cEDirective = new CVCEDirective();
        cEDirective.setUniqueId(articleCeDirectiveId);
        cEDirective.setName(articleCeDirectiveName);
        article.setCeDirective(cEDirective);

        CVOrderUnit unit = createValidUnit(articleOrderUnitId);
        CVPackageUnit packgeUnit = createValidPackageUnit(articleOrderUnitId);
        article.setOrderUnit(unit);
        article.setArticleQuantityInOuterPackage(articleArticleQuantityInOuterPackage);
        article.setArticleQuantityInOuterPackageUnit(unit);
        article.setPackageContent(articlePackageContent);
        article.setPackageContentUnit(packgeUnit);
        article.setPackageLevelBase(articlePackageLevelBase);
        article.setPackageLevelMiddle(articlePackageLevelMiddle);
        article.setPackageLevelTop(articlePackageLevelTop);

        return article;
    }

    public static CVOrderUnit createValidUnit( long id ) {
        CVOrderUnit unit = new CVOrderUnit();
        unit.setUniqueId(id);
        unit.setCode(articleOrderUnitCode);
        unit.setName(articleOrderUnitName);
        return unit;
    }

    public static CVPackageUnit createValidPackageUnit( long id ) {
        CVPackageUnit unit = new CVPackageUnit();
        unit.setUniqueId(id);
        unit.setCode(articleOrderUnitCode);
        unit.setName(articleOrderUnitName);
        return unit;
    }

    public static ArticleAPI createValidArticleAPI() {
        ArticleAPI articleAPI = new ArticleAPI();
        articleAPI.setId(articleId);
        articleAPI.setArticleName("test");
        articleAPI.setArticleNumber(articleArticleNumber);
        articleAPI.setCustomerUnique(articleCustomerUnique);
        articleAPI.setStatus(Product.Status.PUBLISHED.toString());
        articleAPI.setCategory(ProductAPITest.createValidCategoryAPI());

        // preventive maintenance
        articleAPI.setPreventiveMaintenanceDescription(articlePreventiveMaintenanceDescription);
        articleAPI.setPreventiveMaintenanceNumberOfDays(articlePreventiveMaintenanceNumberOfDays);
        articleAPI.setPreventiveMaintenanceValidFrom(PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenanceAPI(preventiveMaintenanceUnitCode, preventiveMaintenanceUnitName));

        // set manufacturer
        articleAPI.setTrademark(articleTrademark);
        articleAPI.setSupplementedInformation(articleSupplementedInformation);
        articleAPI.setManufacturer(articleManufacturer);
        articleAPI.setManufacturerArticleNumber(articleManufacturerProductNumber);
        ElectronicAddressAPI manufacturerElectronicAddress = new ElectronicAddressAPI();
        manufacturerElectronicAddress.setWeb(articleManufacturerWeb);
        articleAPI.setManufacturerElectronicAddress(manufacturerElectronicAddress);

        CategoryAPI categoryAPI = new CategoryAPI();
        categoryAPI.setId(categoryId);
        categoryAPI.setArticleType(Article.Type.T);
        articleAPI.setCategory(categoryAPI);

        articleAPI.setCeMarked(articleCeMarked);
        articleAPI.setCeDirective(createValidCEDirectiveAPI());
        articleAPI.setCeStandard(createValidCEStandardAPI());

        List<ProductAPI> fitsToProductAPIs = new ArrayList<>();
        ProductAPI productAPI = new ProductAPI();
        productAPI.setId(fitsToProductId);
        fitsToProductAPIs.add(productAPI);
        articleAPI.setFitsToProducts(fitsToProductAPIs);

        CVOrderUnitAPI unitAPI = createValidUnitAPI(articleOrderUnitId);
        CVPackageUnitAPI packageUnitAPI = createValidPackageUnitAPI(articleOrderUnitId);
        articleAPI.setOrderUnit(unitAPI);
        articleAPI.setArticleQuantityInOuterPackage(articleArticleQuantityInOuterPackage);
        articleAPI.setArticleQuantityInOuterPackageUnit(unitAPI);
        articleAPI.setPackageContent(articlePackageContent);
        articleAPI.setPackageContentUnit(packageUnitAPI);
        articleAPI.setPackageLevelBase(articlePackageLevelBase);
        articleAPI.setPackageLevelMiddle(articlePackageLevelMiddle);
        articleAPI.setPackageLevelTop(articlePackageLevelTop);

       return articleAPI;
    }

    private static CVCEDirectiveAPI createValidCEDirectiveAPI() {
        CVCEDirectiveAPI directiveAPI = new CVCEDirectiveAPI();
        directiveAPI.setId(articleCeDirectiveId);
        return directiveAPI;
    }

    private static CVCEStandardAPI createValidCEStandardAPI() {
        CVCEStandardAPI standardAPI = new CVCEStandardAPI();
        standardAPI.setId(articleCeStandardId);
        return standardAPI;
    }

    public static CVOrderUnitAPI createValidUnitAPI( long id ) {
        CVOrderUnitAPI unitAPI = new CVOrderUnitAPI();
        unitAPI.setId(id);
        unitAPI.setCode(articleOrderUnitCode);
        unitAPI.setName(articleOrderUnitName);
        return unitAPI;
    }

    public static CVPackageUnitAPI createValidPackageUnitAPI( long id ) {
        CVPackageUnitAPI unitAPI = new CVPackageUnitAPI();
        unitAPI.setId(id);
        unitAPI.setCode(articleOrderUnitCode);
        unitAPI.setName(articleOrderUnitName);
        return unitAPI;
    }

    public static CVGuaranteeUnitAPI createValidGuaranteeUnitAPI( long id ) {
        CVGuaranteeUnitAPI unitAPI = new CVGuaranteeUnitAPI();
        unitAPI.setId(id);
        unitAPI.setCode(articleGuaranteeUnitCode);
        unitAPI.setName(articleGuaranteeUnitName);
        return unitAPI;
    }

    public static CVGuaranteeUnit createValidGuaranteeUnit( long id ) {
        CVGuaranteeUnit unit = new CVGuaranteeUnit();
        unit.setUniqueId(id);
        unit.setCode(articleGuaranteeUnitCode);
        unit.setName(articleGuaranteeUnitName);
        return unit;
    }

}
