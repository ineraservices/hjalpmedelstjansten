package se.inera.hjalpmedelstjansten.business.product.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.UserAPITest;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;



public class ArticleValidationTest {
    
    static long articleId = 1;
    static long organizationId = 1;
    static final long pricelistApproverUserId = 15;
    private ArticleValidation businessValidator;
    ArticleController articleController;
    ProductController productController;
    UserAPI userAPI;
    
    @Before
    public void init() {
        articleController = Mockito.mock(ArticleController.class);
        productController = Mockito.mock(ProductController.class);
        businessValidator = new ArticleValidation();
        businessValidator.LOG = new NoLogger();
        Mockito.when(articleController.findByGtin(Mockito.anyString())).thenReturn(null);
        Mockito.when(articleController.findByArticleNumberAndOrganization(Mockito.anyString(), Mockito.anyLong())).thenReturn(null);
        businessValidator.articleController = articleController;
        businessValidator.productController = productController;
        businessValidator.validationMessageService = Mockito.mock(ValidationMessageService.class);
        userAPI = UserAPITest.createValidUserAPI(pricelistApproverUserId, organizationId, null, UserRole.RoleName.CustomerAgreementManager, null);

    }

    // business validator tests
    // business tests assumes bean validation has already been done
    
   /* @Test HJAL-1825: No longer a valid case since users needs to be allowed to create already inactive articles/ products.
    public void businessTestInvalidCreateStatusDiscontinued() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.setStatus(Product.Status.DISCONTINUED.toString());
        try {
            businessValidator.validateForCreate(articleAPI, organizationId);
            Assert.fail("Validation passed but shouldn't");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
        }
    } */
    
    @Test
    public void businessTestRightStatusPublishedToDiscontinued() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.setStatus(Product.Status.DISCONTINUED.toString());
        articleAPI.setReplacementDate(1l);
        articleAPI.setInactivateRowsOnReplacement(Boolean.TRUE);
        Article article = ArticleControllerTest.createValidArticle(articleId, organizationId);
        article.setStatus(Product.Status.PUBLISHED);
        try {
            businessValidator.validateForUpdate(articleAPI, article, organizationId,userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
//            Assert.fail("Validation didn't pass but should");
        }
    }
    
    @Test
    public void businessTestRightStatusPublishedToDiscontinuedNoInactivateRows() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.setStatus(Product.Status.DISCONTINUED.toString());
        articleAPI.setReplacementDate(1l);        
        Article article = ArticleControllerTest.createValidArticle(articleId, organizationId);
        article.setStatus(Product.Status.PUBLISHED);
        try {
            businessValidator.validateForUpdate(articleAPI, article, organizationId,userAPI);
            Assert.fail("Validation passed but shouldn't");
        } catch (HjalpmedelstjanstenValidationException ex) {
        }
    }
    
    @Test
    public void businessTestRightStatusSame() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.setStatus(Product.Status.PUBLISHED.toString());
        Article article = ArticleControllerTest.createValidArticle(articleId, organizationId);
        article.setStatus(Product.Status.PUBLISHED);
        try {
            businessValidator.validateForUpdate(articleAPI, article, organizationId,userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Validation didn't pass but should");     
        }
    }
        
    @Test
    public void businessTestWrongStatusDiscontinuedToPublished() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.setStatus(Product.Status.PUBLISHED.toString());
        Article article = ArticleControllerTest.createValidArticle(articleId, organizationId);
        article.setStatus(Product.Status.DISCONTINUED);
        try {
            businessValidator.validateForUpdate(articleAPI, article, organizationId,userAPI);

        } catch (HjalpmedelstjanstenValidationException ex) {
//              Assert.fail("Validation didn't pass but should");
//            HJAL-2035 ska inte gå att uppdatera inaktiva artiklar
//            success
        }
    }
    
    @Test
    public void businessTestWrongSetManufacturerEmail() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.getManufacturerElectronicAddress().setEmail("test@test.se");
        Article article = ArticleControllerTest.createValidArticle(articleId, organizationId);
        try {
            businessValidator.validateForUpdate(articleAPI, article, organizationId,userAPI);
            Assert.fail("Validation passed but shouldn't");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // success
        }
    }
    
    @Test
    public void businessTestWrongSetManufacturerMobile() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.getManufacturerElectronicAddress().setMobile("1234567");
        Article article = ArticleControllerTest.createValidArticle(articleId, organizationId);
        try {
            businessValidator.validateForUpdate(articleAPI, article, organizationId,userAPI);
            Assert.fail("Validation passed but shouldn't");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // success
        }
    }
    
    @Test
    public void businessTestWrongSetManufacturerTelephone() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.getManufacturerElectronicAddress().setTelephone("1234567");
        Article article = ArticleControllerTest.createValidArticle(articleId, organizationId);
        try {
            businessValidator.validateForUpdate(articleAPI, article, organizationId,userAPI);
            Assert.fail("Validation passed but shouldn't");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // success
        }
    }
    
    @Test
    public void businessTestWrongSetManufacturerFax() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.getManufacturerElectronicAddress().setFax("1234567");
        Article article = ArticleControllerTest.createValidArticle(articleId, organizationId);
        try {
            businessValidator.validateForUpdate(articleAPI, article, organizationId,userAPI);
            Assert.fail("Validation passed but shouldn't");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // success
        }
    }
    
    @Test
    public void testUpdateExistingArticleNumber() {
        Article existingArticle = ArticleControllerTest.createValidArticle(articleId, organizationId);
        existingArticle.setUniqueId(existingArticle.getUniqueId()+1);
        Mockito.when(articleController.findByArticleNumberAndOrganization(Mockito.anyString(), Mockito.anyLong())).thenReturn(existingArticle);
        
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        Article article = ArticleControllerTest.createValidArticle(articleId, organizationId);
        try {
            businessValidator.validateForUpdate(articleAPI, article, organizationId,userAPI);
            Assert.fail("Validation passed but shouldn't");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("articleNumber", errorMessageAPI.getField());
        }
    }
    
    @Test
    public void testUpdateExistingProductNumber() {
        Product existingProduct = ProductControllerTest.createValidProduct(articleId, organizationId);
        Mockito.when(productController.findByProductNumberAndOrganization(Mockito.anyString(), Mockito.anyLong())).thenReturn(existingProduct);
        
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        Article article = ArticleControllerTest.createValidArticle(articleId, organizationId);
        try {
            businessValidator.validateForUpdate(articleAPI, article, organizationId,userAPI);
            Assert.fail("Validation passed but shouldn't");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("articleNumber", errorMessageAPI.getField());
        }
    }
    
}
