package se.inera.hjalpmedelstjansten.business.security.view;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ShiroAuthHandlerTest {

    @InjectMocks
    private ShiroAuthHandler shiroAuthHandler;

    @Nested
    class TestIsAuthenticated {

        @Test
        void shallReturnFalseIfSubjectIsNull() {
            try (MockedStatic<SecurityUtils> securityUtils = Mockito.mockStatic(SecurityUtils.class)) {
                securityUtils.when(SecurityUtils::getSubject).thenReturn(null);

                assertFalse(shiroAuthHandler.isAthenticated());
            }
        }

        @Test
        void shallReturnFalseIfSubjectIsNotAuthenticated() {
            final var subject = mock(Subject.class);
            doReturn(false).when(subject).isAuthenticated();
            try (MockedStatic<SecurityUtils> securityUtils = Mockito.mockStatic(SecurityUtils.class)) {
                securityUtils.when(SecurityUtils::getSubject).thenReturn(subject);

                assertFalse(shiroAuthHandler.isAthenticated());
            }
        }

        @Test
        void shallReturnTrueIfSubjectIsAuthenticated() {
            final var subject = mock(Subject.class);
            doReturn(true).when(subject).isAuthenticated();
            try (MockedStatic<SecurityUtils> securityUtils = Mockito.mockStatic(SecurityUtils.class)) {
                securityUtils.when(SecurityUtils::getSubject).thenReturn(subject);

                assertTrue(shiroAuthHandler.isAthenticated());
            }
        }
    }
}
