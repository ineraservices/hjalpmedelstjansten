package se.inera.hjalpmedelstjansten.business.agreement.controller;

import org.junit.Assert;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelist;

import java.util.Calendar;


public class AgreementPricelistMapperTest {
    
    private static final long id = 1l;
    
    @Test
    public void testGetPricelistStatusFuture() {
        Calendar validFrom = Calendar.getInstance();
        validFrom.add(Calendar.DAY_OF_YEAR, 10);
        AgreementPricelist toTest = new AgreementPricelist();
        toTest.setValidFrom(validFrom.getTime());

        Agreement agTest = new Agreement();
        agTest.setStatus(Agreement.Status.CURRENT);
        toTest.setAgreement(agTest);

        AgreementPricelist currentPricelist = new AgreementPricelist();
        AgreementPricelist.Status status = AgreementPricelistMapper.getPricelistStatus(toTest, currentPricelist);
        Assert.assertEquals(AgreementPricelist.Status.FUTURE, status);
    }
    
    @Test
    public void testGetPricelistStatusCurrentPricelistSameAsNew() {
        Calendar validFrom = Calendar.getInstance();
        validFrom.add(Calendar.HOUR_OF_DAY, 0);
        validFrom.add(Calendar.MINUTE, 0);
        validFrom.add(Calendar.SECOND, 0);
        AgreementPricelist toTest = new AgreementPricelist();
        toTest.setUniqueId(id);
        toTest.setValidFrom(validFrom.getTime());

        Agreement agTest = new Agreement();
        agTest.setStatus(Agreement.Status.CURRENT);
        toTest.setAgreement(agTest);

        AgreementPricelist currentPricelist = new AgreementPricelist();
        currentPricelist.setUniqueId(id);
        AgreementPricelist.Status status = AgreementPricelistMapper.getPricelistStatus(toTest, currentPricelist);
        Assert.assertEquals(AgreementPricelist.Status.CURRENT, status);
    }
     
    @Test
    public void testGetPricelistStatusNoCurrentPricelist() {
        Calendar validFrom = Calendar.getInstance();
        validFrom.add(Calendar.HOUR_OF_DAY, 0);
        validFrom.add(Calendar.MINUTE, 0);
        validFrom.add(Calendar.SECOND, 0);
        AgreementPricelist toTest = new AgreementPricelist();
        toTest.setUniqueId(id);
        toTest.setValidFrom(validFrom.getTime());

        Agreement agTest = new Agreement();
        agTest.setStatus(Agreement.Status.CURRENT);
        toTest.setAgreement(agTest);

        AgreementPricelist.Status status = AgreementPricelistMapper.getPricelistStatus(toTest, null);
        Assert.assertEquals(AgreementPricelist.Status.CURRENT, status);
    }
    
}
