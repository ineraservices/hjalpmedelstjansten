package se.inera.hjalpmedelstjansten.business.product.controller;

import org.junit.Before;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Category;


public class CategoryControllerTest {
    
    static final Article.Type PRODUCT_CATEGORY_ARTICLE_TYPE = Article.Type.T;
    static final String PRODUCT_CATEGORY_NAME = "product category name";
    static final String PRODUCT_CATEGORY_CODE = "category code";

    @Before
    public void init() {
        
    }
    
    public static Category createValidCategory(Long categoryId) {
        Category category = new Category();
        category.setUniqueId(categoryId);
        category.setArticleType(PRODUCT_CATEGORY_ARTICLE_TYPE);
        category.setName(PRODUCT_CATEGORY_NAME);
        category.setCode(PRODUCT_CATEGORY_CODE);
        return category;
    }
    
    public static Category createValidCategory(Long categoryId, Article.Type type, String code) {
        Category category = new Category();
        category.setUniqueId(categoryId);
        category.setArticleType(type);
        category.setName(PRODUCT_CATEGORY_NAME);
        category.setCode(code);
        return category;
    }
    
    public static Category createValidExtendedCategory() {
        Category category = new Category();
        category.setArticleType(PRODUCT_CATEGORY_ARTICLE_TYPE);
        category.setName(PRODUCT_CATEGORY_NAME);
        category.setCode(PRODUCT_CATEGORY_CODE);
        return category;
    }
    
}
