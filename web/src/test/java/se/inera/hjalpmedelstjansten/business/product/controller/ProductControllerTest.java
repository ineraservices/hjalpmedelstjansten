package se.inera.hjalpmedelstjansten.business.product.controller;

import static org.mockito.Mockito.mock;
import static se.inera.hjalpmedelstjansten.model.api.validation.ProductAPITest.createValidCategoryAPI;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jakarta.enterprise.event.Event;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowController;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistRowController;
import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationControllerTest;
import se.inera.hjalpmedelstjansten.business.product.service.SendMailRegardingDiscontinuedArticlesOnAssortment;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.ResourceSpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEDirectiveAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEStandardAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVOrderUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPackageUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.UserAPITest;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificPropertyListValue;
import se.inera.hjalpmedelstjansten.model.entity.ElectronicAddress;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValue;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueDecimal;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueInterval;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueTextField;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueValueListMultiple;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueValueListSingle;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEDirective;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEStandard;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;


public class ProductControllerTest {

    ProductController productController;
    ArticleController articleController;
    OrderUnitController unitController;
    PackageUnitController packageUnitController;
    OrganizationController organizationController;
    CategoryController categoryController;
    CeController ceController;
    EntityManager myEntityManager;
    Event<InternalAuditEvent> event;
    CVPreventiveMaintenanceController cVPreventiveMaintenanceController;
    UserController userController;
    ElasticSearchController elasticSearchController;
    UserAPI userAPI;
    AgreementPricelistRowController agreementPricelistRowController;
    GeneralPricelistPricelistRowController generalPricelistPricelistRowController;
    SendMailRegardingDiscontinuedArticlesOnAssortment sendMailRegardingDiscontinuedArticlesOnAssortment;

    static String productName = "Product name";
    static String productNumber = "Product Number";
    static Product.Status productStatus = Product.Status.PUBLISHED;
    static String productSupplementedInformation = "Product supplemented information";
    static String descriptionForElvasjuttiosju = "Description for 1177";
    static String mainImageUrl = "test.png";
    static String mainImageAltText = "test";
    static String productTrademark = "Product trademark";
    static String productManufacturer = "Product manufacturer";
    static String productManufacturerProductNumber = "Product manufacturer product number";
    static String productManufacturerWeb = "https://www.inera.se";
    static boolean productCeMarked = true;
    static long productCeDirectiveId = 10;
    static String productCeDirectiveName = "Ce Directive name";
    static long productCeStandardId = 11;
    static String productCeStandardName = "Ce Standard name";
    static String productPreventiveMaintenanceDescription = "Preventive maintenance description";
    static int productPreventiveMaintenanceNumberOfDays = 30;
    static String productOrderUnitName = "Product order unit name";
    static String productOrderUnitCode = "Product order unit code";
    static Double productArticleQuantityInOuterPackage = 2.0;
    static Double productPackageContent = 1.0;
    static Integer productPackageLevelBase = 1;
    static Integer productPackageLevelMiddle = 2;
    static Integer productPackageLevelTop = 3;
    static String organizationName = "organization name";
    static String preventiveMaintenanceUnitCode = "code";
    static String preventiveMaintenanceUnitName = "name";
    static CVPreventiveMaintenance productPreventiveMaintenanceValidFrom = PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenance(preventiveMaintenanceUnitCode, preventiveMaintenanceUnitName);
    static Map<Long, CVOrderUnit> orderUnitIdMap;
    static Map<Long, CVPackageUnit> packageUnitIdMap;

    static String resourceSpecificPropertyValueTextField = "text data";
    static double resourceSpecificPropertyValueDecimal = 1.0;
    static double resourceSpecificPropertyValueIntervalFrom = 1.0;
    static double resourceSpecificPropertyValueIntervalTo = 10.0;

    static long productId = 1;
    static long organizationId = 2;
    static long categoryId = 3;
    static long directiveId = 4;
    static long standardId = 5;
    static long productOrderUnitId = 6;

    static long fitsToProductHId = 6;
    static long fitsToProductTId = 7;
    static long fitsToProductIId = 8;
    static long fitsToProductTjId = 9;
    static long fitsToProductRId = 10;
    static long fitsToProductTNoCodeId = 11;
    static long userId = 12;
    static long categorySpecificPropertyIdTextField = 13;
    static long categorySpecificPropertyIdDecimal = 14;
    static long categorySpecificPropertyIdInterval = 15;
    static long categorySpecificPropertyIdValueListSingle = 16;
    static long categorySpecificPropertyIdValueListMultiple = 17;
    static long resourceSpecificPropertyListValue1 = 18;
    static long resourceSpecificPropertyListValue2 = 19;
    static long resourceSpecificPropertyListValue3 = 20;
    static long resourceSpecificPropertyValueId = 21;

    @Before
    public void init() {
        userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.SupplierProductAndArticleHandler, null);

        myEntityManager = Mockito.mock(EntityManager.class);
        Mockito.when(myEntityManager.find(Product.class, productId)).thenReturn(createValidProduct(productId, organizationId));
        productController = new ProductController();
        productController.LOG = new NoLogger();
        productController.em = myEntityManager;
        organizationController = Mockito.mock(OrganizationController.class);
        Mockito.when(organizationController.getOrganization(organizationId)).thenReturn(OrganizationControllerTest.createValidOrganization(organizationId, Organization.OrganizationType.SUPPLIER, new Date(), null, organizationName));
        Query mockQuery = Mockito.mock(Query.class);
        Mockito.when(mockQuery.setParameter(Mockito.anyString(), Mockito.any())).thenReturn(mockQuery);
        Mockito.when(mockQuery.getResultList()).thenReturn(new ArrayList());
        Mockito.when(myEntityManager.createNamedQuery(Product.FIND_BY_PRODUCT_NUMBER_AND_ORGANIZATION)).thenReturn(mockQuery);
        Mockito.when(myEntityManager.createNamedQuery(Article.GET_IDS_BASED_ON_PRODUCT)).thenReturn(mockQuery);
        Mockito.when(myEntityManager.createNamedQuery(Article.GET_IDS_FITS_TO_PRODUCT)).thenReturn(mockQuery);
        productController.organizationController = organizationController;
        categoryController = Mockito.mock(CategoryController.class);
        List<Category> categorys = new ArrayList<>();
        categorys.add(CategoryControllerTest.createValidCategory(categoryId));
        Mockito.when(categoryController.getChildlessById(categoryId)).thenReturn(categorys);
        CategorySpecificPropertyListValue categorySpecificPropertyListValue1 = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue1.setUniqueId(resourceSpecificPropertyListValue1);
        Mockito.when(categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyListValue1)).thenReturn(categorySpecificPropertyListValue1);
        CategorySpecificPropertyListValue categorySpecificPropertyListValue2 = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue2.setUniqueId(resourceSpecificPropertyListValue2);
        Mockito.when(categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyListValue2)).thenReturn(categorySpecificPropertyListValue2);
        CategorySpecificPropertyListValue categorySpecificPropertyListValue3 = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue3.setUniqueId(resourceSpecificPropertyListValue3);
        Mockito.when(categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyListValue3)).thenReturn(categorySpecificPropertyListValue3);
        productController.categoryController = categoryController;
        event = Mockito.mock(Event.class);
        productController.internalAuditEvent = event;
        ValidationMessageService validationMessageService = Mockito.mock(ValidationMessageService.class);
        Mockito.when(validationMessageService.generateValidationException(Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer<HjalpmedelstjanstenValidationException>() {
            @Override
            public HjalpmedelstjanstenValidationException answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException((String) args[0] + "->" + (String) args[1]);
                exception.addValidationMessage((String) args[0], (String) args[1]);
                return exception;
            }
        });
        productController.validationMessageService = validationMessageService;

        articleController = Mockito.mock(ArticleController.class);

        ProductValidation productValidation = new ProductValidation();
        productValidation.LOG = new NoLogger();
        productValidation.validationMessageService = validationMessageService;
        productValidation.productController = productController;
        productValidation.articleController = articleController;
        productController.productValidation = productValidation;

        ceController = Mockito.mock(CeController.class);
        productController.ceController = ceController;
        CVCEStandard standard = new CVCEStandard();
        standard.setUniqueId(standardId);
        CVCEDirective directive = new CVCEDirective();
        directive.setUniqueId(directiveId);
        Mockito.when(ceController.findCEDirectiveById(directiveId)).thenReturn(directive);
        Mockito.when(ceController.findCEStandardById(standardId)).thenReturn(standard);
        unitController = Mockito.mock(OrderUnitController.class);
        productController.orderUnitController = unitController;
        CVOrderUnit unit = createValidUnit();
        Mockito.when(unitController.getUnit(productOrderUnitId)).thenReturn(unit);
        orderUnitIdMap = new HashMap<>();
        orderUnitIdMap.put(unit.getUniqueId(), unit);
        Mockito.when(unitController.findAllAsIdMap()).thenReturn(orderUnitIdMap);

        packageUnitController = Mockito.mock(PackageUnitController.class);
        productController.packageUnitController = packageUnitController;
        CVPackageUnit packageUnit = createValidPackageUnit();
        Mockito.when(packageUnitController.getUnit(productOrderUnitId)).thenReturn(packageUnit);
        packageUnitIdMap = new HashMap<>();
        packageUnitIdMap.put(unit.getUniqueId(), packageUnit);
        Mockito.when(packageUnitController.findAllAsIdMap()).thenReturn(packageUnitIdMap);

        Mockito.when(productController.getProduct(organizationId, fitsToProductHId, userAPI)).thenReturn(createValidProduct(fitsToProductHId, organizationId, Article.Type.H, "test"));
        Mockito.when(productController.getProduct(organizationId, fitsToProductIId, userAPI)).thenReturn(createValidProduct(fitsToProductIId, organizationId, Article.Type.I, "test"));
        Mockito.when(productController.getProduct(organizationId, fitsToProductRId, userAPI)).thenReturn(createValidProduct(fitsToProductRId, organizationId, Article.Type.R, "test"));
        Mockito.when(productController.getProduct(organizationId, fitsToProductTjId, userAPI)).thenReturn(createValidProduct(fitsToProductTjId, organizationId, Article.Type.Tj, "test"));
        Mockito.when(productController.getProduct(organizationId, fitsToProductTId, userAPI)).thenReturn(createValidProduct(fitsToProductTId, organizationId, Article.Type.T, "test"));
        Mockito.when(productController.getProduct(organizationId, fitsToProductTNoCodeId, userAPI)).thenReturn(createValidProduct(fitsToProductTId, organizationId, Article.Type.T, null));
        cVPreventiveMaintenanceController = Mockito.mock(CVPreventiveMaintenanceController.class);
        Mockito.when(cVPreventiveMaintenanceController.findByCode(preventiveMaintenanceUnitCode)).thenReturn(PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenance(preventiveMaintenanceUnitCode, preventiveMaintenanceUnitName));
        productController.cVPreventiveMaintenanceController = cVPreventiveMaintenanceController;

        userController = Mockito.mock(UserController.class);
        productController.userController = userController;

        elasticSearchController = Mockito.mock(ElasticSearchController.class);
        productController.elasticSearchController = elasticSearchController;

        agreementPricelistRowController = Mockito.mock(AgreementPricelistRowController.class);
        productController.agreementPricelistRowController = agreementPricelistRowController;

        generalPricelistPricelistRowController = Mockito.mock(GeneralPricelistPricelistRowController.class);
        articleController.generalPricelistPricelistRowController = generalPricelistPricelistRowController;

        sendMailRegardingDiscontinuedArticlesOnAssortment = mock(SendMailRegardingDiscontinuedArticlesOnAssortment.class);
        articleController.sendMailRegardingDiscontinuedArticlesOnAssortment = sendMailRegardingDiscontinuedArticlesOnAssortment;
    }

    @Test
    public void testGetProductValidOrganization() {
        Product product = productController.getProduct(organizationId, productId, userAPI);
        Assert.assertNotNull(product);
    }

    @Test
    public void testGetProductWrongUniqueId() {
        Product product = productController.getProduct(organizationId, -1l, userAPI);
        Assert.assertNull(product);
    }

//    @Test
//    public void testGetProductInvalidOrganization() {
//        long invalidOrganizationUniqueId = 3l;
//        Product validProduct = createValidProduct(productId, organizationId);
//        validProduct.getOrganization().setUniqueId(invalidOrganizationUniqueId);
//        Mockito.when(myEntityManager.find(Product.class, productId)).thenReturn(validProduct);
//        Product product = productController.getProduct(organizationId, productId, userAPI);
//        Assert.assertNull(product);
//    }

    @Test
    public void testGetProductAPI() {
        Product validProduct = createValidProduct(productId, organizationId);
        Mockito.when(myEntityManager.find(Product.class, productId)).thenReturn(validProduct);
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI productAPI = productController.getProductAPI(organizationId, productId, userAPI, null, null);
        Assert.assertNotNull(productAPI);
        Assert.assertEquals(productId, productAPI.getId().longValue());
        Assert.assertEquals(productStatus.toString(), productAPI.getStatus());
        Assert.assertEquals(organizationName, productAPI.getOrganizationName());
        Assert.assertEquals(productSupplementedInformation, productAPI.getSupplementedInformation());
        Assert.assertEquals(descriptionForElvasjuttiosju, productAPI.getDescriptionForElvasjuttiosju());

        // validate category
        Assert.assertNotNull(productAPI.getCategory());
        Assert.assertEquals(CategoryControllerTest.PRODUCT_CATEGORY_ARTICLE_TYPE, productAPI.getCategory().getArticleType());
        Assert.assertEquals(CategoryControllerTest.PRODUCT_CATEGORY_NAME, productAPI.getCategory().getName());
        Assert.assertEquals(CategoryControllerTest.PRODUCT_CATEGORY_CODE, productAPI.getCategory().getCode());

        // validate extended category
        Assert.assertNotNull(productAPI.getExtendedCategories());
        Assert.assertEquals(1, productAPI.getExtendedCategories().size());
        CategoryAPI extendedCategoryAPI = productAPI.getExtendedCategories().get(0);
        Assert.assertNotNull(extendedCategoryAPI);
        Assert.assertEquals(CategoryControllerTest.PRODUCT_CATEGORY_ARTICLE_TYPE, extendedCategoryAPI.getArticleType());
        Assert.assertEquals(CategoryControllerTest.PRODUCT_CATEGORY_NAME, extendedCategoryAPI.getName());
        Assert.assertEquals(CategoryControllerTest.PRODUCT_CATEGORY_CODE, extendedCategoryAPI.getCode());

        // validate manufacturer
        Assert.assertEquals(productTrademark, productAPI.getTrademark());
        Assert.assertEquals(productManufacturer, productAPI.getManufacturer());
        Assert.assertEquals(productManufacturerProductNumber, productAPI.getManufacturerProductNumber());
        Assert.assertEquals(productManufacturerWeb ,productAPI.getManufacturerElectronicAddress().getWeb());

        // validate ce
        Assert.assertTrue(productAPI.isCeMarked());

        // validate ce directive
        Assert.assertNotNull(productAPI.getCeDirective());
        Assert.assertEquals(productCeDirectiveId, productAPI.getCeDirective().getId());
        Assert.assertEquals(productCeDirectiveName, productAPI.getCeDirective().getName());

        // validate ce standard
        Assert.assertNotNull(productAPI.getCeStandard());
        Assert.assertEquals(productCeStandardId, productAPI.getCeStandard().getId());
        Assert.assertEquals(productCeStandardName, productAPI.getCeStandard().getName());

        // validate preventive maintenance
        Assert.assertEquals(productPreventiveMaintenanceDescription, productAPI.getPreventiveMaintenanceDescription());
        Assert.assertEquals(productPreventiveMaintenanceNumberOfDays, productAPI.getPreventiveMaintenanceNumberOfDays().intValue());
        Assert.assertEquals(productPreventiveMaintenanceValidFrom.getCode(), productAPI.getPreventiveMaintenanceValidFrom().getCode());
    }

    // CREATE PRODUCT

    @Test
    public void testCreateProduct() throws HjalpmedelstjanstenValidationException {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        ProductAPI productAPI = productController.createProduct(organizationId, validProductAPI, userAPI, null, null, false);
        Assert.assertNotNull(productAPI);
    }

    @Test
    public void testCreateProductTrimProductNumber() throws HjalpmedelstjanstenValidationException {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        String validProductNumber = validProductAPI.getProductNumber();
        validProductAPI.setProductNumber("   " + validProductNumber);
        ProductAPI productAPI = productController.createProduct(organizationId, validProductAPI, userAPI, null, null, false);
        Assert.assertNotNull(productAPI);
        Assert.assertEquals(validProductNumber, productAPI.getProductNumber());
    }

    @Test
    public void testCreateInvalidProductNoName() {
        // we only test one case here to make sure error is thrown, other bean
        // validation is made in the ProductAPITest unit test
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        validProductAPI.setProductName(null);
        try {
            productController.createProduct(organizationId, validProductAPI, userAPI, null, null, false);
            Assert.fail("Create product successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("productName", errorMessageAPI.getField());
        }
    }

    @Test
    public void testCreateInvalidOrganization() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        try {
            productController.createProduct(-1l, validProductAPI, userAPI, null, null, false);
            Assert.fail("Create product successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("organizationId", errorMessageAPI.getField());
        }
    }

    @Test
    public void testCreateInvalidCategory() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        validProductAPI.getCategory().setId(-1l);
        try {
            productController.createProduct(organizationId, validProductAPI, userAPI, null, null, false);
            Assert.fail("Create product successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("category", errorMessageAPI.getField());
        }
    }

    @Test
    public void testCreateInvalidExtendedCategory() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        validProductAPI.getExtendedCategories().get(0).setId(-1l);
        try {
            productController.createProduct(organizationId, validProductAPI, userAPI, null, null, false);
            Assert.fail("Create product successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("extendedCategories", errorMessageAPI.getField());
        }
    }

    @Test
    public void testCreateInvalidDirective() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        validProductAPI.getCeDirective().setId(-1l);
        try {
            productController.createProduct(organizationId, validProductAPI, userAPI, null, null, false);
            Assert.fail("Create product successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("ceDirective", errorMessageAPI.getField());
        }
    }

    @Test
    public void testCreateInvalidStandard() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        validProductAPI.getCeStandard().setId(-1l);
        try {
            productController.createProduct(organizationId, validProductAPI, userAPI, null, null, false);
            Assert.fail("Create product successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("ceStandard", errorMessageAPI.getField());
        }
    }

    @Test
    public void testCreateInvalidManufacturerFieldSet() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        validProductAPI.getManufacturerElectronicAddress().setMobile("1234556");
        try {
            productController.createProduct(organizationId, validProductAPI, userAPI, null, null, false);
            Assert.fail("Create product successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("web", errorMessageAPI.getField());
        }
    }

    // UPDATE PRODUCT

/*    @Test
    public void testUpdateProduct() throws HjalpmedelstjanstenValidationException {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        ProductAPI productAPI = productController.updateProduct(organizationId, productId, validProductAPI, userAPI, null, null, false);
        Assert.assertNotNull(productAPI);
        Assert.assertEquals(Product.Status.PUBLISHED.toString(), productAPI.getStatus());
    }*/

/*    @Test
    public void testUpdateProductButNotProductNumber() throws HjalpmedelstjanstenValidationException {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        Query mockQuery = Mockito.mock(Query.class);
        Mockito.when(mockQuery.setParameter(Mockito.anyString(), Mockito.any())).thenReturn(mockQuery);
        List<Long> articlesFitsToArticle = new ArrayList<>();
        articlesFitsToArticle.add(fitsToProductHId);
        Mockito.when(mockQuery.getResultList()).thenReturn(articlesFitsToArticle);
        Mockito.when(myEntityManager.createNamedQuery(Article.GET_IDS_BASED_ON_PRODUCT)).thenReturn(mockQuery);
        Mockito.when(agreementPricelistRowController.existRowsByArticle(articlesFitsToArticle)).thenReturn(true);
        ProductAPI validProductAPI = createValidProductAPI();
        String newProductNumber = "new" + productNumber;
        validProductAPI.setProductNumber(newProductNumber);
        ProductAPI productAPI = productController.updateProduct(organizationId, productId, validProductAPI, userAPI, null, null, false);
        Assert.assertNotNull(productAPI);
        Assert.assertEquals(productNumber, productAPI.getProductNumber());
    }*/

    /*
    @Test
    public void testUpdateProductWithProductNumber() throws HjalpmedelstjanstenValidationException {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        String newProductNumber = "new" + productNumber;
        validProductAPI.setProductNumber(newProductNumber);
        ProductAPI productAPI = productController.updateProduct(organizationId, productId, validProductAPI, userAPI, null, null, false);
        Assert.assertNotNull(productAPI);
        Assert.assertEquals(newProductNumber, productAPI.getProductNumber());
    }*/

/*    @Test
    public void testUpdateInvalidCategory() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        validProductAPI.getCategory().setId(-1l);
        try {
            productController.updateProduct(organizationId, productId, validProductAPI, userAPI, null, null, false);
            Assert.fail("Update product successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("category", errorMessageAPI.getField());
        }
    }*/
/*
    @Test
    public void testUpdateInvalidExtendedCategory() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        validProductAPI.getExtendedCategories().get(0).setId(-1l);
        try {
            productController.updateProduct(organizationId, productId, validProductAPI, userAPI, null, null, false);
            Assert.fail("Update product successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("extendedCategories", errorMessageAPI.getField());
        }
    }*/

/*    @Test
    public void testUpdateInvalidDirective() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        validProductAPI.getCeDirective().setId(-1l);
        try {
            productController.updateProduct(organizationId, productId, validProductAPI, userAPI, null, null, false);
            Assert.fail("Update product successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("ceDirective", errorMessageAPI.getField());
        }
    }*/

/*    @Test
    public void testUpdateInvalidStandard() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        validProductAPI.getCeStandard().setId(-1l);
        try {
            productController.updateProduct(organizationId, productId, validProductAPI, userAPI, null, null, false);
            Assert.fail("Update product successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("ceStandard", errorMessageAPI.getField());
        }
    }*/

/*    @Test
    public void testUpdateInvalidManufacturerFieldSet() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        validProductAPI.getManufacturerElectronicAddress().setMobile("1234556");
        try {
            productController.updateProduct(organizationId, productId, validProductAPI, userAPI, null, null, false);
            Assert.fail("Update product successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("web", errorMessageAPI.getField());
        }
    }*/

/*    @Test
    public void testUpdateInvalidStatus() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        validProductAPI.setStatus(Product.Status.DISCONTINUED.toString());
        validProductAPI.setReplacementDate(1l);
        validProductAPI.setInactivateRowsOnReplacement(Boolean.TRUE);
        Query mockQuery = Mockito.mock(Query.class);
        Mockito.when(mockQuery.setParameter(Mockito.anyString(), Mockito.any())).thenReturn(mockQuery);
        Mockito.when(mockQuery.getResultList()).thenReturn(null);
        Mockito.when(myEntityManager.createNamedQuery(Article.SEARCH_BASED_ON_PRODUCT)).thenReturn(mockQuery);
        try {
            productController.updateProduct(organizationId, productId, validProductAPI, userAPI, null, null, false);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Update product failed but should succeed");
        }
    }*/

/*    @Test
    public void testUpdateReplacementDateFuture() throws HjalpmedelstjanstenValidationException {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ProductAPI validProductAPI = createValidProductAPI();
        Product publishedProduct = createValidProduct(productId, organizationId);
        publishedProduct.setStatus(Product.Status.PUBLISHED);
        Mockito.when(myEntityManager.find(Product.class, productId)).thenReturn(publishedProduct);
        validProductAPI.setReplacementDate(System.currentTimeMillis() + 100000);
        validProductAPI.setInactivateRowsOnReplacement(Boolean.TRUE);
        ProductAPI productAPI = productController.updateProduct(organizationId, productId, validProductAPI, userAPI, null, null, false);
        Assert.assertEquals(Product.Status.PUBLISHED, Product.Status.valueOf(productAPI.getStatus()));
    }*/

/*    @Test
    public void testUpdateReplacementDatePast() throws HjalpmedelstjanstenValidationException {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        Product publishedProduct = createValidProduct(productId, organizationId);
        publishedProduct.setStatus(Product.Status.PUBLISHED);
        Mockito.when(myEntityManager.find(Product.class, productId)).thenReturn(publishedProduct);

        ProductAPI validProductAPI = createValidProductAPI();
        validProductAPI.setReplacementDate(System.currentTimeMillis() - 100000);
        validProductAPI.setInactivateRowsOnReplacement(Boolean.TRUE);
        Query mockQuery = Mockito.mock(Query.class);
        Mockito.when(mockQuery.setParameter(Mockito.anyString(), Mockito.any())).thenReturn(mockQuery);
        Mockito.when(mockQuery.getResultList()).thenReturn(null);
        Mockito.when(myEntityManager.createNamedQuery(Article.SEARCH_BASED_ON_PRODUCT)).thenReturn(mockQuery);
        ProductAPI productAPI = productController.updateProduct(organizationId, productId, validProductAPI, userAPI, null, null, false);
        Assert.assertEquals(Product.Status.DISCONTINUED, Product.Status.valueOf(productAPI.getStatus()));
    }*/

/*    @Test
    public void testUpdateReplacementDateOnDiscontinuedProduct() throws HjalpmedelstjanstenValidationException {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        Product discontinuedProduct = createValidProduct(productId, organizationId);
        discontinuedProduct.setStatus(Product.Status.DISCONTINUED);
        discontinuedProduct.setReplacementDate(new Date());
        Mockito.when(myEntityManager.find(Product.class, productId)).thenReturn(discontinuedProduct);
        ProductAPI validProductAPI = createValidProductAPI();
        validProductAPI.setReplacementDate(System.currentTimeMillis() - 100000000);
        validProductAPI.setInactivateRowsOnReplacement(Boolean.TRUE);
        try {
            productController.updateProduct(organizationId, productId, validProductAPI, userAPI, null, null, false);

        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
//            Assert.fail("Update product fail");
//            Assert.assertEquals(1, ex.getValidationMessages().size());
//            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
//            Assert.assertEquals("replacementDate", errorMessageAPI.getField());
        }
    }*/

    @Test
    public void testSetExtendedCategoriesNoProductCategoryCode() {
        ProductAPI productAPI = createValidProductAPI();
        CategoryAPI extendedCategoryAPI = new CategoryAPI();
        extendedCategoryAPI.setId(categoryId);
        List<CategoryAPI> extendedCategoriesAPI = new ArrayList<>();
        extendedCategoriesAPI.add(extendedCategoryAPI);
        productAPI.setExtendedCategories(extendedCategoriesAPI);

        Product validProduct = createValidProduct(productId, organizationId);
        validProduct.getCategory().setCode(null);
        try {
            productController.setExtendedCategories(productAPI, validProduct);
            Assert.fail("Update article successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("extendedCategories", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetCategoriesInvalidExtendedCategory() {
        ProductAPI productAPI = createValidProductAPI();
        CategoryAPI extendedCategoryAPI = new CategoryAPI();
        extendedCategoryAPI.setId(-1l);
        List<CategoryAPI> extendedCategoriesAPI = new ArrayList<>();
        extendedCategoriesAPI.add(extendedCategoryAPI);
        productAPI.setExtendedCategories(extendedCategoriesAPI);

        Product validProduct = createValidProduct(productId, organizationId);
        try {
            productController.setExtendedCategories(productAPI, validProduct);
            Assert.fail("Create article successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("extendedCategories", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetCategorySpecificPropertyAllNull() {
        ProductAPI productAPI = createValidProductAPI();
        Product validProduct = createValidProduct(productId, organizationId);
        try {
            productController.setResourceSpecificProperties(productAPI, validProduct, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testSetCategorySpecificPropertyOnePropertyInPossible() {
        ProductAPI productAPI = createValidProductAPI();
        Product validProduct = createValidProduct(productId, organizationId);
        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setType(CategorySpecificProperty.Type.TEXTFIELD);
        categorySpecificPropertys.add(categorySpecificProperty);

        try {
            productController.setResourceSpecificProperties(productAPI, validProduct, categorySpecificPropertys, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testSetCategorySpecificPropertyOneValidInAPI() {
        ProductAPI productAPI = createValidProductAPI();
        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdTextField);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        productAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);
        Product validProduct = createValidProduct(productId, organizationId);
        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdTextField);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.TEXTFIELD);
        categorySpecificPropertys.add(categorySpecificProperty);

        try {
            productController.setResourceSpecificProperties(productAPI, validProduct, categorySpecificPropertys, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testSetCategorySpecificPropertyOneInvalidInAPI() {
        ProductAPI productAPI = createValidProductAPI();
        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdTextField);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        productAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);
        Product validProduct = createValidProduct(productId, organizationId);
        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdTextField+1);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.TEXTFIELD);
        categorySpecificPropertys.add(categorySpecificProperty);

        try {
            productController.setResourceSpecificProperties(productAPI, validProduct, categorySpecificPropertys, null);
            Assert.fail("Set resource specific property successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("categoryProperties", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetCategorySpecificPropertyTextfield() {
        ProductAPI productAPI = createValidProductAPI();
        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdTextField);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPI.setTextValue(resourceSpecificPropertyValueTextField);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        productAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);
        Product validProduct = createValidProduct(productId, organizationId);
        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdTextField);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.TEXTFIELD);
        categorySpecificPropertys.add(categorySpecificProperty);

        try {
            productController.setResourceSpecificProperties(productAPI, validProduct, categorySpecificPropertys, null);
            Assert.assertNotNull(validProduct.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, validProduct.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(validProduct.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueTextField);
            ResourceSpecificPropertyValueTextField textfield = ( ResourceSpecificPropertyValueTextField ) validProduct.getResourceSpecificPropertyValues().get(0);
            Assert.assertEquals(resourceSpecificPropertyValueTextField, textfield.getValue());
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testSetCategorySpecificPropertyDecimal() {
        ProductAPI productAPI = createValidProductAPI();
        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdDecimal);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPI.setDecimalValue(resourceSpecificPropertyValueDecimal);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        productAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);
        Product validProduct = createValidProduct(productId, organizationId);
        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdDecimal);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.DECIMAL);
        categorySpecificPropertys.add(categorySpecificProperty);

        try {
            productController.setResourceSpecificProperties(productAPI, validProduct, categorySpecificPropertys, null);
            Assert.assertNotNull(validProduct.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, validProduct.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(validProduct.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueDecimal);
            ResourceSpecificPropertyValueDecimal decimalValue = ( ResourceSpecificPropertyValueDecimal ) validProduct.getResourceSpecificPropertyValues().get(0);
            Assert.assertTrue(resourceSpecificPropertyValueDecimal == decimalValue.getValue());
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testSetCategorySpecificPropertyInterval() {
        ProductAPI productAPI = createValidProductAPI();
        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdInterval);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPI.setIntervalFromValue(resourceSpecificPropertyValueIntervalFrom);
        resourceSpecificPropertyAPI.setIntervalToValue(resourceSpecificPropertyValueIntervalTo);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        productAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);
        Product validProduct = createValidProduct(productId, organizationId);
        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdInterval);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.INTERVAL);
        categorySpecificPropertys.add(categorySpecificProperty);

        try {
            productController.setResourceSpecificProperties(productAPI, validProduct, categorySpecificPropertys, null);
            Assert.assertNotNull(validProduct.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, validProduct.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(validProduct.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueInterval);
            ResourceSpecificPropertyValueInterval intervalValue = ( ResourceSpecificPropertyValueInterval ) validProduct.getResourceSpecificPropertyValues().get(0);
            Assert.assertTrue(resourceSpecificPropertyValueIntervalFrom == intervalValue.getFromValue());
            Assert.assertTrue(resourceSpecificPropertyValueIntervalTo == intervalValue.getToValue());
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testSetCategorySpecificPropertySingleList() {
        ProductAPI productAPI = createValidProductAPI();
        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdValueListSingle);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPI.setSingleListValue(resourceSpecificPropertyListValue1);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        productAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);
        Product validProduct = createValidProduct(productId, organizationId);
        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdValueListSingle);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.VALUELIST_SINGLE);
        categorySpecificPropertys.add(categorySpecificProperty);

        try {
            productController.setResourceSpecificProperties(productAPI, validProduct, categorySpecificPropertys, null);
            Assert.assertNotNull(validProduct.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, validProduct.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(validProduct.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueValueListSingle);
            ResourceSpecificPropertyValueValueListSingle singleValue = ( ResourceSpecificPropertyValueValueListSingle ) validProduct.getResourceSpecificPropertyValues().get(0);
            Assert.assertTrue(resourceSpecificPropertyListValue1 == singleValue.getValue().getUniqueId());
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testSetCategorySpecificPropertyMultipleList() {
        ProductAPI productAPI = createValidProductAPI();
        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdValueListMultiple);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        List<Long> multiListValues = new ArrayList<>();
        multiListValues.add(resourceSpecificPropertyListValue2);
        multiListValues.add(resourceSpecificPropertyListValue3);
        resourceSpecificPropertyAPI.setMultipleListValue(multiListValues);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        productAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);
        Product validProduct = createValidProduct(productId, organizationId);
        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdValueListMultiple);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.VALUELIST_MULTIPLE);
        categorySpecificPropertys.add(categorySpecificProperty);

        try {
            productController.setResourceSpecificProperties(productAPI, validProduct, categorySpecificPropertys, null);
            Assert.assertNotNull(validProduct.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, validProduct.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(validProduct.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueValueListMultiple);
            ResourceSpecificPropertyValueValueListMultiple multipleValue = ( ResourceSpecificPropertyValueValueListMultiple ) validProduct.getResourceSpecificPropertyValues().get(0);
            Assert.assertEquals(2, multipleValue.getValues().size());
            Assert.assertTrue(multiListValues.contains(multipleValue.getValues().get(0).getUniqueId()));
            Assert.assertTrue(multiListValues.contains(multipleValue.getValues().get(1).getUniqueId()));
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testUpdateCategorySpecificPropertyTextfield() {
        ProductAPI productAPI = createValidProductAPI();

        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        resourceSpecificPropertyAPI.setId(resourceSpecificPropertyValueId);
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdTextField);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPI.setTextValue(resourceSpecificPropertyValueTextField+resourceSpecificPropertyValueTextField);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        productAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);


        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdTextField);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.TEXTFIELD);
        categorySpecificPropertys.add(categorySpecificProperty);

        Product validProduct = createValidProduct(productId, organizationId);
        List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues = new ArrayList<>();
        ResourceSpecificPropertyValueTextField textfield = new ResourceSpecificPropertyValueTextField();
        textfield.setUniqueId(resourceSpecificPropertyValueId);
        textfield.setValue(resourceSpecificPropertyValueTextField);
        textfield.setCategorySpecificProperty(categorySpecificProperty);
        resourceSpecificPropertyValues.add(textfield);
        validProduct.setResourceSpecificPropertyValues(resourceSpecificPropertyValues);

        try {
            productController.setResourceSpecificProperties(productAPI, validProduct, categorySpecificPropertys, null);
            Assert.assertNotNull(validProduct.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, validProduct.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(validProduct.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueTextField);
            ResourceSpecificPropertyValueTextField updatedTextfield = ( ResourceSpecificPropertyValueTextField ) validProduct.getResourceSpecificPropertyValues().get(0);
            Assert.assertEquals(resourceSpecificPropertyValueTextField+resourceSpecificPropertyValueTextField, updatedTextfield.getValue());
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testUpdateCategorySpecificPropertyDecimal() {
        ProductAPI productAPI = createValidProductAPI();

        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        resourceSpecificPropertyAPI.setId(resourceSpecificPropertyValueId);
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdDecimal);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPI.setDecimalValue(resourceSpecificPropertyValueDecimal+resourceSpecificPropertyValueDecimal);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        productAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);


        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdDecimal);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.DECIMAL);
        categorySpecificPropertys.add(categorySpecificProperty);

        Product validProduct = createValidProduct(productId, organizationId);
        List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues = new ArrayList<>();
        ResourceSpecificPropertyValueDecimal decimalfield = new ResourceSpecificPropertyValueDecimal();
        decimalfield.setUniqueId(resourceSpecificPropertyValueId);
        decimalfield.setValue(resourceSpecificPropertyValueDecimal);
        decimalfield.setCategorySpecificProperty(categorySpecificProperty);
        resourceSpecificPropertyValues.add(decimalfield);
        validProduct.setResourceSpecificPropertyValues(resourceSpecificPropertyValues);

        try {
            productController.setResourceSpecificProperties(productAPI, validProduct, categorySpecificPropertys, null);
            Assert.assertNotNull(validProduct.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, validProduct.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(validProduct.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueDecimal);
            ResourceSpecificPropertyValueDecimal updatedDecimal = ( ResourceSpecificPropertyValueDecimal ) validProduct.getResourceSpecificPropertyValues().get(0);
            Assert.assertTrue((resourceSpecificPropertyValueDecimal+resourceSpecificPropertyValueDecimal) == updatedDecimal.getValue());
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testUpdateCategorySpecificPropertyInterval() {
        ProductAPI productAPI = createValidProductAPI();

        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        resourceSpecificPropertyAPI.setId(resourceSpecificPropertyValueId);
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdInterval);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPI.setIntervalFromValue(resourceSpecificPropertyValueIntervalFrom+resourceSpecificPropertyValueIntervalFrom);
        resourceSpecificPropertyAPI.setIntervalToValue(resourceSpecificPropertyValueIntervalTo+resourceSpecificPropertyValueIntervalTo);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        productAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);


        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdInterval);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.INTERVAL);
        categorySpecificPropertys.add(categorySpecificProperty);

        Product validProduct = createValidProduct(productId, organizationId);
        List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues = new ArrayList<>();
        ResourceSpecificPropertyValueInterval intervalField = new ResourceSpecificPropertyValueInterval();
        intervalField.setUniqueId(resourceSpecificPropertyValueId);
        intervalField.setFromValue(resourceSpecificPropertyValueIntervalFrom);
        intervalField.setToValue(resourceSpecificPropertyValueIntervalTo);
        intervalField.setCategorySpecificProperty(categorySpecificProperty);
        resourceSpecificPropertyValues.add(intervalField);
        validProduct.setResourceSpecificPropertyValues(resourceSpecificPropertyValues);

        try {
            productController.setResourceSpecificProperties(productAPI, validProduct, categorySpecificPropertys, null);
            Assert.assertNotNull(validProduct.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, validProduct.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(validProduct.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueInterval);
            ResourceSpecificPropertyValueInterval updatedInterval = ( ResourceSpecificPropertyValueInterval ) validProduct.getResourceSpecificPropertyValues().get(0);
            Assert.assertTrue((resourceSpecificPropertyValueIntervalFrom+resourceSpecificPropertyValueIntervalFrom) == updatedInterval.getFromValue());
            Assert.assertTrue((resourceSpecificPropertyValueIntervalTo+resourceSpecificPropertyValueIntervalTo) == updatedInterval.getToValue());
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testUpdateCategorySpecificPropertySingleValue() {
        ProductAPI productAPI = createValidProductAPI();

        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        resourceSpecificPropertyAPI.setId(resourceSpecificPropertyValueId);
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdValueListSingle);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPI.setSingleListValue(resourceSpecificPropertyListValue2);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        productAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);


        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdValueListSingle);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.VALUELIST_SINGLE);
        categorySpecificPropertys.add(categorySpecificProperty);

        Product validProduct = createValidProduct(productId, organizationId);
        List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues = new ArrayList<>();
        ResourceSpecificPropertyValueValueListSingle singleList = new ResourceSpecificPropertyValueValueListSingle();
        singleList.setUniqueId(resourceSpecificPropertyValueId);
        CategorySpecificPropertyListValue categorySpecificPropertyListValue = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue.setCategorySpecificProperty(categorySpecificProperty);
        categorySpecificPropertyListValue.setUniqueId(resourceSpecificPropertyListValue1);
        singleList.setValue(categorySpecificPropertyListValue);
        singleList.setCategorySpecificProperty(categorySpecificProperty);
        resourceSpecificPropertyValues.add(singleList);
        validProduct.setResourceSpecificPropertyValues(resourceSpecificPropertyValues);

        try {
            productController.setResourceSpecificProperties(productAPI, validProduct, categorySpecificPropertys, null);
            Assert.assertNotNull(validProduct.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, validProduct.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(validProduct.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueValueListSingle);
            ResourceSpecificPropertyValueValueListSingle singleValue = ( ResourceSpecificPropertyValueValueListSingle ) validProduct.getResourceSpecificPropertyValues().get(0);
            Assert.assertTrue(singleValue.getValue().getUniqueId() == resourceSpecificPropertyListValue2);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testUpdateCategorySpecificPropertyMultipleValue() {
        ProductAPI productAPI = createValidProductAPI();

        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        resourceSpecificPropertyAPI.setId(resourceSpecificPropertyValueId);
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdValueListMultiple);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        List<Long> multipleListValue = new ArrayList<>();
        multipleListValue.add(resourceSpecificPropertyListValue2);
        multipleListValue.add(resourceSpecificPropertyListValue3);
        resourceSpecificPropertyAPI.setMultipleListValue(multipleListValue);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        productAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);


        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdValueListMultiple);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.VALUELIST_MULTIPLE);
        categorySpecificPropertys.add(categorySpecificProperty);

        Product validProduct = createValidProduct(productId, organizationId);
        List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues = new ArrayList<>();
        ResourceSpecificPropertyValueValueListMultiple multiList = new ResourceSpecificPropertyValueValueListMultiple();
        multiList.setUniqueId(resourceSpecificPropertyValueId);
        List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues = new ArrayList<>();
        CategorySpecificPropertyListValue categorySpecificPropertyListValue = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue.setCategorySpecificProperty(categorySpecificProperty);
        categorySpecificPropertyListValue.setUniqueId(resourceSpecificPropertyListValue1);
        categorySpecificPropertyListValues.add(categorySpecificPropertyListValue);
        CategorySpecificPropertyListValue categorySpecificPropertyListValue2 = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue2.setCategorySpecificProperty(categorySpecificProperty);
        categorySpecificPropertyListValue2.setUniqueId(resourceSpecificPropertyListValue2);
        categorySpecificPropertyListValues.add(categorySpecificPropertyListValue2);
        multiList.setValues(categorySpecificPropertyListValues);
        multiList.setCategorySpecificProperty(categorySpecificProperty);
        resourceSpecificPropertyValues.add(multiList);
        validProduct.setResourceSpecificPropertyValues(resourceSpecificPropertyValues);

        try {
            productController.setResourceSpecificProperties(productAPI, validProduct, categorySpecificPropertys, null);
            Assert.assertNotNull(validProduct.getResourceSpecificPropertyValues());
            Assert.assertEquals(1, validProduct.getResourceSpecificPropertyValues().size());
            Assert.assertTrue(validProduct.getResourceSpecificPropertyValues().get(0) instanceof ResourceSpecificPropertyValueValueListMultiple);
            ResourceSpecificPropertyValueValueListMultiple multipleValue = ( ResourceSpecificPropertyValueValueListMultiple ) validProduct.getResourceSpecificPropertyValues().get(0);
            Assert.assertTrue(multipleListValue.contains(multipleValue.getValues().get(0).getUniqueId()));
            Assert.assertTrue(multipleListValue.contains(multipleValue.getValues().get(1).getUniqueId()));
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Set resource specific property failed");
        }
    }

    @Test
    public void testUpdateCategorySpecificPropertyAlreadySet() {
        ProductAPI productAPI = createValidProductAPI();

        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificPropertyIdTextField);
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);
        resourceSpecificPropertyAPI.setTextValue(resourceSpecificPropertyValueTextField+resourceSpecificPropertyValueTextField);
        resourceSpecificPropertyAPIs.add(resourceSpecificPropertyAPI);
        productAPI.setCategoryPropertys(resourceSpecificPropertyAPIs);


        List<CategorySpecificProperty> categorySpecificPropertys = new ArrayList<>();
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        categorySpecificProperty.setUniqueId(categorySpecificPropertyIdTextField);
        categorySpecificProperty.setType(CategorySpecificProperty.Type.TEXTFIELD);
        categorySpecificPropertys.add(categorySpecificProperty);

        Product validProduct = createValidProduct(productId, organizationId);
        List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues = new ArrayList<>();
        ResourceSpecificPropertyValueTextField textfield = new ResourceSpecificPropertyValueTextField();
        textfield.setUniqueId(resourceSpecificPropertyValueId);
        textfield.setValue(resourceSpecificPropertyValueTextField);
        textfield.setCategorySpecificProperty(categorySpecificProperty);
        resourceSpecificPropertyValues.add(textfield);
        validProduct.setResourceSpecificPropertyValues(resourceSpecificPropertyValues);

        try {
            productController.setResourceSpecificProperties(productAPI, validProduct, categorySpecificPropertys, null);
            Assert.fail("Set resource specific property successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("categoryProperties", errorMessageAPI.getField());
        }
    }

    public static Product createValidProduct(long productId, long organizationId) {
        Product product = new Product();
        product.setUniqueId(productId);
        product.setProductName(productName);
        product.setProductNumber(productNumber);
        product.setStatus(productStatus);
        product.setSupplementedInformation(productSupplementedInformation);
        product.setDescriptionForElvasjuttiosju(descriptionForElvasjuttiosju);
        product.setMainImageUrl(mainImageUrl);
        product.setMainImageAltText(mainImageAltText);

        // set manufacturer
        product.setTrademark(productTrademark);
        product.setManufacturer(productManufacturer);
        product.setManufacturerProductNumber(productManufacturerProductNumber);
        ElectronicAddress manufacturerElectronicAddress = new ElectronicAddress();
        manufacturerElectronicAddress.setWeb(productManufacturerWeb);
        product.setManufacturerElectronicAddress(manufacturerElectronicAddress);

        // preventive maintenance
        product.setPreventiveMaintenanceDescription(productPreventiveMaintenanceDescription);
        product.setPreventiveMaintenanceNumberOfDays(productPreventiveMaintenanceNumberOfDays);
        product.setPreventiveMaintenanceValidFrom(productPreventiveMaintenanceValidFrom);

        // create category
        product.setCategory(CategoryControllerTest.createValidCategory(categoryId));

        // create extended category
        List<Category> extendedCategorys = new ArrayList<>();
        extendedCategorys.add(CategoryControllerTest.createValidCategory(null));
        product.setExtendedCategories(extendedCategorys);

        product.setOrganization(OrganizationControllerTest.createValidOrganization(organizationId, Organization.OrganizationType.SUPPLIER, new Date(), null, organizationName));

        // set ce
        product.setCeMarked(productCeMarked);

        // create ce standard
        CVCEStandard cEStandard = new CVCEStandard();
        cEStandard.setUniqueId(productCeStandardId);
        cEStandard.setName(productCeStandardName);
        product.setCeStandard(cEStandard);

        // create ce directive
        CVCEDirective cEDirective = new CVCEDirective();
        cEDirective.setUniqueId(productCeDirectiveId);
        cEDirective.setName(productCeDirectiveName);
        product.setCeDirective(cEDirective);

        // order information
        CVOrderUnit unit = createValidUnit();
        CVPackageUnit packageUnit = createValidPackageUnit();
        product.setOrderUnit(unit);
        product.setArticleQuantityInOuterPackage(productArticleQuantityInOuterPackage);
        product.setArticleQuantityInOuterPackageUnit(unit);
        product.setPackageContent(productPackageContent);
        product.setPackageContentUnit(packageUnit);
        product.setPackageLevelBase(productPackageLevelBase);
        product.setPackageLevelMiddle(productPackageLevelMiddle);
        product.setPackageLevelTop(productPackageLevelTop);

        return product;
    }

    private static CVOrderUnit createValidUnit() {
        CVOrderUnit unit = new CVOrderUnit();
        unit.setUniqueId(productOrderUnitId);
        unit.setCode(productOrderUnitCode);
        unit.setName(productOrderUnitName);
        return unit;
    }

    private static CVPackageUnit createValidPackageUnit() {
        CVPackageUnit unit = new CVPackageUnit();
        unit.setUniqueId(productOrderUnitId);
        unit.setCode(productOrderUnitCode);
        unit.setName(productOrderUnitName);
        return unit;
    }

    public static ProductAPI createValidProductAPI() {
        ProductAPI productAPI = new ProductAPI();
        productAPI.setProductName("test");
        productAPI.setProductNumber("test");
        productAPI.setStatus(Product.Status.PUBLISHED.toString());
        productAPI.setCategory(createValidCategoryAPI());
        productAPI.setManufacturer("test");
        productAPI.setTrademark("test");

        productAPI.setMainImageUrl("https://upload.wikimedia.org/wikipedia/commons/thumb/2/28/JPG_Test.jpg/477px-JPG_Test.jpg");
        productAPI.setMainImageOriginalUrl("https://upload.wikimedia.org/wikipedia/commons/thumb/2/28/JPG_Test.jpg/477px-JPG_Test.jpg");
        productAPI.setMainImageAltText("test");

        productAPI.setPreventiveMaintenanceDescription("test");
        productAPI.setPreventiveMaintenanceNumberOfDays(365);
        productAPI.setPreventiveMaintenanceValidFrom(PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenanceAPI(preventiveMaintenanceUnitCode, preventiveMaintenanceUnitName));
        ElectronicAddressAPI manufacturerElectronicAddressAPI = new ElectronicAddressAPI();
        manufacturerElectronicAddressAPI.setWeb("http://test");
        productAPI.setManufacturerElectronicAddress(manufacturerElectronicAddressAPI);

        CategoryAPI categoryAPI = new CategoryAPI();
        categoryAPI.setId(categoryId);
        productAPI.setCategory(categoryAPI);

        CategoryAPI extendedCategoryAPI = new CategoryAPI();
        extendedCategoryAPI.setId(categoryId);
        List<CategoryAPI> extendedCategoriesAPI = new ArrayList<>();
        extendedCategoriesAPI.add(extendedCategoryAPI);
        productAPI.setExtendedCategories(extendedCategoriesAPI);

        productAPI.setCeMarked(true);
        productAPI.setCeDirective(createValidCEDirectiveAPI());
        productAPI.setCeStandard(createValidCEStandardAPI());

        productAPI.setOrderUnit(createValidOrderUnitAPI());
        productAPI.setArticleQuantityInOuterPackage(productArticleQuantityInOuterPackage);
        productAPI.setArticleQuantityInOuterPackageUnit(createValidOrderUnitAPI());
        productAPI.setPackageContent(productPackageContent);
        productAPI.setPackageContentUnit(createValidPackageUnitAPI());
        productAPI.setPackageLevelBase(productPackageLevelBase);
        productAPI.setPackageLevelMiddle(productPackageLevelMiddle);
        productAPI.setPackageLevelTop(productPackageLevelTop);

       return productAPI;
    }

    private static CVCEDirectiveAPI createValidCEDirectiveAPI() {
        CVCEDirectiveAPI directiveAPI = new CVCEDirectiveAPI();
        directiveAPI.setId(directiveId);
        return directiveAPI;
    }

    private static CVCEStandardAPI createValidCEStandardAPI() {
        CVCEStandardAPI standardAPI = new CVCEStandardAPI();
        standardAPI.setId(standardId);
        return standardAPI;
    }

    private static CVOrderUnitAPI createValidOrderUnitAPI() {
        CVOrderUnitAPI unitAPI = new CVOrderUnitAPI();
        unitAPI.setId(productOrderUnitId);
        unitAPI.setCode(productOrderUnitCode);
        unitAPI.setName(productOrderUnitName);
        return unitAPI;
    }

    private static CVPackageUnitAPI createValidPackageUnitAPI() {
        CVPackageUnitAPI unitAPI = new CVPackageUnitAPI();
        unitAPI.setId(productOrderUnitId);
        unitAPI.setCode(productOrderUnitCode);
        unitAPI.setName(productOrderUnitName);
        return unitAPI;
    }

    private Product createValidProduct(long productId, long organizationId, Article.Type articleType, String code) {
        Product validProduct = createValidProduct(productId, organizationId);
        validProduct.getCategory().setArticleType(articleType);
        validProduct.getCategory().setCode(code);
        return validProduct;
    }

}
