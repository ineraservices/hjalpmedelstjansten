package se.inera.hjalpmedelstjansten.business.helpers;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.product.controller.exportimport.ExportImportProductsArticlesController;

@ExtendWith(MockitoExtension.class)
public class ImportExcelHelperTest {

    @Mock
    HjmtLogger LOG;

    @InjectMocks
    private ImportExcelHelper importExcelHelper;

    //Makes sure test fails if DATE_PATTERN changes. Today nothing tests this, and it would cause a mess if it was changed.
    SimpleDateFormat dateFormatter = new SimpleDateFormat(ExportImportProductsArticlesController.DATE_PATTERN);


    @Test
    public void isProductDiscontinued() {

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(2019, 3, 1);

        assertEquals(true, this.importExcelHelper.isProductOrArticleDiscontinued(gregorianCalendar.getTime()));
    }

    @Test
    public void isProductNotDiscontinued() {

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(4000, 4, 20);

        assertEquals(false, importExcelHelper.isProductOrArticleDiscontinued(gregorianCalendar.getTime()));
    }

    @Test
    public void getCellStringValueByNameWholeCheck() {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("test");
        XSSFRow row = sheet.createRow(1);
        XSSFRow row2 = sheet.createRow(2);
        XSSFCell cell = row2.createCell(1);
        XSSFCell cell2 = row.createCell(1);
        cell2.setCellValue("value");
        cell.setCellValue(779081);

        assertEquals("779081", importExcelHelper.getCellStringValueByName(row, row2, "value"));
    }

    @Test
    public void getCellStringValueByNameNonWholeCheck() {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("test");
        XSSFRow row = sheet.createRow(1);
        XSSFRow row2 = sheet.createRow(2);
        XSSFCell cell = row2.createCell(1);
        XSSFCell cell2 = row.createCell(1);
        cell2.setCellValue("value");
        cell.setCellValue(779081.123);

        assertEquals("779081.123", importExcelHelper.getCellStringValueByName(row, row2, "value"));
    }

    @Test
    public void getCellNumericValueByNameNonWholeCheck() {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("test");
        XSSFRow row = sheet.createRow(1);
        XSSFRow row2 = sheet.createRow(2);
        XSSFCell cell = row2.createCell(1);
        XSSFCell cell2 = row.createCell(1);
        cell2.setCellValue("value");
        cell.setCellValue(779081.123);

        assertEquals(Double.valueOf(779081.123), importExcelHelper.getCellNumericValueByName(row, row2, "value"));

    }

    @Test
    public void getCellNumericValueByNameWholeCheck() {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("test");
        XSSFRow row = sheet.createRow(1);
        XSSFRow row2 = sheet.createRow(2);
        XSSFCell cell = row2.createCell(1);
        XSSFCell cell2 = row.createCell(1);
        cell2.setCellValue("value");
        cell.setCellValue(779081);

        assertEquals(Double.valueOf(779081), importExcelHelper.getCellNumericValueByName(row, row2, "value"));
    }

    //Remove all non numeric chars from stringed numeric field
    @Test
    public void makeSureSpacesAndSignsAreRemoved() {
        assertEquals(0.0, this.importExcelHelper.removeAllNonNumericCharactersFromNumericField("Produkt 0 - Huvudhjälpmedel 0") , 0);
    }

    @Test
    public void makeSureNegativeNumbersCanBeImported() {
        assertEquals(-1.0, this.importExcelHelper.removeAllNonNumericCharactersFromNumericField("-1"), 0);
    }

    @Test
    public void makeSurePositiveNumbersCanBeImported() {
        assertEquals(1.0, this.importExcelHelper.removeAllNonNumericCharactersFromNumericField("+1"), 0);
    }

    @Test
    public void makeSureTheresOnlyOneDotLeftAfterFilter() {

        assertEquals(1.0, this.importExcelHelper.removeAllNonNumericCharactersFromNumericField("1............................0......."), 0);
    }

    @Test
    public void getDateMakeSureMethodReturnsNullIfCellTypeIsAnythingElseThanNumericOrString() throws IllegalStateException {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("test");
        XSSFRow row = sheet.createRow(1);
        XSSFRow row2 = sheet.createRow(2);
        XSSFCell cell = row.createCell(1);
        XSSFCell cell2 = row2.createCell(1);
        cell.setCellValue("date");
        cell2.setCellValue(43659.0);
        cell2.setCellType(CellType.BOOLEAN);

        Date date = importExcelHelper.getCellDateValueByName(row, row2, "date");
        assertEquals(null, date);
    }

    @Test
    public void handleDateCellValueAsStringType() throws IllegalStateException {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("test");
        XSSFRow row = sheet.createRow(1);
        XSSFRow row2 = sheet.createRow(2);
        XSSFCell cell = row.createCell(1);
        XSSFCell cell2 = row2.createCell(1);
        cell.setCellValue("date");

        cell2.setCellValue(43659.0); //Raw numeric equivalent of 2019-07-13

        Date date = importExcelHelper.getCellDateValueByName(row, row2, "date");
        assertEquals("2019-07-13", this.dateFormatter.format(date));
    }


    @Test
    public void whatIfCustomerEntersANonStandardDateFormatYYYYMMDD() throws IllegalStateException {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("test");
        XSSFRow row = sheet.createRow(1);
        XSSFRow row2 = sheet.createRow(2);
        XSSFCell cell = row.createCell(1);
        XSSFCell cell2 = row2.createCell(1);

        cell.setCellValue("date");
        cell2.setCellValue(20190713);


        Date date = importExcelHelper.getCellDateValueByName(row, row2, "date");
        assertEquals("2019-07-13", this.dateFormatter.format(date));
    }

    @Test
    public void whatIfCustomerEntersANonStandardDateFormatYYMMDD() throws IllegalStateException {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("test");
        XSSFRow row = sheet.createRow(1);
        XSSFRow row2 = sheet.createRow(2);
        XSSFCell cell = row.createCell(1);
        XSSFCell cell2 = row2.createCell(1);

        cell.setCellValue("date");
        cell2.setCellValue(190713);

        Date date = importExcelHelper.getCellDateValueByName(row, row2, "date");
        assertEquals("2019-07-13", this.dateFormatter.format(date));
    }

    @Test
    public void whatIfCustomerEntersANonStandardDateFormatYYMMDDAsString() throws IllegalStateException {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("test");
        XSSFRow row = sheet.createRow(1);
        XSSFRow row2 = sheet.createRow(2);
        XSSFCell cell = row.createCell(1);
        XSSFCell cell2 = row2.createCell(1);

        cell.setCellValue("date");
        cell2.setCellType(CellType.STRING);
        cell2.setCellValue("190713");



        Date date = importExcelHelper.getCellDateValueByName(row, row2, "date");
        assertEquals("2019-07-13", this.dateFormatter.format(date));
    }

    @Test
    public void whatIfCustomerEntersANonStandardDateFormatYYYYMMDDAsString() throws IllegalStateException {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("test");
        XSSFRow row = sheet.createRow(1);
        XSSFRow row2 = sheet.createRow(2);
        XSSFCell cell = row.createCell(1);
        XSSFCell cell2 = row2.createCell(1);

        cell.setCellValue("date");
        cell2.setCellType(CellType.STRING);
        cell2.setCellValue("20190713");

        Date date = importExcelHelper.getCellDateValueByName(row, row2, "date");
        assertEquals("2019-07-13", this.dateFormatter.format(date));
    }

    @Test
    public void whatIfCustomerEntersANonStandardDateFormatYYYYMMDDAsStringAdditionalSigns() throws IllegalStateException {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("test");
        XSSFRow row = sheet.createRow(1);
        XSSFRow row2 = sheet.createRow(2);
        XSSFCell cell = row.createCell(1);
        XSSFCell cell2 = row2.createCell(1);

        cell.setCellValue("date");
        cell2.setCellType(CellType.STRING);
        cell2.setCellValue("2019/07/13");

        Date date = importExcelHelper.getCellDateValueByName(row, row2, "date");
        assertEquals("2019-07-13", this.dateFormatter.format(date));

        cell2.setCellValue("2167-01-01");
        date = importExcelHelper.getCellDateValueByName(row, row2, "date");
        assertEquals("2167-01-01", this.dateFormatter.format(date));

        cell2.setCellValue("2019.07.13");
        date = importExcelHelper.getCellDateValueByName(row, row2, "date");
        assertEquals("2019-07-13", this.dateFormatter.format(date));

        cell2.setCellValue("2019,07,13");
        date = importExcelHelper.getCellDateValueByName(row, row2, "date");
        assertEquals("2019-07-13", this.dateFormatter.format(date));

        cell2.setCellValue("2020,00,00");
        date = importExcelHelper.getCellDateValueByName(row, row2, "date");
        assertEquals("2019-12-31", this.dateFormatter.format(date));
    }

}
