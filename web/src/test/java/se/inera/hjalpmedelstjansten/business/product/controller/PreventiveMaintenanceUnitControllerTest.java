package se.inera.hjalpmedelstjansten.business.product.controller;


import org.junit.Before;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;


public class PreventiveMaintenanceUnitControllerTest {
    
    PreventiveMaintenanceUnitController preventiveMaintenanceUnitController;


    @Before
    public void init() {
        preventiveMaintenanceUnitController = new PreventiveMaintenanceUnitController();
    }
    
    public static CVPreventiveMaintenanceAPI createValidPreventiveMaintenanceAPI(String code, String name) {
        CVPreventiveMaintenanceAPI preventiveMaintenanceAPI = new CVPreventiveMaintenanceAPI();
        preventiveMaintenanceAPI.setCode(code);
        preventiveMaintenanceAPI.setName(name);
        return preventiveMaintenanceAPI;
    }
    
    public static CVPreventiveMaintenance createValidPreventiveMaintenance(String code, String name) {
        CVPreventiveMaintenance preventiveMaintenance = new CVPreventiveMaintenance();
        preventiveMaintenance.setCode(code);
        preventiveMaintenance.setName(name);
        return preventiveMaintenance;
    }
    
}
