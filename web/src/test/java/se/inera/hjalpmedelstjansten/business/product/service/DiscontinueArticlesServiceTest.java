package se.inera.hjalpmedelstjansten.business.product.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.CategoryController;
import se.inera.hjalpmedelstjansten.business.product.repository.ArticleRepository;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.Product.Status;

@ExtendWith(MockitoExtension.class)
class DiscontinueArticlesServiceTest {

    @Mock
    private HjmtLogger LOG;
    @Mock
    private ArticleController articleController;
    @Mock
    private ArticleRepository articleRepository;
    @Mock
    ElasticSearchController elasticSearchController;
    @Mock
    CategoryController categoryController;
    @Mock
    SendMailRegardingDiscontinuedArticlesOnAssortment sendMailRegardingDiscontinuedArticlesOnAssortment;
    @InjectMocks
    private DiscontinueArticlesService discontinueArticlesService;

    @Test
    void shallNotDiscontinueAnyArticles() {
        doReturn(Collections.emptyList()).when(articleRepository).findByStatusAndReplacementDatePassed(eq(Status.PUBLISHED), any(Date.class));

        discontinueArticlesService.discontinue();

        verifyNoInteractions(articleController);
        verifyNoInteractions(elasticSearchController);
        verifyNoInteractions(categoryController);
    }

    @Nested
    class ArticlesToDiscontinue {

        private ArticleAPI articleApiOne;
        private Article articleOne;
        private ArticleAPI articleApiTwo;
        private Article articleTwo;

        @BeforeEach
        void setUp() {
            articleApiOne = new ArticleAPI();
            articleOne = new Article();
            articleOne.setUniqueId(1L);
            articleOne.setInactivateRowsOnReplacement(Boolean.TRUE);
            final var categoryOne = new Category();
            categoryOne.setUniqueId(3L);
            articleOne.setCategory(categoryOne);

            final var product = new Product();
            final var categoryTwo = new Category();
            categoryTwo.setUniqueId(4L);
            product.setCategory(categoryTwo);

            articleApiTwo = new ArticleAPI();
            articleTwo = new Article();
            articleTwo.setUniqueId(2L);
            articleTwo.setInactivateRowsOnReplacement(Boolean.FALSE);
            articleTwo.setBasedOnProduct(product);

            final var articlesToDiscontinue = List.of(
                articleOne,
                articleTwo
            );

            doReturn(articlesToDiscontinue).when(articleRepository).findByStatusAndReplacementDatePassed(eq(Status.PUBLISHED), any(Date.class));
            doReturn(Collections.emptyList()).when(categoryController).getCategoryPropertys(categoryOne.getUniqueId());
            doReturn(Collections.emptyList()).when(categoryController).getCategoryPropertys(categoryTwo.getUniqueId());
            doReturn(articlesToDiscontinue).when(articleRepository).findByStatusAndReplacementDateBetweenFromAndTo(eq(Status.DISCONTINUED), any(Date.class), any(Date.class));
        }

        @Test
        void shallDiscontinueArticles() {

            try (MockedStatic<ArticleMapper> mocked = mockStatic(ArticleMapper.class)) {
                mocked.when(() -> ArticleMapper.map(articleOne, true, Collections.emptyList())).thenReturn(articleApiOne);
                mocked.when(() -> ArticleMapper.map(articleTwo, true, Collections.emptyList())).thenReturn(articleApiTwo);
                discontinueArticlesService.discontinue();
            }

            verify(articleController).handleArticleDiscontinued(articleOne, articleOne.isInactivateRowsOnReplacement());
        }

        @Test
        void shallUpdateElasticForDiscontinuedArticles() {
            try (MockedStatic<ArticleMapper> mocked = mockStatic(ArticleMapper.class)) {
                mocked.when(() -> ArticleMapper.map(articleOne, true, Collections.emptyList())).thenReturn(articleApiOne);
                mocked.when(() -> ArticleMapper.map(articleTwo, true, Collections.emptyList())).thenReturn(articleApiTwo);
                discontinueArticlesService.discontinue();
            }

            verify(elasticSearchController).bulkIndexArticle(articleApiOne);
            verify(elasticSearchController).bulkIndexArticle(articleApiTwo);
        }

        @Test
        void shallQueryDiscontinuedArticlesToBeMailNotified() {
            final var from = Date.from(LocalDate.now().atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());

            final var to = Date.from(LocalDate.now().atTime(LocalTime.MAX)
                .atZone(ZoneId.systemDefault())
                .toInstant());

            try (MockedStatic<ArticleMapper> mocked = mockStatic(ArticleMapper.class)) {
                mocked.when(() -> ArticleMapper.map(articleOne, true, Collections.emptyList())).thenReturn(articleApiOne);
                mocked.when(() -> ArticleMapper.map(articleTwo, true, Collections.emptyList())).thenReturn(articleApiTwo);
                discontinueArticlesService.discontinue();
            }

            final var fromArgumentCaptor = ArgumentCaptor.forClass(Date.class);
            final var toArgumentCaptor = ArgumentCaptor.forClass(Date.class);

            verify(articleRepository).findByStatusAndReplacementDateBetweenFromAndTo(eq(Status.DISCONTINUED), fromArgumentCaptor.capture(), toArgumentCaptor.capture());

            assertAll(
                () -> assertEquals(from, fromArgumentCaptor.getValue()),
                () -> assertEquals(to, toArgumentCaptor.getValue())
            );
        }

        @Test
        void shallSendMailBasedOnDiscontinuedArticles() {
            final var expectedArticleIds = List.of(articleOne.getUniqueId(), articleTwo.getUniqueId());

            try (MockedStatic<ArticleMapper> mocked = mockStatic(ArticleMapper.class)) {
                mocked.when(() -> ArticleMapper.map(articleOne, true, Collections.emptyList())).thenReturn(articleApiOne);
                mocked.when(() -> ArticleMapper.map(articleTwo, true, Collections.emptyList())).thenReturn(articleApiTwo);
                discontinueArticlesService.discontinue();
            }

            final ArgumentCaptor<List<Long>> articleIdsArgumentCaptor = ArgumentCaptor.forClass(List.class);

            verify(sendMailRegardingDiscontinuedArticlesOnAssortment).send(articleIdsArgumentCaptor.capture());

            assertEquals(expectedArticleIds, articleIdsArgumentCaptor.getValue());
        }
    }
}
