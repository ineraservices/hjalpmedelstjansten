package se.inera.hjalpmedelstjansten.business.organization.controller;

import org.junit.Before;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;
import se.inera.hjalpmedelstjansten.model.entity.Organization;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class OrganizationControllerTest {
    
    @Before
    public void init() {
        
    }
    
    public static Organization createValidOrganization(long organizationId,             
            Organization.OrganizationType organizationType, 
            Date validFrom,
            Long businessLevelId, 
            String organizationName) {
        Organization organization = new Organization();
        organization.setUniqueId(organizationId);
        organization.setOrganizationName(organizationName);
        organization.setOrganizationType(organizationType);
        organization.setValidFrom(validFrom);
        if( businessLevelId != null ) {
            List<BusinessLevel> businessLevels = new ArrayList<>();
            BusinessLevel businessLevel = new BusinessLevel();
            businessLevel.setUniqueId(businessLevelId);
            businessLevels.add(businessLevel);
            organization.setBusinessLevels(businessLevels);
        }
        return organization;
    }
    
    public static OrganizationAPI createValidOrganizationAPI(long organizationId, Organization.OrganizationType organizationType, long validFrom) {
        OrganizationAPI organizationAPI = new OrganizationAPI();
        organizationAPI.setId(organizationId);
        organizationAPI.setOrganisationType(organizationType.toString());
        organizationAPI.setValidFrom(validFrom);
        return organizationAPI;
    }
    
}
