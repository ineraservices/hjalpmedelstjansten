package se.inera.hjalpmedelstjansten.business.generalpricelist.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleControllerTest;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.UserAPITest;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import jakarta.enterprise.event.Event;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class GeneralPricelistPricelistRowControllerTest {

    static final long pricelistRowId = 1;
    static final long pricelistId = 2;
    static final long userId = 3;
    static final long organizationIdSupplier = 4;
    static final long generalPricelistId = 5;
    static final long articleId = 6;
    static final long discontinuedArticleId = 7;
    static final long pricelistRowInactiveId = 8;
    static final long guaranteeUnitId = 9;

    static final int leastOrderQuantity = 1;
    static final BigDecimal price = BigDecimal.ONE;

    UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationIdSupplier, null, UserRole.RoleName.SupplierAgreementManager, null);
    GeneralPricelistPricelistRowController pricelistRowController;
    EntityManager myEntityManager;
    GeneralPricelistPricelistController generalPricelistPricelistController;
    GeneralPricelistPricelistRowValidation pricelistRowValidation;
    ArticleController articleController;
    GuaranteeUnitController guaranteeUnitController;
    Event<InternalAuditEvent> event;

    @Before
    public void init() {
        pricelistRowController = new GeneralPricelistPricelistRowController();
        pricelistRowController.LOG = new NoLogger();

        event = Mockito.mock(Event.class);
        pricelistRowController.internalAuditEvent = event;

        myEntityManager = Mockito.mock(EntityManager.class);

        Query mockQuery = Mockito.mock(Query.class);
        Mockito.when(mockQuery.setParameter(Mockito.anyString(), Mockito.any())).thenReturn(mockQuery);
        Mockito.when(myEntityManager.createNamedQuery(GeneralPricelistPricelistRow.FIND_BY_GENERAL_PRICELIST_AND_ARTICLE)).thenReturn(mockQuery);

        // row with status active
        Mockito.when(myEntityManager.find(GeneralPricelistPricelistRow.class, pricelistRowId)).thenReturn(createValidPricelistRow(pricelistRowId, pricelistId, organizationIdSupplier, GeneralPricelistPricelistRow.Status.ACTIVE));
        Mockito.when(myEntityManager.find(GeneralPricelistPricelistRow.class, pricelistRowInactiveId)).thenReturn(createValidPricelistRow(pricelistRowId, pricelistId, organizationIdSupplier, GeneralPricelistPricelistRow.Status.INACTIVE));
        Query mockQueryArticleIds = Mockito.mock(Query.class);
        Mockito.when(myEntityManager.createNamedQuery(GeneralPricelistPricelistRow.GET_ARTICLE_UNIQUE_IDS)).thenReturn(mockQueryArticleIds);
        Mockito.when(mockQueryArticleIds.setParameter(Mockito.anyString(), Mockito.any())).thenReturn(mockQueryArticleIds);
        Mockito.when(mockQueryArticleIds.getResultList()).thenReturn(new ArrayList());
        pricelistRowController.em = myEntityManager;

        generalPricelistPricelistController = Mockito.mock(GeneralPricelistPricelistController.class);
        Mockito.when(generalPricelistPricelistController.getPricelist(organizationIdSupplier, pricelistId, userAPI)).thenReturn(GeneralPricelistPricelistControllerTest.createValidPricelist(pricelistId, null, new Date(), null, generalPricelistId, organizationIdSupplier));
        pricelistRowController.generalPricelistPricelistController = generalPricelistPricelistController;

        pricelistRowValidation = Mockito.mock(GeneralPricelistPricelistRowValidation.class);
        pricelistRowController.pricelistRowValidation = pricelistRowValidation;

        articleController = Mockito.mock(ArticleController.class);
        Mockito.when(articleController.getArticle(organizationIdSupplier, articleId, userAPI)).thenReturn(ArticleControllerTest.createValidArticle(articleId, organizationIdSupplier));
        Mockito.when(articleController.getArticle(organizationIdSupplier, discontinuedArticleId, userAPI)).thenReturn(ArticleControllerTest.createValidArticle(articleId, organizationIdSupplier, Product.Status.DISCONTINUED));
        pricelistRowController.articleController = articleController;

        ValidationMessageService validationMessageService = Mockito.mock(ValidationMessageService.class);
        Mockito.when(validationMessageService.generateValidationException(Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer<HjalpmedelstjanstenValidationException>() {
            @Override
            public HjalpmedelstjanstenValidationException answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException((String) args[0] + "->" + (String) args[1]);
                exception.addValidationMessage((String) args[0], (String) args[1]);
                return exception;
            }
        });
        pricelistRowController.validationMessageService = validationMessageService;

        guaranteeUnitController = Mockito.mock(GuaranteeUnitController.class);
        Mockito.when(guaranteeUnitController.getGuaranteeUnit(guaranteeUnitId)).thenReturn(ArticleControllerTest.createValidGuaranteeUnit(guaranteeUnitId));
        pricelistRowController.guaranteeUnitController = guaranteeUnitController;
    }

    @Test
    public void testGetPricelistRow() {
        GeneralPricelistPricelistRow pricelistRow = pricelistRowController.getPricelistRow(organizationIdSupplier, pricelistId, pricelistRowId, userAPI);
        Assert.assertNotNull(pricelistRow);
    }

    @Test
    public void testGetPricelistWrongPricelistUniqueId() {
        GeneralPricelistPricelistRow pricelistRow = pricelistRowController.getPricelistRow(organizationIdSupplier, 1000000, pricelistRowId, userAPI);
        Assert.assertNull(pricelistRow);
    }

    @Test
    public void testGetPricelistWrongPricelistRowUniqueId() {
        GeneralPricelistPricelistRow pricelistRow = pricelistRowController.getPricelistRow(organizationIdSupplier, pricelistId, 1000000, userAPI);
        Assert.assertNull(pricelistRow);
    }

    @Test
    public void testCreateValidPricelistRow() {
        GeneralPricelistPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, price, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        List<GeneralPricelistPricelistRowAPI> agreementPricelistRowAPIs = new ArrayList<>();
        agreementPricelistRowAPIs.add(agreementPricelistRowAPI);
        try {
            agreementPricelistRowAPIs = pricelistRowController.createPricelistRows(organizationIdSupplier, pricelistId, agreementPricelistRowAPIs, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
        Assert.assertNotNull(agreementPricelistRowAPIs);
    }

    @Test
    public void testCreateInvalidPricelistRowWrongArticleId() {
        GeneralPricelistPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, price, 1000000, GeneralPricelistPricelistRow.Status.ACTIVE);
        List<GeneralPricelistPricelistRowAPI> agreementPricelistRowAPIs = new ArrayList<>();
        agreementPricelistRowAPIs.add(agreementPricelistRowAPI);
        try {
            agreementPricelistRowAPIs = pricelistRowController.createPricelistRows(organizationIdSupplier, pricelistId, agreementPricelistRowAPIs, userAPI, null, null);
            Assert.fail("Exception not thrown, but should have been");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("article", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("pricelistrow.article.notExist"));
        }
    }

    @Test
    public void testCreateInvalidPricelistRowWrongPricelistId() {
        GeneralPricelistPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, price, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        List<GeneralPricelistPricelistRowAPI> agreementPricelistRowAPIs = new ArrayList<>();
        agreementPricelistRowAPIs.add(agreementPricelistRowAPI);
        try {
            agreementPricelistRowAPIs = pricelistRowController.createPricelistRows(organizationIdSupplier, 1000000, agreementPricelistRowAPIs, userAPI, null, null);
            Assert.fail("Exception not thrown, but should have been");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("pricelist", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("pricelistrow.pricelist.notNull"));
        }
    }

    /*
    @Test
    public void testCreateInvalidPricelistRowDiscontinuedArticle() {
        GeneralPricelistPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, price, discontinuedArticleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        List<GeneralPricelistPricelistRowAPI> agreementPricelistRowAPIs = new ArrayList<>();
        agreementPricelistRowAPIs.add(agreementPricelistRowAPI);
        try {
            agreementPricelistRowAPIs = pricelistRowController.createPricelistRows(organizationIdSupplier, pricelistId, agreementPricelistRowAPIs, userAPI, null, null);
            Assert.fail("Exception not thrown, but should have been");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("article", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("pricelistrow.article.discontinued"));
        }
    }*/

    @Test
    public void testUpdateValidPricelistRow() {
        GeneralPricelistPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, price, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        try {
            agreementPricelistRowAPI = pricelistRowController.updatePricelistRow(organizationIdSupplier, pricelistId, pricelistRowId, agreementPricelistRowAPI, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
        Assert.assertNotNull(agreementPricelistRowAPI);
    }

    @Test
    public void testUpdateInalidPricelistRowWrongUniqueId() {
        GeneralPricelistPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(1000000, leastOrderQuantity, price, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        try {
            agreementPricelistRowAPI = pricelistRowController.updatePricelistRow(organizationIdSupplier, pricelistId, 1000000, agreementPricelistRowAPI, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
        Assert.assertNull(agreementPricelistRowAPI);
    }

    @Test
    public void testInactivateRowsActive() {
        GeneralPricelistPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, price, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        List<GeneralPricelistPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.inactivateRows(organizationIdSupplier, pricelistId, rows, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
        Assert.assertNotNull(rows);
    }

    @Test
    public void testInactivateRowsWrongStatus() {
        GeneralPricelistPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowInactiveId, leastOrderQuantity, price, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        List<GeneralPricelistPricelistRowAPI> rows = new ArrayList<>();
        rows.add(agreementPricelistRowAPI);
        try {
            rows = pricelistRowController.inactivateRows(organizationIdSupplier, pricelistId, rows, userAPI, null, null);
            Assert.fail();
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("status", errorMessageAPI.getField());
            Assert.assertTrue(errorMessageAPI.getMessage().contains("pricelistrow.inactivateRows.wrongStatus"));
        }
    }

    @Test
    public void testSetOverriddenWarrantyQuantityUnitOverride() {
        GeneralPricelistPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, price, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        agreementPricelistRowAPI.setWarrantyQuantityUnit(ArticleControllerTest.createValidGuaranteeUnitAPI(guaranteeUnitId));
        GeneralPricelistPricelistRow agreementPricelistRow = createValidPricelistRow(pricelistRowId, pricelistId, organizationIdSupplier, GeneralPricelistPricelistRow.Status.ACTIVE);
        try {
            pricelistRowController.setOverriddenWarrantyQuantityUnit(agreementPricelistRow, agreementPricelistRowAPI, false);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
        Assert.assertNotNull(agreementPricelistRow.getWarrantyQuantityUnit());
        Assert.assertTrue(agreementPricelistRow.isWarrantyQuantityUnitOverridden());
    }

    @Test
    public void testSetOverriddenWarrantyQuantityUnitNotOverride() {
        GeneralPricelistPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, price, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        agreementPricelistRowAPI.setWarrantyQuantityUnit(ArticleControllerTest.createValidGuaranteeUnitAPI(guaranteeUnitId));
        GeneralPricelistPricelistRow agreementPricelistRow = createValidPricelistRow(pricelistRowId, pricelistId, organizationIdSupplier, GeneralPricelistPricelistRow.Status.ACTIVE);
        agreementPricelistRow.getPricelist().getGeneralPricelist().setWarrantyQuantityTUnit(ArticleControllerTest.createValidGuaranteeUnit(guaranteeUnitId));
        try {
            pricelistRowController.setOverriddenWarrantyQuantityUnit(agreementPricelistRow, agreementPricelistRowAPI, false);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
        Assert.assertNull(agreementPricelistRow.getWarrantyQuantityUnit());
        Assert.assertFalse(agreementPricelistRow.isWarrantyQuantityUnitOverridden());
    }

    @Test
    public void testSetOverriddenWarrantyQuantityUnitOverrideWithNull() {
        GeneralPricelistPricelistRowAPI agreementPricelistRowAPI = createValidPricelistRowAPI(pricelistRowId, leastOrderQuantity, price, articleId, GeneralPricelistPricelistRow.Status.ACTIVE);
        GeneralPricelistPricelistRow agreementPricelistRow = createValidPricelistRow(pricelistRowId, pricelistId, organizationIdSupplier, GeneralPricelistPricelistRow.Status.ACTIVE);
        agreementPricelistRow.getPricelist().getGeneralPricelist().setWarrantyQuantityTUnit(ArticleControllerTest.createValidGuaranteeUnit(guaranteeUnitId));
        try {
            pricelistRowController.setOverriddenWarrantyQuantityUnit(agreementPricelistRow, agreementPricelistRowAPI, false);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
        Assert.assertNull(agreementPricelistRow.getWarrantyQuantityUnit());
        Assert.assertTrue(agreementPricelistRow.isWarrantyQuantityUnitOverridden());
    }

    public static GeneralPricelistPricelistRowAPI createValidPricelistRowAPI(long id, int leastOrderQuantity, BigDecimal price, long articleId, GeneralPricelistPricelistRow.Status status) {
        GeneralPricelistPricelistRowAPI pricelistRowAPI = new GeneralPricelistPricelistRowAPI();
        pricelistRowAPI.setId(id);
        pricelistRowAPI.setLeastOrderQuantity(leastOrderQuantity);
        pricelistRowAPI.setPrice(price);
        pricelistRowAPI.setStatus(status.toString());
        pricelistRowAPI.setDeliveryTime(1);
        ArticleAPI articleAPI = new ArticleAPI();
        articleAPI.setId(articleId);
        pricelistRowAPI.setArticle(articleAPI);
        return pricelistRowAPI;
    }

    public static GeneralPricelistPricelistRow createValidPricelistRow(long pricelistRowId, long pricelistId, long organizationId, GeneralPricelistPricelistRow.Status status) {
        GeneralPricelistPricelistRow generalPricelistPricelistRow = new GeneralPricelistPricelistRow();
        generalPricelistPricelistRow.setPrice(price);
        generalPricelistPricelistRow.setUniqueId(pricelistRowId);
        generalPricelistPricelistRow.setStatus(status);
        generalPricelistPricelistRow.setPricelist(GeneralPricelistPricelistControllerTest.createValidPricelist(pricelistId, null, new Date(), null, generalPricelistId, organizationId));
        generalPricelistPricelistRow.setArticle(ArticleControllerTest.createValidArticle(articleId, organizationId));
        return generalPricelistPricelistRow;
    }
}
