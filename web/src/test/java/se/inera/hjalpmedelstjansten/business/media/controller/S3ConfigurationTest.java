package se.inera.hjalpmedelstjansten.business.media.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class S3ConfigurationTest {
    private S3Configuration config;

    @BeforeEach
    public void init() {
        config = new S3Configuration();
    }

    @Test
    void isValid() {
        assertFalse(config.isValid());

        config
            .setRegion("foo")
            .setAccessKeyId("foo")
            .setSecretAccessKey("foo")
            .setBucketName("foo")
            .setMediaBaseUrl("foo");

        assertTrue(config.isValid());
    }

    @Test
    void setRegion() {
        S3Configuration config = new S3Configuration();
        assertEquals("foo", config.setRegion("foo").getRegion());
    }

    @Test
    void setAccessKeyId() {
        S3Configuration config = new S3Configuration();
        assertEquals("foo", config.setAccessKeyId("foo").getAccessKeyId());
    }

    @Test
    void setSecretAccessKey() {
        S3Configuration config = new S3Configuration();
        assertEquals("foo", config.setSecretAccessKey("foo").getSecretAccessKey());
    }

    @Test
    void setBucketName() {
        S3Configuration config = new S3Configuration();
        assertEquals("foo", config.setBucketName("foo").getBucketName());
    }

    @Test
    void setMediaBaseUrl() {
        S3Configuration config = new S3Configuration();
        assertEquals("foo", config.setMediaBaseUrl("foo").getMediaBaseUrl());
    }

}
