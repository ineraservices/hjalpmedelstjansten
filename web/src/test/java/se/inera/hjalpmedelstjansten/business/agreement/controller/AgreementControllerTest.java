package se.inera.hjalpmedelstjansten.business.agreement.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;
import se.inera.hjalpmedelstjansten.business.organization.controller.BusinessLevelControllerTest;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationControllerTest;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleControllerTest;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserControllerTest;
import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.BusinessLevelAPITest;
import se.inera.hjalpmedelstjansten.model.api.validation.UserAPITest;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;

import jakarta.enterprise.event.Event;
import jakarta.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class AgreementControllerTest {

    AgreementController agreementController;
    AgreementPricelistController agreementPricelistController;
    AgreementPricelistRowController agreementPricelistRowController;
    EntityManager myEntityManager;
    OrganizationController organizationController;
    UserController userController;
    GuaranteeUnitController guaranteeUnitController;
    Organization customerOrganization;
    Organization supplierOrganization;
    Organization sharedWithCustomerOrganization;
    Organization sharedWithCustomerBusinessLevelOrganization;
    Event<InternalAuditEvent> event;
    AgreementValidation agreementValidation;

    static final long customerOrganizationId = 1;
    static final long supplierOrganizationId = 2;
    static final long agreementId = 3;
    static final long businessLevelId = 4;
    static final long supplierAgreementUserId = 5;
    static final long customerAgreementUserId = 6;
    static final long supplierBaseUserId = 7;
    static final long customerBaseUserId = 8;
    static final long agreementWithBusinessLevelId = 9;
    static final long customerBaseWithBusinessLevelUserId = 10;
    static final long businessLevelId2 = 11;
    static final long guaranteeUnitId = 12;
    static final long sharedWithCustomerOrganizationId = 13;
    static final long userId = 14;
    static final long pricelistApproverUserId = 15;
    static final long sharedWithCustomerBusinessLevelOrganizationId = 16;
    static final String agreementName = "agreementName";
    static final String agreementNumber = "agreementNumber";

    @Before
    public void init() {
        agreementController = new AgreementController();
        agreementController.LOG = new NoLogger();

        event = Mockito.mock(Event.class);
        agreementController.internalAuditEvent = event;

        myEntityManager = Mockito.mock(EntityManager.class);
        Mockito.when(myEntityManager.find(Agreement.class, agreementId)).thenReturn(createValidAgreement(null));
        Mockito.when(myEntityManager.find(Agreement.class, agreementWithBusinessLevelId)).thenReturn(createValidAgreement(businessLevelId));
        agreementController.em = myEntityManager;

        organizationController = Mockito.mock(OrganizationController.class);
        supplierOrganization = OrganizationControllerTest.createValidOrganization(supplierOrganizationId, Organization.OrganizationType.SUPPLIER, new Date(), null, null);
        customerOrganization = OrganizationControllerTest.createValidOrganization(customerOrganizationId, Organization.OrganizationType.CUSTOMER, new Date(), businessLevelId, null);
        sharedWithCustomerOrganization = OrganizationControllerTest.createValidOrganization(sharedWithCustomerOrganizationId, Organization.OrganizationType.CUSTOMER, new Date(), null, null);
        sharedWithCustomerBusinessLevelOrganization = OrganizationControllerTest.createValidOrganization(sharedWithCustomerBusinessLevelOrganizationId, Organization.OrganizationType.CUSTOMER, new Date(), null, null);
        Mockito.when(organizationController.getOrganization(customerOrganizationId)).thenReturn(customerOrganization);
        Mockito.when(organizationController.getOrganization(customerOrganizationId+1000)).thenReturn(customerOrganization);
        Mockito.when(organizationController.getOrganization(supplierOrganizationId)).thenReturn(supplierOrganization);
        Mockito.when(organizationController.getOrganization(sharedWithCustomerOrganizationId)).thenReturn(sharedWithCustomerOrganization);
        Mockito.when(organizationController.getOrganization(sharedWithCustomerBusinessLevelOrganizationId)).thenReturn(sharedWithCustomerBusinessLevelOrganization);
        Mockito.when(organizationController.getOrganization(sharedWithCustomerBusinessLevelOrganizationId+1000)).thenReturn(sharedWithCustomerBusinessLevelOrganization);
        agreementController.organizationController = organizationController;

        ValidationMessageService validationMessageService = Mockito.mock(ValidationMessageService.class);
        Mockito.when(validationMessageService.generateValidationException(Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer<HjalpmedelstjanstenValidationException>() {
            @Override
            public HjalpmedelstjanstenValidationException answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException((String) args[0] + "->" + (String) args[1]);
                exception.addValidationMessage((String) args[0], (String) args[1]);
                return exception;
            }
        });
        Mockito.when(validationMessageService.generateValidationException(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer<HjalpmedelstjanstenValidationException>() {
            @Override
            public HjalpmedelstjanstenValidationException answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException((String) args[0] + "->" + (String) args[1]);
                exception.addValidationMessage((String) args[0], (String) args[1]);
                return exception;
            }
        });
        agreementController.validationMessageService = validationMessageService;

        userController = Mockito.mock(UserController.class);
        // supplier users
        UserEngagement supplierUserEngagement = UserControllerTest.createValidUserEngagement(supplierOrganizationId, supplierAgreementUserId, Organization.OrganizationType.SUPPLIER, UserRole.RoleName.SupplierAgreementManager, null);
        Mockito.when(userController.getUserEngagement(supplierOrganizationId, supplierAgreementUserId)).thenReturn(supplierUserEngagement);
        UserEngagement baseUserEngagement = UserControllerTest.createValidUserEngagement(supplierOrganizationId, supplierBaseUserId, Organization.OrganizationType.SUPPLIER, UserRole.RoleName.Baseuser, null);
        Mockito.when(userController.getUserEngagement(supplierOrganizationId, supplierBaseUserId)).thenReturn(baseUserEngagement);
        Mockito.when(userController.findRoleByName(UserRole.RoleName.SupplierAgreementManager)).thenReturn(supplierUserEngagement.getRoles().get(0));
        Mockito.when(userController.findRoleByName(UserRole.RoleName.Baseuser)).thenReturn(baseUserEngagement.getRoles().get(0));

        // customer users
        UserEngagement customerUserEngagement = UserControllerTest.createValidUserEngagement(customerOrganizationId, customerAgreementUserId, Organization.OrganizationType.CUSTOMER, UserRole.RoleName.CustomerAgreementManager, null);
        Mockito.when(userController.getUserEngagement(customerOrganizationId, customerAgreementUserId)).thenReturn(customerUserEngagement);
        UserEngagement customerBaseUserEngagement = UserControllerTest.createValidUserEngagement(customerOrganizationId, customerBaseUserId, Organization.OrganizationType.CUSTOMER, UserRole.RoleName.Baseuser, null);
        Mockito.when(userController.getUserEngagement(customerOrganizationId, customerBaseUserId)).thenReturn(customerBaseUserEngagement);
        UserEngagement customerBaseWithBusinessLevelUserEngagement = UserControllerTest.createValidUserEngagement(customerOrganizationId, customerBaseWithBusinessLevelUserId, Organization.OrganizationType.CUSTOMER, UserRole.RoleName.Baseuser, businessLevelId);
        Mockito.when(userController.getUserEngagement(customerOrganizationId, customerBaseWithBusinessLevelUserId)).thenReturn(customerBaseWithBusinessLevelUserEngagement);


        UserEngagement userIdUserEngagement = UserControllerTest.createValidUserEngagement(customerOrganizationId, userId, Organization.OrganizationType.CUSTOMER, UserRole.RoleName.CustomerAgreementManager, null);
        Mockito.when(userController.getUserEngagement(customerOrganizationId, userId)).thenReturn(userIdUserEngagement);



        Mockito.when(userController.findRoleByName(UserRole.RoleName.CustomerAgreementManager)).thenReturn(customerUserEngagement.getRoles().get(0));
        agreementController.userController = userController;

        guaranteeUnitController = Mockito.mock(GuaranteeUnitController.class);
        CVGuaranteeUnit unit = ArticleControllerTest.createValidGuaranteeUnit(guaranteeUnitId);
        Mockito.when(guaranteeUnitController.getGuaranteeUnit(guaranteeUnitId)).thenReturn(unit);
        agreementController.guaranteeUnitController = guaranteeUnitController;

        agreementValidation = new AgreementValidation();
        agreementValidation.validationMessageService = validationMessageService;
        agreementValidation.LOG = new NoLogger();
        agreementController.agreementValidation = agreementValidation;

        agreementPricelistController = Mockito.mock(AgreementPricelistController.class);
        agreementController.pricelistController = agreementPricelistController;

        agreementPricelistRowController = Mockito.mock(AgreementPricelistRowController.class);
        agreementController.agreementPricelistRowController = agreementPricelistRowController;
    }

    @Test
    public void testGetAgreement() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        Agreement agreement = agreementController.getAgreement(customerOrganizationId, agreementId, userAPI);
        Assert.assertNotNull(agreement);
    }

    @Test
    public void testGetAgreementWrongAgreementUniqueId() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        Agreement agreement = agreementController.getAgreement(customerOrganizationId, agreementId+1, userAPI);
        Assert.assertNull(agreement);
    }

    @Test
    public void testGetAgreementWrongCustomerOrganizationUniqueId() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        Agreement agreement = agreementController.getAgreement(customerOrganizationId+1000, agreementId, userAPI);
        Assert.assertNull(agreement);
    }

    @Test
    public void testGetAgreementWithoutBusinessLevelUserWithBusinessLevel() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, businessLevelId, UserRole.RoleName.CustomerAgreementManager, null);
        Agreement agreement = agreementController.getAgreement(customerOrganizationId, agreementId, userAPI);
        Assert.assertNull(agreement);
    }

    @Test
    public void testGetAgreementWithBusinessLevelUserWithoutBusinessLevel() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        Agreement agreement = agreementController.getAgreement(customerOrganizationId, agreementWithBusinessLevelId, userAPI);
        Assert.assertNotNull(agreement);
    }

    @Test
    public void testGetAgreementWithBusinessLevelUserWithBusinessLevel() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, businessLevelId, UserRole.RoleName.CustomerAgreementManager, null);
        Agreement agreement = agreementController.getAgreement(customerOrganizationId, agreementWithBusinessLevelId, userAPI);
        Assert.assertNotNull(agreement);
    }

    @Test
    public void testGetAgreementAPI() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        AgreementAPI agreementAPI = agreementController.getAgreementAPI(customerOrganizationId, agreementId, userAPI, null, null);
        Assert.assertNotNull(agreementAPI);
    }

    @Test
    public void testGetAgreementAPIWrongAgreementUniqueId() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        AgreementAPI agreementAPI = agreementController.getAgreementAPI(customerOrganizationId, agreementId+1, userAPI, null, null);
        Assert.assertNull(agreementAPI);
    }

    @Test
    public void testGetAgreementAPIWrongCustomerOrganizationUniqueId() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        AgreementAPI agreementAPI = agreementController.getAgreementAPI(customerOrganizationId+1000, agreementId, userAPI, null, null);
        Assert.assertNull(agreementAPI);
    }

    @Test
    public void testUserBusinessLevelsContains() {
        List<BusinessLevelAPI> userBusinessLevelAPIs = new ArrayList<>();
        BusinessLevelAPI businessLevelAPI = BusinessLevelAPITest.createValidBusinessLevelAPI(businessLevelId, BusinessLevel.Status.ACTIVE);
        userBusinessLevelAPIs.add(businessLevelAPI);
        Assert.assertTrue(agreementController.userBusinessLevelsContains(userBusinessLevelAPIs, businessLevelId));
    }

    @Test
    public void testUserBusinessLevelsNotContains() {
        List<BusinessLevelAPI> userBusinessLevelAPIs = new ArrayList<>();
        BusinessLevelAPI businessLevelAPI = BusinessLevelAPITest.createValidBusinessLevelAPI(businessLevelId, BusinessLevel.Status.ACTIVE);
        userBusinessLevelAPIs.add(businessLevelAPI);
        Assert.assertFalse(agreementController.userBusinessLevelsContains(userBusinessLevelAPIs, businessLevelId+1));
    }

    @Test
    public void testSetSupplier() {
        Agreement agreement = createValidAgreement(null, customerOrganizationId, supplierOrganizationId);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        try {
            agreementController.setSupplier(agreement, agreementAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    }

    @Test
    public void testSetSupplierWrongSupplierOrganizationId() {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        agreementAPI.getSupplierOrganization().setId(agreementAPI.getSupplierOrganization().getId() + 10000);
        try {
            agreementController.setSupplier(agreement, agreementAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("supplierOrganization", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetSupplierWrongSupplierOrganizationType() {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        agreementAPI.getSupplierOrganization().setId(customerOrganizationId);
        try {
            agreementController.setSupplier(agreement, agreementAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("supplierOrganization", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetCustomer() {
        Agreement agreement = createValidAgreement(null);
        try {
            agreementController.setCustomer(agreement, customerOrganizationId);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    }

    @Test
    public void testSetCustomerWrongCustomerOrganizationId() {
        Agreement agreement = createValidAgreement(null);
        try {
            agreementController.setCustomer(agreement, customerOrganizationId + 1000000);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("customerOrganization", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetCustomerWrongCustomerOrganizationType() throws HjalpmedelstjanstenException, HjalpmedelstjanstenException {
        Agreement agreement = createValidAgreement(null);
        try {
            agreementController.setCustomer(agreement, supplierOrganizationId);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("customerOrganization", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetCustomerBusinessLevel() throws HjalpmedelstjanstenException {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, businessLevelId, UserRole.RoleName.CustomerAgreementManager, null);
        try {
            agreementController.setCustomerBusinessLevel(agreementAPI, agreement, userAPI, customerOrganization);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    }

    @Test
    public void testSetCustomerBusinessLevelNull() throws HjalpmedelstjanstenException {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, businessLevelId, UserRole.RoleName.CustomerAgreementManager, null);
        userAPI.getUserEngagements().get(0).setBusinessLevels(null);
        try {
            agreementController.setCustomerBusinessLevel(agreementAPI, agreement, userAPI, customerOrganization);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    }

    @Test
    public void testSetCustomerBusinessLevelNotInUser() throws HjalpmedelstjanstenException {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, businessLevelId+1, UserRole.RoleName.CustomerAgreementManager, null);
        try {
            agreementController.setCustomerBusinessLevel(agreementAPI, agreement, userAPI, customerOrganization);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("customerBusinessLevel", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetCustomerBusinessLevelNullButUserHasBusinessLevel() throws HjalpmedelstjanstenException {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        agreementAPI.setCustomerBusinessLevel(null);
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, businessLevelId, UserRole.RoleName.CustomerAgreementManager, null);
        try {
            agreementController.setCustomerBusinessLevel(agreementAPI, agreement, userAPI, customerOrganization);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("customerBusinessLevel", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetCustomerBusinessLevelNotInOrganization() throws HjalpmedelstjanstenException {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, businessLevelId, UserRole.RoleName.CustomerAgreementManager, null);
        Organization customerOrganizationWithoutBusinessLevel = OrganizationControllerTest.createValidOrganization(customerOrganizationId, Organization.OrganizationType.CUSTOMER, new Date(), null, null);
        try {
            agreementController.setCustomerBusinessLevel(agreementAPI, agreement, userAPI, customerOrganizationWithoutBusinessLevel);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("customerBusinessLevel", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetCustomerBusinessLevelNotInOrganization2() throws HjalpmedelstjanstenException {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, businessLevelId, UserRole.RoleName.CustomerAgreementManager, null);
        Organization customerOrganizationWithoutBusinessLevel = OrganizationControllerTest.createValidOrganization(customerOrganizationId, Organization.OrganizationType.CUSTOMER, new Date(), businessLevelId+1000, null);
        try {
            agreementController.setCustomerBusinessLevel(agreementAPI, agreement, userAPI, customerOrganizationWithoutBusinessLevel);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("customerBusinessLevel", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetAgreementCustomerPricelistApprovers() {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        UserAPI userAPIPricelistApprover = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, businessLevelId, UserRole.RoleName.CustomerAgreementManager, null);
        List<UserAPI> customerPricelistApprovers = new ArrayList<>();
        customerPricelistApprovers.add(userAPIPricelistApprover);
        agreementAPI.setCustomerPricelistApprovers(customerPricelistApprovers);
        try {
            agreementController.setAgreementPricelistApprovers(customerOrganizationId, agreementAPI, agreement, userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    }

    @Test
    public void testSetAgreementCustomerPricelistApproversWrongRole() {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        UserAPI userAPIPricelistApprover = UserAPITest.createValidUserAPI(customerBaseUserId, customerOrganizationId, businessLevelId, UserRole.RoleName.Baseuser, null);
        List<UserAPI> customerPricelistApprovers = new ArrayList<>();
        customerPricelistApprovers.add(userAPIPricelistApprover);
        agreementAPI.setCustomerPricelistApprovers(customerPricelistApprovers);
        try {
            agreementController.setAgreementPricelistApprovers(customerOrganizationId, agreementAPI, agreement, userAPI);
            Assert.fail("Exception NOT thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("customerPricelistApprovers", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetAgreementCustomerPricelistApproversWrongOrganization() {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        UserAPI userAPIPricelistApprover = UserAPITest.createValidUserAPI(supplierBaseUserId, supplierOrganizationId, businessLevelId, UserRole.RoleName.Baseuser, null);
        List<UserAPI> customerPricelistApprovers = new ArrayList<>();
        customerPricelistApprovers.add(userAPIPricelistApprover);
        agreementAPI.setCustomerPricelistApprovers(customerPricelistApprovers);
        try {
            agreementController.setAgreementPricelistApprovers(customerOrganizationId, agreementAPI, agreement, userAPI);
            Assert.fail("Exception NOT thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("customerPricelistApprovers", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetAgreementCustomerPricelistApproversWrongBusinessLevelNullInAgreement() {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        UserAPI userAPIPricelistApprover = UserAPITest.createValidUserAPI(customerBaseWithBusinessLevelUserId, customerOrganizationId, businessLevelId, UserRole.RoleName.Baseuser, null);
        List<UserAPI> customerPricelistApprovers = new ArrayList<>();
        customerPricelistApprovers.add(userAPIPricelistApprover);
        agreementAPI.setCustomerPricelistApprovers(customerPricelistApprovers);
        try {
            agreementController.setAgreementPricelistApprovers(customerOrganizationId, agreementAPI, agreement, userAPI);
            Assert.fail("Exception NOT thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("customerPricelistApprovers", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetAgreementCustomerPricelistApproversWrongBusinessLevelOtherInAgreement() {
        Agreement agreement = createValidAgreement(businessLevelId2);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        UserAPI userAPI = UserAPITest.createValidUserAPI(customerAgreementUserId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        UserAPI userAPIPricelistApprover = UserAPITest.createValidUserAPI(customerBaseWithBusinessLevelUserId, customerOrganizationId, businessLevelId, UserRole.RoleName.Baseuser, null);
        List<UserAPI> customerPricelistApprovers = new ArrayList<>();
        customerPricelistApprovers.add(userAPIPricelistApprover);
        agreementAPI.setCustomerPricelistApprovers(customerPricelistApprovers);
        try {
            agreementController.setAgreementPricelistApprovers(customerOrganizationId, agreementAPI, agreement, userAPI);
            Assert.fail("Exception NOT thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("customerPricelistApprovers", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetAgreementSharedWithCustomers() {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        OrganizationAPI organizationAPI = OrganizationControllerTest.createValidOrganizationAPI(customerOrganizationId, Organization.OrganizationType.CUSTOMER, System.currentTimeMillis());
        List<OrganizationAPI> sharedWithCustomersAPIs = new ArrayList<>();
        sharedWithCustomersAPIs.add(organizationAPI);
        agreementAPI.setSharedWithCustomers(sharedWithCustomersAPIs);
        UserAPI userAPI = UserAPITest.createValidUserAPI(supplierAgreementUserId, supplierOrganizationId, null, UserRole.RoleName.SupplierAgreementManager, null);
        try {
            agreementController.setSharedWithCustomers(agreement, agreementAPI, userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    }

    @Test
    public void testSetAgreementSharedWithCustomersWrongOrganizationType() {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        OrganizationAPI organizationAPI = OrganizationControllerTest.createValidOrganizationAPI(supplierOrganizationId, Organization.OrganizationType.SUPPLIER, System.currentTimeMillis());
        List<OrganizationAPI> sharedWithCustomersAPIs = new ArrayList<>();
        sharedWithCustomersAPIs.add(organizationAPI);
        agreementAPI.setSharedWithCustomers(sharedWithCustomersAPIs);
        UserAPI userAPI = UserAPITest.createValidUserAPI(supplierAgreementUserId, supplierOrganizationId, null, UserRole.RoleName.SupplierAgreementManager, null);
        try {
            agreementController.setSharedWithCustomers(agreement, agreementAPI, userAPI);
            Assert.fail("Exception NOT thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("sharedWithCustomers", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetAgreementSharedWithCustomersNotExistingOrganization() {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        OrganizationAPI organizationAPI = OrganizationControllerTest.createValidOrganizationAPI(-1, Organization.OrganizationType.SUPPLIER, System.currentTimeMillis());
        List<OrganizationAPI> sharedWithCustomersAPIs = new ArrayList<>();
        sharedWithCustomersAPIs.add(organizationAPI);
        agreementAPI.setSharedWithCustomers(sharedWithCustomersAPIs);
        UserAPI userAPI = UserAPITest.createValidUserAPI(supplierAgreementUserId, supplierOrganizationId, null, UserRole.RoleName.SupplierAgreementManager, null);
        try {
            agreementController.setSharedWithCustomers(agreement, agreementAPI, userAPI);
            Assert.fail("Exception NOT thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("sharedWithCustomers", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetWarrantyQuantityUnits() throws HjalpmedelstjanstenValidationException {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        CVGuaranteeUnitAPI warrantyQuantityUnit = ArticleControllerTest.createValidGuaranteeUnitAPI(guaranteeUnitId);
        agreementAPI.setWarrantyQuantityHUnit(warrantyQuantityUnit);
        agreementAPI.setWarrantyQuantityIUnit(warrantyQuantityUnit);
        agreementAPI.setWarrantyQuantityRUnit(warrantyQuantityUnit);
        agreementAPI.setWarrantyQuantityTUnit(warrantyQuantityUnit);
        agreementAPI.setWarrantyQuantityTJUnit(warrantyQuantityUnit);
        agreementController.setWarrantyQuantityUnits(agreement, agreementAPI);

        Assert.assertNotNull(agreement.getWarrantyQuantityHUnit());
        Assert.assertNotNull(agreement.getWarrantyQuantityIUnit());
        Assert.assertNotNull(agreement.getWarrantyQuantityRUnit());
        Assert.assertNotNull(agreement.getWarrantyQuantityTUnit());
        Assert.assertNotNull(agreement.getWarrantyQuantityTJUnit());
    }

    @Test
    public void testSetWarrantyQuantityUnitsInvalidHUnit() {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        CVGuaranteeUnitAPI warrantyQuantityUnit = ArticleControllerTest.createValidGuaranteeUnitAPI(guaranteeUnitId+1);
        agreementAPI.setWarrantyQuantityHUnit(warrantyQuantityUnit);
        try {
            agreementController.setWarrantyQuantityUnits(agreement, agreementAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("warrantyQuantityHUnit", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetWarrantyQuantityUnitsInvalidIUnit() {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        CVGuaranteeUnitAPI warrantyQuantityUnit = ArticleControllerTest.createValidGuaranteeUnitAPI(guaranteeUnitId+1);
        agreementAPI.setWarrantyQuantityIUnit(warrantyQuantityUnit);
        try {
            agreementController.setWarrantyQuantityUnits(agreement, agreementAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("warrantyQuantityIUnit", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetWarrantyQuantityUnitsInvalidRUnit() {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        CVGuaranteeUnitAPI warrantyQuantityUnit = ArticleControllerTest.createValidGuaranteeUnitAPI(guaranteeUnitId+1);
        agreementAPI.setWarrantyQuantityRUnit(warrantyQuantityUnit);
        try {
            agreementController.setWarrantyQuantityUnits(agreement, agreementAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("warrantyQuantityRUnit", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetWarrantyQuantityUnitsInvalidTUnit() {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        CVGuaranteeUnitAPI warrantyQuantityUnit = ArticleControllerTest.createValidGuaranteeUnitAPI(guaranteeUnitId+1);
        agreementAPI.setWarrantyQuantityTUnit(warrantyQuantityUnit);
        try {
            agreementController.setWarrantyQuantityUnits(agreement, agreementAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("warrantyQuantityTUnit", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetWarrantyQuantityUnitsInvalidTJUnit() {
        Agreement agreement = createValidAgreement(null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        CVGuaranteeUnitAPI warrantyQuantityUnit = ArticleControllerTest.createValidGuaranteeUnitAPI(guaranteeUnitId+1);
        agreementAPI.setWarrantyQuantityTJUnit(warrantyQuantityUnit);
        try {
            agreementController.setWarrantyQuantityUnits(agreement, agreementAPI);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("warrantyQuantityTJUnit", errorMessageAPI.getField());
        }
    }

    @Test
    public void testOrganizationAllowedToReadAgreementSupplier() {
        Agreement agreement = createValidAgreement(null);
        Assert.assertTrue(agreementController.organizationAllowedToReadAgreement(agreement, supplierOrganizationId));
    }

    @Test
    public void testOrganizationAllowedToReadAgreementCustomer() {
        Agreement agreement = createValidAgreement(null);
        Assert.assertTrue(agreementController.organizationAllowedToReadAgreement(agreement, customerOrganizationId));
    }

    @Test
    public void testOrganizationAllowedToReadAgreementSharedWithCustomer() {
        Agreement agreement = createValidAgreement(null);
        List<Organization> sharedWithCustomers = new ArrayList<>();
        sharedWithCustomers.add(OrganizationControllerTest.createValidOrganization(sharedWithCustomerOrganizationId, Organization.OrganizationType.CUSTOMER, new Date(), null, null));
        agreement.setSharedWithCustomers(sharedWithCustomers);
        Assert.assertTrue(agreementController.organizationAllowedToReadAgreement(agreement, sharedWithCustomerOrganizationId));
    }

    @Test
    public void testOrganizationAllowedToReadAgreementSharedWithCustomerBusinessLevel() {
        Agreement agreement = createValidAgreement(null);
        List<BusinessLevel> sharedWithCustomerBusinessLevels = new ArrayList<>();
        BusinessLevel businessLevel = BusinessLevelControllerTest.createValidBusinessLevel(businessLevelId, sharedWithCustomerBusinessLevelOrganizationId, BusinessLevel.Status.ACTIVE);
        sharedWithCustomerBusinessLevels.add(businessLevel);
        agreement.setSharedWithCustomerBusinessLevels(sharedWithCustomerBusinessLevels);
        Assert.assertTrue(agreementController.organizationAllowedToReadAgreement(agreement, sharedWithCustomerBusinessLevelOrganizationId));
    }

    @Test
    public void testOrganizationAllowedToReadAgreementFalse() {
        Agreement agreement = createValidAgreement(null);
        Assert.assertFalse(agreementController.organizationAllowedToReadAgreement(agreement, customerOrganizationId+1000));
    }

    @Test
    public void testOrganizationAllowedToReadAgreementSharedWithCustomerBusinessLevelFalse() {
        Agreement agreement = createValidAgreement(null);
        List<BusinessLevel> sharedWithCustomerBusinessLevels = new ArrayList<>();
        BusinessLevel businessLevel = BusinessLevelControllerTest.createValidBusinessLevel(businessLevelId, sharedWithCustomerBusinessLevelOrganizationId, BusinessLevel.Status.ACTIVE);
        sharedWithCustomerBusinessLevels.add(businessLevel);
        agreement.setSharedWithCustomerBusinessLevels(sharedWithCustomerBusinessLevels);
        Assert.assertFalse(agreementController.organizationAllowedToReadAgreement(agreement, sharedWithCustomerBusinessLevelOrganizationId+1000));
    }

    @Test
    public void testCheckAgreementPricelistApproverBusinessLevelNoEngagementUserNoEngagementAgreement() throws HjalpmedelstjanstenValidationException {
        Agreement agreement = createValidAgreement(null);
        // user with no business level can handle pricelist without business level
        UserEngagement userEngagement = UserControllerTest.createValidUserEngagement(customerOrganizationId, pricelistApproverUserId, Organization.OrganizationType.CUSTOMER, UserRole.RoleName.CustomerAgreementManager, null);
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        agreementController.checkAgreementPricelistApproverBusinessLevel(customerOrganizationId, agreement, userEngagement, userAPI);
    }

    @Test
    public void testCheckAgreementPricelistApproverBusinessLevelNoEngagementUserEngagementAgreement() throws HjalpmedelstjanstenValidationException {
        Agreement agreement = createValidAgreement(businessLevelId);
        // user with no business level can handle pricelist without business level
        UserEngagement userEngagement = UserControllerTest.createValidUserEngagement(customerOrganizationId, pricelistApproverUserId, Organization.OrganizationType.CUSTOMER, UserRole.RoleName.CustomerAgreementManager, null);
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        agreementController.checkAgreementPricelistApproverBusinessLevel(customerOrganizationId, agreement, userEngagement, userAPI);
    }

    @Test
    public void testCheckAgreementPricelistApproverBusinessLevelSameEngagementUserEngagementAgreement() throws HjalpmedelstjanstenValidationException {
        Agreement agreement = createValidAgreement(businessLevelId);
        // user with no business level can handle pricelist without business level
        UserEngagement userEngagement = UserControllerTest.createValidUserEngagement(customerOrganizationId, pricelistApproverUserId, Organization.OrganizationType.CUSTOMER, UserRole.RoleName.CustomerAgreementManager, businessLevelId);
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        agreementController.checkAgreementPricelistApproverBusinessLevel(customerOrganizationId, agreement, userEngagement, userAPI);
    }

    @Test
    public void testCheckAgreementPricelistApproverBusinessLevelNotSameEngagementUserEngagementAgreement() {
        Agreement agreement = createValidAgreement(businessLevelId2);
        // user with no business level can handle pricelist without business level
        UserEngagement userEngagement = UserControllerTest.createValidUserEngagement(customerOrganizationId, pricelistApproverUserId, Organization.OrganizationType.CUSTOMER, UserRole.RoleName.CustomerAgreementManager, businessLevelId);
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        try {
            agreementController.checkAgreementPricelistApproverBusinessLevel(customerOrganizationId, agreement, userEngagement, userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("customerPricelistApprovers", errorMessageAPI.getField());
        }
    }

    @Test
    public void testGetUserEngagementAPIFound() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        Assert.assertNotNull(agreementController.getUserEngagementAPI(userAPI, customerOrganizationId));
    }

    @Test
    public void testGetUserEngagementAPINotFound() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        Assert.assertNull(agreementController.getUserEngagementAPI(userAPI, supplierOrganizationId));
    }
    //Removed since the test-case is no longer valid.
  /*  @Test
    public void testCreateAgreement() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        agreementAPI.setCustomerBusinessLevel(null);
        try {
            agreementController.createAgreement(customerOrganizationId, agreementAPI, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        } catch (HjalpmedelstjanstenException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
    } */

    @Test
    public void testCreateAgreementNODeliveryTimes() {
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        AgreementAPI agreementAPI = createValidAgreementAPI();
        agreementAPI.setCustomerBusinessLevel(null);
        agreementAPI.setDeliveryTimeH(null);
        try {
            agreementController.createAgreement(customerOrganizationId, agreementAPI, userAPI, null, null);
            Assert.fail("Exception not thrown, but should");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("deliveryTimeH", errorMessageAPI.getField());
        } catch (HjalpmedelstjanstenException ex) {
            Assert.fail("HjalpmedelstjanstenException thrown, but shouldn't");
        }
    }

    public static AgreementAPI createValidAgreementAPI() {
        AgreementAPI agreementAPI = new AgreementAPI();
        agreementAPI.setId(agreementId);
        agreementAPI.setAgreementName(agreementName);
        agreementAPI.setAgreementNumber(agreementNumber);
        Calendar validFrom = Calendar.getInstance();
        agreementAPI.setValidFrom(validFrom.getTimeInMillis());
        agreementAPI.setValidTo(validFrom.getTimeInMillis() + 1000000);
        agreementAPI.setSupplierOrganization(OrganizationControllerTest.createValidOrganizationAPI(supplierOrganizationId, Organization.OrganizationType.SUPPLIER, System.currentTimeMillis()));
        agreementAPI.setCustomerBusinessLevel(BusinessLevelAPITest.createValidBusinessLevelAPI(businessLevelId, BusinessLevel.Status.ACTIVE));
        List<UserAPI> customerPricelistApprovers = new ArrayList<>();
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, customerOrganizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
        customerPricelistApprovers.add(userAPI);
        agreementAPI.setCustomerPricelistApprovers(customerPricelistApprovers);
        agreementAPI.setDeliveryTimeH(1);
        agreementAPI.setDeliveryTimeI(1);
        agreementAPI.setDeliveryTimeR(1);
        agreementAPI.setDeliveryTimeT(1);
        agreementAPI.setDeliveryTimeTJ(1);
        return agreementAPI;
    }

    public static Agreement createValidAgreement(Long businessLevelId, Long customerId, Long supplierId) {
        Agreement agreement = new Agreement();
        agreement.setUniqueId(agreementId);
        agreement.setAgreementName(agreementName);
        agreement.setAgreementNumber(agreementNumber);
        agreement.setValidFrom(new Date(System.currentTimeMillis()));
        agreement.setValidTo(new Date(System.currentTimeMillis() + 1000000));
        agreement.setStatus(Agreement.Status.CURRENT);
        agreement.setCustomer(OrganizationControllerTest.createValidOrganization(customerId, Organization.OrganizationType.CUSTOMER, new Date(), businessLevelId, null));
        agreement.setSupplier(OrganizationControllerTest.createValidOrganization(supplierId, Organization.OrganizationType.SUPPLIER, new Date(), null, null));
        if( businessLevelId != null ) {
            BusinessLevel businessLevel = BusinessLevelControllerTest.createValidBusinessLevel(businessLevelId, customerOrganizationId, BusinessLevel.Status.ACTIVE);
            agreement.setCustomerBusinessLevel(businessLevel);
        }
        return agreement;
    }

    public static Agreement createValidAgreement(Long businessLevelId) {
        return createValidAgreement(businessLevelId, customerOrganizationId, supplierOrganizationId);
    }

    public static Agreement createValidAgreement(Long businessLevelId, Long pricelistApproverUserEngagementId, Long customerId, Long supplierId) {
        Agreement agreement = createValidAgreement(businessLevelId, customerId, supplierId);
        List<UserEngagement> userEngagements = new ArrayList<>();
        UserEngagement userEngagement = new UserEngagement();
        userEngagement.setUniqueId(pricelistApproverUserEngagementId);
        userEngagements.add(userEngagement);
        agreement.setCustomerPricelistApprovers(userEngagements);
        return agreement;
    }

}
