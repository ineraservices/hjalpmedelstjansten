package se.inera.hjalpmedelstjansten.business.generalpricelist.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.UserAPITest;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import jakarta.enterprise.event.Event;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class GeneralPricelistPricelistControllerTest {

    UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
    GeneralPricelistPricelistController pricelistController;
    GeneralPricelistController generalPricelistController;
    EntityManager myEntityManager;
    Event<InternalAuditEvent> event;

    static final long pricelistId = 1;
    static final long userId = 2;
    static final long organizationId = 3;
    static final long generalPricelistId = 4;
    static final String pricelistNumber = "001";

    @Before
    public void init() {
        pricelistController = new GeneralPricelistPricelistController();
        pricelistController.LOG = new NoLogger();
        pricelistController.defaultPricelistNumber = pricelistNumber;

        event = Mockito.mock(Event.class);
        pricelistController.internalAuditEvent = event;

        myEntityManager = Mockito.mock(EntityManager.class);
        Mockito.when(myEntityManager.find(GeneralPricelistPricelist.class, pricelistId)).thenReturn(createValidPricelist(pricelistId, pricelistNumber, new Date(), new Date(), generalPricelistId, organizationId));
        Query mockQuery = Mockito.mock(Query.class);
        Mockito.when(mockQuery.setParameter(Mockito.anyString(), Mockito.any())).thenReturn(mockQuery);
        List<GeneralPricelistPricelist> currentPricelists = new ArrayList<>();
        GeneralPricelistPricelist currentPricelist = createValidPricelist(pricelistId, pricelistNumber, new Date(), new Date(), generalPricelistId, organizationId);
        currentPricelists.add(currentPricelist);
        Mockito.when(mockQuery.getResultList()).thenReturn(currentPricelists);
        Mockito.when(myEntityManager.createNamedQuery(GeneralPricelistPricelist.FIND_BY_PASSED_VALID_FROM_AND_ORGANIZATION)).thenReturn(mockQuery);
        Query mockQuery2 = Mockito.mock(Query.class);
        Mockito.when(mockQuery2.setParameter(Mockito.anyString(), Mockito.any())).thenReturn(mockQuery2);
        Mockito.when(mockQuery.getSingleResult()).thenReturn(2l);
        Mockito.when(myEntityManager.createNamedQuery(GeneralPricelistPricelistRow.COUNT_BY_PRICELIST)).thenReturn(mockQuery2);
        pricelistController.em = myEntityManager;

        generalPricelistController = Mockito.mock(GeneralPricelistController.class);
        Mockito.when(generalPricelistController.getGeneralPricelist(organizationId)).thenReturn(createValidGeneralPricelist(generalPricelistId, organizationId, new Date(), new Date()));
        pricelistController.generalPricelistController = generalPricelistController;

        GeneralPricelistPricelistController mockedPricelistController = Mockito.mock(GeneralPricelistPricelistController.class);

        GeneralPricelistPricelistValidation pricelistValidation = new GeneralPricelistPricelistValidation();
        pricelistValidation.LOG = new NoLogger();
        pricelistValidation.pricelistController = mockedPricelistController;
        pricelistController.pricelistValidation = pricelistValidation;
    }

    @Test
    public void testGetPricelist() {
        GeneralPricelistPricelist pricelist = pricelistController.getPricelist(organizationId, generalPricelistId, pricelistId, userAPI);
        Assert.assertNotNull(pricelist);
    }

    @Test
    public void testGetPricelistWrongGeneralPricelistUniqueId() {
        GeneralPricelistPricelist pricelist = pricelistController.getPricelist(organizationId, 1000000, pricelistId, userAPI);
        Assert.assertNull(pricelist);
    }

    @Test
    public void testGetPricelistWrongPricelistUniqueId() {
        GeneralPricelistPricelist pricelist = pricelistController.getPricelist(organizationId, generalPricelistId, 100000, userAPI);
        Assert.assertNull(pricelist);
    }

    @Test
    public void testGetPricelistAPI() {
        GeneralPricelistPricelistAPI pricelistAPI = pricelistController.getPricelistAPI(organizationId, pricelistId, userAPI, null, null);
        Assert.assertNotNull(pricelistAPI);
    }

    @Test
    public void testGetPricelistAPIWrongPricelistUniqueId() {
        GeneralPricelistPricelistAPI pricelistAPI = pricelistController.getPricelistAPI(organizationId, 100000, userAPI, null, null);
        Assert.assertNull(pricelistAPI);
    }

    @Test
    public void testCreateFirstPricelistForGeneralPricelist() {
        Calendar validFrom = Calendar.getInstance();
        validFrom.add(Calendar.DAY_OF_YEAR, 1);
        Calendar validTo = Calendar.getInstance();
        validTo.add(Calendar.YEAR, 1);
        GeneralPricelist generalPricelist = createValidGeneralPricelist(generalPricelistId, organizationId, validFrom.getTime(), validTo.getTime());
        GeneralPricelistPricelistAPI generalPricelistPricelistAPI = null;

        try {
            generalPricelistPricelistAPI = pricelistController.createFirstPricelistForGeneralPricelist(generalPricelist, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
        Assert.assertNotNull(generalPricelistPricelistAPI);
        Assert.assertEquals(pricelistNumber, generalPricelistPricelistAPI.getNumber());
    }

    @Test
    public void testCreatePricelist() {
        Calendar validFrom = Calendar.getInstance();
        validFrom.add(Calendar.DAY_OF_YEAR, 1);
        GeneralPricelistPricelistAPI pricelistAPI = createValidPricelistAPI(generalPricelistId, pricelistNumber, validFrom.getTimeInMillis());
        try {
            pricelistAPI = pricelistController.createPricelist(organizationId, pricelistAPI, userAPI, null, null);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown, but shouldn't");
        }
        Assert.assertNotNull(pricelistAPI);
        Assert.assertEquals(pricelistNumber, pricelistAPI.getNumber());
    }

    public static GeneralPricelistPricelistAPI createValidPricelistAPI(long id, String number, long validFrom) {
        GeneralPricelistPricelistAPI pricelistAPI = new GeneralPricelistPricelistAPI();
        pricelistAPI.setId(id);
        pricelistAPI.setNumber(number);
        pricelistAPI.setValidFrom(validFrom);
        return pricelistAPI;
    }

    public static GeneralPricelistPricelist createValidPricelist( long id, String number, Date validFrom, Date validTo, Long generalPricelistId, Long organizationId ) {
        GeneralPricelistPricelist pricelist = new GeneralPricelistPricelist();
        pricelist.setUniqueId(id);
        pricelist.setNumber(number);
        pricelist.setValidFrom(validFrom);
        if( generalPricelistId != null ) {
            pricelist.setGeneralPricelist(createValidGeneralPricelist(generalPricelistId, organizationId, validFrom, validTo));
        }
        return pricelist;
    }

    private static GeneralPricelist createValidGeneralPricelist(long generalPricelistId, Long organizationId, Date validFrom, Date validTo) {
        GeneralPricelist generalPricelist = new GeneralPricelist();
        generalPricelist.setUniqueId(generalPricelistId);
        generalPricelist.setValidFrom(validFrom);
        generalPricelist.setValidTo(validTo);
        if( organizationId != null ) {
            Organization organization = new Organization();
            organization.setUniqueId(organizationId);
            organization.setOrganizationType(Organization.OrganizationType.SUPPLIER);
            organization.setValidFrom(validFrom);
            generalPricelist.setOwnerOrganization(organization);
        }
        return generalPricelist;
    }

}
