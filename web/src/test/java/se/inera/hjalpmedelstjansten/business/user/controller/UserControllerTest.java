package se.inera.hjalpmedelstjansten.business.user.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.business.organization.controller.BusinessLevelControllerTest;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationControllerTest;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;
import se.inera.hjalpmedelstjansten.model.entity.ElectronicAddress;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.UserAccount;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class UserControllerTest {
    
    UserController userController;
    
    private Long userEngagementId = 1l;
    private Date userEngagementFromDate = new Date();
    private Date userEngagementToDate = new Date();
    private String userAccountFirstName = "userAccountFirstName";
    private String userAccountLastName = "userAccountLastName";
    private String userAccountTitle = "userAccountTitle";
    private String userAccountUsername = "userAccountUsername";
    private Long electronicAddressUniqueId = 2l;
    private String electronicAddressEmail = "electronicAddressEmail";
    private String electronicAddressMobile = "electronicAddressMobile";
    private String electronicAddressTelephone = "electronicAddressTelephone";
    
    @Before
    public void init() {
        userController = new UserController();
    }

    @Test
    public void testMapSearchToUserEngagements() {
        List<Object[]> objectArrayList = new ArrayList<>();
        Object[] objectArray = new Object[11];
        objectArray[0] = userEngagementId; // unique id of user engagement
        objectArray[1] = userEngagementFromDate;
        objectArray[2] = userEngagementToDate;
        objectArray[3] = userAccountFirstName;
        objectArray[4] = userAccountLastName;
        objectArray[5] = userAccountTitle;
        objectArray[6] = userAccountUsername;
        objectArray[7] = electronicAddressUniqueId;
        objectArray[8] = electronicAddressEmail;
        objectArray[9] = electronicAddressMobile;
        objectArray[10] = electronicAddressTelephone;
        objectArrayList.add(objectArray);
        List<UserEngagement> userEngagements = UserMapper.mapSearchToUserEngagements(objectArrayList);
        Assert.assertNotNull(userEngagements);
        Assert.assertEquals(1, userEngagements.size());
        UserEngagement userEngagement = userEngagements.get(0);
        Assert.assertEquals(userEngagementId, userEngagement.getUniqueId());
        Assert.assertEquals(userEngagementFromDate, userEngagement.getValidFrom());
        Assert.assertEquals(userEngagementToDate, userEngagement.getValidTo());
        Assert.assertEquals(userAccountFirstName, userEngagement.getUserAccount().getFirstName());
        Assert.assertEquals(userAccountLastName, userEngagement.getUserAccount().getLastName());
        Assert.assertEquals(userAccountTitle, userEngagement.getUserAccount().getTitle());
        Assert.assertEquals(userAccountUsername, userEngagement.getUserAccount().getUsername());
        Assert.assertEquals(electronicAddressUniqueId, userEngagement.getUserAccount().getElectronicAddress().getUniqueId());
        Assert.assertEquals(electronicAddressEmail, userEngagement.getUserAccount().getElectronicAddress().getEmail());
        Assert.assertEquals(electronicAddressMobile, userEngagement.getUserAccount().getElectronicAddress().getMobile());
        Assert.assertEquals(electronicAddressTelephone, userEngagement.getUserAccount().getElectronicAddress().getTelephone());
    }
        
    public static UserEngagement createValidUserEngagement(long organizationUniqueId, long userUniqueId, Organization.OrganizationType organizationType, UserRole.RoleName roleName, Long businessLevel) {
        UserEngagement userEngagement = new UserEngagement();
        userEngagement.setUniqueId(userUniqueId);
        userEngagement.setOrganization(OrganizationControllerTest.createValidOrganization(organizationUniqueId, organizationType, new Date(), null, null));
        userEngagement.setValidFrom(new Date());
        List<UserRole> userRoles = new ArrayList<>();
        userRoles.add(createValidUserRole(roleName));        
        userEngagement.setRoles(userRoles);
        if( businessLevel != null ) {
            List<BusinessLevel> businessLevels = new ArrayList<>();
            businessLevels.add(BusinessLevelControllerTest.createValidBusinessLevel(businessLevel, organizationUniqueId, BusinessLevel.Status.ACTIVE));
            userEngagement.setBusinessLevels(businessLevels);
        }
        UserAccount userAccount = new UserAccount();
        userAccount.setFirstName("firstName");
        userAccount.setLastName("lastName");
        ElectronicAddress electronicAddress = new ElectronicAddress();
        userAccount.setElectronicAddress(electronicAddress);
        
        userEngagement.setUserAccount(userAccount);
        return userEngagement;
    }
    
    public static UserRole createValidUserRole( UserRole.RoleName roleName ) {
        UserRole userRole = new UserRole();
        userRole.setName(roleName);
        return userRole;
    }
        
}
