package se.inera.hjalpmedelstjansten.business.logging.view;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static se.inera.hjalpmedelstjansten.business.logging.view.MDCHelper.LOG_TRACE_ID_HEADER;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_ORGANIZATION_ID;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_SESSION_ID;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_TRACE_ID;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_USER_ID;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_USER_NAME;
import static se.inera.hjalpmedelstjansten.business.logging.view.MdcConstants.MDC_USER_ROLE;

import java.util.List;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.MDC;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.model.api.RoleAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.UserEngagementAPI;
import se.inera.hjalpmedelstjansten.model.entity.UserRole.RoleName;

@ExtendWith(MockitoExtension.class)
class MDCHelperTest {

    private static final String ROLE_ONE = RoleName.Superadmin.name();
    private static final String ROLE_TWO = RoleName.CustomerAgreementManager.name();

    @Mock
    private HttpServletRequest request;

    @Mock
    private AuthHandler authHandler;

    @InjectMocks
    private MDCHelper mdchelper;

    @Nested
    class TestTraceId {

        @Test
        void shouldReturnTraceIdFromHeader() {
            final var expectedValue = "traceId";
            when(request.getHeader(LOG_TRACE_ID_HEADER)).thenReturn(expectedValue);
            mdchelper.decorateWithUser(request);
            assertEquals(expectedValue, MDC.get(MDC_TRACE_ID));
        }

        @Test
        void shouldGenerateTraceIdIfNotPresentInHeader() {
            mdchelper.decorateWithUser(request);
            assertNotNull(MDC.get(MDC_TRACE_ID));
        }
    }

    @Nested
    class TestClear {

        @Test
        void shouldClearMDC() {
            mdchelper.decorateWithUser(request);
            assertFalse(MDC.getCopyOfContextMap().isEmpty());
            mdchelper.clear();
            assertNull(MDC.getCopyOfContextMap());
        }
    }

    @Nested
    class TestSessionId {

        @Test
        void shouldReturnSessionIfAuthenticated() {
            final var expectedValue = "sessionId";

            doReturn(true).when(authHandler).isAthenticated();
            doReturn(expectedValue).when(authHandler).getSessionId();

            mdchelper.decorateWithUser(request);

            assertEquals(expectedValue, MDC.get(MDC_SESSION_ID));
        }

        @Test
        void shouldNotReturnSessionIfNotAuthenticated() {
            mdchelper.decorateWithUser(request);
            assertNull(MDC.get(MDC_SESSION_ID));
        }
    }

    @Nested
    class TestUser {

        private static final long USER_ID = 99L;
        private static final String USER_NAME = "username";
        private static final long ORGANIZATION_ID = 999L;

        @BeforeEach
        void setUp() {
            final var userApi = new UserAPI();
            userApi.setId(USER_ID);
            userApi.setUsername(USER_NAME);

            final var userEngagementAPI = new UserEngagementAPI();
            userEngagementAPI.setOrganizationId(ORGANIZATION_ID);

            final var roleAPIOne = new RoleAPI();
            roleAPIOne.setName(ROLE_ONE);

            final var roleAPITwo = new RoleAPI();
            roleAPITwo.setName(ROLE_TWO);

            userEngagementAPI.setRoles(
                List.of(roleAPIOne, roleAPITwo)
            );

            userApi.setUserEngagementAPIs(
                List.of(userEngagementAPI)
            );

            doReturn(true).when(authHandler).isAthenticated();
            doReturn(userApi).when(authHandler).getFromSession(AuthHandler.USER_API_SESSION_KEY);
        }

        @Test
        void shouldReturnUserId() {
            mdchelper.decorateWithUser(request);

            assertEquals(Long.toString(USER_ID), MDC.get(MDC_USER_ID));
        }

        @Test
        void shouldReturnUserName() {
            mdchelper.decorateWithUser(request);

            assertEquals(USER_NAME, MDC.get(MDC_USER_NAME));
        }

        @Test
        void shouldReturnRoles() {
            mdchelper.decorateWithUser(request);

            assertEquals(List.of(ROLE_ONE, ROLE_TWO).toString(), MDC.get(MDC_USER_ROLE));
        }

        @Test
        void shouldReturnOrganizationId() {
            mdchelper.decorateWithUser(request);

            assertEquals(Long.toString(ORGANIZATION_ID), MDC.get(MDC_ORGANIZATION_ID));
        }
    }
}
