package se.inera.hjalpmedelstjansten.business.product.controller;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jakarta.enterprise.event.Event;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowController;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistRowController;
import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationControllerTest;
import se.inera.hjalpmedelstjansten.business.product.service.SendMailRegardingDiscontinuedArticlesOnAssortment;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEDirectiveAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEStandardAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVOrderUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPackageUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.ProductAPITest;
import se.inera.hjalpmedelstjansten.model.api.validation.UserAPITest;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificPropertyListValue;
import se.inera.hjalpmedelstjansten.model.entity.ElectronicAddress;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.Product.Status;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEDirective;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEStandard;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;


public class ArticleControllerTest {

    ArticleController articleController;
    OrderUnitController unitController;
    PackageUnitController packageUnitController;
    OrganizationController organizationController;
    CategoryController categoryController;
    CeController ceController;
    EntityManager myEntityManager;
    Event<InternalAuditEvent> event;
    ProductController productController;
    AgreementPricelistRowController agreementPricelistRowController;
    GeneralPricelistPricelistRowController generalPricelistPricelistRowController;
    CVPreventiveMaintenanceController cVPreventiveMaintenanceController;
    UserController userController;
    ElasticSearchController elasticSearchController;
    UserAPI userAPI;
    SendMailRegardingDiscontinuedArticlesOnAssortment sendMailRegardingDiscontinuedArticlesOnAssortment;

    static String articleName = "Article name";
    static String articleArticleNumber = "Article number";
    static Product.Status articleStatus = Product.Status.PUBLISHED;
    static String articleSupplementedInformation = "Article supplemented information";
    static String articleColor = "Article color";
    static String articleTrademark = "Article trademark";
    static String articleManufacturer = "Article manufacturer";
    static String articleManufacturerProductNumber = "Article manufacturer article number";
    static String articleManufacturerWeb = "https://www.inera.se";
    static boolean articleCeMarked = true;
    static String articleCeDirectiveName = "Article Ce Directive name";
    static String articleCeStandardName = "Article Ce Standard name";
    static String articlePreventiveMaintenanceDescription = "Article Preventive maintenance description";
    static int articlePreventiveMaintenanceNumberOfDays = 3;
    static String articleGtin = "1000000000009";
    static String articleOrderUnitName = "Product order unit name";
    static String articleOrderUnitCode = "Product order unit code";
    static String articleGuaranteeUnitName = "Product guarantee unit name";
    static String articleGuaranteeUnitCode = "Product guarantee unit code";
    static Double articleArticleQuantityInOuterPackage = 112.0;
    static Double articlePackageContent = 113.0;
    static Integer articlePackageLevelBase = 114;
    static Integer articlePackageLevelMiddle = 115;
    static Integer articlePackageLevelTop = 116;
    static boolean articleCustomerUnique = false;
    static String organizationName = "organization name";
    static String resourceSpecificPropertyValueTextField = "text data";
    static double resourceSpecificPropertyValueDecimal = 1.0;
    static double resourceSpecificPropertyValueIntervalFrom = 1.0;
    static double resourceSpecificPropertyValueIntervalTo = 10.0;
    static String preventiveMaintenanceUnitCode = "code";
    static String preventiveMaintenanceUnitName = "name";
    static CVPreventiveMaintenance articlePreventiveMaintenanceValidFrom = PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenance(preventiveMaintenanceUnitCode, preventiveMaintenanceUnitName);
    static Map<Long, CVOrderUnit> orderUnitIdMap;
    static Map<Long, CVPackageUnit> packageUnitIdMap;

    static long articleId = 100;
    static long organizationId = 102;
    static long categoryId = 103;
    static long articleCeDirectiveId = 104;
    static long articleCeStandardId = 105;
    static long fitsToProductId = 106;
    static long articleOrderUnitId = 107;
    static long fitsToNonExistingArticleId = 108;
    static long fitsToProductTId = 109;
    static long fitsToProductIId = 110;
    static long fitsToProductTjId = 111;
    static long fitsToProductRId = 112;
    static long fitsToArticleIId = 113;
    static long fitsToArticleTId = 114;
    static long fitsToArticleTjId = 115;
    static long fitsToArticleRId = 116;
    static long fitsToArticleHId = 117;
    static long fitsToProductTNoCodeId = 118;
    static long fitsToArticleTNoCodeId = 119;
    static long userId = 120;
    static long articleBasedOnProductTypeHId = 121;
    static long articleBasedOnProductTypeTId = 122;
    static long categorySpecificPropertyIdTextField = 123;
    static long categorySpecificPropertyIdDecimal = 124;
    static long categorySpecificPropertyIdInterval = 125;
    static long categorySpecificPropertyIdValueListSingle = 126;
    static long categorySpecificPropertyIdValueListMultiple = 127;
    static long resourceSpecificPropertyListValue1 = 128;
    static long resourceSpecificPropertyListValue2 = 129;
    static long resourceSpecificPropertyListValue3 = 130;
    static long resourceSpecificPropertyValueId = 131;
    static long categoryRId = 132;
    static long categoryIId = 133;
    static long categoryTjId = 134;
    static long categoryHId = 135;

    @Before
    public void init() {
        userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.SupplierProductAndArticleHandler, null);
        myEntityManager = mock(EntityManager.class);
        Mockito.when(myEntityManager.find(Article.class, articleId)).thenReturn(createValidArticle(articleId, organizationId));
        Article articleBasedOnProductTypeH = createValidArticle(articleBasedOnProductTypeHId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        basedOnProduct.getCategory().setArticleType(Article.Type.H);
        articleBasedOnProductTypeH.setCategory(null);
        articleBasedOnProductTypeH.setBasedOnProduct(basedOnProduct);
        Mockito.when(myEntityManager.find(Article.class, articleBasedOnProductTypeHId)).thenReturn(articleBasedOnProductTypeH);

        Article articleBasedOnProductTypeT = createValidArticle(articleBasedOnProductTypeTId, organizationId);
        Product basedOnProductTypeT = createBasedOnProductSameValues();
        basedOnProductTypeT.getCategory().setArticleType(Article.Type.T);
        articleBasedOnProductTypeT.setCategory(null);
        articleBasedOnProductTypeT.setBasedOnProduct(basedOnProductTypeT);
        Mockito.when(myEntityManager.find(Article.class, articleBasedOnProductTypeTId)).thenReturn(articleBasedOnProductTypeT);

        Query mockQuery = mock(Query.class);
        Mockito.when(mockQuery.setParameter(Mockito.anyString(), Mockito.any())).thenReturn(mockQuery);
        Mockito.when(mockQuery.getResultList()).thenReturn(new ArrayList());
        Mockito.when(myEntityManager.createNamedQuery(Article.FIND_BY_GTIN)).thenReturn(mockQuery);
        Mockito.when(myEntityManager.createNamedQuery(Article.FIND_BY_ARTICLE_NUMBER_AND_ORGANIZATION)).thenReturn(mockQuery);
        Mockito.when(myEntityManager.createNamedQuery(Article.GET_IDS_FITS_TO_ARTICLE)).thenReturn(mockQuery);
        articleController = new ArticleController();
        articleController.LOG = new NoLogger();
        articleController.em = myEntityManager;
        organizationController = mock(OrganizationController.class);
        Mockito.when(organizationController.getOrganization(organizationId)).thenReturn(OrganizationControllerTest.createValidOrganization(organizationId, Organization.OrganizationType.SUPPLIER, new Date(), null, organizationName));
        articleController.organizationController = organizationController;
        categoryController = mock(CategoryController.class);

        List<Category> categorys = new ArrayList<>();
        Category category = CategoryControllerTest.createValidCategory(categoryId, Article.Type.T, null);
        categorys.add(category);
        Mockito.when(categoryController.getChildlessById(categoryId)).thenReturn(categorys);

        List<Category> rcategorys = new ArrayList<>();
        Category categoryR = CategoryControllerTest.createValidCategory(categoryRId, Article.Type.R, null);
        rcategorys.add(categoryR);
        Mockito.when(categoryController.getChildlessById(categoryRId)).thenReturn(rcategorys);

        List<Category> icategorys = new ArrayList<>();
        Category categoryI = CategoryControllerTest.createValidCategory(categoryIId, Article.Type.I, null);
        icategorys.add(categoryI);
        Mockito.when(categoryController.getChildlessById(categoryIId)).thenReturn(icategorys);

        List<Category> tjcategorys = new ArrayList<>();
        Category categoryTj = CategoryControllerTest.createValidCategory(categoryTjId, Article.Type.Tj, null);
        tjcategorys.add(categoryTj);
        Mockito.when(categoryController.getChildlessById(categoryTjId)).thenReturn(tjcategorys);

        List<Category> hcategorys = new ArrayList<>();
        Category categoryH = CategoryControllerTest.createValidCategory(categoryHId, Article.Type.H, "123456");
        hcategorys.add(categoryH);
        Mockito.when(categoryController.getChildlessById(categoryHId)).thenReturn(hcategorys);

        CategorySpecificPropertyListValue categorySpecificPropertyListValue1 = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue1.setUniqueId(resourceSpecificPropertyListValue1);
        Mockito.when(categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyListValue1)).thenReturn(categorySpecificPropertyListValue1);
        CategorySpecificPropertyListValue categorySpecificPropertyListValue2 = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue2.setUniqueId(resourceSpecificPropertyListValue2);
        Mockito.when(categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyListValue2)).thenReturn(categorySpecificPropertyListValue2);
        CategorySpecificPropertyListValue categorySpecificPropertyListValue3 = new CategorySpecificPropertyListValue();
        categorySpecificPropertyListValue3.setUniqueId(resourceSpecificPropertyListValue3);
        Mockito.when(categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyListValue3)).thenReturn(categorySpecificPropertyListValue3);
        articleController.categoryController = categoryController;
        event = mock(Event.class);
        articleController.internalAuditEvent = event;
        ValidationMessageService validationMessageService = mock(ValidationMessageService.class);
        Mockito.when(validationMessageService.generateValidationException(Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer<HjalpmedelstjanstenValidationException>() {
            @Override
            public HjalpmedelstjanstenValidationException answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException((String) args[0] + "->" + (String) args[1]);
                exception.addValidationMessage((String) args[0], (String) args[1]);
                return exception;
            }
        });
        Mockito.when(validationMessageService.generateValidationException(Mockito.anyString(), Mockito.anyString(), Mockito.any())).thenAnswer(new Answer<HjalpmedelstjanstenValidationException>() {
            @Override
            public HjalpmedelstjanstenValidationException answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException((String) args[0] + "->" + (String) args[1]);
                exception.addValidationMessage((String) args[0], (String) args[1]);
                return exception;
            }
        });
        articleController.validationMessageService = validationMessageService;

        productController = mock(ProductController.class);
        Product connectedToProduct = ProductControllerTest.createValidProduct(fitsToProductId, organizationId);
        connectedToProduct.getCategory().setArticleType(Article.Type.H);
        Mockito.when(productController.getProduct(organizationId, fitsToProductId, userAPI)).thenReturn(connectedToProduct);
        Product connectedToProductI = ProductControllerTest.createValidProduct(fitsToProductIId, organizationId);
        connectedToProductI.getCategory().setArticleType(Article.Type.I);
        Mockito.when(productController.getProduct(organizationId, fitsToProductIId, userAPI)).thenReturn(connectedToProductI);
        Product connectedToProductT = ProductControllerTest.createValidProduct(fitsToProductTId, organizationId);
        connectedToProductT.getCategory().setArticleType(Article.Type.T);
        Mockito.when(productController.getProduct(organizationId, fitsToProductTId, userAPI)).thenReturn(connectedToProductT);
        Product connectedToProductTj = ProductControllerTest.createValidProduct(fitsToProductTjId, organizationId);
        connectedToProductTj.getCategory().setArticleType(Article.Type.Tj);
        Mockito.when(productController.getProduct(organizationId, fitsToProductTjId, userAPI)).thenReturn(connectedToProductTj);
        Product connectedToProductR = ProductControllerTest.createValidProduct(fitsToProductRId, organizationId);
        connectedToProductR.getCategory().setArticleType(Article.Type.R);
        Mockito.when(productController.getProduct(organizationId, fitsToProductRId, userAPI)).thenReturn(connectedToProductR);
        Product connectedToProductTNoCode = ProductControllerTest.createValidProduct(fitsToProductTNoCodeId, organizationId);
        connectedToProductTNoCode.getCategory().setArticleType(Article.Type.T);
        connectedToProductTNoCode.getCategory().setCode(null);
        Mockito.when(productController.getProduct(organizationId, fitsToProductTNoCodeId, userAPI)).thenReturn(connectedToProductTNoCode);
        articleController.productController = productController;

        ArticleValidation articleValidation = new ArticleValidation();
        articleValidation.LOG = new NoLogger();
        articleValidation.validationMessageService = validationMessageService;
        articleValidation.articleController = articleController;
        articleValidation.productController = productController;
        Mockito.when(articleController.findByGtin(articleGtin)).thenReturn(null);
        articleController.articleValidation = articleValidation;

        ceController = mock(CeController.class);
        articleController.ceController = ceController;
        CVCEStandard standard = new CVCEStandard();
        standard.setUniqueId(articleCeStandardId);
        CVCEDirective directive = new CVCEDirective();
        directive.setUniqueId(articleCeDirectiveId);
        Mockito.when(ceController.findCEDirectiveById(articleCeDirectiveId)).thenReturn(directive);
        Mockito.when(ceController.findCEStandardById(articleCeStandardId)).thenReturn(standard);

        Article articleTypeI = createValidArticle(fitsToArticleIId, organizationId);
        articleTypeI.getCategory().setArticleType(Article.Type.I);
        Mockito.when(articleController.getArticle(organizationId, fitsToArticleIId, userAPI)).thenReturn(articleTypeI);
        Article articleTypeTj = createValidArticle(fitsToArticleTjId, organizationId);
        articleTypeTj.getCategory().setArticleType(Article.Type.Tj);
        Mockito.when(articleController.getArticle(organizationId, fitsToArticleTjId, userAPI)).thenReturn(articleTypeTj);
        Article articleTypeT = createValidArticle(fitsToArticleTId, organizationId);
        articleTypeT.getCategory().setArticleType(Article.Type.T);
        Mockito.when(articleController.getArticle(organizationId, fitsToArticleTId, userAPI)).thenReturn(articleTypeT);
        Article articleTypeR = createValidArticle(fitsToArticleRId, organizationId);
        articleTypeR.getCategory().setArticleType(Article.Type.R);
        Mockito.when(articleController.getArticle(organizationId, fitsToArticleRId, userAPI)).thenReturn(articleTypeR);
        Article articleTypeTNoCode = createValidArticle(fitsToArticleTNoCodeId, organizationId);
        articleTypeTNoCode.getCategory().setArticleType(Article.Type.T);
        articleTypeTNoCode.getCategory().setCode(null);
        Mockito.when(articleController.getArticle(organizationId, fitsToArticleTNoCodeId, userAPI)).thenReturn(articleTypeTNoCode);
        Article articleTypeH = createValidArticle(fitsToArticleHId, organizationId);
        articleTypeH.getCategory().setArticleType(Article.Type.H);
        Mockito.when(articleController.getArticle(organizationId, fitsToArticleHId, userAPI)).thenReturn(articleTypeH);

        unitController = mock(OrderUnitController.class);
        articleController.orderUnitController = unitController;
        CVOrderUnit unit = createValidUnit(articleOrderUnitId);
        Mockito.when(unitController.getUnit(articleOrderUnitId)).thenReturn(unit);
        orderUnitIdMap = new HashMap<>();
        orderUnitIdMap.put(unit.getUniqueId(), unit);
        Mockito.when(unitController.findAllAsIdMap()).thenReturn(orderUnitIdMap);

        packageUnitController = mock(PackageUnitController.class);
        articleController.packageUnitController = packageUnitController;
        CVPackageUnit packageUnit = createValidPackageUnit(articleOrderUnitId);
        Mockito.when(packageUnitController.getUnit(articleOrderUnitId)).thenReturn(packageUnit);
        packageUnitIdMap = new HashMap<>();
        packageUnitIdMap.put(unit.getUniqueId(), packageUnit);
        Mockito.when(packageUnitController.findAllAsIdMap()).thenReturn(packageUnitIdMap);

        agreementPricelistRowController = mock(AgreementPricelistRowController.class);
        articleController.agreementPricelistRowController = agreementPricelistRowController;
        generalPricelistPricelistRowController = mock(GeneralPricelistPricelistRowController.class);
        articleController.generalPricelistPricelistRowController = generalPricelistPricelistRowController;

        cVPreventiveMaintenanceController = mock(CVPreventiveMaintenanceController.class);
        Mockito.when(cVPreventiveMaintenanceController.findByCode(preventiveMaintenanceUnitCode)).thenReturn(PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenance(preventiveMaintenanceUnitCode, preventiveMaintenanceUnitName));
        Mockito.when(cVPreventiveMaintenanceController.findByCode(preventiveMaintenanceUnitCode + "2")).thenReturn(PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenance(preventiveMaintenanceUnitCode + "2", preventiveMaintenanceUnitName + "2"));
        articleController.cVPreventiveMaintenanceController = cVPreventiveMaintenanceController;

        userController = mock(UserController.class);
        articleController.userController = userController;

        elasticSearchController = mock(ElasticSearchController.class);
        articleController.elasticSearchController = elasticSearchController;
        sendMailRegardingDiscontinuedArticlesOnAssortment = mock(SendMailRegardingDiscontinuedArticlesOnAssortment.class);
        articleController.sendMailRegardingDiscontinuedArticlesOnAssortment = sendMailRegardingDiscontinuedArticlesOnAssortment;
    }

    @Test
    public void testGetArticleValidOrganization() {
        Article article = articleController.getArticle(organizationId, articleId, userAPI);
        Assert.assertNotNull(article);
    }

    @Test
    public void testGetArticleWrongUniqueId() {
        Article article = articleController.getArticle(organizationId, -1l, userAPI);
        Assert.assertNull(article);
    }

    @Test
    public void testGetArticleInvalidOrganization() {
        long invalidOrganizationUniqueId = 3l;
        Article validArticle = createValidArticle(articleId, organizationId);
        validArticle.getOrganization().setUniqueId(invalidOrganizationUniqueId);
        Mockito.when(myEntityManager.find(Article.class, articleId)).thenReturn(validArticle);
        Article article = articleController.getArticle(organizationId, articleId, userAPI);
        Assert.assertNull(article);
    }

    @Test
    public void testGetArticleAPI() {
        Article validArticle = createValidArticle(articleId, organizationId);
        validArticle.getCategory().setUniqueId(categoryId);
        List<Category> categorys = new ArrayList<>();
        Category category = CategoryControllerTest.createValidCategory(categoryId);
        categorys.add(category);
        validArticle.setExtendedCategories(categorys);
        Mockito.when(myEntityManager.find(Article.class, articleId)).thenReturn(validArticle);
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        ArticleAPI articleAPI = articleController.getArticleAPI(organizationId, articleId, userAPI, null, null);
        Assert.assertNotNull(articleAPI);
        Assert.assertEquals(articleId, articleAPI.getId().longValue());
        Assert.assertEquals(articleStatus.toString(), articleAPI.getStatus());
        Assert.assertEquals(organizationName, articleAPI.getOrganizationName());
        Assert.assertEquals(articleSupplementedInformation, articleAPI.getSupplementedInformation());

        // validate category
        Assert.assertNotNull(articleAPI.getCategory());
        Assert.assertEquals(CategoryControllerTest.PRODUCT_CATEGORY_ARTICLE_TYPE, articleAPI.getCategory().getArticleType());
        Assert.assertEquals(CategoryControllerTest.PRODUCT_CATEGORY_NAME, articleAPI.getCategory().getName());
        Assert.assertEquals(null, articleAPI.getCategory().getCode());

        // validate extended category
        Assert.assertNotNull(articleAPI.getExtendedCategories());
        Assert.assertEquals(1, articleAPI.getExtendedCategories().size());
        CategoryAPI extendedCategoryAPI = articleAPI.getExtendedCategories().get(0);
        Assert.assertNotNull(extendedCategoryAPI);
        Assert.assertEquals(CategoryControllerTest.PRODUCT_CATEGORY_ARTICLE_TYPE, extendedCategoryAPI.getArticleType());
        Assert.assertEquals(CategoryControllerTest.PRODUCT_CATEGORY_NAME, extendedCategoryAPI.getName());
        Assert.assertEquals(CategoryControllerTest.PRODUCT_CATEGORY_CODE, extendedCategoryAPI.getCode());

        // validate manufacturer
        Assert.assertEquals(articleTrademark, articleAPI.getTrademark());
        Assert.assertEquals(articleManufacturer, articleAPI.getManufacturer());
        Assert.assertEquals(articleManufacturerProductNumber, articleAPI.getManufacturerArticleNumber());
        Assert.assertEquals(articleManufacturerWeb ,articleAPI.getManufacturerElectronicAddress().getWeb());

        // validate ce
        Assert.assertTrue(articleAPI.isCeMarked());

        // validate ce directive
        Assert.assertNotNull(articleAPI.getCeDirective());
        Assert.assertEquals(articleCeDirectiveId, articleAPI.getCeDirective().getId());
        Assert.assertEquals(articleCeDirectiveName, articleAPI.getCeDirective().getName());

        // validate ce standard
        Assert.assertNotNull(articleAPI.getCeStandard());
        Assert.assertEquals(articleCeStandardId, articleAPI.getCeStandard().getId());
        Assert.assertEquals(articleCeStandardName, articleAPI.getCeStandard().getName());

        // validate preventive maintenance
        Assert.assertEquals(articlePreventiveMaintenanceDescription, articleAPI.getPreventiveMaintenanceDescription());
        Assert.assertEquals(articlePreventiveMaintenanceNumberOfDays, articleAPI.getPreventiveMaintenanceNumberOfDays().intValue());
        Assert.assertEquals(articlePreventiveMaintenanceValidFrom.getCode(), articleAPI.getPreventiveMaintenanceValidFrom().getCode());

        Assert.assertEquals(articleOrderUnitId, articleAPI.getOrderUnit().getId().longValue());
        Assert.assertEquals(articleOrderUnitCode, articleAPI.getOrderUnit().getCode());
        Assert.assertEquals(articleOrderUnitName, articleAPI.getOrderUnit().getName());
        Assert.assertEquals(articleArticleQuantityInOuterPackage, articleAPI.getArticleQuantityInOuterPackage());
        Assert.assertEquals(articleOrderUnitId, articleAPI.getArticleQuantityInOuterPackageUnit().getId().longValue());
        Assert.assertEquals(articleOrderUnitCode, articleAPI.getArticleQuantityInOuterPackageUnit().getCode());
        Assert.assertEquals(articleOrderUnitName, articleAPI.getArticleQuantityInOuterPackageUnit().getName());
        Assert.assertEquals(articlePackageContent, articleAPI.getPackageContent());
        Assert.assertEquals(articleOrderUnitId, articleAPI.getPackageContentUnit().getId().longValue());
        Assert.assertEquals(articleOrderUnitCode, articleAPI.getPackageContentUnit().getCode());
        Assert.assertEquals(articleOrderUnitName, articleAPI.getPackageContentUnit().getName());
        Assert.assertEquals(articlePackageLevelBase, articleAPI.getPackageLevelBase());
        Assert.assertEquals(articlePackageLevelMiddle, articleAPI.getPackageLevelMiddle());
        Assert.assertEquals(articlePackageLevelTop, articleAPI.getPackageLevelTop());
    }

    // CREATE ARTICLE
    @Test
    public void testCreateArticle() throws HjalpmedelstjanstenValidationException {
        ArticleAPI validArticleAPI = createValidArticleAPI();
        ArticleAPI articleAPI = articleController.createArticle(organizationId, validArticleAPI, userAPI, null, null, false);
        Assert.assertNotNull(articleAPI);
    }

    @Test
    public void testCreateArticleTrimArticleNumber() throws HjalpmedelstjanstenValidationException {
        ArticleAPI validArticleAPI = createValidArticleAPI();
        String validArticleNumber = validArticleAPI.getArticleNumber();
        validArticleAPI.setArticleNumber("   " + validArticleNumber);
        ArticleAPI articleAPI = articleController.createArticle(organizationId, validArticleAPI, userAPI, null, null, false);
        Assert.assertNotNull(articleAPI);
        Assert.assertEquals(validArticleNumber, articleAPI.getArticleNumber());
    }

    @Test
    public void testCreateInvalidArticleNoName() {
        // we only test one case here to make sure error is thrown, other bean
        // validation is made in the ArticleAPITest unit test
        ArticleAPI validArticleAPI = createValidArticleAPI();
        validArticleAPI.setArticleName(null);
        try {
            articleController.createArticle(organizationId, validArticleAPI, userAPI, null, null, false);
            Assert.fail("Create article successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("articleName", errorMessageAPI.getField());
        }
    }

    @Test
    public void testCreateInvalidOrganization() {
        ArticleAPI validArticleAPI = createValidArticleAPI();
        try {
            articleController.createArticle(-1l, validArticleAPI, userAPI, null, null, false);
            Assert.fail("Create article successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("organizationId", errorMessageAPI.getField());
        }
    }

    @Test
    public void testCreateInvalidCategory() {
        ArticleAPI validArticleAPI = createValidArticleAPI();
        validArticleAPI.getCategory().setId(-1l);
        try {
            articleController.createArticle(organizationId, validArticleAPI, userAPI, null, null, false);
            Assert.fail("Create article successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("category", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetExtendedCategoriesNoArticleCategoryCode() {
        ArticleAPI validArticleAPI = createValidArticleAPI();
        CategoryAPI extendedCategoryAPI = new CategoryAPI();
        extendedCategoryAPI.setId(categoryId);
        List<CategoryAPI> extendedCategoriesAPI = new ArrayList<>();
        extendedCategoriesAPI.add(extendedCategoryAPI);
        validArticleAPI.setExtendedCategories(extendedCategoriesAPI);

        Article validArticle = createValidArticle(articleId, organizationId);
        validArticle.getCategory().setCode(null);
        try {
            articleController.setExtendedCategories(validArticleAPI, validArticle);
            Assert.fail("Update article successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("extendedCategories", errorMessageAPI.getField());
        }
    }

    @Test
    public void testSetCategoriesInvalidExtendedCategory() {
        ArticleAPI validArticleAPI = createValidArticleAPI();
        CategoryAPI extendedCategoryAPI = new CategoryAPI();
        extendedCategoryAPI.setId(-1l);
        List<CategoryAPI> extendedCategoriesAPI = new ArrayList<>();
        extendedCategoriesAPI.add(extendedCategoryAPI);
        validArticleAPI.setExtendedCategories(extendedCategoriesAPI);

        Article validArticle = createValidArticle(articleId, organizationId);
        try {
            articleController.setExtendedCategories(validArticleAPI, validArticle);
            Assert.fail("Create article successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("extendedCategories", errorMessageAPI.getField());
        }
    }

    @Test
    public void testCreateInvalidDirective() {
        ArticleAPI validArticleAPI = createValidArticleAPI();
        validArticleAPI.getCeDirective().setId(-1l);
        try {
            articleController.createArticle(organizationId, validArticleAPI, userAPI, null, null, false);
            Assert.fail("Create article successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("ceDirective", errorMessageAPI.getField());
        }
    }

    @Test
    public void testCreateInvalidStandard() {
        ArticleAPI validArticleAPI = createValidArticleAPI();
        validArticleAPI.getCeStandard().setId(-1l);
        try {
            articleController.createArticle(organizationId, validArticleAPI, userAPI, null, null, false);
            Assert.fail("Create article successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("ceStandard", errorMessageAPI.getField());
        }
    }

    @Test
    public void testCreateInvalidManufacturerFieldSet() {
        ArticleAPI validArticleAPI = createValidArticleAPI();
        validArticleAPI.getManufacturerElectronicAddress().setMobile("1234556");
        try {
            articleController.createArticle(organizationId, validArticleAPI, userAPI, null, null, false);
            Assert.fail("Create article successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("web", errorMessageAPI.getField());
        }
    }

    @Test
    public void testUpdateInvalidManufacturerFieldSet() {
        ArticleAPI validArticleAPI = createValidArticleAPI();
        validArticleAPI.getManufacturerElectronicAddress().setMobile("1234556");
        try {
            articleController.updateArticle(organizationId, articleId, validArticleAPI, userAPI, null, null, false, false);
            Assert.fail("Update article successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("web", errorMessageAPI.getField());
        }
    }

    @Test
    public void testUpdateReplacementDateOnDiscontinuedArticle() throws HjalpmedelstjanstenValidationException {
        Article discontinuedArticle = createValidArticle(articleId, organizationId);
        discontinuedArticle.setStatus(Product.Status.DISCONTINUED);
        discontinuedArticle.setReplacementDate(new Date());
        Mockito.when(myEntityManager.find(Article.class, articleId)).thenReturn(discontinuedArticle);
        ArticleAPI validArticleAPI = createValidArticleAPI();
        validArticleAPI.setReplacementDate(System.currentTimeMillis() - 100000000);
        validArticleAPI.setInactivateRowsOnReplacement(Boolean.TRUE);
        try {
            articleController.updateArticle(organizationId, articleId, validArticleAPI, userAPI, null, null, false, false);
            Assert.fail("Update article successful but should fail");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("replacementDate", errorMessageAPI.getField());
        }
    }

    @Test
    public void shouldSetStatusToDiscontinuedIfReplacementDateIsBeforeToday() throws HjalpmedelstjanstenValidationException {
        Article article = createValidArticle(articleId, organizationId, Status.PUBLISHED);
        ArticleAPI validArticleAPI = createValidArticleAPI();

        LocalDate localDate = LocalDate.now().minusDays(1);
        LocalDateTime localDateTime = localDate.atTime(LocalTime.now());
        ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.systemDefault());
        Date date = Date.from(zonedDateTime.toInstant());

        validArticleAPI.setReplacementDate(date.getTime());
        articleController.handleReplaceArticle(article, validArticleAPI, organizationId, userAPI);

        assertEquals(Status.DISCONTINUED, article.getStatus());
    }

    @Test
    public void shouldNotSetStatusToDiscontinuedIfReplacementDateIsInTheFuture() throws HjalpmedelstjanstenValidationException {
        Article article = createValidArticle(articleId, organizationId, Status.PUBLISHED);
        ArticleAPI validArticleAPI = createValidArticleAPI();

        LocalDate localDate = LocalDate.now().plusDays(5);
        LocalDateTime localDateTime = localDate.atTime(LocalTime.now());
        ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.systemDefault());
        Date date = Date.from(zonedDateTime.toInstant());

        validArticleAPI.setReplacementDate(date.getTime());
        articleController.handleReplaceArticle(article, validArticleAPI, organizationId, userAPI);

        assertEquals(Status.PUBLISHED, article.getStatus());
    }

    @Test
    public void shouldSetStatusToDiscontinuedIfReplacementDateIsToday() throws HjalpmedelstjanstenValidationException {
        Article article = createValidArticle(articleId, organizationId, Status.PUBLISHED);
        ArticleAPI validArticleAPI = createValidArticleAPI();

        LocalDate localDate = LocalDate.now();
        LocalDateTime localDateTime = localDate.atTime(LocalTime.now());
        ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.systemDefault());
        Date date = Date.from(zonedDateTime.toInstant());

        validArticleAPI.setReplacementDate(date.getTime());
        articleController.handleReplaceArticle(article, validArticleAPI, organizationId, userAPI);

        assertEquals(Status.DISCONTINUED, article.getStatus());
    }

    @Test
    public void shouldSendEmailIfReplacementDateIsToday() throws HjalpmedelstjanstenValidationException {
        final var expectedArticleIds = List.of(articleId);

        final var article = createValidArticle(articleId, organizationId, Status.PUBLISHED);
        final var validArticleAPI = createValidArticleAPI();

        final var localDate = LocalDate.now();
        final var localDateTime = localDate.atTime(LocalTime.now());
        final var zonedDateTime = localDateTime.atZone(ZoneId.systemDefault());
        final var date = Date.from(zonedDateTime.toInstant());

        validArticleAPI.setReplacementDate(date.getTime());
        articleController.handleReplaceArticle(article, validArticleAPI, organizationId, userAPI);

        final ArgumentCaptor<List<Long>> articleIdsArgumentCaptor = ArgumentCaptor.forClass(List.class);

        verify(sendMailRegardingDiscontinuedArticlesOnAssortment).send(articleIdsArgumentCaptor.capture());

        assertEquals(expectedArticleIds, articleIdsArgumentCaptor.getValue());
    }

    @Test
    public void testDoubleValuesOverriddenTrue() {
        Double d1 = 1.0;
        Double d2 = 2.0;
        Assert.assertTrue(articleController.doubleValuesOverridden(d1, d2));
    }

    @Test
    public void testDoubleValuesOverriddenFalse() {
        Double d1 = 1.0;
        Double d2 = 1.0;
        Assert.assertFalse(articleController.doubleValuesOverridden(d1, d2));
    }

    @Test
    public void testDoubleValuesOverriddenTrueFirstNull() {
        Double d1 = null;
        Double d2 = 2.0;
        Assert.assertTrue(articleController.doubleValuesOverridden(d1, d2));
    }

    @Test
    public void testDoubleValuesOverriddenTrueSecondNull() {
        Double d1 = 1.0;
        Double d2 = null;
        Assert.assertTrue(articleController.doubleValuesOverridden(d1, d2));
    }

    @Test
    public void testDoubleValuesOverriddenBothNull() {
        Double d1 = null;
        Double d2 = null;
        Assert.assertFalse(articleController.doubleValuesOverridden(d1, d2));
    }

    @Test
    public void testIntegerValuesOverriddenTrue() {
        Integer i1 = 1;
        Integer i2 = 2;
        Assert.assertTrue(articleController.integerValuesOverridden(i1, i2));
    }

    @Test
    public void testIntegerValuesOverriddenFalse() {
        Integer i1 = 1;
        Integer i2 = 1;
        Assert.assertFalse(articleController.integerValuesOverridden(i1, i2));
    }

    @Test
    public void testIntegerValuesOverriddenTrueFirstNull() {
        Integer i1 = null;
        Integer i2 = 2;
        Assert.assertTrue(articleController.integerValuesOverridden(i1, i2));
    }

    @Test
    public void testIntegerValuesOverriddenTrueSecondNull() {
        Integer i1 = 1;
        Integer i2 = null;
        Assert.assertTrue(articleController.integerValuesOverridden(i1, i2));
    }

    @Test
    public void testIntegerValuesOverriddenBothNull() {
        Integer i1 = null;
        Integer i2 = null;
        Assert.assertFalse(articleController.integerValuesOverridden(i1, i2));
    }

    @Test
    public void testIsOrderInformationFieldsOverriddenFalse() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = new Article();
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        Assert.assertFalse(articleController.isOrderInformationFieldsOverridden(articleAPI, article));
    }

    @Test
    public void testIsOrderInformationFieldsOverriddenTrueOrderUnit() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = new Article();
        Product basedOnProduct = createBasedOnProductSameValues();
        basedOnProduct.getOrderUnit().setUniqueId(0l);
        article.setBasedOnProduct(basedOnProduct);
        Assert.assertTrue(articleController.isOrderInformationFieldsOverridden(articleAPI, article));
    }

    @Test
    public void testIsOrderInformationFieldsOverriddenTrueArticleQuantityInOuterPackage() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = new Article();
        Product basedOnProduct = createBasedOnProductSameValues();
        basedOnProduct.setArticleQuantityInOuterPackage(0.0);
        article.setBasedOnProduct(basedOnProduct);
        Assert.assertTrue(articleController.isOrderInformationFieldsOverridden(articleAPI, article));
    }

    @Test
    public void testIsOrderInformationFieldsOverriddenTrueArticleQuantityInOuterPackageUnit() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = new Article();
        Product basedOnProduct = createBasedOnProductSameValues();
        basedOnProduct.getArticleQuantityInOuterPackageUnit().setUniqueId(0l);
        article.setBasedOnProduct(basedOnProduct);
        Assert.assertTrue(articleController.isOrderInformationFieldsOverridden(articleAPI, article));
    }

    @Test
    public void testIsOrderInformationFieldsOverriddenTruePackageContent() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = new Article();
        Product basedOnProduct = createBasedOnProductSameValues();
        basedOnProduct.setPackageContent(0.0);
        article.setBasedOnProduct(basedOnProduct);
        Assert.assertTrue(articleController.isOrderInformationFieldsOverridden(articleAPI, article));
    }

    @Test
    public void testIsOrderInformationFieldsOverriddenTruePackageContentUnit() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = new Article();
        Product basedOnProduct = createBasedOnProductSameValues();
        basedOnProduct.getPackageContentUnit().setUniqueId(0l);
        article.setBasedOnProduct(basedOnProduct);
        Assert.assertTrue(articleController.isOrderInformationFieldsOverridden(articleAPI, article));
    }

    @Test
    public void testIsOrderInformationFieldsOverriddenTruePackageLevelBase() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = new Article();
        Product basedOnProduct = createBasedOnProductSameValues();
        basedOnProduct.setPackageLevelBase(0);
        article.setBasedOnProduct(basedOnProduct);
        Assert.assertTrue(articleController.isOrderInformationFieldsOverridden(articleAPI, article));
    }

    @Test
    public void testIsOrderInformationFieldsOverriddenTruePackageLevelMiddle() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = new Article();
        Product basedOnProduct = createBasedOnProductSameValues();
        basedOnProduct.setPackageLevelMiddle(0);
        article.setBasedOnProduct(basedOnProduct);
        Assert.assertTrue(articleController.isOrderInformationFieldsOverridden(articleAPI, article));
    }

    @Test
    public void testIsOrderInformationFieldsOverriddenTruePackageLevelTop() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = new Article();
        Product basedOnProduct = createBasedOnProductSameValues();
        basedOnProduct.setPackageLevelTop(0);
        article.setBasedOnProduct(basedOnProduct);
        Assert.assertTrue(articleController.isOrderInformationFieldsOverridden(articleAPI, article));
    }

    @Test
    public void testIsUnitInfoOverriddenFalse() {
        CVOrderUnit unit = createValidUnit(articleOrderUnitId);
        CVOrderUnitAPI unitAPI = createValidUnitAPI(articleOrderUnitId);
        Assert.assertFalse(articleController.isUnitInfoOverridden(unit, unitAPI));
    }

    @Test
    public void testIsUnitInfoOverriddenTrueUnitNull() {
        CVOrderUnit unit = null;
        CVOrderUnitAPI unitAPI = createValidUnitAPI(articleOrderUnitId);
        Assert.assertTrue(articleController.isUnitInfoOverridden(unit, unitAPI));
    }

    @Test
    public void testIsUnitInfoOverriddenTrueUnitAPINull() {
        CVOrderUnit unit = createValidUnit(articleOrderUnitId);
        CVOrderUnitAPI unitAPI = null;
        Assert.assertTrue(articleController.isUnitInfoOverridden(unit, unitAPI));
    }

    @Test
    public void testIsUnitInfoOverriddenTrueDifferentId() {
        CVOrderUnit unit = createValidUnit(articleOrderUnitId);
        unit.setUniqueId(0l);
        CVOrderUnitAPI unitAPI = createValidUnitAPI(articleOrderUnitId);
        Assert.assertTrue(articleController.isUnitInfoOverridden(unit, unitAPI));
    }

    @Test
    public void testCheckSetOrderInformationSame() throws HjalpmedelstjanstenValidationException {
        Product basedOnProduct = createBasedOnProductSameValues();
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        article.setBasedOnProduct(basedOnProduct);
        articleController.checkSetOrderInformation(articleAPI, article, orderUnitIdMap);
        Assert.assertFalse(article.isOrderInformationOverridden());
    }

    @Test
    public void testCheckSetOrderInformationModifiedOrderUnit() throws HjalpmedelstjanstenValidationException {
        Product basedOnProduct = createBasedOnProductSameValues();
        CVOrderUnit unit = createValidUnit(articleOrderUnitId);
        unit.setUniqueId(articleOrderUnitId+1);
        basedOnProduct.setOrderUnit(unit);
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        article.setBasedOnProduct(basedOnProduct);
        articleController.checkSetOrderInformation(articleAPI, article, orderUnitIdMap);
        Assert.assertTrue(article.isOrderInformationOverridden());
    }

    @Test
    public void testCheckSetOrderInformationModifiedArticleQuantityInOuterPackageUnit() throws HjalpmedelstjanstenValidationException {
        Product basedOnProduct = createBasedOnProductSameValues();
        CVOrderUnit unit = createValidUnit(articleOrderUnitId);
        unit.setUniqueId(articleOrderUnitId+1);
        basedOnProduct.setArticleQuantityInOuterPackageUnit(unit);
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        article.setBasedOnProduct(basedOnProduct);
        articleController.checkSetOrderInformation(articleAPI, article, orderUnitIdMap);
        Assert.assertTrue(article.isOrderInformationOverridden());
    }

    @Test
    public void testCheckSetOrderInformationModifiedPackageContentUnit() throws HjalpmedelstjanstenValidationException {
        Product basedOnProduct = createBasedOnProductSameValues();
        CVPackageUnit unit = createValidPackageUnit(articleOrderUnitId);
        unit.setUniqueId(articleOrderUnitId+1);
        basedOnProduct.setPackageContentUnit(unit);
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        article.setBasedOnProduct(basedOnProduct);
        articleController.checkSetOrderInformation(articleAPI, article, orderUnitIdMap);
        Assert.assertTrue(article.isOrderInformationOverridden());
    }

    @Test
    public void testCheckSetOrderInformationModifiedArticleQuantityInOuterPackage() throws HjalpmedelstjanstenValidationException {
        Product basedOnProduct = createBasedOnProductSameValues();
        basedOnProduct.setArticleQuantityInOuterPackage(articleArticleQuantityInOuterPackage+1);
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        article.setBasedOnProduct(basedOnProduct);
        articleController.checkSetOrderInformation(articleAPI, article, orderUnitIdMap);
        Assert.assertTrue(article.isOrderInformationOverridden());
    }

    @Test
    public void testCheckSetOrderInformationModifiedPackageContent() throws HjalpmedelstjanstenValidationException {
        Product basedOnProduct = createBasedOnProductSameValues();
        basedOnProduct.setPackageContent(articlePackageContent+1);
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        article.setBasedOnProduct(basedOnProduct);
        articleController.checkSetOrderInformation(articleAPI, article, orderUnitIdMap);
        Assert.assertTrue(article.isOrderInformationOverridden());
    }

    @Test
    public void testCheckSetOrderInformationModifiedPackageLevelBase() throws HjalpmedelstjanstenValidationException {
        Product basedOnProduct = createBasedOnProductSameValues();
        basedOnProduct.setPackageLevelBase(articlePackageLevelBase+1);
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        article.setBasedOnProduct(basedOnProduct);
        articleController.checkSetOrderInformation(articleAPI, article, orderUnitIdMap);
        Assert.assertTrue(article.isOrderInformationOverridden());
    }

    @Test
    public void testCheckSetOrderInformationModifiedPackageLevelMiddle() throws HjalpmedelstjanstenValidationException {
        Product basedOnProduct = createBasedOnProductSameValues();
        basedOnProduct.setPackageLevelMiddle(articlePackageLevelMiddle+1);
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        article.setBasedOnProduct(basedOnProduct);
        articleController.checkSetOrderInformation(articleAPI, article, orderUnitIdMap);
        Assert.assertTrue(article.isOrderInformationOverridden());
    }

    @Test
    public void testCheckSetOrderInformationModifiedPackageLevelTop() throws HjalpmedelstjanstenValidationException {
        Product basedOnProduct = createBasedOnProductSameValues();
        basedOnProduct.setPackageLevelTop(articlePackageLevelTop+1);
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        article.setBasedOnProduct(basedOnProduct);
        articleController.checkSetOrderInformation(articleAPI, article, orderUnitIdMap);
        Assert.assertTrue(article.isOrderInformationOverridden());
    }

    @Test
    public void testSetCustomerUniqueNotModified() {
        Product basedOnProduct = createBasedOnProductSameValues();
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        article.setBasedOnProduct(basedOnProduct);
        try {
            articleController.setCustomerUnique(articleAPI, article);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown but should not have been");
        }
    }

    @Test
    public void testSetCustomerUniqueModified() {
        Product basedOnProduct = createBasedOnProductSameValues();
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setCustomerUnique(true);
        Article article = createValidArticle(articleId, organizationId);
        article.setBasedOnProduct(basedOnProduct);
        try {
            articleController.setCustomerUnique(articleAPI, article);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Exception thrown but should not have been");
        }
    }

    @Test
    public void testIsEmptyListNull() {
        List<Object> list = null;
        Assert.assertTrue(articleController.isEmptyList(list));
    }

    @Test
    public void testIsEmptyListEmpty() {
        List<Object> list = new ArrayList<>();
        Assert.assertTrue(articleController.isEmptyList(list));
    }

    @Test
    public void testIsEmptyListNotEmpty() {
        List<Object> list = new ArrayList<>();
        list.add(new Object());
        Assert.assertFalse(articleController.isEmptyList(list));
    }

    @Test
    public void testSetSupplementalInformationNotOverridden() throws HjalpmedelstjanstenValidationException {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setSupplementedInformation("Product info");
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        basedOnProduct.setSupplementedInformation("Product info");
        articleController.setSupplementedInformation(articleAPI, article);

        Assert.assertFalse(article.isSupplementedInformationOverridden());
    }

    @Test
    public void testSetSupplementalInformationOverridden() throws HjalpmedelstjanstenValidationException {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setSupplementedInformation(articleSupplementedInformation);
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        basedOnProduct.setSupplementedInformation("Product info");
        articleController.setSupplementedInformation(articleAPI, article);

        Assert.assertTrue(article.isSupplementedInformationOverridden());
        Assert.assertEquals(articleSupplementedInformation, article.getSupplementedInformation());
    }

    @Test
    public void testSetColorNotOverridden() throws HjalpmedelstjanstenValidationException {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setColor("color");
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        basedOnProduct.setColor("color");
        articleController.setSupplementedInformation(articleAPI, article);

        Assert.assertFalse(article.isColorOverridden());
    }

    @Test
    public void testSetColorOverridden() throws HjalpmedelstjanstenValidationException {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setColor(articleColor);
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        basedOnProduct.setColor("Product color");
        articleController.setSupplementedInformation(articleAPI, article);

        Assert.assertTrue(article.isColorOverridden());
        Assert.assertEquals(articleColor, article.getColor());
    }

    @Test
    public void testSetSupplementalInformationWithEmptyString() throws HjalpmedelstjanstenValidationException {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setSupplementedInformation("");
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        basedOnProduct.setSupplementedInformation("Product info");
        article.setBasedOnProduct(basedOnProduct);
        articleController.setSupplementedInformation(articleAPI, article);

        Assert.assertTrue(article.isSupplementedInformationOverridden());
        Assert.assertEquals("", article.getSupplementedInformation());
    }

    @Test
    public void testSetCeNotOverridden() throws HjalpmedelstjanstenValidationException {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        articleController.setCE(articleAPI, article);
        Assert.assertFalse(article.isCeMarkedOverridden());
        Assert.assertFalse(article.isCeDirectiveOverridden());
        Assert.assertFalse(article.isCeStandardOverridden());
    }

    @Test
    public void testSetCeDirectiveAllOverridden() throws HjalpmedelstjanstenValidationException {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setCeMarked(!articleCeMarked);
        articleAPI.setCeDirective(null);
        articleAPI.setCeStandard(null);
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        articleController.setCE(articleAPI, article);
        Assert.assertTrue(article.isCeMarkedOverridden());
        Assert.assertTrue(article.isCeDirectiveOverridden());
        Assert.assertTrue(article.isCeStandardOverridden());
    }

    @Test
    public void testSetCeDirectiveDirectiveOverridden() throws HjalpmedelstjanstenValidationException {
        CVCEDirective directive = new CVCEDirective();
        long otherArticleCeDirectiveId = articleCeDirectiveId + 1;
        directive.setUniqueId(otherArticleCeDirectiveId);

        ArticleAPI articleAPI = createValidArticleAPI();
        CVCEDirectiveAPI cEDirectiveAPI = createValidCEDirectiveAPI();
        cEDirectiveAPI.setId(otherArticleCeDirectiveId);
        articleAPI.setCeDirective(cEDirectiveAPI);
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);

        Mockito.when(ceController.findCEDirectiveById(otherArticleCeDirectiveId)).thenReturn(directive);
        articleController.setCE(articleAPI, article);
        Assert.assertFalse(article.isCeMarkedOverridden());
        Assert.assertTrue(article.isCeDirectiveOverridden());
        Assert.assertFalse(article.isCeStandardOverridden());
    }

    @Test
    public void testSetCeDirectiveStandardOverridden() throws HjalpmedelstjanstenValidationException {
        CVCEStandard standard = new CVCEStandard();
        long otherArticleCeStandardId = articleCeStandardId + 1;
        standard.setUniqueId(otherArticleCeStandardId);

        ArticleAPI articleAPI = createValidArticleAPI();
        CVCEStandardAPI cEStandardAPI = createValidCEStandardAPI();
        cEStandardAPI.setId(otherArticleCeStandardId);
        articleAPI.setCeStandard(cEStandardAPI);
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);

        Mockito.when(ceController.findCEStandardById(otherArticleCeStandardId)).thenReturn(standard);
        articleController.setCE(articleAPI, article);
        Assert.assertFalse(article.isCeMarkedOverridden());
        Assert.assertFalse(article.isCeDirectiveOverridden());
        Assert.assertTrue(article.isCeStandardOverridden());
    }

    @Test
    public void testSetManufacturerNotOverridden() {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        articleController.setManufacturer(articleAPI, article);
        Assert.assertFalse(article.isManufacturerArticleNumberOverridden());
        Assert.assertFalse(article.isManufacturerElectronicAddressOverridden());
        Assert.assertFalse(article.isManufacturerOverridden());
        Assert.assertFalse(article.isTrademarkOverridden());
    }

    @Test
    public void testSetManufacturerManufacturerArticleNumberOverridden() {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setManufacturerArticleNumber(articleManufacturerProductNumber + "2");
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        articleController.setManufacturer(articleAPI, article);
        Assert.assertTrue(article.isManufacturerArticleNumberOverridden());
        Assert.assertFalse(article.isManufacturerElectronicAddressOverridden());
        Assert.assertFalse(article.isManufacturerOverridden());
        Assert.assertFalse(article.isTrademarkOverridden());
    }

    @Test
    public void testSetManufacturerManufacturerElectronicAddressOverridden() {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.getManufacturerElectronicAddress().setWeb(articleManufacturerWeb + "2");
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        articleController.setManufacturer(articleAPI, article);
        Assert.assertFalse(article.isManufacturerArticleNumberOverridden());
        Assert.assertTrue(article.isManufacturerElectronicAddressOverridden());
        Assert.assertFalse(article.isManufacturerOverridden());
        Assert.assertFalse(article.isTrademarkOverridden());
    }

    @Test
    public void testSetManufacturerManufacturerOverridden() {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setManufacturer(articleManufacturer + "2");
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        articleController.setManufacturer(articleAPI, article);
        Assert.assertFalse(article.isManufacturerArticleNumberOverridden());
        Assert.assertFalse(article.isManufacturerElectronicAddressOverridden());
        Assert.assertTrue(article.isManufacturerOverridden());
        Assert.assertFalse(article.isTrademarkOverridden());
    }

    @Test
    public void testSetManufacturerTrademarkOverridden() {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setTrademark(articleTrademark + "2");
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        articleController.setManufacturer(articleAPI, article);
        Assert.assertFalse(article.isManufacturerArticleNumberOverridden());
        Assert.assertFalse(article.isManufacturerElectronicAddressOverridden());
        Assert.assertFalse(article.isManufacturerOverridden());
        Assert.assertTrue(article.isTrademarkOverridden());
    }

    @Test
    public void testSetManufacturerAllOverridden() {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setManufacturerArticleNumber(articleManufacturerProductNumber + "2");
        articleAPI.getManufacturerElectronicAddress().setWeb(articleManufacturerWeb + "2");
        articleAPI.setManufacturer(articleManufacturer + "2");
        articleAPI.setTrademark(articleTrademark + "2");
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        articleController.setManufacturer(articleAPI, article);
        Assert.assertTrue(article.isManufacturerArticleNumberOverridden());
        Assert.assertTrue(article.isManufacturerElectronicAddressOverridden());
        Assert.assertTrue(article.isManufacturerOverridden());
        Assert.assertTrue(article.isTrademarkOverridden());
    }

    @Test
    public void testSetPreventiveMaintenanceNotOverridden() throws HjalpmedelstjanstenValidationException {
        ArticleAPI articleAPI = createValidArticleAPI();
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        articleController.setPreventiveMaintenance(articleAPI, article, userAPI);
        Assert.assertFalse(article.isPreventiveMaintenanceDescriptionOverridden());
        Assert.assertFalse(article.isPreventiveMaintenanceNumberOfDaysOverridden());
        Assert.assertFalse(article.isPreventiveMaintenanceValidFromOverridden());
    }

    @Test
    public void testSetPreventiveMaintenanceDescriptionOverridden() throws HjalpmedelstjanstenValidationException {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setPreventiveMaintenanceDescription(articlePreventiveMaintenanceDescription + "2");
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        articleController.setPreventiveMaintenance(articleAPI, article, userAPI);
        Assert.assertTrue(article.isPreventiveMaintenanceDescriptionOverridden());
        Assert.assertFalse(article.isPreventiveMaintenanceNumberOfDaysOverridden());
        Assert.assertFalse(article.isPreventiveMaintenanceValidFromOverridden());
    }

    @Test
    public void testSetPreventiveMaintenanceNumberOfDaysOverridden() throws HjalpmedelstjanstenValidationException {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setPreventiveMaintenanceNumberOfDays(articlePreventiveMaintenanceNumberOfDays + 1);
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        articleController.setPreventiveMaintenance(articleAPI, article, userAPI);
        Assert.assertFalse(article.isPreventiveMaintenanceDescriptionOverridden());
        Assert.assertTrue(article.isPreventiveMaintenanceNumberOfDaysOverridden());
        Assert.assertFalse(article.isPreventiveMaintenanceValidFromOverridden());
    }

    @Test
    public void testSetPreventiveMaintenanceValidFromOverridden() throws HjalpmedelstjanstenValidationException {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setPreventiveMaintenanceValidFrom(PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenanceAPI(preventiveMaintenanceUnitCode + "2", preventiveMaintenanceUnitName + "2"));
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        articleController.setPreventiveMaintenance(articleAPI, article, userAPI);
        Assert.assertFalse(article.isPreventiveMaintenanceDescriptionOverridden());
        Assert.assertFalse(article.isPreventiveMaintenanceNumberOfDaysOverridden());
        Assert.assertTrue(article.isPreventiveMaintenanceValidFromOverridden());
    }

    @Test
    public void testSetPreventiveMaintenanceAllOverridden() throws HjalpmedelstjanstenValidationException {
        ArticleAPI articleAPI = createValidArticleAPI();
        articleAPI.setPreventiveMaintenanceDescription(articlePreventiveMaintenanceDescription + "2");
        articleAPI.setPreventiveMaintenanceNumberOfDays(articlePreventiveMaintenanceNumberOfDays + 1);
        articleAPI.setPreventiveMaintenanceValidFrom(PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenanceAPI(preventiveMaintenanceUnitCode + "2", preventiveMaintenanceUnitName + "2"));
        Article article = createValidArticle(articleId, organizationId);
        Product basedOnProduct = createBasedOnProductSameValues();
        article.setBasedOnProduct(basedOnProduct);
        UserAPI userAPI = UserAPITest.createValidUserAPI(userId, organizationId, null, UserRole.RoleName.Supplieradmin, null);
        articleController.setPreventiveMaintenance(articleAPI, article, userAPI);
        Assert.assertTrue(article.isPreventiveMaintenanceDescriptionOverridden());
        Assert.assertTrue(article.isPreventiveMaintenanceNumberOfDaysOverridden());
        Assert.assertTrue(article.isPreventiveMaintenanceValidFromOverridden());
    }

    private static Product createBasedOnProductSameValues() {
        Product basedOnProduct = new Product();
        CVPackageUnit packageUnit = createValidPackageUnit(articleOrderUnitId);
        CVOrderUnit unit = createValidUnit(articleOrderUnitId);
        basedOnProduct.setOrderUnit(unit);
        basedOnProduct.setArticleQuantityInOuterPackage(articleArticleQuantityInOuterPackage);
        basedOnProduct.setArticleQuantityInOuterPackageUnit(unit);
        basedOnProduct.setPackageContent(articlePackageContent);
        basedOnProduct.setPackageContentUnit(packageUnit);
        basedOnProduct.setPackageLevelBase(articlePackageLevelBase);
        basedOnProduct.setPackageLevelMiddle(articlePackageLevelMiddle);
        basedOnProduct.setPackageLevelTop(articlePackageLevelTop);

        basedOnProduct.setCustomerUnique(articleCustomerUnique);

        // create category
        basedOnProduct.setCategory(CategoryControllerTest.createValidCategory(null));

        // set ce
        basedOnProduct.setCeMarked(articleCeMarked);

        // create ce standard
        CVCEStandard cEStandard = new CVCEStandard();
        cEStandard.setUniqueId(articleCeStandardId);
        cEStandard.setName(articleCeStandardName);
        basedOnProduct.setCeStandard(cEStandard);

        // create ce directive
        CVCEDirective cEDirective = new CVCEDirective();
        cEDirective.setUniqueId(articleCeDirectiveId);
        cEDirective.setName(articleCeDirectiveName);
        basedOnProduct.setCeDirective(cEDirective);

        // set manufacturer
        basedOnProduct.setTrademark(articleTrademark);
        basedOnProduct.setManufacturer(articleManufacturer);
        basedOnProduct.setManufacturerProductNumber(articleManufacturerProductNumber);
        ElectronicAddress manufacturerElectronicAddress = new ElectronicAddress();
        manufacturerElectronicAddress.setWeb(articleManufacturerWeb);
        basedOnProduct.setManufacturerElectronicAddress(manufacturerElectronicAddress);

        // preventive maintenance
        basedOnProduct.setPreventiveMaintenanceDescription(articlePreventiveMaintenanceDescription);
        basedOnProduct.setPreventiveMaintenanceNumberOfDays(articlePreventiveMaintenanceNumberOfDays);
        basedOnProduct.setPreventiveMaintenanceValidFrom(articlePreventiveMaintenanceValidFrom);

        // supplemental information
        basedOnProduct.setSupplementedInformation(articleSupplementedInformation);

        basedOnProduct.setOrganization(OrganizationControllerTest.createValidOrganization(organizationId, null, null, null, null));

        return basedOnProduct;
    }

    public static Article createValidArticle(long articleId, long organizationId) {
        return createValidArticle(articleId, organizationId, Product.Status.PUBLISHED);
    }

    public static Article createValidArticle(long articleId, long organizationId, Product.Status status) {
        Article article = new Article();
        article.setArticleNumber(articleArticleNumber);
        article.setUniqueId(articleId);
        article.setArticleName(articleName);
        article.setStatus(status);
        article.setSupplementedInformation(articleSupplementedInformation);
        article.setGtin(articleGtin);
        article.setCustomerUnique(false);

        // set manufacturer
        article.setTrademark(articleTrademark);
        article.setManufacturer(articleManufacturer);
        article.setManufacturerArticleNumber(articleManufacturerProductNumber);
        ElectronicAddress manufacturerElectronicAddress = new ElectronicAddress();
        manufacturerElectronicAddress.setWeb(articleManufacturerWeb);
        article.setManufacturerElectronicAddress(manufacturerElectronicAddress);

        // preventive maintenance
        article.setPreventiveMaintenanceDescription(articlePreventiveMaintenanceDescription);
        article.setPreventiveMaintenanceNumberOfDays(articlePreventiveMaintenanceNumberOfDays);
        article.setPreventiveMaintenanceValidFrom(articlePreventiveMaintenanceValidFrom);

        // create category
        article.setCategory(CategoryControllerTest.createValidCategory(categoryId, Article.Type.T, null));

        article.setOrganization(OrganizationControllerTest.createValidOrganization(organizationId, Organization.OrganizationType.SUPPLIER, new Date(), null, organizationName));

        // set ce
        article.setCeMarked(articleCeMarked);

        // create ce standard
        CVCEStandard cEStandard = new CVCEStandard();
        cEStandard.setUniqueId(articleCeStandardId);
        cEStandard.setName(articleCeStandardName);
        article.setCeStandard(cEStandard);

        // create ce directive
        CVCEDirective cEDirective = new CVCEDirective();
        cEDirective.setUniqueId(articleCeDirectiveId);
        cEDirective.setName(articleCeDirectiveName);
        article.setCeDirective(cEDirective);

        CVOrderUnit unit = createValidUnit(articleOrderUnitId);
        CVPackageUnit packgeUnit = createValidPackageUnit(articleOrderUnitId);
        article.setOrderUnit(unit);
        article.setArticleQuantityInOuterPackage(articleArticleQuantityInOuterPackage);
        article.setArticleQuantityInOuterPackageUnit(unit);
        article.setPackageContent(articlePackageContent);
        article.setPackageContentUnit(packgeUnit);
        article.setPackageLevelBase(articlePackageLevelBase);
        article.setPackageLevelMiddle(articlePackageLevelMiddle);
        article.setPackageLevelTop(articlePackageLevelTop);

        return article;
    }

    public static CVOrderUnit createValidUnit( long id ) {
        CVOrderUnit unit = new CVOrderUnit();
        unit.setUniqueId(id);
        unit.setCode(articleOrderUnitCode);
        unit.setName(articleOrderUnitName);
        return unit;
    }

    public static CVPackageUnit createValidPackageUnit( long id ) {
        CVPackageUnit unit = new CVPackageUnit();
        unit.setUniqueId(id);
        unit.setCode(articleOrderUnitCode);
        unit.setName(articleOrderUnitName);
        return unit;
    }

    public static ArticleAPI createValidArticleAPI() {
        ArticleAPI articleAPI = new ArticleAPI();
        articleAPI.setId(articleId);
        articleAPI.setArticleName("test");
        articleAPI.setArticleNumber(articleArticleNumber);
        articleAPI.setCustomerUnique(articleCustomerUnique);
        articleAPI.setStatus(Product.Status.PUBLISHED.toString());
        articleAPI.setCategory(ProductAPITest.createValidCategoryAPI());

        // preventive maintenance
        articleAPI.setPreventiveMaintenanceDescription(articlePreventiveMaintenanceDescription);
        articleAPI.setPreventiveMaintenanceNumberOfDays(articlePreventiveMaintenanceNumberOfDays);
        articleAPI.setPreventiveMaintenanceValidFrom(PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenanceAPI(preventiveMaintenanceUnitCode, preventiveMaintenanceUnitName));

        // set manufacturer
        articleAPI.setTrademark(articleTrademark);
        articleAPI.setSupplementedInformation(articleSupplementedInformation);
        articleAPI.setManufacturer(articleManufacturer);
        articleAPI.setManufacturerArticleNumber(articleManufacturerProductNumber);
        ElectronicAddressAPI manufacturerElectronicAddress = new ElectronicAddressAPI();
        manufacturerElectronicAddress.setWeb(articleManufacturerWeb);
        articleAPI.setManufacturerElectronicAddress(manufacturerElectronicAddress);

        CategoryAPI categoryAPI = new CategoryAPI();
        categoryAPI.setId(categoryId);
        categoryAPI.setArticleType(Article.Type.T);
        articleAPI.setCategory(categoryAPI);

        articleAPI.setCeMarked(articleCeMarked);
        articleAPI.setCeDirective(createValidCEDirectiveAPI());
        articleAPI.setCeStandard(createValidCEStandardAPI());

        List<ProductAPI> fitsToProductAPIs = new ArrayList<>();
        ProductAPI productAPI = new ProductAPI();
        productAPI.setId(fitsToProductId);
        fitsToProductAPIs.add(productAPI);
        articleAPI.setFitsToProducts(fitsToProductAPIs);

        CVOrderUnitAPI unitAPI = createValidUnitAPI(articleOrderUnitId);
        CVPackageUnitAPI packageUnitAPI = createValidPackageUnitAPI(articleOrderUnitId);
        articleAPI.setOrderUnit(unitAPI);
        articleAPI.setArticleQuantityInOuterPackage(articleArticleQuantityInOuterPackage);
        articleAPI.setArticleQuantityInOuterPackageUnit(unitAPI);
        articleAPI.setPackageContent(articlePackageContent);
        articleAPI.setPackageContentUnit(packageUnitAPI);
        articleAPI.setPackageLevelBase(articlePackageLevelBase);
        articleAPI.setPackageLevelMiddle(articlePackageLevelMiddle);
        articleAPI.setPackageLevelTop(articlePackageLevelTop);

        return articleAPI;
    }

    private static CVCEDirectiveAPI createValidCEDirectiveAPI() {
        CVCEDirectiveAPI directiveAPI = new CVCEDirectiveAPI();
        directiveAPI.setId(articleCeDirectiveId);
        return directiveAPI;
    }

    private static CVCEStandardAPI createValidCEStandardAPI() {
        CVCEStandardAPI standardAPI = new CVCEStandardAPI();
        standardAPI.setId(articleCeStandardId);
        return standardAPI;
    }

    public static CVOrderUnitAPI createValidUnitAPI( long id ) {
        CVOrderUnitAPI unitAPI = new CVOrderUnitAPI();
        unitAPI.setId(id);
        unitAPI.setCode(articleOrderUnitCode);
        unitAPI.setName(articleOrderUnitName);
        return unitAPI;
    }

    public static CVPackageUnitAPI createValidPackageUnitAPI( long id ) {
        CVPackageUnitAPI unitAPI = new CVPackageUnitAPI();
        unitAPI.setId(id);
        unitAPI.setCode(articleOrderUnitCode);
        unitAPI.setName(articleOrderUnitName);
        return unitAPI;
    }

    public static CVGuaranteeUnitAPI createValidGuaranteeUnitAPI( long id ) {
        CVGuaranteeUnitAPI unitAPI = new CVGuaranteeUnitAPI();
        unitAPI.setId(id);
        unitAPI.setCode(articleGuaranteeUnitCode);
        unitAPI.setName(articleGuaranteeUnitName);
        return unitAPI;
    }

    public static CVGuaranteeUnit createValidGuaranteeUnit( long id ) {
        CVGuaranteeUnit unit = new CVGuaranteeUnit();
        unit.setUniqueId(id);
        unit.setCode(articleGuaranteeUnitCode);
        unit.setName(articleGuaranteeUnitName);
        return unit;
    }
}
