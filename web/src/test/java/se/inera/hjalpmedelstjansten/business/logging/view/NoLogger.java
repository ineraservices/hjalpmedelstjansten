package se.inera.hjalpmedelstjansten.business.logging.view;

import java.util.logging.Level;


public class NoLogger implements HjmtLogger {

    @Override
    public void log(Level level, String message, Object[] params) {
        
    }

    @Override
    public void log(Level level, String message) {

    }

    @Override
    public void log(Level level, String message, Throwable t) {

    }

    @Override
    public void logPerformance(String className, String methodName, long duration, String sessionId) {

    }

    @Override
    public void logAudit(String who, String did, String toType, String toId, String sessionId) {

    }
    
}
