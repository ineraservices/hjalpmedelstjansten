package se.inera.hjalpmedelstjansten.business.helpers;

import org.junit.Test;

import java.math.BigDecimal;
import org.junit.jupiter.api.Nested;

import static org.junit.Assert.*;

public class ExportHelperTests {

    private ExportHelper exportHelper = new ExportHelper();

    @Test
    public void willItReturnDecimals() {

        BigDecimal bigDecimal = new BigDecimal(1.1);
        assertEquals("1,1", exportHelper.parseNumberValueToStringWithCorrectDecimalFormatting(bigDecimal));
    }

    @Test
    public void willItReturnLongValue() {

        BigDecimal bigDecimal = new BigDecimal(1.0);
        assertEquals("1", exportHelper.parseNumberValueToStringWithCorrectDecimalFormatting(bigDecimal));
    }

    @Test
    public void willItReturnLargeDecimals() {

        BigDecimal bigDecimal = new BigDecimal(1.0000000000001);
        assertEquals("1,0000000000001", exportHelper.parseNumberValueToStringWithCorrectDecimalFormatting(bigDecimal));
    }

    @Test
    public void shouldReplaceåäö() {
        final var stringToBeReplaced = "Test åäö test";
        assertEquals("Test aao test", exportHelper.replaceSpecialCharacters(stringToBeReplaced));
    }

    @Test
    public void shouldReplaceÅÄÖ() {
        final var stringToBeReplaced = "Test ÅÄÖ test";
        assertEquals("Test AAO test", exportHelper.replaceSpecialCharacters(stringToBeReplaced));
    }

    @Test
    public void shouldRemoveUnicodeCharacters() {
        final var stringToBeReplaced = "–";
        assertEquals("", exportHelper.replaceSpecialCharacters(stringToBeReplaced));
    }

    @Test
    public void shouldRemoveEndOfTextCharacter() {
        final var stringToBeReplaced = "␃";
        assertEquals("", exportHelper.replaceSpecialCharacters(stringToBeReplaced));
    }
}
