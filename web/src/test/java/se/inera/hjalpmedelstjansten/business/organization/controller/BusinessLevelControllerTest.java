package se.inera.hjalpmedelstjansten.business.organization.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementController;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;

import jakarta.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;


public class BusinessLevelControllerTest {

    BusinessLevelController businessLevelController;
    OrganizationController organizationController;
    UserController userController;
    AgreementController agreementController;
    EntityManager myEntityManager;

    static final long businessLevelId = 1;
    static final long organizationUniqueId = 2;
    static final long businessLevelId2 = 3;
    static final long businessLevelId3 = 4;
    static final String businessLevelName = "name";

    @Before
    public void init() {
        businessLevelController = new BusinessLevelController();
        myEntityManager = Mockito.mock(EntityManager.class);
        BusinessLevel businessLevel = createValidBusinessLevel(businessLevelId, organizationUniqueId, BusinessLevel.Status.ACTIVE);
        Mockito.when(myEntityManager.find(BusinessLevel.class, businessLevelId)).thenReturn(businessLevel);
        BusinessLevel businessLevel2 = createValidBusinessLevel(businessLevelId2, organizationUniqueId, BusinessLevel.Status.ACTIVE);
        Mockito.when(myEntityManager.find(BusinessLevel.class, businessLevelId2)).thenReturn(businessLevel2);
        BusinessLevel businessLevel3 = createValidBusinessLevel(businessLevelId3, organizationUniqueId, BusinessLevel.Status.ACTIVE);
        Mockito.when(myEntityManager.find(BusinessLevel.class, businessLevelId3)).thenReturn(businessLevel3);
        businessLevelController.em = myEntityManager;
        businessLevelController.LOG = new NoLogger();
        organizationController = Mockito.mock(OrganizationController.class);
        Organization organization = OrganizationControllerTest.createValidOrganization(organizationUniqueId, Organization.OrganizationType.CUSTOMER, null, null, null);
        Mockito.when(organizationController.getOrganization(organizationUniqueId)).thenReturn(organization);
        businessLevelController.organizationController = organizationController;

        userController = Mockito.mock(UserController.class);
        List<UserEngagement> userEngagements = new ArrayList<>();
        userEngagements.add(new UserEngagement());
        Mockito.when(userController.findByBusinessLevel(businessLevelId2)).thenReturn(userEngagements);
        businessLevelController.userController = userController;

        agreementController = Mockito.mock(AgreementController.class);
        businessLevelController.agreementController = agreementController;
        List<Agreement> agreements = new ArrayList<>();
        agreements.add(new Agreement());
        Mockito.when(agreementController.findByBusinessLevel(businessLevelId3)).thenReturn(agreements);

        ValidationMessageService validationMessageService = Mockito.mock(ValidationMessageService.class);
        Mockito.when(validationMessageService.generateValidationException(Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer<HjalpmedelstjanstenValidationException>() {
            @Override
            public HjalpmedelstjanstenValidationException answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException((String) args[0] + "->" + (String) args[1]);
                exception.addValidationMessage((String) args[0], (String) args[1]);
                return exception;
            }
        });
        businessLevelController.validationMessageService = validationMessageService;
    }

    @Test
    public void testGetBusinessLevelWithOrganization() {
        Assert.assertNotNull(businessLevelController.getBusinessLevel(organizationUniqueId, businessLevelId));
    }

    @Test
    public void testGetBusinessLevelWithOrganizationWrongOrganizationId() {
        Assert.assertNull(businessLevelController.getBusinessLevel(organizationUniqueId+1000, businessLevelId));
    }

    @Test
    public void testGetBusinessLevelWithOrganizationWrongBusinessLevelId() {
        Assert.assertNull(businessLevelController.getBusinessLevel(organizationUniqueId, businessLevelId+1000));
    }

    @Test
    public void testGetBusinessLevelWithId() {
        Assert.assertNotNull(businessLevelController.getBusinessLevel(businessLevelId));
    }

    @Test
    public void testGetBusinessLevelWithIdWrongId() {
        Assert.assertNull(businessLevelController.getBusinessLevel(businessLevelId+1000));
    }

    @Test
    public void testInactivateBusinessLevel() {
        BusinessLevelAPI businessLevelAPI = businessLevelController.inactivateBusinessLevelOnOrganization(organizationUniqueId, businessLevelId);
        Assert.assertEquals(businessLevelAPI.getStatus(), BusinessLevel.Status.INACTIVE.toString());
    }

    @Test
    public void testInactivateBusinessLevelWrongOrganizationId() {
        BusinessLevelAPI businessLevelAPI = businessLevelController.inactivateBusinessLevelOnOrganization(organizationUniqueId+1000, businessLevelId);
        Assert.assertNull(businessLevelAPI);
    }

    @Test
    public void testInactivateBusinessLevelWrongBusinessLevelId() {
        BusinessLevelAPI businessLevelAPI = businessLevelController.inactivateBusinessLevelOnOrganization(organizationUniqueId, businessLevelId+1000);
        Assert.assertNull(businessLevelAPI);
    }

    @Test
    public void testDeleteBusinessLevel() {
        try {
            Assert.assertNotNull(businessLevelController.deleteBusinessLevelFromOrganization(organizationUniqueId, businessLevelId));
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
    }

    @Test
    public void testDeleteBusinessLevelWrongOrganizationId() {
        try {
            Assert.assertNull(businessLevelController.deleteBusinessLevelFromOrganization(organizationUniqueId+1000, businessLevelId));
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
    }

    @Test
    public void testDeleteBusinessLevelWrongBusinessLevelId() {
        try {
            Assert.assertNull(businessLevelController.deleteBusinessLevelFromOrganization(organizationUniqueId, businessLevelId+1000));
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail();
        }
    }

    @Test
    public void testDeleteBusinessLevelHasUserEngagements() {
        try {
            businessLevelController.deleteBusinessLevelFromOrganization(organizationUniqueId, businessLevelId2);
            Assert.fail();
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertTrue(errorMessageAPI.getMessage().contains("businessLevel.delete.userEngagementsExist"));
        }
    }

    @Test
    public void testDeleteBusinessLevelHasAgreements() {
        try {
            businessLevelController.deleteBusinessLevelFromOrganization(organizationUniqueId, businessLevelId3);
            Assert.fail();
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertTrue(errorMessageAPI.getMessage().contains("businessLevel.delete.agreementsExist"));
        }
    }

    @Test
    public void testAddBusinessLevel() {
        BusinessLevelAPI businessLevelAPI = createValidBusinessLevelAPI(businessLevelId, businessLevelName);
        businessLevelAPI = businessLevelController.addBusinessLevelToOrganization(organizationUniqueId, businessLevelAPI);
        Assert.assertNotNull(businessLevelAPI);
        Assert.assertEquals(businessLevelAPI.getStatus(), BusinessLevel.Status.ACTIVE.toString());
        Assert.assertEquals(businessLevelAPI.getName(), businessLevelName);
    }

    @Test
    public void testAddBusinessLevelWrongOrganizationId() {
        BusinessLevelAPI businessLevelAPI = createValidBusinessLevelAPI(businessLevelId, businessLevelName);
        businessLevelAPI = businessLevelController.addBusinessLevelToOrganization(organizationUniqueId+1000, businessLevelAPI);
        Assert.assertNull(businessLevelAPI);
    }

    public static BusinessLevelAPI createValidBusinessLevelAPI(long businessLevelId, String name) {
        BusinessLevelAPI businessLevelAPI = new BusinessLevelAPI();
        businessLevelAPI.setId(businessLevelId);
        businessLevelAPI.setName(name);
        return businessLevelAPI;
    }

    public static BusinessLevel createValidBusinessLevel(long businessLevelId, long organizationUniqueId, BusinessLevel.Status status) {
        BusinessLevel businessLevel = new BusinessLevel();
        businessLevel.setUniqueId(businessLevelId);
        Organization organization = OrganizationControllerTest.createValidOrganization(organizationUniqueId, Organization.OrganizationType.CUSTOMER, null, null, null);
        businessLevel.setOrganization(organization);
        businessLevel.setStatus(status);
        return businessLevel;
    }

}
