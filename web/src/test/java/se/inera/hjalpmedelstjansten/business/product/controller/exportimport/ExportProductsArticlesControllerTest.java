package se.inera.hjalpmedelstjansten.business.product.controller.exportimport;

import org.apache.poi.xssf.usermodel.*;
import org.hibernate.Session;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.helpers.ExportHelper;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.media.controller.DocumentTypeController;
import se.inera.hjalpmedelstjansten.business.media.controller.MediaController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationControllerTest;
import se.inera.hjalpmedelstjansten.business.product.controller.*;
import se.inera.hjalpmedelstjansten.model.api.*;
import se.inera.hjalpmedelstjansten.model.api.cv.CVOrderUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.UserAPITest;
import se.inera.hjalpmedelstjansten.model.entity.*;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaImage;

import static org.junit.Assert.*;

import jakarta.persistence.EntityManager;
import java.io.*;
import java.time.Instant;
import java.util.*;

import static org.mockito.ArgumentMatchers.*;

//@RunWith(MockitoJUnitRunner.class)
public class ExportProductsArticlesControllerTest {
/*
    private final ReferenceValuesForExportTest referenceValuesForExportTest = new ReferenceValuesForExportTest();

    @Mock
    private HjmtLogger LOG;

    @Mock
    private Session session;

    @Mock
    private EntityManager em;

    @Mock
    private CategoryController categoryController;

    @Spy
    private Category category;

    @Spy
    private XSSFWorkbook workbook;

    @Mock
    private UserAPI userAPI;

    @Mock
    private ProductController productController;

    @Mock
    private CeController ceController;

    @Mock
    private DocumentTypeController documentTypeController;

    @Mock
    private OrderUnitController orderUnitController;

    @Mock
    private PackageUnitController packageUnitController;

    @Mock
    private PreventiveMaintenanceUnitController preventiveMaintenanceUnitController;

    @Mock
    private Map<Long, List<ProductAPI>> categoryToProductsMap;

    @Mock
    private SearchController searchController;

    @Mock
    private ProductAPI productAPI;

    @Spy
    private CategoryAPI categoryAPI;

    @Spy
    private CVOrderUnitAPI orderUnitAPI;

    @Spy
    private MediaImage productMediaImage;

    @Mock
    private MediaController mediaController;

    @Mock
    private Organization organization;

    @Spy
    ExportHelper exportHelper;

    @Spy
    private ArticleAPI articleAPI;

    @Mock
    private ArticleController articleController;

    @Mock
    SearchProductsAndArticlesAPI searchProductsAndArticlesAPI;

    @InjectMocks
    private ExportProductsArticlesController exportProductsArticlesController = new ExportProductsArticlesController();

    private List<SearchProductsAndArticlesAPI> additionalArticles = new ArrayList<>();
    private Set<Long> categoryIdList = new HashSet<>();
    private List<Long> productIds = new ArrayList<>();
    private byte[] workbookByteArray;
    private InputStream inputStream;
    private List<ProductAPI> products = new ArrayList<>();
    private List<ArticleAPI> articles = new ArrayList<>();
    private List<XSSFSheet> sheetList = new ArrayList<>();
    private List<ArticleAPI> articlesNonH = new ArrayList<>();
    private List<CategorySpecificProperty> categorySpecificList = new ArrayList<>();

    @Before
    public void setup() {
        setupObjects();
        mockSetup();
        setupXSSFDocument();
    }

    @After
    public void exportFile() {
        generateLocalExcelFile();
    }

    @Test
    public void TestExportFileHeaderRows() {

        String[] headerRowList = referenceValuesForExportTest.setupHeaderRowReference();
        //Comment this fail out if you're adding new things and modify it after you're done, should never fail before a commit goes out.
        if (sheetList.get(0).getRow(1).getLastCellNum() != headerRowList.length) {
            fail("Amount of cells generated (" + sheetList.get(0).getRow(1).getLastCellNum() + ") in the excel sheet is no longer the same amount in our expected amount of generated values (" + headerRowList.length + ")");
        }

        for (int i = 0; i < headerRowList.length; i++) {

            XSSFCell cell = sheetList.get(0).getRow(1).getCell(i);
            String[] splitData = headerRowList[i].split(":");
            String dataToCompare;

            if (splitData.length == 2) {
                dataToCompare = splitData[1];
            } else {
                dataToCompare = "";
            }

            checkInternalTextIndexCorrectness(headerRowList[i], splitData[0], i);
            if (cell != null) {
                assertEquals(dataToCompare, cell.getStringCellValue());
            } else {
                fail("Cell on position: " + i + " is Null, something is not right");
            }
        }
    }

    @Test
    public void TestExportFileDataRowFirstArticle() {

        String[] dataRowList = referenceValuesForExportTest.setupDataFromFirstDataRowBasedOnArticle();
        for (int i = 0; i < dataRowList.length; i++) {

            XSSFCell cell = sheetList.get(0).getRow(3).getCell(i);
            String[] splitData = dataRowList[i].split(":");
            String dataToCompare;

            if (splitData.length == 3) {
                dataToCompare = splitData[2];
            } else {
                dataToCompare = "";
            }

            checkInternalTextIndexCorrectness(dataRowList[i], splitData[0], i);

            switch (cell.getCellTypeEnum()) {

                case STRING:
                    assertEquals(dataToCompare, cell.getStringCellValue());
                    break;

                case NUMERIC:
                    assertEquals(Double.valueOf(dataToCompare).doubleValue(), cell.getNumericCellValue(), 0);
                    break;

                default:
                    assertEquals("BLANK", cell.getCellTypeEnum().toString());
            }
        }
    }

    @Test
    public void TestExportFileDataRowFirstProduct() {

        String[] dataRowList = referenceValuesForExportTest.setupDataFromFirstDataRowBasedOnProduct();
        for (int i = 0; i < dataRowList.length; i++) {

            XSSFCell cell = sheetList.get(0).getRow(2).getCell(i);
            String[] splitData = dataRowList[i].split(":");
            String dataToCompare;

            if (splitData.length == 3) {
                dataToCompare = splitData[2];
            } else {
                dataToCompare = "";
            }

            checkInternalTextIndexCorrectness(dataRowList[i], splitData[0], i);
            switch (cell.getCellTypeEnum()) {
                case STRING:
                    System.out.println("At Array index position: " + splitData[0] + " loop index: " + i + " on header: " + splitData[1] + " with value: " + cell.getStringCellValue());

                    if (cell.getStringCellValue() == null || cell.getStringCellValue() == "null") {
                        break;
                    }

                    assertEquals(dataToCompare, cell.getStringCellValue());
                    break;

                case NUMERIC:
                    System.out.println("At Array index position: " + splitData[0] + " loop index: " + i + " on header: " + splitData[1] + " with value: " + cell.getNumericCellValue());
                    assertEquals(Double.valueOf(dataToCompare).doubleValue(), cell.getNumericCellValue(), 0);
                    break;

                default:
                    System.out.println("At Array index position: " + splitData[0] + " loop index: " + i + " on header: " + splitData[1] + " with value: BLANK");
                    assertEquals("BLANK", cell.getCellTypeEnum().toString());
            }
        }
    }

    @Test
    public void TestCorrectFieldColor() {

        XSSFSheet sheet = sheetList.get(0);
        XSSFRow row;
        short[] colorReferences = referenceValuesForExportTest.setupColorValidationReferences();

        row = sheet.getRow(2);

        List<XSSFCell> cells = getCellsFromRow(row);

        //We only test the first row and the first 12 cells. If everything is alright after that it should be alright in the entire document, expand this test if it's not enough.
        for(int j = 0; j < 12; j++) {
            if(cells.get(j) != null) {
                short expectedColor;
                if(j < colorReferences.length) {
                    expectedColor = colorReferences[j];
                } else expectedColor = 64;

                validateFieldColor(cells.get(j), expectedColor);
            }
        }
    }


    private boolean validateFieldColor(XSSFCell cell, short referenceColor) {

        XSSFCellStyle cellStyle = null;

        if(cell.getCellStyle() != null) {
            cellStyle = cell.getCellStyle();
        }

        if(cellStyle != null) {
            assertEquals(referenceColor, cell.getCellStyle().getFillForegroundColor());
        }

        return true;

    }

    private List<XSSFCell> getCellsFromRow(XSSFRow row) {

        List<XSSFCell> listOfCells = new ArrayList<>();
        for(int i = 0; i < row.getLastCellNum(); i++) {
            listOfCells.add(row.getCell(i));
        }
        return listOfCells;
    }


    @Test
    public void TestValidationFields() {

        //Test the first 5 sheets since these are category sheets.
        XSSFSheet[] listOfSheetsThatNeedTheirValidationChecked = {
                sheetList.get(0),
                sheetList.get(1),
                sheetList.get(2),
                sheetList.get(3),
                sheetList.get(4)
        };

        for(XSSFSheet sheet: listOfSheetsThatNeedTheirValidationChecked) {
            List<XSSFDataValidation> dataValidations = sheet.getDataValidations();
            String[] validationReferences = referenceValuesForExportTest.setupDataValidationReferences();

            // If this fails columns have been added to the document and the validation field test will fail - take a look in "ReferenceValuesForExportTest" and add/ remove what is needed to make it not fail.
            for (int i = 0; i < dataValidations.size(); i++) {
                //System.out.println(dataValidations.get(i).prettyPrint());
                assertEquals(validationReferences[i], dataValidations.get(i).prettyPrint());
            }
        }
    }

    @Test
    public void TestNonHArticlesSheetGetsCorrectName() {

        //Test the first 4 sheets since these are NonHCategory sheets. Names needs to be confirmed since we need to confirm both the composition and the sequence.
        XSSFSheet[] listOfSheetsThatNeedTheirNameConfirmed= {
                sheetList.get(1),
                sheetList.get(2),
                sheetList.get(3),
                sheetList.get(4)
        };

        String categorySheetName = sheetList.get(0).getSheetName().replace("name", "");
        String[] referenceValues = referenceValuesForExportTest.setupNonHArticleSheetNameReference();
        for(int i = 0; i < listOfSheetsThatNeedTheirNameConfirmed.length; i++) {
            assertEquals(categorySheetName + " " + referenceValues[i], sheetList.get(i+1).getSheetName());
        }
    }

    @Test
    public void TestHiddenRows() {

        //Test the first 5 sheets since these are category sheets.
        XSSFSheet[] listOfSheetsThatNeedTheirTopRowChecked = {
                sheetList.get(0),
                sheetList.get(1),
                sheetList.get(2),
                sheetList.get(3),
                sheetList.get(4)
        };

        for(XSSFSheet sheet: listOfSheetsThatNeedTheirTopRowChecked) {
            XSSFRow headerRow = sheet.getRow(0);
            assertEquals(true, headerRow.getZeroHeight());
        }
    }

    @Test
    public void TestHeaderRowNothidden() {
        //Test the first 5 sheets since these are category sheets.
        XSSFSheet[] listOfSheetsThatNeedTheiHeaderRowChecked = {
                sheetList.get(0),
                sheetList.get(1),
                sheetList.get(2),
                sheetList.get(3),
                sheetList.get(4)
        };

        for(XSSFSheet sheet: listOfSheetsThatNeedTheiHeaderRowChecked) {
            XSSFRow headerRow = sheet.getRow(1);
            assertEquals(false, headerRow.getZeroHeight());
        }

    }

    @Test
    public void TestHiddenColumns() {
        XSSFSheet sheet = sheetList.get(0);
        assertEquals(true, sheet.isColumnHidden(0));
    }

    private void setupXSSFDocument() {
        try {
            workbookByteArray = exportProductsArticlesController.exportFile(organization.getUniqueId(), categoryIdList, productIds, true, true, true, true, true, userAPI,"1", "1");
            inputStream = new ByteArrayInputStream(workbookByteArray);
            workbook = new XSSFWorkbook(inputStream);

            setupNewArticle(workbook);

            for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                sheetList.add(workbook.getSheetAt(i));
            }

        } catch (HjalpmedelstjanstenValidationException | IOException e) {
            e.printStackTrace();
        }
    }

    private void setupNewArticle(XSSFWorkbook workbook) {

        XSSFRow row = workbook.getSheetAt(0).createRow(workbook.getSheetAt(0).getLastRowNum() + 1);
        row.createCell(1).setCellValue("Article");
        row.createCell(2).setCellValue("NewArticle");
        row.createCell(3).setCellValue("NewArticle");

    }

    private void setupObjects() {
        setupProductAPI();
        setupArticleAPI();
        setupCategory();
        setupOrganization();
        setupUserAPI();
        setupSearchProductsAndArticlesAPIS();
        setupCategorySpecificProperty();
    }

    private void setupProductAPI() {
        this.productAPI = ProductControllerTest.createValidProductAPI();
        productAPI.setId(200L);
        productAPI.setArticleQuantityInOuterPackage(10D);
        productAPI.setCreated(Instant.now().getEpochSecond());
        categoryAPI.setName("asdasd");
        productAPI.setCategory(categoryAPI);
        products.add(productAPI);
    }

    private void setupUserAPI() {
        this.userAPI = UserAPITest.createValidUserAPI(123123L, this.organization.getUniqueId(), 123123L, UserRole.RoleName.Superadmin, 123123L);
    }

    private void setupArticleAPI() {

        this.articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.setArticleQuantityInOuterPackage(10D);
        articleAPI.setCreated(Instant.now().getEpochSecond());
        articleAPI.setBasedOnProduct(productAPI);
        articles.add(articleAPI);

        for (int i = 0; i < 10; i++) {

            String uid = UUID.randomUUID().toString();
            long randomId = new Random().nextInt(2000);

            this.articleAPI = ArticleControllerTest.createValidArticleAPI();
            articleAPI.setArticleName(uid);
            articleAPI.setArticleNumber(uid);
            articleAPI.setId(randomId);
            articleAPI.setArticleQuantityInOuterPackage(10D);
            articleAPI.setCreated(Instant.now().getEpochSecond());
            articleAPI.setBasedOnProduct(productAPI);
            articles.add(articleAPI);

        }

        CategoryAPI nonHCategoryAPI = new CategoryAPI();
        nonHCategoryAPI.setArticleType(Article.Type.T);
        nonHCategoryAPI.setName("Tillbehör");
        nonHCategoryAPI.setId(555555L);

        ArticleAPI articleAPITestable = ArticleControllerTest.createValidArticleAPI();
        articleAPITestable.setArticleName("testestName");
        articleAPITestable.setArticleNumber("testestNumber");
        articleAPITestable.setId(200L);
        articleAPITestable.setCreated(Instant.now().getEpochSecond());
        articleAPITestable.setFitsToProducts(products);
        articleAPITestable.setCategory(nonHCategoryAPI);
        articlesNonH.add(articleAPITestable);

        for (int i = 0; i < 10; i++) {

            String uid = UUID.randomUUID().toString();
            long randomId = new Random().nextInt(2000);

            ArticleAPI articleAPIFilling = ArticleControllerTest.createValidArticleAPI();
            articleAPIFilling.setArticleName(uid);
            articleAPIFilling.setArticleNumber(uid);
            articleAPIFilling.setId(randomId);
            articleAPIFilling.setArticleQuantityInOuterPackage(10D);
            articleAPIFilling.setCreated(Instant.now().toEpochMilli());
            articleAPIFilling.setFitsToProducts(products);
            articleAPIFilling.setCategory(nonHCategoryAPI);
            articlesNonH.add(articleAPIFilling);

        }
    }

    private void setupSearchProductsAndArticlesAPIS() {

        for (int i = 0; i < 10; i++) {

            long randomId = new Random().nextLong();
            SearchProductsAndArticlesAPI searchProductsAndArticlesAPI = new SearchProductsAndArticlesAPI();
            searchProductsAndArticlesAPI.setId(randomId);
            additionalArticles.add(searchProductsAndArticlesAPI);
        }
    }

    private void setupCategory() {
        this.category = CategoryControllerTest.createValidCategory(100L, Article.Type.H, "123123123");
        categoryIdList.add(category.getUniqueId());
    }

    private void setupCategorySpecificProperty() {

        CategorySpecificProperty categorySpecificPropertyDecimal = new CategorySpecificProperty();
        categorySpecificPropertyDecimal.setCategory(category);
        categorySpecificPropertyDecimal.setName("Decimal-field");
        categorySpecificPropertyDecimal.setType(CategorySpecificProperty.Type.DECIMAL);

        CategorySpecificProperty categorySpecificPropertyTextField = new CategorySpecificProperty();
        categorySpecificPropertyTextField.setCategory(category);
        categorySpecificPropertyTextField.setName("Text-field");
        categorySpecificPropertyTextField.setType(CategorySpecificProperty.Type.TEXTFIELD);

        categorySpecificList.add(categorySpecificPropertyDecimal);
        categorySpecificList.add(categorySpecificPropertyTextField);
    }

    private void setupOrganization() {
        organization = OrganizationControllerTest.createValidOrganization(123123123L,
                Organization.OrganizationType.SUPPLIER,
                Date.from(Instant.EPOCH),
                123L,
                "Test organization");
    }

    /**
     * Will create the Mocked behaviour of the different external classes of ExportProductsArticlesController.
     * @EntityManager em: Mocking the unwrapping of the Session.class - returns an empty non specified session - will fail without it.
     * @CategoryController categoryController.getById() -Will return our custom category created in this test.
     * @CategoryController categoryController.getCategoryPropertys() - Will return our custom List of categoryProperties.
     * @ArticleController articleController.findByCategory() - will return null right now.
     * @ArticleController articleController.getArticleAPI() - Only returns one object, we sat up a flow of different objects that are supposed to be returned.
     * @ProductController productController.findByCategory() - will now return our custom list of products (right now only 1 object will be returned).
     * @ProductController productController.getArticlesAPIsBasedOnProduct() - returns our custom list of articleAPIs based on the Product we're using in the test.
     * @SearchController searchController.searchArticlesByProduct() - since we're using the search controller to find loosely connected articles to products, we return a list of Additions (Tillbehör) to confirm that this functionality is working as expected.
     *
     */
 /*
    private void mockSetup() {

        Mockito.when(em.unwrap(Session.class)).thenReturn(session);

        Mockito.when(categoryController.getById(category.getUniqueId())).thenReturn(category);

        Mockito.when(categoryController.getCategoryPropertys(category.getUniqueId())).thenReturn(this.categorySpecificList);

        Mockito.when(articleController.findByCategory(organization.getUniqueId(), 100L, userAPI)).thenReturn(null);

        Mockito.when(articleController.getArticleAPI(anyLong(), anyLong(), any(UserAPI.class), anyString(), anyString())).thenReturn(
                articlesNonH.get(0),
                articlesNonH.get(1),
                articlesNonH.get(2),
                articlesNonH.get(4),
                articlesNonH.get(5),
                articlesNonH.get(6),
                articlesNonH.get(7),
                articlesNonH.get(8),
                articlesNonH.get(9)
        );

        Mockito.when(productController.findByCategory(anyLong(), anyLong(), any(UserAPI.class))).thenReturn(products);

        Mockito.when(productController.getArticleAPIsBasedOnProduct(anyLong(), anyLong(), any(UserAPI.class))).thenReturn(articles);

        Mockito.when(searchController.searchArticlesByProduct(anyLong(), anyLong(), anyString(), anyList(), anyList(), anyInt(), anyInt(), anyBoolean())).thenReturn(additionalArticles);
    }

    private void checkInternalTextIndexCorrectness(String fullValue, String valueToCompare, int referenceValue) {

        if (Integer.valueOf(valueToCompare) - 1 != referenceValue) {
            fail("You need to number each cell number in the list, this is to make sure consistency is achieved. At: " + fullValue + " with index: " + (referenceValue + 1));
        }
    }

    /**
     * Creates a local excel file so that we can visually confirm how an export looks without building the system.
     */
/*
    public void generateLocalExcelFile() {

        try {
            FileOutputStream out = new FileOutputStream(new File("excelFromTest.xlsx"));
            workbook.write(out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
   */
}
