package se.inera.hjalpmedelstjansten.business.product.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static se.inera.hjalpmedelstjansten.model.entity.Article.FIND_BY_STATUS_AND_REPLACEMENT_DATE;
import static se.inera.hjalpmedelstjansten.model.entity.Article.FIND_BY_STATUS_AND_REPLACEMENT_DATE_PASSED;

import java.util.Date;
import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Product.Status;

@ExtendWith(MockitoExtension.class)
class ArticleRepositoryTest {

    @Mock
    private EntityManager em;
    @Mock
    private TypedQuery typedQuery;
    @InjectMocks
    private ArticleRepository articleRepository;

    @Nested
    class TestFindByStatusAndReplacementDatePassed {

        private List<Article> articleList;

        @BeforeEach
        void setUp() {
            doReturn(typedQuery).when(em).createNamedQuery(any(), any());
            doReturn(typedQuery).when(typedQuery).setParameter(anyString(), any());
            articleList = List.of(new Article());
            doReturn(articleList).when(typedQuery).getResultList();
        }

        @Test
        void shallUseCorrectNamedQuery() {
            articleRepository.findByStatusAndReplacementDatePassed(Status.PUBLISHED, new Date());

            final var stringArgumentCaptor = ArgumentCaptor.forClass(String.class);
            verify(em).createNamedQuery(stringArgumentCaptor.capture(), any());

            assertEquals(FIND_BY_STATUS_AND_REPLACEMENT_DATE_PASSED, stringArgumentCaptor.getValue());
        }

        @Test
        void shallAddCorrectStatusAsParameter() {
            final var expectedStatus = Status.PUBLISHED;
            articleRepository.findByStatusAndReplacementDatePassed(expectedStatus, new Date());

            final var statusArgumentCaptor = ArgumentCaptor.forClass(Status.class);
            verify(typedQuery).setParameter(eq("status"), statusArgumentCaptor.capture());

            assertEquals(expectedStatus, statusArgumentCaptor.getValue());
        }

        @Test
        void shallAddCorrectDateAsParameter() {
            final var expectedDate = new Date();
            articleRepository.findByStatusAndReplacementDatePassed(Status.PUBLISHED, expectedDate);

            final var dateArgumentCaptor = ArgumentCaptor.forClass(Date.class);
            verify(typedQuery).setParameter(eq("replacementDate"), dateArgumentCaptor.capture());

            assertEquals(expectedDate, dateArgumentCaptor.getValue());
        }

        @Test
        void shallReturnResultList() {
            final var actualArticleList = articleRepository.findByStatusAndReplacementDatePassed(Status.PUBLISHED, new Date());

            assertEquals(articleList, actualArticleList);
        }
    }

    @Nested
    class TestFindByStatusAndReplacementDateBetweenFromAndTo {

        private List<Article> articleList;

        @BeforeEach
        void setUp() {
            doReturn(typedQuery).when(em).createNamedQuery(any(), any());
            doReturn(typedQuery).when(typedQuery).setParameter(anyString(), any());
            articleList = List.of(new Article());
            doReturn(articleList).when(typedQuery).getResultList();
        }

        @Test
        void shallUseCorrectNamedQuery() {
            articleRepository.findByStatusAndReplacementDateBetweenFromAndTo(Status.DISCONTINUED, new Date(), new Date());

            final var stringArgumentCaptor = ArgumentCaptor.forClass(String.class);
            verify(em).createNamedQuery(stringArgumentCaptor.capture(), any());

            assertEquals(FIND_BY_STATUS_AND_REPLACEMENT_DATE, stringArgumentCaptor.getValue());
        }

        @Test
        void shallAddCorrectStatusAsParameter() {
            final var expectedStatus = Status.DISCONTINUED;
            articleRepository.findByStatusAndReplacementDateBetweenFromAndTo(expectedStatus, new Date(), new Date());

            final var statusArgumentCaptor = ArgumentCaptor.forClass(Status.class);
            verify(typedQuery).setParameter(eq("status"), statusArgumentCaptor.capture());

            assertEquals(expectedStatus, statusArgumentCaptor.getValue());
        }

        @Test
        void shallAddCorrectFromDateAsParameter() {
            final var expectedFromDate = new Date();
            articleRepository.findByStatusAndReplacementDateBetweenFromAndTo(Status.DISCONTINUED, expectedFromDate, new Date());

            final var dateArgumentCaptor = ArgumentCaptor.forClass(Date.class);
            verify(typedQuery).setParameter(eq("replacementDateFrom"), dateArgumentCaptor.capture());

            assertEquals(expectedFromDate, dateArgumentCaptor.getValue());
        }

        @Test
        void shallAddCorrectToDateAsParameter() {
            final var expectedToDate = new Date();
            articleRepository.findByStatusAndReplacementDateBetweenFromAndTo(Status.DISCONTINUED, new Date(), expectedToDate);

            final var dateArgumentCaptor = ArgumentCaptor.forClass(Date.class);
            verify(typedQuery).setParameter(eq("replacementDateTo"), dateArgumentCaptor.capture());

            assertEquals(expectedToDate, dateArgumentCaptor.getValue());
        }

        @Test
        void shallReturnResultList() {
            final var actualArticleList = articleRepository.findByStatusAndReplacementDateBetweenFromAndTo(Status.DISCONTINUED, new Date(), new Date());

            assertEquals(articleList, actualArticleList);
        }
    }
}
