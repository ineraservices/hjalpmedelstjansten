#!/bin/bash

JBOSS_HOME=/opt/jboss/wildfly
JBOSS_MODE=${1:-"standalone"}
JBOSS_CONFIG=${2:-"$JBOSS_MODE-hjmtj.xml"}

cp /opt/jboss/wildfly/customization/standalone-hjmtj.xml $JBOSS_HOME/$JBOSS_MODE/configuration
sed -i.bak "s|{DB_CONNECTION_URL}|$DB_CONNECTION_URL|g" $JBOSS_HOME/$JBOSS_MODE/configuration/$JBOSS_CONFIG
sed -i.bak "s|{DB_USERNAME}|$DB_USERNAME|g" $JBOSS_HOME/$JBOSS_MODE/configuration/$JBOSS_CONFIG
sed -i.bak "s|{DB_PASSWORD}|$DB_PASSWORD|g" $JBOSS_HOME/$JBOSS_MODE/configuration/$JBOSS_CONFIG

cp /opt/jboss/wildfly/customization/mysql-connector-j-9.0.0.jar $JBOSS_HOME/$JBOSS_MODE/deployments
cp /opt/jboss/wildfly/customization/hjmtj.war $JBOSS_HOME/$JBOSS_MODE/deployments

if [ "$HJMTJ_ENVIRONMENT" = "local" ]
then
    mkdir /opt/jboss/wildfly/modules/hjmtj
    mkdir /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/dev.properties /opt/jboss/wildfly/modules/hjmtj/main/app.properties
    cp /opt/jboss/wildfly/customization/shiro.ini /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/ValidationMessages.properties /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/module.xml /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/organizations.csv /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/categories.csv /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/directives.csv /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/standards.csv /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/orderunits.csv /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/packageunits.csv /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/documenttypes.csv /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/guaranteeunits.csv /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/preventivemaintenances.csv /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/categoryproperties.csv /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/countymunicipality.csv /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/elasticsearch_index_source.json /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/texts.properties /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/message.properties /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/externalcategorygroupingsv2.json /opt/jboss/wildfly/modules/hjmtj/main
    cp /opt/jboss/wildfly/customization/logback-local.xml /opt/jboss/wildfly/modules/hjmtj/main/logback.xml
fi

$JBOSS_HOME/bin/add-user.sh admin Jboss@admin01 --silent

echo "=> Starting WildFly server" with config $JBOSS_CONFIG
$JBOSS_HOME/bin/$JBOSS_MODE.sh -b 0.0.0.0 -c $JBOSS_CONFIG --debug '*:8787' -bmanagement 0.0.0.0
