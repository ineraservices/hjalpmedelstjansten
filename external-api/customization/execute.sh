#!/bin/bash

JBOSS_HOME=/opt/jboss/wildfly
JBOSS_MODE=${1:-"standalone"}
JBOSS_CONFIG=${2:-"$JBOSS_MODE-hjmtj.xml"}

cp /opt/jboss/wildfly/customization/standalone-hjmtj.xml $JBOSS_HOME/$JBOSS_MODE/configuration
sed -i.bak "s|{DB_CONNECTION_URL}|$DB_CONNECTION_URL|g" $JBOSS_HOME/$JBOSS_MODE/configuration/$JBOSS_CONFIG
sed -i.bak "s|{DB_USERNAME}|$DB_USERNAME|g" $JBOSS_HOME/$JBOSS_MODE/configuration/$JBOSS_CONFIG
sed -i.bak "s|{DB_PASSWORD}|$DB_PASSWORD|g" $JBOSS_HOME/$JBOSS_MODE/configuration/$JBOSS_CONFIG

cp /opt/jboss/wildfly/customization/mysql-connector-j-9.0.0.jar $JBOSS_HOME/$JBOSS_MODE/deployments

cp /opt/jboss/wildfly/customization/external.war $JBOSS_HOME/$JBOSS_MODE/deployments

if [ "$HJMTJ_ENVIRONMENT" = "local" ]
then
    mkdir /opt/jboss/wildfly/modules/hjmtj-external
    mkdir /opt/jboss/wildfly/modules/hjmtj-external/main
    cp /opt/jboss/wildfly/customization/dev.properties /opt/jboss/wildfly/modules/hjmtj-external/main/app.properties
    cp /opt/jboss/wildfly/customization/module.xml /opt/jboss/wildfly/modules/hjmtj-external/main
    cp /opt/jboss/wildfly/customization/logback-local.xml /opt/jboss/wildfly/modules/hjmtj-external/main/logback.xml
fi

echo "=> Starting WildFly server" with config $JBOSS_CONFIG
$JBOSS_HOME/bin/$JBOSS_MODE.sh -b 0.0.0.0 -c $JBOSS_CONFIG --debug '*:8787' -bmanagement 0.0.0.0
