package se.inera.hjalpmedelstjansten.externalapi;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;


@ApplicationPath("v1")
public class JAXRSConfiguration extends Application {

}
