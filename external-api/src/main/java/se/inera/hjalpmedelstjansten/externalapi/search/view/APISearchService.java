package se.inera.hjalpmedelstjansten.externalapi.search.view;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.externalapi.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.externalapi.search.controller.APISearchController;
import se.inera.hjalpmedelstjansten.externalapi.search.controller.LogRequest;
import se.inera.hjalpmedelstjansten.model.api.external.SearchRequestAPI;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.json.JsonObject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;


/**
 * The entrypoint to the api. All requests go through here.
 *
 */
@Stateless
@Path("search")
@Interceptors({ PerformanceLogInterceptor.class })
public class APISearchService {

    @Inject
    HjmtLogger LOG;

    @Inject
    APISearchController apiSearchController;
    //String apiKeySaved = "key";

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @LogRequest
    public Response apiSearch(SearchRequestAPI searchRequestAPI) throws UnsupportedEncodingException {
        LOG.log( Level.FINEST, "apiSearch(...)" );
        JsonObject jsonObject = apiSearchController.search(searchRequestAPI);
        return Response.ok(jsonObject).build();
    }

}
