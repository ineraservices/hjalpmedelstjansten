package se.inera.hjalpmedelstjansten.externalapi.search.view;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.externalapi.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.externalapi.search.controller.APIFieldController;
import se.inera.hjalpmedelstjansten.externalapi.search.controller.LogRequest;
import se.inera.hjalpmedelstjansten.model.api.external.FieldRequestAPI;
import se.inera.hjalpmedelstjansten.model.api.external.FieldResponseItemAPI;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptors;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Level;

/**
 * The entrypoint to the api. All requests go through here.
 *
 */
@Stateless
@Path("fields")
@Interceptors({ PerformanceLogInterceptor.class })
public class APIFieldsService {

    @Inject
    HjmtLogger LOG;

    @Inject
    APIFieldController apiFieldController;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @LogRequest
    public Response apiSearch(FieldRequestAPI fieldRequestAPI) {
        LOG.log( Level.FINEST, "apiSearch(...)" );
        List<FieldResponseItemAPI> fieldResponseAPIs = apiFieldController.search(fieldRequestAPI);
        return Response.ok(fieldResponseAPIs).build();
    }

}
