package se.inera.hjalpmedelstjansten.externalapi.property;

import se.inera.hjalpmedelstjansten.property.AppConfigurationService;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.inject.Inject;


@Startup
@Singleton
public class PropertyLoader {

    @Inject
    private AppConfigurationService appConfigurationService;

    @PostConstruct
    private void loadProperties() {
        appConfigurationService.loadProperties("app.properties");
    }

}
