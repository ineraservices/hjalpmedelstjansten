package se.inera.hjalpmedelstjansten.externalapi.search.controller;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;

import jakarta.inject.Inject;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.ext.Provider;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * Filter to log request body
 *
 */
@LogRequest
@Provider
public class LogRequestFilter implements ContainerRequestFilter {

    @Inject
    HjmtLogger LOG;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(containerRequestContext.getEntityStream()))) {
            String body = buffer.lines().collect(Collectors.joining("\n"));
            LOG.log( Level.INFO, "body: {0}", new Object[] {body});
            containerRequestContext.setEntityStream(new ByteArrayInputStream(body.getBytes()));
        }
    }

}
