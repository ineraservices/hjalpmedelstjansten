package se.inera.hjalpmedelstjansten.externalapi.search.controller;

/**
 * Names of fields in the api.
 *
 */
public class FieldNames {

    public static final String ID = "Id";
    public static final String TOTAL_HITS = "TotalHits";
    public static final String TOTAL_COUNTIES = "TotalCounties";
    public static final String TOTAL_MUNICIPALITIES = "TotalMunicipalities";
    public static final String CATEGORY_ID = "CategoryId";
    public static final String CATEGORY_CODE = "CategoryCode";
    public static final String CATEGORY_NAME = "CategoryName";
    public static final String CATEGORY_TEXT = "CategoryText";
    public static final String SUPPLIER_ID = "SupplierId";
    public static final String SUPPLIER_NAME = "SupplierName";
    public static final String NAME = "Name";
    public static final String LABEL = "Label";
    public static final String ITEMS = "Items";
    public static final String DESCRIPTION = "Description";

    // menutree specific
    public static final String MENU_TREE_DESCRIPTION = "939c8fcfa88c4c80b23ee0fb7dc7f1f8";
    public static final String MENU_TREE_SORT_ORDER = "MenuTreeName";
    public static final String MENU_TREE_PARENT_ITEM = "Parentmenutree";
    public static final String MENU_TREE_CATEGORIES = "ProductCategories";

    // classification specific
    public static final String CLASSIFICATION_ID = "CategoryId";
    public static final String CLASSIFICATION_CODE = "CategoryCode";
    public static final String CLASSIFICATION_PARENT = "ParentClassification";

    // region specific
    public static final String COUNTY_CODE = "CountyCode";
    public static final String MUNICIPALITY_CODE = "MunicipalityCode";
    public static final String MUNICIPALITY_PARENT = "ParentCountyMunicipalityId";

    // organization specific
    public static final String ORGANIZATION_GLN = "GLN";
    public static final String ORGANIZATION_ADDRESS = "Address";
    public static final String ORGANIZATION_ADDRESS1 = "Address1";
    public static final String ORGANIZATION_POSTAL_CODE = "PostalCode";
    public static final String ORGANIZATION_TOWN_CITY = "TownCity";
    public static final String ORGANIZATION_COUNTRY = "Country";
    public static final String ORGANIZATION_PHONE_NUMBER = "PhoneNo";
    public static final String ORGANIZATION_ECONOMIC_INFORMATION = "EconomicInformation";
    public static final String ORGANIZATION_NUMBER = "OrganizationNo";

    // product specific
    public static final String PRODUCT_ID = "ProductId";
    public static final String PRODUCT_NAME = "ProductName";
    public static final String PRODUCT_DESCRIPTION_1177 = "ProductDescription1177";
    public static final String PRODUCT_IS_TEMPLATE_PRODUCT = "IsTemplateProduct";
    public static final String PRODUCT_SUPPLIER = "Supplier";
    public static final String PRODUCT_SUPPLIER_URL = "SupplierUrl";
    public static final String PRODUCT_MAIN_IMAGE = "MainImage";
    public static final String PRODUCT_IMAGES = "Images";
    public static final String PRODUCT_DOCUMENTS = "Documents";
    public static final String PRODUCT_URL = "Url";
    public static final String PRODUCT_ALT_TEXT = "AltText";
    public static final String PRODUCT_MIME_TYPE = "MimeType";
    public static final String PRODUCT_CE = "ProductMarking";
    public static final String PRODUCT_CE_MARKED = "CEmark";
    public static final String PRODUCT_CLASSIFICATION = "Classification";
    public static final String PRODUCT_BASED_ON_PRODUCT = "BasedOnTemplateProduct";
    public static final String PRODUCT_CATEGORY_SPECIFIC_PROPERTY_PREFIX = "isoattr_";
    public static final String PRODUCT_CATEGORY_SPECIFIC_PROPERTY_MIN = "Min";
    public static final String PRODUCT_CATEGORY_SPECIFIC_PROPERTY_MAX = "Max";
    public static final String PRODUCT_MAXWIDTH = "MaxWidth";
    public static final String PRODUCT_SCALED_IMAGES = "ScaledImages";
    public static final String PRODUCT_FILE = "File";

}
