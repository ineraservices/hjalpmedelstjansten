package se.inera.hjalpmedelstjansten.externalapi.logging.view;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;

import jakarta.inject.Inject;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.AroundTimeout;
import jakarta.interceptor.InvocationContext;
import java.lang.reflect.Modifier;

/**
 * Interceptor for logging performance of methods
 *
 */
public class PerformanceLogInterceptor {

    @Inject
    private HjmtLogger LOG;

    @AroundInvoke
    @AroundTimeout
    public Object logPerformance( InvocationContext invocationContext ) throws Exception {
        if( Modifier.isPublic(invocationContext.getMethod().getModifiers()) ) {
            // only log public methods. other REST-class methods should be private
            String className = invocationContext.getMethod().getDeclaringClass().getName();
            String methodName = invocationContext.getMethod().getName();
            String sessionId = null; // no session in external api
            long start = System.currentTimeMillis();
            try {
                return invocationContext.proceed();
            } finally {
                long total = System.currentTimeMillis() - start;
                LOG.logPerformance(className, methodName, total, sessionId);
            }
        } else {
            return invocationContext.proceed();
        }
    }

}
