import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PathService} from './path.service';
import {BusinessLevel} from '../models/organization/business-level.model';
import {Organization} from '../models/organization/organization.model';
import {Role} from '../models/user/role.model';

@Injectable()
export class OrganizationService {

	constructor(private httpClient: HttpClient,
							private pathService: PathService) {
	}

	public getBusinessLevels(id): Observable<Array<BusinessLevel>> {
		return this.httpClient.get<Array<BusinessLevel>>(this.pathService.getBusinessLevels(id));
	}

	public getRoles(id): Observable<Array<Role>> {
		return this.httpClient.get<Array<Role>>(this.pathService.getRoles(id));
	}

	public getActiveBusinessLevels(id): Observable<Array<BusinessLevel>> {
		return this.httpClient.get<Array<BusinessLevel>>(this.pathService.getBusinessLevels(id) + '?status=ACTIVE');
	}

	public createBusinessLevel(id, businessLevel): Observable<BusinessLevel> {
		return this.httpClient.post<BusinessLevel>(this.pathService.getBusinessLevels(id), businessLevel);
	}

	public updateBusinessLevel(organizationId, businessLevelId, businessLevel): Observable<BusinessLevel> {
		return this.httpClient.post<BusinessLevel>(this.pathService.getBusinessLevels(
			organizationId) + '/' + businessLevelId + '/update', businessLevel);
	}

	public inactivateBusinessLevel(organizationId, businessLevelId): Observable<BusinessLevel> {
		return this.httpClient.post<BusinessLevel>(this.pathService.getBusinessLevels(
			organizationId) + '/' + businessLevelId + '/inactivate', {});
	}

	public deleteBusinessLevel(organizationId, businessLevelId): Observable<Object> {
		return this.httpClient.delete(this.pathService.getBusinessLevels(organizationId) + '/' + businessLevelId);
	}

	public searchOrganizations(query): Observable<HttpResponse<Array<Organization>>> {
		return this.httpClient.get<Array<Organization>>(this.pathService.searchOrganizations() + query, {observe: 'response'});
	}

	public searchValidOrganizations(query): Observable<HttpResponse<Array<Organization>>> {
		return this.httpClient.get<Array<Organization>>(this.pathService.searchValidOrganizations() + query, {observe: 'response'});
	}

	public exportOrganizations(query) {
		return this.httpClient.post(this.pathService.getExportOrganizations() + query, {}, {
			responseType: 'blob',
			observe: 'response'
		});
	}

	public getOrganization(id): Observable<Organization> {
		return this.httpClient.get<Organization>(this.pathService.getOrganization() + id);
	}

	public updateOrganization(id: string | number, organization: Organization): Observable<Organization> {
		return this.httpClient.put<Organization>(this.pathService.getOrganization() + id, organization);
	}

	public createOrganization(organization: Organization): Observable<Organization> {
		return this.httpClient.post<Organization>(this.pathService.getOrganization(), organization);
	}

	public deleteOrganization(id: string | number): Observable<Object> {
		return this.httpClient.delete(this.pathService.getOrganization() + id);
	}
}
