import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {PathService} from './path.service';
import {DynamicText} from "../models/dynamictext.model";

@Injectable()
export class DynamicTextService {

	constructor(private httpClient: HttpClient,
							private pathService: PathService) {
	}

	public getDynamicText(uniqueId): Observable<DynamicText> {
		return this.httpClient.get<DynamicText>(this.pathService.getDynamicText(uniqueId));
	}

	public updateDynamicText(uniqueId, dynamicText): Observable<DynamicText> {
		return this.httpClient.put<DynamicText>(this.pathService.getDynamicText(uniqueId), dynamicText);
	}

}
