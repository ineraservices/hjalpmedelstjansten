import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {Country} from '../models/country.model';
import {PathService} from './path.service';

@Injectable()
export class CountryService {

	constructor(private httpClient: HttpClient,
							private pathService: PathService) {
	}


	public getCountries(): Observable<Array<Country>> {
		return this.httpClient.get<Array<Country>>(this.pathService.getCountries());
	}
}
