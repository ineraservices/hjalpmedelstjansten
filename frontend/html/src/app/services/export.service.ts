import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PathService} from './path.service';
import {GeneralPricelist} from '../models/general-pricelist/general-pricelist.model';
import {ExportSetting} from '../models/export-setting.model';

@Injectable()
export class ExportService {

	constructor(private httpClient: HttpClient,
							private pathService: PathService) {
	}

	public getAllGP(query): Observable<HttpResponse<Array<GeneralPricelist>>> {
		return this.httpClient.get<Array<GeneralPricelist>>(this.pathService.getAllGP() + query, {observe: 'response'});
	}

	public getAllUnselectedGP(organizationId, exportSettingsId, query): Observable<HttpResponse<Array<GeneralPricelist>>> {
		return this.httpClient.get<Array<GeneralPricelist>>(this.pathService.getAllUnselectedGP(organizationId, exportSettingsId) + query, {observe: 'response'});
		// return this.httpClient.get<Array<GeneralPricelist>>(this.pathService.getAllUnselectedGP(exportSettingsId) + query, {observe: 'response'});
	}

	public getExportSettings(): Observable<Array<ExportSetting>> {
		return this.httpClient.get<Array<ExportSetting>>(this.pathService.getExportSettings());
	}

	public getExportSetting(id): Observable<Array<ExportSetting>> {
		return this.httpClient.get<Array<ExportSetting>>(this.pathService.getExportSettings() + id);
	}

	public getExportSettingsForOrganization(organizationId): Observable<Array<ExportSetting>> {
		return this.httpClient.get<Array<ExportSetting>>(this.pathService.getExportSettingsForOrganization(organizationId));
	}

	public getExportSettingForOrganization(organizationId, id): Observable<ExportSetting> {
		return this.httpClient.get<ExportSetting>(this.pathService.getExportSettingsForOrganization(organizationId) + id);
	}

	public exportGeneralPricelists(organizationId, exportSettingsUniqueId) {
		return this.httpClient.post(this.pathService.getExportGPs(organizationId, exportSettingsUniqueId), {}, {
			responseType: 'blob',
			observe: 'response'
		});
	}

	public createExportSetting(exportSetting): Observable<ExportSetting> {
		return this.httpClient.post<ExportSetting>(this.pathService.getExportSettings(), exportSetting);
	}

	public queueExport(exportSetting): Observable<ExportSetting> {
		return this.httpClient.post<ExportSetting>(this.pathService.queueExport(), exportSetting);
	}

	public updateExportSetting(id, exportSetting): Observable<ExportSetting> {
		return this.httpClient.put<ExportSetting>(this.pathService.getExportSettings() + id, exportSetting);
	}

	public addGPToExportSetting(organizationId, id, GPs): Observable<Array<GeneralPricelist>> {
		return this.httpClient.post<Array<GeneralPricelist>>(
			this.pathService.getExportSettingsForOrganization(organizationId) + id + '/generalpricelists', GPs);
	}

	public removeGPFromExportSetting(organizationId, exportSettingId, gp): Observable<Object> {
		const header: HttpHeaders = new HttpHeaders()
			.append('Content-Type', 'application/json');
		const httpOptions = {
			headers: header,
			body: gp
		};
		return this.httpClient.delete(
			this.pathService.getExportSettingsForOrganization(organizationId) + exportSettingId + '/generalpricelists', httpOptions);
	}
}
