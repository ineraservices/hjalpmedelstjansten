import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PathService} from './path.service';
import {Agreement} from '../models/agreement/agreement.model';
import {Pricelist} from '../models/agreement/pricelist.model';
import {Pricelistrow} from '../models/agreement/pricelistrow.model';
import {GeneralPricelist} from '../models/general-pricelist/general-pricelist.model';
import {GeneralPricelistPricelist} from '../models/general-pricelist/general-pricelist-pricelist.model';
import {GeneralPricelistPricelistrow} from '../models/general-pricelist/general-pricelist-pricelistrow.model';
import {SimpleArticle} from '../models/product/simple-article.model';

@Injectable()
export class AgreementService {

	constructor(private httpClient: HttpClient,
							private pathService: PathService) {
	}

	public searchAgreements(organizationId, query): Observable<HttpResponse<Array<Agreement>>> {
		return this.httpClient.get<Array<Agreement>>(this.pathService.getAgreements(organizationId) + query, {observe: 'response'});
	}

	public adminSearchAllAgreeementsForOrganization(organizationId, query): Observable<HttpResponse<Array<Agreement>>> {
		return this.httpClient.get<Array<Agreement>>(this.pathService.getAdminViewAgreements(organizationId) + query, {observe: 'response'});
	}

	public getAgreement(organizationId, agreementId): Observable<Agreement> {
		return this.httpClient.get<Agreement>(this.pathService.getAgreements(organizationId) + '/' + agreementId);
	}

	public createAgreement(organizationId, agreement): Observable<Agreement> {
		return this.httpClient.post<Agreement>(this.pathService.getAgreements(organizationId), agreement);
	}

	public updateAgreement(organizationId, agreementId, agreement): Observable<Agreement> {
		return this.httpClient.put<Agreement>(this.pathService.getAgreements(organizationId) + '/' + agreementId, agreement);
	}

	public deleteAgreement(organizationId, agreementId): Observable<Object> {
		return this.httpClient.delete(this.pathService.deleteAgreement(organizationId, agreementId));
	}

	public getPriceLists(organizationId, agreementId): Observable<Array<Pricelist>> {
		return this.httpClient.get<Array<Pricelist>>(this.pathService.getAgreements(organizationId) + '/' + agreementId + '/pricelists');
	}

	public getSelectablePriceLists(organizationId, agreementId, articleId): Observable<Array<Pricelist>> {
		return this.httpClient.get<Array<Pricelist>>(this.pathService.getAgreements(organizationId) + '/' + agreementId + '/pricelists/article/' + articleId);
	}

	public getPricelistsFilteredBy(organizationId, filter): Observable<Object> {
		return this.httpClient.get(this.pathService.getPricelistsFilteredBy(organizationId, filter));
	}

	public getAgreementsToExtend(organizationId): Observable<Object> {
		return this.httpClient.get(this.pathService.getAgreementsToExtend(organizationId));
	}

	public getAgreementsChangedByCustomer(organizationId): Observable<Object> {
		return this.httpClient.get(this.pathService.getAgreementsChangedByCustomer(organizationId));
	}

	public getPricelistRowsApprovedByCustomer(organizationId): Observable<Object> {
		return this.httpClient.get(this.pathService.getPricelistRowsApprovedByCustomer(organizationId));
	}

	public removeAgreementChangedNotice(organizationId, agreementChangedId): Observable<Object> {
		return this.httpClient.delete(this.pathService.removeAgreementChangedNotice(organizationId, agreementChangedId));
	}

	public removeApprovedPricelistRowNotice(organizationId, approvementId): Observable<Object> {
		return this.httpClient.delete(this.pathService.removeApprovedPricelistRowNotice(organizationId, approvementId));
	}


	public getPriceList(organizationId, agreementId, pricelistId): Observable<Pricelist> {
		return this.httpClient.get<Pricelist>(
			this.pathService.getAgreements(organizationId) + '/' + agreementId + '/pricelists/' + pricelistId);
	}

	public getPricelistRows(organizationId, agreementId, pricelistId, query): Observable<HttpResponse<Array<Pricelistrow>>> {
		return this.httpClient.get<Array<Pricelistrow>>(
			this.pathService.getPricelistRows(organizationId, agreementId, pricelistId) + '?query=' + query, {observe: 'response'});
	}

  public getAllPricelistRows(organizationId, agreementId, pricelistId, query): Observable<HttpResponse<Array<Pricelistrow>>> {
    return this.httpClient.get<Array<Pricelistrow>>(
      this.pathService.getAllPricelistRows(organizationId, agreementId, pricelistId) + '?query=' + query, {observe: 'response'});
  }

  public getPricelistRowsByApprovementId(organizationId, agreementId, pricelistId, approvementId): Observable<HttpResponse<Array<Pricelistrow>>> {
    return this.httpClient.get<Array<Pricelistrow>>(
      this.pathService.getPricelistRowsByApprovementId(organizationId, agreementId, pricelistId, approvementId), {observe: 'response'});
  }

	public getExportPricelistUrl(organizationId, agreementId, pricelistId) {
		return this.pathService.exportPricelist(organizationId, agreementId, pricelistId);
	}

	public importPricelist(organizationId, agreementId, pricelistId, formData) {
		return this.httpClient.post(this.pathService.importPricelist(organizationId, agreementId, pricelistId), formData);
	}

	public getExportGeneralPricelistPricelistUrl(organizationId, pricelistId, status) {
		if (status === 'discontinued') {
			return this.pathService.exportGeneralPricelistPricelistDiscontinued(organizationId, pricelistId);
		}
		if (status === 'all') {
			return this.pathService.exportGeneralPricelistPricelistAll(organizationId, pricelistId);
		}
		if (status === 'inProduction') {
			return this.pathService.exportGeneralPricelistPricelist(organizationId, pricelistId);
		}
	}

	public getExportPricelistRowsUrl(organizationId, agreementId, pricelistId) {
		return this.pathService.exportPricelistRows(organizationId, agreementId, pricelistId);
	}

	public getExportAgreementsUrl(organizationId) {
		return this.pathService.exportAgreements(organizationId);
	}

	public createPricelist(organizationId, agreementId, pricelist): Observable<Pricelist> {
		return this.httpClient.post<Pricelist>(
			this.pathService.getPricelist(organizationId, agreementId), pricelist);
	}

	public updatePricelist(organizationId, agreementId, pricelistId, pricelist): Observable<Pricelist> {
		return this.httpClient.put<Pricelist>(
			this.pathService.getPricelist(organizationId, agreementId) + '/' + pricelistId, pricelist);
	}

	public createPricelistRows(organizationId, agreementId, pricelistId, pricelistRows): Observable<Array<Pricelistrow>> {
		return this.httpClient.post<Array<Pricelistrow>>(
			this.pathService.getPricelistRows(organizationId, agreementId, pricelistId), pricelistRows);
	}

	public updatePricelistRow(organizationId, agreementId, pricelistId, pricelistRowId, pricelistRow): Observable<Pricelistrow> {
		return this.httpClient.put<Pricelistrow>(
			this.pathService.getPricelistRows(organizationId, agreementId, pricelistId) + '/' + pricelistRowId, pricelistRow);
	}

	public sendPricelistRowsForApproval(organizationId, agreementId, pricelistId, pricelistRows, comment): Observable<Array<Pricelistrow>> {
		return this.httpClient.post<Array<Pricelistrow>>(
			this.pathService.getPricelistRows(organizationId, agreementId, pricelistId) + '/sendForApproval/' + comment, pricelistRows);
	}

	public sendAllPricelistRowsForApproval(organizationId, agreementId, pricelistId, comment): Observable<Array<Pricelistrow>> {
		return this.httpClient.post<Array<Pricelistrow>>(
			this.pathService.getPricelistRows(organizationId, agreementId, pricelistId) + '/sendAllForApproval/' + comment, {});
	}

	public requestInactivationOfPricelistRow(organizationId, agreementId, pricelistId, pricelistRows): Observable<Array<Pricelistrow>> {
		return this.httpClient.post<Array<Pricelistrow>>(
			this.pathService.getPricelistRows(organizationId, agreementId, pricelistId) + '/inactivate', pricelistRows);
	}

	public approveInactivationOfPricelistRow(organizationId, agreementId, pricelistId, pricelistRows, comment): Observable<Array<Pricelistrow>> {
		return this.httpClient.post<Array<Pricelistrow>>(
			this.pathService.getPricelistRows(organizationId, agreementId, pricelistId) + '/approveinactivate/' + comment, pricelistRows);
	}

	public declineInactivationOfPricelistRow(organizationId, agreementId, pricelistId, pricelistRows, comment): Observable<Array<Pricelistrow>> {
		return this.httpClient.post<Array<Pricelistrow>>(
			this.pathService.getPricelistRows(organizationId, agreementId, pricelistId) + '/declineinactivate/' + comment, pricelistRows);
	}

	public activatePricelistRows(organizationId, agreementId, pricelistId, pricelistRows, comment): Observable<Array<Pricelistrow>> {
		return this.httpClient.post<Array<Pricelistrow>>(
			this.pathService.getPricelistRows(organizationId, agreementId, pricelistId) + '/activate/' + comment, pricelistRows);
	}

	public activateAllPricelistRows(organizationId, agreementId, pricelistId, comment): Observable<Array<Pricelistrow>> {
		return this.httpClient.post<Array<Pricelistrow>>(
			this.pathService.getPricelistRows(organizationId, agreementId, pricelistId) + '/activateall/' + comment, {});
	}

	public declinePricelistRows(organizationId, agreementId, pricelistId, pricelistRows, comment): Observable<Array<Pricelistrow>> {
		return this.httpClient.post<Array<Pricelistrow>>(
			this.pathService.getPricelistRows(organizationId, agreementId, pricelistId) + '/decline/' + comment, pricelistRows);
	}

	public reopenInactivatedPricelistRow(organizationId, agreementId, pricelistId, pricelistRows): Observable<Array<Pricelistrow>> {
		return this.httpClient.post<Array<Pricelistrow>>(
			this.pathService.getPricelistRows(organizationId, agreementId, pricelistId) + '/reopen', pricelistRows);
	}

	public searchArticlesForPricelist(query, organizationId, agreementId, pricelistId): Observable<HttpResponse<Array<SimpleArticle>>> {
		return this.httpClient.get<Array<SimpleArticle>>(this.pathService.getArticlesForPricelist(
			organizationId, agreementId, pricelistId) + query, {observe: 'response'});
	}

	// General price list

	public searchGeneralPricelists(query): Observable<HttpResponse<Array<GeneralPricelist>>> {
		return this.httpClient.get<Array<GeneralPricelist>>(
			this.pathService.getAllGP() + query, {observe: 'response'});
	}

	public getGeneralPriceList(organizationId): Observable<GeneralPricelist> {
		return this.httpClient.get<GeneralPricelist>(this.pathService.getGeneralPricelists(organizationId));
	}

	public createGeneralPriceList(organizationId, generalPricelist): Observable<GeneralPricelist> {
		return this.httpClient.post<GeneralPricelist>(this.pathService.getGeneralPricelists(organizationId), generalPricelist);
	}

	public updateGeneralPriceList(organizationId, generalPricelist): Observable<GeneralPricelist> {
		return this.httpClient.put<GeneralPricelist>(this.pathService.getGeneralPricelists(organizationId), generalPricelist);
	}

	public getGeneralPricelistPriceLists(organizationId): Observable<Array<GeneralPricelistPricelist>> {
		return this.httpClient.get<Array<GeneralPricelistPricelist>>(this.pathService.getGeneralPricelists(organizationId) + '/pricelists');
	}

	public getGeneralPricelistPriceList(organizationId, generalPricelistId): Observable<GeneralPricelistPricelist> {
		return this.httpClient.get<GeneralPricelistPricelist>(this.pathService.getGeneralPricelists(organizationId) + '/pricelists/' + generalPricelistId);
	}

	public createGeneralPricelistPricelist(organizationId, pricelist): Observable<GeneralPricelistPricelist> {
		return this.httpClient.post<GeneralPricelistPricelist>(
			this.pathService.getGeneralPricelists(organizationId) + '/pricelists', pricelist);
	}

	public updateGeneralPricelistPricelist(organizationId, pricelistId, pricelist): Observable<GeneralPricelistPricelist> {
		return this.httpClient.put<GeneralPricelistPricelist>(
			this.pathService.getGeneralPricelists(organizationId) + '/pricelists/' + pricelistId, pricelist);
	}

	public getGeneralPricelistPricelistRows(organizationId, pricelistId, query): Observable<HttpResponse<Array<Pricelistrow>>> {
		return this.httpClient.get<Array<Pricelistrow>>(
			this.pathService.getGeneralPricelists(organizationId) + '/pricelists/' + pricelistId + '/pricelistrows?query=' + query, {observe: 'response'});
	}

	/*
	public exportGeneralPricelistPricelistRows(organizationId, pricelistId, query) {
		return this.httpClient.post(this.pathService.getExportGeneralPricelistPricelistRows(organizationId, pricelistId) + query, {}, {
			responseType: 'blob',
			observe: 'response'
		});
	}
*/
	public createGeneralPricelistPricelistRows(organizationId, pricelistId, pricelistRows): Observable<Array<GeneralPricelistPricelistrow>> {
		return this.httpClient.post<Array<GeneralPricelistPricelistrow>>(
			this.pathService.getGeneralPricelists(organizationId) + '/pricelists/' + pricelistId + '/pricelistrows', pricelistRows);
	}

	public updateGeneralPricelistPricelistRow(organizationId, pricelistId, pricelistRowId, pricelistRow): Observable<GeneralPricelistPricelistrow> {
		return this.httpClient.put<GeneralPricelistPricelistrow>(
			this.pathService.getGeneralPricelists(organizationId) + '/pricelists/' + pricelistId + '/pricelistrows/' + pricelistRowId, pricelistRow);
	}

	public deleteGeneralPricelistPriceListRow(organizationId, pricelistId, pricelistRowId) {
		return this.httpClient.delete(
			this.pathService.getGeneralPricelists(organizationId) + '/pricelists/' + pricelistId + '/pricelistrows/' + pricelistRowId);
	}

	public deleteAgreementPriceListRow(organizationId, agreementId, pricelistId, pricelistRowId) {
		return this.httpClient.delete(
			this.pathService.getPricelistRows(organizationId, agreementId, pricelistId) + '/' + pricelistRowId + '/delete');
	}

	public importGeneralPricelistPricelist(organizationId, pricelistId, formData) {
		return this.httpClient.post(this.pathService.importGeneralPricelistPricelist(organizationId, pricelistId), formData);
	}

	public searchArticlesForGeneralPricelistPricelist(query, organizationId, pricelistId): Observable<HttpResponse<Array<SimpleArticle>>> {
		return this.httpClient.get<Array<SimpleArticle>>(this.pathService.getArticlesForGeneralPricelistPricelist(
			organizationId, pricelistId) + query, {observe: 'response'});
	}

}
