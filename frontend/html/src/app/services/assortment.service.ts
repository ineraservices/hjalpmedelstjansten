import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PathService} from './path.service';
import {Assortment} from '../models/assortment/assortment.model';
import {County} from '../models/assortment/county.model';
import {Article} from '../models/product/article.model';
import {ExternalCategoryGrouping} from "../models/product/external-category-grouping.model";

@Injectable()
export class AssortmentService {

	constructor(private httpClient: HttpClient,
							private pathService: PathService) {
	}

	public searchAssortments(organizationId, query): Observable<HttpResponse<Array<Assortment>>> {
		return this.httpClient.get<Array<Assortment>>(this.pathService.getAssortments(organizationId) + query, {observe: 'response'});
	}

	public searchAllAssortments(query): Observable<HttpResponse<Array<Assortment>>> {
		return this.httpClient.get<Array<Assortment>>(this.pathService.getAllAssortments() + query, {observe: 'response'});
	}

	public createAssortment(organizationId, assortment): Observable<Assortment> {
		return this.httpClient.post<Assortment>(this.pathService.getAssortments(organizationId), assortment);
	}

	public updateAssortment(organizationId: number, assortmentId: string | number, assortment: Assortment): Observable<Assortment> {
		return this.httpClient.put<Assortment>(this.pathService.getAssortments(organizationId) + '/' + assortmentId, assortment);
	}

	public getAssortment(organizationId: number, assortmentId: string | number): Observable<Assortment> {
		return this.httpClient.get<Assortment>(this.pathService.getAssortments(organizationId) + '/' + assortmentId);
	}

	public searchArticlesForAssortment(organizationId, assortmentId, query): Observable<HttpResponse<Array<Article>>> {
		return this.httpClient.get<Array<Article>>(
			this.pathService.getAssortmentArticles(organizationId, assortmentId) + query, {observe: 'response'});
	}

	public exportArticlesForAssortment(organizationId, assortmentId, query) {
		return this.httpClient.post(this.pathService.getExportArticlesForAssortment(organizationId, assortmentId) + query, {}, {
			responseType: 'blob',
			observe: 'response'
		});
	}

	public exportAssortments(organizationId, query) {
		return this.httpClient.post(this.pathService.getExportAssortments(organizationId) + query, {}, {
			responseType: 'blob',
			observe: 'response'
		});
	}

	public exportAllAssortments(query) {
		return this.httpClient.post(this.pathService.getExportAllAssortments() + query, {}, {
			responseType: 'blob',
			observe: 'response'
		});
	}

	public addArticlesToAssortment(organizationId, assortmentId, articles) {
		return this.httpClient.post<Assortment>(this.pathService.getAssortmentArticles(organizationId, assortmentId), articles);
	}

  public removeArticleFromAssortment(organizationId, assortmentId, articleId) {
    return this.httpClient.delete<Assortment>(this.pathService.getAssortmentArticlesDelete(organizationId, assortmentId) + articleId);
  }

  public removeAssortment(organizationId, assortmentId) {
    return this.httpClient.delete(this.pathService.deleteAssortment(organizationId, assortmentId));
  }

	public getCounties(): Observable<Array<County>> {
		return this.httpClient.get<Array<County>>(this.pathService.getCounties());
	}

	public getCountiesForOrganization(organizationId: any): Observable<Array<County>> {
		return this.httpClient.get<Array<County>>(this.pathService.getCountiesForOrganization(organizationId));
	}

	public getAssortmentExternalCategories(organizationId: number, assortmentId: number, categoryId: number): Observable<Array<ExternalCategoryGrouping>> {
		return this.httpClient.get<Array<ExternalCategoryGrouping>>(
			this.pathService.getAssortmentExternalCategories(organizationId, assortmentId) + categoryId);
	}

	public getAssortmentAllOtherExternalCategories(organizationId: number, assortmentId: number, categoryId: number): Observable<Array<ExternalCategoryGrouping>> {
		return this.httpClient.get<Array<ExternalCategoryGrouping>>(
			this.pathService.getAssortmentAllOtherExternalCategories(organizationId, assortmentId) + categoryId);
	}

}
