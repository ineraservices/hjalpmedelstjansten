import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PathService} from './path.service';
import {PreventiveMaintenance} from '../models/preventive-maintenance.model';
import {DocumentType} from '../models/document-type.model';

@Injectable()
export class CommonService {

	constructor(private httpClient: HttpClient,
							private pathService: PathService) {
	}


	public getPreventiveMaintenances(): Observable<Array<PreventiveMaintenance>> {
		return this.httpClient.get<Array<PreventiveMaintenance>>(this.pathService.getPreventiveMaintenances());
	}

	public getDocumentTypes(): Observable<Array<DocumentType>> {
		return this.httpClient.get<Array<DocumentType>>(this.pathService.getDocumentTypes());
	}

	isUsingIE() {
		return /* @cc_on!@*/false || !!document['documentMode'];
	}
}
