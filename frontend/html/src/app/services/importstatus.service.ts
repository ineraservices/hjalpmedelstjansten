import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthService} from '../auth/auth.service';
import {PathService} from './path.service';
import {ImportStatus} from "../models/importstatus.model";


@Injectable()
export class ImportStatusService {

	constructor(private httpClient: HttpClient,
							private pathService: PathService,
							private authService: AuthService) {
	}

	public getAllImportsStatus(): Observable<HttpResponse<Array<ImportStatus>>> {
		return this.httpClient.get<Array<ImportStatus>>(this.pathService.getAllImportsStatus(this.authService.getUserId()), {observe: 'response'});
	}

	// public updateImportStatus(changedBy, readStatus): Observable<ImportStatus> {
	//   return this.httpClient.put<ImportStatus>(this.pathService.getImportsStatus(changedBy), readStatus);
	// }

}
