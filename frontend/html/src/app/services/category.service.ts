import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PathService} from './path.service';
import {Article} from '../models/product/article.model';
import {Category} from '../models/product/category.model';
import {ChangeCategory} from "../models/product/changeCategory.model";
import {CatProperty} from "../models/product/cat-property.model";
import {CategoryProperty} from "../models/product/category-property.model";
import {Product} from "../models/product/product.model";
import {ExternalCategoryGrouping} from "../models/product/external-category-grouping.model";


@Injectable()
export class CategoryService {

	constructor(private httpClient: HttpClient,
							private pathService: PathService) {
	}

	public createCategory(category): Observable<Category> {
		return this.httpClient.put<Category>(this.pathService.GetCreateCategory(), category);
	}

	public getCategoriesByCode(code): Observable<Array<Category>> {
		return this.httpClient.get<Array<Category>>(this.pathService.getCategoriesByCode(code));
	}

	public getExternalCategories(): Observable<Array<ExternalCategoryGrouping>> {
		return this.httpClient.get<Array<ExternalCategoryGrouping>>(this.pathService.getExternalCategories());
	}

	public getExternalCategoryChildren(parentId): Observable<Array<ExternalCategoryGrouping>> {
		return this.httpClient.get<Array<ExternalCategoryGrouping>>(this.pathService.getExternalCategoryChildren(parentId));
	}



	public createExternalCategory(category): Observable<ExternalCategoryGrouping> {
		return this.httpClient.put<ExternalCategoryGrouping>(this.pathService.createExternalCategory(), category);
	}

	public deleteExternalCategory(uniqueId): Observable<Boolean> {
		return this.httpClient.delete<Boolean>(this.pathService.deleteExternalCategory(uniqueId));
	}

	public getCategoryByUniqueId(uniqueId): Observable<Array<Category>> {
		return this.httpClient.get<Array<Category>>(this.pathService.getCategoriesById(uniqueId));
	}

	public getAllCategories(): Observable<Array<Category>> {
		return this.httpClient.get<Array<Category>>(this.pathService.getAllCategories());
	}

	public getCategoriesWithCSP(): Observable<Array<Category>> {
		return this.httpClient.get<Array<Category>>(this.pathService.getCategoriesWithCSP());
	}

	public getArticlesByCategory(categoryUniqueId): Observable<Array<Article>> {
		return this.httpClient.get<Array<Article>>(this.pathService.getArticlesByCategory(categoryUniqueId));
	}

	public getProductsByCategory(categoryUniqueId): Observable<Array<Product>> {
		return this.httpClient.get<Array<Product>>(this.pathService.getProductsByCategory(categoryUniqueId));
	}

	public getDeleteCategory(categoryUniqueId): Observable<Boolean> {
		return this.httpClient.delete<Boolean>(this.pathService.getDeleteCategory(categoryUniqueId));
	}

	public changeProductCategory(changeCategory): Observable<ChangeCategory> {
		return this.httpClient.put<ChangeCategory>(this.pathService.getChangeProductCategory(), changeCategory);
	}


	public appCreateCategoryProperty(catProperty): Observable<CatProperty> {
		return this.httpClient.put<CatProperty>(this.pathService.getCreateProperty(), catProperty);
	}

	public DeleteCategoryProperty(propertyId): Observable<Boolean> {
		return this.httpClient.post<Boolean>(this.pathService.getDeleteProperty(), propertyId);
	}

	public DeleteCategoryPropertyListItem(propertyId): Observable<Boolean> {
		return this.httpClient.post<Boolean>(this.pathService.getDeletePropertyListItem(), propertyId);
	}

	public CreateCategoryPropertyListItem(categoryValue): Observable<number> {
		return this.httpClient.put<number>(this.pathService.getCreatePropertyListItem(), categoryValue);
	}

	public UpdateCategoryProperty(categoryProperty): Observable<CategoryProperty> {
		return this.httpClient.post<CategoryProperty>(this.pathService.getUpdateProperty(), categoryProperty);
	}

	public UpdateCategoryPropertyOrder(property): Observable<CategoryProperty> {
		return this.httpClient.post<CategoryProperty>(this.pathService.getUpdatePropertyListOrder(), property);
	}

	public getExportFileIso(value) {
		return this.httpClient.post(this.pathService.getExportFileIso(value), {}, {
			responseType: 'blob',
			observe: 'response'
		});
	}

	public getExportFileIsoCategory(value) {
		return this.httpClient.post(this.pathService.getExportFileIsoCategory(value), {}, {
			responseType: 'blob',
			observe: 'response'
		});
	}


}
