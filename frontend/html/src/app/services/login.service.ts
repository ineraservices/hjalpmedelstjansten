import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {PathService} from './path.service';
import {Login} from '../models/login.model';

@Injectable()
export class LoginService {

	constructor(private httpClient: HttpClient,
							private pathService: PathService) {
	}


	public login(login): Observable<Object> {
		return this.httpClient.post<Login>(this.pathService.getLogin(), login);
	}

	public logout(): Observable<Object> {
		return this.httpClient.post(this.pathService.getLogout(), {});
	}

	public validate(login): Observable<Object> {
		return this.httpClient.post<Login>(this.pathService.getValidate(), login);
	}

	public changePassword(login): Observable<Object> {
		return this.httpClient.post<Login>(this.pathService.getChangePassword(), login);
	}

	public requestPassword(login): Observable<Object> {
		return this.httpClient.post<Login>(this.pathService.getRequestPassword(), login);
	}

	public welcomeAll(): Observable<Object> {
		return this.httpClient.post(this.pathService.getWelcomeAll(), {});
	}
}
