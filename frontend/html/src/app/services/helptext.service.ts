import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PathService} from './path.service';
import {Observable} from 'rxjs/index';
import {debug, RxJsLoggingLevel} from '../util/log-debug-rxjs';

@Injectable()
export class HelptextService {
	constructor(private httpClient: HttpClient,
							private pathService: PathService) {
	}

	public getTexts(): Observable<Array<any>> {
		return this.httpClient.get<Array<any>>(this.pathService.getTexts())
			.pipe(debug(RxJsLoggingLevel.TRACE, 'helpTextService:trace:getTexts()'));
	}

}
