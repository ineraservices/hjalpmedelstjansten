import {Injectable} from '@angular/core';

import {environment} from '../../environments/environment.prod';

@Injectable()
export class PathService {

	private readonly envPath: string = environment.path + '/hjmtj/resources/v1';
	private readonly profile: string = this.envPath + '/profile';
	private readonly texts: string = this.envPath + '/texts';
	private readonly notifications: string = this.envPath + '/organizations/:organizationId/users/:userId/notifications';

	private readonly createCategory: string = environment.path + '/hjmtj/resources/v1/categories/create';

	private readonly createUpdateExternalCategory: string = environment.path + '/hjmtj/resources/v1/categories/createExternalCategory';

	private readonly allCategories: string = environment.path + '/hjmtj/resources/v1/categories/allcategories';
	private readonly categoriesWithCSP: string = environment.path + '/hjmtj/resources/v1/categories/categoriesWithCSP';

	private readonly articlesByCategory: string = environment.path + '/hjmtj/resources/v1/categories/allcategories/:categoryUniqueId/articles';
	private readonly productsByCategory: string = environment.path + '/hjmtj/resources/v1/categories/allcategories/:categoryUniqueId/products';
	private readonly exportFileIso: string = environment.path + '/hjmtj/resources/v1/categories/exportCategories/:selected';
	private readonly exportFileIsoCategory: string = environment.path + '/hjmtj/resources/v1/categories/:selected/exportCategory/';
	private readonly changeProductCategory: string = environment.path + '/hjmtj/resources/v1/categories/changeProductCategory';
	private readonly createPropertyListItem: string = environment.path + '/hjmtj/resources/v1/categories/CreatePropertyListItem';
	private readonly deletePropertyListItem: string = environment.path + '/hjmtj/resources/v1/categories/DeletePropertyItem';
	private readonly createProperty: string = environment.path + '/hjmtj/resources/v1/categories/CreateProperty';
	private readonly deleteProperty: string = environment.path + '/hjmtj/resources/v1/categories/DeleteProperty';
	private readonly updateProperty: string = environment.path + '/hjmtj/resources/v1/categories/UpdateProperty';
	private readonly updatePropertyListOrder: string = environment.path + '/hjmtj/resources/v1/categories/UpdatePropertyListOrder';

	private readonly login: string = environment.path + '/hjmtj/resources/v1/login';
	private readonly logout: string = environment.path + '/hjmtj/resources/v1/logout';
	private readonly dynamicText: string = environment.path + '/hjmtj/resources/v1/dynamicText/:uniqueId';
	private readonly importStatus: string = environment.path + '/hjmtj/resources/v1/importStatus/:userId';
	private readonly validate: string = environment.path + '/hjmtj/resources/v1/reset/validate';
	private readonly changePassword: string = environment.path + 'hjmtj/resources/v1/reset/change';
	private readonly requestPassword: string = environment.path + 'hjmtj/resources/v1/reset/request';
	private readonly welcomeAll: string = environment.path + 'hjmtj/resources/v1/reset/welcomeall';
	private readonly country: string = environment.path + '/hjmtj/resources/v1/country';
	private readonly search: string = environment.path + '/hjmtj/resources/v1/search?query=';
	private readonly businessLevel: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/businesslevels';
	private readonly role: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/roles';
	private readonly searchOrganization: string = environment.path + '/hjmtj/resources/v1/organizations?query=';
	private readonly searchValidOrganization: string = environment.path + '/hjmtj/resources/v1/organizations/valid?query=';
	private readonly exportOrganization: string = environment.path + '/hjmtj/resources/v1/organizations/export';
	private readonly organization: string = environment.path + '/hjmtj/resources/v1/organizations/';
	private readonly searchUser: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/users?query=';
	private readonly exportUser: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/users/:onlyActives/export';
	private readonly user: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/users/';
	private readonly userByRole: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/users/roles?roleName=';
	private readonly pricelistApprover: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/pricelistapprovers?query=';
	private readonly userByBusinessLevel: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/users/businesslevels?query=:query&businessLevel=';
	private readonly searchProductAndArticle: string = environment.path + '/hjmtj/resources/v1/search/organizations/:organizationId?query=';
	private readonly exportProductAndArticle: string = environment.path + '/hjmtj/resources/v1/search/organizations/:organizationId/export';
	private readonly exportProductsAndArticlesForArticle: string = environment.path + '/hjmtj/resources/v1/search/organizations/:organizationId/articles/:articleId/export';
	private readonly exportArticlesForProduct: string = environment.path + '/hjmtj/resources/v1/search/organizations/:organizationId/products/:productId/export';
	private readonly exportArticlesForAssortment: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/assortments/:assortmentId/export/';
	private readonly exportGeneralPricelistPricelistRows: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/generalpricelists/pricelists/:pricelistId/pricelistrows/export';
	private readonly exportSearchResult: string = environment.path + '/hjmtj/resources/v1/search/export';
	private readonly searchProduct: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/products?query=';
	private readonly getArticles: string = environment.path + '/hjmtj/resources/v1/search/organizations/:organizationId/articles?query=';
	private readonly product: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/products/';
	private readonly productByQuery: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/products?';
	private readonly category: string = environment.path + '/hjmtj/resources/v1/categories';
	private readonly searchCategory: string = environment.path + '/hjmtj/resources/v1/categories?query=';
	private readonly childCategory: string = environment.path + '/hjmtj/resources/v1/categories?code=';
	private readonly CategoriesByCode: string = environment.path + '/hjmtj/resources/v1/categories/:code';

  private readonly categoryByUniqueId: string = environment.path + '/hjmtj/resources/v1/categories/getCategoryById/:uniqueId';
  private readonly deleteCategory: string = environment.path + '/hjmtj/resources/v1/categories/:categoryUniqueId/delete';
  private readonly externalCategories: string = environment.path + '/hjmtj/resources/v1/categories/getExternalCategories';
  private readonly externalCategoryChildren: string = environment.path + '/hjmtj/resources/v1/categories/getExternalCategories/:parentId';
	private readonly externalCategoryToCategoryId: string = environment.path + '/hjmtj/resources/v1/categories/getExternalCategories/:categoryId/categories';
  private readonly getDeleteExternalCategory: string = environment.path + '/hjmtj/resources/v1/categories/deleteExternalCategory/:uniqueId';
  private readonly ce: string = environment.path + '/hjmtj/resources/v1/ce';
  private readonly orderUnit: string = environment.path + '/hjmtj/resources/v1/orderunits';
  private readonly guaranteeUnit: string = environment.path + '/hjmtj/resources/v1/guaranteeunits';
  private readonly packageUnit: string = environment.path + '/hjmtj/resources/v1/packageunits';
  private readonly categoryProperty: string = environment.path + '/hjmtj/resources/v1/categories/:categoryId/properties';
  private readonly preventiveMaintenance: string = environment.path + '/hjmtj/resources/v1/preventivemaintenances';
  private readonly documentType: string = environment.path + '/hjmtj/resources/v1/documenttypes';
  private readonly searchArticle: string = environment.path + '/hjmtj/resources/v1/search/organizations/:organizationId/products/:productId?query=';
  private readonly article: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/articles/';
  private readonly categoryForCodelessArticle: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/articles/:articleId/getCategoryCodelessArticle/';
  private readonly searchConnectedArticle: string = environment.path + '/hjmtj/resources/v1/search/organizations/:organizationId/articles/:articleId?query=';
  private readonly connection: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/articles/:articleId/fitsto/';
  private readonly agreement: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements';
  private readonly deleteAgreementPath: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/:agreementId/delete';
  private readonly adminViewAgreement: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/adminViewAgreements/';
  private readonly agreementExport: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/export';
  private readonly pricelist: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/:agreementId/pricelists';
  private readonly pricelistrow: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/:agreementId/pricelists/:pricelistId/pricelistrows';
  private readonly allPricelistrow: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/:agreementId/pricelists/:pricelistId/pricelistrows/all';
  private readonly getPricelistRowsByApprovementIdPath: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/:agreementId/pricelists/:pricelistId/pricelistrows/approvementId/:approvementId';
  private readonly pricelistArticle: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/:agreementId/pricelists/:pricelistId/articles?query=';
  private readonly pricelistExport: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/:agreementId/pricelists/:pricelistId/exportimport/export';
  private readonly pricelistImport: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/:agreementId/pricelists/:pricelistId/exportimport/import';
  private readonly pricelistrowExport: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/:agreementId/pricelists/:pricelistId/pricelistrows/export';
  private readonly generalPricelistPricelistExport: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/generalpricelists/pricelists/:pricelistId/exportimport/export/active';
  private readonly generalPricelistPricelistExportAll: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/generalpricelists/pricelists/:pricelistId/exportimport/export/all';
  private readonly generalPricelistPricelistExportDiscontinued: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/generalpricelists/pricelists/:pricelistId/exportimport/export/inactive';
  private readonly generalPricelist: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/generalpricelists';
  private readonly allGP: string = environment.path + '/hjmtj/resources/v1/generalpricelists?query=';
  private readonly exportSetting: string = environment.path + '/hjmtj/resources/v1/exportsettings/';
  private readonly queueExportPath: string = environment.path + '/hjmtj/resources/v1/exportsettings/queue/';
  private readonly exportSettingForOrganization: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/exportsettings/';
  private readonly exportGP: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/exportsettings/:exportSettingsUniqueId/export/';
  private readonly generalPricelistPricelistImport: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/generalpricelists/pricelists/:pricelistId/exportimport/import';
  private readonly generalPricelistPricelistArticle: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/generalpricelists/pricelists/:pricelistId/articles?query=';
  private readonly assortment: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/assortments';
  private readonly deleteAssortmentPath: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/assortments/:assortmentId/delete';
  private readonly exportAssortment: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/assortments/export';
  private readonly assortmentArticleDelete: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/assortments/:assortmentId/articles/';
  private readonly assortmentArticle: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/assortments/:assortmentId/articles?query=';
  private readonly allAssortments: string = environment.path + '/hjmtj/resources/v1/assortments';
  private readonly exportAllAssortments: string = environment.path + '/hjmtj/resources/v1/assortments/export';
  private readonly county: string = environment.path + '/hjmtj/resources/v1/counties';
	private readonly countyForOrganization: string = environment.path + '/hjmtj/resources/v1/counties/organization/:organizationId';
	private readonly assortmentExternalCategories: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/assortments/:assortmentId/externalCategories?query=';
	private readonly assortmentAllOtherExternalCategories: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/assortments/:assortmentId/allOtherexternalCategories?query=';
  private readonly allUnselectedGP: string = environment.path + '/hjmtj/resources/v1/generalpricelists/organization/:organizationUniqueId/exportsettings/:exportSettingsUniqueId?query=';
  private readonly pricelistsForDashboard: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/getPricelistsForDashboard';
  private readonly pricelistsForDashboardDeclined: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/getPricelistsForDashboardDeclined';
  private readonly pricelistsWaitingForApprovalSupplier: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/getPricelistsWaitingForApprovalSupplier';
  private readonly pricelistsFilteredBy: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/getPricelistsFilteredBy/:filter';
  private readonly agreementsExtensionReminder: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/getAgreementsToExtend';
  private readonly articlesChanged: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/getArticlesChanged';
  private readonly removeArticleChangedNotice: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/removeArticleChangedNotice/:articleChangedId/:userId/:removeAll';
	private readonly articlesDiscontinued: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/assortments/getArticlesDiscontinued';
	private readonly removeArticleDiscontinuedNotice: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/assortments/removeArticleDiscontinuedNotice/:articleDiscontinuedId/:userId/:assortmentId/:remove';
  private readonly agreementsChangedByCustomer: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/getAgreementsChangedByCustomer';
  private readonly removeAgreementChangedNoticePath: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/removeAgreementChangedNotice/:agreementChangedId';
  private readonly pricelistRowsApprovedByCustomer: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/getPricelistRowsApprovedByCustomer';
  private readonly removeApprovedPricelistRowNoticePath: string = environment.path + '/hjmtj/resources/v1/organizations/:organizationId/agreements/removeApprovedPricelistRowNotice/:approvementId';


	public getChangeProductCategory(): string {
		return this.changeProductCategory;
	}

	public getCreateProperty(): string {
		return this.createProperty;
	}


	public getUpdateProperty(): string {
		return this.updateProperty;
	}

	public getDeleteProperty(): string {
		return this.deleteProperty;
	}

	public getUpdatePropertyListOrder(): string {
		return this.updatePropertyListOrder;
	}


	public getCreatePropertyListItem(): string {
		return this.createPropertyListItem;
	}

	public getDeletePropertyListItem(): string {
		return this.deletePropertyListItem;
	}

	public GetCreateCategory(): string {
		return this.createCategory;
	}

	public createExternalCategory(): string {
		return this.createUpdateExternalCategory;
	}

	public getAllCategories(): string {
		return this.allCategories;
	}

	public getCategoriesWithCSP(): string {
		return this.categoriesWithCSP;
	}

	public getArticlesByCategory(categoryUniqueId): string {
		return this.articlesByCategory.replace(':categoryUniqueId', categoryUniqueId);
	}

	public getProductsByCategory(categoryUniqueId): string {
		return this.productsByCategory.replace(':categoryUniqueId', categoryUniqueId);
	}

	public getExportFileIso(value): string {
		return this.exportFileIso.replace(':selected', value);
	}

	public getExportFileIsoCategory(value): string {
		return this.exportFileIsoCategory.replace(':selected', value);
	}

	public getExternalCategories(): string {
		return this.externalCategories;
	}

	public getExternalCategoryChildren(parentId): string {
		return this.externalCategoryChildren.replace(':parentId', parentId);
	}

	public getExternalCategoryToCategoryId(categoryId): string {
		return this.externalCategoryToCategoryId.replace(':categoryId', categoryId);
	}

	public deleteExternalCategory(uniqueId): string {
		return this.getDeleteExternalCategory.replace(':uniqueId', uniqueId);
	}

	public getLogin(): string {
		return this.login;
	}

	public getCategoriesByCode(code): string {
		return this.CategoriesByCode.replace(':code', code);
	}

	public getCategoriesById(uniqueId): string {
		return this.categoryByUniqueId.replace(':uniqueId', uniqueId);
	}

	public getDeleteCategory(categoryUniqueId): string {
		return this.deleteCategory.replace(':categoryUniqueId', categoryUniqueId);
	}

	public getLogout(): string {
		return this.logout;
	}

	public getTexts(): string {
		return this.texts;
	}

	public getDynamicText(uniqueId): string {
		return this.dynamicText.replace(':uniqueId', uniqueId);
	}

	public getAllImportsStatus(userId): string {
		return this.importStatus.replace(':userId', userId);
	}

	public getValidate(): string {
		return this.validate;
	}

	public getChangePassword(): string {
		return this.changePassword;
	}

	public getRequestPassword(): string {
		return this.requestPassword;
	}

	public getWelcomeAll(): string {
		return this.welcomeAll;
	}

	public getCountries(): string {
		return this.country;
	}

	public getSearch(): string {
		return this.search;
	}

	public getBusinessLevels(organizationId): string {
		return this.businessLevel.replace(':organizationId', organizationId);
	}

	/*
	public updateBusinessLevelName(organizationId): string {
		return this.businessLevel.replace(':organizationId', organizationId);
	}
*/
	public getRoles(organizationId): string {
		return this.role.replace(':organizationId', organizationId);
	}

	public searchOrganizations(): string {
		return this.searchOrganization;
	}

	public searchValidOrganizations(): string {
		return this.searchValidOrganization;
	}

	public getExportOrganizations(): string {
		return this.exportOrganization;
	}

  public getExportUsers(onlyActives, organizationId): string {
    return this.exportUser.replace(':organizationId', organizationId).replace(':onlyActives', onlyActives);
  }

	public getOrganization(): string {
		return this.organization;
	}

	public searchUsers(organizationId): string {
		return this.searchUser.replace(':organizationId', organizationId);
	}

	public getUser(organizationId): string {
		return this.user.replace(':organizationId', organizationId);
	}

	public getUsersByRole(organizationId): string {
		return this.userByRole.replace(':organizationId', organizationId);
	}

	public getPricelistApprovers(organizationId): string {
		return this.pricelistApprover.replace(':organizationId', organizationId);
	}

	public getUsersByBusinessLevel(query, organizationId): string {
		return this.userByBusinessLevel.replace(':organizationId', organizationId).replace(':query', query);
	}

	public searchProductsAndArticles(organizationId): string {
		return this.searchProductAndArticle.replace(':organizationId', organizationId);
	}

	public getExportProductsAndArticles(organizationId): string {
		return this.exportProductAndArticle.replace(':organizationId', organizationId);
	}

  public getExportGPs(organizationId, exportSettingsUniqueId): string {
    return this.exportGP.replace(':organizationId', organizationId).replace(':exportSettingsUniqueId', exportSettingsUniqueId);
  }

	public getExportProductsAndArticlesForArticle(organizationId, articleId): string {
		return this.exportProductsAndArticlesForArticle.replace(':organizationId', organizationId).replace(':articleId', articleId);
	}

	public getExportArticlesForProduct(organizationId, productId): string {
		return this.exportArticlesForProduct.replace(':organizationId', organizationId).replace(':productId', productId);
	}

	public getExportArticlesForAssortment(organizationId, assortmentId): string {
		return this.exportArticlesForAssortment.replace(':organizationId', organizationId).replace(':assortmentId', assortmentId);
	}

	public getExportGeneralPricelistPricelistRows(organizationId, pricelistId): string {
		return this.exportGeneralPricelistPricelistRows.replace(':organizationId', organizationId).replace(':pricelistId', pricelistId);
	}

	public getExportSearchResults(): string {
		return this.exportSearchResult;
	}

	public searchProducts(organizationId): string {
		return this.searchProduct.replace(':organizationId', organizationId);
	}

	public getArticlesByOrganization(organizationId): string {
		return this.getArticles.replace(':organizationId', organizationId);
	}

	public getProduct(organizationId): string {
		return this.product.replace(':organizationId', organizationId);
	}

	public getProductsByQuery(organizationId): string {
		return this.productByQuery.replace(':organizationId', organizationId);
	}

	public getProfile(): string {
		return this.profile;
	}


	public notificationTypes(organizationId: string, userId: string) {
		return this.notifications
			.replace(':organizationId', organizationId)
			.replace(':userId', userId);
	}

	public getCategories() {
		return this.category;
	}

	public searchCategories() {
		return this.searchCategory;
	}

	public getChildCategories() {
		return this.childCategory;
	}

	public getCE() {
		return this.ce;
	}

	public getOrderUnits() {
		return this.orderUnit;
	}

	public getGuaranteeUnits() {
		return this.guaranteeUnit;
	}

	public getPackageUnits() {
		return this.packageUnit;
	}

	public getCategoryProperties(categoryId): string {
		return this.categoryProperty.replace(':categoryId', categoryId);
	}

	public getPreventiveMaintenances() {
		return this.preventiveMaintenance;
	}

	public getDocumentTypes() {
		return this.documentType;
	}

	public searchArticles(organizationId, productId): string {
		return this.searchArticle.replace(':organizationId', organizationId).replace(':productId', productId);
	}

	public getArticle(organizationId): string {
		return this.article.replace(':organizationId', organizationId);
	}

	public searchConnectedArticles(organizationId, articleId): string {
		return this.searchConnectedArticle.replace(':organizationId', organizationId).replace(':articleId', articleId);
	}

	public removeConnection(organizationId, articleId): string {
		return this.connection.replace(':organizationId', organizationId).replace(':articleId', articleId);
	}

	public getAgreements(organizationId): string {
		return this.agreement.replace(':organizationId', organizationId);
	}

	public deleteAgreement(organizationId, agreementId): string {
		return this.deleteAgreementPath.replace(':organizationId', organizationId).replace(':agreementId', agreementId);
	}

	public getAdminViewAgreements(organizationId): string {
		return this.adminViewAgreement.replace(':organizationId', organizationId);
	}

	public getPricelist(organizationId, agreementId): string {
		return this.pricelist.replace(':organizationId', organizationId).replace(':agreementId', agreementId);
	}

	public getPricelistsFilteredBy(organizationId, filter): string {
		return this.pricelistsFilteredBy.replace(':organizationId', organizationId).replace(':filter', filter);
	}

	public getAgreementsToExtend(organizationId): string {
		return this.agreementsExtensionReminder.replace(':organizationId', organizationId);
	}

	public getArticlesChanged(organizationId): string {
		return this.articlesChanged.replace(':organizationId', organizationId);
	}

	public removeArticlesChangedNotice(organizationId, articleId, userId, removeAll): string {
		return this.removeArticleChangedNotice.replace(':organizationId', organizationId).replace(':articleChangedId', articleId).replace(':userId', userId).replace(':removeAll', removeAll);
	}

	public getArticlesDiscontinued(organizationId: string): string {
		return this.articlesDiscontinued.replace(':organizationId', organizationId);
	}

	public removeArticlesDiscontinuedNotice(organizationId, articleId, userId, assortmentId, remove): string {
		return this.removeArticleDiscontinuedNotice.replace(':organizationId', organizationId).replace(':articleDiscontinuedId', articleId).replace(':userId', userId).replace(':assortmentId', assortmentId).replace(':remove', remove);
	}

	public getAgreementsChangedByCustomer(organizationId): string {
		return this.agreementsChangedByCustomer.replace(':organizationId', organizationId);
	}

	public getPricelistRowsApprovedByCustomer(organizationId): string {
		return this.pricelistRowsApprovedByCustomer.replace(':organizationId', organizationId);
	}

	public removeAgreementChangedNotice(organizationId, agreementChangedId): string {
		return this.removeAgreementChangedNoticePath.replace(':organizationId', organizationId).replace(':agreementChangedId', agreementChangedId);
	}

	public removeApprovedPricelistRowNotice(organizationId, approvementId): string {
		return this.removeApprovedPricelistRowNoticePath.replace(':organizationId', organizationId).replace(':approvementId', approvementId);
	}

	public getPricelistRowsByApprovementId(organizationId, agreementId, pricelistId, approvementId): string {
		return this.getPricelistRowsByApprovementIdPath.replace(':organizationId', organizationId).replace(':agreementId', agreementId).replace(':pricelistId', pricelistId).replace(':approvementId', approvementId);
	}

	public getPricelistRows(organizationId, agreementId, pricelistId): string {
		return this.pricelistrow.replace(':organizationId', organizationId).replace(':agreementId', agreementId).replace(':pricelistId', pricelistId);
	}

  public getAllPricelistRows(organizationId, agreementId, pricelistId): string {
    return this.allPricelistrow.replace(':organizationId', organizationId).replace(':agreementId', agreementId).replace(':pricelistId', pricelistId);
  }

  public getArticlesForPricelist(organizationId, agreementId, pricelistId): string {
    return this.pricelistArticle.replace(':organizationId', organizationId).replace(':agreementId', agreementId).replace(':pricelistId', pricelistId);
  }

	public exportPricelist(organizationId, agreementId, pricelistId): string {
		return this.pricelistExport.replace(':organizationId', organizationId).replace(':agreementId', agreementId).replace(':pricelistId', pricelistId);
	}

	public importPricelist(organizationId, agreementId, pricelistId): string {
		return this.pricelistImport.replace(':organizationId', organizationId).replace(':agreementId', agreementId).replace(':pricelistId', pricelistId);
	}

	public exportPricelistRows(organizationId, agreementId, pricelistId): string {
		return this.pricelistrowExport.replace(':organizationId', organizationId).replace(':agreementId', agreementId).replace(':pricelistId', pricelistId);
	}

	public exportAgreements(organizationId): string {
		return this.agreementExport.replace(':organizationId', organizationId);
	}

	public exportGeneralPricelistPricelistAll(organizationId, pricelistId): string {
		return this.generalPricelistPricelistExportAll.replace(':organizationId', organizationId).replace(':pricelistId', pricelistId);
	}

	public exportGeneralPricelistPricelist(organizationId, pricelistId): string {
		return this.generalPricelistPricelistExport.replace(':organizationId', organizationId).replace(':pricelistId', pricelistId);
	}

	public exportGeneralPricelistPricelistDiscontinued(organizationId, pricelistId): string {
		return this.generalPricelistPricelistExportDiscontinued.replace(':organizationId', organizationId).replace(':pricelistId', pricelistId);
	}

	public getGeneralPricelists(organizationId): string {
		return this.generalPricelist.replace(':organizationId', organizationId);
	}

	public getAllGP(): string {
		return this.allGP;
	}

	public getAllUnselectedGP(organizationId, exportSettingsId): string {
		return this.allUnselectedGP.replace(':organizationUniqueId', organizationId).replace(':exportSettingsUniqueId', exportSettingsId);
	}

	public getExportSettings(): string {
		return this.exportSetting;
	}

	public queueExport(): string {
		return this.queueExportPath;
	}


	public getExportSettingsForOrganization(organizationId): string {
		return this.exportSettingForOrganization.replace(':organizationId', organizationId);
	}

	public importGeneralPricelistPricelist(organizationId, pricelistId): string {
		return this.generalPricelistPricelistImport.replace(':organizationId', organizationId).replace(':pricelistId', pricelistId);
	}

	public getArticlesForGeneralPricelistPricelist(organizationId, pricelistId): string {
		return this.generalPricelistPricelistArticle.replace(':organizationId', organizationId).replace(':pricelistId', pricelistId);
	}

	public getAssortments(organizationId): string {
		return this.assortment.replace(':organizationId', organizationId);
	}

  public deleteAssortment(organizationId, assortmentId): string {
    return this.deleteAssortmentPath.replace(':organizationId', organizationId).replace(':assortmentId', assortmentId);
  }

  public getExportAssortments(organizationId): string {
    return this.exportAssortment.replace(':organizationId', organizationId);
  }

	public getAssortmentArticles(organizationId: string, assortmentId: string): string {
		return this.assortmentArticle.replace(':organizationId', organizationId).replace(':assortmentId', assortmentId);
	}

	public getAssortmentArticlesDelete(organizationId, assortmentId): string {
    return this.assortmentArticleDelete.replace(':organizationId', organizationId).replace(':assortmentId', assortmentId);
  }

  public getAllAssortments(): string {
    return this.allAssortments;
  }

	public getExportAllAssortments(): string {
		return this.exportAllAssortments;
	}

	public getCounties(): string {
		return this.county;
	}
	public getCountiesForOrganization(organizationId: any): string {
		return this.countyForOrganization.replace(':organizationId', organizationId);
	}


	public getAssortmentExternalCategories(organizationId: any, assortmentId: any): string {
		return this.assortmentExternalCategories.replace(':organizationId', organizationId).replace(':assortmentId', assortmentId);
	}

	public getAssortmentAllOtherExternalCategories(organizationId: any, assortmentId: any): string {
		return this.assortmentAllOtherExternalCategories.replace(':organizationId', organizationId).replace(':assortmentId', assortmentId);
	}

}
