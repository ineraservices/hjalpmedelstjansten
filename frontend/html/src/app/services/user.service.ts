import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PathService} from './path.service';
import {User} from '../models/user/user.model';
import {debug, RxJsLoggingLevel} from '../util/log-debug-rxjs';
import {NotificationObject} from '../modules/profile/profile.controller';

@Injectable()
export class UserService {

	constructor(private httpClient: HttpClient,
							private pathService: PathService) {
	}

	public searchUsers(organizationId, query): Observable<HttpResponse<Array<User>>> {
		return this.httpClient.get<Array<User>>(this.pathService.searchUsers(organizationId) + query, {observe: 'response'});
	}

	public exportUsers(onlyActives, organizationId, query) {
		return this.httpClient.post(this.pathService.getExportUsers(onlyActives, organizationId) + query, {}, {
			responseType: 'blob',
			observe: 'response'
		});
	}

	public getUser(organizationId, userId): Observable<User> {
		return this.httpClient.get<User>(this.pathService.getUser(organizationId) + userId);
	}

	public updateUser(organizationId, userId, user): Observable<User> {
		return this.httpClient.put<User>(this.pathService.getUser(organizationId) + userId, user);
	}

	public createUser(organizationId, user): Observable<User> {
		return this.httpClient.post<User>(this.pathService.getUser(organizationId), user);
	}

	/* public deleteUser(organizationId, userId): Observable<Object> {
	 return this.httpClient.delete(this.pathService.getUser(organizationId) + userId);
 }
*/
	public deleteUser(organizationId, userId, reasonCode): Observable<String> {
		return this.httpClient.post<String>(this.pathService.getUser(organizationId) + userId, reasonCode);
	}


	public getProfile(): Observable<User> {
		return this.httpClient.get<User>(this.pathService.getProfile());
	}

	public getUserByRoles(organizationId, roles): Observable<Array<User>> {
		return this.httpClient.get<Array<User>>(this.pathService.getUsersByRole(organizationId) + roles);
	}

	public searchUsersByBusinessLevel(query, organizationId, businessLevel): Observable<HttpResponse<Array<User>>> {
		return this.httpClient.get<Array<User>>(
			this.pathService.getUsersByBusinessLevel(query, organizationId) + businessLevel, {observe: 'response'});
	}

	public searchPricelistApprovers(organizationId, query): Observable<HttpResponse<Array<User>>> {
		return this.httpClient.get<Array<User>>(this.pathService.getPricelistApprovers(organizationId) + query, {observe: 'response'});
	}

	/**
	 * @param organizationId organizationId string
	 * @param userId userId string
	 */
	public getNotificationTypes(organizationId: string, userId: string): Observable<NotificationObject> {
		return this.httpClient.get<NotificationObject>(this.pathService.notificationTypes(organizationId, userId))
			.pipe(debug(RxJsLoggingLevel.TRACE, 'userService:trace:getNotificationTypes'));
	}

	/**
	 * @param organizationId string
	 * @param userId userif string
	 * @param userNotifications Array of NotificationType-strings.
	 */
	public updateNotificationTypes(organizationId: string, userId: string, userNotifications: Array<string>): Observable<NotificationObject> {
		return this.httpClient.put<Array<string>>(this.pathService.notificationTypes(organizationId, userId), userNotifications)
			.pipe(debug(RxJsLoggingLevel.TRACE, 'userService:trace:updateNotificationTypes'));
	}

}
