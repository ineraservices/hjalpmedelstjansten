export class StorageHelper {
	readonly page: string;
	readonly spacer: string = '.';
	readonly storageName: string;
	readonly browserStorageSupport: boolean;
	private storageInternal: Storage;

	/**
	 *
	 * @param page Name of the page the Storage class will work on. The name is then used as index to manage that particular page's Storage.
	 * @param storageType What kind of storage, takes 'session' and 'local'. Defaults to session.
	 *
	 */

	constructor(page: string, storageType: string = 'session') {
		this.page = page;
		this.storageName = page + this.spacer;
		this.browserStorageSupport = this.browserSupport();
		this.setLocalOrSessionStorage(storageType);
	}

	public setItem(key: string, value: string): void {
		if (this.browserStorageSupport) {
			if (key != null || value != null || key !== 'null' || value !== 'null') {
				this.storageInternal.setItem(this.storageName + key, value);
			}
		}
	}

	public getItemString(key: string): string {
		if (this.browserStorageSupport) {
			const query = this.storageName + key;
			const response = this.storageInternal.getItem(query);
			if (response != null || response !== 'null') {
				return response;
			} else {
				return '';
			}
		}
	}

	public getItemBoolean(key: string): boolean {
		if (this.browserStorageSupport) {
			const query = this.storageName + key;
			const response = this.stringToBoolean(this.storageInternal.getItem(query));
			if (response != null) {
				return response;
			}
		}
	}

	public getItemNumber(key: string): number {
		if (this.browserStorageSupport) {
			const query = this.storageName + key;
			const response = Number(this.storageInternal.getItem(query));
			if (response != null) {
				return response;
			}
		}
	}

	public getItemJSON(key: string): any {
		if (this.browserStorageSupport) {
			const query = this.storageName + key;
			const response = JSON.parse(this.storageInternal.getItem(query));
			if (response != null || response !== 'null') {
				return response;
			}
		}
	}

	public hasPageBeenVisisted(): boolean {
		if (this.browserStorageSupport) {
			return this.checkStorageStatusForThisType();
		} else {
			return false;
		}
	}

	/**
	 *
	 * Unfortunatly there's no way to make this work without double ittirations of the remove function. The session/ localStorage library tosses
	 * it's index around at random points (sub-optimization i guess), which causes certain item's to get skipped. This works but it's a bit
	 * slower than just doing it once.
	 *
	 */
	public removeAllStorageEntriesOfType() {
		if (this.browserStorageSupport) {
			this.itterateAndRemove();
			this.itterateAndRemove();

		}
		location.reload();
	}

	public removeAllStorageEntriesOfType2() {
		if (this.browserStorageSupport) {
			this.itterateAndRemove();
			this.itterateAndRemove();

		}
		/* location.reload();*/
	}

	public removeAllStorageEntries() {
		this.storageInternal.clear();
	}

	private itterateAndRemove() {
		let i = this.storageInternal.length - 1;
		while (i >= 0) {

			const subStringEnd: number = this.storageInternal.key(i).indexOf(this.spacer);
			const storageType: string = this.storageInternal.key(i).substring(0, subStringEnd);
			const storedEntity: string = this.storageInternal.key(i);

			if (storageType === this.page) {
				this.storageInternal.removeItem(storedEntity);
			}

			i--;
		}
	}

	private setLocalOrSessionStorage(storageType: string) {

		switch (storageType.toLowerCase()) {
		case 'session':
			this.storageInternal = sessionStorage;
			break;
		case 'local':
			this.storageInternal = localStorage;
			break;
		default:
			this.storageInternal = sessionStorage;
		}
	}

	private browserSupport(): boolean {

		return typeof (Storage) !== 'undefined';
	}

	private stringToBoolean(stringedBoolean: string): boolean {
		switch (stringedBoolean) {
		case 'True':
		case 'true':
		case '1':
			return true;

		case 'False':
		case 'false':
		case '0':
			return false;
		default:
			return false;
		}
	}

	private checkStorageStatusForThisType(): boolean {
		if (this.storageInternal.length !== 0) {
			for (let i = 0; i < this.storageInternal.length; i++) {
				const subStringEnd: number = this.storageInternal.key(i).indexOf(this.spacer);
				const storageType: string = this.storageInternal.key(i).substring(0, subStringEnd);

				if (storageType === this.page) {
					return true;
				} else if (i === this.storageInternal.length) {
					return false;
				}
			}

		} else {
			return false;
		}
	}

}

