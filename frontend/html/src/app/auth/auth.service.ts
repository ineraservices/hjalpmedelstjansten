import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {share} from 'rxjs/operators';

@Injectable()
export class AuthService {
	isLoginSubject = new BehaviorSubject<boolean>(this.hasToken());


	/**
	 *
	 * @returns {Observable<T>}
	 */
	isLoggedIn(): Observable<boolean> {
		return this.isLoginSubject.asObservable().pipe(share());
	}

	hasPermission(permission): boolean {
		let isPermitted = false;
		JSON.parse(localStorage.getItem('token'))?.userEngagements[0].roles.forEach((role) => {
			role.permissions.forEach((rolePermission) => {
				if (rolePermission.name === permission) {
					isPermitted = true;
				}
			});
		});
		return isPermitted;
	}

	isSuperAdmin(): boolean {
		return JSON.parse(localStorage.getItem('token')).userEngagements[0].roles.some(x => x.name === 'Superadmin');
	}

	getOrganizationId(): number {
		return JSON.parse(localStorage.getItem('token')).userEngagements[0].organizationId;
	}

	getUserId(): number {
		return JSON.parse(localStorage.getItem('token')).id;
	}

	/**
	 *  Login the user then tell all the subscribers about the new status
	 */
	login(profile): void {
		localStorage.setItem('token', JSON.stringify(profile));
		this.isLoginSubject.next(true);
	}

	/**
	 * Log out the user then tell all the subscribers about the new status
	 */
	logout(): void {
		this.clearLocalStorage();
	}

	clearLocalStorage() {
		localStorage.removeItem('token');
		localStorage.clear();
		this.isLoginSubject.next(false);
	}

	/**
	 * if we have token the user is loggedIn
	 * @returns {boolean}
	 */
	private hasToken(): boolean {
		return !!localStorage.getItem('token');
	}
}
