import {Injectable} from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
// import 'rxjs/add/operator/map';
import {AuthService} from './auth.service';
import {Observable} from 'rxjs';

@Injectable()
export class AuthGuard  {
	constructor(private authService: AuthService, private router: Router) {
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {

		// TODO!! Check if this works, else find another solution. And remove unused imports!

		// return this.authService.isLoggedIn().take(1).map(
		//   res => {
		//     if (res) {
		//       return true;
		//     }
		//     this.router.navigate(['/']);
		//     return false;
		//   }
		// );

		const isLoggedIn = this.authService.isLoggedIn();
		if (isLoggedIn) {
			return true;
		}
		this.router.navigate(['/']);
		return false;
	}

}
