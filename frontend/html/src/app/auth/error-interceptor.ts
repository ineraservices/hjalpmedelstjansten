import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from "rxjs/operators";

/**
 *  Globally catch 401 errors and redirect to login page
 */

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

	constructor(private router: Router) {
	}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return next.handle(req).pipe(tap({
			next: (event: HttpEvent<any>) => {
			},
			error: (err)  => {
				if (err instanceof HttpErrorResponse) {
					if (err.status !== 401) {
						return;
					}
					this.router.navigate(['/logged-out']).then(nav => {
						//console.log(nav); // true if navigation is successful
					}, err => {
						//console.log(err) // when there's an error
					});
				}
			}
		}));
	}
}
