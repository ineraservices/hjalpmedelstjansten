import {Component} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {CommonService} from '../../../services/common.service';
import {AlertService} from "../../../services/alert.service";
import {ProductService} from "../../../services/product.service";
import {FormControl} from "@angular/forms";
import {CategoryDialogComponent} from "../../product/category-dialog/category-dialog.component";
import {CategoryProperty} from "../../../models/product/category-property.model";
import {ConfirmationDialogComponent} from "../confirm-dialog/confirmation-dialog.component";
import {CategoryService} from "../../../services/category.service";
import {
	EditPropertyGenericDialogComponent
} from "./create-property/edit-property-genericomponent-dialog/edit-property-generic-dialog.component";

enum propertyType {
	TEXTFIELD = "TEXTFIELD",
	DECIMAL = "DECIMAL",
	INTERVAL = "INTERVAL",
	VALUELIST_MULTIPLE = "VALUELIST_MULTIPLE",
	VALUELIST_SINGLE = "VALUELIST_SINGLE"
}

@Component({
	selector: "app-category-specific-properties",
	templateUrl: './category-specific-properties.component.html',
	styleUrls: ['./category-specific-properties.component.scss'],

})

export class CategorySpecificPropertiesComponent {

	dataSource = null;
	helpTexts;
	helpTextsLoaded = false;
	filterCategory = '';
	flexDirection: string;
	dropSort = [];
	// Validation errors
	categoryError;
	categoryProperties: Array<CategoryProperty>;
	selectedCategory;
	selectedFormType;
	categoryHasProductsOrArticles;


	visualTypes: { [id: string]: string } = {
		"TEXTFIELD": "Textfält",
		"DECIMAL": "Decimal",
		"INTERVAL": "Intervall",
		"VALUELIST_MULTIPLE": "Lista - flerval",
		"VALUELIST_SINGLE": "Lista - enval",
	};


	Types = [
		{type: 'TEXTFIELD', name: 'Textfält'},
		{type: 'DECIMAL', name: 'Decimal'},
		{type: 'INTERVAL', name: 'Intervall'},
		{type: 'VALUELIST_MULTIPLE', name: 'Lista - flerval'},
		{type: 'VALUELIST_SINGLE', name: 'Lista - enval'}
	];


	selectedCategoryForm = new FormControl(null, {
		updateOn: 'blur'
	});

	constructor(
		private productService: ProductService,
		private commonService: CommonService,
		private alertService: AlertService,
		private categoryService: CategoryService,
		private editPropertydialogref: MatDialog,
		private dialog: MatDialog,
		private confirmDialog: MatDialog) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}

	}

	get CleanUpParent() {
		return this.CleanUp.bind(this);
	}

	CleanUp() {
		this.getCategoryProperties(this.selectedCategory.id);
	}

	onChangeType(type) {

		this.selectedFormType = type;
		this.selectedFormType = propertyType[type];

	}

	drop(event: CdkDragDrop<string[]>) {
		moveItemInArray(this.categoryProperties, event.previousIndex, event.currentIndex);
	}

	clearCategory() {
		this.selectedCategoryForm.setValue('');
		this.selectedCategory = null;
	}

	openSelectCategoryDialog() {
		const categoryDialogRef = this.dialog.open(CategoryDialogComponent, {
			width: '90%'
		});

		categoryDialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.selectedCategory = result;
				/* this.moveToCategory = result;*/
				if (result.code) {
					this.selectedCategoryForm.setValue(result.code + ' ' + result.name);
				} else {
					this.selectedCategoryForm.setValue(result.name);
				}
				this.getCategoryProperties(result.id);
				this.productService.search('&includeProducts=true&includeArticles=true&category=' + result.id).subscribe(
					res => {
						this.categoryHasProductsOrArticles = res.body.length > 0;

					}, error => {
						this.alertService.clear();
						error.error.errors.forEach(err => {
							this.alertService.error(err);
						});
					}
				);
			}
		});
	}

	EditProperty(property) {
		const dialogRef = this.editPropertydialogref.open(EditPropertyGenericDialogComponent, {
			width: '30%',
			data: property,
			panelClass: 'myClass',
		});
	}

	deleteProperty(propertyId) {

		const confirmDeleteDialog = this.confirmDialog.open(ConfirmationDialogComponent, {
			data: {
				'message': "Är du säker på att du vill ta bort denna kategoriegenskap ?",
			}
		});

		confirmDeleteDialog.afterClosed().subscribe(result => {
			if (result) {

				if (result === 1) {
					this.categoryService.DeleteCategoryProperty(propertyId).subscribe(
						res => {
							if (res) {
								this.alertService.success("Kategoriegenskap raderad.");
								this.CleanUp();
							}

						}, error => {
							this.alertService.clear();
							error.error.errors.forEach(err => {
								this.alertService.error(err);
							});
						}
					);
				}
				// 1 = go
				// 2 = no-go
			}
		});


	}

	getCategoryProperties(id) {
		this.categoryProperties = [];

		this.productService.getCategoryProperties(id).subscribe(
			res => {
				if (res) {
					// sort the Category Specific properties, the properties with orderindex is sorted and properties with
					// orderindex null is last in the drag n drop-list.
					const propertiesWithOrderIndexNull = res.filter(property => property.orderIndex === null);
					const propertiesWithOrderIndex = res.filter(property => property.orderIndex !== null);
					const sortProperties = propertiesWithOrderIndex.sort((a, b) => a.orderIndex - b.orderIndex);
					const sortPropertiesWithOrderIndexNull = propertiesWithOrderIndexNull.sort((a, b) => a.name.localeCompare(b.name));
					if (propertiesWithOrderIndexNull.length !== 0 && propertiesWithOrderIndex.length === 0) {
						this.categoryProperties = res;
					} else if (propertiesWithOrderIndexNull.length !== 0 && propertiesWithOrderIndex.length !== 0) {
						sortPropertiesWithOrderIndexNull.forEach((property) => {
							sortProperties.push(property);
						});
						this.categoryProperties = sortProperties;
					} else if (propertiesWithOrderIndexNull.length === 0 && propertiesWithOrderIndex.length !== 0) {
						this.categoryProperties = sortProperties;
					} else if (propertiesWithOrderIndexNull.length === 0 && propertiesWithOrderIndex.length === 0) {
						this.categoryProperties = [];
					}
				}
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

	}

	savePropertyOrder() {
		let updateError = false;
		this.categoryProperties.forEach((property, index) => {
			const prop = {id: property.id, orderIndex: index};
			this.categoryService.UpdateCategoryPropertyOrder(prop).subscribe(
				res => {

				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						updateError = true;
						this.alertService.error(err);
					});
				}
			);
		});
		if (!updateError) {
			this.alertService.success("Ordningen sparad.");
		}
	}

}
