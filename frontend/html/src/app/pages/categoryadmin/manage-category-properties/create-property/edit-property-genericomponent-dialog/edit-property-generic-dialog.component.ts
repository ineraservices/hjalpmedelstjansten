import {Component, Inject, Input} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {CommonService} from '../../../../../services/common.service';
import {AlertService} from "../../../../../services/alert.service";
import {ProductService} from "../../../../../services/product.service";
import {FormControl, FormGroup} from "@angular/forms";
import {Category} from "../../../../../models/product/category.model";
import {CategoryProperty} from "../../../../../models/product/category-property.model";
import {CategoryService} from "../../../../../services/category.service";
import {CatProperty} from "../../../../../models/product/cat-property.model";
import {CategoryValue} from '../../../../../models/product/category-value.model';


enum propertyType {
	TEXTFIELD = "TEXTFIELD",
	DECIMAL = "DECIMAL",
	INTERVAL = "INTERVAL",
	VALUELIST_MULTIPLE = "VALUELIST_MULTIPLE",
	VALUELIST_SINGLE = "VALUELIST_SINGLE"
}


@Component({
	selector: 'app-edit-property-generic-component-dialog',
	templateUrl: './edit-property-generic-dialog.component.html',
	styleUrls: ['./edit-property-generic-dialog.component.scss'],
	providers: [CategoryService]
})

export class EditPropertyGenericDialogComponent {


	@Input() categoryId: number;
	@Input() selectedFormType: string;
	@Input() categoryProperties: Array<CategoryProperty>;

	@Input() CleanUpParent: Function;

	propertyType = propertyType;
	categoryProperty = new CategoryProperty();
	dataSource = null;
	helpTexts;
	helpTextsLoaded = false;
	filterCategory = '';
	flexDirection: string;
	// Validation errors
	categoryError;
	selectedCategory = new Category();
	EditCategoryPropertyForm;
	propertyItems = [];
	propertyItemInput;
	categoryValue = new CategoryValue();

	currentProperty = new CategoryProperty();

	/*  catprop = new CategoryProperty();
	catbody = new CreateCategoryProperty();*/

	// Form controls
	nameFormControl = new FormControl(null, {
		updateOn: 'blur'
	});
	descriptionFormControl = new FormControl(null, {
		updateOn: 'blur'
	});
	itemFormControl = new FormControl(null, {
		updateOn: 'blur'
	});


	/* propertyNameFormControl = new FormControl(null, {
	 updateOn: 'blur'
 });

 descriptionFormControl = new FormControl(null, {
	 updateOn: 'blur'
 });*/


	constructor(
		@Inject(MAT_DIALOG_DATA) public data: any,
		private productService: ProductService,
		private commonService: CommonService,
		private alertService: AlertService,
		private categoryService: CategoryService,
		private dialog: MatDialog,
	) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}

		this.EditCategoryPropertyForm = new FormGroup({
			name: new FormControl(),
			description: new FormControl(),
			items: new FormControl(),
		});
		this.currentProperty = data;
		this.nameFormControl.setValue(data.name);
		this.descriptionFormControl.setValue(data.description);

	}


	SaveProperty() {
		const catProp = new CatProperty();

		catProp.description = this.descriptionFormControl.value;
		catProp.name = this.nameFormControl.value;
		catProp.id = this.currentProperty.id;

		this.categoryService.UpdateCategoryProperty(catProp).subscribe(
			res => {
				if (res) {
					this.alertService.success("Kategoriegenskap uppdaterad.");
					this.currentProperty.description = catProp.description;
					this.currentProperty.name = catProp.name;
				}

			}, error => {
				this.alertService.errorWithMessage("Något gick fel.");
			}
		);
	}

	addItem() {
		this.categoryValue.value = this.EditCategoryPropertyForm.controls.items.value;
		this.categoryValue.propertyId = this.currentProperty.id;


		this.categoryService.CreateCategoryPropertyListItem(this.categoryValue).subscribe(
			res => {
				if (res) {
					this.categoryValue.id = res;
					this.alertService.success("alternativ sparad.");
					this.currentProperty.values.push(this.categoryValue);
					this.EditCategoryPropertyForm.controls.items.reset();
					this.categoryValue = new CategoryValue();
				}
			}, error => {
				this.alertService.errorWithMessage("Något gick fel.");
			}
		);

	}

	deleteFromList(item) {
		this.categoryService.DeleteCategoryPropertyListItem(item.id).subscribe(
			res => {
				if (res) {
					this.alertService.success("alternativ raderat");

					const removeIndex = this.currentProperty.values.map(function (value) {
						return value.id;
					}).indexOf(item.id);
					this.currentProperty.values.splice(removeIndex, 1);


				}
			}, error => {
				this.alertService.errorWithMessage("Något gick fel.");
			}
		);

	}

}
