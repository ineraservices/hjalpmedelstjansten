import {Component, Input} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {CommonService} from '../../../../../services/common.service';
import {AlertService} from "../../../../../services/alert.service";
import {ProductService} from "../../../../../services/product.service";
import {FormControl, FormGroup} from "@angular/forms";
import {Category} from "../../../../../models/product/category.model";
import {CategoryProperty} from "../../../../../models/product/category-property.model";
import {CatProperty} from "../../../../../models/product/cat-property.model";
import {CategoryValue} from '../../../../../models/product/category-value.model';
import {CategoryService} from "../../../../../services/category.service";


enum propertyType {
	TEXTFIELD = "TEXTFIELD",
	DECIMAL = "DECIMAL",
	INTERVAL = "INTERVAL",
	VALUELIST_MULTIPLE = "VALUELIST_MULTIPLE",
	VALUELIST_SINGLE = "VALUELIST_SINGLE"
}


@Component({
	selector: 'app-create-category-property',
	templateUrl: './create-property-generic.component.html',
	styleUrls: ['./create-property-generic.component.scss'],
	providers: [CategoryService]
})
export class CreateCategoryPropertyComponent {


	@Input() categoryId: number;
	@Input() selectedFormType: string;
	@Input() categoryProperties: Array<CategoryProperty>;

	@Input() CleanUpParent: Function;

	propertyType = propertyType;
	categoryProperty = new CategoryProperty();
	dataSource = null;
	helpTexts;
	helpTextsLoaded = false;
	filterCategory = '';
	flexDirection: string;
	// Validation errors
	categoryError;

	selectedCategory = new Category();
	categoryPropertyForm;
	propertyItems = [];
	propertyItemInput;

	/*  catprop = new CategoryProperty();
	catbody = new CreateCategoryProperty();*/

	// Form controls
	nameFormControl = new FormControl(null, {
		updateOn: 'blur'
	});
	descriptionFormControl = new FormControl(null, {
		updateOn: 'blur'
	});
	itemFormControl = new FormControl(null, {
		updateOn: 'blur'
	});


	/* propertyNameFormControl = new FormControl(null, {
	 updateOn: 'blur'
 });

 descriptionFormControl = new FormControl(null, {
	 updateOn: 'blur'
 });*/


	constructor(
		private productService: ProductService,
		private commonService: CommonService,
		private alertService: AlertService,
		private categoryService: CategoryService,
		private dialog: MatDialog) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}

		this.categoryPropertyForm = new FormGroup({
			name: new FormControl(),
			description: new FormControl(),
			items: new FormControl(),
		});
	}


	SaveProperty() {
		const catprop = new CatProperty();

		catprop.description = this.descriptionFormControl.value;
		catprop.name = this.nameFormControl.value;
		catprop.type = this.selectedFormType.toString();
		catprop.categoryId = this.categoryId;
		if (this.selectedFormType === propertyType.VALUELIST_MULTIPLE || this.selectedFormType === propertyType.VALUELIST_SINGLE) {
			if (this.propertyItems.length > 0) {

			} else {
				this.alertService.errorWithMessage("Minst ett värde måste läggas till under list-alternativ.");
				return;
			}
		}

		this.propertyItems.forEach(function (value) {
			const i = new CategoryValue();
			i.value = value;
			catprop.values.push(i);
		});

		/*    catprop.categoryId = 479;
		catprop.name = "hej";
		catprop.description = "dsfsdfsdfsdfs";
		catprop.type = "TEXTFIELD";*/

		this.categoryService.appCreateCategoryProperty(catprop).subscribe(
			res => {
				if (res) {
					this.nameFormControl.reset();
					this.descriptionFormControl.reset();
					this.categoryProperties = [];
					this.alertService.success("Kategoriegenskap sparad.");
					this.propertyItems = [];
					this.propertyItemInput = "";
					this.CleanUpParent();
				}

			}, error => {
				this.alertService.errorWithMessage("Något gick fel.");
			}
		);

	}

	CleanForm() {
		this.nameFormControl.reset();
		this.descriptionFormControl.reset();
		this.itemFormControl.reset();
		this.propertyItems = [];
		this.propertyItemInput = "";

	}

	deleteFromList(item) {
		this.propertyItems.splice(this.propertyItems.indexOf(item), 1);
	}

	addItem() {
		this.propertyItems.push(this.categoryPropertyForm.controls.items.value);
		this.categoryPropertyForm.controls.items.reset();
	}

}
