import {Component, ViewChild} from '@angular/core';
import {CommonService} from '../../services/common.service';
import {AlertService} from "../../services/alert.service";
import {Assortment} from "../../models/assortment/assortment.model";
import {Pricelist} from "../../models/agreement/pricelist.model";
import {Organization} from "../../models/organization/organization.model";
import {ProductService} from "../../services/product.service";
import {CategoryService} from "../../services/category.service";
import {
	ExportCategorySpecificPropertiesComponent
} from "./export-category-properties/export-category-properties.component";
import {saveAs} from "file-saver";

@Component({
	selector: 'app-categoryadmin',
	templateUrl: './categoryadmin.component.html',
	styleUrls: ['./categoryadmin.component.scss'],

})
export class CategoryAdminComponent {

	@ViewChild(ExportCategorySpecificPropertiesComponent) child!: ExportCategorySpecificPropertiesComponent;

	dataSource = null;
	allArticles = [];
	pricelistArticles = [];
	assortment: Assortment = new Assortment();
	agreements = [];
	selectedAgreement: number;
	pricelists: Array<Pricelist>;
	selectedPricelist: number;
	organization: Organization = new Organization();
	organizationId: number;
	assortmentId: number;
	helpTexts;
	helpTextsLoaded = false;
	filterCategory = '';
	flexDirection: string;
	showMenu = false;


	selected = null;

	constructor(
		private productService: ProductService,
		private categoryService: CategoryService,
		private commonService: CommonService,
		private alertService: AlertService) {
		this.flexDirection = this.commonService.isUsingIE() ? 'row' : 'column';
	}

	tabChange(event) {
		this.showMenu = event.index === 3;
	}

	exportCategories() {
		this.categoryService.getExportFileIso(this.child.selected).subscribe(
			res => {
				saveAs(res.body, res.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
			},
			error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);


	}


}
