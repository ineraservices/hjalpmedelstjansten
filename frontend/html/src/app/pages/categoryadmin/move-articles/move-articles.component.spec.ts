import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ArticleMoveSearchComponent} from './move-articles.component';

describe('ArticleMoveSearchComponent', () => {
	let component: ArticleMoveSearchComponent;
	let fixture: ComponentFixture<ArticleMoveSearchComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ArticleMoveSearchComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ArticleMoveSearchComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
