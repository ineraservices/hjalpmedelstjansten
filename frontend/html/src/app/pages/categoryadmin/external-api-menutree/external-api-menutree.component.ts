import {Component, OnInit} from '@angular/core';
import {Category} from "../../../models/product/category.model";
import {ProductService} from "../../../services/product.service";
import {AlertService} from "../../../services/alert.service";
import {CommonService} from "../../../services/common.service";
import {CategoryService} from "../../../services/category.service";
import {FormControl, FormGroup} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {ConfirmationDialogComponent} from "../confirm-dialog/confirmation-dialog.component";
import {ExternalCategoryGrouping} from "../../../models/product/external-category-grouping.model";
import {
	CreateUpdateExternalCategoryComponent
} from "./create-update-external-category/create-update-external-category.component";

@Component({
	selector: 'app-external-api-menutree',
	templateUrl: './external-api-menutree.component.html',
	styleUrls: ['./external-api-menutree.component.scss']
})
export class ExternalApiMenutreeComponent implements OnInit {

	lvl1categoryForm;
	lvl2categoryForm;
	lvl3categoryForm;

	query: string;
	flexDirection: string;
	selectedLevelOne: number;
	selectedLevelTwo: number;
	selectedLevelThree: number;
	selectedIsoCategory: Category;
	selectedCategoryOne: ExternalCategoryGrouping = new ExternalCategoryGrouping();
	selectedCategoryTwo: ExternalCategoryGrouping = new ExternalCategoryGrouping();
	selectedCategoryThree: ExternalCategoryGrouping = new ExternalCategoryGrouping();
	levelOneCategories: any = [];

	levelTwoCategories: any = [];

	levelThreeCategories: any = [];
	isoCategoryLoaded = false;

	constructor(private productService: ProductService,
							private alertService: AlertService,
							private commonService: CommonService,
							private createUpdateDialog: MatDialog,
							private confirmDeleteDialog: MatDialog,
							private categoryService: CategoryService) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}

		this.lvl1categoryForm = new FormGroup({
			name: new FormControl(),
			description: new FormControl(),
			id: new FormControl(),
		});
		this.lvl2categoryForm = new FormGroup({
			name: new FormControl(),
			description: new FormControl(),
			id: new FormControl(),
		});
		this.lvl3categoryForm = new FormGroup({
			name: new FormControl(),
			description: new FormControl(),
			id: new FormControl(),
		});

	}

	ngOnInit() {

		// Get level 1 categories
		this.categoryService.getExternalCategories().subscribe(res => {
			this.levelOneCategories = res;
			this.selectedLevelOne = null;

		});
	}

	confirm(level) {

		const confirmDialog = this.confirmDeleteDialog.open(ConfirmationDialogComponent, {
			data: {
				'message': "Är du säker på att du vill ta bort denna 1177-kategori?",
			}

		});
		confirmDialog.afterClosed().subscribe(result => {
			if (result === 1) {

				if (level === 1) {
					this.DeleteCategory(this.selectedLevelOne, level);
				} else if (level === 2) {
					this.DeleteCategory(this.selectedLevelTwo, level);
				} else if (level === 3) {
					this.DeleteCategory(this.selectedLevelThree, level);
				}
			}
		});
	}


	DeleteCategory(uniqueId, level) {
		console.log(uniqueId, level);
		// send response and delete external category
		this.categoryService.deleteExternalCategory(uniqueId).subscribe(
			res => {
				this.alertService.success('1177-Kategori raderad');
				switch (level) {
				case 1:
					this.selectedCategoryOne = new ExternalCategoryGrouping();
					this.selectedLevelOne = null;
					this.ngOnInit();
					break;
				case 2:
					this.selectedCategoryTwo = new ExternalCategoryGrouping();
					this.selectedLevelTwo = null;
					this.onCategorySelection(this.selectedCategoryOne, 1);
					break;
				case 3:
					this.selectedCategoryThree = new ExternalCategoryGrouping();
					this.selectedLevelThree = null;
					this.onCategorySelection(this.selectedCategoryTwo, 2);
					break;
				}
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	OpenCreateUpdateDialog(level) {

		switch (level) {
		case 1:
			const dialogRef = this.createUpdateDialog.open(CreateUpdateExternalCategoryComponent, {
				width: '30%',
				data: {
					'category': null,
					'name': this.selectedCategoryOne.name,
					'id': this.selectedCategoryOne.id,
					'description': this.selectedCategoryOne.description,
					'parent': null,
					'level': 1,

				}
			});

			dialogRef.afterClosed().subscribe(result => {
				if (result) {
					this.ngOnInit();
					const initialIdValue = this.selectedCategoryOne.id;
					this.selectedCategoryOne.id = result.id;
					this.selectedCategoryOne.category = result.categoryId;
					this.selectedCategoryOne.parent = null;
					this.selectedCategoryOne.description = result.description;
					this.selectedCategoryOne.name = result.name;
					this.selectedLevelOne = result.id;

					if (initialIdValue == null) {
						this.onCategorySelection(this.selectedCategoryOne, 1);
						this.OpenCreateUpdateDialog(2);
					}

				}
			});

			break;
		case 2:
			const dialogRef2 = this.createUpdateDialog.open(CreateUpdateExternalCategoryComponent, {
				width: '30%',
				data: {
					'category': this.selectedCategoryTwo.category,
					'name': this.selectedCategoryTwo.name,
					'id': this.selectedCategoryTwo.id,
					'parent': this.selectedCategoryOne.id,
					'description': this.selectedCategoryTwo.description,
					'level': 2,
				}
			});

			dialogRef2.afterClosed().subscribe(result => {
				if (result) {
					const initialIdValue = this.selectedCategoryTwo.id;
					this.onCategorySelection(this.selectedCategoryOne, 1);
					this.selectedCategoryTwo.id = result.id;
					this.selectedCategoryTwo.category = result.category;
					this.selectedCategoryTwo.parent = result.parent;
					this.selectedCategoryTwo.description = result.description;
					this.selectedCategoryTwo.name = result.name;
					this.selectedLevelTwo = result.id;

					this.onCategorySelection(this.selectedCategoryTwo, 2);

					if (initialIdValue == null) {
						this.OpenCreateUpdateDialog(3);
					}

				}
			});

			break;
		case 3:

			const dialogRef3 = this.createUpdateDialog.open(CreateUpdateExternalCategoryComponent, {
				width: '30%',
				data: {
					'category': this.selectedCategoryThree.category,
					'name': this.selectedCategoryThree.name,
					'id': this.selectedCategoryThree.id,
					'parent': this.selectedCategoryTwo.id,
					'description': this.selectedCategoryThree.description,
					'level': 3,
				}
			});

			dialogRef3.afterClosed().subscribe(result => {
				if (result) {
					this.onCategorySelection(this.selectedCategoryTwo, 2);
					this.selectedCategoryThree.id = result.id;
					this.selectedCategoryThree.category = result.category;
					this.selectedCategoryThree.parent = result.parent;
					this.selectedCategoryThree.description = result.description;
					this.selectedCategoryThree.name = result.name;
					this.selectedLevelThree = result.id;
					this.onCategorySelection(this.selectedCategoryThree, 3);

				}
			});
			break;
		}
	}

	onCategorySelection(category, level) {
		switch (level) {
		case 1:
			this.selectedCategoryTwo = new ExternalCategoryGrouping();
			this.selectedCategoryThree = new ExternalCategoryGrouping();
			this.selectedCategoryOne = category;
			this.selectedLevelOne = category.id;
			this.selectedLevelTwo = null;
			this.selectedLevelThree = null;
			this.levelTwoCategories = [];
			this.levelThreeCategories = [];
			this.getChildren(category, level);
			break;
		case 2:
			this.selectedCategoryThree = new ExternalCategoryGrouping();
			this.selectedCategoryTwo = category;
			this.selectedLevelTwo = category.id;
			this.selectedLevelThree = null;
			this.levelThreeCategories = [];

			this.getChildren(category, level);

			break;
		case 3:
			this.selectedCategoryThree = category;
			this.selectedLevelThree = category.id;

			// Hämta iso-kategori för vald 1177-kategori nivå 3 för att visa kod, namn, beskrivning
			this.categoryService.getCategoryByUniqueId(this.selectedCategoryThree.category).subscribe(
				res => {
					this.selectedIsoCategory = res[0];
					this.isoCategoryLoaded = true;
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);

			break;

		}
		window.scroll(0, 0);
	}

	getChildren(category, level) {

		if (category.id != null) {
			this.categoryService.getExternalCategoryChildren(category.id).subscribe(
				res => {
					switch (level) {
					case 1:
						this.levelTwoCategories = res;
						break;
					case 2:
						this.levelThreeCategories = res;
						break;
					}
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		}

	}

	CleanUpForms() {

		this.selectedLevelOne = null;
		this.selectedLevelTwo = null;
		this.selectedLevelThree = null;

		this.selectedCategoryOne = new ExternalCategoryGrouping();
		this.selectedCategoryTwo = new ExternalCategoryGrouping();
		this.selectedCategoryThree = new ExternalCategoryGrouping();

		this.levelOneCategories = [];
		this.levelTwoCategories = [];
		this.levelThreeCategories = [];


		this.ResetForm(this.lvl1categoryForm);
		this.ResetForm(this.lvl2categoryForm);
		this.ResetForm(this.lvl3categoryForm);

		// Get categories level 1
		this.categoryService.getExternalCategories().subscribe(
			res => {
				this.levelOneCategories = res;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

	}

	ResetForm(form: FormGroup) {

		form.reset();

		Object.keys(form.controls).forEach(key => {
			form.get(key).setErrors(null);
		});
	}

}
