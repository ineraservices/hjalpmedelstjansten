import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUpdateExternalCategoryComponent } from './create-update-external-category.component';

describe('CreateUpdateExternalCategoryComponent', () => {
  let component: CreateUpdateExternalCategoryComponent;
  let fixture: ComponentFixture<CreateUpdateExternalCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateUpdateExternalCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUpdateExternalCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
