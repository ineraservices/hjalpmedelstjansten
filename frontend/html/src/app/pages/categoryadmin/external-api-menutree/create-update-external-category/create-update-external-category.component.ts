import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {ProductService} from "../../../../services/product.service";
import {AlertService} from "../../../../services/alert.service";
import {CategoryService} from "../../../../services/category.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ExternalCategoryGrouping} from "../../../../models/product/external-category-grouping.model";
import {Category} from "../../../../models/product/category.model";

@Component({
	selector: 'app-create-update-external-category',
	templateUrl: './create-update-external-category.component.html',
	styleUrls: ['./create-update-external-category.component.scss']
})
export class CreateUpdateExternalCategoryComponent implements OnInit {

	saving = false;
	id: number;
	category: number;
	level: number;
	displayOrder: number;
	parent: number;
	parentCode: number;
	allOK: boolean;
	name: string;
	isoCategories: Array<Category>;
	selectedIsoCategory: Category;
	loading = true;

	// Form controls
	nameFormControl = new FormControl(null, {
		updateOn: 'blur'
	});
	descriptionFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	isoCategoryFormControl = new FormControl(null, {updateOn: 'blur'});


	// Validation errors
	nameError;
	genericError;
	descriptionError;
	codeError;

	constructor(public dialogRef: MatDialogRef<CreateUpdateExternalCategoryComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private route: ActivatedRoute,
							private productService: ProductService,
							private alertService: AlertService,
							private categoryService: CategoryService) {
		dialogRef.disableClose = true;
		this.nameFormControl.setValue(data.name);
		this.descriptionFormControl.setValue(data.description);
		this.isoCategoryFormControl.setValue(data.category);
		this.id = data.id;
		this.category = data.category;
		this.parent = data.parent;
		this.displayOrder = data.displayOrder;
		this.level = data.level;
		this.allOK = true;
	}

	ngOnInit() {
		this.categoryService.getAllCategories().subscribe(
			res => {
				this.isoCategories = res.filter(item => item.code.length > 4);
				this.loading = false;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}


	handleServerValidation(error): void {
		switch (error.field) {
		case 'name': {
			this.nameError = error.message;
			this.nameFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'description': {
			this.descriptionError = error.message;
			this.descriptionFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'code': {
			this.codeError = error.message;
			this.isoCategoryFormControl.setErrors(Validators.pattern(''));
			break;
		}
		}
	}


	resetValidation() {

		this.genericError = null;

		this.nameError = null;
		this.nameFormControl.markAsDirty();
		this.descriptionError = null;
		this.descriptionFormControl.markAsDirty();

	}

	setIsoCategory(category, event: any) {
		if (event.isUserInput) {
			this.selectedIsoCategory = category;
		}

	}


	saveOrUpdateEntity() {

		this.saving = true;
		this.resetValidation();
		const cate = new ExternalCategoryGrouping();

		cate.id = this.id;
		if (this.level === 3) {
			cate.name = this.nameFormControl.value == null ? this.selectedIsoCategory.name : this.nameFormControl.value;
			cate.description = this.descriptionFormControl.value == null ? this.selectedIsoCategory.description : this.descriptionFormControl.value;
		} else {
			cate.name = this.nameFormControl.value;
			cate.description = this.descriptionFormControl.value == null ? null : this.descriptionFormControl.value;
			// cate.description = this.descriptionFormControl.value;
		}

		cate.parent = this.parent;
		cate.category = this.isoCategoryFormControl.value;
		cate.displayOrder = this.displayOrder;


		this.categoryService.createExternalCategory(cate).subscribe(
			res => {
				this.saving = false;
				this.alertService.success('Kategori sparad');
				this.dialogRef.close(res);

			}, error => {
				this.saving = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.handleServerValidation(err);
					this.alertService.error(err);
				});
			}
		);


	}


}
