import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalApiMenutreeComponent } from './external-api-menutree.component';

describe('ExternalApiMenutreeComponent', () => {
  let component: ExternalApiMenutreeComponent;
  let fixture: ComponentFixture<ExternalApiMenutreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalApiMenutreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalApiMenutreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
