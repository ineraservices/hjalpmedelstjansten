import {Component, OnInit} from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {AlertService} from "../../../services/alert.service";
import {FormControl, FormGroup} from "@angular/forms";
import {ProductService} from "../../../services/product.service";
import {Category} from "../../../models/product/category.model";
import {CreateUpdateDialogComponent} from "./create-update-category-dialog/create-update-dialog.component";
import {MatDialog} from '@angular/material/dialog';
import {ConfirmationDialogComponent} from "../confirm-dialog/confirmation-dialog.component";
import {CategoryService} from "../../../services/category.service";


@Component({
	selector: 'app-category-list',
	templateUrl: './create-category.component.html',
	styleUrls: ['./create-category.component.scss']
})
export class CreateCategoryComponent implements OnInit {
	lvl1categoryForm;
	lvl2categoryForm;
	lvl3categoryForm;
	lvl4categoryForm;

	lvl1categoryFormValidated = false;
	lvl2categoryFormValidated = false;
	lvl3categoryFormValidated = false;
	lvl4categoryFormValidated = false;

	query: string;
	flexDirection: string;
	qParam = 'excludeCodelessCategories=true';
	selectedLevelOne: number;
	selectedLevelTwo: number;
	selectedLevelThree: number;
	selectedLevelFour: number;
	categoryHasProductsOrArticles = false;

	queryParam = '&includeProducts=true&includeArticles=true&type=H&type=T';

	selectedCategoryOne: Category = new Category();
	selectedCategoryTwo: Category = new Category();
	selectedCategoryThree: Category = new Category();
	selectedCategoryFour: Category = new Category();
	levelOneCategories: any = [];

	levelTwoCategories: any = [];

	levelThreeCategories: any = [];

	levelFourCategories: any = [];


	articleTypes: any = [
		{
			full: "H"
		},
		{
			full: "T"
		}
	];


	constructor(private productService: ProductService,
							private alertService: AlertService,
							private commonService: CommonService,
							private createUpdateDialog: MatDialog,
							private confirmDeleteDialog: MatDialog,
							private categoryService: CategoryService) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}
		this.lvl1categoryForm = new FormGroup({
			name: new FormControl(),
			description: new FormControl(),
			id: new FormControl(),
			code: new FormControl(),

		});
		this.lvl2categoryForm = new FormGroup({
			name: new FormControl(),
			description: new FormControl(),
			id: new FormControl(),
			code: new FormControl(),
		});
		this.lvl3categoryForm = new FormGroup({
			name: new FormControl(),
			description: new FormControl(),
			id: new FormControl(),
			code: new FormControl(),
			articleType: new FormControl(this.articleTypes)
		});
		this.lvl4categoryForm = new FormGroup({
			name: new FormControl(),
			description: new FormControl(),
			id: new FormControl(),
			code: new FormControl(),
			articleType: new FormControl(this.articleTypes)
		});

		this.lvl1categoryForm.valueChanges.subscribe(selectedValue => {
			if (selectedValue.code) {
				this.lvl1categoryFormValidated = selectedValue.code.length === 2;
			}
		});
	}

	confirm(level) {

		const confirmDialog = this.confirmDeleteDialog.open(ConfirmationDialogComponent, {
			data: {
				'message': "Är du säker på att du vill ta bort denna kategori?",
			}

		});
		confirmDialog.afterClosed().subscribe(result => {
			if (result === 1) {

				if (level === 1) {
					this.DeleteCategory(this.selectedLevelOne, level);
				} else if (level === 2) {
					this.DeleteCategory(this.selectedLevelTwo, level);
				} else if (level === 3) {
					this.DeleteCategory(this.selectedLevelThree, level);
				} else if (level === 4) {
					this.DeleteCategory(this.selectedLevelFour, level);
				} else {

				}
			}


		});
	}


	DeleteCategory(categoryId, level) {
		// send response and delete category
		this.categoryService.getDeleteCategory(categoryId).subscribe(
			res => {
				this.alertService.success('Kategori raderad');
				switch (level) {
				case 1:
					this.selectedCategoryOne = new Category();
					this.selectedLevelOne = null;
					this.ngOnInit();
					break;
				case 2:
					this.selectedCategoryTwo = new Category();
					this.selectedLevelTwo = null;
					this.onCategorySelection(this.selectedCategoryOne, 1);
					break;
				case 3:
					this.selectedCategoryThree = new Category();
					this.selectedLevelThree = null;
					this.onCategorySelection(this.selectedCategoryTwo, 2);
					break;
				case 4:
					this.selectedCategoryFour = new Category();
					this.selectedLevelFour = null;
					this.onCategorySelection(this.selectedCategoryThree, 3);
					break;

				}
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			});
	}

	OpenCreateUpdateDialog(level) {

		switch (level) {
		case 1:
			const dialogRef = this.createUpdateDialog.open(CreateUpdateDialogComponent, {
				width: '30%',
				data: {
					'code': this.selectedCategoryOne.code,
					'name': this.selectedCategoryOne.name,
					'id': this.selectedCategoryOne.id,
					'description': this.selectedCategoryOne.description,
					'level': 1,

				}
			});

			dialogRef.afterClosed().subscribe(result => {
				if (result) {
					this.ngOnInit();
					const initialIdValue = this.selectedCategoryOne.id;
					this.selectedCategoryOne.id = result.uniqueId;
					this.selectedCategoryOne.code = result.code;
					this.selectedCategoryOne.parentId = result.parent;
					this.selectedCategoryOne.description = result.description;
					this.selectedCategoryOne.name = result.name;
					this.selectedCategoryOne.articleType = result.articleType;
					this.selectedLevelOne = result.uniqueId;

					if (initialIdValue == null) {
						this.onCategorySelection(this.selectedCategoryOne, 1);
						this.OpenCreateUpdateDialog(2);
					}

				}
			});

			break;
		case 2:
			const dialogRef2 = this.createUpdateDialog.open(CreateUpdateDialogComponent, {
				width: '30%',
				data: {
					'code': this.selectedCategoryTwo.code,
					'name': this.selectedCategoryTwo.name,
					'id': this.selectedCategoryTwo.id,
					'parentId': this.selectedCategoryOne.id,
					'description': this.selectedCategoryTwo.description,
					'level': 2,
					'parentCode': this.selectedCategoryOne.code,
				}
			});

			dialogRef2.afterClosed().subscribe(result => {
				if (result) {
					const initialIdValue = this.selectedCategoryTwo.id;
					this.onCategorySelection(this.selectedCategoryOne, 1);
					this.selectedCategoryTwo.id = result.uniqueId;
					this.selectedCategoryTwo.code = result.code;
					this.selectedCategoryTwo.parentId = result.parent;
					this.selectedCategoryTwo.description = result.description;
					this.selectedCategoryTwo.name = result.name;
					this.selectedCategoryTwo.articleType = result.articleType;
					this.selectedLevelTwo = result.uniqueId;

					this.onCategorySelection(this.selectedCategoryTwo, 2);

					if (initialIdValue == null) {
						this.OpenCreateUpdateDialog(3);
					}

				}
			});

			break;
		case 3:
			let hasChildren = false;

			if (this.levelFourCategories) {
				if (this.levelFourCategories.length > 0) {
					hasChildren = true;
				}
			}
			const dialogRef3 = this.createUpdateDialog.open(CreateUpdateDialogComponent, {
				width: '30%',
				data: {
					'code': this.selectedCategoryThree.code,
					'name': this.selectedCategoryThree.name,
					'id': this.selectedCategoryThree.id,
					'parentId': this.selectedCategoryTwo.id,
					'articleType': this.selectedCategoryThree.articleType,
					'description': this.selectedCategoryThree.description,
					'level': 3,
					'parentCode': this.selectedCategoryTwo.code,
					'hasChildren': hasChildren,
					'categoryHasProductsOrArticles': this.categoryHasProductsOrArticles
				}
			});

			dialogRef3.afterClosed().subscribe(result => {
				if (result) {
					this.onCategorySelection(this.selectedCategoryTwo, 2);
					this.onCategorySelection(this.selectedCategoryThree, 3);
					this.selectedCategoryThree.id = result.uniqueId;
					this.selectedCategoryThree.code = result.code;
					this.selectedCategoryThree.parentId = result.parent;
					this.selectedCategoryThree.description = result.description;
					this.selectedCategoryThree.name = result.name;
					this.selectedCategoryThree.articleType = result.articleType;
					this.selectedLevelThree = result.uniqueId;

					if (result.articleType == null && !hasChildren) {
						this.OpenCreateUpdateDialog(4);
					}
				}
			});

			break;
		case 4:
			const dialogRef4 = this.createUpdateDialog.open(CreateUpdateDialogComponent, {
				width: '30%',
				data: {
					'code': this.selectedCategoryFour.code,
					'name': this.selectedCategoryFour.name,
					'id': this.selectedCategoryFour.id,
					'articleType': this.selectedCategoryFour.articleType,
					'parentId': this.selectedCategoryThree.id,
					'description': this.selectedCategoryFour.description,
					'level': 4,
					'parentCode': this.selectedCategoryThree.code,
					'parent': this.selectedCategoryThree,
					'categoryHasProductsOrArticles': this.categoryHasProductsOrArticles
				}
			});

			dialogRef4.afterClosed().subscribe(result => {
				if (result) {
					this.onCategorySelection(this.selectedCategoryThree, 3);
					this.selectedCategoryFour.id = result.uniqueId;
					this.selectedCategoryFour.code = result.code;
					this.selectedCategoryFour.parentId = result.parent;
					this.selectedCategoryFour.description = result.description;
					this.selectedCategoryFour.name = result.name;
					this.selectedCategoryFour.articleType = result.articleType;
					this.selectedLevelFour = result.uniqueId;

				}
				this.onCategorySelection(this.selectedCategoryFour, 4);
			}
			);

		}


	}

	onSubmit(form) {
	}

	validateForm(level) {
		switch (level) {
		case 1:

			break;
		case 2:

			break;
		case 3:

			break;
		case 4:

			break;
		case 5:

			break;
		}
	}

	onCategorySelection(category, level) {
		switch (level) {
		case 1:
			this.selectedCategoryTwo = new Category();
			this.selectedCategoryFour = new Category();
			this.selectedCategoryThree = new Category();
			this.selectedCategoryOne = category;
			this.selectedLevelOne = category.id;
			this.selectedLevelTwo = null;
			this.selectedLevelThree = null;
			this.selectedLevelFour = null;
			this.levelTwoCategories = [];
			this.levelThreeCategories = [];
			this.levelFourCategories = [];
			break;
		case 2:
			this.selectedCategoryThree = new Category();
			this.selectedCategoryFour = new Category();
			this.selectedCategoryTwo = category;
			this.selectedLevelTwo = category.id;
			this.selectedLevelThree = null;
			this.selectedLevelFour = null;
			this.levelThreeCategories = [];
			this.levelFourCategories = [];
			break;
		case 3:

			this.selectedLevelFour = null;
			this.selectedCategoryFour = new Category();
			this.selectedCategoryThree = category;
			this.selectedLevelThree = category.id;
			this.levelFourCategories = [];
			break;
		case 4:
			this.selectedCategoryFour = category;
			this.selectedLevelFour = category.id;
			break;
		case 5:
			this.selectedLevelOne = null;
			this.selectedLevelTwo = null;
			this.selectedLevelThree = null;
			this.selectedLevelFour = null;
			this.levelOneCategories = [];
			this.levelTwoCategories = [];
			this.levelThreeCategories = [];
			this.levelFourCategories = [];
			break;
		}


		window.scroll(0, 0);
		this.productService.getChildCategories(category.code).subscribe(
			res => {
				switch (level) {
				case 1:
					this.levelTwoCategories = res;
					break;
				case 2:
					this.levelThreeCategories = res;
					break;
				case 3:
					this.levelFourCategories = res;
					break;
				}
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

	}

	CleanUpForms() {

		this.selectedLevelOne = null;
		this.selectedLevelTwo = null;
		this.selectedLevelThree = null;
		this.selectedLevelFour = null;

		this.selectedCategoryOne = new Category();
		this.selectedCategoryTwo = new Category();
		this.selectedCategoryThree = new Category();
		this.selectedCategoryFour = new Category();

		this.levelOneCategories = [];
		this.levelTwoCategories = [];
		this.levelThreeCategories = [];
		this.levelFourCategories = [];


		this.ResetForm(this.lvl1categoryForm);
		this.ResetForm(this.lvl2categoryForm);
		this.ResetForm(this.lvl3categoryForm);
		this.ResetForm(this.lvl4categoryForm);

		this.productService.getCategories('?' + this.qParam).subscribe(
			res => {
				this.levelOneCategories = res;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

	}

	ResetForm(form: FormGroup) {

		form.reset();

		Object.keys(form.controls).forEach(key => {
			form.get(key).setErrors(null);
		});
	}

	SubmitForm() {
		// here you can access both the forms using  this.addAdress and

	}


	ngOnInit() {
		this.productService.getCategories('?' + this.qParam).subscribe(
			res => {
				this.levelOneCategories = res;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

}
