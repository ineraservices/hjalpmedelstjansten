import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ActivatedRoute} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';
import {AlertService} from '../../../../services/alert.service';
import {Category} from "../../../../models/product/category.model";
import {CategoryService} from "../../../../services/category.service";
import {ProductService} from "../../../../services/product.service";


@Component({
	selector: 'app-create-update-dialog',
	templateUrl: './create-update-dialog.component.html',
	styleUrls: ['./create-update-dialog.component.scss'],
	providers: [CategoryService]
})
export class CreateUpdateDialogComponent implements OnInit {
	saving = false;
	categoryId: number;
	level: number;
	parentId: number;
	parentCode: number;
	allOK: boolean;
	name: string;
	parent;
	hasChildren;
	categoryHasProductsOrArticles: boolean;

	articleTypes: any = [
		{
			full: "H"
		},
		{
			full: "T"
		}
	];


	// Form controls
	nameFormControl = new FormControl(null, {
		updateOn: 'blur'
	});
	codeFormControl = new FormControl(null, {
		updateOn: 'blur'
	});
	descriptionFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	articleTypeFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	// Validation errors
	articleTypeError;
	nameError;
	codeError;
	genericError;
	descriptionError;

	constructor(public dialogRef: MatDialogRef<CreateUpdateDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private route: ActivatedRoute,
							private productService: ProductService,
							private alertService: AlertService,
							private categoryService: CategoryService,
							private confirmDialog: MatDialog) {
		dialogRef.disableClose = true;
		this.nameFormControl.setValue(data.name);
		this.codeFormControl.setValue(data.code);
		this.descriptionFormControl.setValue(data.description);
		this.categoryId = data.id;
		this.articleTypeFormControl.setValue(data.articleType);
		this.level = data.level;
		this.hasChildren = data.hasChildren;
		this.parentId = data.parentId;
		this.parentCode = data.parentCode;
		this.parent = data.parent;
		this.categoryHasProductsOrArticles = false;
		this.allOK = true;
	}

	ngOnInit() {
		this.productService.search('&includeProducts=true&includeArticles=true&category=' + this.categoryId).subscribe(
			res => {
				this.categoryHasProductsOrArticles = res.body.length > 0;

			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}


	handleServerValidation(error): void {
		switch (error.field) {
		case 'name': {
			this.nameError = error.message;
			this.nameFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'code': {
			this.codeError = error.message;
			this.codeFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'description': {
			this.descriptionError = error.message;
			this.descriptionFormControl.setErrors(Validators.pattern(''));
			break;
		}
		}
	}


	resetValidation() {

		this.genericError = null;

		this.nameError = null;
		this.nameFormControl.markAsDirty();

		this.codeError = null;
		this.codeFormControl.markAsDirty();

		this.descriptionError = null;
		this.descriptionFormControl.markAsDirty();

		this.articleTypeError = null;
		this.articleTypeFormControl.markAsDirty();
	}

	validateFormCode(category, parentCode, level) {

		if (parentCode === undefined && level > 1) {
			return false;
		}

		if (level === 1) {
			const regexp = new RegExp('^[0-9]{2}$');
			if (!regexp.test(category.code)) {
				this.codeError = "Ange tvåsiffrigt numeriskt värde mellan 00 - 99";
				this.codeFormControl.setErrors(Validators.pattern(''));
				return false;

			}

		}
		if (level === 2) {
			const regexp = new RegExp('^' + parentCode.toString() + '[0-9]{2}$');
			if (!regexp.test(category.code)) {
				this.codeError = "Ange fyrsiffrigt numeriskt värde mellan " + parentCode + "00- " + parentCode + "99";
				this.codeFormControl.setErrors(Validators.pattern(''));
				return false;

			}
		}

		if (level === 3) {
			const regexp = new RegExp('^' + parentCode.toString() + '[0-9]{2}$');
			if (!regexp.test(category.code)) {
				this.codeError = "Ange sexsiffrigt numeriskt värde mellan " + parentCode + "00- " + parentCode + "99";
				this.codeFormControl.setErrors(Validators.pattern(''));
				return false;
			}

		}
		if (level === 4) {
			const regexp = new RegExp('^' + parentCode.toString() + '-[0-9]{2}$');
			if (!regexp.test(category.code)) {
				this.codeError = "Ange åttasiffrigt numeriskt värde mellan " + parentCode + "-00 " + parentCode + "-99";
				this.codeFormControl.setErrors(Validators.pattern(''));
				return false;
			}

		}
	}

	saveOrUpdateEntity() {


		if (this.level === 4) {
			if (this.parent != null) {
				if (this.parent.articleType != null) {
					this.alertService.errorWithMessage('Nivå 3 får ej innehålla artikeltyp om du vill skapa nivå 4.');
					return;
				}
			}
		}
		if (this.level === 3) {
			if (this.hasChildren && this.articleTypeFormControl.value) {
				this.alertService.errorWithMessage('Kan ej sätta artikeltyp på nivå 3 om nivå 4 finns.');
				return;
			}
			if (this.categoryHasProductsOrArticles && !this.articleTypeFormControl.value) {
				this.alertService.errorWithMessage('Kan ej ta bort artikeltyp på nivå 3 eftersom det finns produkter/artiklar kopplade till kategorin.');
				return;
			}
		}


		this.saving = true;
		this.resetValidation();
		const cate = new Category();

		cate.articleType = this.articleTypeFormControl.value;
		cate.name = this.nameFormControl.value;
		cate.description = this.descriptionFormControl.value;
		cate.code = this.codeFormControl.value;
		cate.level = this.level;
		cate.parentId = this.parentId;

		if (this.categoryId) {
			cate.id = this.categoryId;
		}
		const validated = this.validateFormCode(cate, this.parentCode, this.level);

		if (this.codeError != null && this.genericError != null) {
			this.saving = false;
			return;
		}

		if (validated !== false) {
			this.categoryService.createCategory(cate).subscribe(
				res => {
					this.saving = false;
					this.alertService.success('Kategori sparad');
					this.dialogRef.close(res);

				}, error => {
					this.saving = false;
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.handleServerValidation(err);
						this.alertService.error(err);
					});
				});
		} else {
			this.saving = false;
		}
	}
}
