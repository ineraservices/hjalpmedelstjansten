import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {CommonService} from '../../../services/common.service';
import {AlertService} from "../../../services/alert.service";
import {ProductService} from "../../../services/product.service";
import {CategoryService} from "../../../services/category.service";
import {MoreInformationDialogComponent} from "./more-information-dialog/more-information-dialog.component";

enum propertyType {
	TEXTFIELD = "TEXTFIELD",
	DECIMAL = "DECIMAL",
	INTERVAL = "INTERVAL",
	VALUELIST_MULTIPLE = "VALUELIST_MULTIPLE",
	VALUELIST_SINGLE = "VALUELIST_SINGLE"
}

@Component({
	selector: 'app-export-category-specific-properties',
	templateUrl: './export-category-properties.component.html',
	styleUrls: ['./export-category-properties.component.scss'],
})


export class ExportCategorySpecificPropertiesComponent implements OnInit {

	displayedColumns: string[] = ['code', 'name', 'articleType', 'description', 'anchor'];

	dataSource = null;
	helpTexts;
	helpTextsLoaded = false;
	flexDirection: string;
	allCategories = [];
	categoriesISO = [];
	categoriesISOAddition = [];
	categoriesWithCategorySpecificProperties = [];
	selected: string;
	loaded: boolean;


	constructor(
		private productService: ProductService,
		private commonService: CommonService,
		private alertService: AlertService,
		private moreInformationDialog: MatDialog,
		private categoryService: CategoryService) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}

	}


	ngOnInit() {
		this.getAllCategories();
		this.getCategoriesWithCSP();

		this.loaded = false;


	}

	sortCategories() {
		// sorting for the different views of ISO-categories
		this.allCategories.forEach((category) => {
			if (category.code !== null) {
				if (category.code.length > 7) {
					this.categoriesISOAddition.push(category);
				} else if (category.code.startsWith('01') || category.code.startsWith('02') || category.code.startsWith('9')) {
					this.categoriesISOAddition.push(category);

				} else {
					if (category.code.charAt(2) === '9') {
						this.categoriesISOAddition.push(category);

					} else if (category.code.charAt(2) === '0' && category.code.charAt(3) === '1') {
						this.categoriesISOAddition.push(category);

					} else if (category.code.charAt(2) === '0' && category.code.charAt(3) === '2') {
						this.categoriesISOAddition.push(category);

					} else if (category.code.charAt(4) === '9') {
						this.categoriesISOAddition.push(category);

					} else if (category.code.charAt(4) === '0' && category.code.charAt(5) === '1') {
						this.categoriesISOAddition.push(category);

					} else if (category.code.charAt(4) === '0' && category.code.charAt(5) === '2') {
						this.categoriesISOAddition.push(category);

					} else {
						this.categoriesISO.push(category);
					}
				}
			}
		});
		this.loaded = true;
	}

	select(value) {
		this.selected = value;
		if (value === 'all') {
			this.dataSource = this.allCategories;
		} else if (value === 'ISO') {
			this.dataSource = this.categoriesISO;
		} else if (value === 'additions') {
			this.dataSource = this.categoriesISOAddition;
		} else if (value === 'withCategorySpecificProperties') {
			this.dataSource = this.categoriesWithCategorySpecificProperties;
		}
	}

	getAllCategories() {
		this.categoryService.getAllCategories().subscribe(
			res => {
				this.allCategories = res;
				this.sortCategories();
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

	}

	getCategoriesWithCSP() {
		this.categoryService.getCategoriesWithCSP().subscribe(
			res => {
				this.categoriesWithCategorySpecificProperties = res;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

	}

	openDialog(category) {
		const dialogRef = this.moreInformationDialog.open(MoreInformationDialogComponent, {
			data: category,
			height: '80%',
			width: '70%'
		});

		dialogRef.afterClosed().subscribe(result => {

		});
	}

}

export interface Categories {
	code: string;
	articleType: string;
	name: string;
	description: string;

}


