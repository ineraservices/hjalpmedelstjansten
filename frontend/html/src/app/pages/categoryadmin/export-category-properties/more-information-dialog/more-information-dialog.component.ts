import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CategoryService} from "../../../../services/category.service";
import {ProductService} from "../../../../services/product.service";
import {AlertService} from "../../../../services/alert.service";
import {AuthService} from "../../../../auth/auth.service";
import {ActivatedRoute, Router} from "@angular/router";
import {saveAs} from "file-saver";


@Component({
	selector: 'app-more-information-dialog',
	templateUrl: './more-information-dialog.component.html',
	styleUrls: ['./more-information-dialog.component.scss'],
})

export class MoreInformationDialogComponent implements OnInit {

	displayedColumnsCategorySpecificProperties: string[] = ['name', 'description'];
	displayedColumnsProductsByCategory: string[] = ['productName', 'productNumber', 'created', 'replacementDate', 'organizationName', 'status'];
	displayedColumnsArticlesByCategory: string[] = ['articleName', 'articleNumber', 'created', 'replacementDate', 'organizationName', 'status'];

	category;
	dataSourceProperties;
	dataSourceProducts;
	dataSourceArticles;
	productsByCategory;
	articlesByCategory;
	categorySpecificProperties;
	organizationId;
	loaded: boolean;


	constructor(
		public dialogRef: MatDialogRef<MoreInformationDialogComponent>,
		private categoryService: CategoryService,
		private productService: ProductService,
		private authService: AuthService,
		private route: ActivatedRoute,
		private alertService: AlertService,
		private router: Router,
		@Inject(MAT_DIALOG_DATA) public data: any) {
		this.category = data;

	}

	ngOnInit() {
		this.getArticlesByCategory(this.category.id);
		this.getProductsByCategory(this.category.id);
		this.getCategorySpecificProperties(this.category.id);
		this.loaded = false;


	}

	getCategorySpecificProperties(categoryUniqueId) {
		this.productService.getCategoryProperties(categoryUniqueId).subscribe(
			res => {
				if (res) {
					// sort the Category Specific properties, the properties with orderindex is sorted and properties with
					// orderindex null is last in the drag n drop-list.
					const propertiesWithOrderIndexNull = res.filter(property => property.orderIndex === null);
					const propertiesWithOrderIndex = res.filter(property => property.orderIndex !== null);
					const sortProperties = propertiesWithOrderIndex.sort((a, b) => a.orderIndex - b.orderIndex);
					const sortPropertiesWithOrderIndexNull = propertiesWithOrderIndexNull.sort((a, b) => a.name.localeCompare(b.name));
					if (propertiesWithOrderIndexNull.length !== 0 && propertiesWithOrderIndex.length === 0) {
						this.categorySpecificProperties = res;
						this.dataSourceProperties = this.categorySpecificProperties;
					} else if (propertiesWithOrderIndexNull.length !== 0 && propertiesWithOrderIndex.length !== 0) {
						sortPropertiesWithOrderIndexNull.forEach((property) => {
							sortProperties.push(property);
						});
						this.categorySpecificProperties = sortProperties;
						this.dataSourceProperties = this.categorySpecificProperties;

					} else if (propertiesWithOrderIndexNull.length === 0 && propertiesWithOrderIndex.length !== 0) {
						this.categorySpecificProperties = sortProperties;
						this.dataSourceProperties = this.categorySpecificProperties;

					} else if (propertiesWithOrderIndexNull.length === 0 && propertiesWithOrderIndex.length === 0) {
						this.categorySpecificProperties = [];
					}
					this.dataSourceProperties = this.categorySpecificProperties;
				}
				this.loaded = true;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	getProductsByCategory(categoryUniqueId) {
		this.categoryService.getProductsByCategory(categoryUniqueId).subscribe(
			res => {
				this.productsByCategory = res;
				if (res != null) {
					this.productsByCategory.forEach((product) => {
						const options: Intl.DateTimeFormatOptions = {year: 'numeric', month: 'numeric', day: 'numeric'};
						product.created = new Date(product.created).toLocaleString('sv-SE', options);
						if (product.replacementDate !== null) {
							product.replacementDate = new Date(product.replacementDate).toLocaleString('sv-SE', options);
						}
					});
				}

				this.dataSourceProducts = this.productsByCategory;
				this.loaded = true;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}


	getArticlesByCategory(categoryUniqueId) {
		this.categoryService.getArticlesByCategory(categoryUniqueId).subscribe(
			res => {
				this.articlesByCategory = res;
				if (res != null) {
					this.articlesByCategory.forEach((article) => {
						const options: Intl.DateTimeFormatOptions = {year: 'numeric', month: 'numeric', day: 'numeric'};
						article.created = new Date(article.created).toLocaleDateString('sv-SE', options);
						if (article.replacementDate !== null) {
							article.replacementDate = new Date(article.replacementDate).toLocaleDateString('sv-SE', options);
						}
					});
				}

				this.dataSourceArticles = this.articlesByCategory;
				this.loaded = true;

			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);

				});
			}
		);

	}

	exportCategory(categoryId) {
		this.categoryService.getExportFileIsoCategory(categoryId).subscribe(
			res => {
				saveAs(
					res.body, res.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]
				);
			},
			error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	async routeToArticle(element: any): Promise<void> {
		await this.router.navigateByUrl("/organization/" + element.organizationId + "/article/" + element.id + "/handle?search=true");
		this.dialogRef.close();
	}

	async routeToProduct(element: any): Promise<void> {
		await this.router.navigateByUrl("/organization/" + element.organizationId + "/product/" + element.id + "/handle?search=true");
		this.dialogRef.close();
	}

}

export interface CategorySpecificProperties {
	name: string;
	description: string;

}

export interface articlesByCategory {
	articleName: string;
	articleNumber: number;
	created: string;
	replacementDate: number;
	organizationName: string;
	status: string;
}

export interface productsByCategory {
	productName: string;
	productNumber: number;
	created: string;
	replacementDate: number;
	organizationName: string;
	status: string;
}
