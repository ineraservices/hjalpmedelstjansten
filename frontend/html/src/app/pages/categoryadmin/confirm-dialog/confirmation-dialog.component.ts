import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
	selector: 'app-confirmation-dialog',
	templateUrl: './confirmation-dialog.component.html',
	styleUrls: ['./confirmation-dialog.component.scss']
})


export class ConfirmationDialogComponent {

	answer: number;
	message = "";


	constructor(
		public dialogRef: MatDialogRef<ConfirmationDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any) {
		this.message = data.message;

	}

	onNoClick(): void {
		this.answer = 2;
		this.dialogRef.close(this.answer);
	}

	onYesClick(): void {
		this.answer = 1;
		this.dialogRef.close(this.answer);
	}
}
