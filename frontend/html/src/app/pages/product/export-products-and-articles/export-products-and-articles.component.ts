import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Category} from '../../../models/product/category.model';
import {MatDialog} from '@angular/material/dialog';
import {CategoryDialogComponent} from '../category-dialog/category-dialog.component';
import {ProductService} from '../../../services/product.service';
import {ExportByProductsDialogComponent} from './export-by-products-dialog/export-by-products-dialog.component';
import {AlertService} from '../../../services/alert.service';
import {CommonService} from '../../../services/common.service';
import {saveAs} from "file-saver";

@Component({
	selector: 'app-export-products-and-articles',
	templateUrl: './export-products-and-articles.component.html',
	styleUrls: ['./export-products-and-articles.component.scss']
})
export class ExportProductsAndArticlesComponent implements OnInit {

	saving = false;
	organizationId: number;
	fileSelection: string;
	productsAndArticlesIncluded = false;
	includeTj = false;
	includeR = false;
	includeI = false;
	includeT = false;
	pIncludeA = false;
	pIncludeR = false;
	pIncludeT = false;
	pIncludeI = false;
	pIncludeTj = false;
	selectedCategories: Array<Category>;
	selectedProducts = [];
	flexDirection: string;
	filterPublished = false;
	filterDiscontinued = false;

	constructor(private route: ActivatedRoute,
							private productService: ProductService,
							private matDialog: MatDialog,
							private alertService: AlertService,
							private commonService: CommonService) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}
		this.route.params.subscribe(params => {
			this.organizationId = params.organizationId;
		});
	}

	ngOnInit() {
		this.fileSelection = 'selectByCategories';
		this.filterPublished = true;
	}

	openAddDialog(fileSelection) {
		if (fileSelection === 'selectByCategories') {
			const dialogRef = this.matDialog.open(CategoryDialogComponent, {
				width: '90%',
				data: {
					'selectedCategories': this.selectedCategories && this.selectedCategories.length ? this.selectedCategories : [],
					'includeCodelessCategories': true
				}
			});

			dialogRef.afterClosed().subscribe(result => {
				if (result) {
					this.selectedCategories = result;
				}
			});
		} else { // select by products
			const dialogRef = this.matDialog.open(ExportByProductsDialogComponent, {
				width: '90%',
				height: '80%',
				data: {'organizationId': this.organizationId}
			});

			dialogRef.afterClosed().subscribe(result => {
				if (result) {
					result.forEach(prod => {
						if (!this.selectedProducts.find(x => x.id === prod.id)) {
							this.selectedProducts.push(prod);
						}
					});
				}
			});
		}
	}

	export(fileSelection) {
		this.saving = true;
		if (fileSelection === 'selectByCategories') {
			let categoryParams = 'includeProductsAndArticles=' + this.productsAndArticlesIncluded;
			if (this.includeI) {
				categoryParams += "&includeI=true";
			}
			if (this.includeT) {
				categoryParams += "&includeT=true";
			}
			if (this.includeTj) {
				categoryParams += "&includeTj=true";
			}
			if (this.includeR) {
				categoryParams += "&includeR=true";
			}

			if (this.filterPublished) {
				categoryParams += '&includePublished=true';
			}
			if (this.filterDiscontinued) {
				categoryParams += '&includeDiscontinued=true';
			}

			this.selectedCategories.forEach(category => {
				categoryParams += '&category=' + category.id;
			});
			this.productService.export(this.organizationId, categoryParams).subscribe(
				data => {
					saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
					this.saving = false;
				}, error => {
					this.saving = false;
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		} else { // selectByProducts
			let productParams = '';
			if (this.pIncludeA) {
				productParams += "&pIncludeA=true";
			}
			if (this.pIncludeR) {
				productParams += "&pIncludeR=true";
			}
			if (this.pIncludeT) {
				productParams += "&pIncludeT=true";
			}
			if (this.pIncludeI) {
				productParams += "&pIncludeI=true";
			}
			if (this.pIncludeTj) {
				productParams += "&pIncludeTj=true";
			}

			if (this.filterPublished) {
				productParams += '&includePublished=true';
			}
			if (this.filterDiscontinued) {
				productParams += '&includeDiscontinued=true';
			}

			this.selectedProducts.forEach(product => {
				productParams += '&product=' + product.id;
			});

			if (productParams) { // ta bort första &-tecknet
				productParams = productParams.substring(1);
			}
			this.productService.export(this.organizationId, productParams).subscribe(
				data => {
					saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
					this.saving = false;
				}, error => {
					this.saving = false;
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		}
	}

	removeProduct(product) {
		this.selectedProducts = this.selectedProducts.filter(o => o.id !== product.id);
	}

}
