import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {ProductService} from '../../../../services/product.service';
import {Product} from '../../../../models/product/product.model';
import {AlertService} from '../../../../services/alert.service';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
	selector: 'app-export-by-products-dialog',
	templateUrl: './export-by-products-dialog.component.html',
	styleUrls: ['./export-by-products-dialog.component.scss']
})
export class ExportByProductsDialogComponent implements OnInit {
	displayedColumns = ['checkbox', 'typ', 'benämning', 'produktnummer', 'status', 'kategori'];

	loading = true;
	organizationId: number;
	cachedProducts = [];
	products = [];
	dataSource = null;
	query = '';
	tmpQuery = '';
	// filterQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	searchTotal;
	offset = 25;
	totalPages;
	currentPage = 1;
	paginatorArray = [];
	allPages;
	selection = new SelectionModel<Product>(true, []);
	tmpSelection = [];

	// Filters
	filterProducts = false;
	filterArticles = false;
	filterH = false;
	filterT = false;
	filterPublished = false;
	filterDiscontinued = false;

	constructor(public dialogRef: MatDialogRef<ExportByProductsDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private productService: ProductService,
							private alertService: AlertService) {
		// The user is considered done with their search after 500 ms without further input
		this.searchChanged.pipe(
			debounceTime(500),
			distinctUntilChanged())
			.subscribe(query => {
				this.query = encodeURIComponent(query);
				this.onQueryChange();
			}
			);
	}

	ngOnInit() {
		if (this.data) {
			this.organizationId = this.data.organizationId;
		}
		this.filterPublished = true;
		// this.onFilterChange();
		this.searchProducts('');
	}

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
	}

	onQueryChange() {
		this.searchProducts(this.query);
		// this.onFilterChange();
	}

	/*
onFilterChange() {
	this.loading = true;
	this.filterQuery = '';

	this.filterPublished ? this.filterQuery += '&status=PUBLISHED' : this.filterQuery = this.filterQuery;
	this.filterDiscontinued ? this.filterQuery += '&status=DISCONTINUED' : this.filterQuery = this.filterQuery;

	this.searchProducts(this.query + this.filterQuery);
	this.loading = false;
}
*/

	/** Whether the number of selected elements matches the total number of selectable rows. */
	isAllSelected() {
		let result = true;
		this.dataSource.data.forEach(item => {
			const rowChecked = this.selection.selected.some(function (el) {
				return (el.id === item.id);
			});
			if (!rowChecked) {
				result = false;
			}
		});
		return result;
	}

	/** Selects all selectable rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.dataSource.data.forEach(
				row => {
					this.selection.select(row);
				});
		}
	}

	searchProducts(query) {
		this.loading = true;
		this.productService.searchProductsAndArticles(query + '&includeProducts=true&includeArticles=false', this.organizationId).subscribe(
			res => {
				this.searchTotal = res.headers.get('X-Total-Count');
				if (this.searchTotal !== '0') {
					this.products = res.body;
				} else {
					this.products = [];
				}
				this.totalPages = Math.ceil(this.searchTotal / this.offset);
				this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
				this.dataSource = new MatTableDataSource<Product>(this.products);
				this.generatePaginator();
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	goToPage(page) {
		if (page !== '...' && page !== this.currentPage) {
			this.loading = true;
			this.tmpSelection = this.selection.selected;
			this.currentPage = page;
			this.productService.searchProductsAndArticles(
				this.query + '&includeProducts=true&includeArticles=false&offset=' +
				(this.currentPage - 1) * this.offset, this.organizationId).subscribe(
				res => {
					this.products = res.body;
					this.products.forEach(row => {
						this.tmpSelection.forEach(tmpRow => {
							if (tmpRow.id === row.id) {
								this.selection.deselect(this.selection.selected.find(x => x.id === row.id));
								this.selection.select(row);
							}
						});
					});
					this.dataSource = new MatTableDataSource<Product>(this.products);
					this.currentPage = page;
					this.generatePaginator();
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		}
	}

	generatePaginator() {
		let hasFirstEllipsis = false;
		let hasSecondEllipsis = false;
		this.paginatorArray = [];
		if (this.searchTotal > this.offset) {
			if (this.totalPages < 6) {
				this.paginatorArray = this.allPages;
			} else {
				this.allPages.forEach((pageNumber) => {
					if (pageNumber === 1
						|| pageNumber === this.currentPage
						|| pageNumber === this.totalPages
						|| pageNumber === this.currentPage + 1
						|| pageNumber === this.currentPage - 1) {
						this.paginatorArray.push(pageNumber);
					} else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
						this.paginatorArray.push('...');
						hasFirstEllipsis = true;
					} else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
						this.paginatorArray.push('...');
						hasSecondEllipsis = true;
					}
				});
			}
		}
		this.loading = false;
	}

	done() {
		this.dialogRef.close(this.selection.selected);
	}

}
