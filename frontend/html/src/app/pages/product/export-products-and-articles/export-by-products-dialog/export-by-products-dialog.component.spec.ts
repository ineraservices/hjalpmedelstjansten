import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ExportByProductsDialogComponent} from './export-by-products-dialog.component';

describe('ExportByProductsDialogComponent', () => {
	let component: ExportByProductsDialogComponent;
	let fixture: ComponentFixture<ExportByProductsDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ExportByProductsDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ExportByProductsDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
