import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ExportProductsAndArticlesComponent} from './export-products-and-articles.component';

describe('ExportProductsAndArticlesComponent', () => {
	let component: ExportProductsAndArticlesComponent;
	let fixture: ComponentFixture<ExportProductsAndArticlesComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ExportProductsAndArticlesComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ExportProductsAndArticlesComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
