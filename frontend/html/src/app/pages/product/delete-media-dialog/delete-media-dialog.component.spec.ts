import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DeleteMediaDialogComponent} from './delete-media-dialog.component';

describe('DeleteMediaDialogComponent', () => {
	let component: DeleteMediaDialogComponent;
	let fixture: ComponentFixture<DeleteMediaDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DeleteMediaDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DeleteMediaDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
