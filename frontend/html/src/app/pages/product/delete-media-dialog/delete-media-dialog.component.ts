import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UploadDialogComponent} from '../upload-dialog/upload-dialog.component';
import {ProductService} from '../../../services/product.service';
import {AlertService} from '../../../services/alert.service';

@Component({
	selector: 'app-delete-media-dialog',
	templateUrl: './delete-media-dialog.component.html',
	styleUrls: ['./delete-media-dialog.component.scss']
})
export class DeleteMediaDialogComponent {

	loading = false;


	constructor(public dialogRef: MatDialogRef<UploadDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private productService: ProductService,
							private alertService: AlertService) {
	}

	done() {
		console.log('done has been pressed');
		this.loading = true;
		if (this.data.productId) {
			this.productService.deleteMediaFromProduct(this.data.organizationId, this.data.productId, this.data.media.id).subscribe(
				res => {
					this.alertService.clear();
					this.alertService.success('Sparat');
					this.dialogRef.close('success');
					this.loading = false;
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
						this.loading = false;
					});
				});
		} else {
			this.productService.deleteMediaFromArticle(this.data.organizationId, this.data.articleId, this.data.media.id).subscribe(
				res => {
					this.alertService.clear();
					this.alertService.success('Sparat');
					this.dialogRef.close('success');
					this.loading = false;
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
						this.loading = false;
					});
				});
		}
	}

}
