import {Component, Inject} from '@angular/core';
import {AlertService} from '../../../services/alert.service';
import {UploadDialogComponent} from '../upload-dialog/upload-dialog.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProductService} from '../../../services/product.service';
import {ErrorMessage} from '../../../models/error-message.model';

@Component({
	selector: 'app-import-dialog',
	templateUrl: './import-dialog.component.html',
	styleUrls: ['./import-dialog.component.scss']
})
export class ImportDialogComponent {

	file;
	formData: FormData;
	errors: Array<ErrorMessage>;
	importInProgress = false;
	showImportText = false;

	constructor(public dialogRef: MatDialogRef<UploadDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private productService: ProductService,
							private alertService: AlertService) {
	}

	fileSelected(file) {
		this.file = file;
	}

	import() {
		this.importInProgress = true;
		this.showImportText = false;
		this.formData = new FormData();
		this.formData.append('file', this.file);
		this.errors = new Array<ErrorMessage>();

		this.productService.import(this.data.organizationId, this.formData).subscribe(
			res => {
				setTimeout(function () {
					console.log(res);
				}, 5000);
				this.importInProgress = false;
				this.showImportText = true;
				this.file = null;
				this.alertService.clear();
				this.alertService.success('Inläsning påbörjad - Resultatet kommer att skickas till din e-post. Status för importen hittas under fliken Inläsningar.');
			}, error => {
				this.importInProgress = false;
				this.alertService.clear();
				this.errors = new Array<ErrorMessage>();
				this.alertService.errorWithMessage('Någonting gick fel');
				error.error.errors.forEach(err => {
					this.errors.push(err);
				});
			});
	}

}
