import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CategoryDialogAdminSpecificComponent} from './category-dialog-admin-specific.component';

describe('CategoryDialogAdminSpecificComponent', () => {
	let component: CategoryDialogAdminSpecificComponent;
	let fixture: ComponentFixture<CategoryDialogAdminSpecificComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [CategoryDialogAdminSpecificComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CategoryDialogAdminSpecificComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
