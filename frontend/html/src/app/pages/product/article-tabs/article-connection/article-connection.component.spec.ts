import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ArticleConnectionComponent} from './article-connection.component';

describe('ArticleConnectionComponent', () => {
	let component: ArticleConnectionComponent;
	let fixture: ComponentFixture<ArticleConnectionComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ArticleConnectionComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ArticleConnectionComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
