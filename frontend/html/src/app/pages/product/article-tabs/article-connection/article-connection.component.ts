import {Component} from '@angular/core';
import {ProductService} from '../../../../services/product.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {Article} from '../../../../models/product/article.model';
import {ConnectionDialogComponent} from '../../connection-dialog/connection-dialog.component';
import {AlertService} from '../../../../services/alert.service';
import {HelptextService} from '../../../../services/helptext.service';
import {AuthService} from '../../../../auth/auth.service';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {CommonService} from '../../../../services/common.service';

@Component({
	selector: 'app-article-connection',
	templateUrl: './article-connection.component.html',
	styleUrls: ['./article-connection.component.scss']
})
export class ArticleConnectionComponent {

	displayedColumns = ['typ', 'benämning', 'produkt/artnr', 'status', 'kategori', 'disconnect'];

	redirectedFromSearch = false;
	loaded = false;
	loading = false;
	isOwnOrganization = false;
	organizationId: number;
	articleId: number;
	article: Article;
	connections = [];
	dataSource = null;
	query = '';
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	queryParams = '';
	searchTotal;
	offset = 25;
	totalPages;
	currentPage = 1;
	paginatorArray = [];
	allPages;
	helpTexts;
	helpTextsLoaded = false;
	flexDirection: string;

	// Filters
	filterProducts = false;
	filterArticles = false;
	filterH = false;
	filterT = false;
	filterR = false;
	filterTj = false;
	filterI = false;
	filterPublished = false;
	filterDiscontinued = false;

	constructor(private authService: AuthService,
							private productService: ProductService,
							private route: ActivatedRoute,
							private connectionDialog: MatDialog,
							private alertService: AlertService,
							private helpTextService: HelptextService,
							private commonService: CommonService,
							private router: Router) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}
		this.route.params.subscribe(params => {
			this.organizationId = params.organizationId;
			this.articleId = params.articleId;
			this.isOwnOrganization = Number(this.organizationId) === this.authService.getOrganizationId();
		});
		this.route.queryParams.subscribe(qParams => {
			if (qParams.search) {
				this.redirectedFromSearch = qParams.search;
			}
		});
		// The user is considered done with their search after 500 ms without further input
		this.searchChanged.pipe(
			debounceTime(500),
			distinctUntilChanged())
			.subscribe(query => {
				this.query = encodeURIComponent(query);
				this.onQueryChange();
			});
	}

	getArticle() {
		if (!this.helpTextsLoaded) {
			this.helpTexts = this.helpTextService.getTexts().subscribe(
				texts => {
					this.helpTexts = texts;
					this.helpTextsLoaded = true;
				}
			);
		}
		this.productService.getArticle(this.organizationId, this.articleId).subscribe(
			res => {
				this.article = res;
				this.searchConnectedArticles('');
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	initConnections(article) {
		if (!this.helpTextsLoaded) {
			this.helpTexts = this.helpTextService.getTexts().subscribe(
				texts => {
					this.helpTexts = texts;
					this.helpTextsLoaded = true;
				}
			);
		}
		this.article = article;
		this.connections = [];
		this.checkFitsToProducts();
		this.checkFitsToArticles();
		this.loaded = true;
	}

	checkFitsToProducts() {
		if (this.article.fitsToProducts && this.article.fitsToProducts.length) {
			this.article.fitsToProducts.forEach((element) => {
				this.productService.getProduct(this.organizationId, element.id).subscribe(
					result => {
						this.connections.push({
							'articleType': result.category.articleType,
							'name': result.productName,
							'number': result.productNumber,
							'status': result.status,
							'code': result.category.code,
							'id': result.id,
							'type': 'PRODUCT'
						});
						this.dataSource = new MatTableDataSource<Connection>(this.connections);
					}, error => {
						this.alertService.clear();
						error.error.errors.forEach(err => {
							this.alertService.error(err);
						});
					}
				);
			});

		} else {
			this.article.fitsToProducts = [];
		}
	}

	checkFitsToArticles() {
		if (this.article.fitsToArticles && this.article.fitsToArticles.length) {
			this.article.fitsToArticles.forEach((element) => {
				this.productService.getArticle(this.organizationId, element.id).subscribe(
					result => {
						this.connections.push({
							'articleType': result.category.articleType,
							'name': result.articleName,
							'number': result.articleNumber,
							'status': result.status,
							'code': result.category.code,
							'id': result.id,
							'type': 'ARTICLE'
						});
						this.dataSource = new MatTableDataSource<Connection>(this.connections);
					}, error => {
						this.alertService.clear();
						error.error.errors.forEach(err => {
							this.alertService.error(err);
						});
					}
				);
			});

		} else {
			this.article.fitsToArticles = [];
		}
	}

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
	}

	onQueryChange() {
		this.onFilterChange();
	}

	onFilterChange() {
		this.queryParams = '';
		this.currentPage = 1;
		if (!this.filterArticles) {
			this.filterH = false;
			this.filterT = false;
			this.filterR = false;
			this.filterTj = false;
			this.filterI = false;
		}
		if (this.filterProducts) {
			this.queryParams += '&includeProducts=true';
		} else {
			this.queryParams += '&includeProducts=false';
		}
		if (this.filterArticles) {
			this.queryParams += '&includeArticles=true';
		} else {
			this.queryParams += '&includeArticles=false';
		}
		if (this.filterH) {
			this.queryParams += '&type=H';
		}
		if (this.filterT) {
			this.queryParams += '&type=T';
		}
		if (this.filterR) {
			this.queryParams += '&type=R';
		}
		if (this.filterTj) {
			this.queryParams += '&type=Tj';
		}
		if (this.filterI) {
			this.queryParams += '&type=I';
		}
		if (this.filterPublished) {
			this.queryParams += '&status=PUBLISHED';
		}
		if (this.filterDiscontinued) {
			this.queryParams += '&status=DISCONTINUED';
		}
		this.searchConnectedArticles(this.query + this.queryParams);
	}

	searchConnectedArticles(query) {
		this.loading = true;
		this.productService.searchConnectedArticles(query, this.organizationId, this.articleId).subscribe(
			res => {
				this.searchTotal = res.headers.get('X-Total-Count');
				if (this.searchTotal !== '0') {
					this.connections = res.body;
				} else {
					this.connections = [];
				}
				this.totalPages = Math.ceil(this.searchTotal / this.offset);
				this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
				this.dataSource = new MatTableDataSource<Connection>(this.connections);
				this.generatePaginator();
				this.loaded = true;
				this.loading = false;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	goToPage(page) {
		if (page !== '...' && page !== this.currentPage) {
			this.currentPage = page;
			this.productService.searchConnectedArticles(
				this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset, this.organizationId, this.articleId).subscribe(
				res => {
					this.connections = res.body;
					this.dataSource = new MatTableDataSource<Connection>(this.connections);
					this.currentPage = page;
					this.generatePaginator();
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		}
	}

	generatePaginator() {
		let hasFirstEllipsis = false;
		let hasSecondEllipsis = false;
		this.paginatorArray = [];
		if (this.searchTotal > this.offset) {
			if (this.totalPages < 6) {
				this.paginatorArray = this.allPages;
			} else {
				this.allPages.forEach((pageNumber) => {
					if (pageNumber === 1
						|| pageNumber === this.currentPage
						|| pageNumber === this.totalPages
						|| pageNumber === this.currentPage + 1
						|| pageNumber === this.currentPage - 1) {
						this.paginatorArray.push(pageNumber);
					} else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
						this.paginatorArray.push('...');
						hasFirstEllipsis = true;
					} else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
						this.paginatorArray.push('...');
						hasSecondEllipsis = true;
					}
				});
			}
		}
		this.loading = false;
	}

	openConnectionDialog() {
		const dialogRef = this.connectionDialog.open(ConnectionDialogComponent, {
			width: '80%',
			data: {
				'organizationId': this.organizationId,
				'articleType': this.article.category.articleType,
				'article': this.article
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				if (!this.articleId) {
					this.articleId = result.id;
					this.article.id = result.id;
				}
				this.searchConnectedArticles(this.query);
			}
		});
	}

	onRowClick(type) {
		if (type === 'ARTICLE') {
			location.reload();
		}
	}

	removeConnection(element) {
		this.productService.removeConnection(this.organizationId, this.articleId, element.id).subscribe(
			res => {
				this.article = res;
				this.searchConnectedArticles(this.query);
				this.alertService.success('Koppling borttagen');
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	async routeTo(element: any): Promise<void> {
		const href = "/organization/" + this.organizationId + "/" + element.type.toLowerCase() + "/" + element.id + "/handle";
		await this.router.navigateByUrl(!!this.redirectedFromSearch ? href : href + "?search=true");
		this.onRowClick(element.type);
	}

}

export interface Connection {
	type: string;
	name: string;
	number: string;
	status: string;
	code: string;
	deleteable: boolean;
}
