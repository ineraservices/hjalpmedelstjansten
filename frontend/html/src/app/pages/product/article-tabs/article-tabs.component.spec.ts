import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ArticleTabsComponent} from './article-tabs.component';

describe('ArticleTabsComponent', () => {
	let component: ArticleTabsComponent;
	let fixture: ComponentFixture<ArticleTabsComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ArticleTabsComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ArticleTabsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
