import {Component, OnInit, ViewChild} from '@angular/core';
import {HandleArticleComponent} from './handle-article/handle-article.component';
import {ProductService} from '../../../services/product.service';
import {ActivatedRoute} from '@angular/router';
import {Product} from '../../../models/product/product.model';
import {Article} from '../../../models/product/article.model';
import {ArticleConnectionComponent} from './article-connection/article-connection.component';
import {AlertService} from '../../../services/alert.service';
import {OrganizationService} from '../../../services/organization.service';
import {AuthService} from '../../../auth/auth.service';
import {ArticlePricelistComponent} from './article-pricelist/article-pricelist.component';
import {HasPricelistRowResponse} from "../../../models/product/has-pricelist-row-response.model";
import {HelptextService} from '../../../services/helptext.service';
import {saveAs} from "file-saver";

@Component({
	selector: 'app-article-tabs',
	templateUrl: './article-tabs.component.html',
	styleUrls: ['./article-tabs.component.scss']
})
export class ArticleTabsComponent implements OnInit {
	@ViewChild(HandleArticleComponent) childArticle!: HandleArticleComponent;
	@ViewChild(ArticleConnectionComponent) childArticleConnection!: ArticleConnectionComponent;
	@ViewChild(ArticlePricelistComponent) childArticlePricelist!: ArticlePricelistComponent;

	redirectedFromSearch = false;
	loaded = false;
	tabIndex = 0;
	organizationId: number;
	productId: number;
	articleId: number;
	product: Product;
	article: Article;
	haveConnectedArticles: boolean;
	hasPricelistRows: HasPricelistRowResponse = new HasPricelistRowResponse();
	isLeafNode = false;

	query = '';

	queryParams = '';
	helpTexts;
	helpTextsLoaded = false;
	// Permissions

	hasPermissionToViewPricelistRows: boolean;
	hasPermissionToViewPricelistRowsExists: boolean;
	hasPermissionToViewGeneralPricelistPricelistRows: boolean;
	hasPermissionToViewAllGeneralPricelistPricelistRows: boolean;
	isSupplier = false;
	isOwnOrganization = false;
	isServiceOwner = false;
	isCheckingIfArticleIsRemovable: boolean;


	constructor(private route: ActivatedRoute,
							private authService: AuthService,
							private productService: ProductService,
							private organizationService: OrganizationService,
							private alertService: AlertService,
							private helpTextService: HelptextService) {
		this.route.params.subscribe(params => {
			this.organizationId = params.organizationId;
			this.articleId = params.articleId;
		});
		this.route.queryParams.subscribe(qParams => {
			this.productId = qParams.productid;
			if (qParams.search) {
				this.redirectedFromSearch = qParams.search;
			}
		});
		this.isOwnOrganization = Number(this.organizationId) === this.authService.getOrganizationId();
		this.hasPermissionToViewGeneralPricelistPricelistRows = this.hasPermissionToViewGeneralPricelistPricelistRows && this.isOwnOrganization;
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	ngOnInit() {
		this.hasPermissionToViewPricelistRows = this.hasPermission('pricelistrow:view_own');
		this.hasPermissionToViewPricelistRowsExists = this.hasPermission('pricelistrow:view_exists');
		this.hasPermissionToViewGeneralPricelistPricelistRows = this.hasPermission('generalpricelist_pricelistrow:view_own');
		this.hasPermissionToViewAllGeneralPricelistPricelistRows = this.hasPermission('generalpricelist_pricelistrow:view_all');

		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);

		this.organizationService.getOrganization(this.authService.getOrganizationId()).subscribe(
			res => {
				this.isSupplier = res.organizationType === 'SUPPLIER';
				this.isServiceOwner = res.organizationType === 'SERVICE_OWNER';
				if (this.isSupplier) {
					this.hasPermissionToViewPricelistRows = this.hasPermissionToViewPricelistRows && this.isOwnOrganization;
					this.hasPermissionToViewPricelistRowsExists = this.hasPermissionToViewPricelistRowsExists && this.isOwnOrganization;
				}
				if (this.productId) {
					this.productService.getProduct(this.organizationId, this.productId).subscribe(
						product => {
							this.product = product;
							this.loaded = true;
						}, error => {
							this.alertService.clear();
							error.error.errors.forEach(err => {
								this.alertService.error(err);
							});
						}
					);
				} else {
					this.loaded = true;
				}
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
		if (this.articleId) {
			this.productService.hasPricelistRowsForArticle(this.organizationId, this.articleId).subscribe(
				res => {
					this.hasPricelistRows = res;
				}
			);
		}
		if (this.articleId) {
			this.productService.getArticle(this.organizationId, this.articleId).subscribe(
				res => {
					const articleType = res.category.articleType.toLowerCase();
					if (articleType === "t" || articleType === "r" || articleType === "i" || articleType === "tj") {
						this.isLeafNode = true;
					}
				});

			this.isCheckingIfArticleIsRemovable = true;
			this.productService.searchConnectedArticles(this.query, this.organizationId, this.articleId).subscribe(
				res => {
					this.haveConnectedArticles = false;
					if (res.body) {
						res.body.forEach(item => {
							if (item['deleteable']) {
								this.haveConnectedArticles = true;
							}
						}
						);
					}
				}
			);
		}
		this.isCheckingIfArticleIsRemovable = false;
	}

	toggleEditMode() {
		this.childArticle.toggleEditMode();
	}

	exportSearchResults() {
		const query = '?query=' + this.childArticleConnection.query + this.childArticleConnection.queryParams;

		this.productService.exportProductsAndArticlesForArticle(this.organizationId, this.articleId, query).subscribe(
			data => {
				saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	onTabSelection(event, i) {
		this.tabIndex = event.index;
		this.childArticle.tabIndex = event.index;
		if (event.index === 1) {
			this.childArticleConnection.isOwnOrganization = this.childArticle.isOwnOrganization;
			this.article = this.childArticle.article;
			if (this.article.ceMarked) {
				this.article.ceDirective = this.childArticle.selectedCeDirective;
			}
			this.article.articleNumber = this.childArticle.articleNumberFormControl.value;
			this.article.articleName = this.childArticle.articleNameFormControl.value;
			this.article.basedOnProduct = this.childArticle.selectedBasedOnProduct;
			this.article.ceStandard = this.childArticle.selectedCeStandard;
			if (this.childArticle.selectedPreventiveMaintenanceValidity) {
				this.article.preventiveMaintenanceValidFrom = this.childArticle.selectedPreventiveMaintenanceValidity;
			} else {
				this.article.preventiveMaintenanceValidFrom = null;
			}
			this.article.orderUnit = this.childArticle.selectedOrderUnit;
			this.article.articleQuantityInOuterPackageUnit = this.childArticle.selectedArticleQuantityInOuterPackageUnit;
			this.article.packageContentUnit = this.childArticle.selectedPackageContentUnit;
			if (!this.childArticle.articleId) {
				this.childArticleConnection.initConnections(this.article);
			} else {
				this.childArticleConnection.articleId = this.childArticle.articleId;
				this.childArticleConnection.getArticle();
			}
		} else if (event.index === 0 && this.childArticleConnection.articleId) {
			this.childArticle.loaded = false;
			if (i === 1) {
				this.articleId = this.childArticleConnection.articleId;
				this.article.id = this.childArticleConnection.articleId;
				this.childArticle.articleId = this.childArticleConnection.articleId;
				this.childArticle.article.id = this.childArticleConnection.articleId;
				this.childArticle.article.fitsToArticles = this.childArticleConnection.article.fitsToArticles;
				this.childArticle.article.fitsToProducts = this.childArticleConnection.article.fitsToProducts;
				this.childArticle.getMedia();
			} else {
				this.childArticle.initialize();
			}
		}
		if (event.index === 2) {
			this.childArticlePricelist.getPricelists(this.isSupplier, 0, 0);
		}
	}

}
