import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SoDeleteArticleDialogComponent} from './so-delete-article-dialog.component';

describe('SoDeleteArticleDialogComponent', () => {
	let component: SoDeleteArticleDialogComponent;
	let fixture: ComponentFixture<SoDeleteArticleDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [SoDeleteArticleDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SoDeleteArticleDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
