import {Component, OnInit} from '@angular/core';
import {OrganizationService} from '../../../../services/organization.service';
import {Category} from '../../../../models/product/category.model';
import {Product} from '../../../../models/product/product.model';
import {CeArray} from '../../../../models/product/ce-array';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../../../../services/product.service';
import {Article} from '../../../../models/product/article.model';
import {AuthService} from '../../../../auth/auth.service';
import {ElectronicAddress} from '../../../../models/electronic-address.model';
import {forkJoin} from 'rxjs';
import {Ce} from '../../../../models/product/ce.model';
import {CategoryDialogComponent} from '../../category-dialog/category-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {OrderUnit} from '../../../../models/product/order-unit.model';
import {AlertService} from '../../../../services/alert.service';
import {Location} from '@angular/common';
import {CategoryProperty} from '../../../../models/product/category-property.model';
import {PreventiveMaintenance} from '../../../../models/preventive-maintenance.model';
import {CommonService} from '../../../../services/common.service';
import {Media} from '../../../../models/product/media.model';
import {Image} from '../../../../models/product/image.model';
import {UploadDialogComponent} from '../../upload-dialog/upload-dialog.component';
import {DeleteMediaDialogComponent} from '../../delete-media-dialog/delete-media-dialog.component';
import {DeleteArticleDialogComponent} from "./delete-article-dialog/delete-article-dialog.component";
import {DocumentType} from '../../../../models/document-type.model';
import {FormControl, Validators} from '@angular/forms';
import {ArticleReplacementDialogComponent} from './article-replacement-dialog/article-replacement-dialog.component';
import {HelptextService} from '../../../../services/helptext.service';
import {PackageUnit} from '../../../../models/product/package-unit.model';
import {ArticleCategoryProperty} from '../../../../models/product/article-category-property.model';
import {ChangeBasedOnDialogComponent} from './change-based-on-dialog/change-based-on-dialog.component';
import {SoDeleteArticleDialogComponent} from "./so-delete-article-dialog/so-delete-article-dialog.component";
import {EditMediaDialogComponent} from "../../edit-media-dialog/edit-media-dialog.component";

@Component({
	selector: 'app-handle-article',
	templateUrl: './handle-article.component.html',
	styleUrls: ['./handle-article.component.scss']
})
export class HandleArticleComponent implements OnInit {
	loaded = false;
	saving = false;
	reactivating = false;
	tabIndex = 0;
	editMode = false;
	organizationId: number;
	productId: number;
	articleId: number;
	copiedArticleId: number;
	isTiso = false;
	supplier: string;
	product: Product;
	products: Array<Product>;
	article: Article = new Article();
	media: Media;
	mainImage: Image;
	otherImages: Array<Image>;
	extendedCategories = '';
	codelessTCategory;
	ce: CeArray = new CeArray();
	categoryProperties: Array<CategoryProperty>;
	selectedCeDirective: Ce;
	selectedCeStandard: Ce;
	selectedPreventiveMaintenanceValidity: PreventiveMaintenance;
	preventiveMaintenanceValidity: Array<PreventiveMaintenance>;
	selectedBasedOnProduct: Product;
	replaced = false;
	replacementDate;
	orderUnits: Array<OrderUnit>;
	packageUnits: Array<PackageUnit>;
	documentTypes: Array<DocumentType>;
	selectedOrderUnit: OrderUnit;
	selectedArticleQuantityInOuterPackageUnit: OrderUnit;
	selectedPackageContentUnit: PackageUnit;
	customerUniquenessToggleable = true;
	helpTexts;
	helpTextsLoaded = false;
	flexDirection: string;
	codeForTRITJ: Category;

	isSupplier = false;
	isColorOverridden = false;
	isCustomerUniqueOverridden = false;
	isCeMarkedOverridden = false;
	isOrderInformationOverridden = false;
	isCeDirectiveOverridden = false;
	isCeStandardOverridden = false;
	isPreventiveMaintenanceDescriptionOverridden = false;
	isPreventiveMaintenanceNumberOfDaysOverridden = false;
	isPreventiveMaintenanceValidFromOverridden = false;
	isManufacturerArticleNumberOverridden = false;
	isManufacturerElectronicAddressOverridden = false;
	isManufacturerOverridden = false;
	isTrademarkOverridden = false;
	isSupplementedInformationOverridden = false;
	isStatusOverridden = false;


	// Form controls
	basedOnProductFormControl = new FormControl(null, {
		updateOn: 'change',
	});

	articleNumberFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	articleNameFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	discontinuedDateFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	categoryFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	// Validation errors
	articleNumberError;
	articleNameError;
	categoryError;
	productError;

	// Permissions
	hasPermissionToCreateArticle: boolean;
	hasPermissionToUpdateArticle: boolean;
	hasPermissionToUploadMedia: boolean;
	hasPermissionToDeleteMedia: boolean;
	isOwnOrganization = false;

	constructor(private router: Router,
							private productService: ProductService,
							private organizationService: OrganizationService,
							private commonService: CommonService,
							private route: ActivatedRoute,
							private authService: AuthService,
							private alertService: AlertService,
							private dialog: MatDialog,
							private location: Location,
							private helpTextService: HelptextService) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}
		this.route.params.subscribe(params => {
			this.organizationId = params.organizationId;
			this.articleId = params.articleId;
		});

		this.route.queryParams.subscribe(qParams => {
			this.copiedArticleId = qParams.copiedArticle;
			if (qParams.productid) {
				this.productId = Number(qParams.productid);
			}
			this.article.category = new Category();
			if (qParams.articletype) {
				if (qParams.articletype === 'Tiso') {
					this.article.category.articleType = 'T';
					this.isTiso = true;
				} else {
					this.article.category.articleType = qParams.articletype;
				}
			}
		});

		this.isOwnOrganization = Number(this.organizationId) === this.authService.getOrganizationId();
		this.hasPermissionToCreateArticle = this.hasPermissionToCreateArticle && this.isOwnOrganization;
		this.hasPermissionToUpdateArticle = this.hasPermissionToUpdateArticle && this.isOwnOrganization;
	}

	ngOnInit() {
		this.hasPermissionToCreateArticle = this.hasPermission('article:create_own');
		this.hasPermissionToUpdateArticle = this.hasPermission('article:update_own');
		this.hasPermissionToUploadMedia = this.hasPermission('media:create');
		this.hasPermissionToDeleteMedia = this.hasPermission('media:delete');

		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);

		this.organizationService.getOrganization(this.authService.getOrganizationId()).subscribe(
			res => {
				this.isSupplier = res.organizationType === 'SUPPLIER';
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

		this.onEditModeChange();
		if (this.isOwnOrganization) {
			this.productService.getProductsByStatus(this.organizationId, "DISCONTINUED", "PUBLISHED").subscribe(
				res => {
					this.products = res;
					this.initialize();
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		} else {
			this.initialize();
		}

	}

	initialize() {
		forkJoin([this.productService.getCE(),
												this.organizationService.getOrganization(this.organizationId),
												this.productService.getOrderUnits(),
												this.commonService.getPreventiveMaintenances(),
												this.commonService.getDocumentTypes(),
												this.productService.getPackageUnits()])
			.subscribe(
				data => {
					this.ce = data[0];
					this.supplier = data[1].organizationName;
					this.orderUnits = data[2];
					this.preventiveMaintenanceValidity = data[3];
					this.documentTypes = data[4];
					this.packageUnits = data[5];

					if (!this.articleId && !this.copiedArticleId) {
						// Set default values

						if (!this.productId) {
							this.getProductsByArticleType(this.article.category.articleType);
						} else {
							this.product = this.products.find(e => e.id === this.productId);
						}
						this.editMode = true;
						this.onEditModeChange();

						this.article.extendedCategories = new Array<Category>();
						this.article.manufacturerElectronicAddress = new ElectronicAddress();
						this.article.ceMarked = false;
						this.article.customerUnique = false;
						if (this.article.category.articleType !== 'H' && !this.isTiso) {
							this.productService.getCategories('?type=' + this.article.category.articleType).subscribe(
								category => {
									this.article.category = category[0];
									if (this.article.category.articleType === 'T') {
										this.codelessTCategory = category[0];
									}
									if (category[0].code) {
										this.categoryFormControl.setValue(category[0].code + '' + category[0].name);
									} else {
										this.categoryFormControl.setValue(category[0].name);
									}
								}
							);
						} else {
							this.categoryFormControl.disable();
						}
						if (this.productId && (this.article.category.articleType === 'H' ||
							(this.isTiso && this.product.category.articleType === 'T' && this.product.category.code))) {
							this.selectedBasedOnProduct = this.products.find(e => e.id === this.productId);
							this.onBasedOnSelection();
						} else if (this.productId) {
							this.article.fitsToProducts = [];
							this.productService.getProduct(this.organizationId, this.productId).subscribe(
								result => {
									this.article.fitsToProducts.push({'id': result.id});
								}
							);
						}
						this.loaded = true;
						this.selectedOrderUnit = this.orderUnits[0];
					} else {
						this.getArticle(true);
					}
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
	}

	getProductsByArticleType(articleType) {
		this.productService.getProductsByType(this.organizationId, articleType).subscribe(
			res => {
				this.products = res;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	getMedia() {
		this.otherImages = new Array<Image>();
		this.productService.getMediaForArticle(this.organizationId, this.articleId).subscribe(
			media => {
				this.media = media;
				this.media.images.forEach(image => {
					if (image.mainImage) {
						this.mainImage = image;
					} else {
						this.otherImages.push(image);
					}
				});
				this.loaded = true;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	getArticle(loadMedia) {
		const articleId = this.articleId ? this.articleId : this.copiedArticleId;
		this.productService.getArticle(this.organizationId, articleId).subscribe(
			res => {
				this.article = res;
				this.article.categoryPropertys.sort((a, b) => a.property.orderIndex - b.property.orderIndex);
				this.articleNameFormControl.setValue(res.articleName);
				this.selectedCeDirective = null;
				this.selectedCeStandard = null;
				this.selectedBasedOnProduct = null;
				this.selectedPreventiveMaintenanceValidity = null;
				this.selectedPackageContentUnit = null;
				this.selectedOrderUnit = null;
				this.selectedArticleQuantityInOuterPackageUnit = null;

				// overridden
				this.isCustomerUniqueOverridden = this.article.customerUniqueOverridden;
				this.isOrderInformationOverridden = this.article.orderInformationOverridden;
				this.isCeDirectiveOverridden = this.article.ceDirectiveOverridden;
				this.isCeMarkedOverridden = this.article.ceMarkedOverridden;
				this.isCeStandardOverridden = this.article.ceStandardOverridden;
				this.isPreventiveMaintenanceDescriptionOverridden = this.article.preventiveMaintenanceDescriptionOverridden;
				this.isPreventiveMaintenanceNumberOfDaysOverridden = this.article.preventiveMaintenanceNumberOfDaysOverridden;
				this.isPreventiveMaintenanceValidFromOverridden = this.article.preventiveMaintenanceValidFromOverridden;
				this.isManufacturerArticleNumberOverridden = this.article.manufacturerArticleNumberOverridden;
				this.isManufacturerElectronicAddressOverridden = this.article.manufacturerElectronicAddressOverridden;
				this.isManufacturerOverridden = this.article.manufacturerOverridden;
				this.isTrademarkOverridden = this.article.trademarkOverridden;
				this.isColorOverridden = this.article.colorOverridden;
				this.isSupplementedInformationOverridden = this.article.supplementedInformationOverridden;
				this.isStatusOverridden = this.article.statusOverridden;


				if (!this.article.manufacturerElectronicAddress) {
					this.article.manufacturerElectronicAddress = new ElectronicAddress();
				}
				if (this.article.ceDirective) {
					this.selectedCeDirective = this.ce.directives.find(e => e.id === this.article.ceDirective.id);
				}
				if (this.article.ceStandard) {
					this.selectedCeStandard = this.ce.standards.find(e => e.id === this.article.ceStandard.id);
				}
				if (this.article.orderUnit) {
					this.selectedOrderUnit = this.orderUnits.find(e => e.id === this.article.orderUnit.id);
				}
				if (this.article.articleQuantityInOuterPackageUnit) {
					this.selectedArticleQuantityInOuterPackageUnit = this.orderUnits.find(
						e => e.id === this.article.articleQuantityInOuterPackageUnit.id);
				}
				if (this.article.packageContentUnit) {
					this.selectedPackageContentUnit = this.packageUnits.find(e => e.id === this.article.packageContentUnit.id);
				}
				if (this.article.basedOnProduct) {
					if (this.products) {
						this.selectedBasedOnProduct = this.products.find(e => e.id === this.article.basedOnProduct.id);
					} else {
						this.products = new Array<Product>();
						this.products.push(this.article.basedOnProduct);
						this.selectedBasedOnProduct = this.products.find(e => e.id === this.article.basedOnProduct.id);

					}
				}
				if (this.article.category.code) {
					this.categoryFormControl.setValue(this.article.category.code + ' ' + this.article.category.name);
				} else {
					this.categoryFormControl.setValue(this.article.category.name);
				}
				this.getCategoryCodeForCodelessArticle();
				this.extendedCategories = '';
				if (this.article.extendedCategories) {
					this.article.extendedCategories.forEach((e) => {
						if (this.extendedCategories) {
							this.extendedCategories += '\n';
						}
						this.extendedCategories += e.code ? e.code + ' ' + e.name : e.name;
					});
				}
				if (this.article.preventiveMaintenanceValidFrom) {
					this.selectedPreventiveMaintenanceValidity = this.preventiveMaintenanceValidity.find(
						e => e.code === this.article.preventiveMaintenanceValidFrom.code);
				}
				if (this.articleId) {
					this.articleNumberFormControl.setValue(res.articleNumber);
					if (loadMedia) {
						this.getMedia();
					}
					if (this.article.replacementDate) {
						this.replacementDate = this.convertToDateString(this.article.replacementDate);
						if (this.article.replacedByArticles && this.article.replacedByArticles.length) {
							this.replaced = true;
						}
					}
				} else {
					this.article.articleNumber = null;
					this.article.gtin = null;
					this.article.replacementDate = null;
					this.article.replacedByArticles = null;
					this.editMode = true;
					this.onEditModeChange();
					this.loaded = true;
				}
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	toggleEditMode() {
		this.editMode = !this.editMode;
		this.onEditModeChange();
	}

	onEditModeChange() {
		if (!this.editMode) {
			this.basedOnProductFormControl.disable();
			this.discontinuedDateFormControl.disable();
			this.articleNumberFormControl.disable();
			this.articleNameFormControl.disable();
			this.categoryFormControl.disable();
		} else {
			this.basedOnProductFormControl.enable();
			this.discontinuedDateFormControl.enable();
			this.articleNameFormControl.enable();
			if (this.article.category.articleType !== 'H' && !this.isTiso && !this.article.basedOnProduct) {
				this.categoryFormControl.enable();
			}
			if (!this.articleId || this.article.numberEditable) {
				this.articleNumberFormControl.enable();
			}
		}
	}

	basedOnChanged(product) {
		this.selectedBasedOnProduct = product;
		this.onBasedOnSelection();
	}

	// Inherit values from the product this article is based on
	onBasedOnSelection() {
		this.selectedCeStandard = null;
		this.selectedCeDirective = null;
		if (this.selectedBasedOnProduct) {
			this.productService.getProduct(this.organizationId, this.selectedBasedOnProduct.id).subscribe(
				res => {
					// Categories
					this.article.category = res.category;
					if (res.category.code) {
						this.categoryFormControl.setValue(res.category.code + ' ' + res.category.name);
					} else {
						this.categoryFormControl.setValue(res.category.name);
					}
					// this.getCategoryCodeForCodelessArticle();
					this.extendedCategories = '';
					if (res.extendedCategories) {
						this.article.extendedCategories = res.extendedCategories;
						res.extendedCategories.forEach((e) => {
							if (this.extendedCategories) {
								this.extendedCategories += '\n';
							}
							if (e.code) {
								this.extendedCategories += e.code + ' ' + e.name;
							} else {
								this.extendedCategories += e.name;
							}
						});
					}

					// Ce
					this.article.ceMarked = res.ceMarked;
					if (res.ceMarked) {
						if (res.ceDirective) {
							this.selectedCeDirective = this.ce.directives.find(e => e.id === res.ceDirective.id);
						}
					}
					if (res.ceStandard) {
						this.selectedCeStandard = this.ce.standards.find(e => e.id === res.ceStandard.id);
					}

					// Preventive maintenance
					if (res.preventiveMaintenanceValidFrom) {
						this.selectedPreventiveMaintenanceValidity = this.preventiveMaintenanceValidity.find(
							e => e.code === res.preventiveMaintenanceValidFrom.code);
					}
					this.article.preventiveMaintenanceNumberOfDays = res.preventiveMaintenanceNumberOfDays;

					// Manufacturer
					this.article.manufacturer = res.manufacturer;
					if (res.manufacturerProductNumber) {
						this.article.manufacturerArticleNumber = res.manufacturerProductNumber;
					}
					if (res.manufacturerElectronicAddress) {
						this.article.manufacturerElectronicAddress = res.manufacturerElectronicAddress;
					}
					this.article.trademark = res.trademark;

					// Order information
					if (res.orderUnit) {
						this.selectedOrderUnit = this.orderUnits.find(e => e.id === res.orderUnit.id);
					}
					if (res.articleQuantityInOuterPackageUnit) {
						this.selectedArticleQuantityInOuterPackageUnit = this.orderUnits.find(
							e => e.id === res.articleQuantityInOuterPackageUnit.id);
					}
					if (res.packageContentUnit) {
						this.selectedPackageContentUnit = this.packageUnits.find(e => e.id === res.packageContentUnit.id);
					}
					if (res.articleQuantityInOuterPackage) {
						this.article.articleQuantityInOuterPackage = res.articleQuantityInOuterPackage;
					}
					this.article.packageContent = res.packageContent;
					this.article.packageLevelBase = res.packageLevelBase;
					this.article.packageLevelMiddle = res.packageLevelMiddle;
					this.article.packageLevelTop = res.packageLevelTop;

					// Supplemented information
					this.article.supplementedInformation = res.supplementedInformation;
					this.article.color = res.color;
					// category properties
					this.article.categoryPropertys = res.categoryPropertys;

					// customerUnique
					this.article.customerUnique = res.customerUnique;
					this.customerUniquenessToggleable = !res.customerUnique;
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		} else {
			this.article.category = this.codelessTCategory;
			this.categoryFormControl.setValue(this.codelessTCategory);
			this.article.categoryPropertys = null;
		}

	}

	getCategoryCodeForCodelessArticle() {
		// if-statement när en kallar på metoden, om articleType T R I Tj ( ej tiso) gör det här
		if (!this.isTiso) {
			// case 1
			if (this.article.basedOnProduct == null) {
				this.codeForTRITJ = this.article.category;
			} else {
				this.codeForTRITJ = this.article.basedOnProduct.category;
			}


			// PELLE 2020-02-20
			// case 2

			if (this.article.category.code === null) {
				if (this.article.fitsToProducts !== null && this.article.fitsToProducts.length > 0) {
					this.codeForTRITJ = this.article.fitsToProducts[0].category;
				}
			}

			// case 3
			if (this.article.category.code === null) {
				if (this.article.fitsToArticles !== null && this.article.fitsToArticles.length > 0) {
					if (this.article.fitsToArticles[0].basedOnProduct !== null) {
						this.codeForTRITJ = this.article.fitsToArticles[0].basedOnProduct.category;
						// eslint-disable-next-line brace-style
					}
					// kan tänkas att detta ska med? PA 2020-10-05
					else {
						this.codeForTRITJ = this.article.fitsToArticles[0].category;
					}
				}
			}
			if (this.article.category.code === null) {
				if (this.article.fitsToArticles !== null && this.article.fitsToArticles.length > 0) {
					if (this.article.fitsToArticles[0].fitsToProducts != null && this.article.fitsToArticles[0].fitsToProducts.length > 0) {
						this.codeForTRITJ = this.article.fitsToArticles[0].fitsToProducts[0].category;
					}
				}
			}

		}
	}

	openSoDeleteArticleDialog() {
		// this.saving = true;
		const dialogRef = this.dialog.open(SoDeleteArticleDialogComponent, {
			width: '80%',
			data: {'organizationId': this.organizationId, 'articleId': this.articleId}
		});

		dialogRef.afterClosed().subscribe(
			res => {
				// this.saving = false;
				if (res) {
					this.alertService.clear();
					this.router.navigate(['/search/']);
					this.alertService.success('Artikel och samtliga dess kopplingar borttagna');
				}
			},
			error => {
				this.alertService.clear();
				// this.saving = false;
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	openDeleteArticleDialog() {
		// this.saving = true;
		const dialogRef = this.dialog.open(DeleteArticleDialogComponent, {
			width: '80%',
			data: {'organizationId': this.organizationId, 'articleId': this.articleId}
		});

		dialogRef.afterClosed().subscribe(
			res => {
				// this.saving = false;
				if (res) {
					this.alertService.clear();
					// this.alertService.success('Artikel borttagen');
					this.router.navigate(['/organization/' + this.organizationId + '/product']);
					this.alertService.success('Artikel borttagen');
				}
			},
			error => {
				this.alertService.clear();
				// this.saving = false;
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	openChangeBasedOnDialog() {
		const productSelection = this.products.filter(product => product.id !== this.selectedBasedOnProduct.id && product.status !== 'DISCONTINUED');
		const dialogRef = this.dialog.open(ChangeBasedOnDialogComponent, {
			width: '90%',
			data: {
				organizationId: this.organizationId,
				articleId: this.article.id,
				products: productSelection
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.getArticle(true);
			}
		});
	}

	openMainCategoryDialog() {
		const dialogRef = this.dialog.open(CategoryDialogComponent, {
			width: '90%',
			data: {
				excludeCodeCategories: true
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				if (result.code) {
					this.categoryFormControl.setValue(result.code + ' ' + result.name);
				} else {
					this.categoryFormControl.setValue(result.name);
				}
				this.article.category = result;
				this.productService.getCategoryProperties(result.id).subscribe(
					res => {
						this.categoryProperties = res;
						this.article.categoryPropertys = new Array<ArticleCategoryProperty>();
						if (this.categoryProperties) {
							this.categoryProperties.forEach(prop => {
								const tmp = new ArticleCategoryProperty();
								tmp.property = prop;
								tmp.textValue = null;
								tmp.decimalValue = null;
								tmp.intervalFromValue = null;
								tmp.intervalToValue = null;
								tmp.singleListValue = null;
								tmp.multipleListValue = null;
								this.article.categoryPropertys.push(tmp);
							});
						}
					}, error => {
						this.alertService.clear();
						error.error.errors.forEach(err => {
							this.alertService.error(err);
						});
					}
				);
			}
		});
	}

	openExtendedCategoryDialog() {
		const dialogRef = this.dialog.open(CategoryDialogComponent, {
			width: '90%',
			data: {
				'selectedCategories': this.article.extendedCategories && this.article.extendedCategories.length ?
					this.article.extendedCategories : []
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.article.extendedCategories = result;
				this.extendedCategories = '';
				result.forEach((e) => {
					if (this.extendedCategories) {
						this.extendedCategories += '\n';
					}
					if (e.code) {
						this.extendedCategories += e.code + ' ' + e.name;
					} else {
						this.extendedCategories += e.name;
					}
				});
			}
		});
	}

	chooseReplacementArticle() {
		const dialogRef = this.dialog.open(ArticleReplacementDialogComponent, {
			width: '80%',
			data: {'article': this.article, 'organizationId': this.organizationId}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.editMode = false;
				this.onEditModeChange();
				this.article = result.article;
				if (!this.article.manufacturerElectronicAddress) {
					this.article.manufacturerElectronicAddress = new ElectronicAddress();
				}
				this.replacementDate = this.convertToDateString(this.article.replacementDate);

				this.isStatusOverridden = this.article.statusOverridden;
				if (result.replaced) {
					this.replaced = true;
				} else {
					this.replaced = false;
				}
			}
		});
	}

	saveArticle() {
		this.saving = true;
		this.resetValidation();
		this.article.status = 'PUBLISHED';
		this.article.articleNumber = this.articleNumberFormControl.value;
		this.article.articleName = this.articleNameFormControl.value;
		if (this.article.ceMarked) {
			this.article.ceDirective = this.selectedCeDirective;
		}
		this.article.basedOnProduct = this.selectedBasedOnProduct;
		this.article.ceStandard = this.selectedCeStandard;
		if (this.selectedPreventiveMaintenanceValidity) {
			this.article.preventiveMaintenanceValidFrom = this.selectedPreventiveMaintenanceValidity;
		} else {
			this.article.preventiveMaintenanceValidFrom = null;
		}
		this.article.orderUnit = this.selectedOrderUnit;
		this.article.articleQuantityInOuterPackageUnit = this.selectedArticleQuantityInOuterPackageUnit;
		this.article.packageContentUnit = this.selectedPackageContentUnit;

		if (this.articleId) {
			this.updateArticle();
		} else {
			this.createArticle();
		}

	}

	createArticle() {
		this.productService.createArticle(this.organizationId, this.article).subscribe(
			data => {
				if (this.copiedArticleId || data.id) {
					this.router.navigate(['organization/' + this.organizationId + '/article/' + data.id + '/handle']);
				}
				this.loaded = false;
				this.article = data;
				this.articleId = data.id;
				this.copiedArticleId = null;
				this.articleNumberFormControl.setValue(data.articleNumber);
				this.articleNameFormControl.setValue(data.articleName);
				if (!this.article.manufacturerElectronicAddress) {
					this.article.manufacturerElectronicAddress = new ElectronicAddress();
				}
				this.getCategoryCodeForCodelessArticle();
				this.getMedia();
				this.toggleEditMode();
				this.saving = false;
				this.alertService.clear();
				this.alertService.success('Sparat');
			}, error => {
				this.saving = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.handleServerValidation(err);
					this.alertService.error(err);
				});
			}
		);
	}

	updateArticle() {
		this.productService.updateArticle(this.organizationId, this.articleId, this.article).subscribe(
			data => {
				this.article = data;
				this.articleNumberFormControl.setValue(data.articleNumber);
				this.articleNameFormControl.setValue(data.articleName);
				if (!this.article.manufacturerElectronicAddress) {
					this.article.manufacturerElectronicAddress = new ElectronicAddress();
				}

				this.isColorOverridden = this.article.colorOverridden;
				this.isCeDirectiveOverridden = this.article.ceDirectiveOverridden;
				this.isCeMarkedOverridden = this.article.ceMarkedOverridden;
				this.isCeStandardOverridden = this.article.ceStandardOverridden;
				this.isCustomerUniqueOverridden = this.article.customerUniqueOverridden;
				this.isManufacturerArticleNumberOverridden = this.article.manufacturerArticleNumberOverridden;
				this.isManufacturerElectronicAddressOverridden = this.article.manufacturerElectronicAddressOverridden;
				this.isManufacturerOverridden = this.article.manufacturerOverridden;
				this.isOrderInformationOverridden = this.article.orderInformationOverridden;
				this.isPreventiveMaintenanceDescriptionOverridden = this.article.preventiveMaintenanceDescriptionOverridden;
				this.isPreventiveMaintenanceNumberOfDaysOverridden = this.article.preventiveMaintenanceNumberOfDaysOverridden;
				this.isPreventiveMaintenanceValidFromOverridden = this.article.preventiveMaintenanceValidFromOverridden;
				this.isStatusOverridden = this.article.statusOverridden;
				this.isSupplementedInformationOverridden = this.article.supplementedInformationOverridden;
				this.isTrademarkOverridden = this.article.trademarkOverridden;

				this.toggleEditMode();
				this.saving = false;
				this.alertService.clear();
				this.alertService.success('Sparat');
			}, error => {
				this.saving = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.handleServerValidation(err);
					this.alertService.error(err);
				});
			}
		);
	}

	reactivateArticle() {
		this.reactivating = true;
		this.article.replacementDate = null;
		this.article.inactivateRowsOnReplacement = null;
		this.productService.updateArticle(this.organizationId, this.articleId, this.article).subscribe(
			data => {
				this.article = data;
				if (!this.article.manufacturerElectronicAddress) {
					this.article.manufacturerElectronicAddress = new ElectronicAddress();
				}
				this.articleNumberFormControl.setValue(data.articleNumber);
				this.articleNameFormControl.setValue(data.articleName);
				this.replacementDate = null;
				this.replaced = false;
				this.reactivating = false;

				this.isStatusOverridden = this.article.statusOverridden;
				this.alertService.clear();
				this.alertService.success('Artikeln återaktiverad');
			}, error => {
				this.reactivating = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.handleServerValidation(err);
					this.alertService.error(err);
				});
			});
	}

	editReplacementArticles() {
		const dialogRef = this.dialog.open(ArticleReplacementDialogComponent, {
			width: '80%',
			data: {'article': this.article, 'organizationId': this.organizationId}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.editMode = false;
				this.onEditModeChange();
				this.article = result.article;
				this.replacementDate = this.convertToDateString(this.article.replacementDate);
				if (result.replaced) {
					this.replaced = true;
				} else {
					this.replaced = false;
					this.article.replacedByArticles = [];
				}
			}
		});
	}

	onCEToggle() {
		if (!this.article.ceMarked) {
			this.article.ceDirective = null;
		}
	}

	isFutureDate() {
		return this.replacementDate > new Date();
	}

	convertToDateString(date) {
		return new Date(date);
	}

	cancel() {
		this.getArticle(false);
		this.toggleEditMode();
	}

	goBack() {
		this.location.back();
	}

	openUploadMainImageDialog() {
		const dialogRef = this.dialog.open(UploadDialogComponent, {
			width: '80%',
			data: {'organizationId': this.organizationId, 'articleId': this.articleId, 'mediaType': 'mainImage'}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.mainImage = result;
			}
		});
	}

	openUploadImageDialog() {
		const dialogRef = this.dialog.open(UploadDialogComponent, {
			width: '80%',
			data: {'organizationId': this.organizationId, 'articleId': this.articleId, 'mediaType': 'image'}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.otherImages.push(result);
			}
		});
	}

	openUploadVideoDialog() {
		const dialogRef = this.dialog.open(UploadDialogComponent, {
			width: '80%',
			data: {'organizationId': this.organizationId, 'articleId': this.articleId, 'mediaType': 'video'}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.media.videos.push(result);
			}
		});
	}

	openUploadDocumentDialog() {
		const dialogRef = this.dialog.open(UploadDialogComponent, {
			width: '80%',
			data: {'organizationId': this.organizationId, 'articleId': this.articleId, 'mediaType': 'document'}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.media.documents.push(result);
			}
		});
	}

	openDeleteMediaDialog(media) {
		const dialogRef = this.dialog.open(DeleteMediaDialogComponent, {
			width: '80%',
			data: {'organizationId': this.organizationId, 'articleId': this.articleId, 'media': media}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.media.images = this.media.images.filter(item => item.id !== media.id);
				this.media.videos = this.media.videos.filter(item => item.id !== media.id);
				this.media.documents = this.media.documents.filter(item => item.id !== media.id);
				this.otherImages = this.otherImages.filter(item => item.id !== media.id);
				if (media.mainImage) {
					this.mainImage = null;
				}
			}
		});
	}

	openEditMediaDialog(mediaType, media) {
		const dialogRef = this.dialog.open(EditMediaDialogComponent, {
			width: '40%',
			data: {'organizationId': this.organizationId, 'articleId': this.articleId, 'mediaType': mediaType, 'media': media}
		});

		dialogRef.afterClosed().subscribe(result => {
			this.getMedia();
		});
	}

	getMultiTooltip(values, data) {
		const selected = [];
		if (values) {
			values.forEach(value => {
				selected.push(data.find(x => x.id === value));
			});
		}
		if (selected && selected.length) {
			let msg = '';
			selected.forEach(res => {
				if (msg) {
					msg += ', ';
				}
				msg += res.value;
			});
			return msg;
		}
	}

	handleServerValidation(error): void {
		switch (error.field) {
		case 'product': {
			this.productError = error.message;
			this.basedOnProductFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'articleNumber': {
			this.articleNumberError = error.message;
			this.articleNumberFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'articleName': {
			this.articleNameError = error.message;
			this.articleNameFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'category': {
			this.categoryError = error.message;
			this.categoryFormControl.setErrors(Validators.pattern(''));
			break;
		}
		}
	}

	changeTab() {
		this.tabIndex = 1;
		this.getCategoryCodeForCodelessArticle();
	}

	resetValidation() {
		this.productError = null;
		this.basedOnProductFormControl.markAsDirty();
		this.articleNumberError = null;
		this.articleNumberFormControl.markAsDirty();
		this.articleNameError = null;
		this.articleNameFormControl.markAsDirty();
	}

}
