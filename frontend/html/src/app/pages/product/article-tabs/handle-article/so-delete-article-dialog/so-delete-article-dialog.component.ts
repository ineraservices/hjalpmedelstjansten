import {Component, Inject} from '@angular/core';
import {ProductService} from "../../../../../services/product.service";
import {AlertService} from "../../../../../services/alert.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";


@Component({
	selector: 'app-delete-article-dialog',
	templateUrl: './so-delete-article-dialog.component.html',
	styleUrls: ['./so-delete-article-dialog.component.scss']
})
export class SoDeleteArticleDialogComponent {

	loading = false;

	constructor(public dialogRef: MatDialogRef<SoDeleteArticleDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private productService: ProductService,
							private alertService: AlertService) {
	}

	done() {
		this.loading = true;
		this.productService.soDeleteArticle(this.data.organizationId, this.data.articleId).subscribe(
			res => {
				this.alertService.clear();
				this.alertService.success('Sparat');
				this.dialogRef.close('success');
				this.loading = false;

			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
					this.loading = false;
				});
			});

	}

}
