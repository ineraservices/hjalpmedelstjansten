import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ArticleReplacementDialogComponent} from './article-replacement-dialog.component';

describe('ArticleReplacementDialogComponent', () => {
	let component: ArticleReplacementDialogComponent;
	let fixture: ComponentFixture<ArticleReplacementDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ArticleReplacementDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ArticleReplacementDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
