import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProductService} from "../../../../../services/product.service";
import {AlertService} from "../../../../../services/alert.service";


@Component({
	selector: 'app-delete-article-dialog',
	templateUrl: './delete-article-dialog.component.html',
	styleUrls: ['./delete-article-dialog.component.scss']
})
export class DeleteArticleDialogComponent {

	loading = false;

	constructor(public dialogRef: MatDialogRef<DeleteArticleDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private productService: ProductService,
							private alertService: AlertService) {
	}

	done() {
		this.loading = true;
		this.productService.deleteArticle(this.data.organizationId, this.data.articleId).subscribe(
			res => {
				this.alertService.clear();
				this.alertService.success('Sparat');
				this.dialogRef.close('success');
				this.loading = false;

			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
					this.loading = false;
				});
			});

	}

}
