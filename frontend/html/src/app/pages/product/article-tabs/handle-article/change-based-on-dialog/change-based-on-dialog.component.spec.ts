import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChangeBasedOnDialogComponent} from './change-based-on-dialog.component';

describe('ChangeBasedOnDialogComponent', () => {
	let component: ChangeBasedOnDialogComponent;
	let fixture: ComponentFixture<ChangeBasedOnDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ChangeBasedOnDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ChangeBasedOnDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
