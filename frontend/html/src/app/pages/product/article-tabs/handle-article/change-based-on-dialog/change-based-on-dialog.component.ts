import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProductService} from '../../../../../services/product.service';
import {AlertService} from '../../../../../services/alert.service';
import {Product} from '../../../../../models/product/product.model';

@Component({
	selector: 'app-change-based-on-dialog',
	templateUrl: './change-based-on-dialog.component.html',
	styleUrls: ['./change-based-on-dialog.component.scss']
})
export class ChangeBasedOnDialogComponent {


	saving = false;
	selectedBasedOnProduct: Product;

	constructor(public dialogRef: MatDialogRef<ChangeBasedOnDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private productService: ProductService,
							private alertService: AlertService) {
	}

	done() {
		this.saving = true;

		this.productService.deleteAllMediaFromArticle(this.data.organizationId, this.data.articleId).subscribe(
			res => {
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			});

		this.productService.switchBasedOn(this.data.organizationId, this.data.articleId, this.selectedBasedOnProduct.id).subscribe(
			res => {
				this.saving = false;
				this.alertService.success('Sparat');
				this.dialogRef.close({
					'article': res
				});
			}, error => {
				this.saving = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

}
