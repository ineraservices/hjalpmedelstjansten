import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AddArticleToPricelistDialogComponent} from './add-article-to-pricelist-dialog.component';

describe('AddArticleToPricelistDialogComponent', () => {
	let component: AddArticleToPricelistDialogComponent;
	let fixture: ComponentFixture<AddArticleToPricelistDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [AddArticleToPricelistDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(AddArticleToPricelistDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
