import {Component, OnInit} from '@angular/core';
import {ProductService} from '../../../../services/product.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Pricelistrow} from '../../../../models/agreement/pricelistrow.model';
import {MatDialog} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {AuthService} from '../../../../auth/auth.service';
import {HasPricelistRowResponse} from '../../../../models/product/has-pricelist-row-response.model';
import {
	AddArticleToPricelistDialogComponent
} from './add-article-to-pricelist-dialog/add-article-to-pricelist-dialog.component';
import {CommonService} from '../../../../services/common.service';
import {OrganizationService} from "../../../../services/organization.service";

@Component({
	selector: 'app-article-pricelist',
	templateUrl: './article-pricelist.component.html',
	styleUrls: ['./article-pricelist.component.scss']
})
export class ArticlePricelistComponent implements OnInit {
	displayedColumns = ['avtalsnummer', 'avtalsnamn', 'prislistenummer', 'pris', 'leverantör', 'kund', 'gäller från', 'status'];
	loaded = false;
	loading = false;
	organizationId: number;
	userOrganizationId: number;
	articleId: number;
	dataSource = null;
	dataSource2 = null;
	pricelistRows: Array<Pricelistrow>;
	generalPricelistPricelistRows: Array<Pricelistrow>;
	hasPricelistRows: HasPricelistRowResponse = new HasPricelistRowResponse();
	flexDirection: string;
	queryParams: number;
	queryParams_GP: number;

	// Filters
	filterFuture = false;
	filterCurrent = false;
	filterPast = false;
	filterFuture_GP = false;
	filterCurrent_GP = false;
	filterPast_GP = false;

	// Permissions
	hasPermissionToCreatePricelistRows: boolean;
	hasPermissionToViewPricelistRows: boolean;
	hasPermissionToViewPricelistRowsExists: boolean;
	hasPermissionToViewGeneralPricelistPricelistRows: boolean;
	hasPermissionToViewAllGeneralPricelistPricelistRows: boolean;
	isOwnOrganization = false;
	isSupplier = false;
	isCustomer = false;

	constructor(private authService: AuthService,
							private route: ActivatedRoute,
							private productService: ProductService,
							private organizationService: OrganizationService,
							private dialog: MatDialog,
							private commonService: CommonService,
							private router: Router) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}
		this.route.params.subscribe(params => {
			this.organizationId = params.organizationId;
			this.articleId = params.articleId;
		});
		this.userOrganizationId = this.authService.getOrganizationId();
		this.isOwnOrganization = Number(this.organizationId) === this.authService.getOrganizationId();
		this.hasPermissionToCreatePricelistRows = this.hasPermissionToCreatePricelistRows && this.isOwnOrganization;
		this.hasPermissionToViewGeneralPricelistPricelistRows = this.hasPermissionToViewGeneralPricelistPricelistRows && this.isOwnOrganization;
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	ngOnInit() {
		this.hasPermissionToCreatePricelistRows = this.hasPermission('pricelistrow:create_own');
		this.hasPermissionToViewPricelistRows = this.hasPermission('pricelistrow:view_own');
		this.hasPermissionToViewPricelistRowsExists = this.hasPermission('pricelistrow:view_exists');
		this.hasPermissionToViewGeneralPricelistPricelistRows = this.hasPermission('generalpricelist_pricelistrow:view_own');
		this.hasPermissionToViewAllGeneralPricelistPricelistRows = this.hasPermission('generalpricelist_pricelistrow:view_all');
		this.organizationService.getOrganization(this.authService.getOrganizationId()).subscribe(
			res => {
				this.isSupplier = res.organizationType === 'SUPPLIER';
				this.isCustomer = res.organizationType === 'CUSTOMER';
			}
		);
	}

	getPricelists(isSupplier, queryParams, queryParams_GP) {
		this.loading = true;
		let loadingAgreementRows = true;
		let loadingGPRows = true;
		if (isSupplier) {
			this.hasPermissionToViewPricelistRows = this.hasPermissionToViewPricelistRows && this.isOwnOrganization;
			this.hasPermissionToViewPricelistRowsExists = this.hasPermissionToViewPricelistRowsExists && this.isOwnOrganization;
		}
		if (this.hasPermissionToViewPricelistRows) {
			// this.productService.getPricelistRowsForArticle(this.organizationId, this.articleId).subscribe(
			this.productService.getPricelistRowsAndPricelistStatusForArticle(this.organizationId, this.articleId, queryParams).subscribe(
				res => {
					this.pricelistRows = res;
					this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
					loadingAgreementRows = false;
					if (!loadingGPRows) {
						this.loading = false;
						this.loaded = true;

					}
				}
			);
		} else if (this.hasPermissionToViewPricelistRowsExists) {
			this.productService.hasPricelistRowsForArticle(this.organizationId, this.articleId).subscribe(
				res => {
					this.hasPricelistRows = res;
					loadingAgreementRows = false;
					if (!loadingGPRows) {
						this.loading = false;
						this.loaded = true;

					}
				}
			);
		} else {
			loadingAgreementRows = false;
			if (!loadingGPRows) {
				this.loading = false;
				this.loaded = true;

			}
		}

		if (this.hasPermissionToViewGeneralPricelistPricelistRows || this.hasPermissionToViewAllGeneralPricelistPricelistRows) {
			// this.productService.getGeneralPricelistPricelistRowsForArticle(this.organizationId, this.articleId).subscribe(
			this.productService.getGeneralPricelistRowsAndPricelistStatusForArticle(this.organizationId, this.articleId, queryParams_GP).subscribe(
				data => {
					this.generalPricelistPricelistRows = data;
					this.dataSource2 = new MatTableDataSource<Pricelistrow>(this.generalPricelistPricelistRows);
					loadingGPRows = false;
					if (!loadingAgreementRows) {
						this.loading = false;
						this.loaded = true;

					}
				}
			);
		} else {
			this.loaded = true;
			loadingGPRows = false;
			if (!loadingAgreementRows) {
				this.loading = false;

			}
		}
	}

	changeFilter(text: string) {

		switch (text) {

		case 'FUTURE':
			this.toggleBoolean(this.filterFuture);
			break;

		case 'CURRENT':
			this.toggleBoolean(this.filterCurrent);
			break;

		case 'PAST':
			this.toggleBoolean(this.filterPast);
			break;

		case 'FUTURE_GP':
			this.toggleBoolean(this.filterFuture_GP);
			break;

		case 'CURRENT_GP':
			this.toggleBoolean(this.filterCurrent_GP);
			break;

		case 'PAST_GP':
			this.toggleBoolean(this.filterPast_GP);
			break;
		}

		this.onFilterChange();
	}

	onFilterChange() {

		this.queryParams = 0;
		this.queryParams_GP = 0;

		if (this.filterFuture) {
			this.queryParams += 1;
		}
		if (this.filterCurrent) {
			this.queryParams += 2;
		}
		if (this.filterPast) {
			this.queryParams += 4;
		}
		if (this.filterFuture_GP) {
			this.queryParams_GP += 1;
		}
		if (this.filterCurrent_GP) {
			this.queryParams_GP += 2;
		}
		if (this.filterPast_GP) {
			this.queryParams_GP += 4;
		}
		this.getPricelists(this.isSupplier, this.queryParams, this.queryParams_GP);
	}

	openCreatePricelistRowDialog() {
		const dialogRef = this.dialog.open(AddArticleToPricelistDialogComponent, {
			width: '80%',
			data: {
				'organizationId': this.userOrganizationId,
				'articleId': this.articleId
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				// this.getPricelists(true, this.queryParams, this.queryParams_GP);
				this.onFilterChange();
			}
		});
	}

	async routeTo(element: any): Promise<void> {
		await this.router.navigateByUrl("/organization/" + this.userOrganizationId + "/agreement/" + element.pricelist.agreement.id + "/pricelist/" + element.pricelist.id + "/handle");
	}

	private toggleBoolean(bool: boolean): boolean {

		switch (bool) {
		case true:
			bool = false;
			break;
		case false:
			bool = true;
			break;
		}
		return bool;
	}
}
