import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ArticlePricelistComponent} from './article-pricelist.component';

describe('ArticlePricelistComponent', () => {
	let component: ArticlePricelistComponent;
	let fixture: ComponentFixture<ArticlePricelistComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ArticlePricelistComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ArticlePricelistComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
