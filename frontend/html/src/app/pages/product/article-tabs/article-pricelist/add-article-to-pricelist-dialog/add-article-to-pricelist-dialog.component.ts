import {Component, Inject, OnInit} from '@angular/core';
import {Pricelist} from '../../../../../models/agreement/pricelist.model';
import {AgreementService} from '../../../../../services/agreement.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AlertService} from '../../../../../services/alert.service';
import {Pricelistrow} from '../../../../../models/agreement/pricelistrow.model';
import {Article} from '../../../../../models/product/article.model';

@Component({
	selector: 'app-add-article-to-pricelist-dialog',
	templateUrl: './add-article-to-pricelist-dialog.component.html',
	styleUrls: ['./add-article-to-pricelist-dialog.component.scss']
})
export class AddArticleToPricelistDialogComponent implements OnInit {
	loadingAgreements = false;
	loadingPricelists = false;
	saving = false;
	agreements = [];
	selectedAgreement: number;
	pricelists: Array<Pricelist>;
	selectedPricelist: number;

	searchTotal;
	rows;
	existingRow = false;

	constructor(private agreementService: AgreementService,
							private dialogRef: MatDialogRef<AddArticleToPricelistDialogComponent>,
							@Inject(MAT_DIALOG_DATA) private data: any,
							private alertService: AlertService) {
	}

	ngOnInit() {
		this.loadingAgreements = true;
		this.agreementService.searchAgreements(this.data.organizationId, '?limit=0').subscribe(
			agreements => {
				this.agreements = agreements.body;
				this.loadingAgreements = false;
			}, error => {
				this.loadingAgreements = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	onAgreementSelection() {
		this.loadingPricelists = true;
		this.selectedPricelist = null;
		// this.agreementService.getPriceLists(this.data.organizationId, this.selectedAgreement).subscribe(
		this.agreementService.getSelectablePriceLists(this.data.organizationId, this.selectedAgreement, this.data.articleId).subscribe(
			pricelists => {
				this.pricelists = pricelists;
				this.loadingPricelists = false;
			}
		);
	}

	done() {
		this.saving = true;
		const pricelistRows = new Array<Pricelistrow>();
		const pricelistRow = new Pricelistrow();
		pricelistRow.article = new Article();
		pricelistRow.article.id = this.data.articleId;

		this.agreementService.getPricelistRows(this.data.organizationId, this.selectedAgreement, this.selectedPricelist, '').subscribe(
			res => {
				this.searchTotal = res.body.length;
				this.rows = res;

				for (let i = 0; i < res.body.length; i++) {
					if (res.body[i].article.id === this.data.articleId) {
						this.existingRow = true;
						break;
					}
				}

				if (this.existingRow) { // det här ska aldrig inträffa
					this.saving = false;
					this.alertService.errorWithMessage('Artikeln finns redan i vald prislista!');
					this.dialogRef.close(res);
				} else {
					pricelistRow.status = 'CREATED';
					pricelistRow.leastOrderQuantity = 1;
					pricelistRows.push(pricelistRow);
					this.agreementService.createPricelistRows(
						this.data.organizationId, this.selectedAgreement, this.selectedPricelist, pricelistRows).subscribe(
						result => {
							this.saving = false;
							this.alertService.success('Sparat');
							this.dialogRef.close(result);
						}, error => {
							this.saving = false;
							this.alertService.clear();
							error.error.errors.forEach(err => {
								this.alertService.error(err);
							});
						}
					);
				}
			}
		);
	}
}
