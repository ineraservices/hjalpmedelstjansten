import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {ProductService} from '../../../services/product.service';
import {SelectionModel} from '@angular/cdk/collections';
import {Article} from '../../../models/product/article.model';
import {AlertService} from '../../../services/alert.service';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {ActivatedRoute} from "@angular/router";

@Component({
	selector: 'app-connection-dialog',
	templateUrl: './connection-dialog.component.html',
	styleUrls: ['./connection-dialog.component.scss']
})
export class ConnectionDialogComponent implements OnInit {
	displayedColumns = ['checkbox', 'typ', 'benämning', 'produkt/artnr', 'status', 'kategori'];

	loading = true;
	saving = false;
	organizationId: number;
	articleType: string;
	article: Article;
	cachedArticles = [];
	cachedProducts = [];
	productsAndArticles = [];
	dataSource = null;
	query = '';
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	queryParams = '&type=H&type=T';
	searchTotal;
	offset = 25;
	totalPages;
	currentPage = 1;
	paginatorArray = [];
	allPages;
	selection = new SelectionModel<ProductOrArticle>(true, []);
	tmpSelection = [];
	copiedArticleId: number;

	// Filters
	filterProducts = false;
	filterArticles = false;
	filterH = false;
	filterT = false;
	filterPublished = false;
	filterDiscontinued = false;

	constructor(public dialogRef: MatDialogRef<ConnectionDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private productService: ProductService,
							private alertService: AlertService,
							private route: ActivatedRoute) {
		// The user is considered done with their search after 500 ms without further input
		this.searchChanged.pipe(
			debounceTime(500),
			distinctUntilChanged())
			.subscribe(query => {
				this.query = encodeURIComponent(query);
				this.onQueryChange();
			}
			);
		this.route.queryParams.subscribe(qParams => {
			this.copiedArticleId = qParams.copiedArticle;
		});
	}

	ngOnInit() {
		if (this.data) {
			this.organizationId = this.data.organizationId;
			this.articleType = this.data.articleType;
			if (this.data.article) {
				this.article = this.data.article;
				this.data.article.fitsToArticles.forEach(article => {
					this.cachedArticles.push(article);
				});
				this.data.article.fitsToProducts.forEach(product => {
					this.cachedProducts.push(product);
				});
			}
		}
		this.searchProductsAndArticles('&type=H&type=T&includeOnlyTiso=true');
	}

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
	}

	onQueryChange() {
		// debugger;
		this.onFilterChange();
	}

	onFilterChange() {
		this.queryParams = '';
		this.currentPage = 1;
		if (!this.filterArticles) {
			this.filterH = false;
			this.filterT = false;
		}
		this.queryParams += this.filterProducts ? '&includeProducts=true' : '&includeProducts=false';
		this.queryParams += this.filterArticles ? '&includeArticles=true' : '&includeArticles=false';
		this.queryParams += this.filterH ? '&type=H' : '';
		this.queryParams += this.filterT ? '&type=T' : '';
		if (!this.filterH && !this.filterT) {
			this.queryParams += '&type=H&type=T';
		}
		this.queryParams += this.filterPublished ? '&status=PUBLISHED' : '';
		this.queryParams += this.filterDiscontinued ? '&status=DISCONTINUED' : '';
		this.queryParams += "&includeOnlyTiso=true";
		this.searchProductsAndArticles(this.query + this.queryParams);
	}

	/** Whether the number of selected elements matches the total number of selectable rows. */
	isAllSelected() {
		let result = true;
		this.dataSource.data.forEach(item => {
			const rowChecked = this.selection.selected.some(function (el) {
				return (el.id === item.id);
			});
			if (!rowChecked) {
				result = false;
			}
		});
		return result;
	}

	/** Selects all selectable rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.dataSource.data.forEach(
				row => {
					this.selection.select(row);
				});
		}
	}

	searchProductsAndArticles(query) {
		this.loading = true;
		this.productService.searchProductsAndArticles(query, this.organizationId).subscribe(
			res => {
				this.searchTotal = res.headers.get('X-Total-Count');
				this.productsAndArticles = this.searchTotal !== '0' ? res.body : [];
				this.totalPages = Math.ceil(this.searchTotal / this.offset);
				this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
				this.dataSource = new MatTableDataSource<ProductOrArticle>(this.productsAndArticles);
				this.generatePaginator();
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	goToPage(page) {
		if (page !== '...' && page !== this.currentPage) {
			this.loading = true;
			this.tmpSelection = this.selection.selected;
			this.currentPage = page;
			this.productService.searchProductsAndArticles(
				this.query + this.queryParams + '&includeOnlyTiso=true&offset=' + (this.currentPage - 1) * this.offset, this.organizationId).subscribe(
				res => {
					this.productsAndArticles = res.body;
					this.productsAndArticles.forEach(row => {
						this.tmpSelection.forEach(tmpRow => {
							if (tmpRow.id === row.id) {
								this.selection.deselect(this.selection.selected.find(x => x.id === row.id));
								this.selection.select(row);
							}
						});
					});
					this.dataSource = new MatTableDataSource<ProductOrArticle>(this.productsAndArticles);
					this.currentPage = page;
					this.generatePaginator();
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		}
	}

	generatePaginator() {
		let hasFirstEllipsis = false;
		let hasSecondEllipsis = false;
		this.paginatorArray = [];
		if (this.searchTotal > this.offset) {
			if (this.totalPages < 6) {
				this.paginatorArray = this.allPages;
			} else {
				this.allPages.forEach((pageNumber) => {
					if (pageNumber === 1
						|| pageNumber === this.currentPage
						|| pageNumber === this.totalPages
						|| pageNumber === this.currentPage + 1
						|| pageNumber === this.currentPage - 1) {
						this.paginatorArray.push(pageNumber);
					} else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
						this.paginatorArray.push('...');
						hasFirstEllipsis = true;
					} else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
						this.paginatorArray.push('...');
						hasSecondEllipsis = true;
					}
				});
			}
		}
		this.loading = false;
	}

	done() {

		if (this.article) {
			this.saving = true;
			this.selection.selected.forEach(item => {
				if (item.type === 'PRODUCT' && !this.article.fitsToProducts.find(product => product.id === item.id)) {
					this.article.fitsToProducts.push({'id': item.id});
				} else if (item.type === 'ARTICLE' && !this.article.fitsToArticles.find(article => article.id === item.id)) {
					this.article.fitsToArticles.push({'id': item.id});
				}
			});
			if (this.copiedArticleId) {
				this.createArticle();
			} else {
				if (this.article.id) {
					this.updateArticle();
				} else {
					this.createArticle();
				}
			}
		}
	}

	createArticle() {
		if (!this.article.status) {
			this.article.status = 'PUBLISHED';
		}
		this.productService.createArticle(this.organizationId, this.article).subscribe(
			res => {
				this.saving = false;
				this.alertService.success('Sparat');
				this.dialogRef.close(res);
			}, error => {
				this.article.fitsToProducts = [];
				this.article.fitsToArticles = [];
				this.cachedProducts.forEach(cp => {
					this.article.fitsToProducts.push(cp);
				});
				this.cachedArticles.forEach(ca => {
					this.article.fitsToArticles.push(ca);
				});
				this.saving = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	updateArticle() {
		this.productService.updateArticle(this.organizationId, this.article.id, this.article).subscribe(
			res => {
				this.saving = false;
				this.alertService.success('Sparat');
				this.dialogRef.close(res);
			}, error => {
				this.article.fitsToProducts = [];
				this.article.fitsToArticles = [];
				this.cachedProducts.forEach(cp => {
					this.article.fitsToProducts.push(cp);
				});
				this.cachedArticles.forEach(ca => {
					this.article.fitsToArticles.push(ca);
				});
				this.saving = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

}

export interface ProductOrArticle {
	id: string;
	type: string;
	articleName: string;
	articleNumber: string;
	status: string;
	code: string;
}
