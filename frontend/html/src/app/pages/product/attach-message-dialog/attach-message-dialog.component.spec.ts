import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AttachMessageDialogComponent} from './attach-message-dialog.component';

describe('AttachMessageDialogComponent', () => {
	let component: AttachMessageDialogComponent;
	let fixture: ComponentFixture<AttachMessageDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [AttachMessageDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(AttachMessageDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
