import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';


@Component({
	selector: 'app-attach-message-dialog-dialog',
	templateUrl: './attach-message-dialog.component.html',
	styleUrls: ['./attach-message-dialog.component.scss']
})
export class AttachMessageDialogComponent {

	loading = false;
	message = '';

	constructor(public dialogRef: MatDialogRef<AttachMessageDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any) {
	}

	done() {
		this.message = encodeURIComponent(this.message)
			.replace(/\{/g, "%7B")
			.replace(/}/g, "%7D")
			.replace(/\//g, "%2F")
			.replace(/-/g, "%2D")
			.replace(/_/g, "%5F")
			.replace(/\./g, "%2E")
			.replace(/!/g, "%21")
			.replace(/~/g, "%7E")
			.replace(/\*/g, "%2A")
			.replace(/'/g, "%27")
			.replace(/\(/g, "%28")
			.replace(/\)/g, "%29");
		// this.loading = true; apa
		if (this.message.length === 0) {
			this.message = '%2D';
		} // this is '-'
		this.dialogRef.close(this.message);
	}

}
