import {Component, Inject, OnInit} from '@angular/core';
import {ProductService} from '../../../services/product.service';
import {CommonService} from '../../../services/common.service';
import {DocumentType} from '../../../models/document-type.model';
import {Document} from '../../../models/product/document.model';
import {AlertService} from '../../../services/alert.service';
import {HelptextService} from '../../../services/helptext.service';
import {FormControl} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
	selector: 'app-edit-media-dialog',
	templateUrl: './edit-media-dialog.component.html',
	styleUrls: ['./edit-media-dialog.component.scss']
})
export class EditMediaDialogComponent implements OnInit {

	documentTypes: Array<DocumentType>;
	document: Document;
	description: string;
	oldDescription: string;
	documentType: string;
	url: string;
	alternativeText: string;
	oldAlternativeText: string;
	file;
	method;
	uploading = false;
	formData: FormData;
	helpTexts;
	helpTextsLoaded = false;
	documentTypeControl = new FormControl();

	constructor(public dialogRef: MatDialogRef<EditMediaDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private productService: ProductService,
							private commonService: CommonService,
							private alertService: AlertService,
							private helpTextService: HelptextService) {
	}

	ngOnInit() {
		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);

		this.oldDescription = this.data.media.description;
		this.oldAlternativeText = this.data.media.alternativeText;

		if (this.data.mediaType === 'document') {
			this.commonService.getDocumentTypes().subscribe(
				res => {
					this.documentTypes = res;
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);

			this.documentTypeControl.setValue(this.data.media.documentType.id);
		}
	}


	showSaveButton() {
		if (this.uploading) {
			return false;
		}

		if (this.data.mediaType === 'document') {
			return ((this.method === 'upload' && this.file) || (this.method === 'link' && this.url));
		} else if (this.data.mediaType === 'video') {
			return this.url;
		} else {
			return (this.method === 'upload' && this.file) || (this.method === 'link' && this.url);
		}
	}

	fileSelected(file) {
		this.file = file;
	}

	cancel() {
		this.description = this.oldDescription;
		this.alternativeText = this.oldAlternativeText;
	}

	done() {
		this.formData = new FormData();
		if (this.description) {
			this.formData.append('description', this.description);
		}
		if (this.alternativeText) {
			this.formData.append('alternativeText', this.alternativeText);
		}
		if (this.documentType) {
			this.formData.append('documenttype', this.documentType);
		}
		// if (this.method === 'upload') {
		//   this.formData.append('file', this.data.media.file);
		// } else {
		//   this.formData.append('url', this.data.media.url);
		// }

		if (this.data.productId) {
			this.updateMediaToProduct();
		} else {
			this.updateMediaToArticle();
		}
	}

	updateMediaToProduct() {
		this.productService.updateMediaToProduct(this.data.organizationId, this.data.productId, this.data.media.id, this.formData).subscribe(
			media => {
				this.alertService.clear();
				// this.alertService.success('Sparat');
				this.dialogRef.close(media);
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			});
	}

	updateMediaToArticle() {
		this.productService.updateMediaToArticle(this.data.organizationId, this.data.articleId, this.data.media.id, this.formData).subscribe(
			media => {
				this.alertService.clear();
				this.alertService.success('Sparat');
				this.dialogRef.close(media);
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			});
	}
}
