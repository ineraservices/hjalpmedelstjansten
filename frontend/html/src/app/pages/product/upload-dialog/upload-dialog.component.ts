import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProductService} from '../../../services/product.service';
import {CommonService} from '../../../services/common.service';
import {DocumentType} from '../../../models/document-type.model';
import {Document} from '../../../models/product/document.model';
import {AlertService} from '../../../services/alert.service';
import {HelptextService} from '../../../services/helptext.service';

@Component({
	selector: 'app-upload-dialog',
	templateUrl: './upload-dialog.component.html',
	styleUrls: ['./upload-dialog.component.scss']
})
export class UploadDialogComponent implements OnInit {

	documentTypes: Array<DocumentType>;
	document: Document;
	description: string;
	documentType: string;
	url: string;
	alternativeText: string;
	file;
	method;
	uploading = false;
	formData: FormData;
	helpTexts;
	helpTextsLoaded = false;

	constructor(public dialogRef: MatDialogRef<UploadDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private productService: ProductService,
							private commonService: CommonService,
							private alertService: AlertService,
							private helpTextService: HelptextService) {
	}

	ngOnInit() {
		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);
		if (this.data.mediaType === 'document') {
			this.commonService.getDocumentTypes().subscribe(
				res => {
					this.documentTypes = res;
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		}
	}

	fileSelected(file) {
		this.file = file;
	}

	showSaveButton() {
		if (this.uploading) {
			return false;
		}
		if (this.data.mediaType === 'document') {
			return ((this.method === 'upload' && this.file) || (this.method === 'link' && this.url));
		} else if (this.data.mediaType === 'video') {
			return this.url;
		} else {
			return (this.method === 'upload' && this.file) || (this.method === 'link' && this.url);
		}
	}

	done() {
		this.formData = new FormData();
		if (this.method === 'upload') {
			this.formData.append('file', this.file);
		} else {
			this.formData.append('url', this.url);
		}
		if (this.description) {
			this.formData.append('description', this.description);
		}
		if (this.alternativeText) {
			this.formData.append('alternativeText', this.alternativeText);
		}
		if (this.data.mediaType === 'mainImage') {
			this.formData.append('mainimage', 'true');
		}
		if (this.documentType) {
			this.formData.append('documenttype', this.documentType);
		}

		if (this.data.productId) {
			this.uploadMediaToProduct();
		} else {
			this.uploadMediaToArticle();
		}
	}

	uploadMediaToProduct() {
		this.uploading = true;
		if (this.data.mediaType === 'document') {
			this.productService.uploadMediaToProduct(this.data.organizationId, this.data.productId, 'document', this.formData).subscribe(
				media => {
					this.uploading = false;
					this.alertService.clear();
					this.alertService.success('Sparat');
					this.dialogRef.close(media);
				}, error => {
					this.uploading = false;
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				});
		} else if (this.data.mediaType === 'video') {
			this.productService.uploadMediaToProduct(this.data.organizationId, this.data.productId, 'video', this.formData).subscribe(
				media => {
					this.uploading = false;
					this.alertService.clear();
					this.alertService.success('Sparat');
					this.dialogRef.close(media);
				}, error => {
					this.uploading = false;
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				});
		} else {
			this.productService.uploadMediaToProduct(this.data.organizationId, this.data.productId, 'image', this.formData).subscribe(
				media => {
					this.uploading = false;
					this.alertService.clear();
					this.alertService.success('Sparat');
					this.dialogRef.close(media);
				}, error => {
					this.uploading = false;
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				});
		}
	}

	uploadMediaToArticle() {
		this.uploading = true;
		if (this.data.mediaType === 'document') {
			this.productService.uploadMediaToArticle(this.data.organizationId, this.data.articleId, 'document', this.formData).subscribe(
				media => {
					this.uploading = false;
					this.alertService.clear();
					this.alertService.success('Sparat');
					this.dialogRef.close(media);
				}, error => {
					this.uploading = false;
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				});
		} else if (this.data.mediaType === 'video') {
			this.productService.uploadMediaToArticle(this.data.organizationId, this.data.articleId, 'video', this.formData).subscribe(
				media => {
					this.uploading = false;
					this.alertService.clear();
					this.alertService.success('Sparat');
					this.dialogRef.close(media);
				}, error => {
					this.uploading = false;
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				});
		} else {
			this.productService.uploadMediaToArticle(this.data.organizationId, this.data.articleId, 'image', this.formData).subscribe(
				media => {
					this.uploading = false;
					this.alertService.clear();
					this.alertService.success('Sparat');
					this.dialogRef.close(media);
				}, error => {
					this.uploading = false;
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				});
		}
	}

	getFile(target: EventTarget): File {
		return (target as HTMLInputElement).files[0];
	}
}
