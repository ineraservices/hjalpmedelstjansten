import {Component, OnInit} from '@angular/core';
import {Product} from '../../../../models/product/product.model';
import {ProductService} from '../../../../services/product.service';
import {Category} from '../../../../models/product/category.model';
import {CeArray} from '../../../../models/product/ce-array';
import {ElectronicAddress} from '../../../../models/electronic-address.model';
import {ActivatedRoute, Router} from '@angular/router';
import {OrganizationService} from '../../../../services/organization.service';
import {AlertService} from '../../../../services/alert.service';
import {forkJoin} from 'rxjs';
import {Ce} from '../../../../models/product/ce.model';
import {AuthService} from '../../../../auth/auth.service';
import {OrderUnit} from '../../../../models/product/order-unit.model';
import {MatDialog} from '@angular/material/dialog';
import {CategoryDialogComponent} from '../../category-dialog/category-dialog.component';
import {FormControl, Validators} from '@angular/forms';
import {ProductReplacementDialogComponent} from './product-replacement-dialog/product-replacement-dialog.component';
import {Location} from '@angular/common';
import {CategoryProperty} from '../../../../models/product/category-property.model';
import {ProductCategoryProperty} from '../../../../models/product/product-category-property.model';
import {PreventiveMaintenance} from '../../../../models/preventive-maintenance.model';
import {CommonService} from '../../../../services/common.service';
import {DocumentType} from '../../../../models/document-type.model';
import {Document} from '../../../../models/product/document.model';
import {Media} from '../../../../models/product/media.model';
import {EditMediaDialogComponent} from '../../edit-media-dialog/edit-media-dialog.component';
import {UploadDialogComponent} from '../../upload-dialog/upload-dialog.component';
import {Image} from '../../../../models/product/image.model';
import {DeleteMediaDialogComponent} from '../../delete-media-dialog/delete-media-dialog.component';
import {HelptextService} from '../../../../services/helptext.service';
import {PackageUnit} from '../../../../models/product/package-unit.model';
import {DeleteProductDialogComponent} from "./delete-product-dialog/delete-product-dialog.component";

@Component({
	selector: 'app-handle-product',
	templateUrl: './handle-product.component.html',
	styleUrls: ['./handle-product.component.scss']
})
export class HandleProductComponent implements OnInit {
	loaded = false;
	saving = false;
	reactivating = false;
	editMode = false;
	editModeMainImage = false;
	organizationId: number;
	productId: number;
	copiedProductId: number;
	supplier: string;
	extendedCategories = '';
	product: Product = new Product();
	media: Media;
	mainImage: Image;
	otherImages: Array<Image>;
	ce: CeArray = new CeArray();
	categoryProperties: Array<CategoryProperty>;
	selectedCeDirective: Ce;
	selectedCeStandard: Ce;
	selectedPreventiveMaintenanceValidity: PreventiveMaintenance;
	preventiveMaintenanceValidity: Array<PreventiveMaintenance>;
	orderUnits: Array<OrderUnit>;
	packageUnits: Array<PackageUnit>;
	documentTypes: Array<DocumentType>;
	selectedOrderUnit: OrderUnit;
	selectedArticleQuantityInOuterPackageUnit: OrderUnit;
	selectedPackageContentUnit: PackageUnit;
	replaced = false;
	replacementDate;
	helpTexts;
	helpTextsLoaded = false;
	flexDirection: string;
	formData: FormData;
	url: string;
	method;
	file;
	description: string;
	alternativeText: string;

	// Form controls
	productNumberFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	productNameFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	categoryFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	mainImageAltTextFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	discontinuedDateFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	// Validation errors
	productNumberError;
	productNameError;
	categoryError;
	mainImageAltTextError;
	mainImageUploadLinkError;
	formControlErrors: Array<String>;

	// Permissions
	hasPermissionToUpdateProduct: boolean;
	hasPermissionToUploadMedia: boolean;
	hasPermissionToDeleteMedia: boolean;
	isOwnOrganization = false;
	isSupplier = false;

	constructor(private router: Router,
							private productService: ProductService,
							private organizationService: OrganizationService,
							private commonService: CommonService,
							private route: ActivatedRoute,
							private alertService: AlertService,
							private authService: AuthService,
							private categoryDialog: MatDialog,
							private productReplacementDialog: MatDialog,
							private dialog: MatDialog,
							private location: Location,
							private helpTextService: HelptextService) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}
		this.route.params.subscribe(params => {
			this.organizationId = params.organizationId;
			this.productId = params.productId;
		});
		this.route.queryParams.subscribe(qParams => {
			this.copiedProductId = qParams.copiedProduct;
		});

		this.isOwnOrganization = Number(this.organizationId) === this.authService.getOrganizationId();
		this.hasPermissionToUpdateProduct = this.hasPermissionToUpdateProduct && this.isOwnOrganization;
	}

	ngOnInit() {
		this.hasPermissionToUpdateProduct = this.hasPermission('product:update_own');
		this.hasPermissionToUploadMedia = this.hasPermission('media:create');
		this.hasPermissionToDeleteMedia = this.hasPermission('media:delete');
		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);
		this.onEditModeChange();
		forkJoin([this.productService.getCE(), this.organizationService.getOrganization(this.organizationId),
			this.productService.getOrderUnits(), this.commonService.getPreventiveMaintenances(),
			this.commonService.getDocumentTypes(), this.productService.getPackageUnits()])
			.subscribe(
				data => {
					this.ce = data[0];
					this.supplier = data[1].organizationName;
					this.orderUnits = data[2];
					this.preventiveMaintenanceValidity = data[3];
					this.documentTypes = data[4];
					this.packageUnits = data[5];
					if (this.productId || this.copiedProductId) {
						this.getProduct(true);
					} else {
						// Set default values
						this.editMode = true;
						this.onEditModeChange();

						this.product.extendedCategories = new Array<Category>();
						this.product.manufacturerElectronicAddress = new ElectronicAddress();
						this.product.ceMarked = false;
						this.product.customerUnique = false;
						this.selectedOrderUnit = this.orderUnits[0];
						this.loaded = true;
					}
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		this.organizationService.getOrganization(this.authService.getOrganizationId()).subscribe(
			res => {
				res.organizationType === 'SUPPLIER' ? this.isSupplier = true : this.isSupplier = false;
			}
		);
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	toggleEditMode() {
		this.editMode = !this.editMode;
		this.onEditModeChange();
	}

	toggleEditModeMainImage() {
		this.editModeMainImage = !this.editModeMainImage;
		this.onEditModeMainImageChange();
		this.method = null;
		this.file = null;
		this.url = null;
	}

	onEditModeMainImageChange() {
		if (!this.editModeMainImage) {
			// this.mainImageAltTextFormControl.disable();
		} else {
			// this.mainImageAltTextFormControl.enable();
		}
	}

	onEditModeChange() {
		if (!this.editMode) {
			this.discontinuedDateFormControl.disable();
			this.productNumberFormControl.disable();
			this.productNameFormControl.disable();
			this.mainImageAltTextFormControl.disable();
			this.categoryFormControl.disable();
		} else {
			this.discontinuedDateFormControl.enable();
			this.productNameFormControl.enable();
			this.mainImageAltTextFormControl.enable();
			this.categoryFormControl.enable();
			if (!this.productId || this.product.numberEditable) {
				this.productNumberFormControl.enable();
			}
		}
	}

	getMedia() {
		this.otherImages = new Array<Image>();
		const pid = this.copiedProductId ? this.copiedProductId : this.productId;
		this.productService.getMediaForProduct(this.organizationId, pid).subscribe(
			media => {
				this.media = media;
				this.media.images.forEach(image => {
					if (image.mainImage) {
						this.mainImage = image;
						this.mainImageAltTextFormControl.setValue(this.mainImage.alternativeText);
						this.description = this.mainImage.description;
						if (this.mainImage.originalUrl) {
							this.method = "link";
						} else {
							this.method = "upload";
						}
						this.url = this.mainImage.originalUrl;
						// this.file = this.mainImage.originalUrl;
					} else {
						this.otherImages.push(image);
					}
				});
				this.loaded = true;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	getProduct(loadMedia) {
		const productId = this.productId ? this.productId : this.copiedProductId;
		this.productService.getProduct(this.organizationId, productId).subscribe(
			res => {
				this.product = res;
				this.product.categoryPropertys.sort((a, b) => a.property.orderIndex - b.property.orderIndex);
				this.productNameFormControl.setValue(res.productName);
				this.selectedPreventiveMaintenanceValidity = null;
				this.selectedCeStandard = null;
				this.selectedPackageContentUnit = null;
				this.selectedArticleQuantityInOuterPackageUnit = null;
				this.selectedOrderUnit = null;
				this.selectedCeDirective = null;
				if (!this.product.manufacturerElectronicAddress) {
					this.product.manufacturerElectronicAddress = new ElectronicAddress();
				}
				if (this.product.ceDirective) {
					this.selectedCeDirective = this.ce.directives.find(e => e.id === this.product.ceDirective.id);
				}
				if (this.product.ceStandard) {
					this.selectedCeStandard = this.ce.standards.find(e => e.id === this.product.ceStandard.id);
				}
				if (this.product.orderUnit) {
					this.selectedOrderUnit = this.orderUnits.find(e => e.id === this.product.orderUnit.id);
				}
				if (this.product.articleQuantityInOuterPackageUnit) {
					this.selectedArticleQuantityInOuterPackageUnit = this.orderUnits.find(
						e => e.id === this.product.articleQuantityInOuterPackageUnit.id);
				}
				if (this.product.packageContentUnit) {
					this.selectedPackageContentUnit = this.packageUnits.find(e => e.id === this.product.packageContentUnit.id);
				}
				if (this.product.category.code) {
					this.categoryFormControl.setValue(this.product.category.code + ' ' + this.product.category.name);
				} else {
					this.categoryFormControl.setValue(this.product.category.name);
				}
				this.extendedCategories = '';
				if (this.product.extendedCategories) {
					this.product.extendedCategories.forEach((e) => {
						if (this.extendedCategories) {
							this.extendedCategories += '\n';
						}
						if (e.code) {
							this.extendedCategories += e.code + ' ' + e.name;
						} else {
							this.extendedCategories += e.name;
						}
					});
				}
				if (this.product.preventiveMaintenanceValidFrom) {
					this.selectedPreventiveMaintenanceValidity = this.preventiveMaintenanceValidity.find(
						e => e.code === this.product.preventiveMaintenanceValidFrom.code);
				}

				if (this.productId) {
					this.productNumberFormControl.setValue(res.productNumber);
					if (this.product.replacementDate) {
						this.replacementDate = this.convertToDateString(this.product.replacementDate);
						if (this.product.replacedByProducts && this.product.replacedByProducts.length) {
							this.replaced = true;
						}
					}
					if (loadMedia) {
						this.getMedia();
					}
				} else {
					if (this.copiedProductId) {
						this.getMedia();
					}
					this.product.productNumber = null;
					this.replacementDate = null;
					this.product.replacementDate = null;
					this.product.replacedByProducts = null;
					this.editMode = true;
					this.editModeMainImage = true;
					this.onEditModeChange();
					this.loaded = true;
				}
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	openMainCategoryDialog() {
		const dialogRef = this.categoryDialog.open(CategoryDialogComponent, {
			width: '90%'
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				if (result.code) {
					this.categoryFormControl.setValue(result.code + ' ' + result.name);
				} else {
					this.categoryFormControl.setValue(result.name);
				}
				this.product.category = result;
				this.productService.getCategoryProperties(result.id).subscribe(
					res => {
						this.categoryProperties = res;
						this.product.categoryPropertys = new Array<ProductCategoryProperty>();
						if (this.categoryProperties) {
							this.categoryProperties.forEach(prop => {
								const tmp = new ProductCategoryProperty();
								tmp.property = prop;
								tmp.textValue = null;
								tmp.decimalValue = null;
								tmp.intervalFromValue = null;
								tmp.intervalToValue = null;
								tmp.singleListValue = null;
								tmp.multipleListValue = null;
								this.product.categoryPropertys.push(tmp);
							});
						}
					}, error => {
						this.alertService.clear();
						error.error.errors.forEach(err => {
							this.alertService.error(err);
						});
					}
				);
			}
		});
	}

	openExtendedCategoryDialog() {
		const dialogRef = this.categoryDialog.open(CategoryDialogComponent, {
			width: '90%',
			data: {
				'selectedCategories': this.product.extendedCategories && this.product.extendedCategories.length ?
					this.product.extendedCategories : []
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.product.extendedCategories = result;
				this.extendedCategories = '';
				result.forEach((e) => {
					if (this.extendedCategories) {
						this.extendedCategories += '\n';
					}
					if (e.code) {
						this.extendedCategories += e.code + ' ' + e.name;
					} else {
						this.extendedCategories += e.name;
					}
				});
			}
		});
	}

	chooseReplacementProduct() {
		const dialogRef = this.productReplacementDialog.open(ProductReplacementDialogComponent, {
			width: '90%',
			data: {'product': this.product, 'organizationId': this.organizationId, 'productId': this.productId}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.editMode = false;
				this.onEditModeChange();
				this.product = result.product;
				this.replacementDate = this.convertToDateString(this.product.replacementDate);
				this.replaced = !!result.replaced;
			}
		});
	}

	saveProduct() {
		this.saving = true;
		this.resetValidation();
		this.product.status = 'PUBLISHED';
		this.product.productNumber = this.productNumberFormControl.value;
		this.product.productName = this.productNameFormControl.value;
		this.product.mainImageAltText = this.mainImageAltTextFormControl.value;
		this.product.mainImageDescription = this.description;
		if (this.editModeMainImage || !this.mainImage) {
			this.product.mainImageUrl = null;
			this.product.mainImageOriginalUrl = null;
			if (this.method === 'upload' && this.file) {
				this.product.mainImageUrl = this.file.name;
			} else if (this.method === 'link') {
				this.product.mainImageUrl = this.url;
				this.product.mainImageOriginalUrl = this.url;
			}
		}
		if (!this.productId) {
			if (this.method === 'upload' && this.file) {
				this.product.mainImageUrl = this.file.name;
			} else if (this.method === 'link') {
				this.product.mainImageUrl = this.url;
				this.product.mainImageOriginalUrl = this.url;
			}
		}

		if (this.product.ceMarked) {
			this.product.ceDirective = this.selectedCeDirective;
		}
		this.product.ceStandard = this.selectedCeStandard;
		if (this.selectedPreventiveMaintenanceValidity) {
			this.product.preventiveMaintenanceValidFrom = this.selectedPreventiveMaintenanceValidity;
		} else {
			this.product.preventiveMaintenanceValidFrom = null;
		}
		this.product.orderUnit = this.selectedOrderUnit;
		this.product.articleQuantityInOuterPackageUnit = this.selectedArticleQuantityInOuterPackageUnit;
		this.product.packageContentUnit = this.selectedPackageContentUnit;

		if (this.productId) {
			this.updateProduct();
		} else {
			this.createProduct();
		}
	}

	createProduct() {
		this.productService.createProduct(this.organizationId, this.product).subscribe(
			data => {
				this.loaded = false;
				this.product = data;
				this.productId = data.id;
				this.copiedProductId = null;
				this.productNumberFormControl.setValue(data.productNumber);
				this.productNameFormControl.setValue(data.productName);
				// this.mainImageAltTextFormControl.setValue(data.mainImageAltText);
				if (data.mainImageOriginalUrl) {
					this.url = data.mainImageUrl;
				} else {
					this.formData = new FormData();
					this.formData.append('mainimage', 'true');
					this.alternativeText = this.mainImageAltTextFormControl.value;
					if (this.method === 'upload') {
						this.formData.append('file', this.file);
					} else {
						this.formData.append('url', this.url);
					}
					if (this.description) {
						this.formData.append('description', this.description);
					}
					if (this.alternativeText) {
						this.formData.append('alternativeText', this.alternativeText);
					}
					this.productService.uploadMediaToProduct(this.organizationId, this.productId, 'image', this.formData).subscribe(
						media => {
							this.media = media;
							this.getMedia();
							this.alertService.clear();
							this.alertService.success('Sparat');
						}, error => {
							this.saving = false;
							this.alertService.clear();
							error.error.errors.forEach(err => {
								this.handleServerValidation(err);
								this.alertService.error(err);
							});
						});
					this.file = data.mainImageUrl;
				}

				// this.mainImageAltTextFormControl.setValue(this.alternativeText);
				// this.toggleEditMode();
				this.getMedia();
				// this.saving = false;
				// this.alertService.clear();
				// this.alertService.success('Sparat');


				this.toggleEditMode();
				this.saving = false;
				this.alertService.clear();
				this.alertService.success('Sparat');
			}, error => {
				this.saving = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.handleServerValidation(err);
					this.alertService.error(err);
				});
			}
		);

	}

	updateProduct() {
		this.productService.updateProduct(this.organizationId, this.productId, this.product).subscribe(
			data => {
				this.product = data;
				this.productNumberFormControl.setValue(data.productNumber);
				this.productNameFormControl.setValue(data.productName);
				this.mainImageAltTextFormControl.setValue(data.mainImageAltText);
				this.formData = new FormData();
				this.formData.append('mainimage', 'true');
				this.alternativeText = this.mainImageAltTextFormControl.value;
				if (this.method === 'upload') {
					this.formData.append('file', this.file);
				} else {
					this.formData.append('url', this.url);
				}
				if (this.description) {
					this.formData.append('description', this.description);
				}
				if (this.alternativeText) {
					this.formData.append('alternativeText', this.alternativeText);
				}
				if (this.editModeMainImage || !this.mainImage) {
					// this.mainImageAltTextFormControl.setValue(data.mainImageAltText);
					// if (data.mainImageOriginalUrl) {
					//   this.url = data.mainImageUrl;
					// }

					this.productService.uploadMediaToProduct(this.organizationId, this.productId, 'image', this.formData).subscribe(
						media => {
							this.media = media;
							this.getMedia();
							this.editMode = false;
							this.editModeMainImage = false;
							this.alertService.clear();
							this.alertService.success('Sparat');
						}, error => {
							this.alertService.clear();
							this.toggleEditMode();
							this.editMode = true;
							error.error.errors.forEach(err => {
								this.handleServerValidation(err);
								this.alertService.error(err);
							});
						});
				} else if (!this.editModeMainImage && this.mainImage) {
					this.productService.updateMediaToProduct(this.organizationId, this.productId, this.mainImage.id, this.formData).subscribe(
						media => {
							this.media = media;
							this.getMedia();
							this.editMode = false;
							this.editModeMainImage = false;
							this.alertService.clear();
							this.alertService.success('Sparat');
						}, error => {
							this.alertService.clear();
							this.editMode = true;
							error.error.errors.forEach(err => {
								this.handleServerValidation(err);
								this.alertService.error(err);
							});
						});
				}
				this.getMedia();
				this.toggleEditMode();
				this.saving = false;
				if (!this.editModeMainImage) {
					this.saving = false;
					this.alertService.clear();
					// this.alertService.success('Sparat');
				}
			}, error => {
				this.saving = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.handleServerValidation(err);
					this.alertService.error(err);
				});
			});
	}

	reactivateProduct() {
		this.reactivating = true;
		this.product.replacementDate = null;
		this.product.inactivateRowsOnReplacement = null;
		this.productService.updateProduct(this.organizationId, this.productId, this.product).subscribe(
			data => {
				this.product = data;
				if (!this.product.manufacturerElectronicAddress) {
					this.product.manufacturerElectronicAddress = new ElectronicAddress();
				}
				this.productNumberFormControl.setValue(data.productNumber);
				this.productNameFormControl.setValue(data.productName);
				this.mainImageAltTextFormControl.setValue(data.mainImageAltText);
				this.replacementDate = null;
				this.replaced = false;
				this.reactivating = false;
				this.alertService.clear();
				this.alertService.success('Produkten återaktiverad');
			}, error => {
				this.reactivating = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.handleServerValidation(err);
					this.alertService.error(err);
				});
			});
	}

	openDeleteProductDialog() {
		// this.saving = true;
		const dialogRef = this.dialog.open(DeleteProductDialogComponent, {
			width: '80%',
			data: {'organizationId': this.organizationId, 'productId': this.productId}
		});

		dialogRef.afterClosed().subscribe(
			res => {
				// this.saving = false;
				if (res) {
					this.alertService.clear();
					// this.alertService.success('Produkt borttagen');
					this.router.navigate(['/organization/' + this.organizationId + '/product']);
					this.alertService.success('Produkt borttagen');
				}
			},
			error => {
				this.alertService.clear();
				// this.saving = false;
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	editReplacementProducts() {
		const dialogRef = this.productReplacementDialog.open(ProductReplacementDialogComponent, {
			width: '90%',
			data: {'product': this.product, 'organizationId': this.organizationId, 'productId': this.productId}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.editMode = false;
				this.onEditModeChange();
				this.product = result.product;
				this.replacementDate = this.convertToDateString(this.product.replacementDate);
				this.replaced = !!result.replaced;
			}
		});
	}

	onCEToggle() {
		if (!this.product.ceMarked) {
			this.product.ceDirective = null;
		}
	}

	isFutureDate() {
		return this.replacementDate > new Date();
	}

	getMultiTooltip(values, data) {
		const selected = [];
		if (values) {
			values.forEach(value => {
				selected.push(data.find(x => x.id === value));
			});
		}
		if (selected && selected.length) {
			let msg = '';
			selected.forEach(res => {
				if (msg) {
					msg += ', ';
				}
				msg += res.value;
			});
			return msg;
		}
	}

	convertToDateString(date) {
		return new Date(date);
	}

	cancel() {
		this.getProduct(false);
		this.alternativeText = "";
		this.method = "";
		this.url = "";
		this.file = "";
		this.toggleEditMode();
		this.editModeMainImage = false;
	}

	goBack() {
		this.location.back();
	}

	fileSelected(file) {
		this.file = file;
	}

	openEditMediaDialog(mediaType, media) {
		const dialogRef = this.dialog.open(EditMediaDialogComponent, {
			width: '40%',
			data: {'organizationId': this.organizationId, 'productId': this.productId, 'mediaType': mediaType, 'media': media}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.getMedia();
			}
		});
	}

	openUploadMainImageDialog() {
		const dialogRef = this.dialog.open(UploadDialogComponent, {
			width: '40%',
			data: {'organizationId': this.organizationId, 'productId': this.productId, 'mediaType': 'mainImage'}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.mainImage = result;
			}
		});
	}

	openUploadImageDialog() {
		const dialogRef = this.dialog.open(UploadDialogComponent, {
			width: '40%',
			data: {'organizationId': this.organizationId, 'productId': this.productId, 'mediaType': 'image'}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.otherImages.push(result);
			}
		});
	}

	openUploadVideoDialog() {
		const dialogRef = this.dialog.open(UploadDialogComponent, {
			width: '40%',
			data: {'organizationId': this.organizationId, 'productId': this.productId, 'mediaType': 'video'}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.media.videos.push(result);
			}
		});
	}

	openUploadDocumentDialog() {
		const dialogRef = this.dialog.open(UploadDialogComponent, {
			width: '40%',
			data: {'organizationId': this.organizationId, 'productId': this.productId, 'mediaType': 'document'}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.media.documents.push(result);
			}
		});
	}

	openDeleteMediaDialog(media) {
		const dialogRef = this.dialog.open(DeleteMediaDialogComponent, {
			width: '40%',
			data: {'organizationId': this.organizationId, 'productId': this.productId, 'media': media}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.media.images = this.media.images.filter(item => item.id !== media.id);
				this.media.videos = this.media.videos.filter(item => item.id !== media.id);
				this.media.documents = this.media.documents.filter(item => item.id !== media.id);
				this.otherImages = this.otherImages.filter(item => item.id !== media.id);
				if (media.mainImage) {
					this.mainImage = null;
				}
			}
		});
	}

	handleServerValidation(error): void {
		switch (error.field) {
			case 'productNumber': {
				this.productNumberError = error.message;
				this.productNumberFormControl.setErrors(Validators.pattern(''));
				break;
			}
			case 'productName': {
				this.productNameError = error.message;
				this.productNameFormControl.setErrors(Validators.pattern(''));
				break;
			}
			case 'category': {
				this.categoryError = error.message;
				this.categoryFormControl.setErrors(Validators.pattern(''));
				break;
			}
			case 'mainImageUploadLink': {
				this.mainImageUploadLinkError = error.message;
				// this.mainImageUploadLinkFormControl.setErrors(Validators.pattern(''));
				break;
			}
			case 'mainImageAltText': {
				this.mainImageAltTextError = error.message;
				this.mainImageAltTextFormControl.setErrors(Validators.pattern(''));
				break;
			}
		}
	}

	resetValidation() {
		this.productNumberError = null;
		this.productNumberFormControl.markAsDirty();
		this.productNameError = null;
		this.productNameFormControl.markAsDirty();
		this.categoryError = null;
		this.categoryFormControl.markAsDirty();
		this.mainImageAltTextError = null;
		this.mainImageAltTextFormControl.markAsDirty();
		this.mainImageUploadLinkError = null;
	}

}

export interface Article {
	type: string;
	name: string;
	number: string;
	status: string;
	code: string;
}
