import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductReplacementDialogComponent} from './product-replacement-dialog.component';

describe('ProductReplacementDialogComponent', () => {
	let component: ProductReplacementDialogComponent;
	let fixture: ComponentFixture<ProductReplacementDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ProductReplacementDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ProductReplacementDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
