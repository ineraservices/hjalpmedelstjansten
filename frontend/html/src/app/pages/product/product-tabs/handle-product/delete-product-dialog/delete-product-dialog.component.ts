import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProductService} from "../../../../../services/product.service";
import {AlertService} from "../../../../../services/alert.service";


@Component({
	selector: 'app-delete-product-dialog',
	templateUrl: './delete-product-dialog.component.html',
	styleUrls: ['./delete-product-dialog.component.scss']
})
export class DeleteProductDialogComponent {

	loading = false;

	constructor(public dialogRef: MatDialogRef<DeleteProductDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private productService: ProductService,
							private alertService: AlertService) {
	}

	done() {
		// eslint-disable-next-line no-debugger
		// debugger;
		console.log('done has been pressed');
		this.loading = true;

		this.productService.deleteProduct(this.data.organizationId, this.data.productId).subscribe(
			res => {
				this.alertService.clear();
				this.alertService.success('Sparat');
				this.dialogRef.close('success');
				this.loading = false;

			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
					this.loading = false;
				});
			});

	}

}
