import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {CategoryDialogComponent} from '../../../category-dialog/category-dialog.component';
import {Product} from '../../../../../models/product/product.model';
import {Article} from '../../../../../models/product/article.model';
import {ProductService} from '../../../../../services/product.service';
import {AlertService} from '../../../../../services/alert.service';
import {HelptextService} from '../../../../../services/helptext.service';
import {ElectronicAddress} from '../../../../../models/electronic-address.model';

@Component({
	selector: 'app-product-replacement-dialog',
	templateUrl: './product-replacement-dialog.component.html',
	styleUrls: ['./product-replacement-dialog.component.scss']
})
export class ProductReplacementDialogComponent implements OnInit {

	displayedColumns = ['typ', 'benämning', 'artikelnummer', 'status', 'kategori'];

	saving = false;
	dataSource = null;
	dataSource2 = null;
	searchTotal;
	searchTotal2;
	offset = 25;
	pageFrom = 0;
	pageFrom2 = 0;
	pageTo = this.offset;
	pageTo2 = this.offset;
	pageToText = this.offset;
	pageToText2 = this.offset;

	products: Array<Product>;
	replaced = false;
	replacementDate: Date;
	selectedReplacementProducts: Array<Product>;
	articlesBasedOnProduct = [];
	articlesFittingToProduct = [];
	helpTexts;
	helpTextsLoaded = false;

	constructor(public dialogRef: MatDialogRef<CategoryDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private productService: ProductService,
							private alertService: AlertService,
							private helpTextService: HelptextService) {
	}

	ngOnInit() {
		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);
		this.productService.getProducts(this.data.organizationId).subscribe(
			products => {
				this.products = products.filter(product => product.id !== Number(this.data.productId));
				if (this.data.product.replacedByProducts && this.data.product.replacedByProducts.length) {
					this.replaced = true;
					const tmp = this.data.product.replacedByProducts;
					this.selectedReplacementProducts = this.products.filter(function (o) {
						return tmp.some(function (o2) {
							return o.id === o2.id;
						});
					});
				}
				this.productService.getArticlesBasedOnProduct(this.data.organizationId, this.data.productId, 0).subscribe(
					res => {
						this.articlesBasedOnProduct = res.body;
						this.searchTotal = res.headers.get('X-Total-Count');
						this.dataSource = new MatTableDataSource<Article>(this.articlesBasedOnProduct);
						this.checkLastPage(true);
					}, error => {
						this.alertService.clear();
						error.error.errors.forEach(err => {
							this.alertService.error(err);
						});
					}
				);
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

		this.productService.getArticlesFittingToProduct(this.data.organizationId, this.data.productId, 0).subscribe(
			res => {
				this.articlesFittingToProduct = res.body;
				this.searchTotal2 = res.headers.get('X-Total-Count');
				this.dataSource2 = new MatTableDataSource<Article>(this.articlesFittingToProduct);
				this.checkLastPage(false);
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

		if (this.data.product.replacementDate) {
			this.replacementDate = this.convertToDateString(this.data.product.replacementDate);
		}

	}

	nextPage(isBasedOnTable: boolean) {
		if (isBasedOnTable) {
			this.productService.getArticlesBasedOnProduct(this.data.organizationId, this.data.productId, this.pageTo).subscribe(
				res => {
					this.articlesBasedOnProduct = res.body;
					this.dataSource = new MatTableDataSource<Article>(this.articlesBasedOnProduct);
					this.pageFrom += this.offset;
					this.pageTo += this.offset;
					this.checkLastPage(true);
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		} else {
			this.productService.getArticlesFittingToProduct(this.data.organizationId, this.data.productId, this.pageTo2).subscribe(
				res => {
					this.articlesFittingToProduct = res.body;
					this.dataSource2 = new MatTableDataSource<Article>(this.articlesFittingToProduct);
					this.pageFrom2 += this.offset;
					this.pageTo2 += this.offset;
					this.checkLastPage(false);
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		}

	}

	previousPage(isBasedOnTable) {
		if (isBasedOnTable) {
			this.productService.getArticlesBasedOnProduct(this.data.organizationId, this.data.productId, this.pageFrom - this.offset).subscribe(
				res => {
					this.articlesBasedOnProduct = res.body;
					this.dataSource = new MatTableDataSource<Article>(this.articlesBasedOnProduct);
					this.pageTo -= this.offset;
					this.pageFrom -= this.offset;
					this.checkLastPage(true);
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		} else {
			this.productService.getArticlesFittingToProduct(
				this.data.organizationId, this.data.productId, this.pageFrom2 - this.offset).subscribe(
				res => {
					this.articlesFittingToProduct = res.body;
					this.dataSource2 = new MatTableDataSource<Article>(this.articlesFittingToProduct);
					this.pageTo2 -= this.offset;
					this.pageFrom2 -= this.offset;
					this.checkLastPage(false);
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		}

	}

	checkLastPage(isBasedOnTable: boolean) {
		if (isBasedOnTable) {
			if (this.pageTo >= this.searchTotal) {
				this.pageToText = this.searchTotal;
			} else {
				this.pageToText = this.pageTo;
			}
		} else {
			if (this.pageTo2 >= this.searchTotal2) {
				this.pageToText2 = this.searchTotal2;
			} else {
				this.pageToText2 = this.pageTo2;
			}
		}
	}

	convertToDateString(date) {
		return new Date(date);
	}

	getMillisecondsFromDate(date): number {
		if (date) {
			return date.getTime();
		}
		return null;
	}

	done() {
		this.saving = true;
		this.data.product.replacedByProducts = new Array<Product>();

		this.data.product.replacementDate = this.getMillisecondsFromDate(this.replacementDate);
		if (this.replaced) {
			this.selectedReplacementProducts.forEach(replacementProduct => {
				this.data.product.replacedByProducts.push(
					{'id': replacementProduct.id}
				);
			});
		}
		if (!this.data.product.inactivateRowsOnReplacement) {
			this.data.product.inactivateRowsOnReplacement = false;
		}
		this.productService.updateProduct(this.data.organizationId, this.data.product.id, this.data.product).subscribe(
			res => {
				if (!res.manufacturerElectronicAddress) {
					res.manufacturerElectronicAddress = new ElectronicAddress();
				}
				this.saving = false;
				this.alertService.success('Sparat');
				this.dialogRef.close({
					'product': res,
					'replaced': this.replaced,
				});
			}, error => {
				this.alertService.clear();
				this.saving = false;
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

	}

}
