import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {HandleProductComponent} from './handle-product/handle-product.component';
import {ConnectionComponent} from './connection/connection.component';
import {ActivatedRoute} from '@angular/router';
import {ProductService} from "../../../services/product.service";
import {AlertService} from "../../../services/alert.service";
import {HelptextService} from "../../../services/helptext.service";
import {saveAs} from "file-saver";

@Component({
	selector: 'app-product-tabs',
	templateUrl: './product-tabs.component.html',
	styleUrls: ['./product-tabs.component.scss']
})
export class ProductTabsComponent implements OnInit, AfterViewInit {
	@ViewChild(HandleProductComponent) childProduct!: HandleProductComponent;
	@ViewChild(ConnectionComponent) childProductConnection!: ConnectionComponent;
	loaded = false;
	redirectedFromSearch = false;
	organizationId: number;
	tabIndex = 0;
	productId: number;
	hasArticleBasedOnIt = true;

	query = '';
	queryParams = '';
	helpTexts;
	helpTextsLoaded = false;
	isCheckingIfProductIsRemovable: boolean;
	childEdit = false;

	constructor(private route: ActivatedRoute,
							private productService: ProductService,
							private alertService: AlertService,
							private helpTextService: HelptextService) {
		this.route.queryParams.subscribe(qParams => {
			if (qParams.search) {
				this.redirectedFromSearch = qParams.search;
			}
		});
	}

	ngOnInit() {
		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);
	}


	ngAfterViewInit(): void {
		this.organizationId = this.childProduct?.organizationId;
		this.productId = this.childProduct?.productId;
		if (this.productId) {
			this.isCheckingIfProductIsRemovable = true;
			this.productService.searchArticles('', this.organizationId, this.productId).subscribe(res => {
				this.hasArticleBasedOnIt = res.headers.get('X-Total-Count') !== '0';
				this.loaded = true;
			});

		}
		this.isCheckingIfProductIsRemovable = false;
	}

	toggleEditMode() {
		this.childProduct.toggleEditMode();
	}

	exportSearchResults() {
		const query = '?query=' + this.childProductConnection?.query + this.childProductConnection?.queryParams;

		this.productService.exportArticlesForProduct(this.organizationId, this.productId, query).subscribe(
			data => {
				saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	onTabSelection(event) {
		this.tabIndex = event.index;
		if (event.index === 1) {
			this.childProductConnection.product = this.childProduct?.product;
			this.childProductConnection.productId = this.childProduct?.productId;
			if (!this.childProductConnection?.productId) {
				this.childProductConnection?.initConnection(this.childProduct?.productId);
			} else {
				this.childProductConnection?.getProduct();
			}
		}
	}

}
