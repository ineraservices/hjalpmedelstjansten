import {Component} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../../../../services/product.service';
import {Product} from '../../../../models/product/product.model';
import {ProductOrArticle} from '../../product.component';
import {HelptextService} from '../../../../services/helptext.service';
import {AuthService} from '../../../../auth/auth.service';
import {AlertService} from '../../../../services/alert.service';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

@Component({
	selector: 'app-connection',
	templateUrl: './connection.component.html',
	styleUrls: ['./connection.component.scss']
})
export class ConnectionComponent {

	displayedColumns = ['typ', 'benämning', 'artikelnummer', 'status', 'kategori'];

	redirectedFromSearch = false;
	loaded = false;
	loading = false;
	organizationId: number;
	productId: number;
	product: Product;
	articles = [];
	dataSource = null;
	query = '';
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	queryParams = '';
	searchTotal;
	offset = 25;
	totalPages;
	currentPage = 1;
	paginatorArray = [];
	allPages;
	helpTexts;
	helpTextsLoaded = false;
	isOwnOrganization = false;

	// Filters
	filterH = false;
	filterT = false;
	filterR = false;
	filterTj = false;
	filterI = false;
	filterPublished = false;
	filterDiscontinued = false;

	constructor(private authService: AuthService,
							private productService: ProductService,
							private route: ActivatedRoute,
							private alertService: AlertService,
							private helpTextService: HelptextService,
							private router: Router) {
		this.route.params.subscribe(params => {
			this.organizationId = params.organizationId;
			this.productId = params.productId;
			this.isOwnOrganization = Number(this.organizationId) === this.authService.getOrganizationId();
		});
		this.route.queryParams.subscribe(qParams => {
			if (qParams.search) {
				this.redirectedFromSearch = qParams.search;
			}
		});
		// The user is considered done with their search after 500 ms without further input
		this.searchChanged.pipe(
			debounceTime(500),
			distinctUntilChanged())
			.subscribe(query => {
				this.query = encodeURIComponent(query);
				this.onQueryChange();
			}
			);
	}

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
	}

	initConnection(productId) {
		this.productId = productId;
		this.getProduct();
	}

	getProduct() {
		if (!this.helpTextsLoaded) {
			this.helpTexts = this.helpTextService.getTexts().subscribe(
				texts => {
					this.helpTexts = texts;
					this.helpTextsLoaded = true;
				}
			);
		}
		this.productService.getProduct(this.organizationId, this.productId).subscribe(
			res => {
				this.product = res;
				this.loaded = true;
				this.searchArticles('');
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	onFilterChange() {
		this.queryParams = '';
		this.currentPage = 1;
		if (this.filterH) {
			this.queryParams += '&type=H';
		}
		if (this.filterT) {
			this.queryParams += '&type=T';
		}
		if (this.filterR) {
			this.queryParams += '&type=R';
		}
		if (this.filterTj) {
			this.queryParams += '&type=Tj';
		}
		if (this.filterI) {
			this.queryParams += '&type=I';
		}
		if (this.filterPublished) {
			this.queryParams += '&status=PUBLISHED';
		}
		if (this.filterDiscontinued) {
			this.queryParams += '&status=DISCONTINUED';
		}
		this.searchArticles(this.query + this.queryParams);
	}

	onQueryChange() {
		this.onFilterChange();
	}

	searchArticles(query) {
		this.loading = true;
		this.productService.searchArticles(query, this.organizationId, this.productId).subscribe(
			res => {
				this.searchTotal = res.headers.get('X-Total-Count');
				if (this.searchTotal !== '0') {
					this.articles = res.body;
				} else {
					this.articles = [];
				}
				this.totalPages = Math.ceil(this.searchTotal / this.offset);
				this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
				this.dataSource = new MatTableDataSource<Article>(this.articles);
				this.generatePaginator();
				this.loading = false;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	goToPage(page) {
		if (page !== '...' && page !== this.currentPage) {
			this.loading = true;
			this.currentPage = page;
			this.productService.searchArticles(
				this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset, this.organizationId, this.productId).subscribe(
				res => {
					this.articles = res.body;
					this.dataSource = new MatTableDataSource<ProductOrArticle>(this.articles);
					this.currentPage = page;
					this.generatePaginator();
					this.loading = false;
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		}
	}

	generatePaginator() {
		let hasFirstEllipsis = false;
		let hasSecondEllipsis = false;
		this.paginatorArray = [];
		if (this.searchTotal > this.offset) {
			if (this.totalPages < 6) {
				this.paginatorArray = this.allPages;
			} else {
				this.allPages.forEach((pageNumber) => {
					if (pageNumber === 1
						|| pageNumber === this.currentPage
						|| pageNumber === this.totalPages
						|| pageNumber === this.currentPage + 1
						|| pageNumber === this.currentPage - 1) {
						this.paginatorArray.push(pageNumber);
					} else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
						this.paginatorArray.push('...');
						hasFirstEllipsis = true;
					} else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
						this.paginatorArray.push('...');
						hasSecondEllipsis = true;
					}
				});
			}
		}
	}

	async routeTo(element: any): Promise<void> {
		const href = "/organization/" + this.organizationId + "/" + element.type.toLowerCase() + "/" + element.id + "/handle";
		await this.router.navigateByUrl(!!this.redirectedFromSearch ? href : href + "?search=true");
	}
}

export interface Article {
	type: string;
	articleName: string;
	articleNumber: string;
	status: string;
	code: string;
}
