import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProductService} from '../../../services/product.service';
import {Category} from '../../../models/product/category.model';
import {HelptextService} from '../../../services/helptext.service';
import {AlertService} from '../../../services/alert.service';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {CommonService} from '../../../services/common.service';

@Component({
	selector: 'app-category-dialog',
	templateUrl: './category-dialog.component.html',
	styleUrls: ['./category-dialog.component.scss']
})
export class CategoryDialogComponent implements OnInit {
	loaded = false;
	query: string;
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	qParam = 'excludeCodelessCategories=true';
	searchResults: Array<Category>;
	levelOneCategories: Array<Category>;
	levelTwoCategories: Array<Category>;
	levelThreeCategories: Array<Category>;
	levelFourCategories: Array<Category>;
	selectedLevelOne: number;
	selectedLevelTwo: number;
	selectedLevelThree: number;
	selectedLevelFour: number;
	selectedCategory: Category = new Category();
	extendedCategories: Array<Category>;
	helpTexts;
	flexDirection: string;

	constructor(public dialogRef: MatDialogRef<CategoryDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private productService: ProductService,
							private alertService: AlertService,
							private helpTextService: HelptextService,
							private commonService: CommonService) {
		this.flexDirection = this.commonService.isUsingIE() ? 'row' : 'column';
		// The user is considered done with their search after 500 ms without further input
		this.searchChanged.pipe(
			debounceTime(500),
			distinctUntilChanged())
			.subscribe(query => {
				this.query = encodeURIComponent(query);
				this.onQueryChange();
			}
			);
	}

	ngOnInit() {
		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.loaded = true;
			}
		);
		if (this.data) {
			if (this.data.selectedCategories) {
				this.extendedCategories = this.data.selectedCategories;
			}
			this.qParam = this.data.includeCodelessCategories ? '' : 'excludeCodelessCategories=true';
			if (this.data.excludeCodeCategories) {
				this.qParam = 'excludeCodeCategories=true';
			}
		}

		this.productService.getCategories('?' + this.qParam).subscribe(
			res => {
				this.levelOneCategories = res;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	clearSearch() {
		this.query = '';
		this.tmpQuery = '';
		this.selectedCategory = new Category();
	}

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
	}

	onQueryChange() {
		this.searchCategories(this.query);
	}

	searchCategories(query) {
		this.productService.searchCategories(query + '&' + this.qParam).subscribe(
			res => {
				this.searchResults = res;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	onCategorySelection(category, level) {
		this.selectedCategory = category;

		switch (level) {
		case 1:
			this.selectedLevelOne = category.id;
			this.selectedLevelTwo = null;
			this.selectedLevelThree = null;
			this.selectedLevelFour = null;
			this.levelTwoCategories = [];
			this.levelThreeCategories = [];
			this.levelFourCategories = [];
			break;
		case 2:
			this.selectedLevelTwo = category.id;
			this.selectedLevelThree = null;
			this.selectedLevelFour = null;
			this.levelThreeCategories = [];
			this.levelFourCategories = [];
			break;
		case 3:
			this.selectedLevelThree = category.id;
			this.selectedLevelFour = null;
			this.levelFourCategories = [];
			break;
		case 4:
			this.selectedLevelFour = category.id;
			break;
		case 5:
			this.selectedLevelOne = null;
			this.selectedLevelTwo = null;
			this.selectedLevelThree = null;
			this.selectedLevelFour = null;
			this.levelTwoCategories = [];
			this.levelThreeCategories = [];
			this.levelFourCategories = [];
			break;
		}

		if (!category.articleType) {
			window.scroll(0, 0);
			this.productService.getChildCategories(category.code).subscribe(
				res => {
					switch (level) {
					case 1:
						this.levelTwoCategories = res;
						break;
					case 2:
						this.levelThreeCategories = res;
						break;
					case 3:
						this.levelFourCategories = res;
						break;
					}
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		} else if (this.data && this.extendedCategories && this.extendedCategories.indexOf(category) === -1) {
			this.extendedCategories.push(category);
		}
	}

	remove(category) {
		const index = this.extendedCategories.indexOf(category);

		if (index >= 0) {
			this.extendedCategories.splice(index, 1);
		}
	}

	done() {
		if (!this.data || !this.data.selectedCategories) {
			this.dialogRef.close(this.selectedCategory);
		} else {
			this.dialogRef.close(this.extendedCategories);
		}
	}
}
