import {Component, OnInit} from '@angular/core';
import {ProductService} from '../../services/product.service';
import {MatDialog} from '@angular/material/dialog';
import {MatSort} from '@angular/material/sort';
import {FormControl} from '@angular/forms';
import {CategoryDialogComponent} from '../product/category-dialog/category-dialog.component';
import {AlertService} from '../../services/alert.service';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {OrganizationService} from '../../services/organization.service';
import {AuthService} from '../../auth/auth.service';
import {CommonService} from '../../services/common.service';
import {HelptextService} from '../../services/helptext.service';
import {StorageHelper as BigSearchStorage} from '../../helpers/storage.helper';
import {Organization} from "../../models/organization/organization.model";
import {saveAs} from "file-saver";
import {Router} from "@angular/router";
import {MatTableDataSource} from "@angular/material/table";

@Component({
	selector: 'app-search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

	displayedColumns = ['articleType', 'name', 'number', 'organizationName', 'code'];

	// Sets the storage index to be used.
	storage = new BigSearchStorage('BigSearch');

	loading = true;
	isCustomer: boolean;
	productsAndArticles = [];
	categoryProperties = [];
	dataSource = new MatTableDataSource<ProductOrArticle>();
	query = '';
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	queryParams = '';
	searchTotal;
	offset = 25;
	limit = 25;
	totalPages;
	currentPage = 1;
	allPages;
	flexDirection: string;
	helpTexts;
	helpTextsLoaded = false;
	suppliers: Organization[];
	suppliersLoaded = false;

	// Sorting
	sortResults = true;
	sortOrder = 'asc';
	sortType = 'name';

	// Filters
	filterProducts = false;
	filterArticles = false;
	filterH = false;
	filterT = false;
	filterR = false;
	filterTj = false;
	filterI = false;
	filterPublished = false;
	filterDiscontinued = false;
	filterHasPrice = false;
	filterHasGP = false;
	filterCeMarked = false;
	filterCategory = '';
	filterCategoryProperty = '';
	selectedSupplier = '-1';

	categoryResult = '';

	// Form controls
	categoryFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	// SessionStorage
	loadSessionStorage = false;

	// Validation errors
	categoryError;

	constructor(private authService: AuthService,
							private organizationService: OrganizationService,
							private productService: ProductService,
							private dialog: MatDialog,
							private alertService: AlertService,
							private commonService: CommonService,
							private helpTextService: HelptextService,
							private router: Router) {

		this.flexDirection = this.commonService.isUsingIE() ? 'row' : 'column';

		// The user is considered done with their search after 500 ms without further input
		this.searchChanged.pipe(
			debounceTime(500),
			distinctUntilChanged())
			.subscribe(query => {
				this.query = encodeURIComponent(query);
				this.onQueryChange();
				this.currentPage = 1;

			}
			);
	}

	// TODO! Fix in backend somewthing to distinguish Tiso

	ngOnInit() {
		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);

		this.organizationService.searchOrganizations('&type=SUPPLIER&limit=0').subscribe(
			orgs => {
				this.suppliers = orgs.body;
				this.suppliersLoaded = true;
			}
		);

		this.organizationService.getOrganization(this.authService.getOrganizationId()).subscribe(
			res => {
				this.isCustomer = res.organizationType === 'CUSTOMER';

				if (this.loadSessionStorage !== true) {
					this.filterArticles = this.isCustomer;
				}
				this.filterPublished = true;
				if (this.storage.hasPageBeenVisisted()) {
					this.loadSessionStorage = true;
					this.getSessionStorage();
				} else {
					// Else set initial values.
					this.sortResults = false;
					this.sortOrder = '';
					this.sortType = '';
					this.filterProducts = true;
					this.limit = 25;
					this.offset = 25;
					this.selectedSupplier = '-1';
				}

				if (this.tmpQuery !== '') {
					this.onQueryChange();
				} else {
					this.onFilterChange();
				}
				this.loadSessionStorage = false;

			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			});
	}


	setSessionStorage() {
		this.storage.setItem('Query', this.tmpQuery.toString());
		this.storage.setItem('FilterProducts', this.filterProducts.toString());
		this.storage.setItem('FilterArticles', this.filterArticles.toString());
		this.storage.setItem('FilterH', this.filterH.toString());
		this.storage.setItem('FilterT', this.filterT.toString());
		this.storage.setItem('FilterR', this.filterR.toString());
		this.storage.setItem('FilterTj', this.filterTj.toString());
		this.storage.setItem('FilterI', this.filterI.toString());
		this.storage.setItem('Limit', this.limit.toString());
		this.storage.setItem('Offset', this.offset.toString());
		this.storage.setItem('Sort', this.sortResults.toString());
		this.storage.setItem('SortOrder', this.sortOrder.toString());
		this.storage.setItem('SortType', this.sortType.toString());
		this.storage.setItem('FilterPublished', this.filterPublished.toString());
		this.storage.setItem('FilterDiscontinued', this.filterDiscontinued.toString());
		this.storage.setItem('FilterCategory', this.filterCategory);
		this.storage.setItem('FilterCategoryProperty', this.filterCategoryProperty);
		this.storage.setItem('CurrentPage', this.currentPage.toString());
		this.storage.setItem('selectedSupplier', this.selectedSupplier);

		// Same as CategoryFormControl, to avoid dupe storage i'm putting it here
		if (this.categoryResult !== 'undefined') {
			this.storage.setItem('CategoryResult', this.categoryResult);
		} else {
			this.storage.setItem('CategoryResult', '');
		}

		this.storage.setItem('CategoryProperties', JSON.stringify(this.categoryProperties));
		this.storage.setItem('HelpTextsLoaded', this.helpTextsLoaded.toString());
	}

	getSessionStorage() {

		this.filterProducts = this.storage.getItemBoolean('FilterProducts');
		this.filterArticles = this.storage.getItemBoolean('FilterArticles');
		this.filterH = this.storage.getItemBoolean('FilterH');
		this.filterT = this.storage.getItemBoolean('FilterT');
		this.filterR = this.storage.getItemBoolean('FilterR');
		this.filterTj = this.storage.getItemBoolean('FilterTj');
		this.filterI = this.storage.getItemBoolean('FilterI');
		this.limit = this.storage.getItemNumber('Limit');
		this.offset = this.storage.getItemNumber('Offset');
		this.sortResults = this.storage.getItemBoolean('Sort');
		this.sortOrder = this.storage.getItemString('SortOrder');
		this.sortType = this.storage.getItemString('SortType');
		this.currentPage = this.storage.getItemNumber('CurrentPage');
		this.filterPublished = this.storage.getItemBoolean('FilterPublished');
		this.filterDiscontinued = this.storage.getItemBoolean('FilterDiscontinued');
		this.categoryFormControl.setValue(this.storage.getItemString('CategoryResult'));
		this.filterCategory = this.storage.getItemString('FilterCategory');
		this.filterCategoryProperty = this.storage.getItemString('FilterCategoryProperty');
		this.helpTextsLoaded = this.storage.getItemBoolean('HelpTextsLoaded');
		this.categoryProperties = this.storage.getItemJSON('CategoryProperties');
		this.categoryResult = this.storage.getItemString('CategoryResult');
		this.selectedSupplier = this.storage.getItemString('selectedSupplier');

		// Query needs to be at the end of the method.
		if (this.storage.getItemString('Query') !== 'null' || this.storage.getItemString('Query') !== '') {
			this.tmpQuery = this.storage.getItemString('Query');
			this.query = this.storage.getItemString('Query');
		}
	}

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
	}

	onSupplierChange() {
		this.onFilterChange();
	}

	onQueryChange() {
		this.sortResults = false;
		// Checks to see if this is a load from sessionStorage, if it is set sortType to the previous session.
		if (this.loadSessionStorage) {
			this.sortType = this.storage.getItemString('bigSearchSortType');
			this.sortOrder = this.storage.getItemString('bigSearchSortOrder');

		} else {
			this.sortResults = false;
			this.currentPage = 1;
			this.sortType = '';
			this.sortOrder = '';
		}
		this.onFilterChange();
	}

	onCategoryPropertyChange() {
		const tmp = this.filterCategoryProperty;
		this.filterCategoryProperty = '';

		this.categoryProperties.forEach(property => {
			switch (property.type) {
			case 'DECIMAL':
				if (property.decimalFromValue || property.decimalToValue) {
					if (property.decimalFromValue && property.decimalToValue && (property.decimalFromValue > property.decimalToValue)) {
						this.alertService.errorWithMessage('KATEGORISPECIFIKA EGENSKAPER: "' + property.name + '" är felaktigt angivet');
						break;
					}
					this.alertService.clear();
					this.filterCategoryProperty += '&csp_' + property.id + '_from=' +
								(property.decimalFromValue ? property.decimalFromValue : '') + '&csp_' + property.id + '_to='
								+ (property.decimalToValue ? property.decimalToValue : '');
				}
				break;
			case 'INTERVAL':
				if (property.intervalFromValue || property.intervalToValue) {
					if (property.intervalFromValue && property.intervalToValue && (property.intervalFromValue > property.intervalToValue)) {
						this.alertService.errorWithMessage('KATEGORISPECIFIKA EGENSKAPER: "' + property.name + '" är felaktigt angivet');
						break;
					}
					this.alertService.clear();
					this.filterCategoryProperty += '&csp_' + property.id + '_from=' +
								(property.intervalFromValue ? property.intervalFromValue : '') + '&csp_' + property.id + '_to='
								+ (property.intervalToValue ? property.intervalToValue : '');
				}
				break;
			case 'VALUELIST_SINGLE':
				if (property.singleListValue) {
					this.filterCategoryProperty += '&csp_' + property.id + '=' + property.singleListValue;
				}
				break;
			case 'VALUELIST_MULTIPLE':
				if (property.multipleListValue && property.multipleListValue.length) {
					property.multipleListValue.forEach(val => {
						this.filterCategoryProperty += '&csp_' + property.id + '=' + val;
					});
				}
				break;
			case 'TEXTFIELD':
				if (property.textValue) {
					this.filterCategoryProperty += '&csp_' + property.id + '=' + property.textValue;
				}
				break;
			}
		}, error => {
			this.alertService.clear();
			error.error.errors.forEach(err => {
				this.alertService.error(err);
			});
		}
		);

		if (this.filterCategoryProperty !== tmp) {

			this.onFilterChange();
		}
	}

	sortData(matSort: MatSort) {

		this.sortResults = true;
		switch (this.sortOrder) {
		case 'asc':
			this.sortOrder = 'desc';
			break;
		default:
			this.sortOrder = 'asc';
			break;
		}

		this.sortType = matSort.active;
		this.onFilterChange();

	}

	// Should be called "render()" and be built as a general method instead of what it is
	onFilterChange() {

		this.loading = true;
		this.queryParams = '';
		if (!this.filterArticles) {
			this.filterH = false;
			this.filterT = false;
			this.filterR = false;
			this.filterTj = false;
			this.filterI = false;
		}
		this.queryParams += this.filterProducts ? '&includeProducts=true' : '&includeProducts=false';
		this.queryParams += this.filterArticles ? '&includeArticles=true' : '&includeArticles=false';
		if (this.filterH) {
			this.queryParams += '&type=H';
		}
		if (this.filterT) {
			this.queryParams += '&type=T';
		}
		if (this.filterR) {
			this.queryParams += '&type=R';
		}
		if (this.filterTj) {
			this.queryParams += '&type=Tj';
		}
		if (this.filterI) {
			this.queryParams += '&type=I';
		}
		if (this.filterPublished) {
			this.queryParams += '&status=PUBLISHED';
		}
		if (this.filterDiscontinued) {
			this.queryParams += '&status=DISCONTINUED';
		}
		if (this.filterHasPrice) {
			this.queryParams += '&hasPrice=true';
		}
		if (this.filterHasGP) {
			this.queryParams += '&hasGP=true';
		}
		if (this.filterCeMarked) {
			this.queryParams += '&ceMarked=true';
		}
		if (this.selectedSupplier !== "-1") {
			this.queryParams += '&supplier=' + this.selectedSupplier.split(":")[0];
		}

		this.queryParams += '&limit=' + this.limit;

		if (this.sortResults) {
			this.queryParams += '&sortOrder=' + this.sortOrder;
			this.queryParams += '&sortType=' + this.sortType;
		}

		if (this.filterCategory) {
			this.queryParams += this.filterCategory + this.filterCategoryProperty;
		}

		if (this.currentPage > 1) {
			this.goToPage(this.currentPage);
		} else {
			if (this.query.length > 2) {
				this.searchProductsAndArticles(this.query + this.queryParams);
			} else {
				this.searchProductsAndArticles(this.queryParams);
			}
		}

		this.categoryProperties.sort((a, b) => a.orderIndex - b.orderIndex);
		this.setSessionStorage();
		this.loading = false;

	}

	changePreferredAmountOfArticlesPerPage(limit: number): void {
		this.limit = limit;
		this.offset = limit;
		this.setCurrentPageSessionStorage(1);
		this.goToPage(1);
		this.onFilterChange();
	}

	searchProductsAndArticles(query) {
		this.loading = true;

		this.productService.search(query).subscribe(res => {
			this.searchTotal = res.headers.get('X-Total-Count');
			this.productsAndArticles = this.searchTotal !== '0' ? res.body : [];
			this.totalPages = Math.ceil(this.searchTotal / this.offset);
			this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
			this.dataSource = new MatTableDataSource<ProductOrArticle>(this.productsAndArticles);

		}, error => {
			this.alertService.clear();
			error.error.errors.forEach(err => {
				this.alertService.error(err);
			});
		}
		);
	}

	goToPage(page) {
		if (page !== '...' && page !== this.currentPage || this.loadSessionStorage) {
			this.loading = true;
			this.currentPage = page;

			this.productService.search(this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset).subscribe(res => {
				this.productsAndArticles = res.body;

				this.searchTotal = res.headers.get('X-Total-Count');
				this.productsAndArticles = this.searchTotal !== '0' ? res.body : [];
				this.totalPages = Math.ceil(this.searchTotal / this.offset);
				this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
				this.dataSource = new MatTableDataSource<ProductOrArticle>(this.productsAndArticles);

				this.currentPage = page;
				this.storage.setItem('CurrentPage', page.toString());
				this.storage.setItem('Offset', this.offset.toString());

				this.loading = false;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
			);
		}
	}

	exportSearchResults() {
		const query = '?query=' + this.query + this.queryParams;
		this.productService.exportSearchResults(query).subscribe(
			data => {
				saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	openCategoryDialog() {
		const dialogRef = this.dialog.open(CategoryDialogComponent, {
			width: '90%'
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.sortResults = false;
				this.sortOrder = '';
				this.sortType = '';
				this.categoryResult = result.code + ' ' + result.name;

				if (result.code) {
					this.categoryFormControl.setValue(result.code + ' ' + result.name);
				} else {
					this.categoryFormControl.setValue(result.name);
				}
				this.filterCategory = '&category=' + result.id;
				this.productService.getCategoryProperties(result.id).subscribe(
					res => {
						this.categoryProperties = res;
						this.filterCategoryProperty = '';
						this.onFilterChange();
					}, error => {
						this.alertService.clear();
						error.error.errors.forEach(err => {
							this.alertService.error(err);
						});
					}
				);
			}
		});
	}

	clearCategory() {
		this.categoryFormControl.setValue('');
		this.filterCategory = '';
		this.categoryProperties = [];
		this.filterCategoryProperty = '';
		this.categoryResult = '';
		this.onFilterChange();

	}

	private setCurrentPageSessionStorage(pageNumber: number) {
		this.storage.setItem('CurrentPage', pageNumber.toString());
	}
}


export interface ProductOrArticle {
	type: string;
	articleName: string;
	articleNumber: string;
	status: string;
	code: string;
	articleType: string;
}
