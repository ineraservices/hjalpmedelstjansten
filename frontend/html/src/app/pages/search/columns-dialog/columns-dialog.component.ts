import {Component} from '@angular/core';
import {CategoryDialogComponent} from '../../product/category-dialog/category-dialog.component';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
	selector: 'app-columns-dialog',
	templateUrl: './columns-dialog.component.html',
	styleUrls: ['./columns-dialog.component.scss']
})
export class ColumnsDialogComponent {

	constructor(public dialogRef: MatDialogRef<CategoryDialogComponent>) {
	}

	done() {
	}

}
