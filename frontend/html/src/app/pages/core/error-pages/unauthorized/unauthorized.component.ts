import {Component} from '@angular/core';

@Component({
	selector: 'app-unauthorized',
	template: `
    <div class="logged-out-wrapper">
    <div class="logged-out">
      <h1>Du har blivit utloggad pga inaktivitet</h1>
      <a routerLink="/">Logga in igen</a>
    </div>

  </div>`,

	styles: [`
    .logged-out-wrapper {
      margin: 200px auto;
      width: 445px;
    }

    .logged-out {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
    }
  `]
})
export class UnauthorizedComponent {

	constructor() {
	}

}
