import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {ImportStatusService} from "../../services/importstatus.service";
import {ActivatedRoute} from '@angular/router';
import {OrganizationService} from '../../services/organization.service';
import {HelptextService} from '../../services/helptext.service';
import {AlertService} from '../../services/alert.service';
import {Subject} from 'rxjs';
import {CommonService} from '../../services/common.service';

@Component({
	selector: 'app-product',
	templateUrl: './importstatus.component.html',
	styleUrls: ['./importstatus.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class ImportStatusComponent implements OnInit {

	displayedColumns = ['filnamn', 'importerad', 'inläst', 'status'];

	// Sets the storage index to be used.
	// storage = new ProductStorage('ImportStatus');
	loadSessionStorage = false;

	loading = true;
	organizationId: number;
	importStatus = [];
	dataSource = null;
	query = '';
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	queryParams = '';
	searchTotal;
	offset = 25;
	totalPages;
	currentPage = 1;
	paginatorArray = [];
	allPages;
	helpTexts;
	helpTextsLoaded = false;
	flexDirection: string;


	constructor(private importStatusService: ImportStatusService,
							private organizationService: OrganizationService,
							private route: ActivatedRoute,
							private dialog: MatDialog,
							private helpTextService: HelptextService,
							private alertService: AlertService,
							private commonService: CommonService) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}
		this.route.params.subscribe(params => {
			this.organizationId = params.organizationId;
		});
		// The user is considered done with their search after 500 ms without further input
	}

	ngOnInit() {
		this.getImportReadStatus();
		// if (this.storage.hasPageBeenVisisted()) {
		//   this.loadSessionStorage = true;
		//   this.getSessionStorage();
		//   this.onFilterChange();
		//   this.loadSessionStorage = false;
		// } else {
		//   this.onFilterChange();
		// }
	}

	// onQueryChanged(text: string) {
	//   this.searchChanged.next(text);
	//   this.currentPage = 1;
	// }

	// onQueryChange() {
	//   this.onFilterChange();
	//
	// }


	// setSessionStorage() {
	//
	//   // this.storage.setItem('Query', this.tmpQuery.toString());
	//   // this.storage.setItem('FilterProducts', this.filterProducts.toString());
	//   // this.storage.setItem('FilterArticles', this.filterArticles.toString());
	//   // this.storage.setItem('FilterH', this.filterH.toString());
	//   // this.storage.setItem('FilterT', this.filterT.toString());
	//   // this.storage.setItem('FilterR', this.filterR.toString());
	//   // this.storage.setItem('FilterTj', this.filterTj.toString());
	//   // this.storage.setItem('FilterI', this.filterI.toString());
	//   // this.storage.setItem('FilterPublished', this.filterPublished.toString());
	//   // this.storage.setItem('FilterDiscontinued', this.filterDiscontinued.toString());
	//   // this.storage.setItem('CurrentPage', this.currentPage.toString());
	//   // this.storage.setItem('HelpTextsLoaded', this.helpTextsLoaded.toString());
	// }

	getSessionStorage() {

		//   this.filterProducts = this.storage.getItemBoolean('FilterProducts');
		//   this.filterArticles = this.storage.getItemBoolean('FilterArticles');
		//   this.filterH = this.storage.getItemBoolean('FilterH');
		//   this.filterT = this.storage.getItemBoolean('FilterT');
		//   this.filterR = this.storage.getItemBoolean('FilterR');
		//   this.filterTj = this.storage.getItemBoolean('FilterTj');
		//   this.filterI = this.storage.getItemBoolean('FilterI');
		//   this.currentPage = this.storage.getItemNumber('CurrentPage');
		//   this.filterPublished = this.storage.getItemBoolean('FilterPublished');
		//   this.filterDiscontinued = this.storage.getItemBoolean('FilterDiscontinued');
		//   this.helpTextsLoaded = this.storage.getItemBoolean('HelpTextsLoaded');
		//
		//   //Query needs to be at the end of the method.
		//   if(this.storage.getItemString('Query') !== 'null' || this.storage.getItemString('Query') !== '' ) {
		//     this.tmpQuery = this.storage.getItemString('Query');
		//     this.query = this.storage.getItemString('Query');
		// }
	}

	// onFilterChange() {
	//   this.loading = true;
	//   this.queryParams = '';
	//
	//   if (!this.filterArticles) {
	//     this.filterH = false;
	//     this.filterT = false;
	//     this.filterR = false;
	//     this.filterTj = false;
	//     this.filterI = false;
	//   }
	//   this.filterProducts ? this.queryParams += '&includeProducts=true' : this.queryParams += '&includeProducts=false';
	//   this.filterArticles ? this.queryParams += '&includeArticles=true' : this.queryParams += '&includeArticles=false';
	//   this.filterH ? this.queryParams += '&type=H' : this.queryParams = this.queryParams;
	//   this.filterT ? this.queryParams += '&type=T' : this.queryParams = this.queryParams;
	//   this.filterR ? this.queryParams += '&type=R' : this.queryParams = this.queryParams;
	//   this.filterTj ? this.queryParams += '&type=Tj' : this.queryParams = this.queryParams;
	//   this.filterI ? this.queryParams += '&type=I' : this.queryParams = this.queryParams;
	//   this.filterPublished ? this.queryParams += '&status=PUBLISHED' : this.queryParams = this.queryParams;
	//   this.filterDiscontinued ? this.queryParams += '&status=DISCONTINUED' : this.queryParams = this.queryParams;
	//
	//   // if(this.currentPage > 1) {
	//   //   this.goToPage(this.currentPage);
	//   // } else {
	//   //   this.searchProductsAndArticles(this.query + this.queryParams);
	//   // }
	//
	//   // this.setSessionStorage();
	//   this.loading = false;
	//
	// }


	getImportReadStatus() {
		this.loading = true;
		this.importStatusService.getAllImportsStatus().subscribe(
			res => {
				this.importStatus = res.body;
				for (let i = 0; i < this.importStatus.length; i++) {
					if (this.importStatus[i].importStartDate != null) {
						const startDate = new Date(this.importStatus[i].importStartDate);
						this.importStatus[i].importStartDate = startDate.toLocaleDateString() + " " + startDate.toLocaleTimeString();
					}

					if (this.importStatus[i].importEndDate != null) {
						const endDate = new Date(this.importStatus[i].importStartDate);
						this.importStatus[i].importEndDate = endDate.toLocaleDateString() + " " + endDate.toLocaleTimeString();
						console.log(endDate);
					}


				}
				this.dataSource = new MatTableDataSource<ImportStatus>(this.importStatus);
				this.loading = false;
				this.helpTextsLoaded = true;
				// console.log(res.body);
				this.searchTotal = this.dataSource.data.length;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	// goToPage(page) {
	//   if (page !== '...' && page !== this.currentPage || this.loadSessionStorage) {
	//     this.loading = true;
	//     this.currentPage = page;
	//     this.productService.searchProductsAndArticles(
	//       this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset, this.organizationId).subscribe(
	//       res => {
	//         this.searchTotal = res.headers.get('X-Total-Count');
	//         this.searchTotal !== '0' ? this.productsAndArticles = res.body : this.productsAndArticles = [];
	//         this.totalPages = Math.ceil(this.searchTotal / this.offset);
	//         this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
	//         this.productsAndArticles = res.body;
	//         this.dataSource = new MatTableDataSource<ProductOrArticle>(this.productsAndArticles);
	//         this.currentPage = page;
	//         this.storage.setItem('CurrentPage', page);
	//         this.generatePaginator();
	//       }, error => {
	//         this.alertService.clear();
	//         error.error.errors.forEach(err => {
	//           this.alertService.error(err);
	//         });
	//       }
	//     );
	//   }
	// }

	generatePaginator() {
		let hasFirstEllipsis = false;
		let hasSecondEllipsis = false;
		this.paginatorArray = [];
		if (this.searchTotal > this.offset) {
			if (this.totalPages < 6) {
				this.paginatorArray = this.allPages;
			} else {
				this.allPages.forEach((pageNumber) => {
					if (pageNumber === 1
						|| pageNumber === this.currentPage
						|| pageNumber === this.totalPages
						|| pageNumber === this.currentPage + 1
						|| pageNumber === this.currentPage - 1) {
						this.paginatorArray.push(pageNumber);
					} else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
						this.paginatorArray.push('...');
						hasFirstEllipsis = true;
					} else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
						this.paginatorArray.push('...');
						hasSecondEllipsis = true;
					}
				});
			}
		}

		this.loading = false;
	}


}

export interface ImportStatus {
	uniqueId: string;
	fileName: string;
	changedBy: string;
	readStatus: number;
	importStartDate: string;
	importEndDate: string;

}
