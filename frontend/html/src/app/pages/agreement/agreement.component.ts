import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../auth/auth.service';
import {AgreementService} from '../../services/agreement.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';
import {OrganizationService} from '../../services/organization.service';
import {Organization} from '../../models/organization/organization.model';
import {HelptextService} from '../../services/helptext.service';
import {AlertService} from '../../services/alert.service';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {StorageHelper as AgreementStorage} from '../../helpers/storage.helper';
import {HandleAgreementContactComponent} from './handle-agreement-contact/handle-agreement-contact.component';


@Component({
	selector: 'app-agreement',
	templateUrl: './agreement.component.html',
	styleUrls: ['./agreement.component.scss']
})
export class AgreementComponent implements OnInit {
	displayedColumns = [
		'avtalsnummer', 'avtalsnamn', 'godkänner pris', 'leverantör', 'kund', 'giltigt från', 'giltigt till', 'status'];

	storage = new AgreementStorage('Agreement');
	loadSessionStorage = false;

	loaded = false;
	loading = false;
	organizationId: number;
	query = '';
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	queryParams = '';
	searchTotal;
	offset = 25;
	totalPages;
	currentPage = 1;
	allPages;
	dataSource = null;
	agreements = [];
	isCustomer = false;
	helpTexts;
	helpTextsLoaded = false;

	// Filters
	filterFuture = false;
	filterCurrent = false;
	filterDiscontinued = false;
	include = 'ALL';

	// Permissions
	hasPermissionToViewAgreement: boolean;
	hasPermissionToCreateAgreement: boolean;

	isOwnOrganization = false;

	constructor(private route: ActivatedRoute,
							private authService: AuthService,
							private agreementService: AgreementService,
							private organizationService: OrganizationService,
							private helpTextService: HelptextService,
							private alertService: AlertService,
							private router: Router,
							public dialog: MatDialog) {
		this.route.params.subscribe(params => this.organizationId = params.organizationId);
		this.isOwnOrganization = Number(this.organizationId) === this.authService.getOrganizationId();
		this.hasPermissionToViewAgreement = this.hasPermissionToViewAgreement && this.isOwnOrganization;
		this.hasPermissionToCreateAgreement = this.hasPermissionToCreateAgreement && this.isOwnOrganization;

		// The user is considered done with their search after 500 ms without further input
		this.searchChanged.pipe(
			debounceTime(500),
			distinctUntilChanged())
			.subscribe(query => {
				this.query = encodeURIComponent(query);
				this.currentPage = 1;
				this.storage.setItem('CurrentPage', this.currentPage.toString());
				this.onQueryChange();
			}
			);
	}

	ngOnInit() {
		this.hasPermissionToViewAgreement = this.hasPermission('agreement:view_own');
		this.hasPermissionToCreateAgreement = this.hasPermission('agreement:create_own');

		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);

		if (this.storage.hasPageBeenVisisted()) {

			this.loadSessionStorage = true;

			this.organizationService.getOrganization(this.organizationId).subscribe(
				res => {

					this.getSessionStorage();

					if (res.organizationType === 'CUSTOMER') {
						this.isCustomer = true;
						this.onFilterChange();

					} else {
						this.onFilterChange();
					}
					this.loadSessionStorage = false;
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				});

		} else {

			this.organizationService.getOrganization(this.organizationId).subscribe(
				res => {
					if (res.organizationType === 'CUSTOMER') {
						this.isCustomer = true;
						this.searchAgreements('?include=ALL');
					} else {
						this.searchAgreements('?query=');
					}
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		}
	}

	setSessionStorage() {
		this.storage.setItem('Query', this.tmpQuery.toString());
		this.storage.setItem('Offset', this.offset.toString());
		this.storage.setItem('CurrentPage', this.currentPage.toString());
		this.storage.setItem('HelpTextsLoaded', this.helpTextsLoaded.toString());
		this.storage.setItem('FilterFuture', this.filterFuture.toString());
		this.storage.setItem('FilterCurrent', this.filterCurrent.toString());
		this.storage.setItem('FilterDiscontinued', this.filterDiscontinued.toString());
	}

	getSessionStorage() {
		this.offset = this.storage.getItemNumber('Offset');
		this.currentPage = this.storage.getItemNumber('CurrentPage');
		this.helpTextsLoaded = this.storage.getItemBoolean('HelpTextsLoaded');
		this.filterFuture = this.storage.getItemBoolean('FilterFuture');
		this.filterCurrent = this.storage.getItemBoolean('FilterCurrent');
		this.filterDiscontinued = this.storage.getItemBoolean('FilterDiscontinued');


		// Query needs to be at the end of the method.
		if (this.storage.getItemString('Query') !== 'null' || this.storage.getItemString('Query') !== '') {
			this.tmpQuery = this.storage.getItemString('Query');
			this.query = this.storage.getItemString('Query');
		}
	}

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
		this.storage.setItem('Query', this.query);
	}

	onQueryChange() {
		this.onFilterChange();
	}

	// More stream-lined way of changing the filter states. Needed since we need to change offset and
	// current page when they are changed (return to page 1) while also being able to keep the persistance functionality intact.
	changeFilter(text: string) {

		switch (text) {

		case 'FUTURE':
			this.toggleBoolean(this.filterFuture);
			break;

		case 'CURRENT':
			this.toggleBoolean(this.filterCurrent);
			break;

		case 'DISCONTINUED':
			this.toggleBoolean(this.filterDiscontinued);
			break;
		}

		this.offset = 25;
		this.currentPage = 1;

		this.onFilterChange();
	}

	onFilterChange() {

		this.queryParams = '';

		if (this.filterFuture) {
			this.queryParams += '&status=FUTURE';
		}
		if (this.filterCurrent) {
			this.queryParams += '&status=CURRENT';
		}
		if (this.filterDiscontinued) {
			this.queryParams += '&status=DISCONTINUED';
		}

		if (this.isCustomer && !this.loadSessionStorage) {
			this.queryParams += '&include=' + this.include;
		}

		if (this.loadSessionStorage) {

			this.initiateSessionStorage();

		} else {
			if (this.currentPage > 1) {
				this.searchAgreements('?query=' + this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset);
			} else {
				this.searchAgreements('?query=' + this.query + this.queryParams);
			}
		}

		this.setSessionStorage();
	}

	searchAgreements(query) {
		this.loading = true;
		this.agreementService.searchAgreements(this.organizationId, query).subscribe(
			res => {
				this.searchTotal = res.headers.get('X-Total-Count');
				this.agreements = this.searchTotal !== '0' ? res.body : [];
				this.dataSource = new MatTableDataSource<AgreementTable>(this.agreements);
				this.totalPages = Math.ceil(this.searchTotal / this.offset);
				this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
				this.loaded = true;
				this.loading = false;
				this.setSessionStorage();
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	exportAgreements() {
		window.location.href = this.agreementService.getExportAgreementsUrl(this.organizationId) + '?query=' + this.query + this.queryParams;
	}

	handleAgreementsContact(): void {

		this.dialog.open(HandleAgreementContactComponent, {
			width: '90%',
			height: '90%',
			data: {organizationId: this.organizationId}
		});
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	goToPage(page) {
		if (page !== '...' && page !== this.currentPage) {
			this.loading = true;
			this.currentPage = page;
			this.agreementService.searchAgreements(this.organizationId,
																																										'?query=' + this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset).subscribe(
				res => {
					this.searchTotal = res.headers.get('X-Total-Count');
					this.agreements = this.searchTotal !== '0' ? res.body : [];
					this.agreements = res.body;
					this.dataSource = new MatTableDataSource<AgreementTable>(this.agreements);
					this.storage.setItem('CurrentPage', page);
					this.currentPage = page;
					this.loaded = true;
					this.loading = false;
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		}
	}

	async handleRowClick(event: MouseEvent): Promise<void> {
		if (!this.hasPermissionToViewAgreement) {
			event.preventDefault();
			this.alertService.errorWithMessage('Du saknar behörighet att se detta avtal.');
		}
	}

	private initiateSessionStorage(): void {

		if (this.isCustomer) {

			if (this.query !== '') {
				this.searchAgreements('?query=' + this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset);
			} else {
				this.searchAgreements('?include=ALL' + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset);
			}

		} else {
			this.searchAgreements('?query=' + this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset);
		}
	}

	private toggleBoolean(bool: boolean): boolean {

		switch (bool) {
		case true:
			bool = false;
			break;
		case false:
			bool = true;
			break;
		}
		return bool;
	}

}


export interface AgreementTable {
	id: number;
	agreementNumber: string;
	agreementName: string;
	approvers: string;
	supplierOrganization: Organization;
	customerOrganization: Organization;
	validFrom: number;
	validTo: number;
	status: string;
}
