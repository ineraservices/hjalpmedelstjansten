import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ImportPricelistDialogComponent} from './import-pricelist-dialog.component';

describe('ImportPricelistDialogComponent', () => {
	let component: ImportPricelistDialogComponent;
	let fixture: ComponentFixture<ImportPricelistDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ImportPricelistDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ImportPricelistDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
