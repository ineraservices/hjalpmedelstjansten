import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AddPricelistRowDialogComponent} from './add-pricelist-row-dialog.component';

describe('AddPricelistRowDialogComponent', () => {
	let component: AddPricelistRowDialogComponent;
	let fixture: ComponentFixture<AddPricelistRowDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [AddPricelistRowDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(AddPricelistRowDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
