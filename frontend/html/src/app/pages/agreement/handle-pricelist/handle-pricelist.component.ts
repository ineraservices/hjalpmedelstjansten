import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { AuthService } from '../../../auth/auth.service';
import { GuaranteeUnit } from '../../../models/agreement/guarantee-unit.model';
import { Pricelist } from '../../../models/agreement/pricelist.model';
import { Pricelistrow } from '../../../models/agreement/pricelistrow.model';
import { PreventiveMaintenance } from '../../../models/preventive-maintenance.model';
import { Article } from '../../../models/product/article.model';
import { AgreementService } from '../../../services/agreement.service';
import { AlertService } from '../../../services/alert.service';
import { CommonService } from '../../../services/common.service';
import { HelptextService } from '../../../services/helptext.service';
import { OrganizationService } from '../../../services/organization.service';
import { ProductService } from '../../../services/product.service';
import { UserService } from '../../../services/user.service';
import { AttachMessageDialogComponent } from "../../product/attach-message-dialog/attach-message-dialog.component";
import { AddPricelistRowDialogComponent } from './add-pricelist-row-dialog/add-pricelist-row-dialog.component';
import { ImportPricelistDialogComponent } from './import-pricelist-dialog/import-pricelist-dialog.component';

@Component({
	selector: 'app-handle-pricelist',
	templateUrl: './handle-pricelist.component.html',
	styleUrls: ['./handle-pricelist.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class HandlePricelistComponent implements OnInit {

	displayedColumns = ['checkbox', 'typ', 'artnr', 'benämning', 'kategori', 'beställningsenhet',
																					'utgått', 'pris', 'gäller från', 'min best', 'levtid', 'garanti(antal)', '(enhet)',
																					'(gäller från)', '(villkor)', 'status', 'comment', 'inactivate'];
	displayedColumnsCustomer = ['activate', 'decline', 'typ', 'artnr', 'benämning', 'kategori', 'beställningsenhet',
																													'utgått', 'pris', 'gäller från', 'min best', 'levtid', 'garanti(antal)', '(enhet)', '(gäller från)',
																													'(villkor)', 'status', 'comment', 'inactivate'];
	loaded = false;
	loading = true;
	saving = false;
	editMode = false;
	organizationId: number;
	agreementId: number;
	pricelistId: number;
	isCustomer: boolean;
	isPricelistApprover: boolean;
	userId: number;
	selection = new SelectionModel<Pricelistrow>(true, []);
	tmpSelection = [];
	activationSelection = new SelectionModel<Pricelistrow>(true, []);
	tmpActivationSelection = [];
	declineSelection = new SelectionModel<Pricelistrow>(true, []);
	tmpDeclineSelection = [];
	units: Array<GuaranteeUnit>;
	query = '';
	approvementId: number;
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	queryParams = '';
	searchTotal;
	offset = 25;
	totalPages;
	currentPage = 1;
	paginatorArray = [];
	allPages;
	dataSource = null;
	pricelist: Pricelist;
	pricelistRows = [];
	pricelistValidFrom: Date;
	warrantyValidFrom: Array<PreventiveMaintenance>;
	helpTexts;
	flexDirection: string;
	agreementValidFrom;

	// Filters
	filterActive = false;
	filterPendingApproval = false;
	filterPendingApprovalShowMore = false;
	filterDeclined = false;
	filterCreated = false;
	filterChanged = false;
	filterInactive = false;
	filterPendingInactivation = false;
	filterShowRowsWithPrices = false;
	filterShowRowsWithoutPrices = false;
	filterH = false;
	filterT = false;
	filterR = false;
	filterTJ = false;
	filterI = false;
	filterArticlePublished = false;
	filterArticleDiscontinued = false;

	// Permissions
	hasPermissionToViewPricelist: boolean;
	hasPermissionToViewPricelistRow: boolean;
	hasPermissionToUpdatePricelistRow: boolean;
	hasPermissionToSendForCustomerApproval: boolean;
	hasPermissionToActivatePricelistRow: boolean;
	hasPermissionToDeclinePricelistRow: boolean;
	hasPermissionToApproveInactivatePricelistRow: boolean;
	hasPermissionToDeclineInactivatePricelistRow: boolean;
	hasPermissionToReopenInactivatedPricelistRow: boolean;
	hasPermissionToExportPricelistRows: boolean;
	hasPermissionToImportPricelistRows: boolean;
	isOwnOrganization = false;

	constructor(private route: ActivatedRoute,
							private authService: AuthService,
							private productService: ProductService,
							private agreementService: AgreementService,
							private organizationService: OrganizationService,
							private commonService: CommonService,
							private userService: UserService,
							private alertService: AlertService,
							private dialog: MatDialog,
							private router: Router,
							private helpTextService: HelptextService) {
		this.flexDirection = this.commonService.isUsingIE() ? 'row' : 'column';
		this.route.params.subscribe(params => {
			this.organizationId = params.organizationId;
			this.agreementId = params.agreementId;
			this.pricelistId = params.pricelistId;
			this.approvementId = params.approvementId;
		});
		this.isOwnOrganization = Number(this.organizationId) === this.authService.getOrganizationId();
		this.hasPermissionToViewPricelist = this.hasPermissionToViewPricelist && this.isOwnOrganization;
		this.hasPermissionToUpdatePricelistRow = this.hasPermissionToUpdatePricelistRow && this.isOwnOrganization;
		this.hasPermissionToSendForCustomerApproval = this.hasPermissionToSendForCustomerApproval && this.isOwnOrganization;

		// The user is considered done with their search after 500 ms without further input
		this.searchChanged.pipe(
			debounceTime(500),
			distinctUntilChanged())
			.subscribe(query => {
				this.query = encodeURIComponent(query);
				this.onQueryChange();
			}
			);
	}

	ngOnInit() {
		this.hasPermissionToViewPricelist = this.hasPermission('pricelist:view_own');
		this.hasPermissionToViewPricelistRow = this.hasPermission('pricelistrow:view_own');
		this.hasPermissionToUpdatePricelistRow = this.hasPermission('pricelistrow:update_own');
		this.hasPermissionToSendForCustomerApproval = this.hasPermission('pricelistrow:sendForCustomerApproval');
		this.hasPermissionToActivatePricelistRow = this.hasPermission('pricelistrow:activate');
		this.hasPermissionToDeclinePricelistRow = this.hasPermission('pricelistrow:decline');
		this.hasPermissionToApproveInactivatePricelistRow = this.hasPermission('pricelistrow:approve_inactivate');
		this.hasPermissionToDeclineInactivatePricelistRow = this.hasPermission('pricelistrow:decline_inactivate');
		this.hasPermissionToReopenInactivatedPricelistRow = this.hasPermission('pricelistrow:reopen');
		this.hasPermissionToExportPricelistRows = this.hasPermission('agreementpricelistexport:view');
		this.hasPermissionToImportPricelistRows = this.hasPermission('agreementpricelistimport:view');

		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
			}
		);
		this.organizationService.getOrganization(this.organizationId).subscribe(
			res => {
				this.isCustomer = res.organizationType === 'CUSTOMER';
			}
		);
		this.commonService.getPreventiveMaintenances().subscribe(
			res => {
				this.warrantyValidFrom = res;
				this.productService.getGuaranteeUnits().subscribe(
					units => {
						this.units = units;
						this.userService.getProfile().subscribe(
							profile => {
								this.userId = profile.id;
								this.agreementService.getPriceList(this.organizationId, this.agreementId, this.pricelistId).subscribe(
									pricelist => {
										this.pricelist = pricelist;
										this.isPricelistApprover = this.pricelist.agreement.customerPricelistApprovers?.some(x => x.id === this.userId);
										this.convertToDateString();
										if (this.approvementId) {
											this.getPricelistRowsByApprovementId(this.approvementId);
										} else {
											this.getPricelistRows('');
										}
									}, error => {
										this.alertService.clear();
										error.error.errors.forEach(err => {
											this.alertService.error(err);
										});
									}
								);
							}
						);
					}
				);
			}
		);

	}

	toggleEditMode() {
		this.editMode = !this.editMode;
		if (!this.editMode) {
			this.getPricelistRows(this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset);
		} else {
			// if (this.hasPermissionToActivatePricelistRow || this.hasPermissionToDeclinePricelistRow) {
			// 	this.displayedColumnsCustomer.splice(-1, 1);
			// } else {
			// 	this.displayedColumns.splice(-1, 1);
			// }
		}
	}

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
	}

	onQueryChange() {
		this.onFilterChange();
	}

	onShowMoreFilterChange() {
		if (this.filterPendingApprovalShowMore) {
			this.filterActive = false;
			this.filterDeclined = false;
			this.filterChanged = false;
			this.filterInactive = false;
			this.filterPendingInactivation = false;
		}
		this.onFilterChange();
	}

	onFilterChange() {
		this.queryParams = '';
		this.currentPage = 1;

		if (this.filterActive) {
			this.filterPendingApprovalShowMore = false;
			this.queryParams += '&status=ACTIVE';
		}

		if (this.filterPendingApproval) {
			this.queryParams += '&status=PENDING_APPROVAL';
		} else {
			this.filterPendingApprovalShowMore = false;
		}

		if (this.filterDeclined) {
			this.filterPendingApprovalShowMore = false;
			this.queryParams += '&status=DECLINED';
		}
		if (!this.isCustomer) {
			this.queryParams += this.filterCreated ? '&status=CREATED' : '';
		}
		if (this.filterChanged) {
			this.filterPendingApprovalShowMore = false;
			this.queryParams += '&status=CHANGED';
		}
		if (this.filterInactive) {
			this.filterPendingApprovalShowMore = false;
			this.queryParams += '&status=INACTIVE';
		}
		if (this.filterPendingInactivation) {
			this.filterPendingApprovalShowMore = false;
			this.queryParams += '&status=PENDING_INACTIVATION';
		}
		this.queryParams += this.filterPendingApprovalShowMore ? '&status=PENDING_APPROVAL_SHOW_MORE' : '';


		this.queryParams += this.filterShowRowsWithPrices ? this.queryParams += '&showRowsWithPrices=true' : this.queryParams += '&showRowsWithPrices=false';
		this.queryParams += this.filterShowRowsWithoutPrices ? this.queryParams += '&showRowsWithoutPrices=true' : this.queryParams += '&showRowsWithoutPrices=false';
		this.queryParams += this.filterH ? '&type=H' : '';
		this.queryParams += this.filterT ? '&type=T' : '';
		this.queryParams += this.filterR ? '&type=R' : '';
		this.queryParams += this.filterTJ ? '&type=Tj' : '';
		this.queryParams += this.filterI ? '&type=I' : '';
		this.queryParams += this.filterArticlePublished ? '&articleStatus=PUBLISHED' : '';
		this.queryParams += this.filterArticleDiscontinued ? '&articleStatus=DISCONTINUED' : '';
		this.getPricelistRows(this.query + this.queryParams);
	}

	exportPricelistRows() {
		window.location.href = this.agreementService.getExportPricelistRowsUrl(
			this.organizationId, this.agreementId, this.pricelistId) + '?query=' + this.query + this.queryParams;
	}

	exportPricelist() {
		window.location.href = this.agreementService.getExportPricelistUrl(
			this.organizationId, this.agreementId, this.pricelistId);
	}

	openImportPricelistDialog() {
		const dialogRef = this.dialog.open(ImportPricelistDialogComponent, {
			width: '80%',
			data: {
				'organizationId': this.organizationId,
				'agreementId': this.agreementId,
				'pricelistId': this.pricelistId
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			this.getPricelistRows(this.query + this.queryParams);
		});
	}

	getPricelistRowsByApprovementId(approvementId) {
		this.loading = true;
		this.tmpSelection = this.selection.selected;
		this.tmpActivationSelection = this.activationSelection.selected;
		this.tmpDeclineSelection = this.declineSelection.selected;
		this.agreementService.getPricelistRowsByApprovementId(this.organizationId, this.agreementId, this.pricelistId, approvementId).subscribe(
			res => {
				this.searchTotal = res.headers.get('X-Total-Count');
				this.pricelistRows = this.searchTotal !== '0' ? res.body : [];
				this.totalPages = Math.ceil(this.searchTotal / this.offset);
				this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
				// Extract units
				this.pricelistRows.forEach((row) => {
					if (row.warrantyQuantityUnit) {
						row.warrantyQuantityUnit = this.units.find(x => x.id === row.warrantyQuantityUnit.id);
					} else {
						row.warrantyQuantityUnit = new GuaranteeUnit();
					}
					if (row.warrantyValidFrom) {
						row.warrantyValidFrom = this.warrantyValidFrom.find(x => x.id === row.warrantyValidFrom.id);
					} else {
						row.warrantyValidFrom = new PreventiveMaintenance();
					}
					if (!row.warrantyTerms) {
						row.warrantyTerms = '';
					}

					if (row.article.status === 'DISCONTINUED' && this.isCustomer) {
						row.price = '';
					}

					row.validFrom = new Date(row.validFrom);
					this.tmpSelection.forEach(tmpRow => {
						if (tmpRow.id === row.id) {
							this.selection.deselect(this.selection.selected.find(x => x.id === row.id));
							this.selection.select(row);
						}
					});
					this.tmpActivationSelection.forEach(tmpRow => {
						if (tmpRow.id === row.id) {
							this.activationSelection.deselect(this.activationSelection.selected.find(x => x.id === row.id));
							this.activationSelection.select(row);
						}
					});
					this.tmpDeclineSelection.forEach(tmpRow => {
						if (tmpRow.id === row.id) {
							this.declineSelection.deselect(this.declineSelection.selected.find(x => x.id === row.id));
							this.declineSelection.select(row);
						}
					});
				});
				this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
				this.generatePaginator();
				this.loaded = true;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

	}

	getPricelistRows(query) {
		this.loading = true;
		this.tmpSelection = this.selection.selected;
		this.tmpActivationSelection = this.activationSelection.selected;
		this.tmpDeclineSelection = this.declineSelection.selected;
		this.agreementService.getPricelistRows(this.organizationId, this.agreementId, this.pricelistId, query).subscribe(
			res => {
				this.searchTotal = res.headers.get('X-Total-Count');
				this.pricelistRows = this.searchTotal !== '0' ? res.body : [];
				this.totalPages = Math.ceil(this.searchTotal / this.offset);
				this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
				// Extract units
				this.pricelistRows.forEach((row) => {
					if (row.warrantyQuantityUnit) {
						row.warrantyQuantityUnit = this.units.find(x => x.id === row.warrantyQuantityUnit.id);
					} else {
						row.warrantyQuantityUnit = new GuaranteeUnit();
					}
					if (row.warrantyValidFrom) {
						row.warrantyValidFrom = this.warrantyValidFrom.find(x => x.id === row.warrantyValidFrom.id);
					} else {
						row.warrantyValidFrom = new PreventiveMaintenance();
					}
					if (!row.warrantyTerms) {
						row.warrantyTerms = '';
					}

					if (row.article.status === 'DISCONTINUED' && this.isCustomer) {
						row.price = '';
					}

					if (row.validFrom != null) {
						row.validFrom = new Date(row.validFrom);
					} else {
						row.validFrom = '';
					}
					this.tmpSelection.forEach(tmpRow => {
						if (tmpRow.id === row.id) {
							this.selection.deselect(this.selection.selected.find(x => x.id === row.id));
							this.selection.select(row);
						}
					});
					this.tmpActivationSelection.forEach(tmpRow => {
						if (tmpRow.id === row.id) {
							this.activationSelection.deselect(this.activationSelection.selected.find(x => x.id === row.id));
							this.activationSelection.select(row);
						}
					});
					this.tmpDeclineSelection.forEach(tmpRow => {
						if (tmpRow.id === row.id) {
							this.declineSelection.deselect(this.declineSelection.selected.find(x => x.id === row.id));
							this.declineSelection.select(row);
						}
					});
				});
				this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
				this.generatePaginator();
				this.loaded = true;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	getCategoryCodeForCodelessArticle(article) {
		let code = article.category.code;

		if (code === null) {
			if (article.fitsToProducts !== null && article.fitsToProducts.length > 0) {
				code = article.fitsToProducts[0].category.code;
			}
		}

		if (code === null) {
			if (article.fitsToArticles !== null && article.fitsToArticles.length > 0) {
				if (article.fitsToArticles[0].basedOnProduct !== null) {

					code = article.fitsToArticles[0].basedOnProduct.category.code;
					// eslint-disable-next-line brace-style
				}
				// kan tänkas att detta ska med? PA 2020-10-05
				else {
					code = article.fitsToArticles[0].category.code;
				}
			}
		}
		if (code === null) {
			if (article.fitsToArticles !== null && article.fitsToArticles.length > 0) {
				if (article.fitsToArticles[0].fitsToProducts != null && article.fitsToArticles[0].fitsToProducts.length > 0) {
					code = article.fitsToArticles[0].fitsToProducts[0].category.code;
				}
			}
		}

		return code;
	}

	getDisplayedColumns() {
		return this.hasPermissionToActivatePricelistRow || this.hasPermissionToDeclinePricelistRow ? this.displayedColumnsCustomer : this.displayedColumns;
	}

	/** Whether a row is selected for activation and declined at the same time. */
	hasFaultySelection() {
		return this.activationSelection.selected.some(item => this.declineSelection.selected.indexOf(item) >= 0);
	}

	/** Whether the number of selected elements matches the total number of selectable rows. */
	isAllSelected() {
		let result = true;
		let numRows = 0;
		this.dataSource.data.forEach(item => {
			if (item.status === 'CREATED' || item.status === 'DECLINED' || item.status === 'CHANGED') {
				numRows++;
				const rowChecked = this.selection.selected.some(function (el) {
					return (el.id === item.id);
				});
				if (!rowChecked) {
					result = false;
				}
			}
		});
		return result;
	}

	/** Selects all selectable rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.dataSource.data.forEach(
				row => {
					if (row.status === 'CREATED' || row.status === 'DECLINED' || row.status === 'CHANGED') {
						this.selection.select(row);
					}
				});
		}
	}

	/** Whether the number of selected elements matches the total number of selectable rows. */
	isAllSelectedApproval(type) {
		let result = true;
		if (type === 'activation') {
			this.dataSource.data.forEach(item => {
				if (item.status === 'PENDING_APPROVAL' || item.status === 'PENDING_INACTIVATION') {
					const rowChecked = this.activationSelection.selected.some(function (el) {
						return (el.id === item.id);
					});
					if (!rowChecked) {
						result = false;
					}
				}
			});
		} else {
			this.dataSource.data.forEach(item => {
				if (item.status === 'PENDING_APPROVAL' || item.status === 'PENDING_INACTIVATION') {
					const rowChecked = this.declineSelection.selected.some(function (el) {
						return (el.id === item.id);
					});
					if (!rowChecked) {
						result = false;
					}
				}
			});
		}
		return result;
	}

	/** Selects all selectable rows if they are not all selected; otherwise clear selection. */
	masterToggleApprovals(type) {
		if (type === 'activation') {
			if (this.isAllSelectedApproval('activation')) {
				this.activationSelection.clear();
			} else {
				this.dataSource.data.forEach(
					row => {
						if (row.status === 'PENDING_APPROVAL' || row.status === 'PENDING_INACTIVATION') {
							this.activationSelection.select(row);
							this.declineSelection.deselect(row);
						}
					});
			}
		} else {
			if (this.isAllSelectedApproval('decline')) {
				this.declineSelection.clear();
			} else {
				this.dataSource.data.forEach(
					row => {
						if (row.status === 'PENDING_APPROVAL' || row.status === 'PENDING_INACTIVATION') {
							this.activationSelection.deselect(row);
							this.declineSelection.select(row);
						}
					});
			}
		}

	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	goToPage(page) {
		if (page !== '...' && page !== this.currentPage) {
			this.loading = true;
			this.tmpSelection = this.selection.selected;
			this.tmpActivationSelection = this.activationSelection.selected;
			this.tmpDeclineSelection = this.declineSelection.selected;
			this.currentPage = page;
			this.agreementService.getPricelistRows(
				this.organizationId, this.agreementId, this.pricelistId,
				this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset)
				.subscribe(
					res => {
						this.pricelistRows = res.body;
						// Extract units dates and selections
						this.pricelistRows.forEach((row) => {
							if (row.warrantyQuantityUnit) {
								row.warrantyQuantityUnit = this.units.find(x => x.id === row.warrantyQuantityUnit.id);
							} else {
								row.warrantyQuantityUnit = new GuaranteeUnit();
							}
							if (row.warrantyValidFrom) {
								row.warrantyValidFrom = this.warrantyValidFrom.find(x => x.id === row.warrantyValidFrom.id);
							} else {
								row.warrantyValidFrom = new PreventiveMaintenance();
							}
							if (!row.warrantyTerms) {
								row.warrantyTerms = '';
							}
							row.validFrom = new Date(row.validFrom);
							this.tmpSelection.forEach(tmpRow => {
								if (tmpRow.id === row.id) {
									this.selection.deselect(this.selection.selected.find(x => x.id === row.id));
									this.selection.select(row);
								}
							});
							this.tmpActivationSelection.forEach(tmpRow => {
								if (tmpRow.id === row.id) {
									this.activationSelection.deselect(this.activationSelection.selected.find(x => x.id === row.id));
									this.activationSelection.select(row);
								}
							});
							this.tmpDeclineSelection.forEach(tmpRow => {
								if (tmpRow.id === row.id) {
									this.declineSelection.deselect(this.declineSelection.selected.find(x => x.id === row.id));
									this.declineSelection.select(row);
								}
							});
						});
						this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
						this.currentPage = page;
						this.generatePaginator();
					}, error => {
						this.alertService.clear();
						error.error.errors.forEach(err => {
							this.alertService.error(err);
						});
					}
				);
		}
	}

	generatePaginator() {
		let hasFirstEllipsis = false;
		let hasSecondEllipsis = false;
		this.paginatorArray = [];
		if (this.searchTotal > this.offset) {
			if (this.totalPages < 6) {
				this.paginatorArray = this.allPages;
			} else {
				this.allPages.forEach((pageNumber) => {
					if (pageNumber === 1
						|| pageNumber === this.currentPage
						|| pageNumber === this.totalPages
						|| pageNumber === this.currentPage + 1
						|| pageNumber === this.currentPage - 1) {
						this.paginatorArray.push(pageNumber);
					} else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
						this.paginatorArray.push('...');
						hasFirstEllipsis = true;
					} else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
						this.paginatorArray.push('...');
						hasSecondEllipsis = true;
					}
				});
			}
		}
		this.loading = false;
	}

	convertToDateString() {
		this.pricelistValidFrom = new Date(this.pricelist.validFrom);
		this.agreementValidFrom = new Date(this.pricelist.agreement.validFrom);
	}

	getMillisecondsFromDate(date) {
		if (date) {
			return date.getTime();
		}
		return null;
	}

	openAddPricelistRowDialog() {
		const dialogRef = this.dialog.open(AddPricelistRowDialogComponent, {
			width: '80%',
			data: {
				'organizationId': this.organizationId,
				'agreementId': this.agreementId,
				'pricelistId': this.pricelistId
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.loading = true;
				const pricelistRows = new Array<Pricelistrow>();
				result.forEach(article => {
					const tmpArticle = new Article();
					const pricelistRow = new Pricelistrow();
					pricelistRow.status = 'CREATED';
					pricelistRow.leastOrderQuantity = 1;
					tmpArticle.id = article.id;
					pricelistRow.article = tmpArticle;
					pricelistRows.push(pricelistRow);
				});
				this.agreementService.createPricelistRows(this.organizationId, this.agreementId, this.pricelistId, pricelistRows).subscribe(
					res => {
						this.onFilterChange();
						this.alertService.success('Sparat');
					}, error => {
						this.loading = false;
						this.alertService.clear();
						error.error.errors.forEach(err => {
							this.alertService.error(err);
						});
					}
				);
			}
		});
	}

	updatePricelistRow(row) {
		// HJAL-1561 ska inte gå att sätta pris till NULL om raden en gång varit aktiv.
		if (row.status !== 'CREATED') {
			if (row.price == null) {
				this.alertService.clear();
				this.alertService.errorWithMessage('Pris måste anges, ändringen är inte sparad');
				return;
			}
		}
		if (row.status === 'ACTIVE_EDIT') {
			row.status = 'ACTIVE';
		}
		if (row.validFrom) {
			row.validFrom = this.getMillisecondsFromDate(row.validFrom);
		}

		if (row.warrantyValidFrom && !row.warrantyValidFrom.id) {
			row.warrantyValidFrom = null;
		}

		this.agreementService.updatePricelistRow(this.organizationId, this.agreementId, this.pricelistId, row.id, row).subscribe(
			res => {
				row.status = res.status;
				if (row.validFrom) {
					row.validFrom = new Date(row.validFrom);
				}
				this.alertService.clear();
				this.alertService.success('Sparat');
			}, error => {
				if (row.validFrom) {
					row.validFrom = new Date(row.validFrom);
				}
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	onCheckActivation(event, element) {
		this.activationSelection.toggle(element);
		if (event.checked) {
			if (this.declineSelection.isSelected(element)) {
				this.declineSelection.deselect(element);
			}
		}
	}

	onCheckDecline(event, element) {
		this.declineSelection.toggle(element);
		if (event.checked) {
			if (this.activationSelection.isSelected(element)) {
				this.activationSelection.deselect(element);
			}
		}
	}

	sendForApproval(comment) {
		this.saving = true;
		this.selection.selected.forEach((row) => {
			if (row.validFrom) {
				row.validFrom = this.getMillisecondsFromDate(row.validFrom);
			}
		});
		this.agreementService.sendPricelistRowsForApproval(
			this.organizationId, this.agreementId, this.pricelistId, this.selection.selected, comment).subscribe(
			res => {
				res.forEach((newRow) => {
					this.pricelistRows.forEach((oldRow) => {
						if (oldRow.id === newRow.id) {
							oldRow.status = newRow.status;
							if (newRow.validFrom) {
								oldRow.validFrom = new Date(newRow.validFrom);
							}
							this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
						}
					});
				});
				this.selection.clear();
				this.saving = false;
				this.alertService.success('Markerade rader har skickats för godkännande');
			}, error => {
				this.pricelistRows.forEach((row) => {
					if (row.validFrom) {
						row.validFrom = new Date(row.validFrom);
					}
				});
				this.saving = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	openAttachMessageDialog(sender, rows) {
		const dialogRef = this.dialog.open(AttachMessageDialogComponent, {
			width: '80%'
		});
		if (sender === 'supplier') {
			if (rows === 'marked') {
				dialogRef.afterClosed().subscribe(result => {
					if (result) {
						this.sendForApproval(result);
					}
				});
			}
			if (rows === 'all') {
				dialogRef.afterClosed().subscribe(result => {
					if (result) {
						this.sendAllForApproval(result);
					}
				});
			}
		}
		if (sender === 'customer') {
			if (rows === 'marked') {
				dialogRef.afterClosed().subscribe(result => {
					if (result) {
						this.handleApprovalRequests(result);
					}
				});
			}
			if (rows === 'all') {
				dialogRef.afterClosed().subscribe(result => {
					if (result) {
						this.approveAll(result);
					}
				});
			}
		}
	}

	sendAllForApproval(comment) {
		this.loading = true;
		setTimeout(() => {
			this.loading = false;
		}, 1000 * 60 * 2); // 2 minuter

		this.agreementService.sendAllPricelistRowsForApproval(this.organizationId, this.agreementId, this.pricelistId, comment).subscribe(
			res => {
				this.selection.clear();
				this.getPricelistRows(this.query + this.queryParams);
				this.alertService.success('Samtliga rader (max 10 000) i prislistan med status Ny och Ändrad har skickats för godkännande');
			}, error => {
				this.pricelistRows.forEach((row) => {
					if (row.validFrom) {
						row.validFrom = new Date(row.validFrom);
					}
				});
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
				this.loading = false;
			}
		);
	}

	editPricelistRow(row) {
		// Temporary status to handle editing of active rows
		row.status = 'ACTIVE_EDIT';
	}

	deletePricelistRow(row) {
		this.agreementService.deleteAgreementPriceListRow(this.organizationId, this.agreementId, this.pricelistId, row.id.toString()).subscribe(
			res => {
				this.alertService.clear();
				this.getPricelistRows(this.query + this.queryParams);
				this.alertService.success('Tog bort artikeln \"' + row.article.articleName + '\" från prislista på inköpsavtal');
			}, error => {
				if (row.validFrom) {
					row.validFrom = new Date(row.validFrom);
				}
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	requestInactivation(row) {
		const pricelistRows = new Array<Pricelistrow>();
		row.validFrom = this.getMillisecondsFromDate(row.validFrom);
		pricelistRows.push(row);
		this.agreementService.requestInactivationOfPricelistRow(
			this.organizationId, this.agreementId, this.pricelistId, pricelistRows).subscribe(
			res => {
				row.status = res[0].status;
				if (res[0].validFrom) {
					row.validFrom = new Date(res[0].validFrom);
				}
				this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
				this.alertService.success('Inaktivering har begärts');
			}, error => {
				this.alertService.clear();
				if (error.error.errors) {
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				} else {
					if (this.isCustomer) {
						this.alertService.error(new Error('Det gick inte att godkänna/avböja begäran om inaktivering, försök igen senare'));
					} else {
						this.alertService.error(new Error('Det gick inte att begära inaktivering av artikel på prislisterad, försök igen senare'));
					}
				}
				this.loading = false;
			}
		);
	}


	reopenInactivated(row) {
		const pricelistRows = new Array<Pricelistrow>();
		row.validFrom = this.getMillisecondsFromDate(row.validFrom);
		pricelistRows.push(row);
		this.agreementService.reopenInactivatedPricelistRow(
			this.organizationId, this.agreementId, this.pricelistId, pricelistRows).subscribe(
			res => {
				row.status = res[0].status;
				if (res[0].validFrom) {
					row.validFrom = new Date(res[0].validFrom);
				}
				this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
				this.loading = false;
			}
		);
	}

	handleApprovalRequests(comment) {
		const approveInactivations = new Array<Pricelistrow>();
		const declineInactivations = new Array<Pricelistrow>();
		const approveActivations = new Array<Pricelistrow>();
		const declineActivations = new Array<Pricelistrow>();
		if (this.activationSelection.selected.length) {
			this.activationSelection.selected.forEach(item => {
				item.validFrom = this.getMillisecondsFromDate(item.validFrom);
				if (item.status === 'PENDING_INACTIVATION') {
					approveInactivations.push(item);
				} else {
					approveActivations.push(item);
				}
			});
			if (approveActivations.length) {
				this.agreementService.activatePricelistRows(
					this.organizationId, this.agreementId, this.pricelistId, approveActivations, comment).subscribe(
					res => {
						res.forEach((newRow) => {
							this.pricelistRows.forEach((oldRow) => {
								if (oldRow.id === newRow.id) {
									oldRow.status = newRow.status;
									if (newRow.validFrom) {
										oldRow.validFrom = new Date(newRow.validFrom);
									}
								}
							});
						});
						this.activationSelection.clear();
						if (approveActivations.length === 1) {
							this.alertService.success(approveActivations.length + ' rad godkänd');
						} else {
							this.alertService.success(approveActivations.length + ' rader godkända');
						}
						this.getPricelistRows(this.query + this.queryParams);
						this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
						// this.activationSelection.clear();
					}, error => {
						this.alertService.clear();
						error.error.errors.forEach(err => {
							this.alertService.error(err);
						});
						this.loading = false;
					}
				);
			}
			if (approveInactivations.length) {
				this.agreementService.approveInactivationOfPricelistRow(
					this.organizationId, this.agreementId, this.pricelistId, approveInactivations, comment).subscribe(
					res => {
						if (approveInactivations.length === 1) {
							this.alertService.success(approveInactivations.length + ' rad inaktiverad');
						} else {
							this.alertService.success(approveInactivations.length + ' rader inaktiverade');
						}
						res.forEach((newRow) => {
							this.pricelistRows.forEach((oldRow) => {
								if (oldRow.id === newRow.id) {
									oldRow.status = newRow.status;
									if (newRow.validFrom) {
										oldRow.validFrom = new Date(newRow.validFrom);
									}
								}
							});
						});
						this.activationSelection.clear();
						this.getPricelistRows(this.query + this.queryParams);
						this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
						// this.activationSelection.clear();
					}, error => {
						this.alertService.clear();
						error.error.errors.forEach(err => {
							this.alertService.error(err);
						});
						this.loading = false;
					}
				);
			}
		}

		if (this.declineSelection.selected.length) {
			this.declineSelection.selected.forEach(item => {
				item.validFrom = this.getMillisecondsFromDate(item.validFrom);
				if (item.status === 'PENDING_INACTIVATION') {
					declineInactivations.push(item);
				} else {
					declineActivations.push(item);
				}
			});
			if (declineActivations.length) {
				this.agreementService.declinePricelistRows(
					this.organizationId, this.agreementId, this.pricelistId, declineActivations, comment).subscribe(
					res => {
						if (declineActivations.length === 1) {
							this.alertService.success(declineActivations.length + ' rad avböjd');
						} else {
							this.alertService.success(declineActivations.length + ' rader avböjda');
						}
						res.forEach((newRow) => {
							this.pricelistRows.forEach((oldRow) => {
								if (oldRow.id === newRow.id) {
									oldRow.status = newRow.status;
									if (newRow.validFrom) {
										oldRow.validFrom = new Date(newRow.validFrom);
									}
								}
							});
						});
						this.declineSelection.clear();
						this.getPricelistRows(this.query + this.queryParams);
						this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
						// this.declineSelection.clear();
					}, error => {
						this.alertService.clear();
						error.error.errors.forEach(err => {
							this.alertService.error(err);
						});
						this.loading = false;
					}
				);
			}
			if (declineInactivations.length) {
				this.agreementService.declineInactivationOfPricelistRow(
					this.organizationId, this.agreementId, this.pricelistId, declineInactivations, comment).subscribe(
					res => {
						if (declineInactivations.length === 1) {
							this.alertService.success('Inaktivering har avböjts för ' + declineInactivations.length + ' rad');
						} else {
							this.alertService.success('Inaktivering har avböjts för ' + declineInactivations.length + ' rader');
						}
						res.forEach((newRow) => {
							this.pricelistRows.forEach((oldRow) => {
								if (oldRow.id === newRow.id) {
									oldRow.status = newRow.status;
									if (newRow.validFrom) {
										oldRow.validFrom = new Date(newRow.validFrom);
									}
								}
							});
						});
						this.declineSelection.clear();
						this.getPricelistRows(this.query + this.queryParams);
						this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
						// this.declineSelection.clear();
					}, error => {
						this.alertService.clear();
						error.error.errors.forEach(err => {
							this.alertService.error(err);
						});
						this.loading = false;
					}
				);
			}
		}
	}

	approveAll(comment) {
		this.loading = true;

		setTimeout(() => {
			if (this.loading) {
				this.activationSelection.clear();
				this.declineSelection.clear();
				this.getPricelistRows(this.query + this.queryParams);
				this.alertService.success('Samtliga rader (max 10 000) i prislistan har godkänts');
			}
		}, 1000 * 60 * 5); // 5 minuter

		this.agreementService.activateAllPricelistRows(
			this.organizationId, this.agreementId, this.pricelistId, comment).subscribe(
			res => {
				this.activationSelection.clear();
				this.declineSelection.clear();
				if (res.length > 10000) {
					this.alertService.success('10 000 prislisterader har godkänts');
					this.getPricelistRows(this.query + this.queryParams);
				} else {
					this.alertService.success(res.length + ' prislisterader har godkänts');
					this.getPricelistRows(this.query + this.queryParams);
				}
			}, error => {
				this.pricelistRows.forEach((row) => {
					if (row.validFrom) {
						row.validFrom = new Date(row.validFrom);
					}
				});
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
				this.loading = false;
			}
		);
	}

	async routeTo(element: any): Promise<void> {
		if (!this.editMode) {
			await this.router.navigateByUrl("/organization/" + element.article.organizationId + "/article/" + element.article.id + "/handle?search=true");
		}
	}
}
