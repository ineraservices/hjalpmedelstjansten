import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HandlePricelistComponent} from './handle-pricelist.component';

describe('HandlePricelistComponent', () => {
	let component: HandlePricelistComponent;
	let fixture: ComponentFixture<HandlePricelistComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [HandlePricelistComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(HandlePricelistComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
