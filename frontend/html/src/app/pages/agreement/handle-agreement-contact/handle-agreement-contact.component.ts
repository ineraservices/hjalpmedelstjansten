import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatTabChangeEvent} from "@angular/material/tabs";
import {SelectionModel} from "@angular/cdk/collections";
import {ActivatedRoute} from '@angular/router';
import {Organization} from '../../../models/organization/organization.model';
import {OrganizationService} from '../../../services/organization.service';
import {AgreementService} from '../../../services/agreement.service';
import {AlertService} from '../../../services/alert.service';
import {HelptextService} from '../../../services/helptext.service';
import {AuthService} from "../../../auth/auth.service";
import {StorageHelper as AgreementStorage} from "../../../helpers/storage.helper";
import {Agreement} from "../../../models/agreement/agreement.model";
import {UserService} from "../../../services/user.service";
import {User} from "../../../models/user/user.model";

@Component({
	selector: 'app-handle-agreement-contact',
	templateUrl: './handle-agreement-contact.component.html',
	styleUrls: ['./handle-agreement-contact.component.scss']
})
export class HandleAgreementContactComponent implements OnInit {
	displayedColumns = ['välj', 'avtalsnummer', 'avtalsnamn', 'godkänner pris', 'leverantör', 'kund', 'giltigt från', 'giltigt till', 'status'];

	saving = false;
	organizationId: number;
	helpTexts;
	helpTextsLoaded = false;
	searchTotal;
	dataSource = null;
	loaded = false;
	loading = false;
	agreements = [];
	customerAgreementManagers = [];
	availableCustomerAgreementManagers = [];
	sortedManagersByBusinessLevel;
	selection = new SelectionModel<Agreement>(true, []);
	storage = new AgreementStorage('Agreement');
	loadSessionStorage = false;
	isOwnAgreement = false;
	addCustomerPricelistApprover = true;
	removeCustomerPricelistApprover = false;
	limit = 0;
	offset = 0;
	savedCount = [];
	failedCount = [];
	hasFailedUpdates = false;
	someUpdated = false;
	addUserSelection = new SelectionModel<User>(false, []);
	removeUserSelection = new SelectionModel<User>(false, []);
	allAgreements;
	sorted = false;

	// Permissions
	hasPermissionToCreateAgreement: boolean;
	hasPermissionToDeleteAgreement: boolean;
	hasPermissionToUpdateAgreement: boolean;
	hasPermissionToCreatePricelist: boolean;
	hasPermissionToUpdatePricelist: boolean;
	isOwnOrganization = false;

	constructor(private dialogRef: MatDialogRef<HandleAgreementContactComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private route: ActivatedRoute,
							private agreementService: AgreementService,
							private organizationService: OrganizationService,
							private alertService: AlertService,
							private authService: AuthService,
							private helpTextService: HelptextService,
							private userService: UserService,
	) {
		this.organizationId = data.organizationId;

		this.isOwnOrganization = Number(this.organizationId) === this.authService.getOrganizationId();
		this.hasPermissionToCreateAgreement = this.hasPermissionToCreateAgreement && this.isOwnOrganization;
		this.hasPermissionToDeleteAgreement = this.hasPermissionToDeleteAgreement && this.isOwnOrganization;
		this.hasPermissionToCreatePricelist = this.hasPermissionToCreatePricelist && this.isOwnOrganization;
		this.hasPermissionToUpdatePricelist = this.hasPermissionToUpdatePricelist && this.isOwnOrganization;


	}

	ngOnInit() {
		this.hasPermissionToCreateAgreement = this.hasPermission('agreement:create_own');
		this.hasPermissionToDeleteAgreement = this.hasPermission('agreement:delete_own');
		this.hasPermissionToUpdateAgreement = this.hasPermission('agreement:update_own');
		this.hasPermissionToCreatePricelist = this.hasPermission('pricelist:create_own');
		this.hasPermissionToUpdatePricelist = this.hasPermission('pricelist:update_own');

		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
				console.log("");
			}
		);

		// List of this customers pricelist-approvers managers for the select dropdown menus
		this.userService.getUserByRoles(this.organizationId, 'CustomerAgreementManager').subscribe(
			res => {
				// const users = [];
				this.customerAgreementManagers = [];
				this.availableCustomerAgreementManagers = [];
				res.forEach((user) => {
					this.userService.getUser(this.organizationId, user.id).subscribe(
						result => {
							this.customerAgreementManagers.push(result);
							if (result.userEngagements[0].businessLevels === null) {
								this.availableCustomerAgreementManagers.push(result);

							}
						}, error => {
							this.alertService.clear();
							error.error.errors.forEach(err => {
								this.alertService.error(err);
							});
						}
					);
				});
				this.loaded = true;
				this.loading = false;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

		this.agreementService.searchAgreements(this.organizationId, '?query=' + '&offset=' + this.offset + '&limit=' + this.limit).subscribe(
			res => {
				const filterSharedAgreementsFromOtherCustomer = res.body.filter(agreement => agreement.customerOrganization.id === this.organizationId);
				const filteredCurrentAgreements = filterSharedAgreementsFromOtherCustomer.filter(agreement => agreement.status === "CURRENT" || agreement.status === "FUTURE");
				this.searchTotal = res.headers.get('X-total-Count');
				this.agreements = this.searchTotal !== '0' ? filteredCurrentAgreements : [];
				this.allAgreements = filteredCurrentAgreements;
				this.dataSource = new MatTableDataSource<AgreementTable>(this.agreements);
				this.setSessionStorage();
				this.loaded = true;
				this.loading = false;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	getMillisecondsFromDate(date): number {
		if (date) {
			return date.getTime();
		}
		return null;
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	setSessionStorage() {
		this.storage.setItem('HelpTextsLoaded', this.helpTextsLoaded.toString());
	}

	/** Whether the number of selected elements matches the total number of selectable rows. */
	isAllSelected() {
		let result = true;
		let numRows = 0;
		this.agreements.forEach(item => {
			numRows++;
			const rowChecked = this.selection.selected.some(function (el) {
				return (el.id === item.id);
			});
			if (!rowChecked) {
				result = false;
			}
		});
		return result;
	}

	/** Selects all selectable rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.agreements.forEach(
				row => {
					this.selection.select(row);
				});
		}
	}

	onTabChange(event: MatTabChangeEvent) {

		this.refreshAgreementsTable();
		const tab = event.tab.textLabel;
		if (tab === "Lägg till") {

			this.addCustomerPricelistApprover = true;
			this.removeCustomerPricelistApprover = false;
		}
		if (tab === "Ta bort") {

			this.addCustomerPricelistApprover = false;
			this.removeCustomerPricelistApprover = true;

		}
	}

	refreshAgreementsTable() {
		this.selection.clear();
		this.someUpdated = false;
		this.dataSource = this.allAgreements;
		this.agreements = this.dataSource;
		this.sorted = false;
		this.addUserSelection.clear();
		this.removeUserSelection.clear();
		this.ngOnInit();
	}

	onSort(contact) {
		this.sorted = true;
		this.addUserSelection.clear();
		this.removeUserSelection.clear();

		this.selection.clear();

		if (this.addCustomerPricelistApprover) {

			if (contact === 'All') {
				// Sortera så att bara personer som KAN godkänna alla avtal finns med som valbara.
				const sortedManagersByBusinessLevel = [];
				this.customerAgreementManagers.forEach((person) => {
					if (person.userEngagements[0].businessLevels === null) {
						sortedManagersByBusinessLevel.push(person);
					}
				});
				this.availableCustomerAgreementManagers = sortedManagersByBusinessLevel;
				this.dataSource = this.allAgreements;
				this.agreements = this.dataSource;

			} else {
				const sortedAgreements = [];

				this.allAgreements.map((agreement) => {
					const isContact = (con) => agreement.customerPricelistApprovers.some(person => person.id === con.id);
					if (isContact(contact)) {
						return sortedAgreements.push(agreement);
					}
				});
				this.dataSource = sortedAgreements;
				this.agreements = this.dataSource;


				// Only pricelistapprovers with the same businesslevels as the contact sorted on are available to choose.
				const sortedContactsByBusinessLevel = [];
				if (contact.userEngagements[0].businessLevels !== null) {
					this.customerAgreementManagers.forEach((person) => {
						if (person.userEngagements[0].businessLevels !== null) {
							for (let i = 0; i < person.userEngagements[0].businessLevels.length; i++) {
								if (contact.userEngagements[0].businessLevels[i] === person.userEngagements[0].businessLevels[i]) {
									sortedContactsByBusinessLevel.push(person);
								}
							}
						} else if (person.userEngagements[0].businessLevels === null) {
							sortedContactsByBusinessLevel.push(person);
						}
					});
					this.availableCustomerAgreementManagers = sortedContactsByBusinessLevel;
				} else {
					const sortedManagersByBusinessLevel = [];
					this.customerAgreementManagers.forEach((person) => {
						if (person.userEngagements[0].businessLevels === null) {
							sortedManagersByBusinessLevel.push(person);
						}
					});
					this.availableCustomerAgreementManagers = sortedManagersByBusinessLevel;
				}
			}
		} else if (this.removeCustomerPricelistApprover) {
			if (contact === 'All') {
				this.dataSource = this.allAgreements;
				this.agreements = this.dataSource;
			} else {
				const sortedAgreements = [];
				this.allAgreements.map((agreement) => {
					const isContact = (con) => agreement.customerPricelistApprovers.some(person => person.id === con.id);
					if (isContact(contact)) {
						return sortedAgreements.push(agreement);
					}
				});
				this.dataSource = sortedAgreements;
				this.agreements = this.dataSource;
			}
		}
	}

	save() {
		// om en person redan finns som godkännare, men ej har rätt businesslevel kommer det ändå inte gå att uppdatera

		this.saving = true;
		this.failedCount = [];
		this.savedCount = [];
		this.selection.selected.map((item) => {
			this.agreementService.getAgreement(this.organizationId, item.id).subscribe(
				agreement => {
					if (Number(this.organizationId) === agreement.customerOrganization.id) {
						this.isOwnAgreement = true;
					}
					this.loaded = true;
					if (this.addCustomerPricelistApprover) {
						this.addUserSelection.selected.forEach((newApprover) => {
							const hasThisApprover = agreement.customerPricelistApprovers.some(element => element.id === newApprover.id);
							if (!hasThisApprover) {
								agreement.customerPricelistApprovers.push(newApprover);
							}
						});
					}
					if (this.removeCustomerPricelistApprover) {
						if (this.removeUserSelection.hasValue()) {
							this.removeUserSelection.selected.forEach((removableApprover) => {
								const indexOfThisApprover = agreement.customerPricelistApprovers.findIndex(element => element.id === removableApprover.id);
								if (indexOfThisApprover !== -1) {
									agreement.customerPricelistApprovers.splice(indexOfThisApprover, 1);
								}
							});

						}
					}
					this.agreementService.updateAgreement(this.organizationId, agreement.id, agreement).subscribe(
						res => {
							this.saving = false;
							this.savedCount.push('Sparat');
							this.someUpdated = true;
							if (this.failedCount.length === 0) {
								this.hasFailedUpdates = false;
							}
						}, error => {
							this.saving = false;
							const id = agreement.agreementNumber;
							this.failedCount.push({id: id, error: error.error.errors[0].message});
							this.hasFailedUpdates = true;
							this.alertService.clear();
						}
					);
				}
			);
		});
	}

}

export interface AgreementTable {
	id: number;
	agreementNumber: string;
	agreementName: string;
	customerPricelistApprover: string;
	supplierOrganization: Organization;
	customerOrganization: Organization;
	validFrom: number;
	validTo: number;
	status: string;
}
