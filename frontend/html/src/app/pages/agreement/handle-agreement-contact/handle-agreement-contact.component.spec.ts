import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HandleAgreementContactComponent} from "./handle-agreement-contact.component";

describe('HandleAgreementContactComponent', () => {
	let component: HandleAgreementContactComponent;
	let fixture: ComponentFixture<HandleAgreementContactComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [HandleAgreementContactComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(HandleAgreementContactComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
