import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ShareWithCustomerDialogComponent} from './share-with-customer-dialog.component';

describe('ShareWithCustomerDialogComponent', () => {
	let component: ShareWithCustomerDialogComponent;
	let fixture: ComponentFixture<ShareWithCustomerDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ShareWithCustomerDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ShareWithCustomerDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
