import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HandleAgreementComponent} from './handle-agreement.component';

describe('HandleAgreementComponent', () => {
	let component: HandleAgreementComponent;
	let fixture: ComponentFixture<HandleAgreementComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [HandleAgreementComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(HandleAgreementComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
