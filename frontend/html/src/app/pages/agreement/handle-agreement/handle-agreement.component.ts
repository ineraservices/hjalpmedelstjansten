import {Component, OnInit} from '@angular/core';
import {Organization} from '../../../models/organization/organization.model';
import {OrganizationService} from '../../../services/organization.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertService} from '../../../services/alert.service';
import {FormControl, Validators} from '@angular/forms';
import {AuthService} from '../../../auth/auth.service';
import {BusinessLevel} from '../../../models/organization/business-level.model';
import {Agreement} from '../../../models/agreement/agreement.model';
import {AgreementService} from '../../../services/agreement.service';
import {UserService} from '../../../services/user.service';
import {User} from '../../../models/user/user.model';
import {MatDialog} from '@angular/material/dialog';
import {AddUsersDialogComponent} from '../../general/add-users-dialog/add-users-dialog.component';
import {ShareWithCustomerDialogComponent} from './share-with-customer-dialog/share-with-customer-dialog.component';
import {ProductService} from '../../../services/product.service';
import {GuaranteeUnit} from '../../../models/agreement/guarantee-unit.model';
import {Pricelist} from '../../../models/agreement/pricelist.model';
import {Location} from '@angular/common';
import {PricelistDialogComponent} from './pricelist-dialog/pricelist-dialog.component';
import {PreventiveMaintenance} from '../../../models/preventive-maintenance.model';
import {CommonService} from '../../../services/common.service';
import {HelptextService} from '../../../services/helptext.service';
import {
	DeleteAgreementConfirmationDialogComponent
} from "./delete-agreement-confirmation-dialog/delete-agreement-confirmation-dialog.component";

@Component({
	selector: 'app-handle-agreement',
	templateUrl: './handle-agreement.component.html',
	styleUrls: ['./handle-agreement.component.scss']
})
export class HandleAgreementComponent implements OnInit {

	loaded = false;
	loading = false;
	saving = false;
	isOwnAgreement = false;
	editMode = false;
	tabIndex = 0;
	agreement: Agreement = new Agreement();
	profile: User;
	organization: Organization = new Organization();
	customer: string;
	suppliers: Array<Organization>;
	organizationId: number;
	agreementId: number;
	businessLevels: Array<BusinessLevel>;
	organizationBusinessLevels: Array<BusinessLevel>;
	selectedBusinessLevel: BusinessLevel = new BusinessLevel();
	supplierContacts: Array<User>;
	pricelists: Array<Pricelist>;
	helpTexts;
	helpTextsLoaded = false;
	flexDirection: string;

	// Warranty
	units: Array<GuaranteeUnit>;
	selectedWarrantyQuantityHUnit: GuaranteeUnit;
	selectedWarrantyQuantityTUnit: GuaranteeUnit;
	selectedWarrantyQuantityRUnit: GuaranteeUnit;
	selectedWarrantyQuantityIUnit: GuaranteeUnit;
	selectedWarrantyQuantityTJUnit: GuaranteeUnit;
	selectedWarrantyValidFromH: PreventiveMaintenance;
	selectedWarrantyValidFromT: PreventiveMaintenance;
	selectedWarrantyValidFromR: PreventiveMaintenance;
	selectedWarrantyValidFromI: PreventiveMaintenance;
	selectedWarrantyValidFromTJ: PreventiveMaintenance;
	preventiveMaintenanceValidity: Array<PreventiveMaintenance>;

	// Permissions
	hasPermissionToCreateAgreement: boolean;
	hasPermissionToDeleteAgreement: boolean;
	hasPermissionToCreatePricelist: boolean;
	hasPermissionToUpdatePricelist: boolean;
	isOwnOrganization = false;

	// Form controls
	numberFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	nameFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	supplierFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	validFromFormControl = new FormControl(null, {
		updateOn: 'blur'
	});
	validToFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	extensionFormControl = new FormControl(null, {
		updateOn: 'blur'
	});
	reminderFormControl = new FormControl(null, {
		updateOn: 'blur'
	});
	endDateFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	// Validation errors
	nameError;
	numberError;
	supplierError;
	validFromError;
	validToError;
	extensionOptionToError;
	sendReminderOnError;
	endDateError;
	pricelistApproverError;

	constructor(private organizationService: OrganizationService,
							private agreementService: AgreementService,
							private userService: UserService,
							private productService: ProductService,
							private commonService: CommonService,
							private route: ActivatedRoute,
							private router: Router,
							private alertService: AlertService,
							private authService: AuthService,
							private addUsersDialog: MatDialog,
							private shareWithCustomersDialog: MatDialog,
							private pricelistDialog: MatDialog,
							private location: Location,
							private helpTextService: HelptextService,
							private dialog: MatDialog) {
		this.flexDirection = this.commonService.isUsingIE() ? 'row' : 'column';
		this.route.params.subscribe(params => {
			this.organizationId = params.organizationId;
			this.agreementId = params.agreementId;
		});
		this.isOwnOrganization = Number(this.organizationId) === this.authService.getOrganizationId();
		this.hasPermissionToCreateAgreement = this.hasPermissionToCreateAgreement && this.isOwnOrganization;
		this.hasPermissionToDeleteAgreement = this.hasPermissionToDeleteAgreement && this.isOwnOrganization;
		this.hasPermissionToCreatePricelist = this.hasPermissionToCreatePricelist && this.isOwnOrganization;
		this.hasPermissionToUpdatePricelist = this.hasPermissionToUpdatePricelist && this.isOwnOrganization;
	}

	ngOnInit() {
		this.hasPermissionToCreateAgreement = this.hasPermission('agreement:create_own');
		this.hasPermissionToDeleteAgreement = this.hasPermission('agreement:delete_own');
		this.hasPermissionToCreatePricelist = this.hasPermission('pricelist:create_own');
		this.hasPermissionToUpdatePricelist = this.hasPermission('pricelist:update_own');

		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);
		this.onEditModeChange();
		this.productService.getGuaranteeUnits().subscribe(
			res => {
				this.units = res;
			}
		);
		this.commonService.getPreventiveMaintenances().subscribe(
			res => {
				this.preventiveMaintenanceValidity = res;
			}
		);
		this.userService.getProfile().subscribe(
			profile => {
				this.profile = profile;
				this.organizationService.getOrganization(this.organizationId).subscribe(
					res => {
						this.organization = res;

						if (!profile.userEngagements[0].businessLevels || !profile.userEngagements[0].businessLevels.length) {
							this.organizationService.getActiveBusinessLevels(this.organizationId).subscribe(
								levels => {
									this.organizationBusinessLevels = levels;
								}
							);
						} else {
							if (profile.userEngagements[0].businessLevels.length === 1) {
								this.agreement.customerBusinessLevel = profile.userEngagements[0].businessLevels[0];
							}
						}

						this.organizationService.searchValidOrganizations('&type=SUPPLIER&limit=0').subscribe(
							orgs => {
								this.suppliers = orgs.body;
								if (this.agreementId) {
									this.getAgreement();
								} else {
									// Set default values
									this.editMode = true;
									this.onEditModeChange();
									this.loaded = true;
								}
							}
						);
					}
				);
			}
		);
	}

	onTabSelection(event) {
		this.tabIndex = event.index;
	}

	toggleEditMode() {
		this.editMode = !this.editMode;
		this.onEditModeChange();
	}

	onEditModeChange() {
		if (!this.editMode) {
			this.numberFormControl.disable();
			this.nameFormControl.disable();
			this.supplierFormControl.disable();
			this.validFromFormControl.disable();
			this.validToFormControl.disable();
			this.extensionFormControl.disable();
			this.reminderFormControl.disable();
			this.endDateFormControl.disable();
		} else {
			if (this.hasPermissionToCreateAgreement) {
				if (!this.agreementId) {
					this.numberFormControl.enable();
					this.supplierFormControl.enable();
				}
				// HJAL-2117
				if (this.agreement.status !== 'DISCONTINUED') {
					this.validFromFormControl.enable();
				}
				this.nameFormControl.enable();
				this.validToFormControl.enable();
				this.extensionFormControl.enable();
				this.reminderFormControl.enable();
				this.endDateFormControl.enable();
			}
		}
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	getAgreement() {
		this.agreementService.getAgreement(this.organizationId, this.agreementId).subscribe(
			res => {
				if (Number(this.organizationId) === res.customerOrganization.id) {
					this.isOwnAgreement = true;
				}
				this.agreement = res;
				this.numberFormControl.setValue(res.agreementNumber);
				this.nameFormControl.setValue(res.agreementName);
				this.supplierFormControl.setValue(this.suppliers.find(e => e.id === this.agreement.supplierOrganization.id));
				this.onSupplierChange();
				this.convertToDateString();
				this.loaded = true;

				// Warranty
				if (this.agreement.warrantyQuantityHUnit) {
					this.selectedWarrantyQuantityHUnit = this.units.find(e => e.id === this.agreement.warrantyQuantityHUnit.id);
				} else {
					this.selectedWarrantyQuantityHUnit = null;
				}
				if (this.agreement.warrantyQuantityTUnit) {
					this.selectedWarrantyQuantityTUnit = this.units.find(e => e.id === this.agreement.warrantyQuantityTUnit.id);
				} else {
					this.selectedWarrantyQuantityTUnit = null;
				}
				if (this.agreement.warrantyQuantityRUnit) {
					this.selectedWarrantyQuantityRUnit = this.units.find(e => e.id === this.agreement.warrantyQuantityRUnit.id);
				} else {
					this.selectedWarrantyQuantityRUnit = null;
				}
				if (this.agreement.warrantyQuantityIUnit) {
					this.selectedWarrantyQuantityIUnit = this.units.find(e => e.id === this.agreement.warrantyQuantityIUnit.id);
				} else {
					this.selectedWarrantyQuantityIUnit = null;
				}
				if (this.agreement.warrantyQuantityTJUnit) {
					this.selectedWarrantyQuantityTJUnit = this.units.find(e => e.id === this.agreement.warrantyQuantityTJUnit.id);
				} else {
					this.selectedWarrantyQuantityTJUnit = null;
				}
				if (this.agreement.warrantyValidFromH) {
					this.selectedWarrantyValidFromH = this.preventiveMaintenanceValidity.find(e => e.id === this.agreement.warrantyValidFromH.id);
				} else {
					this.selectedWarrantyValidFromH = null;
				}
				if (this.agreement.warrantyValidFromT) {
					this.selectedWarrantyValidFromT = this.preventiveMaintenanceValidity.find(e => e.id === this.agreement.warrantyValidFromT.id);
				} else {
					this.selectedWarrantyValidFromT = null;
				}
				if (this.agreement.warrantyValidFromR) {
					this.selectedWarrantyValidFromR = this.preventiveMaintenanceValidity.find(e => e.id === this.agreement.warrantyValidFromR.id);
				} else {
					this.selectedWarrantyValidFromR = null;
				}
				if (this.agreement.warrantyValidFromI) {
					this.selectedWarrantyValidFromI = this.preventiveMaintenanceValidity.find(e => e.id === this.agreement.warrantyValidFromI.id);
				} else {
					this.selectedWarrantyValidFromI = null;
				}
				if (this.agreement.warrantyValidFromTJ) {
					this.selectedWarrantyValidFromTJ = this.preventiveMaintenanceValidity.find(e => e.id === this.agreement.warrantyValidFromTJ.id);
				} else {
					this.selectedWarrantyValidFromTJ = null;
				}
				this.getPricelists();
			}
		);
	}

	onSupplierChange() {
		if (this.supplierFormControl.value) {
			this.userService.getUserByRoles(this.supplierFormControl.value.id, 'SupplierAgreementManager').subscribe(
				res => {
					this.supplierContacts = res;
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		}
	}

	openAddUsersDialog() {
		const dialogRef = this.addUsersDialog.open(AddUsersDialogComponent, {
			width: '80%',
			data: {
				'organizationId': this.organizationId,
				'businessLevel': (this.selectedBusinessLevel && this.selectedBusinessLevel.id && this.selectedBusinessLevel.id !== 99) ?
					this.selectedBusinessLevel : null,
				'excludedUsers': this.agreement.customerPricelistApprovers && this.agreement.customerPricelistApprovers.length
					? this.agreement.customerPricelistApprovers : []
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				if (!this.agreement.customerPricelistApprovers) {
					this.agreement.customerPricelistApprovers = [];
				}
				result.forEach(item => {
					this.agreement.customerPricelistApprovers.push(item);
				});
			}
		});
	}

	openShareWithCustomersDialog() {
		const dialogRef = this.shareWithCustomersDialog.open(ShareWithCustomerDialogComponent, {
			width: '80%',
			data: {
				'customersToShareWith': this.agreement.sharedWithCustomers && this.agreement.sharedWithCustomers.length
					? this.agreement.sharedWithCustomers : [],
				'businessLevelsToShareWith': this.agreement.sharedWithCustomersBusinessLevels &&
				this.agreement.sharedWithCustomersBusinessLevels.length
					? this.agreement.sharedWithCustomersBusinessLevels : []
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				if (result.customers && result.customers.length && !this.agreement.sharedWithCustomers) {
					this.agreement.sharedWithCustomers = [];
				}
				if (result.businessLevels && result.businessLevels.length && !this.agreement.sharedWithCustomersBusinessLevels) {
					this.agreement.sharedWithCustomersBusinessLevels = [];
				}
				result.customers.forEach(customer => {
					if (!this.agreement.sharedWithCustomers.some(x => x.id === customer.id)) {
						this.agreement.sharedWithCustomers.push(customer);
					}
				});
				result.businessLevels.forEach(level => {
					if (!this.agreement.sharedWithCustomersBusinessLevels.some(x => x.id === level.id)) {
						this.agreement.sharedWithCustomersBusinessLevels.push(level);
					}
				});
			}
		});
	}

	openCreatePricelistDialog() {
		const dialogRef = this.pricelistDialog.open(PricelistDialogComponent, {
			width: '80%',
			data: {
				'organizationId': this.organizationId,
				'agreementId': this.agreementId,
				'agreementValidFrom': this.agreement.validFrom,
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.getPricelists();
			}
		});
	}

	openUpdatePricelistDialog(row) {
		const dialogRef = this.pricelistDialog.open(PricelistDialogComponent, {
			width: '80%',
			data: {
				'organizationId': this.organizationId,
				'agreementId': this.agreementId,
				'pricelistNumber': row.number,
				'validFrom': row.validFrom,
				'pricelistId': row.id
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.getPricelists();
			}
		});
	}

	removeUser(user) {
		this.agreement.customerPricelistApprovers = this.agreement.customerPricelistApprovers.filter(o => o.id !== user.id);
	}

	removeCustomer(customer) {
		this.agreement.sharedWithCustomers = this.agreement.sharedWithCustomers.filter(o => o.id !== customer.id);
	}

	removeBusinessLevel(level) {
		this.agreement.sharedWithCustomersBusinessLevels = this.agreement.sharedWithCustomersBusinessLevels.filter(
			o => o.id !== level.id);
	}

	convertToDateString() {
		this.validFromFormControl.setValue(new Date(this.agreement.validFrom));
		this.validToFormControl.setValue(new Date(this.agreement.validTo));
		if (this.agreement.extensionOptionTo) {
			this.extensionFormControl.setValue(new Date(this.agreement.extensionOptionTo));
		} else {
			this.extensionFormControl.setValue(null);
		}
		if (this.agreement.sendReminderOn) {
			this.reminderFormControl.setValue(new Date(this.agreement.sendReminderOn));
		} else {
			this.reminderFormControl.setValue(null);
		}
		if (this.agreement.endDate) {
			this.endDateFormControl.setValue(new Date(this.agreement.endDate));
		} else {
			this.endDateFormControl.setValue(null);
		}
	}

	getMillisecondsFromDate(date): number {
		if (date) {
			return date.getTime();
		}
		return null;
	}

	getPricelists() {
		this.loading = true;
		this.agreementService.getPriceLists(this.organizationId, this.agreementId).subscribe(
			pricelists => {
				this.loading = false;
				this.pricelists = pricelists;
			}, error => {
				this.loading = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	handleServerValidation(error): void {
		switch (error.field) {
		case 'agreementName': {
			this.nameError = error.message;
			this.nameFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'agreementNumber': {
			this.numberError = error.message;
			this.numberFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'supplierOrganization': {
			this.supplierError = error.message;
			this.supplierFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'validFrom': {
			this.validFromError = error.message;
			this.validFromFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'validTo': {
			this.validToError = error.message;
			this.validToFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'customerPricelistApprovers': {
			this.pricelistApproverError = error.message;
			break;
		}
		}
	}

	resetValidation() {
		this.nameError = null;
		this.nameFormControl.markAsDirty();
		this.numberError = null;
		this.numberFormControl.markAsDirty();
		this.supplierError = null;
		this.supplierFormControl.markAsDirty();
		this.validFromError = null;
		this.validFromFormControl.markAsDirty();
		this.validToError = null;
		this.validToFormControl.markAsDirty();
		this.extensionOptionToError = null;
		this.extensionFormControl.markAsDirty();
		this.sendReminderOnError = null;
		this.reminderFormControl.markAsDirty();
		this.endDateError = null;
		this.endDateFormControl.markAsDirty();
		this.pricelistApproverError = null;
	}

	saveAgreement() {
		this.saving = true;
		this.resetValidation();
		this.agreement.agreementNumber = this.numberFormControl.value;
		this.agreement.agreementName = this.nameFormControl.value;
		this.agreement.supplierOrganization = this.supplierFormControl.value;
		this.agreement.validFrom = this.getMillisecondsFromDate(this.validFromFormControl.value);
		this.agreement.validTo = this.getMillisecondsFromDate(this.validToFormControl.value);
		this.agreement.extensionOptionTo = this.getMillisecondsFromDate(this.extensionFormControl.value);
		this.agreement.sendReminderOn = this.getMillisecondsFromDate(this.reminderFormControl.value);
		this.agreement.endDate = this.getMillisecondsFromDate(this.endDateFormControl.value);
		// Warranty
		this.agreement.warrantyQuantityHUnit = this.selectedWarrantyQuantityHUnit;
		this.agreement.warrantyQuantityTUnit = this.selectedWarrantyQuantityTUnit;
		this.agreement.warrantyQuantityRUnit = this.selectedWarrantyQuantityRUnit;
		this.agreement.warrantyQuantityIUnit = this.selectedWarrantyQuantityIUnit;
		this.agreement.warrantyQuantityTJUnit = this.selectedWarrantyQuantityTJUnit;
		this.agreement.warrantyValidFromH = this.selectedWarrantyValidFromH;
		this.agreement.warrantyValidFromT = this.selectedWarrantyValidFromT;
		this.agreement.warrantyValidFromR = this.selectedWarrantyValidFromR;
		this.agreement.warrantyValidFromI = this.selectedWarrantyValidFromI;
		this.agreement.warrantyValidFromTJ = this.selectedWarrantyValidFromTJ;
		if (!this.agreement.warrantyTermsH) {
			this.agreement.warrantyTermsH = null;
		}
		if (!this.agreement.warrantyTermsT) {
			this.agreement.warrantyTermsT = null;
		}
		if (!this.agreement.warrantyTermsR) {
			this.agreement.warrantyTermsR = null;
		}
		if (!this.agreement.warrantyTermsI) {
			this.agreement.warrantyTermsI = null;
		}
		if (!this.agreement.warrantyTermsTJ) {
			this.agreement.warrantyTermsTJ = null;
		}

		if (this.selectedBusinessLevel && this.selectedBusinessLevel.id !== 99) {
			this.agreement.customerBusinessLevel = this.selectedBusinessLevel;
		} else {
			this.agreement.customerBusinessLevel = null;
		}
		if (this.agreementId) {
			this.updateAgreement();
		} else {
			this.createAgreement();
		}
	}

	updateAgreement() {
		this.agreementService.updateAgreement(this.organizationId, this.agreementId, this.agreement).subscribe(
			data => {
				this.agreement = data;
				this.toggleEditMode();
				this.saving = false;
				this.alertService.success('Sparat');
			}, error => {
				this.saving = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.handleServerValidation(err);
					this.alertService.error(err);
				});
			}
		);
	}

	createAgreement() {
		this.agreementService.createAgreement(this.organizationId, this.agreement).subscribe(
			data => {
				this.agreementId = data.id;
				this.agreement = data;
				if (Number(this.organizationId) === data.customerOrganization.id) {
					this.isOwnAgreement = true;
				}
				this.toggleEditMode();
				this.getPricelists();
				this.saving = false;
				this.alertService.success('Sparat');
			}, error => {
				this.saving = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.handleServerValidation(err);
					this.alertService.error(err);
				});
			}
		);
	}

	deleteAgreement() {
		this.agreementService.deleteAgreement(this.organizationId, this.agreementId).subscribe(
			() => {
				this.alertService.clear();
				this.router.navigate(['/organization/' + this.organizationId + '/agreement']);
				// this.alertService.success('Sparat');
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	openDeleteAgreementDialog() {
		// this.saving = true;
		const dialogRef = this.dialog.open(DeleteAgreementConfirmationDialogComponent, {
			width: '80%',
			data: {'organizationId': this.organizationId, 'agreementId': this.agreementId}
		});

		dialogRef.afterClosed().subscribe(
			res => {
				// this.saving = false;
				if (res) {
					this.alertService.clear();
					this.router.navigate(['/organization/' + this.organizationId + '/agreement']);
					this.alertService.success('Inköpsavtalet borttaget');
				}
			},
			error => {
				this.alertService.clear();
				// this.saving = false;
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	cancel() {
		this.getAgreement();
		this.toggleEditMode();
	}

	goBack() {
		this.location.back();
	}

}
