import {Component, Inject} from '@angular/core';
import {AgreementService} from "../../../../services/agreement.service";
import {AlertService} from "../../../../services/alert.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
	selector: 'app-delete-agreement-dialog',
	templateUrl: './delete-agreement-confirmation-dialog.component.html',
	styleUrls: ['./delete-agreement-confirmation-dialog.component.scss']
})
export class DeleteAgreementConfirmationDialogComponent {

	loading = false;

	constructor(public dialogRef: MatDialogRef<DeleteAgreementConfirmationDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private agreementService: AgreementService,
							private alertService: AlertService) {
	}

	done() {
		// eslint-disable-next-line no-debugger
		// debugger;
		console.log('done has been pressed');
		this.loading = true;


		this.agreementService.deleteAgreement(this.data.organizationId, this.data.agreementId).subscribe(
			() => {
				this.alertService.clear();
				this.alertService.success('Sparat');
				this.dialogRef.close('success');
				this.loading = false;

			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
					this.loading = false;
				});
			});

	}

}
