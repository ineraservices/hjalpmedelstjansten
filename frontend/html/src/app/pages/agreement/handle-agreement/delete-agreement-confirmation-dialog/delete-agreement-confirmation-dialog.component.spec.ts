import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DeleteAgreementConfirmationDialogComponent} from './delete-agreement-confirmation-dialog.component';

describe('DeleteAgreementConfirmationDialog', () => {
	let component: DeleteAgreementConfirmationDialogComponent;
	let fixture: ComponentFixture<DeleteAgreementConfirmationDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DeleteAgreementConfirmationDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DeleteAgreementConfirmationDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
