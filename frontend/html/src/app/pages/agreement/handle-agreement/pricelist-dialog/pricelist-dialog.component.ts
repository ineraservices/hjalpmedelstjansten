import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ActivatedRoute} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';
import {Pricelist} from '../../../../models/agreement/pricelist.model';
import {AgreementService} from '../../../../services/agreement.service';
import {AlertService} from '../../../../services/alert.service';
import {HelptextService} from '../../../../services/helptext.service';

@Component({
	selector: 'app-pricelist-dialog',
	templateUrl: './pricelist-dialog.component.html',
	styleUrls: ['./pricelist-dialog.component.scss']
})
export class PricelistDialogComponent implements OnInit {
	saving = false;
	organizationId: number;
	agreementId: number;
	pricelist: Pricelist = new Pricelist();
	pricelistId: number;
	helpTexts;
	helpTextsLoaded = false;
	agreementValidFrom;

	// Form controls
	numberFormControl = new FormControl(null, {
		updateOn: 'blur'
	});
	validFromFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	// Validation errors
	numberError;
	validFromError;

	constructor(public dialogRef: MatDialogRef<PricelistDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private route: ActivatedRoute,
							private agreementService: AgreementService,
							private alertService: AlertService,
							private helpTextService: HelptextService) {
		this.organizationId = data.organizationId;
		this.agreementId = data.agreementId;
		this.numberFormControl.setValue(data.pricelistNumber);
		this.validFromFormControl.setValue(new Date(data.validFrom));
		this.pricelistId = data.pricelistId;
	}

	ngOnInit() {
		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);

		this.agreementValidFrom = new Date(this.data.agreementValidFrom);
		console.log(this.agreementValidFrom);
	}

	getMillisecondsFromDate(date): number {
		if (date) {
			return date.getTime();
		}
		return null;
	}

	handleServerValidation(error): void {
		switch (error.field) {
		case 'number': {
			this.numberError = error.message;
			this.numberFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'validFrom': {
			this.validFromError = error.message;
			this.validFromFormControl.setErrors(Validators.pattern(''));
			break;
		}
		}
	}

	resetValidation() {
		this.validFromError = null;
		this.validFromFormControl.markAsDirty();
	}

	done() {
		this.saving = true;
		this.resetValidation();
		this.pricelist.validFrom = this.getMillisecondsFromDate(this.validFromFormControl.value);
		this.pricelist.number = this.numberFormControl.value;

		if (!this.pricelistId) {
			this.agreementService.createPricelist(this.organizationId, this.agreementId, this.pricelist).subscribe(
				res => {
					this.saving = false;
					this.alertService.success('Sparat');
					this.dialogRef.close('success');
				}, error => {
					this.saving = false;
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.handleServerValidation(err);
						this.alertService.error(err);
					});
				}
			);
		} else {
			this.agreementService.updatePricelist(this.organizationId, this.agreementId, this.pricelistId, this.pricelist).subscribe(
				res => {
					this.saving = false;
					this.alertService.success('Sparat');
					this.dialogRef.close('success');
				}, error => {
					this.saving = false;
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.handleServerValidation(err);
						this.alertService.error(err);
					});
				}
			);
		}
	}

}
