
import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AssortmentService} from "../../../../services/assortment.service";
import {AlertService} from "../../../../services/alert.service";


@Component({
  selector: 'app-delete-assortment-dialog',
  templateUrl: './delete-assortment-dialog.component.html',
  styleUrls: ['./delete-assortment-dialog.component.scss']
})
export class DeleteAssortmentDialogComponent implements OnInit {

  loading = false;

  constructor(public dialogRef: MatDialogRef<DeleteAssortmentDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private assortmentService: AssortmentService,
              private alertService: AlertService) {
  }

  ngOnInit() {
  }

  done() {
    console.log('done has been pressed');
    this.loading = true;
    this.assortmentService.removeAssortment( this.data.organizationId, this.data.assortmentId).subscribe(
    res => {
      this.dialogRef.close('success');
      this.loading = false;

    }, error => {
      this.alertService.clear();
      error.error.errors.forEach(err => {
        this.alertService.error(err);
        this.loading = false;
      });
    });

  }

}
