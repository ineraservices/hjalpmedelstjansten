
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAssortmentDialogComponent } from './delete-assortment-dialog.component';

describe('DeleteProductDialogComponent', () => {
  let component: DeleteAssortmentDialogComponent;
  let fixture: ComponentFixture<DeleteAssortmentDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteAssortmentDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAssortmentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
