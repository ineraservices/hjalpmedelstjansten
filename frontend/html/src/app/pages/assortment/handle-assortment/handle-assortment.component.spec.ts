import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HandleAssortmentComponent} from './handle-assortment.component';

describe('HandleAssortmentComponent', () => {
	let component: HandleAssortmentComponent;
	let fixture: ComponentFixture<HandleAssortmentComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [HandleAssortmentComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(HandleAssortmentComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
