import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { CommonService } from '../../../services/common.service';
import { Organization } from '../../../models/organization/organization.model';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../auth/auth.service';
import { HelptextService } from '../../../services/helptext.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { OrganizationService } from '../../../services/organization.service';
import { AlertService } from '../../../services/alert.service';
import { Assortment } from '../../../models/assortment/assortment.model';
import { AssortmentService } from '../../../services/assortment.service';
import { County } from '../../../models/assortment/county.model';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user/user.model';
import { SelectionModel } from '@angular/cdk/collections';
import { Municipality } from '../../../models/assortment/municipality.model';
import { Article } from '../../../models/product/article.model';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { DeleteAssortmentrowDialogComponent } from './delete-assortmentrow-dialog/delete-assortmentrow-dialog.component';
import { DeleteAssortmentDialogComponent } from './delete-assortment-dialog/delete-assortment-dialog.component';
import { StorageHelper as AssortmentStorage } from '../../../helpers/storage.helper';
import { CategoryDialogComponent } from '../../product/category-dialog/category-dialog.component';
import { ExternalCategoryGrouping } from '../../../models/product/external-category-grouping.model';
import { saveAs } from 'file-saver';

@Component({
	selector: 'app-handle-assortment',
	templateUrl: './handle-assortment.component.html',
	styleUrls: ['./handle-assortment.component.scss'],
})
export class HandleAssortmentComponent implements OnInit {
	displayedColumns = [
		'articleType',
		'number',
		'name',
		'basedOnProduct',
		'code',
		'organizationName',
		'status',
		'remove',
	];

	// Sets the storage index to be used.
	storage = new AssortmentStorage('Assortment');
	loadSessionStorage = false;
	loaded = false;
	loading = false;
	categoryLoaded = false;
	saving = false;
	editMode = false;
	isSuperAdmin = false;
	isCustomer = false;
	query = '';
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	queryParams = '';
	searchTotal;
	offset = 25;
	totalPages;
	currentPage = 1;
	allPages;
	dataSource = null;
	articles = [];
	isoCodes = new Map<number, string>();
	isoCodesSorted = new Map<number, string>();
	assortmentManagers: Array<User>;
	selectedAssortmentManagers: Array<User>;
	selectedAssortmentManagersString = '';
	selectedIsoCode: number;
	assortment: Assortment = new Assortment();
	counties: Array<County>;
	selectedCounty: County;
	municipalitySelection = new SelectionModel<Municipality>(true, []);
	categoryExceptionSelection = new SelectionModel<ExternalCategoryGrouping>(
		true,
		[],
	);
	allOtherCategoryExceptionSelection =
		new SelectionModel<ExternalCategoryGrouping>(true, []);
	organization: Organization = new Organization();
	organizationId: number;
	assortmentId: number;
	assortmentOrgName: string;
	helpTexts: any[] | Subscription;
	helpTextsLoaded = false;
	flexDirection: string;
	exceptionIsDirty = false;
	selectedLength: number;
	externalCategoriesForThisIsoCode: Array<ExternalCategoryGrouping>;
	allOtherExternalCategoriesForThisIsoCode: Array<ExternalCategoryGrouping>;

	// Sorting
	sortResults = true;
	sortOrder: String = 'asc';
	sortType: String = 'name';

	// Filters
	filterH = false;
	filterT = false;
	filterR = false;
	filterTj = false;
	filterI = false;
	filterPublished = false;
	filterDiscontinued = false;
	filterCategory = '';
	categoryResult = '';

	// Permissions
	hasPermissionToCreateAssortment: boolean;
	hasPermissionToDeleteAssortment: boolean;
	hasPermissionToUpdateAssortment: boolean;
	hasPermissionToAddAllArticles: boolean;
	hasPermissionToDeleteAllArticles: boolean;
	hasPermissionToAddArticles = false;
	hasPermissionToDeleteArticles = false;
	isOwnOrganization = false;

	// Form controls
	nameFormControl = new FormControl(null, {
		updateOn: 'blur',
	});

	validFromFormControl = new FormControl(null, {
		updateOn: 'blur',
	});
	validToFormControl = new FormControl(null, {
		updateOn: 'blur',
	});

	categoryFormControl = new FormControl(null, {
		updateOn: 'blur',
	});

	// Validation errors
	nameError;
	validFromError;
	validToError;
	categoryError;

	constructor(
		private organizationService: OrganizationService,
		private assortmentService: AssortmentService,
		private userService: UserService,
		private commonService: CommonService,
		private route: ActivatedRoute,
		private alertService: AlertService,
		private authService: AuthService,
		private dialog: MatDialog,
		private location: Location,
		private helpTextService: HelptextService,
		private router: Router,
	) {
		this.flexDirection = this.commonService.isUsingIE() ? 'row' : 'column';
		this.route.params.subscribe((params) => {
			this.organizationId = params.organizationId;
			this.assortmentId = params.assortmentId;
		});
		this.isOwnOrganization =
			Number(this.organizationId) === this.authService.getOrganizationId();
		this.hasPermissionToCreateAssortment =
			this.hasPermissionToCreateAssortment && this.isOwnOrganization;
		this.hasPermissionToDeleteAssortment =
			this.hasPermissionToDeleteAssortment && this.isOwnOrganization;
		this.hasPermissionToUpdateAssortment =
			this.hasPermissionToUpdateAssortment && this.isOwnOrganization;
		this.isSuperAdmin = this.authService.isSuperAdmin();

		// The user is considered done with their search after 500 ms without further input
		this.searchChanged
			.pipe(debounceTime(500), distinctUntilChanged())
			.subscribe((query) => {
				this.query = encodeURIComponent(query);
				this.onQueryChange();
				this.currentPage = 1;
			});
	}

	ngOnInit() {
		this.hasPermissionToCreateAssortment =
			this.hasPermission('assortment:create');
		this.hasPermissionToDeleteAssortment =
			this.hasPermission('assortment:delete');
		this.hasPermissionToUpdateAssortment =
			this.hasPermission('assortment:update');
		this.hasPermissionToAddAllArticles = this.hasPermission(
			'assortmentarticle:create_all',
		);
		this.hasPermissionToDeleteAllArticles = this.hasPermission(
			'assortmentarticle:delete_all',
		);

		this.helpTexts = this.helpTextService.getTexts().subscribe((texts) => {
			this.helpTexts = texts;
			this.helpTextsLoaded = true;
		});

		this.filterPublished = true;
		this.filterDiscontinued = true;

		if (this.storage.hasPageBeenVisisted()) {
			this.loadSessionStorage = true;
			this.onFilterChange();
			this.getSessionStorage();
			this.loadSessionStorage = false;
		} else {
			this.onFilterChange();
			this.sortResults = false;
			this.sortOrder = '';
			this.sortType = '';
		}

		this.assortmentService
			.getCountiesForOrganization(this.organizationId)
			.subscribe((counties) => {
				this.counties = counties;
				this.userService
					.getUserByRoles(
						this.organizationId,
						'CustomerAssignedAssortmentManager',
					)
					.subscribe({
						next: (managers) => {
							this.assortmentManagers = managers;

							this.onEditModeChange();
							if (this.assortmentId) {
								this.getAssortment();
							} else {
								// Set default values
								this.editMode = true;
								this.onEditModeChange();
								this.loaded = true;
							}
						},
						error: (error) => {
							this.alertService.clear();
							error.error.errors.forEach((err: any) => {
								this.alertService.error(err);
							});
						},
					});
			});
	}

	// Should be called "render()" and be built as a general method instead of what it is
	onFilterChange() {
		this.loading = true;
		this.queryParams = '';
		if (this.filterH) {
			this.queryParams += '&type=H';
		}
		if (this.filterT) {
			this.queryParams += '&type=T';
		}
		if (this.filterPublished) {
			this.queryParams += '&status=PUBLISHED';
		}
		if (this.filterDiscontinued) {
			this.queryParams += '&status=DISCONTINUED';
		}
		this.queryParams += '&includeOnlyTiso=true';
		if (this.sortResults) {
			this.queryParams += '&sortOrder=' + this.sortOrder;
			this.queryParams += '&sortType=' + this.sortType;
		}

		if (this.filterCategory) {
			this.queryParams += this.filterCategory;
		}

		if (this.currentPage > 1) {
			this.goToPage(this.currentPage);
		} else {
			this.getArticlesForAssortment(this.query + this.queryParams);
		}

		this.setSessionStorage();
		this.loading = false;
	}

	setSessionStorage() {
		this.storage.setItem('Query', this.tmpQuery.toString());
		this.storage.setItem('FilterH', this.filterH.toString());
		this.storage.setItem('FilterT', this.filterT.toString());
		this.storage.setItem('FilterPublished', this.filterPublished.toString());
		this.storage.setItem(
			'FilterDiscontinued',
			this.filterDiscontinued.toString(),
		);
		this.storage.setItem('FilterCategory', this.filterCategory);
		this.storage.setItem('Sort', this.sortResults.toString());
		this.storage.setItem('SortOrder', this.sortOrder.toString());
		this.storage.setItem('SortType', this.sortType.toString());
		// Same as CategoryFormControl, to avoid dupe storage i'm putting it here
		if (this.categoryResult !== 'undefined') {
			this.storage.setItem('CategoryResult', this.categoryResult);
		} else {
			this.storage.setItem('CategoryResult', '');
		}

		this.storage.setItem('CurrentPage', this.currentPage.toString());
		this.storage.setItem('HelpTextsLoaded', this.helpTextsLoaded.toString());
	}

	getSessionStorage() {
		this.filterH = this.storage.getItemBoolean('FilterH');
		this.filterT = this.storage.getItemBoolean('FilterT');
		this.filterR = this.storage.getItemBoolean('FilterR');
		this.filterTj = this.storage.getItemBoolean('FilterTj');
		this.filterI = this.storage.getItemBoolean('FilterI');
		this.currentPage = this.storage.getItemNumber('CurrentPage');
		this.filterPublished = this.storage.getItemBoolean('FilterPublished');
		this.filterDiscontinued = this.storage.getItemBoolean('FilterDiscontinued');
		this.categoryFormControl.setValue(
			this.storage.getItemString('CategoryResult'),
		);
		this.filterCategory = this.storage.getItemString('FilterCategory');
		this.categoryResult = this.storage.getItemString('CategoryResult');
		this.helpTextsLoaded = this.storage.getItemBoolean('HelpTextsLoaded');
		this.sortResults = this.storage.getItemBoolean('Sort');
		this.sortOrder = this.storage.getItemString('SortOrder');
		this.sortType = this.storage.getItemString('SortType');

		// Query needs to be at the end of the method.
		if (
			this.storage.getItemString('Query') !== 'null' ||
			this.storage.getItemString('Query') !== ''
		) {
			this.tmpQuery = this.storage.getItemString('Query');
			this.query = this.storage.getItemString('Query');
		}
	}

	toggleEditMode() {
		this.editMode = !this.editMode;
		this.onEditModeChange();
	}

	exportSearchResults() {
		const query = '?query=' + this.query + this.queryParams;
		this.assortmentService
			.exportArticlesForAssortment(
				this.organizationId,
				this.assortmentId,
				query,
			)
			.subscribe({
				next: (data) => {
					saveAs(
						data.body,
						data.headers
							.get('Content-Disposition')
							.split(';')[1]
							.trim()
							.split('=')[1],
					);
				},
				error: (error) => {
					this.alertService.clear();
					error.error.errors.forEach((err) => {
						this.alertService.error(err);
					});
				},
			});
	}

	onEditModeChange() {
		if (!this.editMode) {
			this.nameFormControl.disable();
			this.validFromFormControl.disable();
			this.validToFormControl.disable();
			this.selectedAssortmentManagersString = '';
			if (this.assortment.managers && this.assortment.managers.length) {
				this.assortment.managers.forEach((manager) => {
					if (this.selectedAssortmentManagersString) {
						this.selectedAssortmentManagersString += '\n';
					}
					this.selectedAssortmentManagersString +=
						manager.firstName + ' ' + manager.lastName;
				});
			}
		} else if (!this.isSuperAdmin) {
			this.nameFormControl.enable();
			this.validFromFormControl.enable();
			this.validToFormControl.enable();
		}
	}

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
	}

	onQueryChange() {
		this.sortResults = false;
		this.currentPage = 1;
		this.getArticlesForAssortment('?query=' + this.query + this.queryParams);
		// Checks to see if this is a load from sessionStorage, if it is set sortType to the previous session.
		if (this.loadSessionStorage) {
			this.sortType = this.storage.getItemString('bigSearchSortType');
			this.sortOrder = this.storage.getItemString('bigSearchSortOrder');
		} else {
			this.sortType = '';
			this.sortOrder = '';
		}
		this.onFilterChange();
	}

	sortData(matSort: MatSort) {
		this.sortResults = true;
		switch (this.sortOrder) {
		case 'asc':
			this.sortOrder = 'desc';
			break;
		case 'desc':
			this.sortOrder = 'asc';
			break;
		default:
			this.sortOrder = 'asc';
			break;
		}
		this.sortType = matSort.active;
		this.onFilterChange();
	}

	goToPage(page) {
		if (page !== '...') {
			this.loading = true;
			this.currentPage = page;
			this.assortmentService
				.searchArticlesForAssortment(
					this.organizationId,
					this.assortmentId,
					this.query +
					this.queryParams +
					'&includeOnlyTiso=true&&offset=' +
					(this.currentPage - 1) * this.offset,
				)
				.subscribe({
					next: (res) => {
						this.searchTotal = res.headers.get('X-Total-Count');
						this.articles = this.searchTotal !== '0' ? res.body : [];
						this.articles = res.body;
						this.dataSource = new MatTableDataSource<Assortment>(this.articles);
						this.currentPage = page;
						this.loaded = true;
						this.loading = false;
					},
					error: (error) => {
						this.alertService.clear();
						error.error.errors.forEach((err) => {
							this.alertService.error(err);
						});
					},
				});
		}
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	getAssortment() {
		this.resetExceptions();

		this.assortmentService
			.getAssortment(this.organizationId, this.assortmentId)
			.subscribe({
				next: (res) => {
					this.assortmentId = res.id;
					this.assortment = res;
					this.assortmentOrgName = res.customer.organizationName;

					this.nameFormControl.setValue(res.name);
					if (res.managers) {
						const userId = this.authService.getUserId();
						res.managers.forEach((manager) => {
							if (manager.id === userId) {
								this.hasPermissionToAddArticles = true;
								this.hasPermissionToDeleteArticles = true;
							}
						});
						this.selectedAssortmentManagers = this.assortmentManagers.filter(
							function (o) {
								return res.managers.some(function (o2) {
									return o.id === o2.id;
								});
							},
						);
						this.selectedAssortmentManagersString = '';
						this.assortment.managers.forEach((manager) => {
							if (this.selectedAssortmentManagersString) {
								this.selectedAssortmentManagersString += '\n';
							}
							this.selectedAssortmentManagersString +=
								manager.firstName + ' ' + manager.lastName;
						});
					}
					if (this.assortment.county && this.assortment.county) {
						this.selectedCounty = this.counties.find(
							(x) => x.id === this.assortment.county.id,
						);
						if (
							this.selectedCounty.municipalities &&
							this.selectedCounty.municipalities.length
						) {
							this.selectedCounty.municipalities.forEach((municipality) => {
								this.assortment.municipalities.forEach(
									(selectedMunicipality) => {
										if (municipality.id === selectedMunicipality.id) {
											this.municipalitySelection.select(municipality);
										}
									},
								);
							});
						}
					}
					this.convertToDateString();
					this.getArticlesForAssortment(this.query + this.queryParams);
					this.loaded = true;
				},
				error: (error) => {
					this.alertService.clear();
					error.error.errors.forEach((err: any) => {
						this.alertService.error(err);
					});
				},
			});
	}

	getArticlesForAssortment(query: string) {
		this.loading = true;
		this.assortmentService
			.searchArticlesForAssortment(
				this.organizationId,
				this.assortmentId,
				query,
			)
			.subscribe({
				next: (res) => {
					this.searchTotal = res.headers.get('X-Total-Count');
					this.articles = this.searchTotal !== '0' ? res.body : [];

					this.articles.forEach((article) => {
						if (!this.isoCodes.has(article.category.id)) {
							this.isoCodes.set(
								article.category.id,
								article.category.code + ' ' + article.category.name,
							);
						}
					});

					this.isoCodesSorted = new Map([...this.isoCodes.entries()].sort());

					this.dataSource = new MatTableDataSource<Article>(this.articles);
					this.totalPages = Math.ceil(this.searchTotal / this.offset);
					this.allPages = Array(this.totalPages)
						.fill(1)
						.map((x, i) => i + 1);
					this.loaded = true;
					this.loading = false;
				},
				error: (error) => {
					this.loading = false;
					this.alertService.clear();
					error.error.errors.forEach((err) => {
						this.alertService.error(err);
					});
				},
			});
	}

	openDeleteAssortmentRowDialog(articleId) {
		const dialogRef = this.dialog.open(DeleteAssortmentrowDialogComponent, {
			width: '80%',
			data: {
				organizationId: this.organizationId,
				assortmentId: this.assortmentId,
				articleId: articleId,
			},
		});
		dialogRef.afterClosed().subscribe(
			(res) => {
				if (res) {
					this.alertService.clear();
					this.getArticlesForAssortment(this.query + this.queryParams);
					this.alertService.success('Artikeln borttagen från utbudet');
				}
			},
			(error) => {
				this.alertService.clear();
				error.error.errors.forEach((err) => {
					this.alertService.error(err);
				});
			},
		);
	}

	openDeleteAssortmentDialog() {
		const dialogRef = this.dialog.open(DeleteAssortmentDialogComponent, {
			width: '80%',
			data: {
				organizationId: this.organizationId,
				assortmentId: this.assortmentId,
			},
		});
		dialogRef.afterClosed().subscribe(
			(res) => {
				if (res) {
					this.alertService.clear();
					// go back to list of assortments
					this.router.navigate([
						'/organization/' + this.organizationId + '/assortment/',
					]);
					this.alertService.success('Utbudet borttaget');
				}
			},
			(error) => {
				this.alertService.clear();
				error.error.errors.forEach((err) => {
					this.alertService.error(err);
				});
			},
		);
	}

	convertToDateString() {
		this.validFromFormControl.setValue(new Date(this.assortment.validFrom));
		if (this.assortment.validTo) {
			this.validToFormControl.setValue(new Date(this.assortment.validTo));
		} else {
			this.validToFormControl.setValue(null);
		}
	}

	getMillisecondsFromDate(date): number {
		if (date) {
			return date.getTime();
		}
		return null;
	}

	handleServerValidation(error): void {
		switch (error.field) {
		case 'name': {
			this.nameError = error.message;
			this.nameFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'validFrom': {
			this.validFromError = error.message;
			this.validFromFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'validTo': {
			this.validToError = error.message;
			this.validToFormControl.setErrors(Validators.pattern(''));
			break;
		}
		}
	}

	resetValidation() {
		this.nameError = null;
		this.nameFormControl.markAsDirty();
		this.validFromError = null;
		this.validFromFormControl.markAsDirty();
		this.validToError = null;
		this.validToFormControl.markAsDirty();
	}

	onCountySelection() {
		this.municipalitySelection.clear();
	}

	saveAssortment() {
		this.saving = true;
		this.resetValidation();

		this.assortment.name =
			this.nameFormControl.value?.trim() || this.nameFormControl.value;
		this.assortment.validFrom = this.getMillisecondsFromDate(
			this.validFromFormControl.value,
		);
		this.assortment.validTo = this.getMillisecondsFromDate(
			this.validToFormControl.value,
		);
		this.assortment.managers = this.selectedAssortmentManagers;

		this.assortment.externalCategoriesGroupings = [
			...this.mapCategorySelections(this.categoryExceptionSelection.selected),
			...this.mapCategorySelections(
				this.allOtherCategoryExceptionSelection.selected,
			),
		];

		this.assortment.county = this.selectedCounty;
		this.assortment.municipalities = this.municipalitySelection.selected;

		if (this.assortmentId) {
			this.updateAssortment();
		} else {
			this.createAssortment();
		}
	}

	updateAssortment() {
		this.assortmentService
			.updateAssortment(this.organizationId, this.assortmentId, this.assortment)
			.subscribe({
				next: (res) => {
					this.assortment = res;
					this.toggleEditMode();
					this.saving = false;
					this.alertService.success('Sparat');
					this.getAssortment();
				},
				error: (error) => {
					this.saving = false;
					this.alertService.clear();
					error.error.errors.forEach((err) => {
						this.handleServerValidation(err);
						this.alertService.error(err);
					});
				},
			});
	}

	createAssortment() {
		this.assortmentService
			.createAssortment(this.organizationId, this.assortment)
			.subscribe({
				next: (res) => {
					this.assortmentId = res.id;
					this.assortment = res;
					this.toggleEditMode();
					this.saving = false;
					this.alertService.success('Sparat');
					this.getAssortment();
				},
				error: (error) => {
					this.saving = false;
					this.alertService.clear();
					error.error.errors.forEach((err) => {
						this.handleServerValidation(err);
						this.alertService.error(err);
					});
				},
			});
	}

	openCategoryDialog() {
		const dialogRef = this.dialog.open(CategoryDialogComponent, {
			width: '90%',
		});

		dialogRef.afterClosed().subscribe((result) => {
			if (result) {
				this.sortResults = false;
				this.sortOrder = '';
				this.sortType = '';
				this.categoryResult = result.code + ' ' + result.name;

				if (result.code) {
					this.categoryFormControl.setValue(result.code + ' ' + result.name);
				} else {
					this.categoryFormControl.setValue(result.name);
				}
				this.filterCategory = '&category=' + result.id;
				this.onFilterChange();
			}
		});
	}

	clearCategory() {
		this.categoryFormControl.setValue('');
		this.filterCategory = '';
		this.categoryResult = '';
		this.onFilterChange();
	}

	cancel() {
		this.getAssortment();
		this.toggleEditMode();
	}

	goBack() {
		this.location.back();
	}

	rearrange(unordered): Array<any> {
		return unordered.map(([id, description, displayOrder, name, category]) => ({
			id,
			description,
			displayOrder,
			name,
			category,
		}));
	}

	onIsoCodeSelection() {
		this.categoryLoaded = false;

		this.categoryExceptionSelection.clear();
		this.allOtherCategoryExceptionSelection.clear();

		this.assortmentService
			.getAssortmentExternalCategories(
				this.organizationId,
				this.assortmentId,
				this.selectedIsoCode,
			)
			.subscribe({
				next: (result) => {
					this.externalCategoriesForThisIsoCode = this.rearrange(result);
					if (
						this.assortment.externalCategoriesGroupings &&
						this.externalCategoriesForThisIsoCode
					) {
						this.assortment.externalCategoriesGroupings.forEach((ecg) => {
							this.externalCategoriesForThisIsoCode.forEach((ec) => {
								if (ec.id === ecg.id) {
									this.categoryExceptionSelection.select(ec);
									this.selectedLength =
										this.categoryExceptionSelection.selected.length;
								}
							});
						});
					}
					this.loading = false;
				},
				error: (error) => {
					this.loading = false;
					this.alertService.clear();
					error.error.errors.forEach((err: any) => {
						this.alertService.error(err);
					});
				},
			});

		this.assortmentService
			.getAssortmentAllOtherExternalCategories(
				this.organizationId,
				this.assortmentId,
				this.selectedIsoCode,
			)
			.subscribe({
				next: (result) => {
					this.allOtherExternalCategoriesForThisIsoCode =
						this.rearrange(result);
					if (
						this.assortment.externalCategoriesGroupings &&
						this.allOtherExternalCategoriesForThisIsoCode
					) {
						this.assortment.externalCategoriesGroupings.forEach((ecg) => {
							this.allOtherExternalCategoriesForThisIsoCode.forEach((ec) => {
								if (ec.id === ecg.id) {
									this.allOtherCategoryExceptionSelection.select(ec);
								}
							});
						});
					}
					this.loading = false;
				},
				error: (error) => {
					this.loading = false;
					this.alertService.clear();
					error.error.errors.forEach((err: any) => {
						this.alertService.error(err);
					});
				},
			});
	}

	resetExceptions() {
		this.exceptionIsDirty = false;
		this.onIsoCodeSelection();
	}

	exceptionChanged(externalCategory: ExternalCategoryGrouping) {
		this.categoryExceptionSelection.toggle(externalCategory);
		const a = this.categoryExceptionSelection.selected.length;
		const b = this.selectedLength ? this.selectedLength : 0;

		if (a !== b) {
			this.exceptionIsDirty = true;
		}
		if (a === b) {
			let match = true;
			this.categoryExceptionSelection.selected.forEach((aces) => {
				if (!match) {
					return;
				}
				let exists = false;
				this.assortment.externalCategoriesGroupings.forEach((ecg) => {
					if (exists) {
						return;
					}
					if (aces.id === ecg.id) {
						exists = true;
						return;
					}
				});
				match = exists;
				return;
			});
			this.exceptionIsDirty = !match;
		}
	}

	private mapCategorySelections(selections: any[]): any[] {
		return selections.map((ecg) => ({
			id: ecg.id,
			name: ecg.name,
			description: ecg.description,
			parent: ecg.parent,
			displayOrder: ecg.displayOrder,
			category: ecg.category,
		}));
	}
}
