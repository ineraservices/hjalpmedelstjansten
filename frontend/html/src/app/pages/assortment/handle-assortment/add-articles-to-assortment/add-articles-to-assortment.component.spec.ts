import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AddArticlesToAssortmentComponent} from './add-articles-to-assortment.component';

describe('AddArticlesToAssortmentComponent', () => {
	let component: AddArticlesToAssortmentComponent;
	let fixture: ComponentFixture<AddArticlesToAssortmentComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [AddArticlesToAssortmentComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(AddArticlesToAssortmentComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
