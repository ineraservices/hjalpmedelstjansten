import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../../services/product.service';
import { AssortmentService } from '../../../../services/assortment.service';
import { OrganizationService } from '../../../../services/organization.service';
import { AlertService } from '../../../../services/alert.service';
import { FormControl } from '@angular/forms';
import { AuthService } from '../../../../auth/auth.service';
import { HelptextService } from '../../../../services/helptext.service';
import { Organization } from '../../../../models/organization/organization.model';
import { Assortment } from '../../../../models/assortment/assortment.model';
import { Location } from '@angular/common';
import { CommonService } from '../../../../services/common.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AgreementService } from '../../../../services/agreement.service';
import { Pricelist } from '../../../../models/agreement/pricelist.model';
import { Pricelistrow } from '../../../../models/agreement/pricelistrow.model';
import { SelectionModel } from '@angular/cdk/collections';
import { CategoryDialogComponent } from '../../../product/category-dialog/category-dialog.component';
import { SimpleArticle } from '../../../../models/product/simple-article.model';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { GeneralPricelistPricelist } from '../../../../models/general-pricelist/general-pricelist-pricelist.model';
import { Article } from '../../../../models/product/article.model';
import { StorageHelper } from '../../../../helpers/storage.helper';
import { Product } from '../../../../models/product/product.model';

@Component({
	selector: 'app-add-articles-to-assortment',
	templateUrl: './add-articles-to-assortment.component.html',
	styleUrls: ['./add-articles-to-assortment.component.scss'],
})
export class AddArticlesToAssortmentComponent implements OnInit {
	displayedColumns = [
		'checkbox',
		'articleType',
		'number',
		'name',
		'basedOnProduct',
		'code',
		'organizationName',
		'status',
	];

	storage = new StorageHelper('Assortment');
	loadSessionStorage = false;
	loaded = false;
	loading = false;
	saving = false;
	selection = new SelectionModel<Pricelistrow>(true, []);
	query = '';
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	queryParams = '';
	searchTotal;
	offset = 25;
	totalPages;
	currentPage = 1;
	paginatorArray = [];
	allPages;
	dataSource = null;
	allArticles = [];
	pricelistArticles = [];
	allPricelistArticles = [];
	allAssortmentArticles = [];
	assortment: Assortment = new Assortment();
	agreements = [];
	assortments: any[];
	GPs = [];
	selectedAgreement: number;
	selectedAssortment: number;
	selectedGP: number;
	pricelists: Array<Pricelist>;

	GPPricelists: Array<GeneralPricelistPricelist>;
	selectedPricelist: number;
	selectedGPPricelist: number;
	organization: Organization = new Organization();
	organizationId: number;
	assortmentId: number;
	helpTexts;
	helpTextsLoaded = false;
	flexDirection: string;

	// Permissions
	hasPermissionToCreateAssortment: boolean;
	hasPermissionToDeleteAssortment: boolean;
	hasPermissionToUpdateAssortment: boolean;
	isOwnOrganization = false;

	// Filters
	filterH = false;
	filterT = false;
	filterPublished = false;
	filterDiscontinued = false;
	filterCategory = '';
	categoryResult = '';

	sortResults = false;
	sortOrder = '';
	sortType = '';

	// Form controls
	nameFormControl = new FormControl(null, {
		updateOn: 'blur',
	});
	validFromFormControl = new FormControl(null, {
		updateOn: 'blur',
	});
	validToFormControl = new FormControl(null, {
		updateOn: 'blur',
	});
	categoryFormControl = new FormControl(null, {
		updateOn: 'blur',
	});

	// Validation errors
	nameError;
	validFromError;
	validToError;

	constructor(
		private organizationService: OrganizationService,
		private assortmentService: AssortmentService,
		private productService: ProductService,
		private agreementService: AgreementService,
		private commonService: CommonService,
		private route: ActivatedRoute,
		private alertService: AlertService,
		private authService: AuthService,
		private dialog: MatDialog,
		private location: Location,
		private helpTextService: HelptextService,
	) {
		this.flexDirection = this.commonService.isUsingIE() ? 'row' : 'column';

		this.route.params.subscribe((params) => {
			this.organizationId = params.organizationId;
			this.assortmentId = params.assortmentId;
		});
		this.isOwnOrganization =
			Number(this.organizationId) === this.authService.getOrganizationId();
		this.hasPermissionToCreateAssortment =
			this.hasPermissionToCreateAssortment && this.isOwnOrganization;
		this.hasPermissionToDeleteAssortment =
			this.hasPermissionToDeleteAssortment && this.isOwnOrganization;
		this.hasPermissionToUpdateAssortment =
			this.hasPermissionToUpdateAssortment && this.isOwnOrganization;

		// The user is considered done with their search after 500 ms without further input
		this.searchChanged
			.pipe(debounceTime(500), distinctUntilChanged())
			.subscribe((query) => {
				this.query = encodeURIComponent(query);
				this.onQueryChange();
			});
	}

	ngOnInit() {
		this.hasPermissionToCreateAssortment =
			this.hasPermission('assortment:create');
		this.hasPermissionToDeleteAssortment =
			this.hasPermission('assortment:delete');
		this.hasPermissionToUpdateAssortment =
			this.hasPermission('assortment:update');

		this.helpTexts = this.helpTextService.getTexts().subscribe((texts) => {
			this.helpTexts = texts;
			this.helpTextsLoaded = true;
			this.loaded = true;
		});

		this.assortmentService
			.getAssortment(this.organizationId, this.assortmentId)
			.subscribe((res) => {
				this.assortment = res;
				this.agreementService
					.searchAgreements(this.organizationId, '?limit=0')
					.subscribe({
						next: (agreements) => {
							this.agreements = agreements.body;
						},
						error: (error) => {
							this.alertService.clear();
							error.error.errors.forEach((err) => {
								this.alertService.error(err);
							});
						},
					});

				this.agreementService.searchGeneralPricelists('').subscribe({
					next: (GPs) => {
						this.GPs = GPs.body;
					},
					error: (error) => {
						this.alertService.clear();
						error.error.errors.forEach((err) => {
							this.alertService.error(err);
						});
					},
				});

				this.assortmentService
					.searchAssortments(this.organizationId, '')
					.subscribe({
						next: (assortments) => {
							this.assortments = assortments.body;
							// remove the assortment we want to add articles to
							for (const assortment of this.assortments) {
								if (assortment.id === this.assortmentId.valueOf()) {
									this.assortments.splice(
										this.assortments.indexOf(assortment),
										1,
									);
									break;
								}
							}
						},
						error: (error) => {
							this.alertService.clear();
							error.error.errors.forEach((err) => {
								this.alertService.error(err);
							});
						},
					});
			});

		this.queryParams = '&type=H&type=T&assortmentId=' + this.assortmentId;

		if (this.storage.hasPageBeenVisisted()) {
			this.loadSessionStorage = true;
			this.onFilterChange();
			this.getSessionStorage();
			this.loadSessionStorage = false;
		} else {
			this.onFilterChange();
		}
	}

	setSessionStorage() {
		this.storage.setItem('Query', this.tmpQuery.toString());
		this.storage.setItem('FilterH', this.filterH.toString());
		this.storage.setItem('FilterT', this.filterT.toString());
		this.storage.setItem('FilterPublished', this.filterPublished.toString());
		this.storage.setItem(
			'FilterDiscontinued',
			this.filterDiscontinued.toString(),
		);
		this.storage.setItem('FilterCategory', this.filterCategory);
		// Same as CategoryFormControl, to avoid dupe storage i'm putting it here
		if (this.categoryResult !== 'undefined') {
			this.storage.setItem('CategoryResult', this.categoryResult);
		} else {
			this.storage.setItem('CategoryResult', '');
		}

		this.storage.setItem('CurrentPage', this.currentPage.toString());
		this.storage.setItem('HelpTextsLoaded', this.helpTextsLoaded.toString());
	}

	getSessionStorage() {
		this.filterH = this.storage.getItemBoolean('FilterH');
		this.filterT = this.storage.getItemBoolean('FilterT');
		this.currentPage = this.storage.getItemNumber('CurrentPage');
		this.filterPublished = this.storage.getItemBoolean('FilterPublished');
		this.filterDiscontinued = this.storage.getItemBoolean('FilterDiscontinued');
		this.categoryFormControl.setValue(
			this.storage.getItemString('CategoryResult'),
		);
		this.filterCategory = this.storage.getItemString('FilterCategory');
		this.categoryResult = this.storage.getItemString('CategoryResult');
		this.helpTextsLoaded = this.storage.getItemBoolean('HelpTextsLoaded');

		// Query needs to be at the end of the method.
		if (
			this.storage.getItemString('Query') !== 'null' ||
			this.storage.getItemString('Query') !== ''
		) {
			this.tmpQuery = this.storage.getItemString('Query');
			this.query = this.storage.getItemString('Query');
		}
	}

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
	}

	onQueryChange() {
		this.sortResults = false;
		this.onFilterChange();
	}

	onFilterChange() {
		this.queryParams = '&assortmentId=' + this.assortmentId;
		if (this.filterH) {
			this.queryParams += '&type=H';
		}
		if (this.filterT) {
			this.queryParams += '&type=T';
		}
		if (!this.filterH && !this.filterT) {
			this.queryParams += '&type=H&type=T';
		}
		if (this.filterPublished) {
			this.queryParams += '&status=PUBLISHED';
		}
		if (this.filterDiscontinued) {
			this.queryParams += '&status=DISCONTINUED';
		}

		this.currentPage = 1;
		this.searchTotal = 0;
		this.allArticles = [];
		this.pricelistArticles = [];
		this.selection.clear();
		if (this.filterCategory) {
			this.queryParams += this.filterCategory;
		}
		if (this.sortResults) {
			this.queryParams += '&sortOrder=' + this.sortOrder;
			this.queryParams += '&sortType=' + this.sortType;
		}
		if (this.selectedPricelist) {
			// avtal
			this.searchArticlesForPricelistRows();
			this.searchArticlesForPricelistRows_getAll();
		} else if (this.selectedGPPricelist) {
			// GP
			this.searchArticlesForGPPricelistRows();
		} else if (this.selectedAssortment) {
			// utbud
			this.queryParams += '&status=PUBLISHED';
			this.queryParams += '&status=DISCONTINUED';
			this.searchArticlesForAssortment();
			this.searchArticlesForAssortment_getAll();
		} else if (
			!this.selectedAgreement &&
			!this.selectedGP &&
			!this.selectedAssortment
		) {
			this.searchArticles(
				this.query +
					'&includeArticles=true&includeProducts=false' +
					this.queryParams,
			); // elastic
		}

		this.setSessionStorage();
	}

	searchArticles(query) {
		// elastic
		this.loading = true;
		this.productService.search(query).subscribe({
			next: (res) => {
				this.searchTotal = res.headers.get('X-Total-Count');
				this.allArticles = this.searchTotal !== '0' ? res.body : [];
				this.totalPages = Math.ceil(this.searchTotal / this.offset);
				this.allPages = Array(this.totalPages)
					.fill(1)
					.map((x, i) => i + 1);
				this.dataSource = new MatTableDataSource<SimpleArticle>(
					this.allArticles,
				);
				this.loaded = true;
				this.loading = false;
			},
			error: (error) => {
				this.loading = false;
				this.alertService.clear();
				error.error.errors.forEach((err) => {
					this.alertService.error(err);
				});
			},
		});
	}

	sortData(matSort: MatSort) {
		this.sortResults = true;
		switch (this.sortOrder) {
		case 'asc':
			this.sortOrder = 'desc';
			break;
		case 'desc':
			this.sortOrder = 'asc';
			break;
		default:
			this.sortOrder = 'asc';
			break;
		}
		this.sortType = matSort.active;
		this.onFilterChange();
	}

	goToPage(page: number | string) {
		if (page === '...' || page === this.currentPage) {
			return;
		}

		this.loading = true;
		this.currentPage = typeof page === 'number' ? page : this.currentPage;
		const offset = `&offset=${(this.currentPage - 1) * this.offset}`;
		let searchObservable;

		if (this.selectedAssortment) {
			searchObservable = this.assortmentService.searchArticlesForAssortment(
				this.organizationId,
				this.selectedAssortment,
				this.queryParams + offset,
			);
		} else if (this.selectedPricelist) {
			searchObservable = this.agreementService.getPricelistRows(
				this.organizationId,
				this.selectedAgreement,
				this.selectedPricelist,
				this.query + this.queryParams + offset,
			);
		} else if (this.selectedGPPricelist) {
			searchObservable = this.agreementService.getGeneralPricelistPricelistRows(
				this.selectedGP,
				this.selectedGPPricelist,
				this.query + this.queryParams + offset,
			);
		} else {
			searchObservable = this.productService.search(
				this.query +
					'&includeArticles=true&includeProducts=false' +
					this.queryParams +
					offset,
			);
		}

		searchObservable.subscribe({
			next: (res) => this.handleSearchSuccess(res, page),
			error: (error) => this.handleSearchError(error),
		});
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	onAssortmentSelection() {
		if (this.selectedAssortment) {
			this.selectedAgreement = null;
			this.selectedPricelist = null;
			this.selectedGP = null;
			this.selectedGPPricelist = null;
			this.onFilterChange();
		} else {
			this.onFilterChange();
		}
	}

	onAgreementSelection() {
		this.selectedPricelist = null;

		if (this.selectedAgreement) {
			this.selectedAssortment = null;
			this.selectedGPPricelist = null;
			this.selectedGP = null;
			this.onFilterChange();
			this.agreementService
				.getPriceLists(this.organizationId, this.selectedAgreement)
				.subscribe((pricelists) => {
					this.pricelists = pricelists;
				});
		} else {
			this.onFilterChange();
		}
	}

	onGPSelection() {
		this.selectedGPPricelist = null;

		if (this.selectedGP) {
			this.selectedAssortment = null;
			this.selectedAgreement = null;
			this.selectedPricelist = null;
			this.onFilterChange();

			this.agreementService
				.getGeneralPricelistPriceLists(this.selectedGP)
				.subscribe((pricelists) => {
					this.GPPricelists = pricelists;
				});
		} else {
			this.onFilterChange();
		}
	}

	searchArticlesForAssortment() {
		this.queryParams += '&selectedAssortmentUniqueId=' + this.assortment.id;
		this.assortmentService
			.searchArticlesForAssortment(
				this.organizationId,
				this.selectedAssortment,
				this.queryParams,
			)
			.subscribe({
				next: (res) => {
					this.searchTotal = res.headers.get('X-Total-Count');
					this.pricelistArticles =
						this.searchTotal !== '0'
							? (res.body as Article[])
							: ([] as Article[]);
					this.totalPages = Math.ceil(this.searchTotal / this.offset);
					this.allPages = Array(this.totalPages)
						.fill(1)
						.map((x, i) => i + 1);
					this.dataSource = new MatTableDataSource<TableColumnData>(
						this.pricelistArticles.map(({ article }) => ({
							articleType: article.type,
							name: article.articleName,
							number: article.articleNumber,
							organizationName: article.organizationName,
							status: article.status,
							basedOnProduct: article.basedOnProduct,
						})),
					);
					this.loaded = true;
					this.loading = false;
				},
				error: (error) => {
					this.alertService.clear();
					error.error.errors.forEach((err: any) => {
						this.alertService.error(err);
					});
				},
			});
	}

	searchArticlesForAssortment_getAll() {
		this.queryParams += '&selectedAssortmentUniqueId=' + this.assortment.id;
		this.assortmentService
			.searchArticlesForAssortment(
				this.organizationId,
				this.selectedAssortment,
				this.queryParams,
			)
			.subscribe({
				next: (res) => {
					this.allAssortmentArticles = res.body;
				},
				error: (error) => {
					this.alertService.clear();
					error.error.errors.forEach((err: any) => {
						this.alertService.error(err);
					});
				},
			});
	}

	searchArticlesForPricelistRows() {
		this.agreementService
			.getPricelistRows(
				this.organizationId,
				this.selectedAgreement,
				this.selectedPricelist,
				this.query + this.queryParams,
			)
			.subscribe({
				next: (res) => {
					this.searchTotal = res.headers.get('X-Total-Count');
					this.pricelistArticles = this.searchTotal !== '0' ? res.body : [];
					this.totalPages = Math.ceil(this.searchTotal / this.offset);
					this.allPages = Array(this.totalPages)
						.fill(1)
						.map((x, i) => i + 1);
					this.dataSource = new MatTableDataSource<Pricelistrow>(
						this.pricelistArticles,
					);
					this.loaded = true;
					this.loading = false;
				},
				error: (error) => {
					this.alertService.clear();
					error.error.errors.forEach((err) => {
						this.alertService.error(err);
					});
				},
			});
	}

	searchArticlesForPricelistRows_getAll() {
		this.agreementService
			.getAllPricelistRows(
				this.organizationId,
				this.selectedAgreement,
				this.selectedPricelist,
				this.query + this.queryParams,
			)
			.subscribe({
				next: (res) => {
					this.allPricelistArticles = res.body;
				},
				error: (error) => {
					this.alertService.clear();
					error.error.errors.forEach((err) => {
						this.alertService.error(err);
					});
				},
			});
	}

	searchArticlesForGPPricelistRows() {
		this.agreementService
			.getGeneralPricelistPricelistRows(
				this.selectedGP,
				this.selectedGPPricelist,
				this.query + this.queryParams.replace('status=', 'articleStatus='),
			)
			.subscribe({
				next: (res) => {
					this.searchTotal = res.headers.get('X-Total-Count');
					this.pricelistArticles = this.searchTotal !== '0' ? res.body : [];
					this.totalPages = Math.ceil(this.searchTotal / this.offset);
					this.allPages = Array(this.totalPages)
						.fill(1)
						.map((x, i) => i + 1);
					this.dataSource = new MatTableDataSource<Pricelistrow>(
						this.pricelistArticles,
					);
					this.loaded = true;
					this.loading = false;
				},
				error: (error) => {
					this.alertService.clear();
					error.error.errors.forEach((err) => {
						this.alertService.error(err);
					});
				},
			});
	}

	searchArticlesForGPPricelistRows_getAll() {
		this.agreementService
			.getAllPricelistRows(
				this.organizationId,
				this.selectedAgreement,
				this.selectedPricelist,
				this.query + '&type=H&type=T' + this.queryParams,
			)
			.subscribe({
				next: (res) => {
					this.allPricelistArticles = res.body;
				},
				error: (error) => {
					this.alertService.clear();
					error.error.errors.forEach((err) => {
						this.alertService.error(err);
					});
				},
			});
	}

	openCategoryDialog() {
		const dialogRef = this.dialog.open(CategoryDialogComponent, {
			width: '90%',
			data: { includeCodelessCategories: false },
		});

		dialogRef.afterClosed().subscribe((result) => {
			if (result) {
				this.sortResults = false;
				this.sortOrder = '';
				this.sortType = '';
				if (result.code) {
					this.categoryFormControl.setValue(result.code + ' ' + result.name);
				} else {
					this.categoryFormControl.setValue(result.name);
				}
				this.filterCategory = '&category=' + result.id;
				this.onFilterChange();
			}
		});
	}

	clearCategory() {
		this.categoryFormControl.setValue('');
		this.filterCategory = '';
		this.onFilterChange();
	}

	/** Whether the number of selected elements matches the total number of selectable rows. */
	isAllSelected() {
		let result = true;
		let numRows = 0;
		this.dataSource.data.forEach((item) => {
			numRows++;
			const rowChecked = this.selection.selected.some(function (el) {
				return el.id === item.id;
			});
			if (!rowChecked) {
				result = false;
			}
		});
		return result;
	}

	/** Selects all selectable rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.dataSource.data.forEach((row) => {
				this.selection.select(row);
			});
		}
	}

	resetValidation() {
		this.nameError = null;
		this.nameFormControl.markAsDirty();
		this.validFromError = null;
		this.validFromFormControl.markAsDirty();
		this.validToError = null;
		this.validToFormControl.markAsDirty();
	}

	addArticlesToAssortment() {
		this.saving = true;
		const articles = [];
		if (this.selectedPricelist || this.selectedGPPricelist) {
			this.selection.selected.forEach((article) => {
				articles.push(article.article);
			});
		} else {
			this.selection.selected.forEach((article) => {
				articles.push({ id: article.id });
			});
		}

		this.assortmentService
			.addArticlesToAssortment(this.organizationId, this.assortmentId, articles)
			.subscribe({
				next: (res) => {
					this.saving = false;
					this.alertService.success('Sparat');
					this.onFilterChange();
				},
				error: (error) => {
					this.saving = false;
					this.alertService.clear();
					error.error.errors.forEach((err) => {
						this.alertService.error(err);
					});
				},
			});
	}

	addAllArticlesToAssortment() {
		this.saving = true;
		const articles = [];
		if (this.selectedPricelist) {
			this.allPricelistArticles.forEach((article) => {
				articles.push(article.article);
			});
		} else {
			this.allAssortmentArticles.forEach((article) => {
				articles.push({ id: article.id });
			});
		}

		this.assortmentService
			.addArticlesToAssortment(this.organizationId, this.assortmentId, articles)
			.subscribe({
				next: (res) => {
					this.saving = false;
					this.alertService.success('Sparat');
					this.onFilterChange();
				},
				error: (error) => {
					this.saving = false;
					this.alertService.clear();
					error.error.errors.forEach((err) => {
						this.alertService.error(err);
					});
				},
			});
	}

	goBack() {
		this.location.back();
	}

	private pricelistrowToSimpleArticle(list: Pricelistrow[]): TableColumnData[] {
		return list.map(({ article, basedOnProduct }) => ({
			articleType: article.type,
			name: article.articleName,
			number: article.articleNumber,
			organizationName: article.organizationName,
			status: article.status,
			basedOnProduct: basedOnProduct,
		}));
	}

	private handleSearchSuccess(res: any, page: number | string) {
		this.loaded = true;
		this.loading = false;

		if (this.selectedGPPricelist) {
			this.searchTotal = res.headers.get('X-Total-Count');
			this.pricelistArticles = this.searchTotal !== '0' ? res.body : [];
			this.totalPages = Math.ceil(this.searchTotal / this.offset);
			this.allPages = Array.from({ length: this.totalPages }, (_, i) => i + 1);
		} else {
			this.pricelistArticles = res.body;
		}

		this.dataSource = new MatTableDataSource(
			this.pricelistrowToSimpleArticle(this.pricelistArticles),
		);
		this.currentPage = typeof page === 'number' ? page : this.currentPage;
	}

	private handleSearchError(error: any) {
		this.loading = false;
		this.alertService.clear();
		error.error.errors.forEach((err: any) => this.alertService.error(err));
	}
}

interface TableColumnData {
	articleType: string;
	number: string;
	name: string;
	basedOnProduct: Product;
	organizationName: string;
	status: string;
}
