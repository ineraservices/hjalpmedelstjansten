
import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AssortmentService} from "../../../../services/assortment.service";
import {AlertService} from "../../../../services/alert.service";


@Component({
  selector: 'app-delete-assortmentrow-dialog',
  templateUrl: './delete-assortmentrow-dialog.component.html',
  styleUrls: ['./delete-assortmentrow-dialog.component.scss']
})
export class DeleteAssortmentrowDialogComponent implements OnInit {

  loading = false;

  constructor(public dialogRef: MatDialogRef<DeleteAssortmentrowDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private assortmentService: AssortmentService,
              private alertService: AlertService) {
  }

  ngOnInit() {
  }

  done() {
    console.log('done has been pressed');
    this.loading = true;
    this.assortmentService.removeArticleFromAssortment( this.data.organizationId, this.data.assortmentId, this.data.articleId).subscribe(
    res => {
      this.dialogRef.close('success');
      this.loading = false;

    }, error => {
      this.alertService.clear();
      error.error.errors.forEach(err => {
        this.alertService.error(err);
        this.loading = false;
      });
    });

  }

}
