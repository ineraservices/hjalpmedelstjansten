
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAssortmentrowDialogComponent } from './delete-assortmentrow-dialog.component';

describe('DeleteProductDialogComponent', () => {
  let component: DeleteAssortmentrowDialogComponent;
  let fixture: ComponentFixture<DeleteAssortmentrowDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteAssortmentrowDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAssortmentrowDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
