import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {MatTableDataSource} from '@angular/material/table';
import {ActivatedRoute, Router} from '@angular/router';
import {Assortment} from '../../models/assortment/assortment.model';
import {AssortmentService} from '../../services/assortment.service';
import {AlertService} from '../../services/alert.service';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {HelptextService} from '../../services/helptext.service';
import {StorageHelper as AssortmentStorage} from '../../helpers/storage.helper';
import {saveAs} from "file-saver";

@Component({
	selector: 'app-assortment',
	templateUrl: './assortment.component.html',
	styleUrls: ['./assortment.component.scss']
})
export class AssortmentComponent implements OnInit {
	displayedColumns = ['namn', 'organisation', 'län', 'giltigt från', 'giltigt till', 'utbudsansvarig'];

	// Sets the storage index to be used.
	storage = new AssortmentStorage('Assortment');
	loadSessionStorage = false;


	loaded = false;
	loading = false;
	organizationId: number;
	isSuperAdmin = false;
	query = '';
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	queryParams = '';
	searchTotal;
	offset = 25;
	totalPages;
	currentPage = 1;
	allPages;
	dataSource = null;
	assortments = [];
	helpTexts;
	helpTextsLoaded = false;

	// Filters
	filterCurrent = false;
	filterFuture = false;
	filterDiscontinued = false;
	include = 'ALL';

	// Permissions
	hasPermissionToViewAssortment: boolean;
	hasPermissionToCreateAssortment: boolean;
	isOwnOrganization = false;

	constructor(private route: ActivatedRoute,
							private authService: AuthService,
							private assortmentService: AssortmentService,
							private alertService: AlertService,
							private helpTextService: HelptextService,
							private router: Router) {
		this.route.params.subscribe(params => this.organizationId = params.organizationId);
		this.isOwnOrganization = Number(this.organizationId) === this.authService.getOrganizationId();
		this.hasPermissionToViewAssortment = this.hasPermissionToViewAssortment && this.isOwnOrganization;
		this.hasPermissionToCreateAssortment = this.hasPermissionToCreateAssortment && this.isOwnOrganization;
		this.isSuperAdmin = this.authService.isSuperAdmin();

		// The user is considered done with their search after 500 ms without further input
		this.searchChanged.pipe(
			debounceTime(500),
			distinctUntilChanged())
			.subscribe(query => {
				this.query = encodeURIComponent(query);
				this.onQueryChange();
				this.currentPage = 1;

			}
			);
	}

	ngOnInit() {
		this.hasPermissionToViewAssortment = this.hasPermission('assortment:view');
		this.hasPermissionToCreateAssortment = this.hasPermission('assortment:create');

		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);

		if (this.storage.hasPageBeenVisisted()) {
			this.loadSessionStorage = true;
			this.getSessionStorage();

			if (this.currentPage > 1) {
				// Assortment differs implementation-wise from the other modules - it's a different recursive flow - have to call searchAssortments manually.
				this.searchAssortments('?query=' + this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset);
			} else {
				this.searchAssortments('?query=' + this.tmpQuery + this.queryParams);
			}

		} else {
			this.searchAssortments('?include=ALL');
		}
	}

	setSessionStorage() {
		this.storage.setItem('Query', this.tmpQuery.toString());
		this.storage.setItem('Offset', this.offset.toString());
		this.storage.setItem('FilterDiscontinued', this.filterDiscontinued.toString());
		this.storage.setItem('CurrentPage', this.currentPage.toString());
		this.storage.setItem('HelpTextsLoaded', this.helpTextsLoaded.toString());
		this.storage.setItem('FilterFuture', this.filterFuture.toString());
		this.storage.setItem('FilterCurrent', this.filterCurrent.toString());
		this.storage.setItem('Include', this.include);
	}

	getSessionStorage() {
		this.offset = this.storage.getItemNumber('Offset');
		this.currentPage = this.storage.getItemNumber('CurrentPage');
		this.filterDiscontinued = this.storage.getItemBoolean('FilterDiscontinued');
		this.helpTextsLoaded = this.storage.getItemBoolean('HelpTextsLoaded');
		this.filterFuture = this.storage.getItemBoolean('FilterFuture');
		this.filterCurrent = this.storage.getItemBoolean('FilterCurrent');
		this.include = this.storage.getItemString('Include');


		// Query needs to be at the end of the method.
		if (this.storage.getItemString('Query') !== 'null' || this.storage.getItemString('Query') !== '') {
			this.tmpQuery = this.storage.getItemString('Query');
			this.query = this.storage.getItemString('Query');
		}
	}

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
	}

	onQueryChange() {
		this.onFilterChange();
	}

	onFilterChange() {
		this.queryParams = '';
		this.currentPage = 1;
		this.queryParams += this.filterCurrent ? '&status=CURRENT' : '';
		this.queryParams += this.filterFuture ? '&status=FUTURE' : '';
		this.queryParams += this.filterDiscontinued ? '&status=DISCONTINUED' : '';
		this.queryParams += '&include=' + this.include;
		this.searchAssortments('?query=' + this.query + this.queryParams);
		this.setSessionStorage();
	}

	searchAssortments(query: string) {
		this.loading = true;

		const searchObservable = this.isSuperAdmin
			? this.assortmentService.searchAllAssortments(query)
			: this.assortmentService.searchAssortments(this.organizationId, query);

		searchObservable.subscribe({
			next: this.handleSearchSuccess.bind(this),
			error: this.handleSearchError.bind(this)
		});
	}

	exportSearchResults() {
		const query = '?query=' + this.query + this.queryParams;
		if (!this.isSuperAdmin) {
			this.assortmentService.exportAssortments(this.organizationId, query).subscribe(
				data => {
					saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		} else {
			this.assortmentService.exportAllAssortments(query).subscribe(
				data => {
					saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		}
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	goToPage(page) {
		if (page !== '...' && page !== this.currentPage) {
			this.currentPage = page;
			this.storage.setItem('CurrentPage', page);
			this.searchAssortments('?query=' + this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset);
		}
	}

	private handleSearchSuccess(res: any) {
		this.searchTotal = res.headers.get('X-Total-Count');
		this.assortments = this.searchTotal !== '0' ? res.body : [];
		this.dataSource = new MatTableDataSource<Assortment>(this.assortments);
		this.totalPages = Math.ceil(this.searchTotal / this.offset);
		this.allPages = Array.from({ length: this.totalPages }, (_, i) => i + 1);
		this.loaded = true;
		this.loading = false;
		this.setSessionStorage();
	}

	private handleSearchError(error: any) {
		this.loading = false;
		this.alertService.clear();
		error.error.errors.forEach(err => {
			this.alertService.error(err);
		});
	}

}
