import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {SendWelcomeMailDialogComponent} from './send-welcome-mail-dialog/send-welcome-mail-dialog.component';
import {CommonService} from '../../services/common.service';
import {DynamicTextService} from "../../services/dynamictext.service";
import {DynamicText} from "../../models/dynamictext.model";
import {AlertService} from "../../services/alert.service";

@Component({
	selector: 'app-administration',
	templateUrl: './administration.component.html',
	styleUrls: ['./administration.component.scss']
})
export class AdministrationComponent implements OnInit {
	flexDirection: string;
	dynamicTextLoaded = false;
	dynamicText: DynamicText;
	welcomeTextLoaded = false;
	welcomeText: DynamicText;
	linksTextLoaded = false;
	linksText: DynamicText;

	constructor(private dialog: MatDialog,
							private dynamicTextService: DynamicTextService,
							private commonService: CommonService,
							private alertService: AlertService) {
		this.flexDirection = this.commonService.isUsingIE() ? 'row' : 'column';
	}

	ngOnInit() {
		this.dynamicTextService.getDynamicText(1).subscribe(
			data => {
				this.dynamicText = data;
				this.dynamicTextLoaded = true;
			}
		);
		this.dynamicTextService.getDynamicText(2).subscribe(
			data => {
				this.welcomeText = data;
				this.welcomeTextLoaded = true;
			}
		);
		this.dynamicTextService.getDynamicText(3).subscribe(
			data => {
				this.linksText = data;
				this.linksTextLoaded = true;
			}
		);
	}

	openSendWelcomeMailDialog() {
		const dialogRef = this.dialog.open(SendWelcomeMailDialogComponent, {
			width: '80%'
		});
	}

	// skulle kunna slå ihop dessa tre till en...
	saveInfoTextToDatabase() {
		this.dynamicTextService.updateDynamicText(1, this.dynamicText).subscribe(
			data => {
				this.dynamicText = data;
				this.alertService.success('Sparat');
			}
		);
	}

	saveWelcomeTextToDatabase() {
		this.dynamicTextService.updateDynamicText(2, this.welcomeText).subscribe(
			data => {
				this.welcomeText = data;
				this.alertService.success('Sparat');
			}
		);
	}

	saveLinksTextToDatabase() {
		this.dynamicTextService.updateDynamicText(3, this.linksText).subscribe(
			data => {
				this.linksText = data;
				this.alertService.success('Sparat');
			}
		);
	}

}
