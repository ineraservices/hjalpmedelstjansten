import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {AlertService} from '../../../services/alert.service';
import {LoginService} from '../../../services/login.service';

@Component({
	selector: 'app-send-welcome-mail-dialog',
	templateUrl: './send-welcome-mail-dialog.component.html',
	styleUrls: ['./send-welcome-mail-dialog.component.scss']
})
export class SendWelcomeMailDialogComponent {
	saving = false;

	constructor(public dialogRef: MatDialogRef<SendWelcomeMailDialogComponent>,
							private loginService: LoginService,
							private alertService: AlertService) {
	}

	done() {
		this.saving = true;
		this.loginService.welcomeAll().subscribe(
			res => {
				this.saving = false;
				this.alertService.clear();
				this.alertService.success('Skickat');
				this.dialogRef.close('success');
			}, error => {
				this.saving = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			});
	}

}
