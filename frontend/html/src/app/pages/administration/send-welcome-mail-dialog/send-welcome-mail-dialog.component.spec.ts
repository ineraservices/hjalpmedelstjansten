import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SendWelcomeMailDialogComponent} from './send-welcome-mail-dialog.component';

describe('SendWelcomeMailDialogComponent', () => {
	let component: SendWelcomeMailDialogComponent;
	let fixture: ComponentFixture<SendWelcomeMailDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [SendWelcomeMailDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SendWelcomeMailDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
