import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {UserService} from '../../../services/user.service';
import {BusinessLevel} from '../../../models/organization/business-level.model';
import {User} from '../../../models/user/user.model';
import {AlertService} from '../../../services/alert.service';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

@Component({
	selector: 'app-add-users-dialog',
	templateUrl: './add-users-dialog.component.html',
	styleUrls: ['./add-users-dialog.component.scss']
})
export class AddUsersDialogComponent implements OnInit {
	displayedColumns = ['checkbox', 'namn', 'epost', 'mobilnummer'];
	loading = true;
	businessLevel: BusinessLevel;
	users = [];
	dataSource = null;
	query = '';
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	searchTotal;
	offset = 25;
	totalPages;
	currentPage = 1;
	paginatorArray = [];
	allPages;
	usersToAdd = [];
	excludedUsers = [];

	constructor(public dialogRef: MatDialogRef<AddUsersDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private userService: UserService,
							private alertService: AlertService) {
		this.businessLevel = data.businessLevel;
		this.excludedUsers = data.excludedUsers;

		// The user is considered done with their search after 500 ms without further input
		this.searchChanged.pipe(
			debounceTime(500),
			distinctUntilChanged())
			.subscribe(query => {
				this.query = encodeURIComponent(query);
				this.onQueryChange();
			}
			);
	}

	ngOnInit() {
		this.searchUsers('');
	}

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
	}

	onQueryChange() {
		this.searchUsers(this.query);
	}

	searchUsers(query) {
		this.loading = true;
		if (this.businessLevel && this.businessLevel.id !== 99) {
			query += '&businessLevel=' + this.businessLevel.id;
		}
		if (this.excludedUsers && this.excludedUsers.length) {
			this.excludedUsers.forEach(user => {
				query += '&exclude=' + user.id;
			});
		}
		this.userService.searchPricelistApprovers(this.data.organizationId, query).subscribe(
			res => {
				this.searchTotal = res.headers.get('X-Total-Count');
				if (this.searchTotal !== '0') {
					this.users = res.body;
				} else {
					this.users = [];
				}
				this.totalPages = Math.ceil(this.searchTotal / this.offset);
				this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
				this.dataSource = new MatTableDataSource<User>(this.users);
				this.generatePaginator();
			}, error => {
				this.loading = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	goToPage(page) {
		if (page !== '...' && page !== this.currentPage) {
			this.currentPage = page;
			this.searchUsers(this.query + '&offset=' + (this.currentPage - 1) * this.offset);
		}
	}

	generatePaginator() {
		let hasFirstEllipsis = false;
		let hasSecondEllipsis = false;
		this.paginatorArray = [];
		if (this.searchTotal > this.offset) {
			if (this.totalPages < 6) {
				this.paginatorArray = this.allPages;
			} else {
				this.allPages.forEach((pageNumber) => {
					if (pageNumber === 1
						|| pageNumber === this.currentPage
						|| pageNumber === this.totalPages
						|| pageNumber === this.currentPage + 1
						|| pageNumber === this.currentPage - 1) {
						this.paginatorArray.push(pageNumber);
					} else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
						this.paginatorArray.push('...');
						hasFirstEllipsis = true;
					} else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
						this.paginatorArray.push('...');
						hasSecondEllipsis = true;
					}
				});
			}
		}
		this.loading = false;
	}

	onChecked(element, event) {
		if (event.checked) {
			if (!this.usersToAdd.some(function (user) {
				return user.id === element.id;
			})) {
				this.usersToAdd.push(element);
			}
		} else {
			this.usersToAdd = this.usersToAdd.filter(o => o !== element);
		}
	}

	done() {
		this.dialogRef.close(this.usersToAdd);
	}

}
