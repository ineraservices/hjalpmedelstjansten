import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HandleGeneralPricelistComponent} from './handle-general-pricelist.component';

describe('HandleGeneralPricelistComponent', () => {
	let component: HandleGeneralPricelistComponent;
	let fixture: ComponentFixture<HandleGeneralPricelistComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [HandleGeneralPricelistComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(HandleGeneralPricelistComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
