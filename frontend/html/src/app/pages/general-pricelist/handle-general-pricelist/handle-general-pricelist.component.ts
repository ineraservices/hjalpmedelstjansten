import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {OrganizationService} from '../../../services/organization.service';
import {Location} from '@angular/common';
import {AlertService} from '../../../services/alert.service';
import {AuthService} from '../../../auth/auth.service';
import {GeneralPricelist} from '../../../models/general-pricelist/general-pricelist.model';
import {ActivatedRoute} from '@angular/router';
import {ProductService} from '../../../services/product.service';
import {User} from '../../../models/user/user.model';
import {GuaranteeUnit} from '../../../models/agreement/guarantee-unit.model';
import {UserService} from '../../../services/user.service';
import {AgreementService} from '../../../services/agreement.service';
import {GeneralPricelistPricelist} from '../../../models/general-pricelist/general-pricelist-pricelist.model';
import {MatDialog} from '@angular/material/dialog';
import {
	GeneralPricelistPricelistDialogComponent
} from '../general-pricelist-pricelist-dialog/general-pricelist-pricelist-dialog.component';
import {PreventiveMaintenance} from '../../../models/preventive-maintenance.model';
import {CommonService} from '../../../services/common.service';
import {HelptextService} from '../../../services/helptext.service';

@Component({
	selector: 'app-handle-general-pricelist',
	templateUrl: './handle-general-pricelist.component.html',
	styleUrls: ['./handle-general-pricelist.component.scss']
})
export class HandleGeneralPricelistComponent implements OnInit {

	loaded = false;
	loading = false;
	saving = false;
	editMode = false;
	tabIndex = 0;
	showSaveBar = true;
	organizationId: number;
	generalPricelist: GeneralPricelist = new GeneralPricelist();
	generalPricelistId: number;
	generalPricelistPricelists: Array<GeneralPricelistPricelist>;
	supplier: string;
	supplierContacts: Array<User>;
	helpTexts;
	helpTextsLoaded = false;
	flexDirection: string;

	// Warranty
	units: Array<GuaranteeUnit>;
	selectedWarrantyQuantityHUnit: GuaranteeUnit;
	selectedWarrantyQuantityTUnit: GuaranteeUnit;
	selectedWarrantyQuantityRUnit: GuaranteeUnit;
	selectedWarrantyQuantityIUnit: GuaranteeUnit;
	selectedWarrantyQuantityTJUnit: GuaranteeUnit;
	selectedWarrantyValidFromH: PreventiveMaintenance;
	selectedWarrantyValidFromT: PreventiveMaintenance;
	selectedWarrantyValidFromR: PreventiveMaintenance;
	selectedWarrantyValidFromI: PreventiveMaintenance;
	selectedWarrantyValidFromTJ: PreventiveMaintenance;
	preventiveMaintenanceValidity: Array<PreventiveMaintenance> = new Array<PreventiveMaintenance>();

	// Permissions
	hasPermissionToCreateGeneralPricelist: boolean;
	hasPermissionToUpdateGeneralPricelist: boolean;
	hasPermissionToCreateGeneralPricelistPricelist: boolean;
	hasPermissionToUpdateGeneralPricelistPricelist: boolean;
	isOwnOrganization = false;

	// Form controls
	numberFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	nameFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	validFromFormControl = new FormControl(null, {
		updateOn: 'blur'
	});
	validToFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	// Validation errors
	nameError;
	numberError;
	validFromError;
	validToError;


	constructor(private organizationService: OrganizationService,
							private productService: ProductService,
							private userService: UserService,
							private agreementService: AgreementService,
							private commonService: CommonService,
							private route: ActivatedRoute,
							private alertService: AlertService,
							private authService: AuthService,
							private location: Location,
							private pricelistDialog: MatDialog,
							private helpTextService: HelptextService) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}
		this.route.params.subscribe(params => {
			this.organizationId = params.organizationId;
			this.generalPricelistId = params.id;
		});
		this.isOwnOrganization = Number(this.organizationId) === this.authService.getOrganizationId();
		this.hasPermissionToCreateGeneralPricelist = this.hasPermissionToCreateGeneralPricelist && this.isOwnOrganization;
		this.hasPermissionToUpdateGeneralPricelist = this.hasPermissionToUpdateGeneralPricelist && this.isOwnOrganization;
		this.hasPermissionToCreateGeneralPricelistPricelist = this.hasPermissionToCreateGeneralPricelistPricelist && this.isOwnOrganization;
		this.hasPermissionToUpdateGeneralPricelistPricelist = this.hasPermissionToUpdateGeneralPricelistPricelist && this.isOwnOrganization;
	}

	ngOnInit() {
		this.hasPermissionToCreateGeneralPricelist = this.hasPermission('generalpricelist:create_own');
		this.hasPermissionToUpdateGeneralPricelist = this.hasPermission('generalpricelist:update_own');
		this.hasPermissionToCreateGeneralPricelistPricelist = this.hasPermission('generalpricelist_pricelist:create_own');
		this.hasPermissionToUpdateGeneralPricelistPricelist = this.hasPermission('generalpricelist_pricelist:update_own');

		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);
		this.onEditModeChange();

		this.organizationService.getOrganization(this.organizationId).subscribe(
			res => {
				this.supplier = res.organizationName;
				this.userService.getUserByRoles(res.id, 'SupplierAgreementManager').subscribe(
					data => {
						this.supplierContacts = data;
					}
				);
				this.commonService.getPreventiveMaintenances().subscribe(
					preventiveMaintenanceValidity => {
						this.preventiveMaintenanceValidity = preventiveMaintenanceValidity;
						this.productService.getGuaranteeUnits().subscribe(
							data => {
								this.units = data;
								this.getGeneralPricelist();
							}
						);
					}
				);

			}
		);

	}

	onTabSelection(event) {
		this.tabIndex = event.index;
	}

	toggleEditMode() {
		this.editMode = !this.editMode;
		this.onEditModeChange();
	}

	onEditModeChange() {
		if (!this.editMode) {
			this.numberFormControl.disable();
			this.nameFormControl.disable();
			this.validFromFormControl.disable();
			this.validToFormControl.disable();
		} else if (!this.generalPricelist.hasPricelistRows) {
			if (this.hasPermissionToCreateGeneralPricelist) {
				if (!this.generalPricelistId) {
					this.numberFormControl.enable();
				}
				this.nameFormControl.enable();
				this.validFromFormControl.enable();
				this.validToFormControl.enable();
			}
		} else {
			this.nameFormControl.enable();
			this.validToFormControl.enable();
		}
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	getGeneralPricelist() {
		this.agreementService.getGeneralPriceList(this.organizationId).subscribe(
			res => {
				if (res) {
					this.generalPricelistId = res.id;
					this.generalPricelist = res;
					this.numberFormControl.setValue(res.generalPricelistNumber);
					this.nameFormControl.setValue(res.generalPricelistName);
					this.convertToDateString();
					this.loaded = true;

					// Warranty
					if (this.generalPricelist.warrantyQuantityHUnit) {
						this.selectedWarrantyQuantityHUnit = this.units.find(e => e.id === this.generalPricelist.warrantyQuantityHUnit.id);
					} else {
						this.selectedWarrantyQuantityHUnit = null;
					}
					if (this.generalPricelist.warrantyQuantityTUnit) {
						this.selectedWarrantyQuantityTUnit = this.units.find(e => e.id === this.generalPricelist.warrantyQuantityTUnit.id);
					} else {
						this.selectedWarrantyQuantityTUnit = null;
					}
					if (this.generalPricelist.warrantyQuantityRUnit) {
						this.selectedWarrantyQuantityRUnit = this.units.find(e => e.id === this.generalPricelist.warrantyQuantityRUnit.id);
					} else {
						this.selectedWarrantyQuantityRUnit = null;
					}
					if (this.generalPricelist.warrantyQuantityIUnit) {
						this.selectedWarrantyQuantityIUnit = this.units.find(e => e.id === this.generalPricelist.warrantyQuantityIUnit.id);
					} else {
						this.selectedWarrantyQuantityIUnit = null;
					}
					if (this.generalPricelist.warrantyQuantityTJUnit) {
						this.selectedWarrantyQuantityTJUnit = this.units.find(e => e.id === this.generalPricelist.warrantyQuantityTJUnit.id);
					} else {
						this.selectedWarrantyQuantityTJUnit = null;
					}
					if (this.generalPricelist.warrantyValidFromH) {
						this.selectedWarrantyValidFromH = this.preventiveMaintenanceValidity.find(
							e => e.id === this.generalPricelist.warrantyValidFromH.id);
					} else {
						this.selectedWarrantyValidFromH = null;
					}
					if (this.generalPricelist.warrantyValidFromT) {
						this.selectedWarrantyValidFromT = this.preventiveMaintenanceValidity.find(
							e => e.id === this.generalPricelist.warrantyValidFromT.id);
					} else {
						this.selectedWarrantyValidFromT = null;
					}
					if (this.generalPricelist.warrantyValidFromR) {
						this.selectedWarrantyValidFromR = this.preventiveMaintenanceValidity.find(
							e => e.id === this.generalPricelist.warrantyValidFromR.id);
					} else {
						this.selectedWarrantyValidFromR = null;
					}
					if (this.generalPricelist.warrantyValidFromI) {
						this.selectedWarrantyValidFromI = this.preventiveMaintenanceValidity.find(
							e => e.id === this.generalPricelist.warrantyValidFromI.id);
					} else {
						this.selectedWarrantyValidFromI = null;
					}
					if (this.generalPricelist.warrantyValidFromTJ) {
						this.selectedWarrantyValidFromTJ = this.preventiveMaintenanceValidity.find(
							e => e.id === this.generalPricelist.warrantyValidFromTJ.id);
					} else {
						this.selectedWarrantyValidFromTJ = null;
					}
					this.getPricelistsForGeneralPricelist();
				} else {
					this.editMode = true;
					this.onEditModeChange();
				}
				this.loaded = true;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	getPricelistsForGeneralPricelist() {
		this.loading = true;
		this.agreementService.getGeneralPricelistPriceLists(this.organizationId).subscribe(
			pricelists => {
				this.loading = false;
				this.generalPricelistPricelists = pricelists;
			}, error => {
				this.loading = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	openCreatePricelistDialog() {
		const dialogRef = this.pricelistDialog.open(GeneralPricelistPricelistDialogComponent, {
			width: '80%',
			data: {
				'organizationId': this.organizationId,
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.getPricelistsForGeneralPricelist();
			}
		});
	}

	openUpdatePricelistDialog(row) {
		const dialogRef = this.pricelistDialog.open(GeneralPricelistPricelistDialogComponent, {
			width: '80%',
			data: {
				'organizationId': this.organizationId,
				'pricelistNumber': row.number,
				'validFrom': row.validFrom,
				'pricelistId': row.id
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.getPricelistsForGeneralPricelist();
			}
		});
	}

	convertToDateString() {
		this.validFromFormControl.setValue(new Date(this.generalPricelist.validFrom));
		this.validToFormControl.setValue(new Date(this.generalPricelist.validTo));
	}

	getMillisecondsFromDate(date): number {
		if (date) {
			return date.getTime();
		}
		return null;
	}

	handleServerValidation(error): void {
		switch (error.field) {
		case 'agreementName': {
			this.nameError = error.message;
			this.nameFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'agreementNumber': {
			this.numberError = error.message;
			this.numberFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'validFrom': {
			this.validFromError = error.message;
			this.validFromFormControl.setErrors(Validators.pattern(''));
			break;
		}
		case 'validTo': {
			this.validToError = error.message;
			this.validToFormControl.setErrors(Validators.pattern(''));
			break;
		}
		}
	}

	resetValidation() {
		this.nameError = null;
		this.nameFormControl.markAsDirty();
		this.numberError = null;
		this.numberFormControl.markAsDirty();
		this.validFromError = null;
		this.validFromFormControl.markAsDirty();
		this.validToError = null;
		this.validToFormControl.markAsDirty();

	}

	saveGeneralPricelist() {
		this.saving = true;
		this.resetValidation();
		this.generalPricelist.validFrom = this.getMillisecondsFromDate(this.validFromFormControl.value);
		this.generalPricelist.validTo = this.getMillisecondsFromDate(this.validToFormControl.value);
		this.generalPricelist.generalPricelistNumber = this.numberFormControl.value;
		this.generalPricelist.generalPricelistName = this.nameFormControl.value;
		// Warranty
		this.generalPricelist.warrantyQuantityHUnit = this.selectedWarrantyQuantityHUnit;
		this.generalPricelist.warrantyQuantityTUnit = this.selectedWarrantyQuantityTUnit;
		this.generalPricelist.warrantyQuantityRUnit = this.selectedWarrantyQuantityRUnit;
		this.generalPricelist.warrantyQuantityIUnit = this.selectedWarrantyQuantityIUnit;
		this.generalPricelist.warrantyQuantityTJUnit = this.selectedWarrantyQuantityTJUnit;
		this.generalPricelist.warrantyValidFromH = this.selectedWarrantyValidFromH;
		this.generalPricelist.warrantyValidFromT = this.selectedWarrantyValidFromT;
		this.generalPricelist.warrantyValidFromR = this.selectedWarrantyValidFromR;
		this.generalPricelist.warrantyValidFromI = this.selectedWarrantyValidFromI;
		this.generalPricelist.warrantyValidFromTJ = this.selectedWarrantyValidFromTJ;

		if (this.generalPricelistId) {
			this.updateGeneralPricelist();
		} else {
			this.createGeneralPricelist();
		}

	}

	updateGeneralPricelist() {
		this.agreementService.updateGeneralPriceList(this.organizationId, this.generalPricelist).subscribe(
			data => {
				this.generalPricelist = data;
				this.toggleEditMode();
				this.saving = false;
				this.alertService.success('Sparat');
			}, error => {
				this.saving = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.handleServerValidation(err);
					this.alertService.error(err);
				});
			}
		);
	}

	createGeneralPricelist() {
		this.agreementService.createGeneralPriceList(this.organizationId, this.generalPricelist).subscribe(
			data => {
				this.generalPricelistId = data.id;
				this.generalPricelist = data;
				this.getPricelistsForGeneralPricelist();
				this.toggleEditMode();
				this.saving = false;
				this.alertService.success('Sparat');
			}, error => {
				this.saving = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.handleServerValidation(err);
					this.alertService.error(err);
				});
			}
		);
	}

	cancel() {
		this.getGeneralPricelist();
		this.toggleEditMode();
	}

	goBack() {
		this.location.back();
	}

}

