import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ViewGeneralPricelistComponent} from './view-general-pricelist.component';

describe('ViewGeneralPricelistComponent', () => {
	let component: ViewGeneralPricelistComponent;
	let fixture: ComponentFixture<ViewGeneralPricelistComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ViewGeneralPricelistComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ViewGeneralPricelistComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
