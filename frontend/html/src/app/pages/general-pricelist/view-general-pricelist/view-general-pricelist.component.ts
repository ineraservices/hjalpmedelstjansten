import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {AgreementService} from '../../../services/agreement.service';
import {GeneralPricelist} from '../../../models/general-pricelist/general-pricelist.model';
import {AlertService} from '../../../services/alert.service';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {HelptextService} from '../../../services/helptext.service';
import {Router} from "@angular/router";

@Component({
	selector: 'app-view-general-pricelist',
	templateUrl: './view-general-pricelist.component.html',
	styleUrls: ['./view-general-pricelist.component.scss']
})
export class ViewGeneralPricelistComponent implements OnInit {
	displayedColumns = ['avtalsnummer', 'avtalsnamn', 'leverantör', 'giltigt from', 'giltigt tom'];

	loading = false;
	query = '';
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	queryParams = '';
	searchTotal;
	offset = 25;
	totalPages;
	currentPage = 1;
	paginatorArray = [];
	allPages;
	dataSource = null;
	generalPricelists = [];
	helpTexts;
	helpTextsLoaded = false;

	// Filters
	filterCurrent = false;
	filterFuture = false;
	filterDiscontinued = false;

  // Sorting
  sortResults = true;
  sortOrder: String = 'asc';
  sortType: String = 'ownerOrganization.organizationName';


	constructor(private agreementService: AgreementService,
							private alertService: AlertService,
							private helpTextService: HelptextService,
							private router: Router) {
		// The user is considered done with their search after 500 ms without further input
		this.searchChanged.pipe(
			debounceTime(500),
			distinctUntilChanged())
			.subscribe(query => {
				this.query = encodeURIComponent(query);
				this.onQueryChange();
			}
			);
	}

	ngOnInit() {
		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);
		if (this.sortResults) {
      this.queryParams += '&sortOrder=' + this.sortOrder;
      this.queryParams += '&sortType=' + this.sortType;
    }
    this.searchGeneralPricelists('' + this.queryParams);

  }

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
	}

	onQueryChange() {
		this.onFilterChange();
	}

	onFilterChange() {
		this.queryParams = '';
		this.currentPage = 1;
		// if (this.filterCurrent) {
		// 	this.queryParams += '&status=CURRENT';
		// }
		// if (this.filterFuture) {
		// 	this.queryParams += '&status=FUTURE';
		// }
		// if (this.filterDiscontinued) {
		// 	this.queryParams += '&status=DISCONTINUED';
		// }
		if (this.sortResults) {
			this.queryParams += '&sortOrder=' + this.sortOrder;
			this.queryParams += '&sortType=' + this.sortType;
		}
		this.searchGeneralPricelists(this.query + this.queryParams);
	}

	sortData(matSort: MatSort) {
		this.sortResults = true;

		this.sortOrder = matSort.direction;
		this.sortType = matSort.active;
		this.onFilterChange();

	}

	searchGeneralPricelists(query) {
		this.loading = true;
		this.agreementService.searchGeneralPricelists(query).subscribe(
			res => {
				this.searchTotal = res.headers.get('X-Total-Count');
				if (this.searchTotal !== '0') {
					this.generalPricelists = res.body;
				} else {
					this.generalPricelists = [];
				}
				this.dataSource = new MatTableDataSource<GeneralPricelist>(this.generalPricelists);
				this.totalPages = Math.ceil(this.searchTotal / this.offset);
				this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
				this.generatePaginator();
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	goToPage(page) {
		if (page !== '...' && page !== this.currentPage) {
			this.loading = true;
			this.currentPage = page;
			this.agreementService.searchGeneralPricelists(
				this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset).subscribe(
				res => {
					this.generalPricelists = res.body;
					this.dataSource = new MatTableDataSource<GeneralPricelist>(this.generalPricelists);
					this.currentPage = page;
					this.generatePaginator();
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		}
	}

	generatePaginator() {
		let hasFirstEllipsis = false;
		let hasSecondEllipsis = false;
		this.paginatorArray = [];
		if (this.searchTotal > this.offset) {
			if (this.totalPages < 6) {
				this.paginatorArray = this.allPages;
			} else {
				this.allPages.forEach((pageNumber) => {
					if (pageNumber === 1
						|| pageNumber === this.currentPage
						|| pageNumber === this.totalPages
						|| pageNumber === this.currentPage + 1
						|| pageNumber === this.currentPage - 1) {
						this.paginatorArray.push(pageNumber);
					} else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
						this.paginatorArray.push('...');
						hasFirstEllipsis = true;
					} else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
						this.paginatorArray.push('...');
						hasSecondEllipsis = true;
					}
				});
			}
		}
		this.loading = false;
	}

	async routeTo(element: any): Promise<void> {
		await this.router.navigateByUrl("/organization/" + element.ownerOrganization.id + "/generalpricelist/" + element.id);
	}

}
