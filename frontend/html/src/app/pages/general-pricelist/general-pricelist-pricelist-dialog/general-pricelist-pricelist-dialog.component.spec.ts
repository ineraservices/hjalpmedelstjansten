import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GeneralPricelistPricelistDialogComponent} from './general-pricelist-pricelist-dialog.component';

describe('GeneralPricelistPricelistDialogComponent', () => {
	let component: GeneralPricelistPricelistDialogComponent;
	let fixture: ComponentFixture<GeneralPricelistPricelistDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [GeneralPricelistPricelistDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(GeneralPricelistPricelistDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
