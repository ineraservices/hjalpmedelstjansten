import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {GeneralPricelist} from "../../../models/general-pricelist/general-pricelist.model";
import {Router} from "@angular/router";
@Component({
  selector: 'app-general-pricelist-table',
  templateUrl: './general-pricelist-table.component.html',
  styleUrls: ['./general-pricelist-table.component.scss']
})
export class GeneralPricelistTableComponent implements OnInit {

  displayedColumns = ['ownerOrganization.organizationName', 'generalPricelistName', 'generalPricelistNumber', 'validFrom', 'validTo'];

  @Input() generalPricelists: GeneralPricelist[];
  // // @Output() sortOrderChange = new EventEmitter<any>();
  @Input() sortOrder: String;
  @Input() sortType: String;
  @Output() matSortChanged = new EventEmitter<any>();
  dataSource;

  // sortResults = true;
  // sortOrder: String = 'asc';
  // sortType: String = 'name';
  // @ViewChild(MatSort) sort: MatSort

  constructor(private router: Router) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.generalPricelists);
    // console.log(this.dataSource)
    // this.dataSource.sortingDataAccessor = (item, property) => {
    //   switch(property) {
    //     case 'ownerOrganization.organizationName': return item.ownerOrganization.organizationName;
    //     default: return item[property];
    //   }
    // };
    // this.dataSource.sort = this.sort;
  }

  sortData(matSort: MatSort) {

    // this.sortResults = true;
    switch (this.sortOrder) {
      case 'asc':
        this.sortOrder = 'desc';
        break;
      case 'desc':
        this.sortOrder = 'asc';
        break;
      default: this.sortOrder = 'asc';
        break;
    }
    this.matSortChanged.emit(matSort)
  }
	async routeTo(element: any): Promise<void> {
		await this.router.navigateByUrl("/organization/" + element.ownerOrganization.id + "/generalpricelist/" + element.id);
	}

}
export interface gp {
  supplier: string;
  generalPricelistNumber: string;
  generalPricelistName: string;
  validFrom: Date;
  validTo: Date;
}
