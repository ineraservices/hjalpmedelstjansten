import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralPricelistTableComponent } from './general-pricelist-table.component';

describe('GeneralPricelistTableComponent', () => {
  let component: GeneralPricelistTableComponent;
  let fixture: ComponentFixture<GeneralPricelistTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralPricelistTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralPricelistTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
