import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ImportGeneralPricelistPricelistDialogComponent} from './import-general-pricelist-pricelist-dialog.component';

describe('ImportGeneralPricelistPricelistDialogComponent', () => {
	let component: ImportGeneralPricelistPricelistDialogComponent;
	let fixture: ComponentFixture<ImportGeneralPricelistPricelistDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ImportGeneralPricelistPricelistDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ImportGeneralPricelistPricelistDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
