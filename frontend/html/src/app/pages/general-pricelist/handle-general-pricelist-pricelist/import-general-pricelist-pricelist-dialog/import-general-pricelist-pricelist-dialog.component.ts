import {Component, Inject} from '@angular/core';
import {UploadDialogComponent} from '../../../product/upload-dialog/upload-dialog.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ErrorMessage} from '../../../../models/error-message.model';
import {AgreementService} from '../../../../services/agreement.service';
import {AlertService} from '../../../../services/alert.service';

@Component({
	selector: 'app-import-general-pricelist-pricelist-dialog',
	templateUrl: './import-general-pricelist-pricelist-dialog.component.html',
	styleUrls: ['./import-general-pricelist-pricelist-dialog.component.scss']
})
export class ImportGeneralPricelistPricelistDialogComponent {

	file;
	formData: FormData;
	errors: Array<ErrorMessage>;
	importInProgress = false;
	showImportText = false;

	constructor(public dialogRef: MatDialogRef<UploadDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private agreementService: AgreementService,
							private alertService: AlertService) {
	}

	fileSelected(file) {
		this.file = file;
	}

	import() {
		this.importInProgress = true;
		this.showImportText = false;
		this.formData = new FormData();
		this.formData.append('file', this.file);
		this.errors = new Array<ErrorMessage>();

		this.agreementService.importGeneralPricelistPricelist(this.data.organizationId, this.data.pricelistId, this.formData).subscribe(
			res => {
				this.importInProgress = false;
				this.showImportText = true;
				this.file = null;
				this.alertService.clear();
				this.alertService.success('Inläsning påbörjad - Resultatet kommer att skickas till din e-post. Status för importen hittas under fliken Inläsningar.');
			}, error => {
				this.importInProgress = false;
				this.alertService.clear();
				this.errors = new Array<ErrorMessage>();
				this.alertService.errorWithMessage('Någonting gick fel');
				error.error.errors.forEach(err => {
					this.errors.push(err);
				});
			});
	}

}
