import {Component, Inject, OnInit} from '@angular/core';
import {Article} from '../../../../models/product/article.model';
import {AuthService} from '../../../../auth/auth.service';
import {SelectionModel} from '@angular/cdk/collections';
import {HelptextService} from '../../../../services/helptext.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {AgreementService} from '../../../../services/agreement.service';
import {ActivatedRoute} from '@angular/router';
import {AlertService} from '../../../../services/alert.service';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {CommonService} from '../../../../services/common.service';

@Component({
	selector: 'app-add-general-pricelist-pricelist-row-dialog',
	templateUrl: './add-general-pricelist-pricelist-row-dialog.component.html',
	styleUrls: ['./add-general-pricelist-pricelist-row-dialog.component.scss']
})
export class AddGeneralPricelistPricelistRowDialogComponent implements OnInit {
	displayedColumns = ['add', 'typ', 'benämning', 'artikelnummer', 'status', 'kategori'];
	displayedColumns2 = ['remove', 'typ', 'benämning', 'artikelnummer', 'status', 'kategori'];

	loading = true;
	loading2 = false;
	show: boolean;
	organizationId: number;
	pricelistId: number;
	query = '';
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	queryParams = '';
	searchTotal;
	offset = 25;
	totalPages;
	totalPages2;
	currentPage = 1;
	currentPage2 = 1;
	paginatorArray = [];
	paginatorArray2 = [];
	allPages;
	allPages2;
	dataSource = null;
	articles = [];
	selection = new SelectionModel<Article>(true, []);
	pageSelection = new SelectionModel<Article>(true, []);

	dataSource2 = null;
	markedArticles = [];

	helpTexts;
	flexDirection;

	// Filters
	filterH = false;
	filterT = false;
	filterR = false;
	filterTJ = false;
	filterI = false;
	filterPublished = false;
	filterDiscontinued = false;

	constructor(public dialogRef: MatDialogRef<AddGeneralPricelistPricelistRowDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private route: ActivatedRoute,
							private authService: AuthService,
							private agreementService: AgreementService,
							private alertService: AlertService,
							private helpTextService: HelptextService,
							private commonService: CommonService) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}
		this.organizationId = data.organizationId;
		this.pricelistId = data.pricelistId;

		// The user is considered done with their search after 500 ms without further input
		this.searchChanged.pipe(
			debounceTime(500),
			distinctUntilChanged())
			.subscribe(query => {
				this.query = encodeURIComponent(query);
				this.onQueryChange();
			}
			);
	}

	ngOnInit() {
		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
			}
		);
		this.searchArticles('');
		this.dataSource2 = new MatTableDataSource<Article>();
		this.show = false;
	}

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
	}

	onQueryChange() {
		this.onFilterChange();
	}

	onFilterChange() {
		this.queryParams = '';
		this.currentPage = 1;
		if (this.filterH) {
			this.queryParams += '&type=H';
		}
		if (this.filterT) {
			this.queryParams += '&type=T';
		}
		if (this.filterR) {
			this.queryParams += '&type=R';
		}
		if (this.filterTJ) {
			this.queryParams += '&type=Tj';
		}
		if (this.filterI) {
			this.queryParams += '&type=I';
		}
		if (this.filterPublished) {
			this.queryParams += '&status=PUBLISHED';
		}
		if (this.filterDiscontinued) {
			this.queryParams += '&status=DISCONTINUED';
		}
		this.searchArticles(this.query + this.queryParams);
	}

	searchArticles(query) {
		this.loading = true;
		this.agreementService.searchArticlesForGeneralPricelistPricelist(query, this.organizationId, this.pricelistId).subscribe({
				next: res => {
					this.searchTotal = res.headers.get('X-Total-Count');
					if (this.searchTotal !== '0') {
						this.articles = res.body;
					} else {
						this.articles = [];
					}
					this.totalPages = Math.ceil(this.searchTotal / this.offset);
					this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
					this.dataSource = new MatTableDataSource<Article>(this.articles);
					this.generatePaginator();
				},
				error: error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			}
		);
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	showSelected() {
		this.show = true;
	}

	hideSelected() {
		this.show = false;
	}

	goToPage(page) {
		if (page !== '...' && page !== this.currentPage) {
			this.loading = true;
			this.currentPage = page;
			this.pageSelection.clear();
			this.agreementService.searchArticlesForGeneralPricelistPricelist(
				this.query + this.queryParams + '&offset=' +
				(this.currentPage - 1) * this.offset, this.organizationId, this.pricelistId)
				.subscribe(
					res => {
						this.articles = res.body;
						this.dataSource = new MatTableDataSource<Article>(this.articles);
						this.dataSource.data.forEach(row => {
							if (this.selection.selected.some(x => x.id === row.id)) {
								this.pageSelection.select(row);
							}
						});
						this.currentPage = page;
						this.generatePaginator();
					}, error => {
						this.alertService.clear();
						error.error.errors.forEach(err => {
							this.alertService.error(err);
						});
					}
				);
		}
	}

	goToPage2(page) {
		// this.loading2 = true;
		if (page !== '...' && page !== this.currentPage2) {
			this.currentPage2 = page;

			const offset = (this.currentPage2 - 1) * this.offset;
			const endoffset = this.currentPage2 * this.offset;

			this.dataSource2.data = this.markedArticles.slice(offset, endoffset);
			this.currentPage2 = page;
			this.generatePaginator2();
		}
	}

	generatePaginator() {
		let hasFirstEllipsis = false;
		let hasSecondEllipsis = false;
		this.paginatorArray = [];
		if (this.searchTotal > this.offset) {
			if (this.totalPages < 6) {
				this.paginatorArray = this.allPages;
			} else {
				this.allPages.forEach((pageNumber) => {
					if (pageNumber === 1
						|| pageNumber === this.currentPage
						|| pageNumber === this.totalPages
						|| pageNumber === this.currentPage + 1
						|| pageNumber === this.currentPage - 1) {
						this.paginatorArray.push(pageNumber);
					} else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
						this.paginatorArray.push('...');
						hasFirstEllipsis = true;
					} else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
						this.paginatorArray.push('...');
						hasSecondEllipsis = true;
					}
				});
			}
		}
		this.loading = false;
	}

	generatePaginator2() {
		let hasFirstEllipsis = false;
		let hasSecondEllipsis = false;
		this.paginatorArray2 = [];
		if (this.markedArticles.length > this.offset) {
			if (this.totalPages2 < 6) {
				this.paginatorArray2 = this.allPages2;
			} else {
				this.allPages2.forEach((pageNumber) => {
					if (pageNumber === 1
						|| pageNumber === this.currentPage2
						|| pageNumber === this.totalPages2
						|| pageNumber === this.currentPage2 + 1
						|| pageNumber === this.currentPage2 - 1) {
						this.paginatorArray2.push(pageNumber);
					} else if (!hasFirstEllipsis && pageNumber < this.currentPage2 && (pageNumber !== this.currentPage2 - 1)) {
						this.paginatorArray2.push('...');
						hasFirstEllipsis = true;
					} else if (!hasSecondEllipsis && pageNumber > this.currentPage2 && pageNumber !== this.currentPage2 + 1) {
						this.paginatorArray2.push('...');
						hasSecondEllipsis = true;
					}
				});
			}
		}
		this.loading2 = false;
	}

	addRow(element) {
		this.dataSource.data.forEach(row => {
			if (row.id === element.id) {
				if (!this.markedArticles.find(e => e.id === element.id)) {
					this.markedArticles.push(row);
					const offset = (this.currentPage2 - 1) * this.offset;
					const endoffset = this.currentPage2 * this.offset;
					this.dataSource2.data = this.markedArticles.slice(offset, endoffset);

					// this.dataSource2.data = this.markedArticles;
					this.dataSource2.filter = "";
				}
			}
		});
		this.totalPages2 = Math.ceil(this.markedArticles.length / this.offset);
		this.allPages2 = Array(this.totalPages2).fill(1).map((x, i) => i + 1);
		this.generatePaginator2();
	}

	addAllRows() {
		this.dataSource.data.forEach(row => {
			if (!this.markedArticles.find(e => e.id === row.id)) {
				this.markedArticles.push(row);
			}
		});
		const offset = (this.currentPage2 - 1) * this.offset;
		const endoffset = this.currentPage2 * this.offset;
		this.dataSource2.data = this.markedArticles.slice(offset, endoffset);
		// this.dataSource2.data = this.markedArticles;
		this.dataSource2.filter = "";

		this.totalPages2 = Math.ceil(this.markedArticles.length / this.offset);
		this.allPages2 = Array(this.totalPages2).fill(1).map((x, i) => i + 1);
		this.generatePaginator2();
	}

	deleteRow(element) {
		// ta bort raden från valda
		const initialPage = this.currentPage2;
		const initialNumberOfPages = this.totalPages2;
		let j = 0;

		this.markedArticles.forEach(row => {
			if (row.id === element.id) {
				this.markedArticles.splice(j, 1);
			}
			j++;
		});

		this.dataSource2.data = this.markedArticles;
		this.dataSource2.filter = "";

		this.totalPages2 = Math.ceil(this.markedArticles.length / this.offset);
		this.allPages2 = Array(this.totalPages2).fill(1).map((x, i) => i + 1);

		// eslint-disable-next-line no-debugger
		debugger;
		if (this.totalPages2 !== initialNumberOfPages) {
			this.currentPage2 = initialPage - 1;
		} else {
			this.currentPage2 = initialPage;
		}

		if (this.currentPage2 === 0) {
			this.currentPage2 = 1;
		}

		this.generatePaginator2();

		let offset = (this.currentPage2 - 1) * this.offset;
		if (offset < 0) {
			offset = 0;
		}
		this.dataSource2.data = this.markedArticles.slice(offset);

		this.generatePaginator2();

		// eslint-disable-next-line no-debugger
		debugger;
		this.show = this.markedArticles.length > 0;
		// eslint-disable-next-line no-debugger
		debugger;
	}

	deleteAllRows() {
		const initialPage = this.currentPage2;
		const initialNumberOfPages = this.totalPages2;

		let offset = (this.currentPage2 - 1) * this.offset;
		this.markedArticles.splice(offset, this.offset);

		this.dataSource2.data = this.markedArticles;
		this.dataSource2.filter = "";

		this.totalPages2 = Math.ceil(this.markedArticles.length / this.offset);
		this.allPages2 = Array(this.totalPages2).fill(1).map((x, i) => i + 1);

		// eslint-disable-next-line no-debugger
		debugger;
		if (this.totalPages2 !== initialNumberOfPages) {
			this.currentPage2 = initialPage - 1;
		} else {
			this.currentPage2 = initialPage;
		}

		if (this.currentPage2 === 0) {
			this.currentPage2 = 1;
		}


		this.generatePaginator2();

		offset = (this.currentPage2 - 1) * this.offset;
		if (offset < 0) {
			offset = 0;
		}
		this.dataSource2.data = this.markedArticles.slice(offset);

		this.generatePaginator2();
		// this.markedArticles = [];
		this.show = this.markedArticles.length > 0;
	}

	done() {
		// this.dialogRef.close(this.selection.selected);
		this.dialogRef.close(this.markedArticles);
	}
}
