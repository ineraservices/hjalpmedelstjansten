import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AddGeneralPricelistPricelistRowDialogComponent} from './add-general-pricelist-pricelist-row-dialog.component';

describe('AddGeneralPricelistPricelistRowDialogComponent', () => {
	let component: AddGeneralPricelistPricelistRowDialogComponent;
	let fixture: ComponentFixture<AddGeneralPricelistPricelistRowDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [AddGeneralPricelistPricelistRowDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(AddGeneralPricelistPricelistRowDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
