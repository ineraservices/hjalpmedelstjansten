import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ProductService} from '../../../services/product.service';
import {OrganizationService} from '../../../services/organization.service';
import {AgreementService} from '../../../services/agreement.service';
import {AlertService} from '../../../services/alert.service';
import {MatDialog} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {GuaranteeUnit} from '../../../models/agreement/guarantee-unit.model';
import {AuthService} from '../../../auth/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {GeneralPricelistPricelist} from '../../../models/general-pricelist/general-pricelist-pricelist.model';
import {GeneralPricelistPricelistrow} from '../../../models/general-pricelist/general-pricelist-pricelistrow.model';
import {Article} from '../../../models/product/article.model';
import {PreventiveMaintenance} from '../../../models/preventive-maintenance.model';
import {CommonService} from '../../../services/common.service';
import {
	ImportGeneralPricelistPricelistDialogComponent
} from './import-general-pricelist-pricelist-dialog/import-general-pricelist-pricelist-dialog.component';
import {
	AddGeneralPricelistPricelistRowDialogComponent
} from './add-general-pricelist-pricelist-row-dialog/add-general-pricelist-pricelist-row-dialog.component';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {HelptextService} from '../../../services/helptext.service';
import {saveAs} from "file-saver";

@Component({
	selector: 'app-handle-general-pricelist-pricelist',
	templateUrl: './handle-general-pricelist-pricelist.component.html',
	styleUrls: ['./handle-general-pricelist-pricelist.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class HandleGeneralPricelistPricelistComponent implements OnInit {

	displayedColumns = ['typ', 'artnr', 'benämning', 'kategori', 'beställningsenhet',
																					'utgått', 'pris', 'gäller från', 'min best', 'levtid', 'garanti(antal)', '(enhet)',
																					'(gäller från)', '(villkor)', 'status', 'inactivate'];
	loaded = false;
	loading = false;
	editMode = false;
	organizationId: number;
	token;
	ownOrganizationId: number;
	generalPricelistId: number;
	pricelistId: number;
	isCustomer: boolean;
	isServiceOwner: boolean;
	units: Array<GuaranteeUnit>;
	query = '';
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	queryParams = '';
	searchTotal;
	offset = 25;
	totalPages;
	currentPage = 1;
	paginatorArray = [];
	allPages;
	dataSource = null;
	pricelist: GeneralPricelistPricelist;
	pricelistRows = [];
	pricelistValidFrom: Date;
	warrantyValidFrom: Array<PreventiveMaintenance>;
	flexDirection: string;
	helpTexts;
	helpTextsLoaded = false;

	// Filters
	filterActive = false;
	filterDiscontinued = false;
	filterShowRowsWithPrices = false;
	filterShowRowsWithoutPrices = false;
	filterH = false;
	filterT = false;
	filterR = false;
	filterTJ = false;
	filterI = false;
	filterArticlePublished = false;
	filterArticleDiscontinued = false;

	// Permissions
	hasPermissionToViewGeneralPricelistPricelist: boolean;
	hasPermissionToUpdateGeneralPricelistPricelistRow: boolean;
	hasPermissionToInactivateGeneralPricelistPricelistRow: boolean;
	hasPermissionToExportGeneralPricelistPricelistRows: boolean;
	hasPermissionToImportGeneralPricelistPricelistRows: boolean;
	isOwnOrganization = false;

	constructor(private route: ActivatedRoute,
							private authService: AuthService,
							private productService: ProductService,
							private commonService: CommonService,
							private agreementService: AgreementService,
							private organizationService: OrganizationService,
							private alertService: AlertService,
							private dialog: MatDialog,
							private helpTextService: HelptextService,
							private router: Router) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}
		this.route.params.subscribe(params => {
			this.organizationId = params.organizationId;
			this.token = JSON.parse(localStorage.getItem('token'));

			this.ownOrganizationId = this.token.userEngagements[0].organizationId;

			this.generalPricelistId = params.id;
			this.pricelistId = params.pricelistId;
		});
		this.isOwnOrganization = Number(this.organizationId) === this.authService.getOrganizationId();
		this.hasPermissionToViewGeneralPricelistPricelist = this.hasPermissionToViewGeneralPricelistPricelist && this.isOwnOrganization;
		this.hasPermissionToUpdateGeneralPricelistPricelistRow = this.hasPermissionToUpdateGeneralPricelistPricelistRow && this.isOwnOrganization;
		this.hasPermissionToInactivateGeneralPricelistPricelistRow = this.hasPermissionToInactivateGeneralPricelistPricelistRow && this.isOwnOrganization;

		// The user is considered done with their search after 500 ms without further input
		this.searchChanged.pipe(
			debounceTime(500),
			distinctUntilChanged())
			.subscribe(query => {
				this.query = encodeURIComponent(query);
				this.onQueryChange();
			}
			);
	}

	ngOnInit() {
		this.hasPermissionToViewGeneralPricelistPricelist = this.hasPermission('generalpricelist_pricelist:view_own');
		this.hasPermissionToUpdateGeneralPricelistPricelistRow = this.hasPermission('generalpricelist_pricelistrow:update_own');
		this.hasPermissionToInactivateGeneralPricelistPricelistRow = this.hasPermission('generalpricelist_pricelistrow:inactivate_own');
		this.hasPermissionToExportGeneralPricelistPricelistRows = this.hasPermission('generalpricelistpricelistexport:view');
		this.hasPermissionToImportGeneralPricelistPricelistRows = this.hasPermission('generalpricelistpricelistimport:view');

		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);
		this.organizationService.getOrganization(this.authService.getOrganizationId()).subscribe(
			res => {
				this.isCustomer = res.organizationType === 'CUSTOMER';
				this.isServiceOwner = res.organizationType === 'SERVICE_OWNER';
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
		this.productService.getGuaranteeUnits().subscribe(
			units => {
				this.units = units;
				this.commonService.getPreventiveMaintenances().subscribe(
					preventiveMaintenances => {
						this.warrantyValidFrom = preventiveMaintenances;
						this.agreementService.getGeneralPricelistPriceList(this.organizationId, this.pricelistId).subscribe(
							pricelist => {
								this.pricelist = pricelist;
								this.convertToDateString();
								this.getPricelistRows('');
							}, error => {
								this.alertService.clear();
								error.error.errors.forEach(err => {
									this.alertService.error(err);
								});
							}
						);
					}
				);
			}
		);
	}

	toggleEditMode() {
		this.editMode = !this.editMode;

		if (!this.editMode) {
			// this.displayedColumns.push('anchor'); // works wrong by not showing the table with results
			this.getPricelistRows(this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset);
		} else {
			// this.displayedColumns.splice(-1, 1);
		}
	}

	exportSearchResults() {
		const query = '?query=' + this.query + this.queryParams + '&ownOrganizationUniqueId=' + this.ownOrganizationId;

		this.productService.exportGeneralPricelistPricelistRows(this.organizationId, this.pricelistId, query).subscribe(
			data => {
				saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
	}

	onQueryChange() {
		this.onFilterChange();
	}

	onFilterChange() {
		this.queryParams = '';
		this.currentPage = 1;
		if (this.filterActive) {
			this.queryParams += '&status=ACTIVE';
		}
		if (this.filterDiscontinued) {
			this.queryParams += '&status=INACTIVE';
		}
		if (this.filterShowRowsWithPrices) {
			this.queryParams += '&showRowsWithPrices=true';
		} else {
			this.queryParams += '&showRowsWithPrices=false';
		}
		if (this.filterShowRowsWithoutPrices) {
			this.queryParams += '&showRowsWithoutPrices=true';
		} else {
			this.queryParams += '&showRowsWithoutPrices=false';
		}
		if (this.filterH) {
			this.queryParams += '&type=H';
		}
		if (this.filterT) {
			this.queryParams += '&type=T';
		}
		if (this.filterR) {
			this.queryParams += '&type=R';
		}
		if (this.filterTJ) {
			this.queryParams += '&type=Tj';
		}
		if (this.filterI) {
			this.queryParams += '&type=I';
		}
		if (this.filterArticlePublished) {
			this.queryParams += '&articleStatus=PUBLISHED';
		}
		if (this.filterArticleDiscontinued) {
			this.queryParams += '&articleStatus=DISCONTINUED';
		}
		this.getPricelistRows(this.query + this.queryParams);
	}

	exportPricelistOnlyDiscontinued() {
		window.location.href = this.agreementService.getExportGeneralPricelistPricelistUrl(
			this.organizationId, this.pricelistId, 'discontinued');
	}

	exportPricelistAll() {
		window.location.href = this.agreementService.getExportGeneralPricelistPricelistUrl(
			this.organizationId, this.pricelistId, 'all');
	}

	exportPricelistOnlyInProduction() {
		window.location.href = this.agreementService.getExportGeneralPricelistPricelistUrl(
			this.organizationId, this.pricelistId, 'inProduction');
	}

	openImportPricelistDialog() {
		const dialogRef = this.dialog.open(ImportGeneralPricelistPricelistDialogComponent, {
			width: '80%',
			data: {
				'organizationId': this.organizationId,
				'pricelistId': this.pricelistId
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.getPricelistRows(this.query + this.queryParams);
			}
		});
	}

	/* exportPricelistRows() {
	 window.location.href = this.agreementService.getExportPricelistRowsUrl(this.organizationId, this.agreementId, this.pricelistId) + '?query=' + this.query + this.queryParams;
 }*/

	getPricelistRows(query) {
		this.loading = true;
		this.agreementService.getGeneralPricelistPricelistRows(this.organizationId, this.pricelistId, query).subscribe(
			res => {
				this.searchTotal = res.headers.get('X-Total-Count');
				if (this.searchTotal !== '0') {
					this.pricelistRows = res.body;
				} else {
					this.pricelistRows = [];
				}
				this.totalPages = Math.ceil(this.searchTotal / this.offset);
				this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
				// Extract units
				this.pricelistRows.forEach((row) => {
					if (row.warrantyQuantityUnit) {
						row.warrantyQuantityUnit = this.units.find(x => x.id === row.warrantyQuantityUnit.id);
					} else {
						row.warrantyQuantityUnit = new GuaranteeUnit();
					}
					if (row.warrantyValidFrom) {
						row.warrantyValidFrom = this.warrantyValidFrom.find(x => x.id === row.warrantyValidFrom.id);
					} else {
						row.warrantyValidFrom = new PreventiveMaintenance();
					}
					if (!row.warrantyTerms) {
						row.warrantyTerms = '';
					}

					row.validFrom = new Date(row.validFrom);
				});
				this.dataSource = new MatTableDataSource<GeneralPricelistPricelistrow>(this.pricelistRows);
				this.generatePaginator();
				this.loaded = true;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	getCategoryCodeForCodelessArticle(article) {
		let code = article.category.code;

		if (code === null) {
			if (article.fitsToProducts !== null && article.fitsToProducts.length > 0) {
				code = article.fitsToProducts[0].category.code;
			}
		}

		if (code === null) {
			if (article.fitsToArticles !== null && article.fitsToArticles.length > 0) {
				if (article.fitsToArticles[0].basedOnProduct !== null) {
					code = article.fitsToArticles[0].basedOnProduct.category.code;
					// eslint-disable-next-line brace-style
				}

				// kan tänkas att detta ska med? PA 2020-10-05
				else {
					code = article.fitsToArticles[0].category.code;
				}
			}
		}
		if (code === null) {
			if (article.fitsToArticles !== null && article.fitsToArticles.length > 0) {
				if (article.fitsToArticles[0].fitsToProducts != null && article.fitsToArticles[0].fitsToProducts.length > 0) {
					code = article.fitsToArticles[0].fitsToProducts[0].category.code;
				}
			}
		}

		return code;
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	goToPage(page) {
		if (page !== '...' && page !== this.currentPage) {
			this.loading = true;
			this.currentPage = page;
			this.agreementService.getGeneralPricelistPricelistRows(
				this.organizationId, this.pricelistId,
				this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset)
				.subscribe(
					res => {
						this.pricelistRows = res.body;
						// Extract units dates and selections
						this.pricelistRows.forEach((row) => {
							if (row.warrantyQuantityUnit) {
								row.warrantyQuantityUnit = this.units.find(x => x.id === row.warrantyQuantityUnit.id);
							} else {
								row.warrantyQuantityUnit = new GuaranteeUnit();
							}
							if (row.warrantyValidFrom) {
								row.warrantyValidFrom = this.warrantyValidFrom.find(x => x.id === row.warrantyValidFrom.id);
							} else {
								row.warrantyValidFrom = new PreventiveMaintenance();
							}
							if (!row.warrantyTerms) {
								row.warrantyTerms = '';
							}
							row.validFrom = new Date(row.validFrom);
						});
						this.dataSource = new MatTableDataSource<GeneralPricelistPricelistrow>(this.pricelistRows);
						this.currentPage = page;
						this.generatePaginator();
					}, error => {
						this.alertService.clear();
						error.error.errors.forEach(err => {
							this.alertService.error(err);
						});
					}
				);
		}
	}

	generatePaginator() {
		let hasFirstEllipsis = false;
		let hasSecondEllipsis = false;
		this.paginatorArray = [];
		if (this.searchTotal > this.offset) {
			if (this.totalPages < 6) {
				this.paginatorArray = this.allPages;
			} else {
				this.allPages.forEach((pageNumber) => {
					if (pageNumber === 1
						|| pageNumber === this.currentPage
						|| pageNumber === this.totalPages
						|| pageNumber === this.currentPage + 1
						|| pageNumber === this.currentPage - 1) {
						this.paginatorArray.push(pageNumber);
					} else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
						this.paginatorArray.push('...');
						hasFirstEllipsis = true;
					} else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
						this.paginatorArray.push('...');
						hasSecondEllipsis = true;
					}
				});
			}
		}
		this.loading = false;
	}

	convertToDateString() {
		this.pricelistValidFrom = new Date(this.pricelist.validFrom);
	}

	getMillisecondsFromDate(date) {
		if (date) {
			return date.getTime();
		}
		return null;
	}

	openAddPricelistRowDialog() {
		const dialogRef = this.dialog.open(AddGeneralPricelistPricelistRowDialogComponent, {
			width: '80%',
			data: {
				'organizationId': this.organizationId,
				'pricelistId': this.pricelistId
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				const pricelistRows = new Array<GeneralPricelistPricelistrow>();
				result.forEach(article => {
					const tmpArticle = new Article();
					const pricelistRow = new GeneralPricelistPricelistrow();
					pricelistRow.status = 'ACTIVE';
					pricelistRow.leastOrderQuantity = 1;
					tmpArticle.id = article.id;
					pricelistRow.article = tmpArticle;
					pricelistRows.push(pricelistRow);
				});
				this.agreementService.createGeneralPricelistPricelistRows(this.organizationId, this.pricelistId, pricelistRows).subscribe(
					res => {
						this.onFilterChange();
						this.alertService.success('Sparat');
					}, error => {
						this.alertService.clear();
						error.error.errors.forEach(err => {
							this.alertService.error(err);
						});
					}
				);
			}
		});
	}

	updatePricelistRow(row) {
		if (row.validFrom) {
			row.validFrom = this.getMillisecondsFromDate(row.validFrom);
		}
		if (row.warrantyValidFrom && !row.warrantyValidFrom.id) {
			row.warrantyValidFrom = null;
		}
		this.agreementService.updateGeneralPricelistPricelistRow(this.organizationId, this.pricelistId, row.id, row).subscribe(
			res => {
				if (row.validFrom) {
					row.validFrom = new Date(row.validFrom);
				}
				this.alertService.clear();
				this.alertService.success('Sparat');
			}, error => {
				if (row.validFrom) {
					row.validFrom = new Date(row.validFrom);
				}
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	inactivatePrice(row) {

		this.agreementService.deleteGeneralPricelistPriceListRow(this.organizationId, this.pricelistId, row.id).subscribe(
			res => {
				this.alertService.clear();
				this.getPricelistRows(this.query + this.queryParams);
				this.alertService.success('Tog bort artikeln \"' + row.article.articleName + '\" från generell prislista');
			}, error => {
				if (row.validFrom) {
					row.validFrom = new Date(row.validFrom);
				}
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

	}

	async routeTo(element: any): Promise<void> {
		if (!this.editMode) {
			await this.router.navigateByUrl("/organization/" + element.article.organizationId + "/article/" + element.article.id + "/handle?search=true");
		}
	}

}
