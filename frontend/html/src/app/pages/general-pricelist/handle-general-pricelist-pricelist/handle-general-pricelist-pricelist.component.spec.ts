import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HandleGeneralPricelistPricelistComponent} from './handle-general-pricelist-pricelist.component';

describe('HandleGeneralPricelistPricelistComponent', () => {
	let component: HandleGeneralPricelistPricelistComponent;
	let fixture: ComponentFixture<HandleGeneralPricelistPricelistComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [HandleGeneralPricelistPricelistComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(HandleGeneralPricelistPricelistComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
