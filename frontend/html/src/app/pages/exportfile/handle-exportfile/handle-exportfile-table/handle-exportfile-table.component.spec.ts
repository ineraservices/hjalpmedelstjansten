import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HandleExportfileTableComponent} from './handle-exportfile-table.component';

describe('HandleExportfileTableComponent', () => {
	let component: HandleExportfileTableComponent;
	let fixture: ComponentFixture<HandleExportfileTableComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [HandleExportfileTableComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(HandleExportfileTableComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
