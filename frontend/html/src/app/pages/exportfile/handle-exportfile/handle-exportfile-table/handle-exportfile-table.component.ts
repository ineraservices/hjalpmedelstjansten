import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {GeneralPricelist} from "../../../../models/general-pricelist/general-pricelist.model";


@Component({
	selector: 'app-handle-exportfile-table',
	templateUrl: './handle-exportfile-table.component.html',
	styleUrls: ['./handle-exportfile-table.component.scss']
})
export class HandleExportfileTableComponent implements OnInit {

	@ViewChild(MatSort) sort: MatSort;

	@Input() generalPricelists: GeneralPricelist[];
	@Output() removeGP = new EventEmitter<any>();

	displayedColumns = ['ownerOrganization.organizationName', 'generalPricelistNumber', 'validFrom', 'validTo', 'delete'];

	dataSource;

	constructor() {
	}

	ngOnInit() {
		this.dataSource = new MatTableDataSource(this.generalPricelists);
		this.dataSource.sortingDataAccessor = (item, property) => {
			switch (property) {
			case 'ownerOrganization.organizationName':
				return item.ownerOrganization.organizationName;
			default:
				return item[property];
			}
		};
		this.dataSource.sort = this.sort;
	}

	removeFromExportfile(gp) {
		this.removeGP.emit(gp);
	}
}

export interface gp {
	supplier: string;
	generalPricelistNumber: string;
	validFrom: Date;
	validTo: Date;
}
