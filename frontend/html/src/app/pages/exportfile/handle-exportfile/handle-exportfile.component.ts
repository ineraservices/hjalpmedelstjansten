import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {GpDialogComponent} from '../gp-dialog/gp-dialog.component';
import {ActivatedRoute} from '@angular/router';
import {ExportService} from '../../../services/export.service';
import {ExportSetting} from '../../../models/export-setting.model';
import {RemoveGpDialogComponent} from './remove-gp-dialog/remove-gp-dialog.component';
import {AlertService} from '../../../services/alert.service';
import {CommonService} from '../../../services/common.service';
import {HelptextService} from "../../../services/helptext.service";
import {saveAs} from "file-saver";

@Component({
	selector: 'app-handle-exportfile',
	templateUrl: './handle-exportfile.component.html',
	styleUrls: ['./handle-exportfile.component.scss']
})
export class HandleExportfileComponent implements OnInit {

	loading = false;
	organizationId: number;
	exportSettingId: number;
	exportSetting: ExportSetting;
	GPs;
	flexDirection: string;
	helpTexts;
	helpTextsLoaded = false;

	constructor(private exportService: ExportService,
							private route: ActivatedRoute,
							private dialog: MatDialog,
							private alertService: AlertService,
							private commonService: CommonService,
							private helpTextService: HelptextService) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}
		this.route.params.subscribe(params => {
			this.organizationId = params.organizationId;
			this.exportSettingId = params.exportSettingId;
		});
	}

	ngOnInit() {
		this.getExportSettings();
		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);
	}

	getExportSettings() {
		this.loading = true;
		this.exportService.getExportSettingForOrganization(this.organizationId, this.exportSettingId).subscribe(
			res => {
				this.exportSetting = res;
				this.GPs = res.generalPricelists;
				this.loading = false;
			}, error => {
				this.loading = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	exportGeneralPricelists() {
		this.exportService.exportGeneralPricelists(this.organizationId, this.exportSettingId).subscribe(
			data => {
				saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	removeGP(gp) {
		const dialogRef = this.dialog.open(RemoveGpDialogComponent, {
			width: '40%',
			data: {'organizationId': this.organizationId, 'exportSettingId': this.exportSettingId, 'gp': gp}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.getExportSettings();
			}
		});
	}

	openAddGPDialog() {
		const dialogRef = this.dialog.open(GpDialogComponent, {
			width: '90%',
			data: {'organizationId': this.organizationId, 'exportSettingsId': this.exportSettingId}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.getExportSettings();
			}
		});
	}


}
