import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RemoveGpDialogComponent} from './remove-gp-dialog.component';

describe('RemoveGpDialogComponent', () => {
	let component: RemoveGpDialogComponent;
	let fixture: ComponentFixture<RemoveGpDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [RemoveGpDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(RemoveGpDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
