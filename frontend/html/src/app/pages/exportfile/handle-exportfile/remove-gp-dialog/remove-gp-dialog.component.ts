import {Component, Inject} from '@angular/core';
import {AlertService} from '../../../../services/alert.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UploadDialogComponent} from '../../../product/upload-dialog/upload-dialog.component';
import {ExportService} from '../../../../services/export.service';
import {GeneralPricelist} from '../../../../models/general-pricelist/general-pricelist.model';

@Component({
	selector: 'app-remove-gp-dialog',
	templateUrl: './remove-gp-dialog.component.html',
	styleUrls: ['./remove-gp-dialog.component.scss']
})
export class RemoveGpDialogComponent {
	saving = false;
	GPs: Array<GeneralPricelist>;

	constructor(public dialogRef: MatDialogRef<UploadDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private exportService: ExportService,
							private alertService: AlertService) {
	}

	done() {
		this.saving = true;
		this.GPs = new Array<GeneralPricelist>();
		this.GPs.push(this.data.gp);
		this.exportService.removeGPFromExportSetting(this.data.organizationId, this.data.exportSettingId, this.GPs).subscribe(
			res => {
				this.alertService.clear();
				this.saving = false;
				this.alertService.success('Sparat');
				this.dialogRef.close(res);
			}, error => {
				this.saving = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			});
	}
}
