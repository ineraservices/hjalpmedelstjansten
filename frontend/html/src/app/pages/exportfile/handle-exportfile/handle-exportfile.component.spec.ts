import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HandleExportfileComponent} from './handle-exportfile.component';

describe('HandleExportfileComponent', () => {
	let component: HandleExportfileComponent;
	let fixture: ComponentFixture<HandleExportfileComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [HandleExportfileComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(HandleExportfileComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
