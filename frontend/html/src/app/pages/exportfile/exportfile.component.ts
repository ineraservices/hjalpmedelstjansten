import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ExportSetting} from '../../models/export-setting.model';
import {ExportService} from '../../services/export.service';
import {ActivatedRoute} from '@angular/router';
import {AlertService} from '../../services/alert.service';
import {CommonService} from '../../services/common.service';

@Component({
	selector: 'app-exportfile',
	templateUrl: './exportfile.component.html',
	styleUrls: ['./exportfile.component.scss']
})
export class ExportfileComponent implements OnInit {

	displayedColumns = ['kund', 'filnamn', 'verksamhetsområde', 'senast levererad'];

	organizationId: number;
	exportSettings: Array<ExportSetting> = new Array<ExportSetting>();
	dataSource = null;
	flexDirection: string;

	constructor(private exportService: ExportService,
							private route: ActivatedRoute,
							private alertService: AlertService,
							private commonService: CommonService) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}
		this.route.params.subscribe(params => {
			this.organizationId = params.organizationId;
		});
	}

	ngOnInit() {
		this.getExportSettings();
	}

	getExportSettings() {
		this.exportService.getExportSettingsForOrganization(this.organizationId).subscribe(
			res => {
				this.exportSettings = res;
				this.dataSource = new MatTableDataSource<ExportSetting>(this.exportSettings);
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

}
