import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {ExportService} from '../../../services/export.service';
import {ExportSettingDialogComponent} from './export-setting-dialog/export-setting-dialog.component';
import {ExportSetting} from '../../../models/export-setting.model';
import {AlertService} from '../../../services/alert.service';
import {CommonService} from '../../../services/common.service';

@Component({
	selector: 'app-exportsetting',
	templateUrl: './exportsetting.component.html',
	styleUrls: ['./exportsetting.component.scss']
})
export class ExportsettingComponent implements OnInit {

	displayedColumns = ['kund', 'filnamn', 'verksamhetsområde', 'senast levererad', 'aktiv', 'edit', 'start'];

	loading = false;
	exportSettings = [];
	dataSource = null;
	searchTotal;
	offset = 25;
	totalPages;
	currentPage = 1;
	paginatorArray = [];
	allPages;
	flexDirection: string;

	constructor(private exportService: ExportService,
							private dialog: MatDialog,
							private alertService: AlertService,
							private commonService: CommonService) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}
	}

	ngOnInit() {
		this.getExportSettings();
	}

	getExportSettings() {
		this.loading = true;
		this.exportService.getExportSettings().subscribe(
			res => {
				this.exportSettings = res;
				this.dataSource = new MatTableDataSource<ExportSetting>(this.exportSettings);
				this.loading = false;
			}, error => {
				this.alertService.clear();
				this.loading = false;
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	editExportSettings(element) {
		const dialogRef = this.dialog.open(ExportSettingDialogComponent, {
			width: '90%',
			data: {
				'exportSettings': element
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.getExportSettings();
			}
		});
	}

	openAddExportSettingDialog() {
		const dialogRef = this.dialog.open(ExportSettingDialogComponent, {
			width: '90%'
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.getExportSettings();
			}
		});
	}

	startExport(element) {
		this.exportService.queueExport(element).subscribe(
			res => {
				this.alertService.success('Lagt till ' + res.organization.organizationName + ' (' + res.filename + ') i exportkön och kommer att köras inom 30 min');
			}, error => {

				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

	}
}
