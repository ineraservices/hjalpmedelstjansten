import {Component, Inject, OnInit} from '@angular/core';
import {ConnectionDialogComponent} from '../../../product/connection-dialog/connection-dialog.component';
import {AlertService} from '../../../../services/alert.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ExportService} from '../../../../services/export.service';
import {OrganizationService} from '../../../../services/organization.service';
import {Organization} from '../../../../models/organization/organization.model';
import {BusinessLevel} from '../../../../models/organization/business-level.model';
import {ExportSetting} from '../../../../models/export-setting.model';

@Component({
	selector: 'app-export-setting-dialog',
	templateUrl: './export-setting-dialog.component.html',
	styleUrls: ['./export-setting-dialog.component.scss']
})
export class ExportSettingDialogComponent implements OnInit {
	loading = true;
	saving = false;
	enabled = true;
	customers: Array<Organization>;
	selectedCustomer: Organization;
	filename: string;
	businessLevels: Array<BusinessLevel>;
	selectedBusinessLevel: BusinessLevel;
	exportSetting: ExportSetting = new ExportSetting();

	constructor(public dialogRef: MatDialogRef<ConnectionDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private organizationService: OrganizationService,
							private exportService: ExportService,
							private alertService: AlertService) {
	}

	ngOnInit() {
		this.organizationService.searchOrganizations('&type=CUSTOMER&limit=0').subscribe(
			res => {
				this.customers = res.body;
				if (this.data) {
					this.enabled = this.data.exportSettings.enabled;
					this.filename = this.data.exportSettings.filename;
					this.selectedCustomer = this.customers.find(x => x.id === this.data.exportSettings.organization.id);
					if (this.data.exportSettings.businessLevel) {
						this.organizationService.getBusinessLevels(this.selectedCustomer.id).subscribe(
							businessLevels => {
								this.businessLevels = businessLevels;
								this.selectedBusinessLevel = this.businessLevels.find(x => x.id === this.data.exportSettings.businessLevel.id);
								this.loading = false;
							}, error => {
								this.alertService.clear();
								this.loading = false;
								error.error.errors.forEach(err => {
									this.alertService.error(err);
								});
							}
						);
					} else {
						this.loading = false;
					}
				} else {
					this.loading = false;
				}
			}, error => {
				this.alertService.clear();
				this.loading = false;
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	onCustomerSelected() {
		this.businessLevels = null;
		this.selectedBusinessLevel = null;
		this.organizationService.getBusinessLevels(this.selectedCustomer.id).subscribe(
			res => {
				this.businessLevels = res;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	done() {
		this.saving = true;
		this.exportSetting.enabled = this.enabled;
		this.exportSetting.filename = this.filename;
		this.exportSetting.organization = this.selectedCustomer;
		if (this.selectedBusinessLevel) {
			this.exportSetting.businessLevel = this.selectedBusinessLevel;
		}
		if (!this.data) {
			this.exportService.createExportSetting(this.exportSetting).subscribe(
				res => {
					this.saving = false;
					this.alertService.success('Sparat');
					this.dialogRef.close(res);
				}, error => {
					this.alertService.clear();
					this.saving = false;
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		} else {
			this.exportService.updateExportSetting(this.data.exportSettings.id, this.exportSetting).subscribe(
				res => {
					this.saving = false;
					this.alertService.success('Sparat');
					this.dialogRef.close(res);
				}, error => {
					this.alertService.clear();
					this.saving = false;
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		}
	}

}
