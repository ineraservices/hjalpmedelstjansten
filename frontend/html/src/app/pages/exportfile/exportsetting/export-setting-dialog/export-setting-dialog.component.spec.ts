import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ExportSettingDialogComponent} from './export-setting-dialog.component';

describe('ExportSettingDialogComponent', () => {
	let component: ExportSettingDialogComponent;
	let fixture: ComponentFixture<ExportSettingDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ExportSettingDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ExportSettingDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
