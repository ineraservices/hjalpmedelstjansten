import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ExportsettingComponent} from './exportsetting.component';

describe('ExportsettingComponent', () => {
	let component: ExportsettingComponent;
	let fixture: ComponentFixture<ExportsettingComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ExportsettingComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ExportsettingComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
