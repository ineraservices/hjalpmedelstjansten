import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GpDialogComponent} from './gp-dialog.component';

describe('GpDialogComponent', () => {
	let component: GpDialogComponent;
	let fixture: ComponentFixture<GpDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [GpDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(GpDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
