import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {ConnectionDialogComponent} from '../../product/connection-dialog/connection-dialog.component';
import {SelectionModel} from '@angular/cdk/collections';
import {GeneralPricelist} from '../../../models/general-pricelist/general-pricelist.model';
import {AlertService} from '../../../services/alert.service';
import {ExportService} from '../../../services/export.service';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

@Component({
	selector: 'app-gp-dialog',
	templateUrl: './gp-dialog.component.html',
	styleUrls: ['./gp-dialog.component.scss']
})
export class GpDialogComponent implements OnInit {
	displayedColumns = ['checkbox', 'leverantör', 'avtalsnummer', 'giltigt from', 'giltigt tom', 'aktuell prislista'];

	loading = true;
	saving = false;
	organizationId: number;
	GPs = [];
	dataSource = null;
	query = '';
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	searchTotal;
	offset = 25;
	totalPages;
	currentPage = 1;
	paginatorArray = [];
	allPages;
	selection = new SelectionModel<GeneralPricelist>(true, []);
	tmpSelection = [];

	constructor(public dialogRef: MatDialogRef<ConnectionDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private exportService: ExportService,
							private alertService: AlertService) {
		// The user is considered done with their search after 500 ms without further input
		this.searchChanged.pipe(
			debounceTime(500),
			distinctUntilChanged())
			.subscribe(query => {
				this.query = encodeURIComponent(query);
				this.onQueryChange();
			}
			);
	}

	ngOnInit() {
		this.getGP();
	}

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
	}

	onQueryChange() {
		this.getGP();
	}

	getGP() {
		this.loading = true;
		this.exportService.getAllUnselectedGP(this.data.organizationId, this.data.exportSettingsId, this.query).subscribe(
			res => {
				this.searchTotal = res.headers.get('X-Total-Count');
				if (this.searchTotal !== '0') {
					this.GPs = res.body;
				} else {
					this.GPs = [];
				}
				this.GPs = res.body;
				this.dataSource = new MatTableDataSource<GeneralPricelist>(this.GPs);
				this.totalPages = Math.ceil(this.searchTotal / this.offset);
				this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
				this.generatePaginator();
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	/** Whether the number of selected elements matches the total number of selectable rows. */
	isAllSelected() {
		let result = true;
		this.dataSource.data.forEach(item => {
			const rowChecked = this.selection.selected.some(function (el) {
				return (el.id === item.id);
			});
			if (!rowChecked) {
				result = false;
			}
		});
		return result;
	}

	/** Selects all selectable rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.dataSource.data.forEach(row => this.selection.select(row));
		}
	}

	goToPage(page) {
		if (page !== '...' && page !== this.currentPage) {
			this.loading = true;
			this.tmpSelection = this.selection.selected;
			this.currentPage = page;
			this.exportService.getAllGP(
				this.query + '&offset=' + (this.currentPage - 1) * this.offset).subscribe(
				res => {
					this.GPs = res.body;
					this.GPs.forEach(row => {
						this.tmpSelection.forEach(tmpRow => {
							if (tmpRow.id === row.id) {
								this.selection.deselect(this.selection.selected.find(x => x.id === row.id));
								this.selection.select(row);
							}
						});
					});
					this.dataSource = new MatTableDataSource<GeneralPricelist>(this.GPs);
					this.currentPage = page;
					this.generatePaginator();
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		}
	}

	generatePaginator() {
		let hasFirstEllipsis = false;
		let hasSecondEllipsis = false;
		this.paginatorArray = [];
		if (this.searchTotal > this.offset) {
			if (this.totalPages < 6) {
				this.paginatorArray = this.allPages;
			} else {
				this.allPages.forEach((pageNumber) => {
					if (pageNumber === 1
						|| pageNumber === this.currentPage
						|| pageNumber === this.totalPages
						|| pageNumber === this.currentPage + 1
						|| pageNumber === this.currentPage - 1) {
						this.paginatorArray.push(pageNumber);
					} else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
						this.paginatorArray.push('...');
						hasFirstEllipsis = true;
					} else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
						this.paginatorArray.push('...');
						hasSecondEllipsis = true;
					}
				});
			}
		}
		this.loading = false;
	}

	done() {
		this.saving = true;
		this.exportService.addGPToExportSetting(this.data.organizationId, this.data.exportSettingsId, this.selection.selected).subscribe(
			res => {
				this.saving = false;
				this.alertService.success('Sparat');
				this.dialogRef.close(res);
			}, error => {
				this.saving = false;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

}
