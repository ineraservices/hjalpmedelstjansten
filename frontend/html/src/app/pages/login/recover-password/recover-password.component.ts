import {Component} from '@angular/core';
import {LoginService} from '../../../services/login.service';
import {Login} from '../../../models/login.model';
import {AlertService} from '../../../services/alert.service';
import {Router} from '@angular/router';

@Component({
	selector: 'app-recover',
	templateUrl: './recover-password.component.html',
	styleUrls: ['./recover-password.component.scss']
})
export class RecoverComponent {

	username: string;
	email: string;
	userData: Login = new Login();

	constructor(private loginService: LoginService,
							private alertService: AlertService,
							private router: Router) {
	}

	sendRecoverLink() {
		this.userData.username = this.username;
		this.userData.email = this.email;
		this.loginService.requestPassword(this.userData).subscribe(
			data => {
				this.router.navigate(['/'], {queryParams: {passwordRequested: true}});
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				}
				);
			}
		);
	}

}
