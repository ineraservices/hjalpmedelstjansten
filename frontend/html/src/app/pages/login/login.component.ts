import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {LoginService} from '../../services/login.service';
import {Login} from '../../models/login.model';
import {AlertService} from '../../services/alert.service';
import {AuthService} from '../../auth/auth.service';
import {UserService} from '../../services/user.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	login: Login = new Login();
	passwordCreated = false;
	passwordRequested = false;

	constructor(private loginService: LoginService,
							private alertService: AlertService,
							private router: Router,
							private authService: AuthService,
							private userService: UserService,
							private route: ActivatedRoute) {
		this.authService.clearLocalStorage();
		this.route.queryParams.subscribe(params => {
			this.passwordCreated = params.passwordCreated;
			this.passwordRequested = params.passwordRequested;
		});
	}

	ngOnInit() {
		if (this.passwordCreated) {
			this.alertService.success('Lösenordet skapat.');
		}
		if (this.passwordRequested) {
			this.alertService.success('Återställningslänk har skickats till angiven E-post');
		}
	}

	loginUser() {
		if (!this.login.username || !this.login.password) {
			return null;
		}

		this.loginService.login(this.login).subscribe(
			data => {
				this.getProfile();
			}, error => {
				this.alertService.clear();
				if (error.status === 403) {
					this.alertService.errorWithMessage('Felaktigt angivet användarnamn eller lösenord');
				} else {
					this.alertService.errorWithMessage('Någonting gick fel');
				}
			}
		);
	}

	getProfile() {
		this.userService.getProfile().subscribe(
			profile => {
				this.authService.login(profile);
				if (profile.previousLoginFound) {
					this.router.navigate(['start']);
				} else {
					this.router.navigate(['profile']);
				}
			}
		);
	}

}
