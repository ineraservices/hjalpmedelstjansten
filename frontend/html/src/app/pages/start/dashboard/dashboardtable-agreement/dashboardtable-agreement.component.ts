import {Component, Input, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Router} from "@angular/router";

@Component({
	selector: 'app-dashboard-table-agreement',
	templateUrl: './dashboardtable-agreement.component.html',
	styleUrls: ['./dashboardtable-agreement.component.scss']
})
export class DashboardtableAgreementComponent implements OnInit {

	@Input() agreements: any;
	@Input() title: string;
	@Input() userRole: string;
	@Input() organizationId: any;

	displayedColumns = ['datum', 'avtalsnamn', 'avtalsnummer', 'leverantörellerkundOrganisation', 'verksamhetsnivå'];
	dataSource = new MatTableDataSource();

	constructor(private router: Router) {

	}

	ngOnInit() {

		this.dataSource = this.agreements.map((item) => ({
			agreementId: item[0],
			agreementName: item[1],
			agreementNumber: item[2],
			organizationId: item[3],
			organizationName: item[4],
			businessLevelName: item[5],
			businessLevelId: item[6],
			sendReminderOn: item[7]
		}));
	}

	async routeTo(element: any): Promise<void> {
		await this.router.navigateByUrl("/organization/" + this.organizationId + "/agreement/" + element.agreementId + "/handle");
	}

}

export interface priceliststable {
	reminderDate: Date;
	agreementName: string;
	agreementNumber: string;
	supplierorcustomerOrganization: string;
	businessLevel: string;

}
