import {Component, Input, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {ProductService} from "../../../../services/product.service";
import {AuthService} from "../../../../auth/auth.service";
import {Router} from "@angular/router";

@Component({
	selector: 'app-dashboard-table-articles',
	templateUrl: './dashboardtable-articles.component.html',
	styleUrls: ['./dashboardtable-articles.component.scss']
})
export class DashboardtableArticlesComponent implements OnInit {
	@Input() changedArticles: any;
	@Input() title: string;
	@Input() userRole: string;
	@Input() organizationId: any;

	displayedColumns = ['ändringsdatum', 'artikelnummer', 'leverantör', 'artikelnamn', 'beställningsenhet', 'ce-märkning', 'ce-direktiv', 'ce-standard', 'utgångsdatum', 'ersättningsartikel', 'baseradpå', 'iso', 'articleMenu'];
	dataSource = new MatTableDataSource();

	constructor(private productService: ProductService, private authService: AuthService, private router: Router) {

	}

	ngOnInit() {
		this.changedArticles = this.changedArticles.map((item) => {
			if (item.replacementArticles != null) {
				if (item.replacementArticles.length !== 0) {
					const replacementArticlesArray = item.replacementArticles.split(',');
					item.replacementArticles = replacementArticlesArray;
				} else {
					item.replacementArticles = null;
				}
			}
			return item;
		});

		this.dataSource = this.changedArticles.sort((a, b) => b.dateChanged - a.dateChanged);
	}

	removeArticleNotice(organizationId, articleId, removeAll: boolean) {
		const userId = this.authService.getUserId();
		this.productService.removeArticleChangedNotice(organizationId, articleId, userId, removeAll).subscribe();
		//this.router.routeReuseStrategy.shouldReuseRoute = () => false;
		//this.router.onSameUrlNavigation = 'reload';
		//this.router.navigate(['/dashboard']);
		location.reload();

	}


}

export interface changedarticlesstable {
	dateChanged: Date;
	articleName: string;
	articleNumber: string;
	orderUnit: string;
	supplierOrganization: string;
	ceMarked: boolean;
	ceDirective: string;
	ceStandard: string;
	replacementDate: string;
	replacementArticle: string;
	basedOnProductNumber: string;
	iso: string;


}

