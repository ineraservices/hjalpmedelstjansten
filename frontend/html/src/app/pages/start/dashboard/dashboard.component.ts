import {Component, OnInit} from '@angular/core';
import {OrganizationService} from "../../../services/organization.service";
import {AuthService} from "../../../auth/auth.service";
import {AgreementService} from "../../../services/agreement.service";
import {UserService} from "../../../services/user.service";
import {AlertService} from "../../../services/alert.service";
import {ProductService} from "../../../services/product.service";
import {ActivatedRoute} from "@angular/router";
import {take} from "rxjs/operators";

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

	organizationId;
	pricelistsWithRowsPA;
	pricelistsWithRowsPI;
	emptyPricelists;
	pricelistsDeclinedRows;
	pricelistsWaitingForApproval;
	pricelistsNewOrChanged;
	changedArticleList;
	discontinuedArticleList;
	agreementsToExtend;
	agreementsChangedByCustomer;
	pricelistRowsApprovedByCustomer;
	userRole: string;
	activeTab: number;

	constructor(private organizationService: OrganizationService,
							private authService: AuthService,
							private agreementService: AgreementService,
							private productService: ProductService,
							private userService: UserService,
							private alertService: AlertService,
							private route: ActivatedRoute) {
	}

	ngOnInit() {
		this.organizationId = this.authService.getOrganizationId();

		this.userService.getProfile().subscribe(
			res => {
				res.userEngagements[0].roles.forEach((role) => {
					if (role.name === "SupplierAgreementManager") {
						this.userRole = role.name;
						this.getPriceListsFilteredBy("PENDING_APPROVAL");
						this.getPriceListsFilteredBy("DECLINED");
						this.getPriceListsFilteredBy("EMPTY_PRICELISTS");
						this.getPriceListsFilteredBy("NEW_OR_CHANGED");
						this.getAgreementsChangedByCustomer();
						this.getPricelistRowsApprovedByCustomer();
					}
					if (role.name === "CustomerAgreementManager") {
						this.userRole = role.name;
						this.getPriceListsFilteredBy("PENDING_APPROVAL");
						this.getPriceListsFilteredBy("PENDING_INACTIVATION");
						this.getAgreementsExtensionReminder();
						this.getArticlesChanged();
					}

					if (role.name === "CustomerAssortmentManager" || role.name === "CustomerAssignedAssortmentManager"){
						this.getArticlesDiscontinued();
					}
				});
			}, error => {
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

		this.setActiveTab();

	}

	setActiveTab() {

		this.route.queryParams.pipe(take(1)).subscribe(param => {
			this.activeTab = param.tab === "events" ? 0 : 1;
		});

	}

	getPricelistRowsApprovedByCustomer() {
		this.agreementService.getPricelistRowsApprovedByCustomer(this.organizationId).subscribe(
			res => {
				this.pricelistRowsApprovedByCustomer = res;
			}, error => {
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	getAgreementsChangedByCustomer() {
		this.agreementService.getAgreementsChangedByCustomer(this.organizationId).subscribe(
			res => {
				this.agreementsChangedByCustomer = res;
			}, error => {
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	getPriceListsFilteredBy(filter) {
		this.agreementService.getPricelistsFilteredBy(this.organizationId, filter).subscribe(
			res => {
				if (filter === "PENDING_APPROVAL" && this.userRole === "CustomerAgreementManager") {
					this.pricelistsWithRowsPA = res;
				} else if (filter === "PENDING_INACTIVATION" && this.userRole === "CustomerAgreementManager") {
					this.pricelistsWithRowsPI = res;
				} else if (filter === "PENDING_APPROVAL" && this.userRole === "SupplierAgreementManager") {
					this.pricelistsWaitingForApproval = res;
				} else if (filter === "DECLINED") {
					this.pricelistsDeclinedRows = res;
				} else if (filter === "EMPTY_PRICELISTS") {
					this.emptyPricelists = res;
				} else if (filter === "NEW_OR_CHANGED") {
					this.pricelistsNewOrChanged = res;
				}

			}, error => {
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

	}

	getAgreementsExtensionReminder() {

		this.agreementService.getAgreementsToExtend(this.organizationId).subscribe(
			res => {
				this.agreementsToExtend = res;
			}, error => {
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}


	getArticlesChanged() {
		this.productService.getArticlesChanged(this.organizationId).subscribe(
			res => {
				this.changedArticleList = res;
			}, error => {
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	getArticlesDiscontinued(){
		this.productService.getArticlesDiscontinued(this.organizationId.toString()).subscribe({
			next: res => {
				this.discontinuedArticleList = res;
			},
			error: error => {
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		});
	}



}

