import {Component, Input, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {ProductService} from "../../../../services/product.service";
import {AuthService} from "../../../../auth/auth.service";
import {Router} from "@angular/router";

@Component({
	selector: 'app-dashboard-table-assortmentarticlesdiscontinued',
	templateUrl: './dashboardtable-assortmentarticlesdiscontinued.component.html',
	styleUrls: ['./dashboardtable-assortmentarticlesdiscontinued.component.scss']
})
export class DashboardtableAssortmentArticlesDiscontinuedComponent implements OnInit {
	@Input() discontinuedArticles: any;
	@Input() title: string;
	@Input() userRole: string;
	@Input() organizationId: any;

	displayedColumns = ['artikelnummer', 'leverantör', 'artikelnamn', 'utgångsdatum', 'utbudsnamn', 'articleMenu'];
	dataSource = new MatTableDataSource();

	constructor(private productService: ProductService, private authService: AuthService, private router: Router) {

	}

	ngOnInit() {
		this.dataSource = this.discontinuedArticles.map((item) => {
			return {
					articleId: item[0].id,
					articleName: item[0].articleName,
					articleNumber: item[0].articleNumber,
					replacementDate: item[0].replacementDate,
					articleOrganizationId: item[0].organizationId,
					organizationName: item[0].organizationName,
					assortmentId: item[1].id,
					assortmentName: item[1].name,
					assortmentOrganizationId: item[1].customer.id
				};
		});
	}

	removeArticlesDiscontinuedNotice(organizationId: any, articleId: any, assortmentId: any, remove: string) {
		const userId = this.authService.getUserId();
		debugger;
		this.productService.removeArticlesDiscontinuedNotice(organizationId, articleId, userId, assortmentId, remove).subscribe();
		location.reload();
	}


}

export interface discontinuedarticlesstable {
	dateChanged: Date;
	articleName: string;
	articleNumber: string;
	orderUnit: string;
	supplierOrganization: string;
	ceMarked: boolean;
	ceDirective: string;
	ceStandard: string;
	replacementDate: string;
	replacementArticle: string;
	basedOnProductNumber: string;
	iso: string;
}

