import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {MatTableDataSource} from "@angular/material/table";
import {AuthService} from '../../../../auth/auth.service';
import {AgreementService} from "../../../../services/agreement.service";
import {PathService} from "../../../../services/path.service";

@Component({
	selector: 'app-dashboardtable-approved-pricelistrow',
	templateUrl: './dashboardtable-approved-pricelistrow.component.html',
	styleUrls: ['./dashboardtable-approved-pricelistrow.component.scss']
})
export class DashboardtableApprovedPricelistrowComponent implements OnInit {

	// avtalsnamn, avtalsnummer, prislista, kund, verksamhetsområde (om det finns), kommentar
	@Input() pricelistRowsApprovedByCustomer: any;
	@Input() title: string;
	@Input() userRole: string;
	@Input() organizationId: any;

	displayedColumns = ['datum', 'agreementName', 'agreementNumber', 'pricelistNumber', 'customer', 'businesslevel', 'comment', 'numberofrows', 'showrows'];
	dataSource = new MatTableDataSource();

	constructor(private authService: AuthService, private agreementService: AgreementService, private pathService: PathService, private router: Router) {

	}

	ngOnInit() {
		this.dataSource = this.pricelistRowsApprovedByCustomer.map((item) => ({
			numberofrows: item[0],
			agreementName: item[1],
			agreementNumber: item[2],
			pricelistNumber: item[3],
			customer: item[4],
			businesslevel: item[5],
			approvementId: item[6],
			comment: item[7],
			pricelistId: item[8],
			agreementId: item[9],
			created: item[10]
		})
		);
		console.log(this.dataSource);
	}

	getPricelistRowsByApprovementId(organizationId, agreementId, pricelistId, approvementId) {
		this.router.navigate(['/organization/' + organizationId + '/agreement/' + agreementId + '/pricelist/' + pricelistId + '/handle/approvementId/' + approvementId]);
	}


	removeApprovedPricelistRowNotice(organizationId, approvementId) {
		this.agreementService.removeApprovedPricelistRowNotice(organizationId, approvementId).subscribe();
		//this.router.routeReuseStrategy.shouldReuseRoute = () => false;
		//this.router.onSameUrlNavigation = 'reload';
		//this.router.navigate(['/dashboard']);
		location.reload();
		// let j = 0;
		// this.pricelistRowsApprovedByCustomer.forEach(row => {
		//   if (row[6] === approvementId) {
		//     this.pricelistRowsApprovedByCustomer.splice(j, 1);
		//   }
		//   j++;
		// });
		//
		// this.pricelistRowsApprovedByCustomer.data = this.pricelistRowsApprovedByCustomer;
	}
}

export interface approvedpricelistrowtable {
	created: Date;
	agreementName: string;
	agreementNumber: string;
	customer: string;
	businesslevel: string;
	agreementId: string;
	comment: string;
	uniqueId: string;
}
