import {Component, Input, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Router} from "@angular/router";

@Component({
	selector: 'app-dashboardtable-pricelist',
	templateUrl: './dashboardtable-pricelist.component.html',
	styleUrls: ['./dashboardtable-pricelist.component.scss']
})
export class DashboardtablePricelistComponent implements OnInit {
	@Input() pricelists: any;
	@Input() title: string;
	@Input() userRole: string;
	@Input() organizationId: any;

	displayedColumns = ['datum', 'avtalsnamn', 'avtalsnummer', 'prislista', 'leverantörellerkundOrganisation', 'verksamhetsnivå'];
	dataSource = new MatTableDataSource();

	constructor(private router: Router) {

	}

	ngOnInit() {

		this.dataSource = this.pricelists.map((item) => ({
			agreementId: item[0],
			pricelistId: item[1],
			agreementName: item[2],
			agreementNumber: item[3],
			pricelistNumber: item[4],
			organizationId: item[5],
			organizationName: item[6],
			businessLevelName: item[7],
			businessLevelId: item[8],
			date: item[9]
		}));
	}

	async routeTo(element: any): Promise<void> {
		await this.router.navigateByUrl("/organization/" + this.organizationId + "/agreement/" + element.agreementId +"/pricelist/" + element.pricelistId + "/handle");
	}

}

export interface priceliststable {
	date: Date;
	agreementName: string;
	agreementNumber: string;
	pricelistNumber: string;
	supplierorcustomerOrganization: string;
	businessLevel: string;

}

