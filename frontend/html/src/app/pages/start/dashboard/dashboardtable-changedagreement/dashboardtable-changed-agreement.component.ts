import {Component, Input, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {AuthService} from '../../../../auth/auth.service';
import {AgreementService} from "../../../../services/agreement.service";
import {Router} from "@angular/router";

@Component({
	selector: 'app-dashboardtable-changed-agreement',
	templateUrl: './dashboardtable-changed-agreement.component.html',
	styleUrls: ['./dashboardtable-changed-agreement.component.scss']
})
export class DashboardtableChangedAgreementComponent implements OnInit {

	@Input() agreementsChangedByCustomer: any;
	@Input() title: string;
	@Input() userRole: string;
	@Input() organizationId: any;

	displayedColumns = ['whenChanged', 'agreementName', 'agreementNumber', 'customer', 'businesslevel', 'changes'];
	dataSource = new MatTableDataSource();

	constructor(private authService: AuthService,
							private agreementService: AgreementService,
							private router: Router) {

	}

	ngOnInit() {
		this.dataSource = this.agreementsChangedByCustomer.map((item) => {
			const obj = {
				uniqueId: item[0],
				agreementName: item[1],   // lokalt är det agreementId här
				agreementNumber: item[2],
				agreementId: item[3],		// lokalt är det agreementName här
				customer: item[4],
				businesslevel: item[5],
				changes: item[6].split('<br>'),
				whenChanged: item[7]
			};
			return obj;
		}
		);
	}

	removeAgreementNotice(organizationId, agreementChangedId) {
		this.agreementService.removeAgreementChangedNotice(organizationId, agreementChangedId).subscribe();
	}

	async routeTo(element: any): Promise<void> {
		await this.router.navigateByUrl("/organization/" + this.organizationId + "/agreement/" + element.agreementId + "/handle");
		this.removeAgreementNotice(this.organizationId, element.uniqueId);
	}


}

export interface changedagreementtable {
	whenChanged: string;
	agreementName: string;
	agreementNumber: string;
	customer: string;
	businesslevel: string;
	agreementId: string;
	changes: string;
	uniqueId: string;
}
