import {Component, Input, OnInit} from '@angular/core';
import {forkJoin} from 'rxjs';
import {AuthService} from '../../../../auth/auth.service';
import {AgreementService} from '../../../../services/agreement.service';
import {CommonService} from '../../../../services/common.service';
import {AlertService} from "../../../../services/alert.service";
import {ProductService} from "../../../../services/product.service";


@Component({
	selector: 'app-dashboard-information',
	templateUrl: './dashboard-information.component.html',
	styleUrls: ['./dashboard-information.component.scss']
})
export class DashboardInformationComponent implements OnInit {

	@Input() role: string;
	@Input() isAssortmentManager: boolean;

	flexDirection: string;
	userRole: string;
	organizationId: number;
	pricelistsWaitingForApproval = null;
	agreementsChangedByCustomer = null;
	pricelistRowsApprovedByCustomer = null;
	pricelistRowsApprovedByCustomerText: string;
	agreementsChangedByCustomerText: string;
	changedArticles = null;
	changedArticlesText: string;
	discontinuedArticles = null;
	discontinuedArticlesText: string;
	waitingForApprovalText: string;
	noNoticeText = "Du har inga notiser";
	iconName = "info";

	constructor(private authService: AuthService,
							private agreementService: AgreementService,
							private productService: ProductService,
							private commonService: CommonService,
							private alertService: AlertService) {
		this.flexDirection = this.commonService.isUsingIE() ? 'row' : 'column';
	}

	ngOnInit() {

		this.userRole = this.role;
		this.organizationId = this.authService.getOrganizationId();
		if (this.userRole === "SupplierAgreementManager") {
			this.fetchInformationSupplier();
		} else if (this.userRole === "CustomerAgreementManager") {
			this.fetchInformationCustomer();
		}
		if (this.isAssortmentManager) {
			this.fetchAssortmentInformationCustomer();
		}
	}

	fetchInformationSupplier() {
		forkJoin(
			{
				pricelistRowsApprovedByCustomer: this.agreementService.getPricelistRowsApprovedByCustomer(this.organizationId),
				agreementsChanged: this.agreementService.getAgreementsChangedByCustomer(this.organizationId),
				pricelistsPendingApproval: this.agreementService.getPricelistsFilteredBy(this.organizationId, "PENDING_APPROVAL")
			})
			.subscribe(({pricelistRowsApprovedByCustomer, agreementsChanged, pricelistsPendingApproval}) => {
				this.pricelistRowsApprovedByCustomer = pricelistRowsApprovedByCustomer;
				this.agreementsChangedByCustomer = agreementsChanged;
				this.pricelistsWaitingForApproval = pricelistsPendingApproval;
				this.setPricelistsWaitingForApprovalText();
				this.setAgreementsChangedByCustomerText();
				this.setPricelistRowsApprovedByCustomerText();

			}, error => {
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
			);

	}

	fetchInformationCustomer() {
		this.productService.getArticlesChanged(this.organizationId).subscribe(
			res => {
				this.changedArticles = res;
				this.setChangedArticlesText();
			}, error => {
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	fetchAssortmentInformationCustomer(){
		this.productService.getArticlesDiscontinued(this.organizationId.toString()).subscribe({
			next: res => {
				this.discontinuedArticles = res;
				this.setDiscontiuedArticlesText();
			},
			error: error => {
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		});
	}

	setChangedArticlesText() {
		if (this.changedArticles != null) {
			if (this.changedArticles.length === 1) {
				this.changedArticlesText = "Du har 1 ändring, gjord av leverantör, på artikel";
			} else {
				this.changedArticlesText = "Du har " + this.changedArticles.length + " ändringar, gjorda av leverantör, på artiklar";
			}
		}
	}

	setDiscontiuedArticlesText() {
		if (this.discontinuedArticles != null) {
			if (this.discontinuedArticles.length === 1) {
				this.discontinuedArticlesText = "Du har 1 utgången artikel på utbud";
			} else {
				this.discontinuedArticlesText = "Du har " + this.discontinuedArticles.length + " utgångna artiklar på utbud";
			}
		}
	}

	setPricelistsWaitingForApprovalText() {
		if (this.pricelistsWaitingForApproval != null) {
			if (this.pricelistsWaitingForApproval.length === 1) {
				this.waitingForApprovalText = "Du har " + this.pricelistsWaitingForApproval.length + " prislista i inköpsavtal som väntar på godkännande";
			} else if (this.pricelistsWaitingForApproval.length > 1) {
				this.waitingForApprovalText = "Du har " + this.pricelistsWaitingForApproval.length + " prislistor i inköpsavtal som väntar på godkännande";
			}
		}

	}

	setAgreementsChangedByCustomerText() {
		if (this.agreementsChangedByCustomer != null) {
			if (this.agreementsChangedByCustomer.length > 1) {
				this.agreementsChangedByCustomerText = "Du har " + this.agreementsChangedByCustomer.length + " ändringar, gjorda av kund, på inköpsavtal";
			} else if (this.agreementsChangedByCustomer.length === 1) {
				this.agreementsChangedByCustomerText = "Du har 1 ändring, gjord av kund, på inköpsavtal";
			}

		}

	}

	setPricelistRowsApprovedByCustomerText() {
		if (this.pricelistRowsApprovedByCustomer != null) {
			if (this.pricelistRowsApprovedByCustomer.length > 1) {
				this.pricelistRowsApprovedByCustomerText = "Du har " + this.pricelistRowsApprovedByCustomer.length + " godkännanden av prislisterader";
			} else if (this.pricelistRowsApprovedByCustomer.length === 1) {
				this.pricelistRowsApprovedByCustomerText = "Du har 1 godkännande av prislisterader";
			}
		}
	}

}
