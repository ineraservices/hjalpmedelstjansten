import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../../../auth/auth.service';
import {UserService} from '../../../../services/user.service';
import {AgreementService} from '../../../../services/agreement.service';
import {CommonService} from '../../../../services/common.service';
import {AlertService} from "../../../../services/alert.service";
import {forkJoin} from 'rxjs';


@Component({
	selector: 'app-dashboard-events',
	templateUrl: './dashboardevents.component.html',
	styleUrls: ['./dashboardevents.component.scss']
})
export class DashboardEventsComponent implements OnInit {
	@Input() role: string;

	flexDirection: string;
	organizationId: number;
	pricelistsPA = null;
	pricelistsPI = null;
	newOrChangedPricelists = null;
	newOrChangedPricelistsText: string;
	emptyPricelists = null;
	emptyPricelistsText: string;
	pricelistsWithDeclinedRows = null;
	agreementsToExtend = null;
	agreementsToExtendText: string;
	eventsiconName: string;
	noEventsText = "Du har inga ärenden att hantera";
	pricelistsPAtext: string;
	pricelistsPItext: string;
	declinedRowsText: string;
	userRole: string;

	constructor(private authService: AuthService,
							private userService: UserService,
							private agreementService: AgreementService,
							private commonService: CommonService,
							private alertService: AlertService) {
		this.flexDirection = this.commonService.isUsingIE() ? 'row' : 'column';
	}

	ngOnInit() {
		this.userRole = this.role;
		this.organizationId = this.authService.getOrganizationId();

		if (this.userRole === "SupplierAgreementManager") {
			this.fetchEventsSupplier();
		}
		if (this.userRole === "CustomerAgreementManager") {
			this.fetchEventsCustomer();
		}
	}

	fetchEventsSupplier() {
		forkJoin({
			emptyPricelists: this.agreementService.getPricelistsFilteredBy(this.organizationId, "EMPTY_PRICELISTS"),
			newOrChangedPricelists: this.agreementService.getPricelistsFilteredBy(this.organizationId, "NEW_OR_CHANGED"),
			declinedPricelists: this.agreementService.getPricelistsFilteredBy(this.organizationId, "DECLINED")
		}).subscribe(({emptyPricelists, newOrChangedPricelists, declinedPricelists}) => {
			this.pricelistsWithDeclinedRows = declinedPricelists;
			this.emptyPricelists = emptyPricelists;
			this.newOrChangedPricelists = newOrChangedPricelists;
			this.setDeclinedRowsText();
			this.setEmptyPricelistsText();
			this.setNewOrChangedPricelistsText();
			this.setIcon();
		}, error => {
			error.error.errors.forEach(err => {
				this.alertService.error(err);
			});
		});

	}

	fetchEventsCustomer() {
		// TODO! Dela upp Pending approval och pending inactivation till två delar enligt HJAL-2474

		forkJoin(
			{
				pricelistsPA: this.agreementService.getPricelistsFilteredBy(this.organizationId, "PENDING_APPROVAL"),
				pricelistsPI: this.agreementService.getPricelistsFilteredBy(this.organizationId, "PENDING_INACTIVATION"),
				agreementsToExtend: this.agreementService.getAgreementsToExtend(this.organizationId)
			})
			.subscribe(({pricelistsPA, pricelistsPI, agreementsToExtend}) => {
				this.agreementsToExtend = agreementsToExtend;
				this.pricelistsPA = pricelistsPA;
				this.pricelistsPI = pricelistsPI;
				this.setAgreementsToExtendText();
				this.setPricelistsToHandleText();
				this.setIcon();
			}, error => {
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			});

	}

	setAgreementsToExtendText() {
		if (this.agreementsToExtend != null) {
			this.agreementsToExtendText = "Du har " + this.agreementsToExtend.length + " inköpsavtal att förlänga.";
		}

	}

	setEmptyPricelistsText() {

		if (this.emptyPricelists != null) {
			if (this.emptyPricelists.length === 1) {
				this.emptyPricelistsText = "Du har " + this.emptyPricelists.length + " tom prislista.";
			} else if (this.emptyPricelists.length > 1) {
				this.emptyPricelistsText = "Du har " + this.emptyPricelists.length + " tomma prislistor.";
			}
		}

	}

	setNewOrChangedPricelistsText() {
		if (this.newOrChangedPricelists != null) {
			if (this.newOrChangedPricelists.length === 1) {
				this.newOrChangedPricelistsText = "Du har " + this.newOrChangedPricelists.length + " prislista att hantera.";
			} else if (this.newOrChangedPricelists.length > 1) {
				this.newOrChangedPricelistsText = "Du har " + this.newOrChangedPricelists.length + " prislistor att hantera.";
			}
		}
	}

	setDeclinedRowsText() {
		if (this.pricelistsWithDeclinedRows != null) {
			if (this.pricelistsWithDeclinedRows.length === 1) {
				this.declinedRowsText = "Du har " + this.pricelistsWithDeclinedRows.length + " prislista i inköpsavtal som innehåller prislisterader som avböjts av kund.";
			} else if (this.pricelistsWithDeclinedRows.length > 1) {
				this.declinedRowsText = "Du har " + this.pricelistsWithDeclinedRows.length + " prislistor i inköpsavtal som innehåller prislisterader som avböjts av kund.";
			}
		}

	}

	setPricelistsToHandleText() {
		if (this.pricelistsPA != null) {
			if (this.pricelistsPA.length === 1) {
				this.pricelistsPAtext = "Du har " + this.pricelistsPA.length + " prislista att hantera för godkännande";
			} else if (this.pricelistsPA.length > 1) {
				this.pricelistsPAtext = "Du har " + this.pricelistsPA.length + " prislistor att hantera för godkännande";
			}
		}

		if (this.pricelistsPI != null) {
			if (this.pricelistsPI.length === 1) {
				this.pricelistsPItext = "Du har " + this.pricelistsPI.length + " prislista att hantera för inaktivering";
			} else if (this.pricelistsPI.length > 1) {
				this.pricelistsPItext = "Du har " + this.pricelistsPI.length + " prislistor att hantera för inaktivering";
			}
		}

	}

	setIcon() {
		(this.pricelistsPA == null || this.pricelistsPA.length == 0) && (this.pricelistsPI == null || this.pricelistsPI.length == 0) && this.newOrChangedPricelists == null && this.emptyPricelists == null && this.agreementsToExtend == null && this.pricelistsWithDeclinedRows == null ? this.eventsiconName = "notifications" : this.eventsiconName = "notifications_active";
	}

}
