import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../../auth/auth.service';
import {UserService} from '../../../services/user.service';
import {CommonService} from '../../../services/common.service';
import {take} from "rxjs/operators";

@Component({
	selector: 'app-dashboard-alerts',
	templateUrl: './dashboardalerts.component.html',
	styleUrls: ['./dashboardalerts.component.scss']
})
export class DashboardAlertsComponent implements OnInit {
	@Input() role: string;
	@Input() isAssortmentManager: boolean;

	flexDirection: string;
	userName: string;
	userRole: string;

	constructor(private authService: AuthService,
							private userService: UserService,
							private commonService: CommonService) {
		this.flexDirection = this.commonService.isUsingIE() ? 'row' : 'column';
	}

	ngOnInit() {
		this.userRole = this.role;
		this.userService.getProfile().pipe(take(1)).subscribe(
			res => {
				this.userName = res.firstName;
				res.userEngagements[0].roles.forEach((role) => {
					if (role.name === "SupplierAgreementManager") {
						this.userRole = role.name;
					}
					if (role.name === "CustomerAgreementManager") {
						this.userRole = role.name;
					}
				});
			}
		);
	}


}
