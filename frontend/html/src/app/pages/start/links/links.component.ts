import {Component, OnInit} from '@angular/core';
import {CommonService} from "../../../services/common.service";
import {DynamicTextService} from "../../../services/dynamictext.service";
import {DynamicText} from "../../../models/dynamictext.model";


@Component({
	selector: 'app-links',
	templateUrl: './links.component.html',
	styleUrls: ['./links.component.scss']
})
export class LinksComponent implements OnInit {
	flexDirection: string;
	linksTextLoaded = false;
	linksText: DynamicText;


	constructor(private commonService: CommonService,
							private dynamicTextService: DynamicTextService) {
		this.flexDirection = this.commonService.isUsingIE() ? 'row' : 'column';
	}

	ngOnInit() {
		this.dynamicTextService.getDynamicText(3).subscribe(
			data => {
				this.linksText = data;
				this.linksTextLoaded = true;
			}
		);
	}

}
