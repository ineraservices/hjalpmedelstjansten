import {Component, OnInit} from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {DynamicTextService} from "../../../services/dynamictext.service";
import {DynamicText} from "../../../models/dynamictext.model";


@Component({
	selector: 'app-dynamic-text',
	templateUrl: './dynamic-text.component.html',
	styleUrls: ['./dynamic-text.component.scss']
})
export class DynamicTextComponent implements OnInit {
	flexDirection: string;
	dynamicTextLoaded = false;
	dynamicText: DynamicText;
	userRoles;
	organizationId;

	constructor(private dynamicTextService: DynamicTextService,
							private commonService: CommonService) {
		this.flexDirection = this.commonService.isUsingIE() ? 'row' : 'column';
	}

	ngOnInit() {
		this.dynamicTextService.getDynamicText(1).subscribe(
			data => {
				this.dynamicText = data;
				this.dynamicTextLoaded = true;
			}
		);

	}
}

