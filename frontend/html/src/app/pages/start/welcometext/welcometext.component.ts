import {Component, OnInit} from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {UserService} from "../../../services/user.service";
import {DynamicTextService} from "../../../services/dynamictext.service";
import {DynamicText} from "../../../models/dynamictext.model";
import {SafeHTMLPipe} from "../../../pipes/format-html.pipe";

@Component({
	selector: 'app-welcometext',
	templateUrl: './welcometext.component.html',
	styleUrls: ['./welcometext.component.scss']
})
export class WelcometextComponent implements OnInit {
	flexDirection: string;
	expanded: boolean;
	organizationId;
	welcomeTextLoaded = false;
	welcomeText: DynamicText;

	constructor(private commonService: CommonService,
							private userService: UserService,
							private dynamicTextService: DynamicTextService,
							private safeHTMLPipe: SafeHTMLPipe) {
		this.flexDirection = this.commonService.isUsingIE() ? 'row' : 'column';
	}

	ngOnInit() {
		this.getProfile();
		this.dynamicTextService.getDynamicText(2).subscribe(
			data => {
				this.welcomeText = data;
				this.welcomeTextLoaded = true;
			}
		);
	}

	getProfile() {
		this.userService.getProfile().subscribe(
			profile => {
				this.expanded = !profile.previousLoginFound;
			}
		);
	}

}
