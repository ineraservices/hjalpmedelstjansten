import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LoginService} from '../../services/login.service';
import {AuthService} from '../../auth/auth.service';
import {UserService} from '../../services/user.service';
import {AgreementService} from '../../services/agreement.service';
import {CommonService} from '../../services/common.service';
import {DynamicTextService} from "../../services/dynamictext.service";
import {DynamicText} from "../../models/dynamictext.model";
import {OrganizationService} from "../../services/organization.service";
import {AlertService} from "../../services/alert.service";


@Component({
	selector: 'app-start',
	templateUrl: './start.component.html',
	styleUrls: ['./start.component.scss']
})
export class StartComponent implements OnInit {
	flexDirection: string;
	dynamicTextLoaded = false;
	dynamicText: DynamicText;
	userRole;
	organizationId;
	isSupplierAgreementManager: boolean;
	isCustomerAgreementManager: boolean;
	isAssortmentManager: boolean;


	constructor(private authService: AuthService,
							private dynamicTextService: DynamicTextService,
							private loginService: LoginService,
							private userService: UserService,
							private route: ActivatedRoute,
							private router: Router,
							private organizationService: OrganizationService,
							private agreementService: AgreementService,
							private commonService: CommonService,
							private alertService: AlertService) {
		this.flexDirection = this.commonService.isUsingIE() ? 'row' : 'column';
	}

	ngOnInit() {
		this.isSupplierAgreementManager = false;
		this.isCustomerAgreementManager = false;
		this.isAssortmentManager = false;

		this.userService.getProfile().subscribe(
			res => {
				this.authService.login(res);
				res.userEngagements[0].roles.forEach((role) => {
					if (role.name === "SupplierAgreementManager") {
						this.userRole = role.name;
						this.isSupplierAgreementManager = true;
					}
					if (role.name === "CustomerAgreementManager") {
						this.userRole = role.name;
						this.isCustomerAgreementManager = true;
					}
					if (role.name === "CustomerAssortmentManager" || role.name === "CustomerAssignedAssortmentManager") {
						this.isAssortmentManager = true;
					}
				});
			}
		);
		this.dynamicTextService.getDynamicText(1).subscribe(
			data => {
				this.dynamicText = data;
				this.dynamicTextLoaded = true;
			}
		);

	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

}

