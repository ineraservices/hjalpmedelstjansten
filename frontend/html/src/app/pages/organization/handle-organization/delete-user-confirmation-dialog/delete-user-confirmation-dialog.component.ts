import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../../../../auth/auth.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ActivatedRoute} from '@angular/router';
import {HelptextService} from '../../../../services/helptext.service';
import {AgreementService} from '../../../../services/agreement.service';
import {AlertService} from '../../../../services/alert.service';
import {UserService} from '../../../../services/user.service';
import {OrganizationService} from '../../../../services/organization.service';
import {MatTable} from '@angular/material/table';
import {User} from "../../../../models/user/user.model";
import {RemoveUserCode} from "../../../../models/user/removeUserCode.model";
import {ElectronicAddress} from "../../../../models/electronic-address.model";
import {UserEngagement} from "../../../../models/user/user-engagement.model";
import {FormControl} from "@angular/forms";
import {forkJoin} from 'rxjs';


@Component({
	selector: 'app-delete-user-confirmation-dialog',
	templateUrl: './delete-user-confirmation-dialog.component.html',
	styleUrls: ['./delete-user-confirmation-dialog.component.scss']
})
export class DeleteUserConfirmationDialogComponent implements OnInit {

	@ViewChild(MatTable) table2: MatTable<any>;

	loading = true;
	dataSource = null;
	stepOne = true;
	stepTwo = false;
	userId;
	organizationId;
	adminUserId;
	adminOrganizationId;
	adminUser: User = new User();
	removedUser: User = new User();
	removedUserReasonCode: RemoveUserCode = new RemoveUserCode();
	customerAgreements = [];
	supplierContacts: Array<User>;
	removedUserIsSupplier;
	removedUserIsCustomer;
	removedUserIsServiceOwner;
	adminUserIsSupplier;
	adminUserIsCustomer;
	adminUserIsServiceOwner;
	enableRemoval = false;
	stepTwoMessage;
	reasonCode = new FormControl(null, {
		updateOn: 'blur'
	});

	constructor(public dialogRef: MatDialogRef<DeleteUserConfirmationDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any,
							private route: ActivatedRoute,
							private authService: AuthService,
							private agreementService: AgreementService,
							private helpTextService: HelptextService,
							private alertService: AlertService,
							private userService: UserService,
							private organizationService: OrganizationService) {
		this.userId = data.userId;
		this.organizationId = data.organizationId;
	}

	ngOnInit() {
		this.adminOrganizationId = this.authService.getOrganizationId();
		this.adminUserId = this.authService.getUserId();
		this.initUsers();
		this.getAdminUser(this.adminOrganizationId, this.adminUserId);
		this.getRemovedUser(this.organizationId, this.userId);
		this.getAdminOrganization();
		this.getRemovedUserOrganization();
	}


	initUsers() {
		this.adminUser.electronicAddress = new ElectronicAddress();
		this.adminUser.userEngagements = new Array<UserEngagement>();
		this.adminUser.userEngagements.push(new UserEngagement());
		this.adminUser.userEngagements[0].organizationId = this.authService.getOrganizationId();

		this.removedUser.electronicAddress = new ElectronicAddress();
		this.removedUser.userEngagements = new Array<UserEngagement>();
		this.removedUser.userEngagements.push(new UserEngagement());
		this.removedUser.userEngagements[0].organizationId = this.organizationId;
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	goToStepTwo() {
		this.stepOne = false;
		this.stepTwo = true;
		if (this.adminUserIsCustomer) {
			this.validateCustomerRemovingCustomer();
		}
		if (this.adminUserIsSupplier) {
			this.validateSupplierRemovingSupplier();
		}
		if (this.adminUserIsServiceOwner) {
			if (this.removedUserIsServiceOwner && this.adminUserIsServiceOwner) {
				if (this.userId === this.adminUserId) {
					this.alertService.errorWithMessage("Går ej att ta bort sig själv");
					this.dialogRef.close();
				}
			}
			this.validateServiceOwnerRemovingUser();
		}
	}

	getAdminOrganization() {
		this.organizationService.getOrganization(this.adminOrganizationId).subscribe(
			res => {
				this.adminUserIsSupplier = res.organizationType === 'SUPPLIER';
				this.adminUserIsCustomer = res.organizationType === 'CUSTOMER';
				this.adminUserIsServiceOwner = res.organizationType === 'SERVICE_OWNER';
			}
		);
	}

	getRemovedUserOrganization() {
		this.organizationService.getOrganization(this.organizationId).subscribe(
			res => {
				this.removedUserIsSupplier = res.organizationType === 'SUPPLIER';
				this.removedUserIsCustomer = res.organizationType === 'CUSTOMER';
				this.removedUserIsServiceOwner = res.organizationType === 'SERVICE_OWNER';
			}
		);

	}

	validateCustomerRemovingCustomer() {
		const query = '?query=' + this.userId;
		this.agreementService.adminSearchAllAgreeementsForOrganization(this.organizationId, query).subscribe(
			res => {

				const agreementCount = res.headers.get('X-Total-Count');
				if (agreementCount !== '0') {
					this.customerAgreements = res.body;
				} else {
					this.customerAgreements = [];
				}
				let singleApproverCustomerAgreements = "";
				for (let i = 0; i < this.customerAgreements.length; i++) {
					if (this.customerAgreements[i].customerPricelistApprovers != null && this.customerAgreements[i].customerPricelistApprovers.length === 1) {
						if (this.customerAgreements[i].customerPricelistApprovers[0].id === this.userId) {
							singleApproverCustomerAgreements = singleApproverCustomerAgreements + this.customerAgreements[i].agreementNumber + ", ";
						}
					}
				}
				if (singleApproverCustomerAgreements.length > 0) {
					this.stepTwoMessage = "Användaren är den enda som godkänner prislisterader på avtal (avtalsnummer):" + '\n\n' + singleApproverCustomerAgreements + '\n\n' + "Användaren kan därför inte tas bort, lägg upp minst en till användare som godkänner prislisterader på dessa avtal.";
					this.enableRemoval = false;
				} else {
					this.enableRemoval = true;
					this.stepTwoMessage = "Vill du verkligen ta bort användare " + this.removedUser.firstName + " " + this.removedUser.lastName + " från Hjälpmedelstjänsten?";

				}
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

	}

	validateSupplierRemovingSupplier() {
		this.enableRemoval = true;
		this.userService.getUserByRoles(this.organizationId, 'SupplierAgreementManager').subscribe(
			res => {
				this.supplierContacts = res;
				if (this.supplierContacts.length === 1) {
					if (this.supplierContacts[0].id === this.removedUser.id) {
						this.stepTwoMessage = "Användaren är den enda angivna avtalskontakten och bör därför inte tas bort." + '\n\n' + "Vill du verkligen ta bort användare " + this.removedUser.firstName + " " + this.removedUser.lastName + " från Hjälpmedelstjänsten?";
					}
				} else {
					this.stepTwoMessage = "Vill du verkligen ta bort användare " + this.removedUser.firstName + " " + this.removedUser.lastName + " från Hjälpmedelstjänsten?";
				}
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

	}

	validateServiceOwnerRemovingUser() {
		if (this.removedUserIsCustomer) {
			this.validateServiceOwnerRemovingCustomer();
		}
		if (this.removedUserIsSupplier) {
			this.validateServiceOwnerRemovingSupplier();
		}
		if (this.removedUserIsServiceOwner) {
			this.validateServiceOwnerRemovingServiceOwner();
		}
	}

	validateServiceOwnerRemovingCustomer() {

		this.loading = true;
		const query = '?query=' + this.userId;
		forkJoin({
			agreementsForOrganization: this.agreementService.adminSearchAllAgreeementsForOrganization(this.organizationId, query),
			customerAdmins: this.userService.getUserByRoles(this.organizationId, 'Customeradmin')
		}).subscribe(({agreementsForOrganization, customerAdmins}) => {

			const agreementCount = agreementsForOrganization.headers.get('X-Total-Count');
			if (agreementCount !== '0') {
				this.customerAgreements = agreementsForOrganization.body;
			} else {
				this.customerAgreements = [];
			}
			let singleApproverCustomerAgreements = "";
			// console.log(this.customerAgreements[0].customerPricelistApprovers.length)
			const agreementslength = this.customerAgreements.length;
			// console.log(agreementslength)
			console.log(this.customerAgreements);

			for (let i = 0; i < agreementslength; i++) {
				if (this.customerAgreements[i].customerPricelistApprovers != null && this.customerAgreements[i].customerPricelistApprovers.length === 1) {
					if (this.customerAgreements[i].customerPricelistApprovers[0].id === this.userId) {
						singleApproverCustomerAgreements = singleApproverCustomerAgreements + this.customerAgreements[i].agreementNumber + ", ";
					}
				}
			}

			if (singleApproverCustomerAgreements.length > 0) {
				this.stepTwoMessage = "Användaren är den enda som godkänner prislisterader på avtal (avtalsnummer):" + '\n\n' + singleApproverCustomerAgreements + '\n\n' + "Användaren kan därför inte tas bort, lägg upp minst en till användare som godkänner prislisterader på dessa avtal.";
				this.enableRemoval = false;
			} else {
				this.enableRemoval = true;
				const localAdminCount = customerAdmins.length;
				const localAdminId = customerAdmins[0].id;
				if (localAdminCount === 1 && localAdminId === this.removedUser.id) {
					this.stepTwoMessage = "Användaren är den enda lokala administratören i denna organisation och bör därför inte tas bort." + '\n\n' + "Vill du verkligen ta bort användare " + this.removedUser.firstName + " " + this.removedUser.lastName + " från Hjälpmedelstjänsten?";
				} else {
					this.stepTwoMessage = "Vill du verkligen ta bort användare " + this.removedUser.firstName + " " + this.removedUser.lastName + " från Hjälpmedelstjänsten?";
				}
			}

			this.loading = false;
		});


		// const query = '?query=' + this.userId;
		// this.agreementService.adminSearchAllAgreeementsForOrganization(this.organizationId, query).subscribe(
		//   res => {
		//     const agreementCount = res.headers.get('X-Total-Count');
		//     agreementCount !== '0' ? this.customerAgreements = res.body : this.customerAgreements = [];
		//
		//     let singleApproverCustomerAgreements = "";
		//     const agreementslength = this.customerAgreements.length;
		//     for(let i = 0; i < agreementslength; i++){
		//       if(this.customerAgreements[i].customerPricelistApprovers.length == 1){
		//         if(this.customerAgreements[i].customerPricelistApprovers[0].id == this.userId){
		//           singleApproverCustomerAgreements = singleApproverCustomerAgreements + this.customerAgreements[i].agreementNumber + ", ";
		//         }
		//       }
		//     }
		//     if(singleApproverCustomerAgreements.length > 0){
		//       this.stepTwoMessage =  "Användaren är den enda som godkänner prislisterader på avtal (avtalsnummer):"+ '\n\n' + singleApproverCustomerAgreements + '\n\n' +  "Användaren kan därför inte tas bort, lägg upp minst en till användare som godkänner prislisterader på dessa avtal.";
		//       this.enableRemoval = false;
		//     }
		//     else {
		//       this.enableRemoval = true;
		//
		//       this.userService.getUserByRoles(this.organizationId, 'Customeradmin').subscribe(
		//         res => {
		//           const localAdminCount = res.length;
		//           const localAdminId = res[0].id;
		//           if(localAdminCount == 1 && localAdminId == this.removedUser.id){
		//             this.stepTwoMessage = "Användaren är den enda lokala administratören i denna organisation och bör därför inte tas bort." + '\n\n' + "Vill du verkligen ta bort användare " + this.removedUser.firstName + " " + this.removedUser.lastName +" från Hjälpmedelstjänsten?";
		//           }
		//           else{
		//             this.stepTwoMessage = "Vill du verkligen ta bort användare " + this.removedUser.firstName + " " + this.removedUser.lastName +" från Hjälpmedelstjänsten?";
		//           }
		//         }
		//       );
		//     }
		//   }, error => {
		//     this.alertService.clear();
		//     error.error.errors.forEach(err => {
		//       this.alertService.error(err);
		//     });
		//   }
		// );

	}

	validateServiceOwnerRemovingSupplier() {

		this.userService.getUserByRoles(this.organizationId, 'Supplieradmin').subscribe(
			res => {
				this.enableRemoval = true;
				const localAdminCount = res.length;
				let localAdminId;
				if (localAdminCount !== 0) {
					localAdminId = res[0].id;
				}

				this.userService.getUserByRoles(this.organizationId, 'SupplierAgreementManager').subscribe(
					result => {
						this.supplierContacts = result;

						if (localAdminCount === 1) {
							if (this.supplierContacts.length === 1 && this.removedUser.id === this.supplierContacts[0].id && this.removedUser.id === localAdminId) {
								this.stepTwoMessage = "Användaren är den enda lokala administratören i denna organisation och bör därför inte tas bort." + '\n\n' + "Användaren är den enda angivna avtalskontakten och bör därför inte tas bort." + '\n\n' + "Vill du verkligen ta bort användare " + this.removedUser.firstName + " " + this.removedUser.lastName + " från Hjälpmedelstjänsten?";
							} else if (this.removedUser.id === localAdminId) {
								this.stepTwoMessage = "Användaren är den enda lokala administratören i denna organisation och bör därför inte tas bort." + '\n\n' + "Vill du verkligen ta bort användare " + this.removedUser.firstName + " " + this.removedUser.lastName + " från Hjälpmedelstjänsten?";
							} else if (this.supplierContacts.length === 1 && this.supplierContacts[0].id === this.removedUser.id) {
								this.stepTwoMessage = "Användaren är den enda angivna avtalskontakten och bör därför inte tas bort." + '\n\n' + "Vill du verkligen ta bort användare " + this.removedUser.firstName + " " + this.removedUser.lastName + " från Hjälpmedelstjänsten?";
							} else {
								this.stepTwoMessage = "Vill du verkligen ta bort användare " + this.removedUser.firstName + " " + this.removedUser.lastName + " från Hjälpmedelstjänsten?";
							}

						} else if (this.supplierContacts.length === 1) {
							if (this.supplierContacts[0].id === this.removedUser.id) {
								this.stepTwoMessage = "Användaren är den enda angivna avtalskontakten och bör därför inte tas bort." + '\n\n' + "Vill du verkligen ta bort användare " + this.removedUser.firstName + " " + this.removedUser.lastName + " från Hjälpmedelstjänsten?";
							} else {
								this.stepTwoMessage = "Vill du verkligen ta bort användare " + this.removedUser.firstName + " " + this.removedUser.lastName + " från Hjälpmedelstjänsten?";
							}

						} else {
							this.stepTwoMessage = "Vill du verkligen ta bort användare " + this.removedUser.firstName + " " + this.removedUser.lastName + " från Hjälpmedelstjänsten?";
						}
					}, error => {
						this.alertService.clear();
						error.error.errors.forEach(err => {
							this.alertService.error(err);
						});
					}
				);
				this.loading = false;

			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

	}

	validateServiceOwnerRemovingServiceOwner() {
		this.stepTwoMessage = "Vill du verkligen ta bort användare " + this.removedUser.firstName + " " + this.removedUser.lastName + " från Hjälpmedelstjänsten?";
		this.enableRemoval = true;
	}

	getAdminUser(organizationId, userId) {
		this.userService.getUser(organizationId, userId).subscribe(
			res => {
				this.adminUser = res;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	getRemovedUser(organizationId, userId) {
		this.userService.getUser(organizationId, userId).subscribe(
			res => {

				this.removedUser = res;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	deleteUser() {
		this.removedUserReasonCode.reasonCode = this.reasonCode.value;
		this.userService.deleteUser(this.organizationId, this.userId, this.reasonCode.value).subscribe(
			() => {
				this.alertService.success('Användare borttagen.');
				this.dialogRef.close();
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);

	}

	done() {
		this.dialogRef.close();
	}
}
