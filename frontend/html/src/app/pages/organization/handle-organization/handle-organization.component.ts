import {Component, OnInit} from '@angular/core';
import {Organization} from '../../../models/organization/organization.model';
import {OrganizationService} from '../../../services/organization.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PostAddress} from '../../../models/post-address.model';
import {AlertService} from '../../../services/alert.service';
import {FormControl, Validators} from '@angular/forms';
import {CountryService} from '../../../services/country.service';
import {AssortmentService} from '../../../services/assortment.service';
import {ElectronicAddress} from '../../../models/electronic-address.model';
import {AuthService} from '../../../auth/auth.service';
import {Location} from '@angular/common';
import {UserService} from '../../../services/user.service';
import {HelptextService} from '../../../services/helptext.service';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {CommonService} from '../../../services/common.service';
import {
	DeleteUserConfirmationDialogComponent
} from "./delete-user-confirmation-dialog/delete-user-confirmation-dialog.component";
import {MatDialog} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {saveAs} from "file-saver";
import {Municipality} from "../../../models/assortment/municipality.model";
import {SelectionModel} from '@angular/cdk/collections';
import {County} from "../../../models/assortment/county.model";

@Component({
	selector: 'app-handle-organization',
	templateUrl: './handle-organization.component.html',
	styleUrls: ['./handle-organization.component.scss']
})
export class HandleOrganizationComponent implements OnInit {
	displayedColumns = ['namn', 'användarnamn', 'titel', 'e-post', 'status', 'delete'];
	adminOrganizationId;
	loaded = false;
	saving = false;
	editMode = false;
	showSaveBar = true;
	initialTab = 0;
	organization: Organization = new Organization();
	organizationId: number;
	countries = [];
	businessLevels;

	countySelection = new SelectionModel<County>(true, []);
	counties: Array<County>;

	visitAddress: PostAddress = new PostAddress();
	deliveryAddress: PostAddress = new PostAddress();
	users = [];
	filteredUsers = [];
	dataSource = null;
	query = '';
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	searchTotal;
	offset = 25;
	totalPages;
	currentPage = 1;
	paginatorArray = [];
	allPages;
	showInactiveUsers = false;
	helpTexts;
	helpTextsLoaded = false;
	flexDirection: string;
	isServiceOwner: boolean;
	tabName = "";
	isSuperAdmin = false;
	userId;
	organizationInactivationDate;

	adminUserIsSupplier;
	adminUserIsCustomer;
	adminUserIsServiceOwner;

	// Permissions
	hasPermissionToUpdateOrganization: boolean;
	hasPermissionToDeleteOrganization: boolean;
	hasPermissionToUpdateContact: boolean;
	hasPermissionToCreateBusinessLevel: boolean;
	hasPermissionToInactivateBusinessLevel: boolean;
	hasPermissionToCreateUser: boolean;
	hasPermissionToCreateOwnUser: boolean;
	hasPermissionToViewUser: boolean;
	isOwnOrganization = false;
	isSupplier = false;

	// Form controls
	nameFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	organizationNumberFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	glnFormControl = new FormControl(null, {
		validators: Validators.pattern('^(\\d{13})?$'),
		updateOn: 'blur'
	});

	validFromFormControl = new FormControl(null, {
		updateOn: 'blur'
	});
	validToFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	webFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	emailFormControl = new FormControl('', {
		updateOn: 'blur'
	});

	visitZipFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	deliverZipFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	// Validation errors
	nameError;
	numberError;
	glnError;
	validFromError;
	validToError;
	webError;
	emailError;
	visitZipError;
	deliverZipError;

	constructor(private organizationService: OrganizationService,
							private assortmentService: AssortmentService,
							private countryService: CountryService,
							private userService: UserService,
							private route: ActivatedRoute,
							private router: Router,
							private dialog: MatDialog,
							private alertService: AlertService,
							private authService: AuthService,
							private location: Location,
							private helpTextService: HelptextService,
							private commonService: CommonService) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}
		this.route.params.subscribe(params => {
			this.organizationId = params.id;
			if (params.tab === 'user') {
				this.initialTab = 2;
			}
		});
		this.isOwnOrganization = Number(this.organizationId) === this.authService.getOrganizationId();
		this.hasPermissionToUpdateContact = this.hasPermissionToUpdateContact && this.isOwnOrganization;
		this.hasPermissionToCreateOwnUser = this.hasPermissionToCreateOwnUser && this.isOwnOrganization;

		// The user is considered done with their search after 500 ms without further input
		this.searchChanged.pipe(
			debounceTime(500),
			distinctUntilChanged())
			.subscribe(query => {
					this.query = encodeURIComponent(query);
					this.onQueryChange();
				}
			);
	}

	ngOnInit() {
		this.hasPermissionToUpdateOrganization = this.hasPermission('organization:update');
		this.hasPermissionToDeleteOrganization = this.hasPermission('organization:delete');
		this.hasPermissionToUpdateContact = this.hasPermission('organization:update_contact');
		this.hasPermissionToCreateBusinessLevel = this.hasPermission('businesslevel:create');
		this.hasPermissionToInactivateBusinessLevel = this.hasPermission('businesslevel:inactivate');
		this.hasPermissionToCreateUser = this.hasPermission('user:create');
		this.hasPermissionToCreateOwnUser = this.hasPermission('user:create_own');
		this.hasPermissionToViewUser = this.hasPermission('user:view');

		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);
		this.isSuperAdmin = this.authService.isSuperAdmin();
		this.userId = this.authService.getUserId();
		this.organizationService.getOrganization(this.authService.getOrganizationId()).subscribe(
			res => {
				this.isServiceOwner = res.organizationType === 'SERVICE_OWNER';
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
		this.onEditModeChange();


		this.getCounties();


		this.countryService.getCountries().subscribe(
			res => {
				this.countries = res;
				this.organization.country = this.countries.find(x => x.name === 'Sverige');
				if (this.organizationId) {
					this.organizationNumberFormControl.disable();
					if (this.hasPermissionToViewUser) {
						this.searchUsers();
					}
					this.getOrganization();
				} else {
					// Set default values
					this.editMode = true;
					this.onEditModeChange();

					this.validFromFormControl.setValue(new Date());
					this.organization.electronicAddress = new ElectronicAddress();
					this.organization.postAddresses = new Array<PostAddress>();

					this.visitAddress.addressType = 'VISIT';
					this.deliveryAddress.addressType = 'DELIVERY';
					this.organization.organizationType = 'SUPPLIER';
					this.loaded = true;
				}
			}
		);

		this.adminOrganizationId = this.authService.getOrganizationId();
		this.getAdminOrganization();

		this.organizationService.getOrganization(this.authService.getOrganizationId()).subscribe(
			res => {
				res.organizationType === 'SUPPLIER' ? this.isSupplier = true : this.isSupplier = false;
			}
		);
	}

	getAdminOrganization() {
		this.organizationService.getOrganization(this.adminOrganizationId).subscribe(
			res => {
				this.adminUserIsSupplier = res.organizationType === 'SUPPLIER';
				this.adminUserIsCustomer = res.organizationType === 'CUSTOMER';
				this.adminUserIsServiceOwner = res.organizationType === 'SERVICE_OWNER';
			}
		);
	}


	onTabSelection(event) {
		this.tabName = event.tab.textLabel;
		if (event.index === 0) {
			this.showSaveBar = true;
			this.getBusinessLevels();
			if (!this.isSupplier)
				this.getCounties();
		} else {
			this.showSaveBar = false;
		}
	}

	toggleEditMode() {
		this.editMode = !this.editMode;
		this.onEditModeChange();
	}

	openConfirmationDialog(organizationId, userId) {
		const dialogRef = this.dialog.open(DeleteUserConfirmationDialogComponent, {
			width: '40%',
			data: {
				userId: userId,
				organizationId: organizationId
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			this.searchUsers();
		});
	}

	onEditModeChange() {
		if (!this.editMode) {
			this.glnFormControl.disable();
			this.nameFormControl.disable();
			this.validFromFormControl.disable();
			this.validToFormControl.disable();
			this.emailFormControl.disable();
			this.webFormControl.disable();
			this.visitZipFormControl.disable();
			this.deliverZipFormControl.disable();
		} else {
			if (this.hasPermissionToUpdateOrganization) {
				this.glnFormControl.enable();
				this.nameFormControl.enable();
				this.validFromFormControl.enable();
				this.validToFormControl.enable();
				this.emailFormControl.enable();
				this.webFormControl.enable();
				this.visitZipFormControl.enable();
				this.deliverZipFormControl.enable();
			}
			if (this.hasPermissionToUpdateContact && !this.hasPermissionToUpdateOrganization) {
				this.emailFormControl.enable();
				this.webFormControl.enable();
				this.visitZipFormControl.enable();
				this.deliverZipFormControl.enable();
			}
		}
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	getOrganization() {
		this.loaded = false;
		this.organizationService.getOrganization(this.organizationId).subscribe(
			res => {
				this.organization = res;
				this.nameFormControl.setValue(res.organizationName);
				this.organizationNumberFormControl.setValue(res.organizationNumber);
				this.glnFormControl.setValue(res.gln);
				this.webFormControl.setValue(res.electronicAddress.web);
				this.emailFormControl.setValue(res.electronicAddress.email);
				this.organizationInactivationDate = res.validTo;
				this.splitPostAddresses();
				this.convertToDateString();


				if (!this.isSupplier && this.organization.counties && this.organization.counties.length) {
					this.counties.forEach(county => {
					this.organization.counties.forEach(selectedCounty => {
							if (county.id === selectedCounty.id) {
								this.countySelection.select(county);
							}
						});
					});
					this.loaded =true;
				}

				// Extract business levels
				this.getBusinessLevels();
			}, error => {
				this.loaded =true;
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	getBusinessLevels() {
		this.organizationService.getBusinessLevels(this.organizationId).subscribe({
			next: res => {
				if (this.isSuperAdmin) {
					this.businessLevels = res;
				} else {
					this.businessLevels = res.filter((level => level.status === 'ACTIVE'));
				}
				this.loaded = true;
			},
			error: error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		});
	}

	getCounties() {
		if (!this.isSupplier) {
			this.loaded = false;
			this.assortmentService.getCounties().subscribe({
				next: res => {
					this.counties = res;
					this.loaded = true;
				},
				error: error => {
					this.loaded = true;
					this.alertService.clear();
					error.error.errors.forEach((err: any) => {
						this.alertService.error(err);
					});
				}
			});
		}
	}

	saveOrganization() {
		this.saving = true;
		this.resetValidation();

		this.organization.validFrom = this.getMillisecondsFromDate(this.validFromFormControl.value);
		this.organization.validTo = this.getMillisecondsFromDate(this.validToFormControl.value);
		this.organization.organizationName = this.nameFormControl.value;
		this.organization.organizationNumber = this.organizationNumberFormControl.value;
		this.organization.gln = this.glnFormControl.value;
		this.organization.electronicAddress.web = this.webFormControl.value;
		this.organization.electronicAddress.email = this.emailFormControl.value;

		this.organization.postAddresses = [];
		this.visitAddress.postCode = this.visitZipFormControl.value;
		this.organization.postAddresses.push(this.visitAddress);
		this.deliveryAddress.postCode = this.deliverZipFormControl.value;
		this.organization.postAddresses.push(this.deliveryAddress);

		if (this.isSuperAdmin) {
			this.organization.counties = this.countySelection.selected;
		}

		if (this.organizationId) {
			this.updateOrganization();
		} else {
			this.createOrganization();
		}
	}

	updateOrganization() {
		this.organizationService.updateOrganization(this.organizationId, this.organization).subscribe({
			next: data => {
				this.organization = data;
				this.toggleEditMode();
				this.saving = false;
				this.alertService.success('Sparat');
			},
			error: error => {
				this.alertService.clear();
				this.saving = false;
				error.error.errors.forEach(err => {
					this.handleServerValidation(err);
					this.alertService.error(err);
				});
			}
		});
	}

	createOrganization() {
		this.organizationService.createOrganization(this.organization).subscribe({
			next: data => {
				this.organizationId = data.id;
				this.organization = data;
				this.toggleEditMode();
				this.saving = false;
				this.router.navigate(['/organization/' + data.id + '/handle']);
				this.alertService.success('Sparat');
			},
			error: error => {

				this.alertService.clear();
				this.saving = false;
				error.error.errors.forEach(err => {
					this.handleServerValidation(err);
					this.alertService.error(err);
				});
			}
		});
	}

	deleteOrganization() {
		this.saving = true;
		this.organizationService.deleteOrganization(this.organizationId).subscribe({
			next: res => {
				this.saving = false;
				this.router.navigate(['/organization']);
				this.alertService.success('Organisation borttagen');
			},
			error: error => {
				this.alertService.clear();
				this.saving = false;
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		});
	}

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
	}

	onQueryChange() {
		this.searchUsers();
	}

	onShowInactiveUsersChange() {
		this.filterUsers();
	}

	searchUsers() {
		this.userService.searchUsers(this.organizationId, this.query).subscribe(
			res => {
				this.searchTotal = res.headers.get('X-Total-Count');
				if (this.searchTotal !== '0') {
					this.users = res.body;
				} else {
					this.users = [];
				}
				this.totalPages = Math.ceil(this.searchTotal / this.offset);
				this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
				this.filterUsers();
			}
		);
	}

	exportSearchResults() {
		const query = '?query=' + this.query;

		this.userService.exportUsers(!this.showInactiveUsers, this.organizationId, query).subscribe(
			data => {
				saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	filterUsers() {
		if (this.showInactiveUsers) {
			this.filteredUsers = this.users;
		} else {
			this.filteredUsers = this.users.filter(
				user => user.active);
		}
		this.dataSource = new MatTableDataSource<User>(this.filteredUsers);
		this.generatePaginator();
	}

	goToPage(page) {
		if (page !== '...' && page !== this.currentPage) {
			this.currentPage = page;
			this.userService.searchUsers(this.organizationId, this.query + '&offset=' + (this.currentPage - 1) * this.offset).subscribe(
				res => {
					this.users = res.body;
					this.filterUsers();
					this.currentPage = page;
					this.generatePaginator();
				}, error => {
					this.alertService.clear();
					error.error.errors.forEach(err => {
						this.alertService.error(err);
					});
				}
			);
		}
	}

	generatePaginator() {
		let hasFirstEllipsis = false;
		let hasSecondEllipsis = false;
		this.paginatorArray = [];
		if (this.searchTotal > this.offset) {
			if (this.totalPages < 6) {
				this.paginatorArray = this.allPages;
			} else {
				this.allPages.forEach((pageNumber) => {
					if (pageNumber === 1
						|| pageNumber === this.currentPage
						|| pageNumber === this.totalPages
						|| pageNumber === this.currentPage + 1
						|| pageNumber === this.currentPage - 1) {
						this.paginatorArray.push(pageNumber);
					} else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
						this.paginatorArray.push('...');
						hasFirstEllipsis = true;
					} else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
						this.paginatorArray.push('...');
						hasSecondEllipsis = true;
					}
				});
			}
		}
	}

	splitPostAddresses() {
		this.visitAddress = new PostAddress();
		this.deliveryAddress = new PostAddress();
		if (this.organization.postAddresses[0].addressType === 'VISIT') {
			this.visitAddress = this.organization.postAddresses[0];
			this.visitZipFormControl.setValue(this.organization.postAddresses[0].postCode);
		} else {
			this.deliveryAddress = this.organization.postAddresses[0];
			this.deliverZipFormControl.setValue(this.organization.postAddresses[0].postCode);
		}

		if (this.organization.postAddresses[1].addressType === 'VISIT') {
			this.visitAddress = this.organization.postAddresses[1];
			this.visitZipFormControl.setValue(this.organization.postAddresses[1].postCode);
		} else {
			this.deliveryAddress = this.organization.postAddresses[1];
			this.deliverZipFormControl.setValue(this.organization.postAddresses[1].postCode);
		}
	}

	convertToDateString() {
		this.validFromFormControl.setValue(new Date(this.organization.validFrom));
		if (this.organization.validTo) {
			this.validToFormControl.setValue(new Date(this.organization.validTo));
		} else {
			this.validToFormControl.setValue(null);
		}
	}

	getMillisecondsFromDate(date): number {
		if (date) {
			return date.getTime();
		}
		return null;
	}

	handleServerValidation(error): void {
		switch (error.field) {
			case 'organizationName': {
				this.nameError = error.message;
				this.nameFormControl.setErrors(Validators.pattern(''));
				break;
			}
			case 'organizationNumber': {
				this.numberError = error.message;
				this.organizationNumberFormControl.setErrors(Validators.pattern(''));
				break;
			}
			case 'gln': {
				this.glnError = error.message;
				this.glnFormControl.setErrors(Validators.pattern(''));
				break;
			}
			case 'validFrom': {
				this.validFromError = error.message;
				this.validFromFormControl.setErrors(Validators.pattern(''));
				break;
			}
			case 'validTo': {
				this.validToError = error.message;
				this.validToFormControl.setErrors(Validators.pattern(''));
				break;
			}
			case 'electronicAddress.web': {
				this.webError = error.message;
				this.webFormControl.setErrors(Validators.pattern(''));
				break;
			}
			case 'electronicAddress.email': {
				this.emailError = error.message;
				this.emailFormControl.setErrors(Validators.pattern(''));
				break;
			}
			case 'postAddresses[0].postCode': {
				this.visitZipError = error.message;
				this.visitZipFormControl.setErrors(Validators.pattern(''));
				break;
			}
			case 'postAddresses[1].postCode': {
				this.deliverZipError = error.message;
				this.deliverZipFormControl.setErrors(Validators.pattern(''));
				break;
			}
		}
	}

	resetValidation() {
		this.nameError = null;
		this.nameFormControl.markAsDirty();
		this.numberError = null;
		this.organizationNumberFormControl.markAsDirty();
		this.glnError = null;
		this.glnFormControl.markAsDirty();
		this.validFromError = null;
		this.validFromFormControl.markAsDirty();
		this.validToError = null;
		this.validToFormControl.markAsDirty();
		this.webError = null;
		this.webFormControl.markAsDirty();
		this.emailError = null;
		this.emailFormControl.markAsDirty();
		this.visitZipError = null;
		this.visitZipFormControl.markAsDirty();
		this.deliverZipError = null;
		this.deliverZipFormControl.markAsDirty();
	}

	cancel() {
		this.getOrganization();
		this.toggleEditMode();
	}

	goBack() {
		this.location.back();
	}

	resetValidTo() {
		this.validToFormControl.reset(null);
	}

	async routeTo(element: any): Promise<void> {
		await this.router.navigateByUrl("/organization/" + this.organizationId + "/user/" + element.id + "/handle");
	}

}

export interface User {
	name: string;
	username: string;
	title: string;
	email: string;
	status: string;
}
