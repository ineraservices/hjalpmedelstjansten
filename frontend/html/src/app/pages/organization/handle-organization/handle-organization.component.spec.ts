import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HandleOrganizationComponent} from './handle-organization.component';

describe('HandleOrganizationComponent', () => {
	let component: HandleOrganizationComponent;
	let fixture: ComponentFixture<HandleOrganizationComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [HandleOrganizationComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(HandleOrganizationComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
