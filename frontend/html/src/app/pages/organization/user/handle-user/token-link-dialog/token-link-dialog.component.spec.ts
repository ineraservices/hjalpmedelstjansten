import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TokenLinkDialogComponent} from './token-link-dialog.component';

describe('TokenLinkDialogComponent', () => {
	let component: TokenLinkDialogComponent;
	let fixture: ComponentFixture<TokenLinkDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [TokenLinkDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(TokenLinkDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
