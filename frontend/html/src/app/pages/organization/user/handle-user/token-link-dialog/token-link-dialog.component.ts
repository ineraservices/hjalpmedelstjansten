import {Component, Inject} from '@angular/core';
import {UploadDialogComponent} from '../../../../product/upload-dialog/upload-dialog.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
	selector: 'app-token-link-dialog',
	templateUrl: './token-link-dialog.component.html',
	styleUrls: ['./token-link-dialog.component.scss']
})
export class TokenLinkDialogComponent {

	constructor(public dialogRef: MatDialogRef<UploadDialogComponent>,
							@Inject(MAT_DIALOG_DATA) public data: any) {
	}

}
