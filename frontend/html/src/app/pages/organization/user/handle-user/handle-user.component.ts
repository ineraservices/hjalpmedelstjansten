import {Component, OnInit, ViewChildren} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../../../models/user/user.model';
import {Organization} from '../../../../models/organization/organization.model';
import {OrganizationService} from '../../../../services/organization.service';
import {BusinessLevel} from '../../../../models/organization/business-level.model';
import {UserService} from '../../../../services/user.service';
import {AlertService} from '../../../../services/alert.service';
import {ElectronicAddress} from '../../../../models/electronic-address.model';
import {UserEngagement} from '../../../../models/user/user-engagement.model';
import {MatDialog} from '@angular/material/dialog';
import {MatInput} from '@angular/material/input';
import {AuthService} from '../../../../auth/auth.service';
import {Role} from '../../../../models/user/role.model';
import {SelectionModel} from '@angular/cdk/collections';
import {TokenLinkDialogComponent} from './token-link-dialog/token-link-dialog.component';
import {HelptextService} from '../../../../services/helptext.service';
import {CommonService} from '../../../../services/common.service';

@Component({
	selector: 'app-handle-user',
	templateUrl: './handle-user.component.html',
	styleUrls: ['./handle-user.component.scss']
})
export class HandleUserComponent implements OnInit {
	@ViewChildren(MatInput) input: Array<MatInput>;
	loaded = false;
	saving = false;
	editMode = false;
	user: User = new User();
	organization: Organization;
	organizationId: number;
	userId: number;
	isSuperAdmin = false;
	businessLevels: Array<BusinessLevel>;
	roles: Array<Role>;
	selectedBusinessLevels: Array<BusinessLevel>;
	selection = new SelectionModel<Role>(true, []);
	helpTexts;
	helpTextsLoaded = false;
	flexDirection: string;

	// Permissions
	hasPermissionToUpdateUser: boolean;
	hasPermissionToUpdateOwnUser: boolean;
	hasPermissionToUpdateContact: boolean;
	isOwnOrganization = false;
	isOwnUser = false;

	// Form controls

	userNameFormControl = new FormControl('', {
		updateOn: 'blur'
	});

	firstNameFormControl = new FormControl('', {
		updateOn: 'blur'
	});

	lastNameFormControl = new FormControl('', {
		updateOn: 'blur'
	});

	validFromFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	validToFormControl = new FormControl(null, {
		updateOn: 'blur'
	});

	emailFormControl = new FormControl('', {
		validators: Validators.email,
		updateOn: 'blur'
	});

	// validation
	usernameError;
	firstNameError;
	lastNameError;
	validFromError;
	validToError;
	emailError;

	constructor(private organizationService: OrganizationService,
							private userService: UserService,
							private route: ActivatedRoute,
							private alertService: AlertService,
							private authService: AuthService,
							private router: Router,
							private dialog: MatDialog,
							private helpTextService: HelptextService,
							private commonService: CommonService) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}
		this.route.params.subscribe(params => {
			this.organizationId = params.organizationId;
			this.userId = params.userId;

			this.isOwnOrganization = Number(this.organizationId) === this.authService.getOrganizationId();
			this.hasPermissionToUpdateOwnUser = this.hasPermissionToUpdateOwnUser && this.isOwnOrganization;

			this.isOwnUser = Number(this.userId) === this.authService.getUserId();
			this.hasPermissionToUpdateContact = this.hasPermissionToUpdateContact && this.isOwnUser;
		});

	}

	ngOnInit() {
		this.hasPermissionToUpdateUser = this.hasPermission('user:update');
		this.hasPermissionToUpdateOwnUser = this.hasPermission('user:update_own');
		this.hasPermissionToUpdateContact = this.hasPermission('user:update_contact');

		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);
		this.isSuperAdmin = this.authService.isSuperAdmin();
		if (!this.hasPermissionToUpdateUser && !this.hasPermissionToUpdateOwnUser && !this.hasPermissionToUpdateContact) {
			this.emailFormControl.disable();
		}

		this.initUser();

		this.organizationService.getBusinessLevels(this.organizationId).subscribe(
			levels => {
				const businessLevels = levels.filter(level => level.status === 'ACTIVE');
				this.businessLevels = businessLevels;
				this.organizationService.getRoles(this.organizationId).subscribe(
					roles => {
						this.roles = roles;
						this.organizationService.getOrganization(this.organizationId).subscribe(
							org => {
								this.organization = org;
								if (this.userId) {
									this.getUser();
								} else {
									// TODO! check if this date works as expected when initial value of formcontrol is set to null
									this.validFromFormControl.setValue(new Date());
									this.user.loginType = 'PASSWORD';
									this.editMode = true;
									this.loaded = true;
								}
								this.onEditModeChange();
							}
						);
					}
				);
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	toggleEditMode() {
		this.editMode = !this.editMode;
		this.onEditModeChange();
	}

	onEditModeChange() {
		if (!this.editMode) {
			this.userNameFormControl.disable();
			this.firstNameFormControl.disable();
			this.lastNameFormControl.disable();
			this.validFromFormControl.disable();
			this.validToFormControl.disable();
			this.emailFormControl.disable();
		} else {
			if (this.hasPermissionToUpdateUser || this.hasPermissionToUpdateOwnUser) {
				this.userNameFormControl.enable();
				this.firstNameFormControl.enable();
				this.lastNameFormControl.enable();
				this.validFromFormControl.enable();
				this.validToFormControl.enable();
				this.emailFormControl.enable();
			} else if (this.hasPermissionToUpdateContact) {
				this.emailFormControl.enable();
			}
		}
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	initUser() {
		this.user.electronicAddress = new ElectronicAddress();
		this.user.userEngagements = new Array<UserEngagement>();
		this.user.userEngagements.push(new UserEngagement());

		// Default values
		this.user.userEngagements[0].organizationId = this.organizationId;
	}

	getUser() {
		this.userService.getUser(this.organizationId, this.userId).subscribe(
			res => {
				this.user = res;

				// Extract business levels
				this.selectedBusinessLevels = new Array<BusinessLevel>();
				if (res.userEngagements[0].businessLevels) {
					this.selectedBusinessLevels = this.businessLevels.filter(function (o) {
						return res.userEngagements[0].businessLevels.some(function (o2) {
							return o.id === o2.id;
						});
					});
				}
				// Extract roles
				if (res.userEngagements[0].roles) {
					this.roles.forEach(role => {
						if (res.userEngagements[0].roles.find(x => x.id === role.id)) {
							this.selection.select(role);
						}
					});
				}
				// Set form control values
				this.userNameFormControl.setValue(res.username);
				this.firstNameFormControl.setValue(res.firstName);
				this.lastNameFormControl.setValue(res.lastName);
				this.emailFormControl.setValue(res.electronicAddress.email);
				this.convertToDateString();
				this.loaded = true;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	onSelectionChange(event, element) {
		this.selection.toggle(element);
	}

	saveUser() {
		this.saving = true;
		this.resetValidation();
		this.user.userEngagements[0].validFrom = this.getMillisecondsFromDate(this.validFromFormControl.value);
		this.user.userEngagements[0].validTo = this.getMillisecondsFromDate(this.validToFormControl.value);
		this.user.userEngagements[0].businessLevels = [];
		if (this.selectedBusinessLevels) {
			this.selectedBusinessLevels.forEach(level => {
				this.user.userEngagements[0].businessLevels.push(level);
			});
		}
		if (this.selection.selected) {
			this.user.userEngagements[0].roles = this.selection.selected;
		}
		this.user.username = this.userNameFormControl.value;
		if (!this.user.username) {
			this.user.username = null;
		}
		this.user.firstName = this.firstNameFormControl.value;
		if (!this.user.firstName) {
			this.user.firstName = null;
		}
		this.user.lastName = this.lastNameFormControl.value;
		if (!this.user.lastName) {
			this.user.lastName = null;
		}
		this.user.electronicAddress.email = this.emailFormControl.value;

		if (this.userId) {
			this.updateUser();
		} else {
			this.createUser();
		}
	}

	updateUser() {
		this.userService.updateUser(this.organizationId, this.userId, this.user).subscribe(
			res => {
				this.toggleEditMode();
				this.saving = false;
				this.alertService.success('Sparat');
			}, error => {
				this.alertService.clear();
				this.saving = false;
				error.error.errors.forEach(err => {
					this.handleServerValidation(err);
					this.alertService.error(err);
				});
			}
		);
	}

	createUser() {
		this.userService.createUser(this.organizationId, this.user).subscribe(
			res => {
				this.user = res;
				this.userId = res.id;
				this.toggleEditMode();
				this.saving = false;
				this.alertService.success('Sparat');
				if (res.loginUrl) {
					const dialogRef = this.dialog.open(TokenLinkDialogComponent, {
						width: '80%',
						data: res.loginUrl
					});
				}
			}, error => {
				this.alertService.clear();
				this.saving = false;
				if (error) {
					error.error.errors.forEach(err => {
						this.handleServerValidation(err);
						this.alertService.error(err);
					});
				} else {
					this.alertService.errorWithMessage('Någonting gick fel');
				}
			}
		);
	}

	deleteUser() {
		this.userService.deleteUser(this.organizationId, this.userId, null).subscribe(
			res => {
				this.alertService.success('Sparat');
				this.router.navigate(['/organization/' + this.organizationId + '/handle/user']);
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	convertToDateString() {
		// TODO! Check if these dates works as intended now when initial value of formcontrol is set to null
		this.validFromFormControl.setValue(new Date(this.user.userEngagements[0].validFrom));
		if (this.user.userEngagements[0].validTo) {
			this.validToFormControl.setValue(new Date(this.user.userEngagements[0].validTo));
		}
	}

	getMillisecondsFromDate(date): number {
		if (date) {
			return date.getTime();
		}
		return null;
	}

	handleServerValidation(error: any): void {
		switch (error.field) {
		case 'username': {
			this.usernameError = error.message;
			break;
		}
		case 'firstName': {
			this.firstNameError = error.message;
			break;
		}
		case 'lastName': {
			this.lastNameError = error.message;
			break;
		}
		case 'userEngagements[0].validFrom': {
			this.validFromError = error.message;
			break;
		}
		case 'userEngagements[0].validTo': {
			this.validToError = error.message;
			break;
		}
		case 'electronicAddress.email': {
			this.emailError = error.message;
			break;
		}
		}
	}

	resetValidation() {
		this.firstNameError = null;
		this.firstNameFormControl.markAsDirty();
		this.lastNameError = null;
		this.lastNameFormControl.markAsDirty();
		this.emailError = null;
		this.emailFormControl.markAsDirty();
		this.userNameFormControl.markAsDirty();
		this.usernameError = null;
		this.validFromFormControl.markAsDirty();
		this.validFromError = null;
		this.validToFormControl.markAsDirty();
		this.validToError = null;
	}

	cancel() {
		this.getUser();
		this.toggleEditMode();
	}

	goBack() {
		this.router.navigate(['/organization/' + this.organizationId + '/handle/user']);
	}

}
