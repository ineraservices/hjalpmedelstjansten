import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {OrganizationService} from '../../services/organization.service';
import {AuthService} from '../../auth/auth.service';
import {HelptextService} from '../../services/helptext.service';
import {AlertService} from '../../services/alert.service';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {StorageHelper as OrganisationStorage} from '../../helpers/storage.helper';
import {saveAs} from "file-saver";
import {Router} from "@angular/router";

@Component({
	selector: 'app-organization',
	templateUrl: './organization.component.html',
	styleUrls: ['./organization.component.scss']
})
export class OrganizationComponent implements OnInit {

	displayedColumns = ['organisation', 'orggrupp', 'orgnr', 'gln', 'land', 'status'];

	// Sets the storage index to be used.
	storage = new OrganisationStorage('Organization');
	loadSessionStorage = false;

	loaded = false;
	loading = true;
	organizations = [];
	dataSource = null;
	query = '';
	queryParams = '';
	tmpQuery = '';
	searchChanged: Subject<string> = new Subject<string>();
	searchTotal;
	offset = 25;
	totalPages;
	currentPage = 1;
	allPages;
	helpTexts;
	helpTextsLoaded = false;
	isServiceOwner: boolean;

	// Permissions
	hasPermissionToCreateOrganization: boolean;

	constructor(private organizationService: OrganizationService,
							private authService: AuthService,
							private alertService: AlertService,
							private helpTextService: HelptextService,
							private router: Router) {
		// The user is considered done with their search after 500 ms without further input
		this.searchChanged.pipe(
			debounceTime(500),
			distinctUntilChanged())
			.subscribe(query => {
				this.query = encodeURIComponent(query);
				this.onQueryChange();
				this.currentPage = 1;
			}
			);
	}

	ngOnInit() {
		this.hasPermissionToCreateOrganization = this.hasPermission('organization:create');

		this.organizationService.getOrganization(this.authService.getOrganizationId()).subscribe(
			res => {
				this.isServiceOwner = res.organizationType === 'SERVICE_OWNER';
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
		this.helpTexts = this.helpTextService.getTexts().subscribe(
			texts => {
				this.helpTexts = texts;
				this.helpTextsLoaded = true;
			}
		);


		if (this.storage.hasPageBeenVisisted()) {
			this.loadSessionStorage = true;
			this.getSessionStorage();

			if (this.currentPage > 1) {
				this.goToPage(this.currentPage);
			} else {
				this.searchOrganizations(this.tmpQuery);
			}

			this.loadSessionStorage = false;
		} else {
			this.searchOrganizations('');
		}
	}

	onQueryChanged(text: string) {
		this.searchChanged.next(text);
	}

	onQueryChange() {
		this.searchOrganizations(this.query);
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	setSessionStorage() {

		this.storage.setItem('Query', this.tmpQuery.toString());
		this.storage.setItem('CurrentPage', this.currentPage.toString());
		this.storage.setItem('HelpTextsLoaded', this.helpTextsLoaded.toString());
	}

	getSessionStorage() {

		this.currentPage = this.storage.getItemNumber('CurrentPage');
		this.helpTextsLoaded = this.storage.getItemBoolean('HelpTextsLoaded');

		// Query needs to be at the end of the method.
		if (this.storage.getItemString('Query') !== 'null' || this.storage.getItemString('Query') !== '') {
			this.tmpQuery = this.storage.getItemString('Query');
			this.query = this.storage.getItemString('Query');
		}
	}

	searchOrganizations(query) {
		this.loading = true;
		this.organizationService.searchOrganizations(query).subscribe(
			res => {
				this.searchTotal = res.headers.get('X-Total-Count');
				this.organizations = this.searchTotal !== '0' ? res.body : [];
				this.totalPages = Math.ceil(this.searchTotal / this.offset);
				this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
				this.dataSource = new MatTableDataSource<Organization>(this.organizations);
				this.setSessionStorage();
				this.loading = false;
				this.loaded = true;
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	exportSearchResults() {
		const query = '?query=' + this.query + this.queryParams;

		this.organizationService.exportOrganizations(query).subscribe(
			data => {
				saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
			}, error => {
				this.alertService.clear();
				error.error.errors.forEach(err => {
					this.alertService.error(err);
				});
			}
		);
	}

	goToPage(page) {
		if (page !== '...' && page !== this.currentPage || this.loadSessionStorage) {
			this.searchOrganizations(this.query + '&offset=' + (this.currentPage - 1) * this.offset);
		}
	}

	async routeTo(element: any): Promise<void> {
		await this.router.navigateByUrl("/organization/" + element.id + "/handle");
	}
}

export interface Organization {
	organization: string;
	type: string;
	orgnumber: string;
	country: string;
	status: string;
}


