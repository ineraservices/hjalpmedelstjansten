import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UpdateBusinesslevelDialogComponent} from './update-businesslevel-dialog.component';

describe('UpdateBusinesslevelDialogComponent', () => {
	let component: UpdateBusinesslevelDialogComponent;
	let fixture: ComponentFixture<UpdateBusinesslevelDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [UpdateBusinesslevelDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(UpdateBusinesslevelDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
