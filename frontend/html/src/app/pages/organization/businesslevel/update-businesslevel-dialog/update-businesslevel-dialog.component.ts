import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
	selector: 'app-update-businesslevel-dialog',
	templateUrl: './update-businesslevel-dialog.component.html',
	styleUrls: ['./update-businesslevel-dialog.component.scss']
})
export class UpdateBusinesslevelDialogComponent implements OnInit {
	organizationId: number;
	id: string;
	businessLevelName: string;


	constructor(public dialogRef: MatDialogRef<UpdateBusinesslevelDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
	}

	ngOnInit() {
		this.organizationId = this.data.organizationId;
		this.id = this.data.businessLevelId;
		this.businessLevelName = this.data.businessLevelName;
	}

	done() {
		this.dialogRef.close(
			{
				'id': this.id,
				'name': this.businessLevelName
			}
		);
	}
}
