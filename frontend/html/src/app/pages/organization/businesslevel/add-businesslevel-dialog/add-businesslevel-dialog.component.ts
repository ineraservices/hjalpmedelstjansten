import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
	selector: 'app-add-businesslevel-dialog',
	templateUrl: './add-businesslevel-dialog.component.html',
	styleUrls: ['./add-businesslevel-dialog.component.scss']
})
export class AddBusinesslevelDialogComponent {
	businessLevelName: string;

	constructor(public dialogRef: MatDialogRef<AddBusinesslevelDialogComponent>) {
	}

	done() {
		this.dialogRef.close(this.businessLevelName);
	}

}
