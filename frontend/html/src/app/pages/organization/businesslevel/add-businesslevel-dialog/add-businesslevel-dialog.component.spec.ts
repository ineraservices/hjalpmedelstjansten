import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AddBusinesslevelDialogComponent} from './add-businesslevel-dialog.component';

describe('AddBusinesslevelDialogComponent', () => {
	let component: AddBusinesslevelDialogComponent;
	let fixture: ComponentFixture<AddBusinesslevelDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [AddBusinesslevelDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(AddBusinesslevelDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
