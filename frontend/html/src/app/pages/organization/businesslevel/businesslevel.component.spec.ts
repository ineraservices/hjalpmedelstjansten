import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BusinesslevelComponent} from './businesslevel.component';

describe('BusinesslevelComponent', () => {
	let component: BusinesslevelComponent;
	let fixture: ComponentFixture<BusinesslevelComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [BusinesslevelComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(BusinesslevelComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
