import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: 'organizationBelonging'
})
export class OrganizationBelongingPipe implements PipeTransform {

	transform(value: string, organizationName: string): string {
		return organizationName + ' - ' + value;
	}

}
