import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: 'formatArticleType'
})
export class FormatArticleTypePipe implements PipeTransform {
	transform(value: string): string {
		switch (value.toLowerCase()) {
		case 'h': {
			return 'Huvudhjälpmedel';
		}
		case 't': {
			return 'Tillbehör';
		}
		case 'r': {
			return 'Reservdel';
		}
		case 'tj': {
			return 'Tjänst';
		}
		case 'i': {
			return 'Inställning';
		}
		}
	}
}
