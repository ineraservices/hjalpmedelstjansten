import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: 'formatOrganizationStatus'
})
export class FormatOrganizationStatusPipe implements PipeTransform {
	transform(value: boolean): string {

		return value ? 'Aktiv' : 'Inaktiv';
	}
}
