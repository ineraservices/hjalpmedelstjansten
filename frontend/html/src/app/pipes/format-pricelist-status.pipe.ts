import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: 'formatPricelistStatus'
})
export class FormatPricelistStatusPipe implements PipeTransform {
	transform(value: string): string {
		switch (value) {
		case 'CURRENT': {
			return 'Aktuell';
		}
		case 'FUTURE': {
			return 'Framtida';
		}
		// case 'DISCONTINUED': {
		//  return 'Utgånget';
		// }
		case 'PAST': {
			return 'Historisk';
		}
		default:
			return value;
		}
	}
}
