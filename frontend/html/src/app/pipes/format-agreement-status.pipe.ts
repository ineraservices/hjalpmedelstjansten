import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: 'formatAgreementStatus'
})
export class FormatAgreementStatusPipe implements PipeTransform {
	transform(value: string): string {
		switch (value) {
		case 'CURRENT': {
			return 'Aktuellt';
		}
		case 'FUTURE': {
			return 'Framtida';
		}
		case 'DISCONTINUED': {
			return 'Utgånget';
		}
		// case 'PAST': {
		//  return 'Historisk';
		// }
		default:
			return value;
		}
	}
}
