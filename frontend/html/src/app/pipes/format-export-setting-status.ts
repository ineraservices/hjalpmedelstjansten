import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: 'formatExportSettingStatus'
})
export class FormatExportSettingStatusPipe implements PipeTransform {
	transform(value: boolean): string {

		return value ? 'Ja' : 'Nej';
	}
}
