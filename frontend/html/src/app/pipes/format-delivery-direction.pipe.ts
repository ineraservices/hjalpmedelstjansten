import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: 'formatDeliveryDirection'
})
export class FormatDeliveryDirectionPipe implements PipeTransform {
	transform(value: string): string {
		switch (value) {
		case 'INBOUND': {
			return 'Inleverans';
		}
		case 'OUTBOUND': {
			return 'Utleverans';
		}
		default:
			return value;
		}
	}
}
