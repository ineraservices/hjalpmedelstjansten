/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: 'formatPricelistRowStatus'
})
export class FormatPricelistRowStatusPipe implements PipeTransform {
	transform(value: string): string {
		switch (value) {
		case 'CREATED': {
			return 'Ny';
		}
		case 'PENDING_APPROVAL': {
			return 'Skickad';
		}
		case 'DECLINED': {
			return 'Avböjd';
		}
		case 'ACTIVE': {
			return 'Aktiv';
		}
		case 'PENDING_INACTIVATION': {
			return 'Begärd';
		}
		case 'INACTIVE': {
			return 'Inaktiv';
		}
		case 'ACTIVE_EDIT': {
			return 'Aktiv';
		}
		case 'CHANGED': {
			return 'Ändrad';
		}
		default:
			return value;
		}
	}
}
