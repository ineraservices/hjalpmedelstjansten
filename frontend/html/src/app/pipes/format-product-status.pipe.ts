import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: 'formatProductStatus'
})
export class FormatProductStatusPipe implements PipeTransform {
	transform(value: string): string {

		return value === 'PUBLISHED' ? 'Aktiv' : 'Utgått';
	}
}
