import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: 'formatOrganizationType'
})
export class FormatOrganizationTypePipe implements PipeTransform {
	transform(value: string): string {
		switch (value) {
		case 'SERVICE_OWNER': {
			return 'Tjänsteägare';
		}
		case 'CUSTOMER': {
			return 'Kund';
		}
		case 'SUPPLIER': {
			return 'Leverantör';
		}
		default:
			return value;
		}
	}
}
