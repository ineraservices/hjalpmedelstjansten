import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../auth/auth-guard.service';

import {StartComponent} from '../pages/start/start.component';
import {AdministrationComponent} from '../pages/administration/administration.component';
import {CategoryAdminComponent} from '../pages/categoryadmin/categoryadmin.component';
import {HandleOrganizationComponent} from '../pages/organization/handle-organization/handle-organization.component';
import {OrganizationComponent} from '../pages/organization/organization.component';
import {HandleUserComponent} from '../pages/organization/user/handle-user/handle-user.component';
import {LoginComponent} from '../pages/login/login.component';
import {RecoverComponent} from '../pages/login/recover-password/recover-password.component';
import {CreatePasswordComponent} from '../pages/login/create-password/create-password/create-password.component';
import {ProductComponent} from '../pages/product/product.component';
import {ProductTabsComponent} from '../pages/product/product-tabs/product-tabs.component';
import {ArticleTabsComponent} from '../pages/product/article-tabs/article-tabs.component';
import {AgreementComponent} from '../pages/agreement/agreement.component';
import {HandleAgreementComponent} from '../pages/agreement/handle-agreement/handle-agreement.component';
import {HandlePricelistComponent} from '../pages/agreement/handle-pricelist/handle-pricelist.component';
import {
	HandleGeneralPricelistComponent
} from '../pages/general-pricelist/handle-general-pricelist/handle-general-pricelist.component';
import {
	HandleGeneralPricelistPricelistComponent
} from '../pages/general-pricelist/handle-general-pricelist-pricelist/handle-general-pricelist-pricelist.component';
import {
	ExportProductsAndArticlesComponent
} from '../pages/product/export-products-and-articles/export-products-and-articles.component';
import {SearchComponent} from '../pages/search/search.component';
import {ImportStatusComponent} from '../pages/importstatus/importstatus.component';
import {ExportfileComponent} from '../pages/exportfile/exportfile.component';
import {ExportsettingComponent} from '../pages/exportfile/exportsetting/exportsetting.component';
import {HandleExportfileComponent} from '../pages/exportfile/handle-exportfile/handle-exportfile.component';
import {AssortmentComponent} from '../pages/assortment/assortment.component';
import {HandleAssortmentComponent} from '../pages/assortment/handle-assortment/handle-assortment.component';
import {
	AddArticlesToAssortmentComponent
} from '../pages/assortment/handle-assortment/add-articles-to-assortment/add-articles-to-assortment.component';
import {
	ViewGeneralPricelistComponent
} from '../pages/general-pricelist/view-general-pricelist/view-general-pricelist.component';
import {DashboardComponent} from "../pages/start/dashboard/dashboard.component";
import {ProfileComponent} from "../modules/profile/profile.component";
import {UnauthorizedComponent} from "../pages/core/error-pages/unauthorized/unauthorized.component";

const routes: Routes = [
	{path: '', component: LoginComponent},
	{path: 'login/password/create', component: CreatePasswordComponent},
	{path: 'login/password/recover', component: RecoverComponent},
	{path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
	{path: 'exportsettings', component: ExportsettingComponent, canActivate: [AuthGuard]},
	{path: 'start', component: StartComponent},
	{path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
	{path: 'organization', component: OrganizationComponent, canActivate: [AuthGuard]},
	{path: 'search', component: SearchComponent, canActivate: [AuthGuard]},
	{path: 'importstatus', component: ImportStatusComponent, canActivate: [AuthGuard]},
	{path: 'administration', component: AdministrationComponent, canActivate: [AuthGuard]},
	{path: 'categoryadmin', component: CategoryAdminComponent, canActivate: [AuthGuard]},
	{path: 'generalpricelist', component: ViewGeneralPricelistComponent, canActivate: [AuthGuard]},
	{path: 'organization/handle', component: HandleOrganizationComponent, canActivate: [AuthGuard]},
	{path: 'organization/:id/handle', component: HandleOrganizationComponent, canActivate: [AuthGuard]},
	{path: 'organization/:id/handle/:tab', component: HandleOrganizationComponent, canActivate: [AuthGuard]},
	{path: 'organization/:organizationId/user/handle', component: HandleUserComponent, canActivate: [AuthGuard]},
	{path: 'organization/:organizationId/user/:userId/handle', component: HandleUserComponent, canActivate: [AuthGuard]},
	{path: 'organization/:organizationId/product', component: ProductComponent, canActivate: [AuthGuard]},
	{
		path: 'organization/:organizationId/product/export',
		component: ExportProductsAndArticlesComponent,
		canActivate: [AuthGuard]
	},
	{path: 'organization/:organizationId/product/handle', component: ProductTabsComponent, canActivate: [AuthGuard]},
	{path: 'organization/:organizationId/product/copy', component: ProductTabsComponent, canActivate: [AuthGuard]},
	{
		path: 'organization/:organizationId/product/:productId/handle',
		component: ProductTabsComponent,
		canActivate: [AuthGuard]
	},
	{path: 'organization/:organizationId/article/handle', component: ArticleTabsComponent, canActivate: [AuthGuard]},
	{path: 'organization/:organizationId/article/copy', component: ArticleTabsComponent, canActivate: [AuthGuard]},
	{
		path: 'organization/:organizationId/article/:articleId/handle',
		component: ArticleTabsComponent,
		canActivate: [AuthGuard]
	},
	{path: 'organization/:organizationId/agreement', component: AgreementComponent, canActivate: [AuthGuard]},
	{
		path: 'organization/:organizationId/agreement/handle',
		component: HandleAgreementComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'organization/:organizationId/agreement/:agreementId/handle',
		component: HandleAgreementComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'organization/:organizationId/agreement/:agreementId/pricelist/:pricelistId/handle',
		component: HandlePricelistComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'organization/:organizationId/agreement/:agreementId/pricelist/:pricelistId/handle/approvementId/:approvementId',
		component: HandlePricelistComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'organization/:organizationId/generalpricelist',
		component: HandleGeneralPricelistComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'organization/:organizationId/generalpricelist/:id',
		component: HandleGeneralPricelistComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'organization/:organizationId/generalpricelist/:id/pricelist/:pricelistId',
		component: HandleGeneralPricelistPricelistComponent,
		canActivate: [AuthGuard]
	},
	{path: 'organization/:organizationId/exportfile', component: ExportfileComponent, canActivate: [AuthGuard]},
	{
		path: 'organization/:organizationId/exportfile/:exportSettingId',
		component: HandleExportfileComponent,
		canActivate: [AuthGuard]
	},
	{path: 'organization/:organizationId/assortment', component: AssortmentComponent, canActivate: [AuthGuard]},
	{
		path: 'organization/:organizationId/assortment/handle',
		component: HandleAssortmentComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'organization/:organizationId/assortment/:assortmentId/handle',
		component: HandleAssortmentComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'organization/:organizationId/assortment/:assortmentId/add-article',
		component: AddArticlesToAssortmentComponent,
		canActivate: [AuthGuard]
	},
	{path: 'logged-out', component: UnauthorizedComponent},
	{path: '**', component: LoginComponent}

];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})

export class AppRoutingModule {
}
