import {Organization} from '../organization/organization.model';
import {User} from '../user/user.model';
import {County} from './county.model';
import {Municipality} from './municipality.model';
import {ExternalCategoryGrouping} from '../product/external-category-grouping.model';

export class Assortment {
	id: number;
	name: string;
	assortmentNumber: string;
	county: County;
	municipalities: Array<Municipality>;
	externalCategoriesGroupings: Array<ExternalCategoryGrouping>;
	validFrom: number;
	validTo: number;
	customer: Organization;
	managers: Array<User>;
}
