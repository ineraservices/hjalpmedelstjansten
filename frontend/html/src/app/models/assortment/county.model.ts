import {Municipality} from './municipality.model';

export class County {
	id: string;
	name: string;
	code: string;
	municipalities: Array<Municipality>;
}
