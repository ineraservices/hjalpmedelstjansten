export class Municipality {
	id: string;
	name: string;
	code: string;
}
