export class ImportStatus {
	uniqueId: number;
	importStatus: number;
	fileName: string;
	importStartDate: string;
	importEndDate: string;
	changedBy: string;
}
