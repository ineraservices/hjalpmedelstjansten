import {Country} from '../country.model';
import {ElectronicAddress} from '../electronic-address.model';
import {PostAddress} from '../post-address.model';
import {County} from "../assortment/county.model";

export class Organization {
	id: number;
	active: boolean;
	organizationName: string;
	organizationType: string;
	gln: number;
	organizationNumber: string;
	validFrom: number;
	validTo: number;
	country: Country;
	counties: Array<County>;
	electronicAddress: ElectronicAddress;
	postAddresses: Array<PostAddress>;
}
