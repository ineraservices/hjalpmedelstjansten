import {Organization} from './organization.model';

export class BusinessLevel {
	id: number;
	name: string;
	status: string;
	organization: Organization;
}
