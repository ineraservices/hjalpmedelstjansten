export class Login {
	username: string;
	password: string;
	newPassword: string;
	email: string;
	token: string;
}
