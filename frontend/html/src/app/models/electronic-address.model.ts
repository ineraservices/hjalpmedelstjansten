export class ElectronicAddress {
	id: string;
	email: string;
	mobile: string;
	telephone: string;
	web: string;
	fax: string;
}
