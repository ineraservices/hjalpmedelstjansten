import { PreventiveMaintenance } from '../preventive-maintenance.model';
import { Article } from '../product/article.model';
import { OrderUnit } from '../product/order-unit.model';
import { Product } from '../product/product.model';
import { Pricelist } from './pricelist.model';

export class Pricelistrow {
	id: number;
	price: number;
	status: string;
	validFrom: Date;
	validFromOverridden: boolean;
	leastOrderQuantity: number;
	deliveryTime: number;
	deliveryTimeOverridden: boolean;
	warrantyQuantity: number;
	warrantyQuantityOverridden: boolean;
	warrantyQuantityUnit: OrderUnit;
	warrantyQuantityUnitOverridden: boolean;
	warrantyValidFrom: PreventiveMaintenance;
	warrantyValidFromOverridden: boolean;
	warrantyTerms: string;
	warrantyTermsOverridden: boolean;
	pricelist: Pricelist;
	article: Article;
	basedOnProduct: Product;
}
