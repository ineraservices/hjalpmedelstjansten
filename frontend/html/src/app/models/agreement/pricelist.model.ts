import {Agreement} from './agreement.model';

export class Pricelist {
	id: number;
	number: string;
	status: string;
	validFrom: number;
	hasPricelistRows: boolean;
	agreement: Agreement;
}
