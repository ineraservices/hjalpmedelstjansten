export class PreventiveMaintenance {
	id: number;
	name: string;
	code: string;
}
