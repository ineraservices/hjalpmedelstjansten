import {GeneralPricelist} from './general-pricelist.model';

export class GeneralPricelistPricelist {
	id: number;
	number: string;
	status: string;
	validFrom: number;
	hasPricelistRows: boolean;
	generalPricelist: GeneralPricelist;
}
