import {OrderUnit} from '../product/order-unit.model';
import {Article} from '../product/article.model';
import {GeneralPricelistPricelist} from './general-pricelist-pricelist.model';

export class GeneralPricelistPricelistrow {
	id: number;
	price: number;
	status: string;
	validFrom: Date;
	validFromOverridden: boolean;
	leastOrderQuantity: number;
	deliveryTime: number;
	deliveryTimeOverridden: boolean;
	warrantyQuantity: number;
	warrantyQuantityOverridden: boolean;
	warrantyQuantityUnit: OrderUnit;
	warrantyQuantityUnitOverridden: boolean;
	warrantyValidFrom: string;
	warrantyValidFromOverridden: boolean;
	warrantyTerms: string;
	warrantyTermsOverridden: boolean;
	pricelist: GeneralPricelistPricelist;
	article: Article;
}
