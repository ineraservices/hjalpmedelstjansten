export class DynamicText {
	uniqueid: number;
	dynamicText: string;
	whenChanged: string;
	changedBy: string;
}
