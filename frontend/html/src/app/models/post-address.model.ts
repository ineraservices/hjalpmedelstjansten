export class PostAddress {
	addressType: string;
	streetAddress: string;
	postCode: string;
	city: string;
	id: string;
}
