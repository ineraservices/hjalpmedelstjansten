export class DocumentType {
	id: string;
	value: string;
	code: string;
}
