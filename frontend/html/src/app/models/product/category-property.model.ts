import {CategoryValue} from './category-value.model';

export class CategoryProperty {
	id: number;
	orderIndex: number;
	name: string;
	description: string;
	type: string;
	values: CategoryValue[];
}
