export class ExternalCategoryGrouping {
  id: number;
	name: string;
	description: string;
	parent: number;
	displayOrder: number;
	category: number;
}
