export class Video {
	id: number;
	description: string;
	alternativeText: string;
	url: string;
	fileType: string;
}
