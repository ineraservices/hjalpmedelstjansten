export class Document {
	id: number;
	file: any;
	fileName: string;
	description: string;
	url: string;
	originalUrl: string;
	documentType: DocumentType;
	fileType: string;
}
