import {Document} from './document.model';
import {Video} from './video.model';
import {Image} from './image.model';

export class Media {
	images: Array<Image>;
	videos: Array<Video>;
	documents: Array<Document>;
}
