export class Image {
	id: number;
	mainImage: boolean;
	file: any;
	description: string;
	alternativeText: string;
	url: string;
	originalUrl: string;
}
