import {CategoryProperty} from './category-property.model';
import {CategoryValue} from './category-value.model';

export class ProductCategoryProperty {
	property: CategoryProperty;
	textValue: string;
	decimalValue: number;
	intervalFromValue: number;
	intervalToValue: number;
	singleListValue: CategoryValue;
	multipleListValue: Array<CategoryValue>;
	// Fields for searching
	decimalFromValue: number;
	decimalToValue: number;
}
