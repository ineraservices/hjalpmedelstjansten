export class PackageUnit {
	id: number;
	name: string;
	code: string;
}
