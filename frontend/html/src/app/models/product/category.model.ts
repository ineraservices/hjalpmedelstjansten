export class Category {
	id: number;
	code: string;
	name: string;
	description: string;
	articleType: string;
	level: number;
	parentId: number;
	childCategories: Array<Category>;
	hasProductOrArticles: boolean;
	articleCount: number;
	productCount: number;
}
