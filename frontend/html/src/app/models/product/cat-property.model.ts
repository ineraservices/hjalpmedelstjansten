import {CategoryValue} from './category-value.model';

export class CatProperty {
	categoryId: number;
	id: number;
	name: string;
	description: string;
	type: string;
	values: CategoryValue[] = [];
}
