export class SimpleArticle {
	id: number;
	articleType: string;
	code: string;
	deleteable: boolean;
	name: string;
	number: string;
	organizationId: number;
	organizationName: string;
	status: string;
	type: string;
}
