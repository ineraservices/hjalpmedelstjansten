export class SearchedProductOrArticle {
	id: number;
	type: string;
	articleType: string;
	name: string;
	number: string;
	status: string;
	code: string;
	deleteable: boolean;
	organizationId: number;
	organizationName: string;
}
