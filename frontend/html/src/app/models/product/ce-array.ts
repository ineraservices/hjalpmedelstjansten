import {Ce} from './ce.model';

export class CeArray {
	directives: Array<Ce>;
	standards: Array<Ce>;
}
