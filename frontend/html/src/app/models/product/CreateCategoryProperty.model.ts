import {CatProperty} from "./cat-property.model";

export class appCreateCategoryProperty {
	categoryId: number;
	categoryProperty: CatProperty;
}
