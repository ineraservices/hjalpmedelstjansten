import {BusinessLevel} from '../organization/business-level.model';
import {Role} from './role.model';

export class UserEngagement {
	organizationId: number;
	validFrom: number;
	validTo: number;
	businessLevels: Array<BusinessLevel>;
	roles: Array<Role>;
}
