import {ElectronicAddress} from '../electronic-address.model';
import {UserEngagement} from './user-engagement.model';

export class User {
	id: number;
	firstName: string;
	lastName: string;
	title: string;
	username: string;
	electronicAddress: ElectronicAddress;
	userEngagements: Array<UserEngagement>;
	active: boolean;
	previousLoginFound: boolean;
	loginType: string;
	loginUrl: string;
}
