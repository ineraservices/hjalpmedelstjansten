import {Organization} from './organization/organization.model';
import {BusinessLevel} from './organization/business-level.model';
import {GeneralPricelist} from './general-pricelist/general-pricelist.model';

export class ExportSetting {
	id: number;
	enabled: boolean;
	filename: string;
	organization: Organization;
	businessLevel: BusinessLevel;
	generalPricelists: Array<GeneralPricelist>;
	lastExported: number;
}
