import {DragDropModule} from '@angular/cdk/drag-drop';
import {registerLocaleData} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import localeSvExtra from '@angular/common/locales/extra/sv';
import localeSv from '@angular/common/locales/sv';
import {NgModule} from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {
	DateAdapter,
	ErrorStateMatcher,
	MAT_DATE_LOCALE,
	MatNativeDateModule,
	ShowOnDirtyErrorStateMatcher,
} from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CoreModule} from './_core/core.module';
import {SharedModule} from './_shared/shared.module';
import {AppComponent} from './app.component';
import {AuthGuard} from './auth/auth-guard.service';
import {AuthService} from './auth/auth.service';
import {CacheInterceptor} from './auth/cache-interceptor';
import {ErrorInterceptor} from './auth/error-interceptor';
import {AdministrationComponent} from './pages/administration/administration.component';
import {
	SendWelcomeMailDialogComponent
} from './pages/administration/send-welcome-mail-dialog/send-welcome-mail-dialog.component';
import {AgreementComponent} from './pages/agreement/agreement.component';
import {
	HandleAgreementContactComponent
} from './pages/agreement/handle-agreement-contact/handle-agreement-contact.component';
import {
	DeleteAgreementConfirmationDialogComponent
} from './pages/agreement/handle-agreement/delete-agreement-confirmation-dialog/delete-agreement-confirmation-dialog.component';
import {HandleAgreementComponent} from './pages/agreement/handle-agreement/handle-agreement.component';
import {PricelistDialogComponent} from './pages/agreement/handle-agreement/pricelist-dialog/pricelist-dialog.component';
import {
	ShareWithCustomerDialogComponent
} from './pages/agreement/handle-agreement/share-with-customer-dialog/share-with-customer-dialog.component';
import {
	AddPricelistRowDialogComponent
} from './pages/agreement/handle-pricelist/add-pricelist-row-dialog/add-pricelist-row-dialog.component';
import {HandlePricelistComponent} from './pages/agreement/handle-pricelist/handle-pricelist.component';
import {
	ImportPricelistDialogComponent
} from './pages/agreement/handle-pricelist/import-pricelist-dialog/import-pricelist-dialog.component';
import {AssortmentComponent} from './pages/assortment/assortment.component';
import {
	AddArticlesToAssortmentComponent
} from './pages/assortment/handle-assortment/add-articles-to-assortment/add-articles-to-assortment.component';
import {
	DeleteAssortmentDialogComponent
} from './pages/assortment/handle-assortment/delete-assortment-dialog/delete-assortment-dialog.component';
import {
	DeleteAssortmentrowDialogComponent
} from './pages/assortment/handle-assortment/delete-assortmentrow-dialog/delete-assortmentrow-dialog.component';
import {HandleAssortmentComponent} from './pages/assortment/handle-assortment/handle-assortment.component';
import {CategoryAdminComponent} from './pages/categoryadmin/categoryadmin.component';
import {ConfirmationDialogComponent} from './pages/categoryadmin/confirm-dialog/confirmation-dialog.component';
import {CreateCategoryComponent} from './pages/categoryadmin/create-category/create-category.component';
import {
	CreateUpdateDialogComponent
} from './pages/categoryadmin/create-category/create-update-category-dialog/create-update-dialog.component';
import {NumberDirective} from './pages/categoryadmin/create-category/numbers-only-directive';
import {
	ExportCategorySpecificPropertiesComponent
} from './pages/categoryadmin/export-category-properties/export-category-properties.component';
import {
	MoreInformationDialogComponent
} from './pages/categoryadmin/export-category-properties/more-information-dialog/more-information-dialog.component';
import {
	CreateUpdateExternalCategoryComponent
} from './pages/categoryadmin/external-api-menutree/create-update-external-category/create-update-external-category.component';
import {
	ExternalApiMenutreeComponent
} from './pages/categoryadmin/external-api-menutree/external-api-menutree.component';
import {
	CategorySpecificPropertiesComponent
} from './pages/categoryadmin/manage-category-properties/category-specific-properties.component';
import {
	CreateCategoryPropertyComponent
} from './pages/categoryadmin/manage-category-properties/create-property/create-property-generic.component/create-property-generic.component';
import {
	EditPropertyGenericDialogComponent
} from './pages/categoryadmin/manage-category-properties/create-property/edit-property-genericomponent-dialog/edit-property-generic-dialog.component';
import {ArticleMoveSearchComponent} from './pages/categoryadmin/move-articles/move-articles.component';
import {AlertComponent} from './components/alert/alert.component';
import {UnauthorizedComponent} from './pages/core/error-pages/unauthorized/unauthorized.component';
import {FooterComponent} from './components/footer/footer.component';
import {HeaderComponent} from './components/header/header.component';
import {ExportfileComponent} from './pages/exportfile/exportfile.component';
import {
	ExportSettingDialogComponent
} from './pages/exportfile/exportsetting/export-setting-dialog/export-setting-dialog.component';
import {ExportsettingComponent} from './pages/exportfile/exportsetting/exportsetting.component';
import {GpDialogComponent} from './pages/exportfile/gp-dialog/gp-dialog.component';
import {
	HandleExportfileTableComponent
} from './pages/exportfile/handle-exportfile/handle-exportfile-table/handle-exportfile-table.component';
import {HandleExportfileComponent} from './pages/exportfile/handle-exportfile/handle-exportfile.component';
import {
	RemoveGpDialogComponent
} from './pages/exportfile/handle-exportfile/remove-gp-dialog/remove-gp-dialog.component';
import {
	GeneralPricelistPricelistDialogComponent
} from './pages/general-pricelist/general-pricelist-pricelist-dialog/general-pricelist-pricelist-dialog.component';
import {
	GeneralPricelistTableComponent
} from './pages/general-pricelist/general-pricelist-table/general-pricelist-table.component';
import {
	AddGeneralPricelistPricelistRowDialogComponent
} from './pages/general-pricelist/handle-general-pricelist-pricelist/add-general-pricelist-pricelist-row-dialog/add-general-pricelist-pricelist-row-dialog.component';
import {
	HandleGeneralPricelistPricelistComponent
} from './pages/general-pricelist/handle-general-pricelist-pricelist/handle-general-pricelist-pricelist.component';
import {
	ImportGeneralPricelistPricelistDialogComponent
} from './pages/general-pricelist/handle-general-pricelist-pricelist/import-general-pricelist-pricelist-dialog/import-general-pricelist-pricelist-dialog.component';
import {
	HandleGeneralPricelistComponent
} from './pages/general-pricelist/handle-general-pricelist/handle-general-pricelist.component';
import {
	ViewGeneralPricelistComponent
} from './pages/general-pricelist/view-general-pricelist/view-general-pricelist.component';
import {AddUsersDialogComponent} from './pages/general/add-users-dialog/add-users-dialog.component';
import {ImportStatusComponent} from './pages/importstatus/importstatus.component';
import {CreatePasswordComponent} from './pages/login/create-password/create-password/create-password.component';
import {LoginComponent} from './pages/login/login.component';
import {RecoverComponent} from './pages/login/recover-password/recover-password.component';
import {
	AddBusinesslevelDialogComponent
} from './pages/organization/businesslevel/add-businesslevel-dialog/add-businesslevel-dialog.component';
import {BusinesslevelComponent} from './pages/organization/businesslevel/businesslevel.component';
import {
	UpdateBusinesslevelDialogComponent
} from './pages/organization/businesslevel/update-businesslevel-dialog/update-businesslevel-dialog.component';
import {
	DeleteUserConfirmationDialogComponent
} from './pages/organization/handle-organization/delete-user-confirmation-dialog/delete-user-confirmation-dialog.component';
import {HandleOrganizationComponent} from './pages/organization/handle-organization/handle-organization.component';
import {OrganizationComponent} from './pages/organization/organization.component';
import {HandleUserComponent} from './pages/organization/user/handle-user/handle-user.component';
import {
	TokenLinkDialogComponent
} from './pages/organization/user/handle-user/token-link-dialog/token-link-dialog.component';
import {ArticleConnectionComponent} from './pages/product/article-tabs/article-connection/article-connection.component';
import {
	AddArticleToPricelistDialogComponent
} from './pages/product/article-tabs/article-pricelist/add-article-to-pricelist-dialog/add-article-to-pricelist-dialog.component';
import {ArticlePricelistComponent} from './pages/product/article-tabs/article-pricelist/article-pricelist.component';
import {ArticleTabsComponent} from './pages/product/article-tabs/article-tabs.component';
import {
	ArticleReplacementDialogComponent
} from './pages/product/article-tabs/handle-article/article-replacement-dialog/article-replacement-dialog.component';
import {
	ChangeBasedOnDialogComponent
} from './pages/product/article-tabs/handle-article/change-based-on-dialog/change-based-on-dialog.component';
import {
	DeleteArticleDialogComponent
} from './pages/product/article-tabs/handle-article/delete-article-dialog/delete-article-dialog.component';
import {HandleArticleComponent} from './pages/product/article-tabs/handle-article/handle-article.component';
import {
	SoDeleteArticleDialogComponent
} from './pages/product/article-tabs/handle-article/so-delete-article-dialog/so-delete-article-dialog.component';
import {AttachMessageDialogComponent} from './pages/product/attach-message-dialog/attach-message-dialog.component';
import {
	CategoryDialogAdminSpecificComponent
} from './pages/product/category-dialog-admin-specific/category-dialog-admin-specific.component';
import {CategoryDialogComponent} from './pages/product/category-dialog/category-dialog.component';
import {ConnectionDialogComponent} from './pages/product/connection-dialog/connection-dialog.component';
import {DeleteMediaDialogComponent} from './pages/product/delete-media-dialog/delete-media-dialog.component';
import {EditMediaDialogComponent} from './pages/product/edit-media-dialog/edit-media-dialog.component';
import {
	ExportByProductsDialogComponent
} from './pages/product/export-products-and-articles/export-by-products-dialog/export-by-products-dialog.component';
import {
	ExportProductsAndArticlesComponent
} from './pages/product/export-products-and-articles/export-products-and-articles.component';
import {ImportDialogComponent} from './pages/product/import-dialog/import-dialog.component';
import {ConnectionComponent} from './pages/product/product-tabs/connection/connection.component';
import {
	DeleteProductDialogComponent
} from './pages/product/product-tabs/handle-product/delete-product-dialog/delete-product-dialog.component';
import {HandleProductComponent} from './pages/product/product-tabs/handle-product/handle-product.component';
import {
	ProductReplacementDialogComponent
} from './pages/product/product-tabs/handle-product/product-replacement-dialog/product-replacement-dialog.component';
import {ProductTabsComponent} from './pages/product/product-tabs/product-tabs.component';
import {ProductComponent} from './pages/product/product.component';
import {UploadDialogComponent} from './pages/product/upload-dialog/upload-dialog.component';
import {ColumnsDialogComponent} from './pages/search/columns-dialog/columns-dialog.component';
import {SearchComponent} from './pages/search/search.component';
import {DashboardComponent} from './pages/start/dashboard/dashboard.component';
import {
	DashboardtableAgreementComponent
} from './pages/start/dashboard/dashboardtable-agreement/dashboardtable-agreement.component';
import {
	DashboardtableAssortmentArticlesDiscontinuedComponent
} from "./pages/start/dashboard/dashboardtable-assortmentarticlesdiscontinued/dashboardtable-assortmentarticlesdiscontinued.component";
import {
	DashboardtableApprovedPricelistrowComponent
} from './pages/start/dashboard/dashboardtable-approvedpricelistrow/dashboardtable-approved-pricelistrow.component';
import {
	DashboardtableArticlesComponent
} from './pages/start/dashboard/dashboardtable-articles/dashboardtable-articles.component';
import {
	DashboardtableChangedAgreementComponent
} from './pages/start/dashboard/dashboardtable-changedagreement/dashboardtable-changed-agreement.component';
import {
	DashboardtablePricelistComponent
} from './pages/start/dashboard/dashboardtable-pricelist/dashboardtable-pricelist.component';
import {DashboardAlertsComponent} from './pages/start/dashboardalerts/dashboardalerts.component';
import {DashboardEventsComponent} from './pages/start/dashboardalerts/events/dashboardevents.component';
import {DashboardInformationComponent} from './pages/start/dashboardalerts/information/dashboard-information.component';
import {DynamicTextComponent} from './pages/start/dynamictext/dynamic-text.component';
import {LinksComponent} from './pages/start/links/links.component';
import {StartComponent} from './pages/start/start.component';
import {WelcometextComponent} from './pages/start/welcometext/welcometext.component';
import {ProfileModule} from './modules/profile/profile.module';
import {FormatAgreementStatusPipe} from './pipes/format-agreement-status.pipe';
import {FormatArticleTypePipe} from './pipes/format-article-type.pipe';
import {FormatDeliveryDirectionPipe} from './pipes/format-delivery-direction.pipe';
import {FormatExportSettingStatusPipe} from './pipes/format-export-setting-status';
import {SafeHTMLPipe} from './pipes/format-html.pipe';
import {FormatOrganizationStatusPipe} from './pipes/format-organization-status.pipe';
import {FormatOrganizationTypePipe} from './pipes/format-organization-type.pipe';
import {FormatPricelistRowStatusPipe} from './pipes/format-pricelist-row-status.pipe';
import {FormatPricelistStatusPipe} from './pipes/format-pricelist-status.pipe';
import {FormatProductStatusPipe} from './pipes/format-product-status.pipe';
import {OrganizationBelongingPipe} from './pipes/organization-belonging.pipe';
import {AppRoutingModule} from './routing/app-routing.module';
import {AgreementService} from './services/agreement.service';
import {AlertService} from './services/alert.service';
import {AssortmentService} from './services/assortment.service';
import {CategoryService} from './services/category.service';
import {CommonService} from './services/common.service';
import {CountryService} from './services/country.service';
import {DynamicTextService} from './services/dynamictext.service';
import {ExportService} from './services/export.service';
import {HelptextService} from './services/helptext.service';
import {ImportStatusService} from './services/importstatus.service';
import {LoginService} from './services/login.service';
import {OrganizationService} from './services/organization.service';
import {PathService} from './services/path.service';
import {ProductService} from './services/product.service';
import {UserService} from './services/user.service';
import {CustomDateAdapter} from './util/custom-date-adapter';
import {MatPaginator} from "@angular/material/paginator";
import {ProductTableComponent} from "./components/product-table/product-table.component";
import {PaginationComponent} from "./components/pagination/pagination.component";

registerLocaleData(localeSv, 'sv-SE', localeSvExtra);

@NgModule({
	declarations: [
		EditPropertyGenericDialogComponent,
		CreateCategoryPropertyComponent,
		CategorySpecificPropertiesComponent,
		ConfirmationDialogComponent,
		ArticleMoveSearchComponent,
		CreateUpdateDialogComponent,
		AppComponent,
		AlertComponent,
		HeaderComponent,
		FooterComponent,
		OrganizationComponent,
		AdministrationComponent,
		CategoryAdminComponent,
		CategoryDialogAdminSpecificComponent,
		CreateCategoryComponent,
		NumberDirective,
		HandleOrganizationComponent,
		HandleUserComponent,
		FormatAgreementStatusPipe,
		FormatArticleTypePipe,
		FormatDeliveryDirectionPipe,
		FormatOrganizationTypePipe,
		FormatOrganizationStatusPipe,
		FormatPricelistRowStatusPipe,
		SafeHTMLPipe,
		FormatPricelistStatusPipe,
		FormatProductStatusPipe,
		FormatExportSettingStatusPipe,
		LoginComponent,
		RecoverComponent,
		OrganizationBelongingPipe,
		CreatePasswordComponent,
		StartComponent,
		HandleProductComponent,
		ProductComponent,
		HandleArticleComponent,
		ProductTabsComponent,
		ConnectionComponent,
		ArticleTabsComponent,
		CategoryDialogComponent,
		ProductReplacementDialogComponent,
		ArticleConnectionComponent,
		ConnectionDialogComponent,
		AgreementComponent,
		DeleteAgreementConfirmationDialogComponent,
		HandleAgreementComponent,
		AddUsersDialogComponent,
		ShareWithCustomerDialogComponent,
		HandlePricelistComponent,
		AddPricelistRowDialogComponent,
		HandleGeneralPricelistComponent,
		BusinesslevelComponent,
		AddBusinesslevelDialogComponent,
		UpdateBusinesslevelDialogComponent,
		PricelistDialogComponent,
		HandleGeneralPricelistPricelistComponent,
		GeneralPricelistPricelistDialogComponent,
		UploadDialogComponent,
		EditMediaDialogComponent,
		DeleteMediaDialogComponent,
		EditMediaDialogComponent,
		DeleteArticleDialogComponent,
		SoDeleteArticleDialogComponent,
		DeleteProductDialogComponent,
		ExportProductsAndArticlesComponent,
		ExportByProductsDialogComponent,
		ArticleReplacementDialogComponent,
		ImportDialogComponent,
		SearchComponent,
		ExportfileComponent,
		GpDialogComponent,
		ExportsettingComponent,
		ExportSettingDialogComponent,
		HandleExportfileComponent,
		RemoveGpDialogComponent,
		ColumnsDialogComponent,
		TokenLinkDialogComponent,
		ImportPricelistDialogComponent,
		ImportGeneralPricelistPricelistDialogComponent,
		AddGeneralPricelistPricelistRowDialogComponent,
		AssortmentComponent,
		HandleAssortmentComponent,
		AddArticlesToAssortmentComponent,
		ViewGeneralPricelistComponent,
		ArticlePricelistComponent,
		AddArticleToPricelistDialogComponent,
		SendWelcomeMailDialogComponent,
		ChangeBasedOnDialogComponent,
		AttachMessageDialogComponent,
		ImportStatusComponent,
		HandleAgreementContactComponent,
		DashboardComponent,
		DashboardAlertsComponent,
		DashboardEventsComponent,
		DashboardInformationComponent,
		DashboardtablePricelistComponent,
		DashboardtableAgreementComponent,
		DashboardtableArticlesComponent,
		DashboardtableAssortmentArticlesDiscontinuedComponent,
		DashboardtableChangedAgreementComponent,
		DashboardtableApprovedPricelistrowComponent,
		LinksComponent,
		WelcometextComponent,
		DynamicTextComponent,
		HandleAgreementContactComponent,
		DeleteUserConfirmationDialogComponent,
		ExportCategorySpecificPropertiesComponent,
		MoreInformationDialogComponent,
		EditPropertyGenericDialogComponent,
		CreateCategoryPropertyComponent,
		CategorySpecificPropertiesComponent,
		ConfirmationDialogComponent,
		ArticleMoveSearchComponent,
		CreateUpdateDialogComponent,
		HandleExportfileTableComponent,
		UnauthorizedComponent,
		ExternalApiMenutreeComponent,
		CreateUpdateExternalCategoryComponent,
		DeleteAssortmentrowDialogComponent,
		DeleteAssortmentDialogComponent,
		GeneralPricelistTableComponent,
	],
	imports: [
		CoreModule,
		SharedModule,
		ProfileModule,
		BrowserModule,
		HttpClientModule,
		MatAutocompleteModule,
		MatDatepickerModule,
		MatToolbarModule,
		MatButtonModule,
		MatCardModule,
		MatChipsModule,
		MatDialogModule,
		MatExpansionModule,
		MatIconModule,
		MatMenuModule,
		BrowserAnimationsModule,
		FlexLayoutModule,
		FormsModule,
		MatFormFieldModule,
		MatInputModule,
		MatTableModule,
		MatIconModule,
		MatButtonModule,
		MatSortModule,
		MatCheckboxModule,
		MatProgressSpinnerModule,
		MatRadioModule,
		MatSelectModule,
		MatListModule,
		MatNativeDateModule,
		MatTabsModule,
		MatTooltipModule,
		ReactiveFormsModule,
		AppRoutingModule,
		DragDropModule,
		MatSortModule,
		MatPaginator,
		ProductTableComponent,
		PaginationComponent,
	],
	providers: [
		AuthService,
		AuthGuard,
		LoginService,
		AlertService,
		PathService,
		CountryService,
		OrganizationService,
		UserService,
		ProductService,
		AgreementService,
		CommonService,
		ExportService,
		AssortmentService,
		HelptextService,
		DynamicTextService,
		ImportStatusService,
		CategoryService,
		SafeHTMLPipe,
		{ provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
		{ provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true },
		{ provide: MAT_DATE_LOCALE, useValue: 'sv' },
		{ provide: DateAdapter, useClass: CustomDateAdapter },
		{ provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher },
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
