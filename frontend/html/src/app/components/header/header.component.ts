import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';

import {AuthService} from '../../auth/auth.service';
import {LoginService} from '../../services/login.service';
import {Router} from '@angular/router';
import {User} from '../../models/user/user.model';
import {AgreementService} from '../../services/agreement.service';
import {StorageHelper} from '../../helpers/storage.helper';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {
	storageHelper: StorageHelper = new StorageHelper('');
	isLoggedIn: Observable<boolean>;
	productsLink: string;
	agreementsLink: string;
	assortmentsLink: string;
	exportfileLink: string;
	// importstatusLink: string;
	generalPricelistLink: string;
	profile: User;
	loaded = false;
	activeMenuItem: string;
	hasPermissionToCreateProducts: boolean;
	hasPermissionToViewOrganizations: boolean;
	hasPermissionToViewAdministration: boolean;
	hasPermissionToViewAgreements: boolean;
	hasPermissionToViewAllGeneralPricelists: boolean;
	hasPermissionToViewGeneralPricelist: boolean;
	hasPermissionToCreateGeneralPricelist: boolean;
	hasPermissionToSearchGeneralPricelist: boolean;
	hasPermissionToViewExportsettings: boolean;
	hasPermissionToCreateExportSettings: boolean;
	hasPermissionToUpdateUserContact: boolean;
	hasPermissionToViewAssortments: boolean;
	hasPermissionToViewCategoryAdmin: boolean;
	authorized = false;

	constructor(private authService: AuthService,
							private loginService: LoginService,
							private agreementService: AgreementService,
							private router: Router) {
		this.isLoggedIn = this.authService.isLoggedIn();
	}

	ngOnInit() {
		this.authService.isLoginSubject.subscribe(
			res => {
				this.authorized = res;
				if (localStorage.getItem('token')) {
					this.profile = JSON.parse(localStorage.getItem('token'));
					this.productsLink = 'organization/' + this.profile.userEngagements[0].organizationId + '/product';
					this.agreementsLink = 'organization/' + this.profile.userEngagements[0].organizationId + '/agreement';
					this.assortmentsLink = 'organization/' + this.profile.userEngagements[0].organizationId + '/assortment';
					this.exportfileLink = 'organization/' + this.profile.userEngagements[0].organizationId + '/exportfile';
					// this.importstatusLink = 'organization/' + this.profile.userEngagements[0].organizationId + '/importstatus';

					this.hasPermissionToCreateProducts = this.hasPermission('product:create_own');
					this.hasPermissionToViewOrganizations = this.hasPermission('organization:view');
					this.hasPermissionToViewAdministration = this.authService.isSuperAdmin();
					this.hasPermissionToViewAgreements = this.hasPermission('agreement:view_own');
					this.hasPermissionToViewAllGeneralPricelists = this.hasPermission('generalpricelist:view_all');
					this.hasPermissionToViewGeneralPricelist = this.hasPermission('generalpricelist:view');
					this.hasPermissionToCreateGeneralPricelist = this.hasPermission('generalpricelist:create_own');
					this.hasPermissionToSearchGeneralPricelist = this.hasPermission('generalpricelist:search_all');
					this.hasPermissionToViewExportsettings = this.hasPermission('exportsettings:view_own');
					this.hasPermissionToCreateExportSettings = this.hasPermission('exportsettings:create_all');
					this.hasPermissionToUpdateUserContact = this.hasPermission('user:update_contact');
					this.hasPermissionToViewAssortments = this.hasPermission('assortment:view') ||
						this.hasPermission('assortment:view_all');
					// if (this.hasPermissionToViewGeneralPricelist) {
					//   this.agreementService.getGeneralPriceList(this.profile.userEngagements[0].organizationId).subscribe(
					//     gp => {
					//       if (gp) {
					//         this.generalPricelistLink = 'organization/' + this.profile.userEngagements[0].organizationId +
					//           '/generalpricelist/' + gp.id;
					//       } else if (this.hasPermissionToCreateGeneralPricelist) {
					//         this.generalPricelistLink = 'organization/' + this.profile.userEngagements[0].organizationId + '/generalpricelist';
					//       }
					//     }
					//   );
					// }
					this.ngAfterViewInit(); // needs already when iniatilizing
				}
			}
		);
		this.loaded = true;
	}

	ngAfterViewInit(): void {
		if (this.hasPermissionToViewGeneralPricelist) {
			setTimeout(() => {
				this.agreementService.getGeneralPriceList(this.profile.userEngagements[0].organizationId).subscribe(
					gp => {
						if (gp) {
							this.generalPricelistLink = 'organization/' + this.profile.userEngagements[0].organizationId +
								'/generalpricelist/' + gp.id;
						} else if (this.hasPermissionToCreateGeneralPricelist) {
							this.generalPricelistLink = 'organization/' + this.profile.userEngagements[0].organizationId + '/generalpricelist';
						}
					}
				);
			});
		}
	}

	hasPermission(permission): boolean {
		if (!localStorage.getItem('token')) {
			return false;
		}
		return this.authService.hasPermission(permission);
	}

	goHome() {
		if (this.authService.isLoggedIn() && this.authorized) {
			this.setActiveMenuItem('');
			this.router.navigate(['/start']);
		}
	}

	setActiveMenuItem(menuItem) {
		this.activeMenuItem = menuItem;
	}

	clearStorage() {
		this.storageHelper.removeAllStorageEntries();
	}

	logout() {
		this.setActiveMenuItem('');
		this.loginService.logout().subscribe(
			data => {
				this.authService.logout();
				this.router.navigate(['/']);
				this.clearStorage();
			}
		);
	}
}

