import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductTableComponent } from './product-table.component';

describe('HjmtTableComponent', () => {
	let component: ProductTableComponent<unknown>;
	let fixture: ComponentFixture<ProductTableComponent<unknown>>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [ProductTableComponent],
		}).compileComponents();

		fixture = TestBed.createComponent(ProductTableComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
