import {
	Component,
	Input,
	OnChanges,
	OnInit,
	SimpleChanges,
} from '@angular/core';
import { FlexModule } from '@angular/flex-layout';
import { NgClass, NgForOf, NgIf } from '@angular/common';

@Component({
	selector: 'app-pagination',
	standalone: true,
	imports: [FlexModule, NgForOf, NgIf, NgClass],
	templateUrl: './pagination.component.html',
	styleUrl: './pagination.component.css',
})
export class PaginationComponent implements OnInit, OnChanges {
	@Input() searchTotal = 0;
	@Input() offset = 0;
	@Input() currentPage = 1;
	@Input() totalPages = 0;
	@Input() goToPage: (page: number) => void;
	@Input() pages: Array<string | number> = [];

	private internalPages: Array<string | number> = [];

	ngOnInit() {
		this.updateInternalState();
	}

	ngOnChanges() {
		this.updateInternalState();
	}

	updateInternalState() {
		this.internalPages = [];

		if (this.searchTotal <= this.offset) {
			return;
		}

		const pages = this.getNumberedList(this.pages);

		if (this.totalPages < 6) {
			this.internalPages = pages;
			return;
		}

		let hasFirstEllipsis = false;
		let hasSecondEllipsis = false;

		pages.forEach((pageNumber) => {
			const isEdgePage = pageNumber === 1 || pageNumber === this.totalPages;
			const isCurrentOrAdjacentPage =
				Math.abs(pageNumber - this.currentPage) <= 1;

			if (isEdgePage || isCurrentOrAdjacentPage) {
				this.internalPages.push(pageNumber);
			} else if (!hasFirstEllipsis && pageNumber < this.currentPage) {
				this.internalPages.push('...');
				hasFirstEllipsis = true;
			} else if (!hasSecondEllipsis && pageNumber > this.currentPage) {
				this.internalPages.push('...');
				hasSecondEllipsis = true;
			}
		});
	}

	getNumberedList(list: Array<string | number>) {
		return list.map((val) =>
			typeof val === 'string' ? parseInt(val, 10) : val,
		);
	}
}
