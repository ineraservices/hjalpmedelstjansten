import {Component} from '@angular/core';
import { CommonService } from '../../services/common.service';

@Component({
	selector: 'app-footer',
	templateUrl: './footer.component.html',
	styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
	flexDirection: string;

	constructor(private commonService: CommonService) {
		if (this.commonService.isUsingIE()) {
			this.flexDirection = 'row';
		} else {
			this.flexDirection = 'column';
		}
	}

}
