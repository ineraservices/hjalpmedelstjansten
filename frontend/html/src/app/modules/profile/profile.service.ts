import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';
import {debug, RxJsLoggingLevel} from '../../util/log-debug-rxjs';

import {CommonService} from '../../services/common.service';
import {AlertService} from '../../services/alert.service';
import {HelptextService} from '../../services/helptext.service';
import {OrganizationService} from '../../services/organization.service';

import {UserService} from '../../services/user.service';
import {User} from '../../models/user/user.model';
import {Organization} from '../../models/organization/organization.model';

import {NotificationObject} from './profile.controller';


@Injectable()
export class ProfileService {

	constructor(private userService: UserService,
							private organizationService: OrganizationService,
							private helptextService: HelptextService,
							private alertService: AlertService,
							private commonService: CommonService) {

	}

	/**
	 * User is already instantiated in our localStorage by authService.
	 * Use token values from there instead of re-fetching from db.
	 */
	fetchProfile(): Observable<({ user: User; organization: Organization; notification: NotificationObject; admins: Array<User> })> {

		// Exit if browser is not supporting storage.
		// TODO:  Run getProfile() instead of return null, which is exactly the same thing except for no direct localStorage access.
		if (typeof (Storage) === 'undefined') {
			return null;
		}

		// Fetch the localStorage 'token' (User).
		const user: User = JSON.parse(window.localStorage.getItem('token')) as User;

		// Get organization and admins of organization.
		return this.organizationService.getOrganization(user.userEngagements[0].organizationId).pipe(
			switchMap((organization) => this.userService.getUserByRoles(organization.id, 'Customeradmin&roleName=Supplieradmin').pipe(
				switchMap((admins) => this.userService.getNotificationTypes(organization.id.toString(), user.id.toString())
					.pipe(map((notification) => ({user, organization, notification, admins}))
					))
			))
		);
	}

	// getProfile(): Observable<User> {
	//   return this.userService.getProfile()
	//     .pipe(debug(RxJsLoggingLevel.TRACE, 'profileService:trace:getProfile'))
	// }

	getOrganization(userId: string): Observable<Organization> {
		return this.organizationService.getOrganization(userId)
			.pipe(debug(RxJsLoggingLevel.TRACE, 'profileService:trace:getOrganization'));
	}

	getAdmins(organizationId, rolesQuery: string): Observable<Array<User>> {
		return this.userService.getUserByRoles(organizationId, rolesQuery)
			.pipe(debug(RxJsLoggingLevel.TRACE, 'profileService:trace:getAdmins'));
	}

	getHelpTexts(): Observable<Map<string, string>> {
		return this.helptextService.getTexts()
			.pipe(debug(RxJsLoggingLevel.TRACE, 'profileService:trace:getHelpTexts()'));
	}

	saveProfile(organizationId: string, user: User): Observable<User> {
		return this.userService.updateUser(organizationId, user.id.toString(), user)
			.pipe(debug(RxJsLoggingLevel.TRACE, 'profileService:trace:saveProfile '));
	}

	getNotificationTypes(organizationId: string, userId: string): Observable<NotificationObject> {
		return this.userService.getNotificationTypes(organizationId, userId)
			.pipe(debug(RxJsLoggingLevel.TRACE, 'profileService:trace:getNotificationTypes'));
	}

	updateNotificationTypes(organizationId: string, userId: string, notifications: Array<string>): Observable<NotificationObject> {
		return this.userService.updateNotificationTypes(organizationId, userId, notifications)
			.pipe(debug(RxJsLoggingLevel.TRACE, 'profileService:trace:updateNotificationTypes'));
	}

	setAlert(message: string): void {
		this.alertService.errorWithMessage(message);
	}

	setSuccess(message: string): void {
		this.alertService.success(message);
	}

	clearAlert(): void {
		this.alertService.clear();
	}

	getFlexDirection(): string {
		return this.commonService.isUsingIE() ? 'row' : 'column';
	}

}
