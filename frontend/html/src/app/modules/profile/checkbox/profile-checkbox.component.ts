import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';

import {ProfileService} from '../profile.service';
import {CheckboxItem, CheckboxObject, ProfileController} from '../profile.controller';


@Component({
	selector: 'app-profile-checkbox',
	styleUrls: ['../profile.component.scss', 'profile-checkbox.component.scss'],
	template: `
		<mat-card class="checkbox-control">

			<header>
				<h2>{{ checkboxObject.header }}</h2>

				<mat-icon class="tooltip"
									matTooltip="{{ checkboxObject.headerTooltip }}"
									[matTooltipShowDelay]="500">
					info_outline
				</mat-icon>
			</header>

			<article class="content">
				<p>{{ checkboxObject.content }}</p>

				<form [formGroup]="this.checkboxForm"
							(keydown.enter)="$event.preventDefault()"
							(keydown.shift.enter)="$event.preventDefault()">

					<ul [formGroup]="checkboxItemsGroup"
							class="checkbox-list"
							(keydown.enter)="$event.preventDefault()"
							(keydown.shift.enter)="$event.preventDefault()">

						<li *ngFor="let control of checkboxItemsGroup.value | keyvalue">
							<mat-checkbox [formControlName]="control.key"
														[name]="control.key.toString()"
														[attr.data-cy]="'check_' + control.key.toLowerCase()">

								{{ getCheckboxItem($any(control.key)).description }}
							</mat-checkbox>
						</li>

					</ul>

				</form>

				<p class="gray-text">{{ checkboxObject.contentExtra }}</p>

			</article>
		</mat-card>
	`
})
export class ProfileCheckboxComponent implements OnInit {

	// Will be overridden by input-meta from @Component({ 'inputs' : 'checkboxObject' })
	@Input()
		checkboxObject: CheckboxObject = {
			header: 'Titel',
			headerTooltip: 'Tooltip',
			content: 'Innehåll',
			contentControls: [
				{name: 'default', description: 'Standard', checked: true} as CheckboxItem
			],
			contentExtra: 'Innehåll hjälptext'
		};

	@Output()
		emitSave: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

	checkboxForm: FormGroup;
	private _checkboxForm: FormGroup;

	constructor(private profileService: ProfileService,
							private profileController: ProfileController,
							private formBuilder: FormBuilder) {
		this.initializeForm();
	}


	// Only on first-change.
	get checkboxItemsGroup(): FormGroup {
		return this.checkboxForm.get('checkboxItems') as FormGroup;
	}

	ngOnInit() {
		this.addCheckboxControlToForm(this.checkboxObject.contentControls);
		this.saveState();
		this.checkboxForm.disable();
	}

	getCheckboxItem(checkboxName: string): CheckboxItem {
		// console.log(checkboxName)
		const checkboxItem: Array<CheckboxItem> = this.checkboxObject.contentControls
			.filter((item) => item.name.toUpperCase() === checkboxName.toUpperCase());
		if (null === checkboxItem || 0 === checkboxItem.length) {
			throw new Error('No checkboxItem(s) found with that name');
		}
		return checkboxItem[0];
	}


	change($event) {
		console.log($event);
	}


	enable() {
		this.checkboxForm.enable();
	}


	cancel() {
		this.restoreState();
		this.checkboxForm.disable();
	}

	save() {
		this.saveState();
		this.emitSave.emit(this.checkboxItemsGroup);
		this.checkboxForm.disable();
	}

	private initializeForm() {
		this.checkboxForm = this.formBuilder.group({
			checkboxItems: this.formBuilder.group({})
		});
	}

	private addCheckboxControlToForm(items: Array<CheckboxItem>): void {
		const checkboxes: FormGroup = this.checkboxForm.get('checkboxItems') as FormGroup;
		items.forEach((control) => {
			const fControl = new FormControl(control.checked, {updateOn: 'change'});
			checkboxes.addControl(control.name, fControl);
		});
	}


	private saveState() {
		this._checkboxForm = this.checkboxForm.getRawValue();
	}


	private restoreState() {
		this.checkboxForm.reset(this._checkboxForm);
	}

}
