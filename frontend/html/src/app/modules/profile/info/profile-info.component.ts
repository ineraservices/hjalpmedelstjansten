﻿import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {InfoObject} from '../profile.controller';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ProfileService} from '../profile.service';
import {formatDate} from '@angular/common';


@Component({
	selector: 'app-profile-info',
	styleUrls: ['../profile.component.scss', 'profile-info.component.scss'],
	template: `

		<mat-card class="info-control">
			<header>
				<h2>{{ infoObject.header }}</h2>
				<mat-icon class="tooltip"
									matTooltip="{{ infoObject.headerTooltip }}"
									[matTooltipShowDelay]="500"
									data-cy="info-tooltip">
					info_outline
				</mat-icon>
			</header>

			<article class="content">

				<form [formGroup]="this.infoForm" (keydown.enter)="$event.preventDefault()"
							(keydown.shift.enter)="$event.preventDefault()">

					<ul>

						<li>
							<mat-form-field floatLabel="always">
								<mat-label>Organisation</mat-label>
								<input type="text" matInput required autocomplete="off"
											 [formControl]="organization"
											 data-cy="info-organization"/>

							</mat-form-field>


							<mat-form-field floatLabel="always">
								<mat-label class="icon-label">Begränsad till verksamhetsområde:
									<mat-icon class="tooltip"
														matTooltip="{{ infoObject.businessLevelsTooltip }}"
														[matTooltipShowDelay]="500">
										info_outline
									</mat-icon>
								</mat-label>
								<input type="text" matInput required autocomplete="off"
											 [formControl]="businessLevels"
											 data-cy="info-businesslevel"/>

							</mat-form-field>
						</li>


						<li>
							<mat-form-field floatLabel="always">
								<mat-label>Användarnamn</mat-label>
								<input type="text" matInput required autocomplete="off"
											 [formControl]="userName"
											 data-cy="info-username"/>

							</mat-form-field>

							<mat-form-field floatLabel="always">
								<mat-label>Titel</mat-label>
								<input type="text" matInput required autocomplete="off"
											 [formControl]="title"
											 data-cy="info-title"/>

							</mat-form-field>
						</li>


						<li>
							<mat-form-field floatLabel="always">
								<mat-label>Förnamn</mat-label>
								<input type="text" matInput required autocomplete="off"
											 [formControl]="firstName"
											 data-cy="info-firstname"/>

							</mat-form-field>

							<mat-form-field floatLabel="always">
								<mat-label>Efternamn</mat-label>
								<input type="text" matInput required autocomplete="off"
											 [formControl]="lastName"
											 data-cy="info-lastname"/>

							</mat-form-field>
						</li>


						<li>
							<mat-form-field floatLabel="always">
								<mat-label>Giltig from:</mat-label>
								<input type="text" matInput required autocomplete="off"
											 [formControl]="validFrom"
											 data-cy="info-valid-from"/>

							</mat-form-field>

							<mat-form-field floatLabel="always">
								<mat-label class="icon-label">Giltig tom:
									<mat-icon class="tooltip"
														matTooltip="{{ infoObject.validToTooltip }}"
														[matTooltipShowDelay]="500"
														data-cy="info-valid-to-tooltip">
										info_outline
									</mat-icon>
								</mat-label>
								<input type="text" matInput required autocomplete="off"
											 [formControl]="validTo"
											 data-cy="info-valid-to"/>

							</mat-form-field>
						</li>


					</ul>

				</form>

			</article>
		</mat-card>
	`
})
export class ProfileInfoComponent implements OnInit {

	@Input() infoObject: InfoObject = {
		header: 'Information',
		headerTooltip: 'Tooltip',
		organization: 'Organisation',
		businessLevels: 'Verksamhetsområde',
		businessLevelsTooltip: 'Tooltip',
		userName: 'Användarnamn',
		firstName: 'Förnamn',
		lastName: 'Efternamn',
		title: 'Titel',
		validFrom: new Date(),
		validTo: null,
		validToTooltip: 'Tooltip'
	} as InfoObject;

	@Output() emitSave: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

	infoForm: FormGroup;

	private _infoForm: FormGroup;

	constructor(private profileService: ProfileService,
							private formBuilder: FormBuilder) {
		this.initializeForm();

	}

	get organization(): FormControl {
		return this.infoForm.controls.organization as FormControl;
	}

	get businessLevels(): FormControl {
		return this.infoForm.controls.businessLevels as FormControl;
	}

	get title(): FormControl {
		return this.infoForm.controls.title as FormControl;
	}

	get firstName(): FormControl {
		return this.infoForm.controls.firstName as FormControl;
	}

	get lastName(): FormControl {
		return this.infoForm.controls.lastName as FormControl;
	}

	get userName(): FormControl {
		return this.infoForm.controls.userName as FormControl;
	}

	get validTo(): FormControl {
		return this.infoForm.controls.validTo as FormControl;
	}

	get validFrom(): FormControl {
		return this.infoForm.controls.validFrom as FormControl;
	}

	ngOnInit() {
		this.addFormControlsToForm();
		this.infoForm.disable();
		this.saveState();
	}

	cancel() {
		this.restoreState();
		this.infoForm.disable();
	}

	save() {
		this.saveState();
		this.emitSave.emit(this.infoForm);
		this.infoForm.disable();
	}

	enable() {
		this.infoForm.enable();
	}

	private initializeForm() {
		this.infoForm = this.formBuilder.group(
			[],
			{updateOn: 'change'});

		// this._infoForm = this.formBuilder.group([],
		//   { updateOn: 'change' });

	}

	private addFormControlsToForm(): void {

		this.infoForm.addControl(
			'userName', new FormControl([this.infoObject.userName], [])
		);

		this.infoForm.addControl(
			'firstName', new FormControl([this.infoObject.firstName], [])
		);

		this.infoForm.addControl(
			'lastName', new FormControl([this.infoObject.lastName], [])
		);

		this.infoForm.addControl(
			'title', new FormControl([this.infoObject.title], [])
		);

		this.infoForm.addControl(
			'organization', new FormControl([this.infoObject.organization], [])
		);

		this.infoForm.addControl(
			'businessLevels', new FormControl([this.infoObject.businessLevels], [])
		);

		this.infoForm.addControl(
			'validFrom', new FormControl([formatDate(this.infoObject.validFrom, 'yyyy-MM-dd', 'sv-SE')], [])
		);

		let validToString: string;

		if (this.infoObject.validTo) {
			validToString = formatDate(this.infoObject.validTo, 'yyyy-MM-dd', 'sv-SE');
		} else {
			validToString = '';
		}

		this.infoForm.addControl('validTo', new FormControl([validToString], []));

	}

	private saveState(): void {
		this._infoForm = this.infoForm.getRawValue();
	}

	private restoreState() {
		this.infoForm.reset(this._infoForm);
	}
}
