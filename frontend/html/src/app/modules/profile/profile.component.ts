import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {take} from 'rxjs/operators';
import {debug, RxJsLoggingLevel} from '../../util/log-debug-rxjs';
import {ProfileService} from './profile.service';

import {CheckboxObject, ContactObject, InfoObject, NotificationObject, ProfileController} from './profile.controller';
import {ProfileInfoComponent} from './info/profile-info.component';
import {ProfileContactComponent} from './contact/profile-contact.component';
import {ProfileCheckboxComponent} from './checkbox/profile-checkbox.component';
import {User} from '../../models/user/user.model';
import {Organization} from '../../models/organization/organization.model';
import {forkJoin, Subscription} from 'rxjs';
import {FormGroup} from '@angular/forms';


@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnDestroy, OnInit {
	// 'subscribed' objects.
	@ViewChild('infoObject') eInfo: ProfileInfoComponent;
	@ViewChild('permissionObject') ePermission: ProfileCheckboxComponent;
	@ViewChild('contactObject') eContact: ProfileContactComponent;
	@ViewChild('notificationObject') eNotification: ProfileCheckboxComponent;

	_info: string;
	info: InfoObject;
	_contact: string;

	contact: ContactObject;
	_notification: string;
	notification: CheckboxObject;

	_permission: string;
	permission: CheckboxObject;
	saving: boolean;

	editMode: boolean;
	newProfile: boolean;
	helpTextsLoaded: boolean;

	loaded: boolean;
	flexDirection: string;

	isAgreementManager: boolean;
	private $userProfile: User;
	private $organization: Organization;
	private $notification: NotificationObject;
	private $helpTexts: Map<string, string>;

	private profileSubscription: Subscription;
	private helpTextSubscription: Subscription;

	constructor(private profileService: ProfileService,
							private controller: ProfileController) {
		this.flexDirection = this.profileService.getFlexDirection();
	}

	ngOnInit() {

		this.profileSubscription = this.profileService.fetchProfile()
			.pipe(debug(RxJsLoggingLevel.DEBUG, 'profile:debug:fetchProfile'))
			.subscribe(({user, organization, notification, admins}) => {
				this.setupProfile(user, organization, notification, admins);
				this.loaded = true;
			},
														(error) => {
															this.profileService.setAlert('Failed to load profile: ' + error.errors);
														}
			);

		this.helpTextSubscription = this.profileService.getHelpTexts()
			.pipe(take(1))
			.pipe(debug(RxJsLoggingLevel.DEBUG, 'profile:debug:getHelpTexts'))
			.subscribe(
				(help) => {
					this.$helpTexts = help;
					this.helpTextsLoaded = true;
				},
				(error) => {
					this.profileService.setAlert('Failed to load helptexts: ' + error.errors);
				}
			);

	}


	ngOnDestroy() {
		this.profileSubscription.unsubscribe();
		this.helpTextSubscription.unsubscribe();
		this.profileService.clearAlert();
	}


	toggleEditMode(value?: boolean | null) {
		// If boolean is passed, set editMode as value. If boolean is not passed with method, just toggle/flip.
		if (value == null) {
			this.editMode = !this.editMode;
		} else {
			this.editMode = value;
		}
		// console.info('editMode', this.editMode);
		// Sync everything along with the editMode-status.
		this.onEditModeChange();
	}


	cancel(): void {
		this.toggleEditMode(false);
	}


	save(): void {

		this.saving = true;
		// this.eInfo.save();
		// this.ePermission.save();
		this.eContact.save();
		this.eNotification.save();
		this.toggleEditMode(false);

		this.saveProfile();
		this.saving = false;
	}


	handleFormSubmit($event: FormGroup, identifier: any) {

		switch (identifier) {
		case this.info.header:
			this.$userProfile = this.controller.extractInfoFromFormControls($event);
			if ($event.pristine) {
				return;
			}
			// console.info('Extracted user info: ', this.$userProfile);
			break;
		case this.contact.header:
			if ($event.pristine) {
				return;
			}
			this.controller.extractContactsFromFormControls($event, this.$userProfile);
			// console.info('Extracted user contact: ', this.$userProfile.electronicAddress);
			break;
		default:
			throw new Error('unknown identifier: ' + identifier + ' : ' + $event);
		}
	}

	handleCheckSubmit($event: FormGroup, identifier: string) {

		// Identifier is passed in component creation as <type>Object.header
		switch (identifier) {
		case this.notification.header:
			this.$notification.userNotifications = this.controller.extractNotificationTypesFromFormControls($event, this.$notification);
			// debug-log
			// console.info('Extracted user notifications: ', this.$notification.userNotifications);
			break;
		case this.permission.header:
			// alertService?
			// console.info('Permissions can not be changed from Profile so no changes made.');
			break;
		default:
			// alertService?
			// console.info('Unknown identifier from checkboxObject');
			break;
		}

	}

	private setupProfile(user: User, organization: Organization, notification: NotificationObject, admins: Array<User>) {

		this.$userProfile = user;
		this.$organization = organization;
		this.$notification = notification;

		this.info = this.controller.createInfoObject(user, organization, this.$helpTexts);
		this.contact = this.controller.createContactObject(user, this.$helpTexts);
		this.permission = this.controller.createPermissionObject(user, organization, admins, this.$helpTexts);
		this.notification = this.controller.createNotificationObject(notification, organization, this.$helpTexts);

		user.userEngagements[0].roles.forEach((key) => {
			if (key['name'] === 'CustomerAgreementManager' || key['name'] === 'SupplierAgreementManager') {
				this.isAgreementManager = true;
			}
		});
	}


	private onEditModeChange() {
		// Enable/Disable forms programmatically.
		if (this.editMode) {
			this.enableAll();
		} else {
			this.disableAll();
		}
	}


	private enableAll() {
		// this.eInfo.enable();
		this.eContact.enable();
		if (this.isAgreementManager) {
			this.eNotification.enable();
		}
		// this.ePermission.enable();
	}

	private disableAll() {
		// this.eInfo.cancel();
		if (this.isAgreementManager) {
			this.eNotification.cancel();
		}
		this.eContact.cancel();
		// this.ePermission.cancel();
	}

	private saveProfile(): void {

		const savedProfile = this.profileService.saveProfile(
			this.$organization.id.toString(),
			this.$userProfile
		);

		const savedNotifications = this.profileService.updateNotificationTypes(
			this.$organization.id.toString(),
			this.$userProfile.id.toString(),
			this.$notification.userNotifications
		);

		forkJoin([savedProfile, savedNotifications])
			.pipe(debug(RxJsLoggingLevel.DEBUG, 'profile:save:forkJoin[profile+notifications]'))
			.subscribe(
				(results) => {
					this.$userProfile = results[0];
					this.$notification = results[1];
					// results.forEach((key, value) => {
					//   console.info('everythang saved: ', key, value);
					// });
					this.profileService.setSuccess('Profil sparad!');
				},
				(error) => {
					console.log(error);
					error.error.errors.forEach((key, value) => {
						this.profileService.setAlert(key.message);
						// console.info('errors: ', key, value);
					});
				}
			);

	}

}
