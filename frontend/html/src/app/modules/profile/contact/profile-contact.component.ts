import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {ProfileService} from '../profile.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ContactObject} from '../profile.controller';
import {Subscription} from 'rxjs';

@Component({
	selector: 'app-profile-contact',
	styleUrls: ['../profile.component.scss', 'profile-contact.component.scss'],
	template: `
		<mat-card class="contact-control">

			<header>
				<h2>{{ contactObject.header }}</h2>
				<mat-icon class="tooltip"
									matTooltip="{{ contactObject.headerTooltip }}"
									[matTooltipShowDelay]="500"
									data-cy="tooltip-contact">
					info_outline
				</mat-icon>
			</header>

			<article class="contact-content">

				<form>
					<ul>
						<li [formGroup]="phoneGroup" (keydown.enter)="$event.preventDefault()"
								(keydown.shift.enter)="$event.preventDefault()">

							<mat-form-field floatLabel="always">
								<mat-label data-input-type="phone">Telefonnummer:</mat-label>
								<input type="string" matInput name="phone"
											 formControlName="phone"
											 data-cy="contact-phone"/>

							</mat-form-field>

							<mat-form-field floatLabel="always">
								<mat-label data-input-type="mobile">Mobilnummer:</mat-label>
								<input type="string" matInput name="mobile"
											 formControlName="mobile"
											 data-cy="contact-mobile"/>

							</mat-form-field>
						</li>

						<li [formGroup]="emailGroup" (keydown.enter)="$event.preventDefault()"
								(keydown.shift.enter)="$event.preventDefault()">


							<mat-form-field floatLabel="always">
								<mat-label data-input-type="email">E-mail:</mat-label>
								<input type="email" matInput name="email"
											 formControlName="email"
											 data-cy="contact-email"/>

							</mat-form-field>

							<mat-form-field floatLabel="always" *ngIf="this.emailGroup.enabled">
								<mat-label data-input-type="email-repeat">Repetera E-mail:</mat-label>
								<input type="text" matInput name="email-repeat"
											 formControlName="email_repeat"
											 data-cy="contact-email-repeat"/>

							</mat-form-field>

						</li>


						<li>

							<button mat-stroked-button
											[disabled]="this.contactForm.disabled"
											(click)="this.toggleEmail()"
											data-input-type="email-toggle"
											data-cy="btn-email-toggle">

								<mat-icon>cached</mat-icon>
								{{ this.editEmailButtonText }}
							</button>

							<button mat-stroked-button *ngIf="!this.emailGroup.disabled"
											(click)="this.saveEmail()"
											[disabled]="this.emailGroup.disabled || !this.contactForm.valid || !this.emailValid()"
											data-input-type="email-submit"
											aria-label="Spara ny e-post"
											data-cy="btn-email-submit">

								<mat-icon>save</mat-icon>
								<span>Spara ny epost</span>
							</button>

						</li>
					</ul>
				</form>

			</article>
		</mat-card>
	`

	/* Add this pseudo validator to save email button*
/* this.emailGroup.controls('email').value == this.emailGroup.controls('email_repeat').value"; */

	/* Add this kind of animation to the elements dis/appearing *
/* [@emailToggle]="this.editEmailMode ? 'enabled' : 'disabled'" */

})
export class ProfileContactComponent implements OnInit, OnDestroy {

	@Input() contactObject: ContactObject = {
		header: 'Titel',
		headerTooltip: 'Titel tooltip',
		phone: 'Telefonnummer',
		mobile: 'Mobilnummer',
		email: 'E-mail'
	};

	@Output() emitSave: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

	$contactForm: Subscription;

	contactForm: FormGroup;
	_contactForm: FormGroup;

	emailForm: FormGroup;
	$emailGroup: Subscription;

	phoneForm: FormGroup;

	$phoneGroup: Subscription;
	editEmailMode: boolean;
	editEmailButtonText: string;

	isEmailValid: boolean;

	constructor(private profileService: ProfileService,
							private formBuilder: FormBuilder) {
		this.editEmailButtonText = 'Ändra e-post';
	}

	get phoneGroup(): FormGroup {
		return this.contactForm.get('phone') as FormGroup;
	}

	get emailGroup(): FormGroup {
		return this.contactForm.get('email') as FormGroup;
	}

	get email(): FormControl {
		return this.emailGroup.controls.email as FormControl;
	}

	get emailRepeat(): FormControl {
		return this.emailGroup.controls.email_repeat as FormControl;
	}

	ngOnInit(): void {

		this.initializeForm();
		this.addFormControlsToForm();
		this.saveState();

		// Subscribe to changes in the form.
		// this.$contactForm = this.contactForm.valueChanges.subscribe(
		//   (change) => {
		//     // console.info('contact:Change in form: ', change)
		//   },
		//   (error) => {
		//     // console.info('Error on form change: ', error.errors)
		//     this.profileService.setAlert(error.name);
		//   }
		// );

	}

	ngOnDestroy(): void {
		// this.$contactForm.unsubscribe();
		this.$emailGroup.unsubscribe();
		this.$phoneGroup.unsubscribe();
	}

	toggleEmail(value?: boolean | null): void {

		if (null == value) {
			if (this.emailGroup.disabled) {
				this.emailGroup.enable();
				this.editEmailMode = true;
			} else {
				this.editEmailMode = false;
				this.emailGroup.disable();
			}
		} else {
			if (value) {
				this.emailGroup.enable();
				this.editEmailMode = true;
			} else {
				this.emailGroup.disable();
				this.editEmailMode = false;
			}
		}
		this.editEmailButtonText = this.editEmailMode ? 'Återställ' : 'Ändra e-post';
	}

	emailValid() {
		return (this.emailGroup.valid && (this.emailRepeat.value === this.email.value));
	}


	enable(): void {
		this.phoneGroup.enable();
	}

	save(): void {
		this.saveState();
		this.toggleEmail(false);
		this.emitSave.emit(this.contactForm.getRawValue());
		this.contactForm.disable();
	}

	cancel(): void {
		this.restoreState();
		this.toggleEmail(false);
		this.contactForm.disable();
	}

	savePhone() {
		this.phoneForm = this.phoneGroup.getRawValue();
	}

	restorePhone() {
		this.phoneGroup.reset(this.phoneForm);
	}

	saveEmail() {
		this.emailForm = this.emailGroup.getRawValue();
		this.toggleEmail(false);
	}

	restoreEmail() {
		this.emailGroup.reset(this.emailForm);
	}

	// Setup initial form.
	private initializeForm(): void {

		this.contactForm = this.formBuilder.group({
			'phone': this.formBuilder.group([], {}),
			'email': this.formBuilder.group([], {})
		});

		this.$emailGroup = this.emailGroup.valueChanges.subscribe(
			(change) => {
				this.emailValid();
				// console.log(change);
			}
		);

		this.$phoneGroup = this.emailGroup.valueChanges.subscribe(
			(change) => {
				// console.log(change);
			}
		);

	}


	private addFormControlsToForm(): void {
		this.phoneGroup.registerControl(
			'phone', new FormControl(this.contactObject.phone ? this.contactObject.phone : '',
																												[])
		);

		this.phoneGroup.registerControl(
			'mobile', new FormControl(this.contactObject.mobile ? this.contactObject.mobile : '',
																													[])
		);

		this.emailGroup.registerControl(
			'email', new FormControl(this.contactObject.email ? this.contactObject.email : '',
																												[Validators.required, Validators.email])
		);

		this.emailGroup.registerControl(
			'email_repeat', new FormControl([],
																																			[Validators.email])
		);

		this.contactForm.disable();
	}

	private saveState() {
		this._contactForm = this.contactForm.getRawValue();
	}

	private restoreState() {
		this.contactForm.reset(this._contactForm);
	}

}
