import {User} from '../../models/user/user.model';
import {Role} from '../../models/user/role.model';
import {BusinessLevel} from '../../models/organization/business-level.model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Organization} from '../../models/organization/organization.model';
import {FormatOrganizationTypePipe} from '../../_shared/pipes/format-organization-type-pipe';


class ProfileController {

	private pipe: FormatOrganizationTypePipe = new FormatOrganizationTypePipe();

	createInfoObject(user: User, organization: Organization, help: Map<string, string>): InfoObject {
		const infoObject: InfoObject = {
			header: 'Information',
			headerTooltip: 'Tooltip',
			businessLevels: this.createBusinessLevelString(user.userEngagements[0].businessLevels),
			businessLevelsTooltip: 'Tooltip',
			firstName: user.firstName,
			lastName: user.lastName,
			userName: user.username,
			title: user.title,
			organization: organization.organizationName,
			validFrom: new Date(user.userEngagements[0].validFrom),
			validTo: user.userEngagements[0].validTo ? new Date(user.userEngagements[0].validTo) : null,
			validToTooltip: 'Tooltip',
		};
		if (help) {
			infoObject.headerTooltip = help.values['PROFILE_INFO_TOOLTIP'];
			infoObject.businessLevelsTooltip = help.values['PROFILE_INFO_BUSINESSLEVEL'];
			infoObject.validToTooltip = help.values['PROFILE_INFO_VALID_TO'];
		}
		return infoObject;
	}

	extractInfoFromFormControls(controls: FormGroup): User {
		const modifiedUser: User = new User();
		for (const [key, value] of Object.entries(controls.controls)) {
			// console.info('ContactControls: ', key, value);
			for (const [controlKey, controlValue] of Object.entries(controls.controls[key].value)) {
				// console.info('contactControl: ', controlKey, controlValue)
				modifiedUser[key] = controlValue.toString();
			}
		}
		return modifiedUser;
	}


	createContactObject(user: User, help: Map<string, string>): ContactObject {
		const contactObject: ContactObject = {
			header: 'Kontaktinformation',
			headerTooltip: 'Tooltip',
			email: user.electronicAddress.email,
			mobile: user.electronicAddress.mobile,
			phone: user.electronicAddress.telephone,
			// web: user.electronicAddress.web,
			// fax: user.electronicAddress.fax,
		};
		if (help) {
			contactObject.headerTooltip = help.values['PROFILE_CONTACT_TOOLTIP'];
		}
		return contactObject;
	}


	extractContactsFromFormControls(controlsRaw: FormGroup, user: User): void {

		const tempGroup: FormGroup = new FormBuilder().group(controlsRaw);

		for (const [key, value] of Object.entries(tempGroup.controls)) {
			// console.info('ContactControls: ', key, value);
			for (const [controlKey, controlValue] of Object.entries(tempGroup.controls[key].value)) {
				// console.info('contactControl: ', controlKey, controlValue);
				if ('phone' === controlKey) {
					if (user.electronicAddress.telephone !== controlValue) {
						user.electronicAddress.telephone = controlValue.toString();
					}
				} else if ('email_repeat' === controlKey) {
					// ignore.
				} else {
					if (user.electronicAddress[controlKey] !== controlValue) {
						user.electronicAddress[controlKey] = controlValue.toString();
					}
				}
			}
		}
	}


	/**
	 * Create the checkboxes out of all items, and check the ones that are available in user.
	 * @param notifications all and users notifications.
	 */
	createCheckboxItemsFromNotificationTypes(notifications: NotificationObject) {

		const checkboxItems: Array<CheckboxItem> = [];
		if (notifications === null || notifications.allNotifications === null || notifications.userNotifications === null) {
			return checkboxItems;
		}

		// allKey = name, allValue = description
		for (const [allKey, allValue] of Object.entries(notifications.allNotifications)) {
			// console.info('value: ', allKey, allValue);
			let checked = false;
			notifications.userNotifications.forEach((notification, index) => {
				if (notifications.userNotifications[index].toLowerCase() === allKey.toLowerCase()) {
					checked = true;
				}
			});

			// Add the checkboxItem to array.
			const item = {
				name: allKey,
				description: allValue,
				checked: checked
			} as CheckboxItem;
			checkboxItems.push(item);

		}

		// Optional: Sort array alphabetically.
		return checkboxItems.sort((a, b) => a.description.localeCompare(b.description));

	}


	createNotificationObject(notification: NotificationObject, organization: Organization, help: Map<string, string>): CheckboxObject {
		const notificationObject: CheckboxObject = {
			header: 'MEDDELANDE ( ' + this.pipe.transform(organization.organizationType) + ' )',
			headerTooltip: 'Tooltip',
			content: 'Innehåll',
			contentExtra: 'Extra',
			contentControls: this.createCheckboxItemsFromNotificationTypes(notification)
		};
		if (help) {
			notificationObject.headerTooltip = help.values['PROFILE_NOTIFICATION_TOOLTIP'];
			notificationObject.content = help.values['PROFILE_NOTIFICATION_CONTENT'];
			notificationObject.contentExtra = help.values['PROFILE_NOTIFICATION_EXTRA'];
		}
		return notificationObject;
	}


	extractNotificationTypesFromFormControls(controls: FormGroup, original: NotificationObject): Array<string> {
		const userNotifications: Array<string> = [];
		for (const [key, value] of Object.entries(controls.value)) {
			if (value) {
				for (const notification in original.allNotifications) {
					if (key.toLowerCase() === notification.toLowerCase()) {
						userNotifications.push(notification.toString());
					}
				}
			}
		}
		return userNotifications;
	}


	/**
	 * Create the checkboxes out of all items available and assigned to user.
	 * @param roles current users Roles[]
	 */
	createCheckboxItemsFromRoles(roles: Array<Role>): Array<CheckboxItem> {
		const array: Array<CheckboxItem> = [];
		roles.forEach((role) => {

			const checkboxItem = {
				name: role.name,
				description: role.description,
				// All user assigned roles should be checked.
				checked: true
			} as CheckboxItem;

			// console.info('Converted Role to CheckboxItem: ', item)
			array.push(checkboxItem);

		});
		return array;
	}


	createPermissionObject(user: User, organization: Organization, admins: Array<User>, help: Map<string, string>): CheckboxObject {
		const permissionObject: CheckboxObject = {
			header: 'Behörighet ( ' + this.pipe.transform(organization.organizationType) + ' )',
			headerTooltip: 'Tooltip',
			content: 'Innehåll',
			contentExtra: 'Extra',
			contentControls: this.createCheckboxItemsFromRoles(user.userEngagements[0].roles)
		};
		if (help) {
			permissionObject.headerTooltip = help.values['PROFILE_PERMISSIONS_TOOLTIP'];
			permissionObject.content = help.values['PROFILE_PERMISSIONS_CONTENT'];
			permissionObject.contentExtra = help.values['PROFILE_PERMISSIONS_CONSENT'];
			if (admins) {
				const temp: Array<string> = help.values['PROFILE_PERMISSIONS_CONTENT'].split('.').map(p => p.trim());
				// NOTICE: Very hardcoded and dependent on the passed string value in "PROFILE_PERMISSIONS_CONTENT".
				// Insert the created adminString as the second sentence in value string array.
				temp.splice(1, 0, this.createAdminString(admins));
				temp.pop();
				// Set the modified value instead of the standard text.
				permissionObject.content = temp.join('. ') + '.';
			}
		}
		// Returns a [default/partially/fully] initialized PermissionObject.
		return permissionObject;
	}


	/**
	 * Creates a readable formatted string of users organizational admins.
	 *
	 * @param admins Array of users
	 */
	createAdminString(admins: Array<User>): string | null {

		if (admins == null || admins.length <= 0) {
			return null;
		}

		let adminString: string;
		const amount = admins.length;

		// For every admin.
		admins.forEach((admin, index) => {

			const adminName: string = admin.firstName + ' ' + admin.lastName;
			adminString = '';

			// If admins are more than one.
			if (amount >= 1) {
				switch (index) {
				// Second to last admin.
				case (amount - 2):
					adminString += adminName + ' och ';
					break;
					// Last admin.
				case (amount - 1):
					adminString += adminName;
					break;
					// More than two.
				default:
					adminString += adminName + ', ';
				}
			}
		});

		// Return constructed object or null
		if (adminString) {
			return 'I ditt fall, ' + adminString;
		} else {
			return adminString;
		}

	}


	/**
	 * Creates a readable string of users assigned BusinessLevel.
	 * @param businessLevels array of BusinessLevel
	 */
	createBusinessLevelString(businessLevels: Array<BusinessLevel>): string {
		let levels: string;
		if (businessLevels === null || businessLevels.length <= 0) {
			return null;
		}

		levels = '';
		const x = businessLevels.length;
		businessLevels.forEach((level, index) => {
			// (x - 1) === last item in array.
			if (index === (x - 1)) {
				levels += level.name + '\n';
			} else {
				levels += level.name + '.';
			}
		});
		return levels;
	}


}

interface ProfileHeader {
	header: string;
	headerTooltip: string;
}

interface InfoObject extends ProfileHeader {
	userName: string;
	firstName: string;
	lastName: string;
	title: string;
	organization: string;
	businessLevels: string;
	businessLevelsTooltip: string;
	validFrom: Date;
	validTo: Date | null;
	validToTooltip: string;
}

interface ContactObject extends ProfileHeader {
	phone: string;
	mobile: string;
	email: string;
}

interface NotificationObject {
	allNotifications: Map<string, string>;
	userNotifications: Array<string>;
}

interface CheckboxItem {
	name: string;
	description: string;
	checked: boolean;
}

interface CheckboxObject extends ProfileHeader {
	content: string;
	contentExtra: string;
	contentControls: Array<CheckboxItem>;
}

export {ProfileController, InfoObject, ContactObject, CheckboxObject, CheckboxItem, NotificationObject};
