import {NgModule} from '@angular/core';
import {ProfileCheckboxComponent} from './checkbox/profile-checkbox.component';
import {MAT_TOOLTIP_DEFAULT_OPTIONS, MatTooltipDefaultOptions} from '@angular/material/tooltip';
import {ProfileComponent} from './profile.component';
import {EmailValidator, NG_ASYNC_VALIDATORS} from '@angular/forms';
import {SharedModule} from '../../_shared/shared.module';
import {ProfileService} from './profile.service';
import {ProfileRoutingModule} from './profile-routing.module';
import {ProfileContactComponent} from './contact/profile-contact.component';
import {ProfileInfoComponent} from './info/profile-info.component';
import {ProfileController} from './profile.controller';

export const toolTipDefaults: MatTooltipDefaultOptions = {
	showDelay: 150,
	hideDelay: 1000,
	position: 'below',
	touchendHideDelay: 500
};

@NgModule({
	imports: [
		SharedModule,
		ProfileRoutingModule
	],
	declarations: [
		ProfileComponent,
		ProfileCheckboxComponent,
		ProfileContactComponent,
		ProfileInfoComponent
	],
	exports: [],
	providers: [
		ProfileController,
		ProfileService,
		{provide: MAT_TOOLTIP_DEFAULT_OPTIONS, useValue: toolTipDefaults},
		{provide: NG_ASYNC_VALIDATORS, useExisting: EmailValidator, multi: true}
	],
})
export class ProfileModule {
}
