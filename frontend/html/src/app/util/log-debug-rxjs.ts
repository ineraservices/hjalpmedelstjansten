import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

export enum RxJsLoggingLevel {
	TRACE,
	DEBUG,
	INFO,
	ERROR
}

let rxjsLoggingLevel = RxJsLoggingLevel.INFO;

// let rxjsLoggingLevel = RxJsLoggingLevel.INFO;

export function setRxJsLoggingLevel(level: RxJsLoggingLevel) {
	rxjsLoggingLevel = level;
}


// Pass observables through here to observe and output console tracing depending on this.rxjsLoggingLevel
// Example: this.http.get(`path`).pipe(debug(level, message)).subscribe(...)
export const debug = (level: number, message: string) =>
	(source: Observable<any>) => source
		.pipe(tap(val => {

			// Only show messages above this level. (change/add per route/profile/instance)
			if (level >= rxjsLoggingLevel) {
				// eslint-disable-next-line no-console
				console.info(message + ': ', val);
			}
		})
		);
