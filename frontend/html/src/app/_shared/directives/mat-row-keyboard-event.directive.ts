import {Directive, EventEmitter, HostListener, Output} from '@angular/core';

@Directive({
	selector: '[appMatRowKeyboardEvent]'
})
export class MatRowKeyboardEventDirective {
	@Output()
	public enterPressed = new EventEmitter<void>();

	constructor() {
	}

	@HostListener('keydown', ['$event'])
	handleClick(event: KeyboardEvent) {
		if (event.key === "Enter") {
			this.enterPressed.emit();
		}
	}


}
