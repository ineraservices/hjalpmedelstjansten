import {AfterViewInit, Directive, ElementRef} from '@angular/core';

@Directive({
	// eslint-disable-next-line @angular-eslint/directive-selector
	selector: '[autofocus]'
})
export class AutoFocusDirective implements AfterViewInit {

	constructor(
		private elementRef: ElementRef
	) {
	}

	// TODO! Check if this new directive works, replaces the npm package autofocus-fix that does not work with ivy.

	ngAfterViewInit(): void {
		this.elementRef.nativeElement.focus();
	}
}
