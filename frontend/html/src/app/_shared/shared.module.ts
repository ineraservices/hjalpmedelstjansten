import {NgModule} from '@angular/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {MatIconModule} from '@angular/material/icon';
import {FormatOrganizationTypePipe} from './pipes/format-organization-type-pipe';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCardModule} from '@angular/material/card';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AutoFocusDirective} from "./directives/auto-focus.directive";
import {MatRowKeyboardEventDirective} from './directives/mat-row-keyboard-event.directive';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		MatFormFieldModule,
		MatIconModule,
		MatTooltipModule,
		MatCheckboxModule,
		MatInputModule,
		MatButtonModule,
		MatProgressSpinnerModule,
		MatDatepickerModule,
		MatCardModule,
		MatButtonToggleModule,
		FlexLayoutModule,
		BrowserAnimationsModule
	],
	declarations: [
		FormatOrganizationTypePipe,
		AutoFocusDirective,
		MatRowKeyboardEventDirective
	],
	exports: [
		FormatOrganizationTypePipe,
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		MatFormFieldModule,
		MatIconModule,
		MatTooltipModule,
		MatCheckboxModule,
		MatInputModule,
		MatButtonModule,
		MatProgressSpinnerModule,
		MatDatepickerModule,
		MatCardModule,
		MatButtonToggleModule,
		FlexLayoutModule,
		BrowserAnimationsModule,
		AutoFocusDirective,
		MatRowKeyboardEventDirective
	],
	providers: [FormatOrganizationTypePipe]
})
export class SharedModule {
}
