package se.inera.hjalpmedelstjansten.export;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.Singleton;
import jakarta.inject.Inject;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

/**
 * This class uses Jsch (from Jcraft) to upload files to a remote
 * FTP server. In order to make this work, four environment variables must be set
 *
 * FTP_HOST: to the hostname of the FTP server
 * FTP_PORT: to the portname of the FTP server
 * FTP_USERNAME: login username
 * FTP_PASSWORD: login password
 *
 * @see http://www.jcraft.com/jsch/
 */
@Singleton
public class FtpController {

    @Inject
    HjmtLogger LOG;

    private boolean enabled = false;
    private String xmlFtpServerHost;
    private Integer xmlFtpServerPort;
    private String xmlFtpServerUsername;
    private String xmlFtpServerPassword;

    /**
     * Upload the file at the given <code>sourceFileLocation</code> to the folder
     * with the given <code>targetFileDirectory</code>
     *
     * @param sourceFileLocation
     * @param targetFileDirectory
     * @return
     */
    public boolean uploadFile( String sourceFileLocation, String targetFileDirectory ) {
        LOG.log(Level.FINEST, "uploadFile( enabled: {0}, sourceFileLocation: {1}, targetFileDirectory: {2} )", new Object[] {enabled, sourceFileLocation, targetFileDirectory});
        boolean success = false;
        if( enabled ) {
            JSch jsch = new JSch();
            File sourceFile = new File(sourceFileLocation);
            Session session = null;
            ChannelSftp channelSftp = null;
            try (InputStream sourceInputStream = new FileInputStream(sourceFile) ) {
                String remoteFileLocation = targetFileDirectory + "/" + sourceFile.getName();
                session = jsch.getSession(xmlFtpServerUsername, xmlFtpServerHost, xmlFtpServerPort);
                session.setPassword(xmlFtpServerPassword);
                // ftp is in same network so host checking should not be a problem, but
                // added improvement jira
                session.setConfig("StrictHostKeyChecking", "no");
                session.connect();
                Channel channel = session.openChannel("sftp");
                channel.connect();
                channelSftp = (ChannelSftp) channel;
                channelSftp.put(sourceInputStream, remoteFileLocation);
                success = true;
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Failed to upload file to sftp", ex);
            } catch (JSchException ex) {
                LOG.log(Level.SEVERE, "Failed to upload file to sftp", ex);
            } catch (SftpException ex) {
                LOG.log(Level.SEVERE, "Failed to upload file to sftp", ex);
            } finally {
                if( channelSftp != null && channelSftp.isConnected() ) {
                    channelSftp.exit();
                }
                if( session != null && session.isConnected() ) {
                    session.disconnect();
                }
            }
        } else {
            LOG.log( Level.WARNING, "Cannot upload file to FTP since FTP is not configured correctly." );
        }
        return success;
    }

    @PostConstruct
    private void initialize() {
        xmlFtpServerHost = System.getenv("FTP_HOST");
        String xmlFtpServerPortString = System.getenv("FTP_PORT");
        if( xmlFtpServerPortString != null &&
                !xmlFtpServerPortString.isEmpty() &&
                xmlFtpServerPortString.chars().allMatch(Character::isDigit)) {
            xmlFtpServerPort = Integer.parseInt(xmlFtpServerPortString);
        }
        xmlFtpServerUsername = System.getenv("FTP_USERNAME");
        xmlFtpServerPassword = System.getenv("FTP_PASSWORD");
        if( xmlFtpServerHost != null && !xmlFtpServerHost.isEmpty() &&
                xmlFtpServerPort != null &&
                xmlFtpServerUsername != null && !xmlFtpServerUsername.isEmpty() &&
                xmlFtpServerPassword != null && !xmlFtpServerPassword.isEmpty() ) {
            enabled = true;
        }
    }

}
