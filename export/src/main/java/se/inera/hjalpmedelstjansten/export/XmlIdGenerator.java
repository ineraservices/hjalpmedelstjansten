package se.inera.hjalpmedelstjansten.export;

import java.util.HashMap;
import java.util.Map;


public class XmlIdGenerator {
    
    private long nextId = 0;
    private Map<Long, Long> documentTypesMap = new HashMap<>();
    private Map<Long, Long> preventiveMaintenancesMap = new HashMap<>();
    private Map<Long, Long> guaranteeUnitsMap = new HashMap<>();
    private Map<Long, Long> orderUnitsMap = new HashMap<>();
    private Map<Long, Long> organizationsMap = new HashMap<>();
    private Map<Long, Long> articlesMap = new HashMap<>();
    private Map<Long, Long> fitsToArticlesMap = new HashMap<>();
    private Map<Long, Long> productsMap = new HashMap<>();
    private Map<Long, Long> directivesMap = new HashMap<>();
    private Map<Long, Long> classificationsMap = new HashMap<>();
    private Map<Long, Long> agreementsMap = new HashMap<>();
    private Map<String, Long> pricelistRowStatusMap = new HashMap<>();
    
    public long generateDocumentTypesXmlId( long uniqueDatabaseId ) {
        documentTypesMap.put(uniqueDatabaseId, getNextId());
        return nextId;
    }
    
    public Long getDocumentTypesXmlId( long uniqueDatabaseId ) {
        return documentTypesMap.get(uniqueDatabaseId);
    }
    
    public long generatePreventiveMaintenanceXmlId( long uniqueDatabaseId ) {
        preventiveMaintenancesMap.put(uniqueDatabaseId, getNextId());
        return nextId;
    }
    
    public Long getPreventiveMaintenanceXmlId( long uniqueDatabaseId ) {
        return preventiveMaintenancesMap.get(uniqueDatabaseId);
    }
    
    public long generateGuaranteeUnitsXmlId( long uniqueDatabaseId ) {
        guaranteeUnitsMap.put(uniqueDatabaseId, getNextId());
        return nextId;
    }
    
    public Long getGuaranteeUnitsXmlId( long uniqueDatabaseId ) {
        return guaranteeUnitsMap.get(uniqueDatabaseId);
    }
    
    public Map<Long, Long> getGuaranteeUnitsMap() {
        return guaranteeUnitsMap;
    }
    
    public long generateOrderUnitsXmlId( long uniqueDatabaseId ) {
        orderUnitsMap.put(uniqueDatabaseId, getNextId());
        return nextId;
    }
    
    public Long getOrdereUnitsXmlId( long uniqueDatabaseId ) {
        return orderUnitsMap.get(uniqueDatabaseId);
    }
    
    public Map<Long, Long> getOrderUnitsMap() {
        return orderUnitsMap;
    }
    
    public long generateOrganizationsXmlId( long uniqueDatabaseId ) {
        organizationsMap.put(uniqueDatabaseId, getNextId());
        return nextId;
    }
    
    public Long getOrganizationsXmlId( long uniqueDatabaseId ) {
        return organizationsMap.get(uniqueDatabaseId);
    }

    public Map<Long, Long> getOrganizationsMap() {
        return organizationsMap;
    }
    
    public long generateArticlesXmlId( long uniqueDatabaseId ) {
        articlesMap.put(uniqueDatabaseId, getNextId());
        return nextId;
    }
    
    public Long getArticlesXmlId( long uniqueDatabaseId ) {
        return articlesMap.get(uniqueDatabaseId);
    }
    
    public Map<Long, Long> getArticlesMap() {
        return articlesMap;
    }
    
    public long generateFitsToArticlesMapXmlId( long uniqueDatabaseId ) {
        fitsToArticlesMap.put(uniqueDatabaseId, getNextId());
        return nextId;
    }
    
    public Long getFitsToArticlesMapXmlId( long uniqueDatabaseId ) {
        return fitsToArticlesMap.get(uniqueDatabaseId);
    }
    
    public Map<Long, Long> getFitsToArticlesMap() {
        return fitsToArticlesMap;
    }
    
    public long generateProductsXmlId( long uniqueDatabaseId ) {
        productsMap.put(uniqueDatabaseId, getNextId());
        return nextId;
    }
    
    public Long getProductsXmlId( long uniqueDatabaseId ) {
        return productsMap.get(uniqueDatabaseId);
    }
    
    public Map<Long, Long> getProductsMap() {
        return productsMap;
    }
    
    public long generateDirectivesXmlId( long uniqueDatabaseId ) {
        directivesMap.put(uniqueDatabaseId, getNextId());
        return nextId;
    }
    
    public Long getDirectivesXmlId( long uniqueDatabaseId ) {
        return directivesMap.get(uniqueDatabaseId);
    }
    
    public Map<Long, Long> getDirectivesMap() {
        return directivesMap;
    }
    
    public long generateClassificationsXmlId( long uniqueDatabaseId ) {
        classificationsMap.put(uniqueDatabaseId, getNextId());
        return nextId;
    }
    
    public Long getClassificationsXmlId( long uniqueDatabaseId ) {
        return classificationsMap.get(uniqueDatabaseId);
    }
    
    public Map<Long, Long> getClassificationsMap() {
        return classificationsMap;
    }
    
    public long generatePricelistRowStatusXmlId( String status ) {
        pricelistRowStatusMap.put(status, getNextId());
        return nextId;
    }
    
    public Long getPricelistRowStatusXmlId( String status ) {
        return pricelistRowStatusMap.get(status);
    }
    
    public Map<String, Long> getPricelistRowStatusMap() {
        return pricelistRowStatusMap;
    }
    
    public long generateAgreementsXmlId( long uniqueDatabaseId ) {
        agreementsMap.put(uniqueDatabaseId, getNextId());
        return nextId;
    }
    
    public Long getAgreementsXmlId( long uniqueDatabaseId ) {
        return agreementsMap.get(uniqueDatabaseId);
    }
    
    public Map<Long, Long> getAgreementsMap() {
        return agreementsMap;
    }
    
    private long getNextId() {
        return ++nextId;
    }
    
}
