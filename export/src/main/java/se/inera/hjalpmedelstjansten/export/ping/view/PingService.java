package se.inera.hjalpmedelstjansten.export.ping.view;

import jakarta.ejb.Stateless;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

/**
 * This ping service is used to check if the service is up. In the first version
 * it will only return response code 200 if possible.
 *
 */
@Stateless
@Path("ping")
public class PingService {

    @GET
    public Response ping() {
        return Response.ok().build();
    }

}
