package se.inera.hjalpmedelstjansten.export;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelist;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.ExportSettings;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEDirective;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVDocumentType;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;
import se.inera.hjalpmedelstjansten.model.entity.media.ArticleMediaDocument;
import se.inera.hjalpmedelstjansten.model.entity.media.ArticleMediaImage;

import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


@Stateless
public class ExportGenerationController {

    private static final String EXPORT_DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String EXPORT_FILENAME_DATE_TIME_PATTERN = "yyyyMMdd'_'HHmm";

    private static final String ACCESSORY_ELEMENT_NAME = "_x0035_c128a39d932418d93e4c63576ae67d6";
    private static final String SPAREPART_ELEMENT_NAME = "_x0036_fc8b90656504d6aac2274cffb074fff";
    private static final String SETTING_ELEMENT_NAME = "SettingFor";
    private static final String SERVICE_ELEMENT_NAME = "ServiceFor";

    private static final String XML_NAME_IN_ZIP_FILE = "data.xml";

    @Inject
    private HjmtLogger LOG;

    @PersistenceContext(unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    FtpController ftpController;

    @Inject
    private String exportTmpDirectory;

    /**
     * Creates export files for the given export settings.
     * Probably called by a scheduled job. Generates XML, zips it and uploads it
     * to the correctftp area.
     *
     * @param exportSettingsUniqueId unique id of the <code>ExportSettings</code> to
     * generate XML for
     * @param checkAlreadyExportedToday if set to true, no XML file will be generated if one already
     * has been generated on the same day
     * @param sessionId
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @TransactionTimeout(value = 3, unit = TimeUnit.HOURS)
    public void export(long exportSettingsUniqueId, boolean checkAlreadyExportedToday, String sessionId) {
        LOG.log(Level.FINEST, "export( exportSettingsUniqueId: {0}, checkAlreadyExportedToday: {1} )", new Object[] {exportSettingsUniqueId, checkAlreadyExportedToday} );
        Session session = em.unwrap(Session.class);
        session.setHibernateFlushMode(FlushMode.MANUAL);
        ExportSettings exportSettings = em.find(ExportSettings.class, exportSettingsUniqueId);
        if( exportSettings == null ) {
            LOG.log( Level.WARNING, "No export settings found for id: {0}", new Object[] {exportSettingsUniqueId} );
            return;
        }
        if( !exportSettings.isEnabled() ) {
            LOG.log( Level.INFO, "Export settings disabled id: {0}", new Object[] {exportSettingsUniqueId} );
            return;
        }
        LOG.log( Level.FINEST, "Handle export settings with id: {0}, organization: {1}, lastExported: {2}", new Object[] {exportSettings.getUniqueId(), exportSettings.getOrganization().getUniqueId(), exportSettings.getLastExported()});
        if( checkAlreadyExportedToday && exportSettings.getLastExported() != null && isSameDay(exportSettings.getLastExported().getTime(), new Date().getTime()) ) {
            // already exported today
            LOG.log( Level.FINEST, "Export for export settings {0} have already been generated today", new Object[] {exportSettings.getUniqueId()} );
            return;
        }
        LOG.log( Level.INFO, "Start generating XML Export for {0}", new Object[] {exportSettings.getOrganization().getOrganizationName()});
        String exportFileLocation = generateFileName(exportSettings);
        String xmlExportFileLocation = exportFileLocation + ".xml";
        File xmlFile = new File(xmlExportFileLocation);
        String zipExportFileLocation = exportFileLocation + ".zip";
        File zipFile = new File(zipExportFileLocation);
        try {
            byte[] buffer = new byte[1024];
            LOG.log( Level.FINEST, "xmlExportFileLocation: {0}, zipExportFileLocation: {1}", new Object[] {exportFileLocation, zipExportFileLocation});

            // create xml file
            LOG.log(Level.FINEST, "Create XML file: {0}", new Object[]{xmlExportFileLocation});
            try (FileWriter fileWriter = new FileWriter(xmlFile) ) {
                fillXML(fileWriter, exportSettings, sessionId);
                // when debugging, put a breakpoint after this comment, xml file is found in docker
                // git bash:
                // $ docker cp <containerid>:/tmp/<file to copy> <destination path>
                LOG.log(Level.FINEST, "XML export file generated " + xmlExportFileLocation + " for export settings: " + exportSettings.getUniqueId());
            } catch (IOException | XMLStreamException ex) {
                LOG.log(Level.SEVERE, "Failed to generate XML export file " + xmlExportFileLocation + " for export settings: " + exportSettings.getUniqueId(), ex);
                // if we fail to generate xml file, we don't have to proceed
                // with zipping and uploading
                return;
            }

            // create zip file
            LOG.log(Level.FINEST, "Create ZIP file: {0}", new Object[]{zipExportFileLocation});
            try ( FileOutputStream fileOutputStream = new FileOutputStream(zipExportFileLocation);
                ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream)) {
                ZipEntry zipEntry = new ZipEntry(XML_NAME_IN_ZIP_FILE);
                zipOutputStream.putNextEntry(zipEntry);
                try (FileInputStream fileInputStream = new FileInputStream(xmlExportFileLocation)) {
                    int length;
                    while((length = fileInputStream.read(buffer)) > 0 ) {
                        zipOutputStream.write(buffer, 0, length);
                    }
                }
                zipOutputStream.closeEntry();
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Failed to generate zip file: " + zipExportFileLocation + " for export settings: " + exportSettings.getUniqueId(), ex);
            }

            // send to ftp
            boolean success = ftpController.uploadFile(zipExportFileLocation, exportSettings.getFilename());
            if( !success ) {
                LOG.log(Level.SEVERE, "Failed to upload zip file: " + zipExportFileLocation + " for export settings: " + exportSettings.getUniqueId());
                return;
            }

            LOG.log(Level.INFO, "Successfully transferred zip file: " + zipExportFileLocation + " to ftp for export settings: " + exportSettings.getUniqueId());

            // ok, success, update export settings
            exportSettings.setNumberOfExports(exportSettings.getNumberOfExports()+1);
            exportSettings.setLastExported(new Date());
            em.flush();
            LOG.logAudit(null, InternalAudit.ActionType.CREATE.toString(), InternalAudit.EntityType.EXPORTFILE.toString(), exportSettings.getUniqueId().toString(), null);
            LOG.log( Level.INFO, "Finished generating XML Export for {0}", new Object[] {exportSettings.getOrganization().getOrganizationName()});
        } finally {
            // clean up

            // delete zip file
            LOG.log(Level.FINEST, "Delete ZIP file: {0}", new Object[]{zipExportFileLocation});
            boolean successfulZipDelete = zipFile.delete();
            if( !successfulZipDelete ) {
                LOG.log( Level.WARNING, "Failed to delete ZiP file: {0} after export for export settings: {1}", new Object[] {zipExportFileLocation, exportSettings.getUniqueId()} );
            }

            // delete xml file
            LOG.log(Level.FINEST, "Delete XML file: {0}", new Object[]{xmlExportFileLocation});
            boolean successfulXmlDelete = xmlFile.delete();
            if( !successfulXmlDelete ) {
                LOG.log( Level.WARNING, "Failed to delete XML file: {0} after export for export settings: {1}", new Object[] {xmlExportFileLocation, exportSettings.getUniqueId()} );
            }
            else
                LOG.log(Level.FINEST, "XML file deleted");
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @TransactionTimeout(value = 3, unit = TimeUnit.HOURS)
    public void exportAvtal(long exportSettingsUniqueId, boolean checkAlreadyExportedToday, String sessionId) {
        LOG.log(Level.FINEST, "export( exportSettingsUniqueId: {0}, checkAlreadyExportedToday: {1} )", new Object[] {exportSettingsUniqueId, checkAlreadyExportedToday} );
        Session session = em.unwrap(Session.class);
        session.setHibernateFlushMode(FlushMode.MANUAL);
        ExportSettings exportSettings = em.find(ExportSettings.class, exportSettingsUniqueId);
        if( exportSettings == null ) {
            LOG.log( Level.WARNING, "No export settings found for id: {0}", new Object[] {exportSettingsUniqueId} );
            return;
        }
        if( !exportSettings.isEnabled() ) {
            LOG.log( Level.INFO, "Export settings disabled id: {0}", new Object[] {exportSettingsUniqueId} );
            return;
        }
        LOG.log( Level.FINEST, "Handle export settings with id: {0}, organization: {1}, lastExported: {2}", new Object[] {exportSettings.getUniqueId(), exportSettings.getOrganization().getUniqueId(), exportSettings.getLastExported()});
        if( checkAlreadyExportedToday && exportSettings.getLastExported() != null && isSameDay(exportSettings.getLastExported().getTime(), new Date().getTime()) ) {
            // already exported today
            LOG.log( Level.FINEST, "Export for export settings {0} have already been generated today", new Object[] {exportSettings.getUniqueId()} );
            return;
        }
        LOG.log( Level.INFO, "Start generating XML Export (Avtal) for {0}", new Object[] {exportSettings.getOrganization().getOrganizationName()});
        String exportFileLocation = generateFileName(exportSettings);
        String xmlExportFileLocation = exportFileLocation + ".xml";
        File xmlFile = new File(xmlExportFileLocation);
        String zipExportFileLocation = exportFileLocation + ".zip";
        File zipFile = new File(zipExportFileLocation);
        try {
            byte[] buffer = new byte[1024];
            LOG.log( Level.FINEST, "xmlExportFileLocation: {0}, zipExportFileLocation: {1}", new Object[] {xmlExportFileLocation, zipExportFileLocation});

            // create xml file
            LOG.log(Level.FINEST, "Create XML Avtal file: {0}", new Object[]{xmlExportFileLocation});
            try (FileWriter fileWriter = new FileWriter(xmlFile) ) {
                fillXMLAvtal(fileWriter, exportSettings, sessionId);
                // when debugging, put a breakpoint after this comment, xml file is found in docker
                // git bash:
                // $ docker cp <containerid>:/tmp/<file to copy> <destination path>
                LOG.log(Level.FINEST, "XML export file generated " + xmlExportFileLocation + " for export settings: " + exportSettings.getUniqueId());
            } catch (IOException | XMLStreamException ex) {
                LOG.log(Level.SEVERE, "Failed to generate XML export file " + xmlExportFileLocation + " for export settings: " + exportSettings.getUniqueId(), ex);
                // if we fail to generate xml file, we don't have to proceed
                // with zipping and uploading
                return;
            }

            // create zip file
            LOG.log(Level.FINEST, "Create ZIP file: {0}", new Object[]{zipExportFileLocation});
            try ( FileOutputStream fileOutputStream = new FileOutputStream(zipExportFileLocation);
                  ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream)) {
                ZipEntry zipEntry = new ZipEntry(XML_NAME_IN_ZIP_FILE);
                zipOutputStream.putNextEntry(zipEntry);
                try (FileInputStream fileInputStream = new FileInputStream(xmlExportFileLocation)) {
                    int length;
                    while((length = fileInputStream.read(buffer)) > 0 ) {
                        zipOutputStream.write(buffer, 0, length);
                    }
                }
                zipOutputStream.closeEntry();
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Failed to generate zip file: " + zipExportFileLocation + " for export settings: " + exportSettings.getUniqueId(), ex);
            }

            // send to ftp
            boolean success = ftpController.uploadFile(zipExportFileLocation, exportSettings.getFilename());
            if( !success ) {
                LOG.log(Level.SEVERE, "Failed to upload zip file: " + zipExportFileLocation + " for export settings: " + exportSettings.getUniqueId());
                return;
            }

            LOG.log(Level.INFO, "Successfully transferred zip file: " + zipExportFileLocation + " to ftp for export settings: " + exportSettings.getUniqueId());

            // ok, success, update export settings
            exportSettings.setNumberOfExports(exportSettings.getNumberOfExports()+1);
            exportSettings.setLastExported(new Date());
            em.flush();
            LOG.logAudit(null, InternalAudit.ActionType.CREATE.toString(), InternalAudit.EntityType.EXPORTFILE.toString(), exportSettings.getUniqueId().toString(), null);
            LOG.log( Level.INFO, "Finished generating XML Export for {0}", new Object[] {exportSettings.getOrganization().getOrganizationName()});
        } finally {
            // clean up

            // delete zip file
            LOG.log(Level.FINEST, "Delete ZIP file: {0}", new Object[]{zipExportFileLocation});
            boolean successfulZipDelete = zipFile.delete();
            if( !successfulZipDelete ) {
                LOG.log( Level.WARNING, "Failed to delete ZiP file: {0} after export for export settings: {1}", new Object[] {zipExportFileLocation, exportSettings.getUniqueId()} );
            }

            // delete xml file
            LOG.log(Level.FINEST, "Delete XML file: {0}", new Object[]{xmlExportFileLocation});
            boolean successfulXmlDelete = xmlFile.delete();
            if( !successfulXmlDelete ) {
                LOG.log( Level.WARNING, "Failed to delete XML file: {0} after export for export settings: {1}", new Object[] {xmlExportFileLocation, exportSettings.getUniqueId()} );
            }
            else
                LOG.log(Level.FINEST, "XML file deleted");
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @TransactionTimeout(value = 3, unit = TimeUnit.HOURS)
    public void exportGP(long exportSettingsUniqueId, boolean checkAlreadyExportedToday, String sessionId) {
        LOG.log(Level.FINEST, "export( exportSettingsUniqueId: {0}, checkAlreadyExportedToday: {1} )", new Object[] {exportSettingsUniqueId, checkAlreadyExportedToday} );
        Session session = em.unwrap(Session.class);
        session.setHibernateFlushMode(FlushMode.MANUAL);
        ExportSettings exportSettings = em.find(ExportSettings.class, exportSettingsUniqueId);
        if( exportSettings == null ) {
            LOG.log( Level.WARNING, "No export settings found for id: {0}", new Object[] {exportSettingsUniqueId} );
            return;
        }
        if( !exportSettings.isEnabled() ) {
            LOG.log( Level.INFO, "Export settings disabled id: {0}", new Object[] {exportSettingsUniqueId} );
            return;
        }
        LOG.log( Level.FINEST, "Handle export settings with id: {0}, organization: {1}, lastExported: {2}", new Object[] {exportSettings.getUniqueId(), exportSettings.getOrganization().getUniqueId(), exportSettings.getLastExported()});
        if( checkAlreadyExportedToday && exportSettings.getLastExported() != null && isSameDay(exportSettings.getLastExported().getTime(), new Date().getTime()) ) {
            // already exported today
            LOG.log( Level.FINEST, "Export for export settings {0} have already been generated today", new Object[] {exportSettings.getUniqueId()} );
            return;
        }
        LOG.log( Level.INFO, "Start generating XML Export (GP) for {0}", new Object[] {exportSettings.getOrganization().getOrganizationName()});
        String exportFileLocation = generateFileName(exportSettings);
        String xmlExportFileLocation = exportFileLocation + ".xml";
        File xmlFile = new File(xmlExportFileLocation);
        String zipExportFileLocation = exportFileLocation + ".zip";
        File zipFile = new File(zipExportFileLocation);
        try {
            byte[] buffer = new byte[1024];
            LOG.log( Level.FINEST, "xmlExportFileLocation: {0}, zipExportFileLocation: {1}", new Object[] {xmlExportFileLocation, zipExportFileLocation});

            // create xml file
            LOG.log(Level.FINEST, "Create XML file GP: {0}", new Object[]{xmlExportFileLocation});
            try (FileWriter fileWriter = new FileWriter(xmlFile) ) {
                fillXMLGP(fileWriter, exportSettings, sessionId);
                // when debugging, put a breakpoint after this comment, xml file is found in docker
                // git bash:
                // $ docker cp <containerid>:/tmp/<file to copy> <destination path>
                LOG.log(Level.FINEST, "XML export file generated " + xmlExportFileLocation + " for export settings: " + exportSettings.getUniqueId());
            } catch (IOException | XMLStreamException ex) {
                LOG.log(Level.SEVERE, "Failed to generate XML export file " + xmlExportFileLocation + " for export settings: " + exportSettings.getUniqueId(), ex);
                // if we fail to generate xml file, we don't have to proceed
                // with zipping and uploading
                return;
            }

            // create zip file
            LOG.log(Level.FINEST, "Create ZIP file: {0}", new Object[]{zipExportFileLocation});
            try ( FileOutputStream fileOutputStream = new FileOutputStream(zipExportFileLocation);
                  ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream)) {
                ZipEntry zipEntry = new ZipEntry(XML_NAME_IN_ZIP_FILE);
                zipOutputStream.putNextEntry(zipEntry);
                try (FileInputStream fileInputStream = new FileInputStream(xmlExportFileLocation)) {
                    int length;
                    while((length = fileInputStream.read(buffer)) > 0 ) {
                        zipOutputStream.write(buffer, 0, length);
                    }
                }
                zipOutputStream.closeEntry();
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Failed to generate zip file: " + zipExportFileLocation + " for export settings: " + exportSettings.getUniqueId(), ex);
            }

            // send to ftp
            boolean success = ftpController.uploadFile(zipExportFileLocation, exportSettings.getFilename());
            if( !success ) {
                LOG.log(Level.SEVERE, "Failed to upload zip file: " + zipExportFileLocation + " for export settings: " + exportSettings.getUniqueId());
                return;
            }

            LOG.log(Level.INFO, "Successfully transferred zip file: " + zipExportFileLocation + " to ftp for export settings: " + exportSettings.getUniqueId());

            // ok, success, update export settings
            exportSettings.setNumberOfExports(exportSettings.getNumberOfExports()+1);
            exportSettings.setLastExported(new Date());
            em.flush();
            LOG.logAudit(null, InternalAudit.ActionType.CREATE.toString(), InternalAudit.EntityType.EXPORTFILE.toString(), exportSettings.getUniqueId().toString(), null);
            LOG.log( Level.INFO, "Finished generating XML Export for {0}", new Object[] {exportSettings.getOrganization().getOrganizationName()});
        } finally {
            // clean up

            // delete zip file
            LOG.log(Level.FINEST, "Delete ZIP file: {0}", new Object[]{zipExportFileLocation});
            boolean successfulZipDelete = zipFile.delete();
            if( !successfulZipDelete ) {
                LOG.log( Level.WARNING, "Failed to delete ZiP file: {0} after export for export settings: {1}", new Object[] {zipExportFileLocation, exportSettings.getUniqueId()} );
            }

            // delete xml file
            LOG.log(Level.FINEST, "Delete XML file: {0}", new Object[]{xmlExportFileLocation});
            boolean successfulXmlDelete = xmlFile.delete();
            if( !successfulXmlDelete ) {
                LOG.log( Level.WARNING, "Failed to delete XML file: {0} after export for export settings: {1}", new Object[] {xmlExportFileLocation, exportSettings.getUniqueId()} );
            }
            else
                LOG.log(Level.FINEST, "XML file deleted");
        }
    }

    private String generateFileName(ExportSettings exportSettings) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(EXPORT_FILENAME_DATE_TIME_PATTERN);
        String dateString = dateTimeFormatter.format(ZonedDateTime.now());
        return exportTmpDirectory +
                (exportTmpDirectory.endsWith(File.pathSeparator) ? "": "/") +
                exportSettings.getFilename() +
                "_" +
                (exportSettings.getNumberOfExports() + 1 ) +
                "_" +
                dateString;
    }

    /**
     * Method that generates the actual XML.
     *
     * @param writer the writer where the XML should be generated
     * @param exportSettings settings for this XML export
     * @throws XMLStreamException if an error creating XML arises
     */
    private void fillXML( Writer writer, ExportSettings exportSettings, String sessionId ) throws XMLStreamException {
        LOG.log(Level.FINEST, "fillXML( customerUniqueId: {0} )", new Object[] {exportSettings.getOrganization().getUniqueId()} );
        long start = System.currentTimeMillis();
        XMLOutputFactory factory = XMLOutputFactory.newInstance();
        XMLStreamWriter xmlWriter = factory.createXMLStreamWriter(writer);
        xmlWriter.writeStartDocument("UTF-8", "1.0");
        xmlWriter.writeCharacters(System.lineSeparator());
        xmlWriter.writeStartElement("Export");
        xmlWriter.writeCharacters(System.lineSeparator());

        // write basic information
        // writeContainsChangesSince(xmlWriter); only if this is not a complete export (later)
        writeExportNumber(xmlWriter, exportSettings.getNumberOfExports());
        writeTimeCreated(xmlWriter);

        XmlIdGenerator xmlIdGenerator = new XmlIdGenerator();

        List<Category> allCategories = generateClassifications(xmlIdGenerator);

        writeRemovedLists(xmlWriter);

        // write guarantee units
        writeGuaranteeUnits(xmlWriter, xmlIdGenerator);

        // write preventive maintenances
        writePreventiveMaintenances(xmlWriter, xmlIdGenerator);

        // write document types
        writeDocumentTypes(xmlWriter, xmlIdGenerator);

        // write agreements
        xmlWriter.writeStartElement("ListAgreement");
        xmlWriter.writeCharacters(System.lineSeparator());
        List<Long> agreementIds;
        List<Agreement.Status> statuses = new ArrayList<>();
        statuses.add(Agreement.Status.CURRENT);
        statuses.add(Agreement.Status.FUTURE);
        if( exportSettings.getBusinessLevel() == null ) {
            agreementIds = getAgreementsForCustomerAndStatus(exportSettings.getOrganization().getUniqueId(), statuses);
        } else {
            agreementIds = getAgreementsForCustomerAndBusinessLevelAndStatus(exportSettings.getOrganization().getUniqueId(), exportSettings.getBusinessLevel().getUniqueId(), statuses);
        }
        //if (1==0) {
        if( agreementIds != null && !agreementIds.isEmpty() ) {
            LOG.log( Level.FINEST, "Found {0} agreements", new Object[] {agreementIds.size()});
            //LOG.log( Level.INFO, "Found {0} agreements", new Object[] {agreementIds.size()});
            // write agreements
            for( Long agreementId : agreementIds ) {
                //if (agreementId == 5698920) {
                    LOG.log(Level.FINEST, "agreementId: {0}", new Object[]{agreementId});
                    //LOG.log(Level.INFO, "agreementId: {0}", new Object[]{agreementId});
                    Agreement agreement = em.find(Agreement.class, agreementId);
                    List<AgreementPricelist> currentAndFuturePricelists = new ArrayList<>();
                    AgreementPricelist currentPricelist = getCurrentPricelist(agreement.getUniqueId());
                    if (currentPricelist != null) {
                        currentAndFuturePricelists.add(currentPricelist);
                    }
                    List<AgreementPricelist> futurePricelists = getFuturePricelists(agreement.getUniqueId());
                    if (futurePricelists != null && !futurePricelists.isEmpty()) {
                        currentAndFuturePricelists.addAll(futurePricelists);
                    }
                    LOG.log(Level.FINEST, "Found {0} currentAndFuturePricelists", new Object[]{currentAndFuturePricelists.size()});
                    //LOG.log(Level.INFO, "Found {0} currentAndFuturePricelists", new Object[]{currentAndFuturePricelists.size()});
                    if (!currentAndFuturePricelists.isEmpty()) {
                        boolean isSharedAgreement = writeAgreement(xmlWriter, agreement, currentAndFuturePricelists, xmlIdGenerator, exportSettings);

                        // appears not to be necessary
                        //if( isSharedAgreement ) {
                        //    writeSharedAgreement(xmlWriter, agreement, xmlIdGenerator, exportSettings);
                        //}
                        //if( isSharedAgreement ) {
                        //    LOG.log(Level.FINEST, "This one is shared {0}", new Object[]{agreementId});
                        //}
                        //else {
                        //    LOG.log(Level.FINEST, "This one is NOT shared {0}", new Object[]{agreementId});
                        //}
                    }
                //}
            }
        }
        else {
            LOG.log(Level.FINEST, "Found NO agreements");
            //LOG.log(Level.INFO, "Found NO agreements");
        }

        List<Long> generalPricelists = getGeneralPricelistsByExportSettings(exportSettings.getUniqueId());
        //if( 1 == 0 ) {
        if( generalPricelists != null && !generalPricelists.isEmpty() ) {

            LOG.log(Level.FINEST, "generalPricelists.size: " + generalPricelists.size());
            //LOG.log(Level.INFO, "generalPricelists.size: " + generalPricelists.size());
            for( Long generalPricelistId : generalPricelists ) {
                LOG.log(Level.FINEST, "generalPricelistId: " + generalPricelistId);
                //LOG.log(Level.INFO, "generalPricelistId: " + generalPricelistId);
                GeneralPricelist generalPricelist = em.find(GeneralPricelist.class, generalPricelistId);
                GeneralPricelistPricelist currentPricelist = getCurrentGeneralPricelistPricelist(generalPricelist.getUniqueId());
                if( currentPricelist != null ) {
                    writeGeneralPricelist(xmlWriter, generalPricelist, currentPricelist, xmlIdGenerator);
                }
            }
        }
        else {
            LOG.log(Level.FINEST, "Found NO generalPricelists");
            //LOG.log(Level.INFO, "Found NO generalPricelists");
        }
        xmlWriter.writeEndElement(); // ListAgreement
        xmlWriter.writeCharacters(System.lineSeparator());

        // write statuses
        writeAgreementEntryStatuses(xmlWriter, xmlIdGenerator);

        // write classifications (extended categories)
        writeClassifications(xmlWriter, xmlIdGenerator, allCategories);

        // write order units
        writeOrderUnits(xmlWriter, xmlIdGenerator);

        // write organizations
        writeOrganizations(xmlWriter, xmlIdGenerator);

        // write directives
        writeDirectives(xmlWriter, xmlIdGenerator);

        // write products (articles) and template products (products)
        xmlWriter.writeStartElement("ListProduct");
        xmlWriter.writeCharacters(System.lineSeparator());

        writeProducts(xmlWriter, xmlIdGenerator);
        writeTemplateProducts(xmlWriter, xmlIdGenerator);
        xmlWriter.writeEndElement(); // ListProduct
        xmlWriter.writeCharacters(System.lineSeparator());

        xmlWriter.writeEndElement(); // Export
        xmlWriter.writeCharacters(System.lineSeparator());

        xmlWriter.flush();
        xmlWriter.close();
        long total = System.currentTimeMillis() - start;
        LOG.logPerformance( this.getClass().getName(), "fillXml", total, sessionId);
        LOG.log(Level.FINEST, "fillXml for " + exportSettings.getFilename() + " done, took " + total + "ms.");
        //LOG.log(Level.INFO, "fillXml for " + exportSettings.getFilename() + " done, took " + total + "ms.");
    }

    private void fillXMLGP( Writer writer, ExportSettings exportSettings, String sessionId ) throws XMLStreamException {
        LOG.log(Level.FINEST, "fillXMLGP( customerUniqueId: {0} )", new Object[] {exportSettings.getOrganization().getUniqueId()} );
        long start = System.currentTimeMillis();
        XMLOutputFactory factory = XMLOutputFactory.newInstance();
        XMLStreamWriter xmlWriter = factory.createXMLStreamWriter(writer);
        xmlWriter.writeStartDocument("UTF-8", "1.0");
        xmlWriter.writeCharacters(System.lineSeparator());
        xmlWriter.writeStartElement("Export");
        xmlWriter.writeCharacters(System.lineSeparator());

        // write basic information
        // writeContainsChangesSince(xmlWriter); only if this is not a complete export (later)
        writeExportNumber(xmlWriter, exportSettings.getNumberOfExports());
        writeTimeCreated(xmlWriter);

        XmlIdGenerator xmlIdGenerator = new XmlIdGenerator();

        List<Category> allCategories = generateClassifications(xmlIdGenerator);

        writeRemovedLists(xmlWriter);

        // write guarantee units
        writeGuaranteeUnits(xmlWriter, xmlIdGenerator);

        // write preventive maintenances
        writePreventiveMaintenances(xmlWriter, xmlIdGenerator);

        // write document types
        writeDocumentTypes(xmlWriter, xmlIdGenerator);


        // write agreements
        xmlWriter.writeStartElement("ListAgreement");
        xmlWriter.writeCharacters(System.lineSeparator());
                /*
        List<Long> agreementIds;
        List<Agreement.Status> statuses = new ArrayList<>();
        statuses.add(Agreement.Status.CURRENT);
        statuses.add(Agreement.Status.FUTURE);
        if( exportSettings.getBusinessLevel() == null ) {
            agreementIds = getAgreementsForCustomerAndStatus(exportSettings.getOrganization().getUniqueId(), statuses);
        } else {
            agreementIds = getAgreementsForCustomerAndBusinessLevelAndStatus(exportSettings.getOrganization().getUniqueId(), exportSettings.getBusinessLevel().getUniqueId(), statuses);
        }

            if( agreementIds != null && !agreementIds.isEmpty() ) {
            LOG.log( Level.FINEST, "Found {0} agreements", new Object[] {agreementIds.size()});
            //LOG.log( Level.INFO, "Found {0} agreements", new Object[] {agreementIds.size()});
            // write agreements
            for( Long agreementId : agreementIds ) {
                //if (agreementId == 5698920) {
                LOG.log(Level.FINEST, "agreementId: {0}", new Object[]{agreementId});
                //LOG.log(Level.INFO, "agreementId: {0}", new Object[]{agreementId});
                Agreement agreement = em.find(Agreement.class, agreementId);
                List<AgreementPricelist> currentAndFuturePricelists = new ArrayList<>();
                AgreementPricelist currentPricelist = getCurrentPricelist(agreement.getUniqueId());
                if (currentPricelist != null) {
                    currentAndFuturePricelists.add(currentPricelist);
                }
                List<AgreementPricelist> futurePricelists = getFuturePricelists(agreement.getUniqueId());
                if (futurePricelists != null && !futurePricelists.isEmpty()) {
                    currentAndFuturePricelists.addAll(futurePricelists);
                }
                LOG.log(Level.FINEST, "Found {0} currentAndFuturePricelists", new Object[]{currentAndFuturePricelists.size()});
                //LOG.log(Level.INFO, "Found {0} currentAndFuturePricelists", new Object[]{currentAndFuturePricelists.size()});
                if (!currentAndFuturePricelists.isEmpty()) {
                    boolean isSharedAgreement = writeAgreement(xmlWriter, agreement, currentAndFuturePricelists, xmlIdGenerator, exportSettings);

                    // appears not to be necessary
                    //if( isSharedAgreement ) {
                    //    writeSharedAgreement(xmlWriter, agreement, xmlIdGenerator, exportSettings);
                    //}
                    //if( isSharedAgreement ) {
                    //    LOG.log(Level.FINEST, "This one is shared {0}", new Object[]{agreementId});
                    //}
                    //else {
                    //    LOG.log(Level.FINEST, "This one is NOT shared {0}", new Object[]{agreementId});
                    //}
                }
                //}
            }
        }
        else {
            LOG.log(Level.FINEST, "Found NO agreements");
            //LOG.log(Level.INFO, "Found NO agreements");
        }

         */

        List<Long> generalPricelists = getGeneralPricelistsByExportSettings(exportSettings.getUniqueId());
        //int i = 0;
        if( generalPricelists != null && !generalPricelists.isEmpty() ) {
            //if (i <= 10 ) { // for debugging
            LOG.log(Level.FINEST, "generalPricelists.size: " + generalPricelists.size());
            //LOG.log(Level.INFO, "generalPricelists.size: " + generalPricelists.size());
            for (Long generalPricelistId : generalPricelists) {
                LOG.log(Level.FINEST, "generalPricelistId: " + generalPricelistId);
                //LOG.log(Level.INFO, "generalPricelistId: " + generalPricelistId);
                GeneralPricelist generalPricelist = em.find(GeneralPricelist.class, generalPricelistId);
                GeneralPricelistPricelist currentPricelist = getCurrentGeneralPricelistPricelist(generalPricelist.getUniqueId());
                if (currentPricelist != null) {
                    writeGeneralPricelist(xmlWriter, generalPricelist, currentPricelist, xmlIdGenerator);
                }
                //i++;
            }
            //}
        }
        else {
            LOG.log(Level.FINEST, "Found NO generalPricelists");
            //LOG.log(Level.INFO, "Found NO generalPricelists");
        }
        xmlWriter.writeEndElement(); // ListAgreement
        xmlWriter.writeCharacters(System.lineSeparator());

        // write statuses
        writeAgreementEntryStatuses(xmlWriter, xmlIdGenerator);

        // write classifications (extended categories)
        writeClassifications(xmlWriter, xmlIdGenerator, allCategories);

        // write order units
        writeOrderUnits(xmlWriter, xmlIdGenerator);

        // write organizations
        writeOrganizations(xmlWriter, xmlIdGenerator);

        // write directives
        writeDirectives(xmlWriter, xmlIdGenerator);

        // write products (articles) and template products (products)
        xmlWriter.writeStartElement("ListProduct");
        xmlWriter.writeCharacters(System.lineSeparator());

        writeProducts(xmlWriter, xmlIdGenerator);
        writeTemplateProducts(xmlWriter, xmlIdGenerator);
        xmlWriter.writeEndElement(); // ListProduct
        xmlWriter.writeCharacters(System.lineSeparator());

        xmlWriter.writeEndElement(); // Export
        xmlWriter.writeCharacters(System.lineSeparator());

        xmlWriter.flush();
        xmlWriter.close();
        long total = System.currentTimeMillis() - start;
        LOG.logPerformance( this.getClass().getName(), "fillXmlGP", total, sessionId);
        LOG.log(Level.FINEST, "fillXmlGP for " + exportSettings.getFilename() + " done, took " + total + "ms.");
        //LOG.log(Level.INFO, "fillXml for " + exportSettings.getFilename() + " done, took " + total + "ms.");
    }

    private void fillXMLAvtal( Writer writer, ExportSettings exportSettings, String sessionId ) throws XMLStreamException {
        LOG.log(Level.FINEST, "fillXML( customerUniqueId: {0} )", new Object[] {exportSettings.getOrganization().getUniqueId()} );
        long start = System.currentTimeMillis();
        XMLOutputFactory factory = XMLOutputFactory.newInstance();
        XMLStreamWriter xmlWriter = factory.createXMLStreamWriter(writer);
        xmlWriter.writeStartDocument("UTF-8", "1.0");
        xmlWriter.writeCharacters(System.lineSeparator());
        xmlWriter.writeStartElement("Export");
        xmlWriter.writeCharacters(System.lineSeparator());

        // write basic information
        // writeContainsChangesSince(xmlWriter); only if this is not a complete export (later)
        writeExportNumber(xmlWriter, exportSettings.getNumberOfExports());
        writeTimeCreated(xmlWriter);

        XmlIdGenerator xmlIdGenerator = new XmlIdGenerator();

        List<Category> allCategories = generateClassifications(xmlIdGenerator);

        writeRemovedLists(xmlWriter);

        // write guarantee units
        writeGuaranteeUnits(xmlWriter, xmlIdGenerator);

        // write preventive maintenances
        writePreventiveMaintenances(xmlWriter, xmlIdGenerator);

        // write document types
        writeDocumentTypes(xmlWriter, xmlIdGenerator);

        // write agreements
        xmlWriter.writeStartElement("ListAgreement");
        xmlWriter.writeCharacters(System.lineSeparator());
        List<Long> agreementIds;
        List<Agreement.Status> statuses = new ArrayList<>();
        statuses.add(Agreement.Status.CURRENT);
        statuses.add(Agreement.Status.FUTURE);
        if( exportSettings.getBusinessLevel() == null ) {
            agreementIds = getAgreementsForCustomerAndStatus(exportSettings.getOrganization().getUniqueId(), statuses);
        } else {
            agreementIds = getAgreementsForCustomerAndBusinessLevelAndStatus(exportSettings.getOrganization().getUniqueId(), exportSettings.getBusinessLevel().getUniqueId(), statuses);
        }
        if( agreementIds != null && !agreementIds.isEmpty() ) {
            LOG.log( Level.FINEST, "Found {0} agreements", new Object[] {agreementIds.size()});
            //LOG.log( Level.INFO, "Found {0} agreements", new Object[] {agreementIds.size()});
            // write agreements
            //int i = 0;
            for( Long agreementId : agreementIds ) {
                //if (i <= 10 ) { // for debugging
                LOG.log(Level.FINEST, "agreementId: {0}", new Object[]{agreementId});
                //LOG.log(Level.INFO, "agreementId: {0}", new Object[]{agreementId});
                Agreement agreement = em.find(Agreement.class, agreementId);
                List<AgreementPricelist> currentAndFuturePricelists = new ArrayList<>();
                AgreementPricelist currentPricelist = getCurrentPricelist(agreement.getUniqueId());
                if (currentPricelist != null) {
                    currentAndFuturePricelists.add(currentPricelist);
                }
                List<AgreementPricelist> futurePricelists = getFuturePricelists(agreement.getUniqueId());
                if (futurePricelists != null && !futurePricelists.isEmpty()) {
                    currentAndFuturePricelists.addAll(futurePricelists);
                }
                LOG.log(Level.FINEST, "Found {0} currentAndFuturePricelists", new Object[]{currentAndFuturePricelists.size()});
                //LOG.log(Level.INFO, "Found {0} currentAndFuturePricelists", new Object[]{currentAndFuturePricelists.size()});
                if (!currentAndFuturePricelists.isEmpty()) {
                    boolean isSharedAgreement = writeAgreement(xmlWriter, agreement, currentAndFuturePricelists, xmlIdGenerator, exportSettings);

                    // appears not to be necessary
                    //if( isSharedAgreement ) {
                    //    writeSharedAgreement(xmlWriter, agreement, xmlIdGenerator, exportSettings);
                    //}
                    //if( isSharedAgreement ) {
                    //    LOG.log(Level.FINEST, "This one is shared {0}", new Object[]{agreementId});
                    //}
                    //else {
                    //    LOG.log(Level.FINEST, "This one is NOT shared {0}", new Object[]{agreementId});
                    //}
                //}
                //i++;
                }
            }
            LOG.log( Level.FINEST, " agreements done");
        }
        else {
            LOG.log(Level.FINEST, "Found NO agreements");
            //LOG.log(Level.INFO, "Found NO agreements");
        }

        /*
        List<Long> generalPricelists = getGeneralPricelistsByExportSettings(exportSettings.getUniqueId());
        //if( 1 == 0 ) {
        if( generalPricelists != null && !generalPricelists.isEmpty() ) {

            LOG.log(Level.FINEST, "generalPricelists.size: " + generalPricelists.size());
            //LOG.log(Level.INFO, "generalPricelists.size: " + generalPricelists.size());
            for( Long generalPricelistId : generalPricelists ) {
                LOG.log(Level.FINEST, "generalPricelistId: " + generalPricelistId);
                //LOG.log(Level.INFO, "generalPricelistId: " + generalPricelistId);
                GeneralPricelist generalPricelist = em.find(GeneralPricelist.class, generalPricelistId);
                GeneralPricelistPricelist currentPricelist = getCurrentGeneralPricelistPricelist(generalPricelist.getUniqueId());
                if( currentPricelist != null ) {
                    writeGeneralPricelist(xmlWriter, generalPricelist, currentPricelist, xmlIdGenerator);
                }
            }
        }
        else {
            LOG.log(Level.FINEST, "Found NO generalPricelists");
            //LOG.log(Level.INFO, "Found NO generalPricelists");
        }

         */
        xmlWriter.writeEndElement(); // ListAgreement
        xmlWriter.writeCharacters(System.lineSeparator());

        // write statuses
        writeAgreementEntryStatuses(xmlWriter, xmlIdGenerator);

        // write classifications (extended categories)
        writeClassifications(xmlWriter, xmlIdGenerator, allCategories);

        // write order units
        writeOrderUnits(xmlWriter, xmlIdGenerator);

        // write organizations
        writeOrganizations(xmlWriter, xmlIdGenerator);

        // write directives
        writeDirectives(xmlWriter, xmlIdGenerator);

        // write products (articles) and template products (products)
        xmlWriter.writeStartElement("ListProduct");
        xmlWriter.writeCharacters(System.lineSeparator());
        LOG.log( Level.FINEST, " writeProducts");
        writeProducts(xmlWriter, xmlIdGenerator);
        LOG.log( Level.FINEST, " writeTemplateProducts done");
        LOG.log( Level.FINEST, " writeTemplateProducts");
        writeTemplateProducts(xmlWriter, xmlIdGenerator);
        LOG.log( Level.FINEST, " writeTemplateProducts done");
        xmlWriter.writeEndElement(); // ListProduct
        xmlWriter.writeCharacters(System.lineSeparator());

        xmlWriter.writeEndElement(); // Export
        xmlWriter.writeCharacters(System.lineSeparator());

        xmlWriter.flush();
        xmlWriter.close();
        long total = System.currentTimeMillis() - start;
        LOG.logPerformance( this.getClass().getName(), "fillXmlAvtal", total, sessionId);
        LOG.log(Level.FINEST, "fillXmlAvtal for " + exportSettings.getFilename() + " done, took " + total + "ms.");
        //LOG.log(Level.INFO, "fillXml for " + exportSettings.getFilename() + " done, took " + total + "ms.");
    }

    private void writeDocumentTypes( XMLStreamWriter writer, XmlIdGenerator xmlIdGenerator ) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeDocumentTypes(...)" );
        writer.writeStartElement("List9aa56166cb7942018e809ea09653ee1e");
        writer.writeCharacters(System.lineSeparator());
        List<CVDocumentType> cVDocumentTypes = em.createNamedQuery(CVDocumentType.FIND_ALL).getResultList();
        if( cVDocumentTypes != null && !cVDocumentTypes.isEmpty() ) {
            for( CVDocumentType cVDocumentType : cVDocumentTypes ) {
                writer.writeStartElement("_x0039_aa56166cb7942018e809ea09653ee1e");
                long nextId = xmlIdGenerator.generateDocumentTypesXmlId(cVDocumentType.getUniqueId());
                writer.writeAttribute("id", String.valueOf(nextId));
                writer.writeCharacters(System.lineSeparator());
                writer.writeStartElement("Code");
                writer.writeCharacters(cVDocumentType.getCode());
                writer.writeEndElement(); // Code
                writer.writeCharacters(System.lineSeparator());
                writer.writeStartElement("Value");
                writer.writeCharacters(cVDocumentType.getValue());
                writer.writeEndElement(); // Value
                writer.writeCharacters(System.lineSeparator());
                writer.writeEndElement(); // _x0039_aa56166cb7942018e809ea09653ee1e
                writer.writeCharacters(System.lineSeparator());
            }
        }
        writer.writeEndElement(); // List9aa56166cb7942018e809ea09653ee1e
        writer.writeCharacters(System.lineSeparator());
    }

    private void writeRemovedLists( XMLStreamWriter writer ) throws XMLStreamException {
        // the element RemovedLists must exist
        writer.writeStartElement("RemovedLists");
        writer.writeEndElement();
        writer.writeCharacters(System.lineSeparator());
    }

    private void writePreventiveMaintenances( XMLStreamWriter writer, XmlIdGenerator xmlIdGenerator ) throws XMLStreamException {
        LOG.log(Level.FINEST, "writePreventiveMaintenances(...)" );
        writer.writeStartElement("List281c1f9339bb46a1bc8053169e8fdcef");
        writer.writeCharacters(System.lineSeparator());
        List<CVPreventiveMaintenance> cVPreventiveMaintenances = em.createNamedQuery(CVPreventiveMaintenance.FIND_ALL).getResultList();
        if( cVPreventiveMaintenances != null && !cVPreventiveMaintenances.isEmpty() ) {
            for( CVPreventiveMaintenance cVPreventiveMaintenance : cVPreventiveMaintenances ) {
                writer.writeStartElement("_x0032_81c1f9339bb46a1bc8053169e8fdcef");
                long nextId = xmlIdGenerator.generatePreventiveMaintenanceXmlId(cVPreventiveMaintenance.getUniqueId());
                writer.writeAttribute("id", String.valueOf(nextId));
                writer.writeCharacters(System.lineSeparator());
                writer.writeStartElement("Value");
                writer.writeCharacters(cVPreventiveMaintenance.getCode());
                writer.writeEndElement(); // Value
                writer.writeCharacters(System.lineSeparator());
                writer.writeEndElement(); // _x0032_81c1f9339bb46a1bc8053169e8fdcef
                writer.writeCharacters(System.lineSeparator());
            }
        }
        writer.writeEndElement(); // List281c1f9339bb46a1bc8053169e8fdcef
        writer.writeCharacters(System.lineSeparator());
    }

    private void writeGuaranteeUnits( XMLStreamWriter writer, XmlIdGenerator xmlIdGenerator ) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeGuaranteeUnits(...)" );
        writer.writeStartElement("List08d16de85cc646a99fe4157a15a9c80b");
        writer.writeCharacters(System.lineSeparator());
        List<CVGuaranteeUnit> units = em.createNamedQuery(CVGuaranteeUnit.FIND_ALL).getResultList();
        if( units != null && !units.isEmpty() ) {
            for( CVGuaranteeUnit unit : units ) {
                writer.writeStartElement("_x0030_8d16de85cc646a99fe4157a15a9c80b");
                long nextId = xmlIdGenerator.generateGuaranteeUnitsXmlId(unit.getUniqueId());
                writer.writeAttribute("id", String.valueOf(nextId));
                writer.writeCharacters(System.lineSeparator());
                writer.writeStartElement("Code");
                writer.writeCharacters(unit.getCode());
                writer.writeEndElement(); // Code
                writer.writeCharacters(System.lineSeparator());
                writer.writeStartElement("Value");
                writer.writeCharacters(unit.getName());
                writer.writeEndElement(); // Value
                writer.writeCharacters(System.lineSeparator());
                writer.writeEndElement(); // _x0030_8d16de85cc646a99fe4157a15a9c80b
                writer.writeCharacters(System.lineSeparator());
            }
        }
        writer.writeEndElement(); // List08d16de85cc646a99fe4157a15a9c80b
        writer.writeCharacters(System.lineSeparator());
    }

    private void writeOrderUnits( XMLStreamWriter writer, XmlIdGenerator xmlIdGenerator ) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeOrderUnits(...)" );
        List<CVOrderUnit> units = em.createNamedQuery(CVOrderUnit.FIND_ALL).getResultList();
        if( units != null && !units.isEmpty() ) {
            writer.writeStartElement("ListEnhet");
            writer.writeCharacters(System.lineSeparator());
            for( CVOrderUnit unit : units ) {
                writer.writeStartElement("Enhet");
                long nextId = xmlIdGenerator.generateOrderUnitsXmlId(unit.getUniqueId());
                writer.writeAttribute("id", String.valueOf(nextId));
                writer.writeCharacters(System.lineSeparator());
                writer.writeStartElement("Code");
                writer.writeCharacters(unit.getCode());
                writer.writeEndElement(); // Code
                writer.writeCharacters(System.lineSeparator());
                writer.writeStartElement("Value");
                writer.writeCharacters(unit.getName());
                writer.writeEndElement(); // Value
                writer.writeCharacters(System.lineSeparator());
                writer.writeEndElement(); // Enhet
                writer.writeCharacters(System.lineSeparator());
            }
            writer.writeEndElement(); // ListEnhet
            writer.writeCharacters(System.lineSeparator());
        }
    }

    private void writeDirectives( XMLStreamWriter writer, XmlIdGenerator xmlIdGenerator ) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeDirectives(...)");
        List<CVCEDirective> directives = em.createNamedQuery(CVCEDirective.FIND_ALL).getResultList();
        if( directives != null && !directives.isEmpty() ) {
            writer.writeStartElement("ListProdMarkDirective");
            writer.writeCharacters(System.lineSeparator());
            for( CVCEDirective directive : directives ) {
                writer.writeStartElement("ProdMarkDirective");
                long nextId = xmlIdGenerator.generateDirectivesXmlId(directive.getUniqueId());
                writer.writeAttribute("id", String.valueOf(nextId));
                writer.writeCharacters(System.lineSeparator());
                writer.writeStartElement("Code");
                writer.writeCharacters(directive.getName());
                writer.writeEndElement(); // Code
                writer.writeCharacters(System.lineSeparator());
                writer.writeEndElement(); // ProdMarkDirective
                writer.writeCharacters(System.lineSeparator());
            }
            writer.writeEndElement(); // ListProdMarkDirective
            writer.writeCharacters(System.lineSeparator());
        }
    }

    private List<Category> generateClassifications(XmlIdGenerator xmlIdGenerator) {
        List<Category> allCategories = em.createQuery("SELECT c FROM Category c WHERE LENGTH(c.code) = 6 OR c.code IS NULL").
                getResultList();
        if( allCategories != null && !allCategories.isEmpty() ) {
            for( Category category : allCategories ) {
                if( category.getCode() != null && category.getCode().length() > 6 ) {
                    // we do not include categories longer than six digits
                    continue;
                }
                xmlIdGenerator.generateClassificationsXmlId(category.getUniqueId());
            }
        }
        return allCategories;
    }

    private void writeClassifications( XMLStreamWriter writer, XmlIdGenerator xmlIdGenerator, List<Category> allCategories ) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeClassifications(...)");
        if( allCategories != null && !allCategories.isEmpty() ) {
            writer.writeStartElement("ListClassification");
            writer.writeCharacters(System.lineSeparator());
            for( Category category : allCategories ) {
                writer.writeStartElement("Classification");
                writer.writeAttribute("id", xmlIdGenerator.getClassificationsXmlId(category.getUniqueId()).toString());
                writer.writeCharacters(System.lineSeparator());
                writer.writeStartElement("Code");
                writer.writeCharacters(category.getCode() == null ? "": category.getCode());
                writer.writeEndElement(); // Code
                writer.writeCharacters(System.lineSeparator());
                writer.writeStartElement("Name");
                writer.writeCharacters(category.getName());
                writer.writeEndElement(); // Name
                writer.writeCharacters(System.lineSeparator());
                writer.writeEndElement(); // Classification
                writer.writeCharacters(System.lineSeparator());
            }
            writer.writeEndElement(); // ListClassification
            writer.writeCharacters(System.lineSeparator());
        }
    }

    private void writeAgreementEntryStatuses( XMLStreamWriter writer, XmlIdGenerator xmlIdGenerator ) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeStatuses(...)");
        Map<String, Long> statuses = xmlIdGenerator.getPricelistRowStatusMap();
        if( statuses != null && !statuses.isEmpty() ) {
            writer.writeStartElement("ListAgreementEntryStatus");
            writer.writeCharacters(System.lineSeparator());
            for( String status : statuses.keySet() ) {
                writer.writeStartElement("AgreementEntryStatus");
                writer.writeAttribute("id", xmlIdGenerator.getPricelistRowStatusXmlId(status).toString());
                writer.writeCharacters(System.lineSeparator());
                if( AgreementPricelistRow.Status.ACTIVE.toString().equals(status) ||
                        AgreementPricelistRow.Status.PENDING_INACTIVATION.toString().equals(status)) {
                    writer.writeStartElement("Code");
                    writer.writeCharacters("aktiv");
                    writer.writeEndElement(); // Code
                    writer.writeCharacters(System.lineSeparator());
                    writer.writeStartElement("Value");
                    writer.writeCharacters("Aktiv");
                    writer.writeEndElement(); // Value
                    writer.writeCharacters(System.lineSeparator());
                } else if( AgreementPricelistRow.Status.INACTIVE.toString().equals(status)) {
                    writer.writeStartElement("Code");
                    writer.writeCharacters("ejaktiv");
                    writer.writeEndElement(); // Code
                    writer.writeCharacters(System.lineSeparator());
                    writer.writeStartElement("Value");
                    writer.writeCharacters("Ej aktiv");
                    writer.writeEndElement(); // Value
                    writer.writeCharacters(System.lineSeparator());
                } else {
                    LOG.log(Level.WARNING, "Found status agreement pricelist row status: {0} which should not exist here!", new Object[] {status});
                }
                writer.writeEndElement(); // AgreementEntryStatus
                writer.writeCharacters(System.lineSeparator());
            }
            writer.writeEndElement(); // ListAgreementEntryStatus
            writer.writeCharacters(System.lineSeparator());
        }
    }

    private void writeGeneralPricelist( XMLStreamWriter writer,
            GeneralPricelist generalPricelist,
            GeneralPricelistPricelist currentPricelist,
            XmlIdGenerator xmlIdGenerator ) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeGeneralPricelist( generalPricelist->uniqueId: {0}, currentPricelist->uniqueId: {1} )", new Object[] {generalPricelist.getUniqueId(), currentPricelist.getUniqueId()} );
        writer.writeStartElement("Agreement");
        long agreementXmlId = xmlIdGenerator.generateAgreementsXmlId(generalPricelist.getUniqueId());
        writer.writeAttribute("id", String.valueOf(agreementXmlId));
        writer.writeCharacters(System.lineSeparator());

        // valid from and to
        writeValidFromAndTo(writer, generalPricelist.getValidFrom(), generalPricelist.getValidTo());

        // write entries (rows)
        writeGeneralPricelistEntries( writer, generalPricelist, currentPricelist, xmlIdGenerator);

        // write head
        writeGeneralPricelistHead(writer, generalPricelist);

        // lead time days
        Integer deliveryTime = getDeliveryTimeByGuiOrder(generalPricelist.getDeliveryTimeH(),
                generalPricelist.getDeliveryTimeT(),
                generalPricelist.getDeliveryTimeR(),
                generalPricelist.getDeliveryTimeI(),
                generalPricelist.getDeliveryTimeTJ());
        if( deliveryTime != null ) {
            writer.writeStartElement("LeadTimeDays");
            writer.writeCharacters(deliveryTime.toString());
            writer.writeEndElement(); // LeadTimeDays
            writer.writeCharacters(System.lineSeparator());
        }

        // seller (supplier)
        Long sellerXmlId = xmlIdGenerator.getOrganizationsXmlId(generalPricelist.getOwnerOrganization().getUniqueId());
        if( sellerXmlId == null ) {
            sellerXmlId = xmlIdGenerator.generateOrganizationsXmlId(generalPricelist.getOwnerOrganization().getUniqueId());
        }
        writer.writeStartElement("Seller");
        writer.writeCharacters(System.lineSeparator());
        writer.writeStartElement("Organization");
        writer.writeAttribute("idref", sellerXmlId.toString());
        writer.writeEndElement(); // Organization
        writer.writeCharacters(System.lineSeparator());
        writer.writeEndElement(); // Seller
        writer.writeCharacters(System.lineSeparator());

        // last updated / created
        writer.writeStartElement("UpdatedTime");
        Date lastUpdated = generalPricelist.getLastUpdated() == null ? generalPricelist.getCreated(): generalPricelist.getLastUpdated();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.
                ofPattern(EXPORT_DATE_TIME_PATTERN).
                withZone(ZoneId.systemDefault());
        Instant instant = Instant.ofEpochMilli(lastUpdated.getTime());
        writer.writeCharacters(dateTimeFormatter.format(instant));
        writer.writeEndElement(); // UpdatedTime
        writer.writeCharacters(System.lineSeparator());

        // guarantee
        Integer warrantyQuantity = getWarrantyQuantityByGuiOrder(generalPricelist.getWarrantyQuantityH(),
                generalPricelist.getWarrantyQuantityT(),
                generalPricelist.getWarrantyQuantityR(),
                generalPricelist.getWarrantyQuantityI(),
                generalPricelist.getWarrantyQuantityTJ());
        if( warrantyQuantity != null ) {
            writer.writeStartElement("be6470986d7744568de81ffdc8dd9edd");
            writer.writeCharacters(System.lineSeparator());
            writer.writeStartElement("_x0039_a6935ebf5eb4955b6c9a072dd948036");
            writer.writeCharacters(warrantyQuantity.toString());
            writer.writeEndElement(); // _x0039_a6935ebf5eb4955b6c9a072dd948036
            writer.writeCharacters(System.lineSeparator());
            writer.writeEndElement(); // be6470986d7744568de81ffdc8dd9edd
            writer.writeCharacters(System.lineSeparator());
        }

        writer.writeEndElement(); // Agreement
        writer.writeCharacters(System.lineSeparator());
    }

    private boolean writeAgreement( XMLStreamWriter writer,
            Agreement agreement,
            List<AgreementPricelist> currentAndFuturePricelists,
            XmlIdGenerator xmlIdGenerator,
            ExportSettings exportSettings) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeAgreement( agreement->uniqueId: {0}, number of pricelists: {1} )", new Object[] {agreement.getUniqueId(), currentAndFuturePricelists.size()} );
        //LOG.log(Level.INFO, "writeAgreement( agreement->uniqueId: {0}, number of pricelists: {1} )", new Object[] {agreement.getUniqueId(), currentAndFuturePricelists.size()} );
        boolean sharedAgreement = false;
        if( !agreement.getCustomer().getUniqueId().equals(exportSettings.getOrganization().getUniqueId()) ) {
            // the customer of the agreement is not the same as the current organization
            // being processed, the agreement is shared
            sharedAgreement = true;
        }

        writer.writeStartElement("Agreement");
        long agreementXmlId = xmlIdGenerator.generateAgreementsXmlId(agreement.getUniqueId());
        writer.writeAttribute("id", String.valueOf(agreementXmlId));
        writer.writeCharacters(System.lineSeparator());

        // valid from and to
        writeValidFromAndTo(writer, agreement.getValidFrom(), agreement.getValidTo());

        // buyer (customer)
        Long buyerXmlId = xmlIdGenerator.getOrganizationsXmlId(agreement.getCustomer().getUniqueId());
        if( buyerXmlId == null ) {
            buyerXmlId = xmlIdGenerator.generateOrganizationsXmlId(agreement.getCustomer().getUniqueId());
        }
        writer.writeStartElement("Buyer");
        writer.writeCharacters(System.lineSeparator());
        writer.writeStartElement("Organization");
        writer.writeAttribute("idref", buyerXmlId.toString());
        writer.writeEndElement(); // Organization
        writer.writeCharacters(System.lineSeparator());
        writer.writeEndElement(); // Buyer
        writer.writeCharacters(System.lineSeparator());

        // write entries (rows)
        for( AgreementPricelist agreementPricelist : currentAndFuturePricelists ) {
            writeAgreementEntries( writer, agreement, agreementPricelist, xmlIdGenerator);
        }

        // write head
        writeAgreementHead( writer, agreement );

        // lead time days
        Integer deliveryTime = getDeliveryTimeByGuiOrder(agreement.getDeliveryTimeH(),
                agreement.getDeliveryTimeT(),
                agreement.getDeliveryTimeR(),
                agreement.getDeliveryTimeI(),
                agreement.getDeliveryTimeTJ());
        if( deliveryTime != null ) {
            writer.writeStartElement("LeadTimeDays");
            writer.writeCharacters(deliveryTime.toString());
            writer.writeEndElement(); // LeadTimeDays
            writer.writeCharacters(System.lineSeparator());
        }

        // seller (supplier)
        Long sellerXmlId = xmlIdGenerator.getOrganizationsXmlId(agreement.getSupplier().getUniqueId());
        if( sellerXmlId == null ) {
            sellerXmlId = xmlIdGenerator.generateOrganizationsXmlId(agreement.getSupplier().getUniqueId());
        }
        writer.writeStartElement("Seller");
        writer.writeCharacters(System.lineSeparator());
        writer.writeStartElement("Organization");
        writer.writeAttribute("idref", sellerXmlId.toString());
        writer.writeEndElement(); // Organization
        writer.writeCharacters(System.lineSeparator());
        writer.writeEndElement(); // Seller
        writer.writeCharacters(System.lineSeparator());

        // last updated / created
        writer.writeStartElement("UpdatedTime");
        Date lastUpdated = agreement.getLastUpdated() == null ? agreement.getCreated(): agreement.getLastUpdated();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.
                ofPattern(EXPORT_DATE_TIME_PATTERN).
                withZone(ZoneId.systemDefault());
        Instant instant = Instant.ofEpochMilli(lastUpdated.getTime());
        writer.writeCharacters(dateTimeFormatter.format(instant));
        writer.writeEndElement(); // UpdatedTime
        writer.writeCharacters(System.lineSeparator());

        // guarantee
        Integer warrantyQuantity = getWarrantyQuantityByGuiOrder(agreement.getWarrantyQuantityH(),
                agreement.getWarrantyQuantityT(),
                agreement.getWarrantyQuantityR(),
                agreement.getWarrantyQuantityI(),
                agreement.getWarrantyQuantityTJ());
        if( warrantyQuantity != null ) {
            writer.writeStartElement("be6470986d7744568de81ffdc8dd9edd");
            writer.writeCharacters(System.lineSeparator());

            writer.writeStartElement("_x0039_a6935ebf5eb4955b6c9a072dd948036");
            writer.writeCharacters(warrantyQuantity.toString());
            writer.writeEndElement(); // _x0039_a6935ebf5eb4955b6c9a072dd948036
            writer.writeCharacters(System.lineSeparator());
            writer.writeEndElement(); // be6470986d7744568de81ffdc8dd9edd
            writer.writeCharacters(System.lineSeparator());
        }

        writer.writeEndElement(); // Agreement
        writer.writeCharacters(System.lineSeparator());

        return sharedAgreement;
    }

    private void writeSharedAgreement( XMLStreamWriter writer,
            Agreement agreement,
            XmlIdGenerator xmlIdGenerator,
            ExportSettings exportSettings) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeSharedAgreement( agreement->uniqueId: {0} )", new Object[] {agreement.getUniqueId()} );

        writer.writeStartElement("Agreement");
        writer.writeCharacters(System.lineSeparator());
        Long sharedAgreementXmlId = xmlIdGenerator.getAgreementsXmlId(agreement.getUniqueId());

        // buyer (customer), in a shared agreement, it's always the organization
        // we're currently generating XML for
        Long buyerXmlId = xmlIdGenerator.getOrganizationsXmlId(exportSettings.getOrganization().getUniqueId());
        if( buyerXmlId == null ) {
            buyerXmlId = xmlIdGenerator.generateOrganizationsXmlId(exportSettings.getOrganization().getUniqueId());
        }
        writer.writeStartElement("Buyer");
        writer.writeCharacters(System.lineSeparator());
        writer.writeStartElement("Organization");
        writer.writeAttribute("idref", buyerXmlId.toString());
        writer.writeEndElement(); // Organization
        writer.writeCharacters(System.lineSeparator());
        writer.writeEndElement(); // Buyer
        writer.writeCharacters(System.lineSeparator());

        writer.writeStartElement("AgreementCalculation");
        writer.writeStartElement("BaseAgreement");
        writer.writeAttribute("idref", sharedAgreementXmlId.toString());
        writer.writeEndElement(); // BaseAgreement
        writer.writeCharacters(System.lineSeparator());
        writer.writeEndElement(); // AgreementCalculation
        writer.writeCharacters(System.lineSeparator());

        writer.writeEndElement(); // Agreement
        writer.writeCharacters(System.lineSeparator());
    }

    private void writeGeneralPricelistEntries(XMLStreamWriter writer,
            GeneralPricelist generalPricelist,
            GeneralPricelistPricelist currentPricelist,
            XmlIdGenerator xmlIdGenerator) throws XMLStreamException {
            LOG.log(Level.FINEST, "writeGeneralPricelistEntries( currentPricelist->uniqueId: {0} )", new Object[] {currentPricelist.getUniqueId()} );
            List<GeneralPricelistPricelistRow.Status> statuses = new ArrayList<>();
            statuses.add(GeneralPricelistPricelistRow.Status.ACTIVE);
            List<GeneralPricelistPricelistRow> generalPricelistPricelistRows = em.createNamedQuery(GeneralPricelistPricelistRow.FIND_BY_GENERAL_PRICELIST_PRICELIST_AND_STATUS).
                    setParameter("generalPricelistPricelistId", currentPricelist.getUniqueId()).
                    setParameter("statuses", statuses).
                    getResultList();
            if (generalPricelistPricelistRows != null && !generalPricelistPricelistRows.isEmpty()) {
                Date now = new Date();
                int i = 0;
                int j = 0;
                LOG.log(Level.FINEST, " looping through {0} GP-rows)", new Object[]{generalPricelistPricelistRows.size()});

                    for (GeneralPricelistPricelistRow generalPricelistPricelistRow : generalPricelistPricelistRows) {
                        //if (i <= 10) { //for dubugging
                            // rows without price are not interesting
                            if (generalPricelistPricelistRow.getPrice() != null) {
                                Date validFrom = generalPricelistPricelistRow.isValidFromOverridden() ? generalPricelistPricelistRow.getValidFrom() : generalPricelist.getValidFrom();
                                if (now.after(validFrom)) {
                                    writeGeneralPricelistPricelistEntry(writer, generalPricelistPricelistRow, currentPricelist, generalPricelist, validFrom, xmlIdGenerator);
                                }
                            } else {
                                j++;
                            }
                            i++;
                            if ((generalPricelistPricelistRows.size() >= 1 && generalPricelistPricelistRows.size() < 10) ||
                                    (i % 10 == 0 && generalPricelistPricelistRows.size() >= 10 && generalPricelistPricelistRows.size() < 100) ||
                                    (i % 100 == 0 && generalPricelistPricelistRows.size() >= 100 && generalPricelistPricelistRows.size() < 1000) ||
                                    (i % 1000 == 0 && generalPricelistPricelistRows.size() >= 1000)
                            )
                                LOG.log(Level.FINEST, " generalPricelistPricelistRows handled: {0} ({1})", new Object[]{i, generalPricelistPricelistRows.size()});
                        //}
                    }

                LOG.log(Level.FINEST, " generalPricelistPricelistRows handled: {0} ({1})", new Object[]{i, generalPricelistPricelistRows.size()});
                LOG.log(Level.FINEST, " rows without price: {0}", new Object[]{j});
            }
        //}
    }

    private void writeAgreementEntries(XMLStreamWriter writer,
            Agreement agreement,
            AgreementPricelist agreementPricelist,
            XmlIdGenerator xmlIdGenerator) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeAgreementEntries( currentPricelist->uniqueId: {0} )", new Object[] {agreementPricelist.getUniqueId()} );
        List<AgreementPricelistRow.Status> statuses = new ArrayList<>();
        statuses.add(AgreementPricelistRow.Status.ACTIVE);
        statuses.add(AgreementPricelistRow.Status.PENDING_INACTIVATION);
        List<AgreementPricelistRow> agreementPricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_PRICELIST_AND_STATUSES).
                setParameter("pricelistUniqueId", agreementPricelist.getUniqueId()).
                setParameter("statuses", statuses).
                getResultList();
        if (agreementPricelistRows != null && !agreementPricelistRows.isEmpty()) {
            LOG.log(Level.FINEST, " number of agreementpricelistrows to loop through: {0} )", new Object[]{agreementPricelistRows.size()});
            //int i = 0;

            for (AgreementPricelistRow agreementPricelistRow : agreementPricelistRows) {
                //if (i <= 10 ) { // for debugging
                    //HJAL - 1869
                    Date rowValidFrom = agreementPricelistRow.getValidFrom();
                    Date validFrom = rowValidFrom != null ? rowValidFrom : agreementPricelist.getValidFrom();
                    //Pre Hjal 1869
                    //Date validFrom = agreementPricelistRow.isValidFromOverridden() ? agreementPricelistRow.getValidFrom(): agreementPricelist.getValidFrom();
                    writeEntry(writer, agreementPricelistRow, agreementPricelist, agreement, validFrom, xmlIdGenerator);
                    //}
                    //i++;
                //}
            }

        }
    }

    private void writeGeneralPricelistPricelistEntry(XMLStreamWriter writer,
            GeneralPricelistPricelistRow generalPricelistPricelistRow,
            GeneralPricelistPricelist generalPricelistPricelist,
            GeneralPricelist generalPricelist,
            Date validFrom,
            XmlIdGenerator xmlIdGenerator) throws XMLStreamException {
        writer.writeStartElement("Entries");
        writer.writeCharacters(System.lineSeparator());

        // delivery time
        Integer deliveryTime = getDeliveryTime( generalPricelistPricelistRow.getDeliveryTime(),
                generalPricelist.getDeliveryTimeH(),
                generalPricelist.getDeliveryTimeI(),
                generalPricelist.getDeliveryTimeR(),
                generalPricelist.getDeliveryTimeT(),
                generalPricelist.getDeliveryTimeTJ(),
                generalPricelistPricelistRow.isDeliveryTimeOverridden(),
                generalPricelistPricelistRow.getArticle());
        if( deliveryTime != null ) {
            writer.writeStartElement("LeadTimeDays");
            writer.writeCharacters(deliveryTime.toString());
            writer.writeEndElement(); // LeadTimeDays
            writer.writeCharacters(System.lineSeparator());
        }

        writeMaintenance(writer, generalPricelistPricelistRow.getArticle(), xmlIdGenerator);

        // min order quantity
        writer.writeStartElement("MinOrderQty");
        writer.writeCharacters(Integer.toString(generalPricelistPricelistRow.getLeastOrderQuantity()));
        writer.writeEndElement(); // MinOrderQty
        writer.writeCharacters(System.lineSeparator());

        // price
        writer.writeStartElement("Price");
        writer.writeCharacters(generalPricelistPricelistRow.getPrice().toString());
        writer.writeEndElement(); // Price
        writer.writeCharacters(System.lineSeparator());

        // valid from
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.
                ofPattern(EXPORT_DATE_TIME_PATTERN).
                withZone(ZoneId.systemDefault());
        Instant instant = Instant.ofEpochMilli(validFrom.getTime());
        writer.writeStartElement("PriceValidFromTime");
        writer.writeCharacters(dateTimeFormatter.format(instant));
        writer.writeEndElement(); // PriceValidFromTime
        writer.writeCharacters(System.lineSeparator());

        // product (article) reference
        Long articleXmlId = xmlIdGenerator.getArticlesXmlId(generalPricelistPricelistRow.getArticle().getUniqueId());
        if( articleXmlId == null ) {
            articleXmlId = xmlIdGenerator.generateArticlesXmlId(generalPricelistPricelistRow.getArticle().getUniqueId());
        }
        writer.writeStartElement("Product");
        writer.writeAttribute("idref", articleXmlId.toString() );
        writer.writeCharacters(System.lineSeparator());
        writer.writeEndElement(); // Product
        writer.writeCharacters(System.lineSeparator());

        // status, general pricelist rows can only have one of two statuses
        Long statusXmlId = xmlIdGenerator.getPricelistRowStatusXmlId(generalPricelistPricelistRow.getStatus().toString());
        if( statusXmlId == null ) {
            statusXmlId = xmlIdGenerator.generatePricelistRowStatusXmlId(generalPricelistPricelistRow.getStatus().toString());
        }
        writer.writeStartElement("Status");
        writer.writeAttribute("idref", statusXmlId.toString() );
        writer.writeEndElement(); // Status
        writer.writeCharacters(System.lineSeparator());

        // pricelist number
        writer.writeStartElement("SupplierPriceListNo");
        writer.writeCharacters(generalPricelistPricelist.getNumber());
        writer.writeEndElement(); // SupplierPriceListNo
        writer.writeCharacters(System.lineSeparator());

        // warranty, we only show information if warranty "number" is available
        // if we only have unit, we skip this part
        String warrantyQuantity = getWarrantyQuantity(generalPricelistPricelistRow.isWarrantyQuantityOverridden(),
                generalPricelistPricelistRow.getWarrantyQuantity(),
                generalPricelistPricelistRow.getArticle(),
                generalPricelist.getWarrantyQuantityH(),
                generalPricelist.getWarrantyQuantityI(),
                generalPricelist.getWarrantyQuantityR(),
                generalPricelist.getWarrantyQuantityT(),
                generalPricelist.getWarrantyQuantityTJ());
        if( warrantyQuantity != null ) {
            writer.writeStartElement("e1ca9861c0b0442dba7d9d3230f36289");
            writer.writeCharacters(System.lineSeparator());

            writer.writeStartElement("_x0039_a6935ebf5eb4955b6c9a072dd948036");
            writer.writeCharacters(warrantyQuantity);
            writer.writeEndElement(); // _x0039_a6935ebf5eb4955b6c9a072dd948036
            writer.writeCharacters(System.lineSeparator());

            writer.writeEndElement(); // e1ca9861c0b0442dba7d9d3230f36289
            writer.writeCharacters(System.lineSeparator());
        }

        writer.writeEndElement(); // Entries
        writer.writeCharacters(System.lineSeparator());
    }

    private void writeEntry(XMLStreamWriter writer,
            AgreementPricelistRow agreementPricelistRow,
            AgreementPricelist agreementPricelist,
            Agreement agreement,
            Date validFrom,
            XmlIdGenerator xmlIdGenerator) throws XMLStreamException {
        writer.writeStartElement("Entries");
        writer.writeCharacters(System.lineSeparator());

        // delivery time
        Integer deliveryTime = getDeliveryTime( agreementPricelistRow.getDeliveryTime(),
                agreement.getDeliveryTimeH(),
                agreement.getDeliveryTimeI(),
                agreement.getDeliveryTimeR(),
                agreement.getDeliveryTimeT(),
                agreement.getDeliveryTimeTJ(),
                agreementPricelistRow.isDeliveryTimeOverridden(),
                agreementPricelistRow.getArticle());
        if( deliveryTime != null ) {
            writer.writeStartElement("LeadTimeDays");
            writer.writeCharacters(deliveryTime.toString());
            writer.writeEndElement(); // LeadTimeDays
            writer.writeCharacters(System.lineSeparator());
        }

        writeMaintenance(writer, agreementPricelistRow.getArticle(), xmlIdGenerator);

        // min order quantity
        writer.writeStartElement("MinOrderQty");
        writer.writeCharacters(Integer.toString(agreementPricelistRow.getLeastOrderQuantity()));
        writer.writeEndElement(); // MinOrderQty
        writer.writeCharacters(System.lineSeparator());

        // price
        writer.writeStartElement("Price");
        writer.writeCharacters(agreementPricelistRow.getPrice().toString());
        writer.writeEndElement(); // Price
        writer.writeCharacters(System.lineSeparator());

        // valid from
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.
                ofPattern(EXPORT_DATE_TIME_PATTERN).
                withZone(ZoneId.systemDefault());
        Instant instant = Instant.ofEpochMilli(validFrom.getTime());
        writer.writeStartElement("PriceValidFromTime");
        writer.writeCharacters(dateTimeFormatter.format(instant));
        writer.writeEndElement(); // PriceValidFromTime
        writer.writeCharacters(System.lineSeparator());

        // product (article) reference
        Long articleXmlId = xmlIdGenerator.getArticlesXmlId(agreementPricelistRow.getArticle().getUniqueId());
        if( articleXmlId == null ) {
            articleXmlId = xmlIdGenerator.generateArticlesXmlId(agreementPricelistRow.getArticle().getUniqueId());
        }
        writer.writeStartElement("Product");
        writer.writeAttribute("idref", articleXmlId.toString() );
        writer.writeEndElement(); // Product
        writer.writeCharacters(System.lineSeparator());

        // status ACTIVE and PENDING_INACTIVATION both means ACTIVE, but the actual status may be either
        AgreementPricelistRow.Status status = agreementPricelistRow.getStatus();
        if( status == AgreementPricelistRow.Status.PENDING_INACTIVATION ) {
            status = AgreementPricelistRow.Status.ACTIVE;
        }
        Long statusXmlId = xmlIdGenerator.getPricelistRowStatusXmlId(status.toString());
        if( statusXmlId == null ) {
            statusXmlId = xmlIdGenerator.generatePricelistRowStatusXmlId(status.toString());
        }
        writer.writeStartElement("Status");
        writer.writeAttribute("idref", statusXmlId.toString() );
        writer.writeEndElement(); // Status
        writer.writeCharacters(System.lineSeparator());

        // pricelist number
        writer.writeStartElement("SupplierPriceListNo");
        writer.writeCharacters(agreementPricelist.getNumber());
        writer.writeEndElement(); // SupplierPriceListNo
        writer.writeCharacters(System.lineSeparator());

        // warranty, we only show information if warranty "number" is available
        // if we only have unit, we skip this part
        String warrantyQuantity = getWarrantyQuantity(agreementPricelistRow.isWarrantyQuantityOverridden(),
                agreementPricelistRow.getWarrantyQuantity(),
                agreementPricelistRow.getArticle(),
                agreement.getWarrantyQuantityH(),
                agreement.getWarrantyQuantityI(),
                agreement.getWarrantyQuantityR(),
                agreement.getWarrantyQuantityT(),
                agreement.getWarrantyQuantityTJ());
        if( warrantyQuantity != null ) {
            writer.writeStartElement("e1ca9861c0b0442dba7d9d3230f36289");
            writer.writeCharacters(System.lineSeparator());

            writer.writeStartElement("_x0039_a6935ebf5eb4955b6c9a072dd948036");
            writer.writeCharacters(warrantyQuantity);
            writer.writeEndElement(); // _x0039_a6935ebf5eb4955b6c9a072dd948036
            writer.writeCharacters(System.lineSeparator());

            writer.writeEndElement(); // e1ca9861c0b0442dba7d9d3230f36289
            writer.writeCharacters(System.lineSeparator());
        }

        writer.writeEndElement(); // Entries
        writer.writeCharacters(System.lineSeparator());
    }

    private Integer getDeliveryTime(Integer rowDeliveryTime,
            Integer agreementDeliveryTimeH,
            Integer agreementDeliveryTimeI,
            Integer agreementDeliveryTimeR,
            Integer agreementDeliveryTimeT,
            Integer agreementDeliveryTimeTJ,
            boolean deliveryTimeOverridden,
            Article rowArticle ) {
        if( !deliveryTimeOverridden) {
            Article.Type articleType = getArticleType(rowArticle);
            if( articleType == Article.Type.H ) {
                return agreementDeliveryTimeH;
            } else if( articleType == Article.Type.I ) {
                return agreementDeliveryTimeI;
            } else if( articleType == Article.Type.R ) {
                return agreementDeliveryTimeR;
            } else if( articleType == Article.Type.T ) {
                return agreementDeliveryTimeT;
            } else if( articleType == Article.Type.Tj ) {
                return agreementDeliveryTimeTJ;
            }
        }
        return rowDeliveryTime;
    }

    private String getWarrantyQuantity(boolean isWarrantyQuantityOverridden,
            Integer rowWarrantyQuantity,
            Article article,
            Integer defaultWarrantyQuantityH,
            Integer defaultWarrantyQuantityI,
            Integer defaultWarrantyQuantityR,
            Integer defaultWarrantyQuantityT,
            Integer defaultWarrantyQuantityTJ) {
        if( !isWarrantyQuantityOverridden ) {
            Article.Type articleType = getArticleType(article);
            if( articleType == Article.Type.H ) {
                return defaultWarrantyQuantityH != null ? defaultWarrantyQuantityH.toString(): null;
            } else if( articleType == Article.Type.I ) {
                return defaultWarrantyQuantityI != null ? defaultWarrantyQuantityI.toString(): null;
            } else if( articleType == Article.Type.R ) {
                return defaultWarrantyQuantityR != null ? defaultWarrantyQuantityR.toString(): null;
            } else if( articleType == Article.Type.T ) {
                return defaultWarrantyQuantityT != null ? defaultWarrantyQuantityT.toString(): null;
            } else if( articleType == Article.Type.Tj ) {
                return defaultWarrantyQuantityTJ != null ? defaultWarrantyQuantityTJ.toString(): null;
            }
        }
        return rowWarrantyQuantity == null ? null: rowWarrantyQuantity.toString();
    }

    private Long getWarrantyQuantityUnit(XmlIdGenerator xmlIdGenerator,
            boolean warrantyQuantityUnitOverridden,
            Article article,
            CVGuaranteeUnit rowWarrantyQuantityUnit,
            CVGuaranteeUnit defaultWarrantyQuantityHUnit,
            CVGuaranteeUnit defaultWarrantyQuantityIUnit,
            CVGuaranteeUnit defaultWarrantyQuantityRUnit,
            CVGuaranteeUnit defaultWarrantyQuantityTUnit,
            CVGuaranteeUnit defaultWarrantyQuantityTJUnit) {
        Long warrantyQuantityUnitId = null;
        if( !warrantyQuantityUnitOverridden ) {
            Article.Type articleType = getArticleType(article);
            if( articleType == Article.Type.H ) {
                warrantyQuantityUnitId = defaultWarrantyQuantityHUnit != null ? defaultWarrantyQuantityHUnit.getUniqueId(): null;
            } else if( articleType == Article.Type.I ) {
                warrantyQuantityUnitId = defaultWarrantyQuantityIUnit != null ? defaultWarrantyQuantityIUnit.getUniqueId(): null;
            } else if( articleType == Article.Type.R ) {
                warrantyQuantityUnitId = defaultWarrantyQuantityRUnit != null ? defaultWarrantyQuantityRUnit.getUniqueId(): null;
            } else if( articleType == Article.Type.T ) {
                warrantyQuantityUnitId = defaultWarrantyQuantityTUnit != null ? defaultWarrantyQuantityTUnit.getUniqueId(): null;
            } else if( articleType == Article.Type.Tj ) {
                warrantyQuantityUnitId = defaultWarrantyQuantityTJUnit != null ? defaultWarrantyQuantityTJUnit.getUniqueId(): null;
            }
        } else {
            warrantyQuantityUnitId = rowWarrantyQuantityUnit.getUniqueId();
        }
        if( warrantyQuantityUnitId != null ) {
            Long unitXmlId = xmlIdGenerator.getGuaranteeUnitsXmlId(warrantyQuantityUnitId);
            return unitXmlId;
        }
        return null;
    }

    private Article.Type getArticleType(Article article) {
        return article.getBasedOnProduct() == null ? article.getCategory().getArticleType(): article.getBasedOnProduct().getCategory().getArticleType();
    }

    private void writeValidFromAndTo(XMLStreamWriter writer, Date validFromDate, Date validToDate) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeValidFromAndTo( ... )" );
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.
                ofPattern(EXPORT_DATE_TIME_PATTERN).
                withZone(ZoneId.systemDefault());

        // valids
        writer.writeStartElement("_x0034_e7e56aebebb40df80ed49c8e9a58b88");
        writer.writeCharacters(System.lineSeparator());

        // valid to
        writer.writeStartElement("_x0036_8f7af13de5c4688acf085eacf8886a5");
        Instant validTo = Instant.ofEpochMilli(validToDate.getTime());
        writer.writeCharacters(dateTimeFormatter.format(validTo));
        writer.writeEndElement(); // _x0036_8f7af13de5c4688acf085eacf8886a5
        writer.writeCharacters(System.lineSeparator());

        // valid from
        writer.writeStartElement("a30bd8ea7ed34018bed4897f2983be8f");
        Instant validFrom = Instant.ofEpochMilli(validFromDate.getTime());
        writer.writeCharacters(dateTimeFormatter.format(validFrom));
        writer.writeEndElement(); // a30bd8ea7ed34018bed4897f2983be8f
        writer.writeCharacters(System.lineSeparator());

        writer.writeEndElement(); //_x0034_e7e56aebebb40df80ed49c8e9a58b88
        writer.writeCharacters(System.lineSeparator());
    }

    private void writeMaintenance(XMLStreamWriter writer,
            Article article,
            XmlIdGenerator xmlIdGenerator) throws XMLStreamException {
        CVPreventiveMaintenance preventiveMaintenance = getArticlePreventiveMaintenanceValidFrom(article);
        Integer preventiveMaintenanceNumberOfDays = getArticlePreventiveMaintenanceNumberOfDays(article);
        if( preventiveMaintenance != null || preventiveMaintenanceNumberOfDays != null ) {
            writer.writeStartElement("Maintenance");
            writer.writeCharacters(System.lineSeparator());

            if( preventiveMaintenance != null ) {
                writer.writeStartElement("FromType");
                writer.writeAttribute("idref", Long.toString(xmlIdGenerator.getPreventiveMaintenanceXmlId(preventiveMaintenance.getUniqueId())));
                writer.writeEndElement(); // FromType
                writer.writeCharacters(System.lineSeparator());
            }

            if( preventiveMaintenanceNumberOfDays != null ) {
                writer.writeStartElement("Value");
                writer.writeCharacters( preventiveMaintenanceNumberOfDays.toString());
                writer.writeEndElement(); // Value
                writer.writeCharacters(System.lineSeparator());
            }
            writer.writeEndElement(); // Maintenance
            writer.writeCharacters(System.lineSeparator());
        }
    }

    private Integer getArticlePreventiveMaintenanceNumberOfDays(Article article) {
        if( article.getBasedOnProduct() != null ) {
            return article.isPreventiveMaintenanceNumberOfDaysOverridden() ? article.getPreventiveMaintenanceNumberOfDays(): article.getBasedOnProduct().getPreventiveMaintenanceNumberOfDays();
        } else {
            return article.getPreventiveMaintenanceNumberOfDays();
        }
    }

    private CVPreventiveMaintenance getArticlePreventiveMaintenanceValidFrom(Article article) {
        if( article.getBasedOnProduct() != null ) {
            return article.isPreventiveMaintenanceValidFromOverridden() ? article.getPreventiveMaintenanceValidFrom(): article.getBasedOnProduct().getPreventiveMaintenanceValidFrom();
        } else {
            return article.getPreventiveMaintenanceValidFrom();
        }
    }

    private void writeAgreementHead(XMLStreamWriter writer, Agreement agreement) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeAgreementHead( agreement->uniqueId: {0} )", new Object[] {agreement.getUniqueId()} );
        writer.writeStartElement("Head");
        writer.writeCharacters(System.lineSeparator());
        writer.writeStartElement("BuyerAgreementNo");
        writer.writeCharacters(agreement.getAgreementNumber());
        writer.writeEndElement(); // BuyerAgreementNo
        writer.writeCharacters(System.lineSeparator());
        writer.writeStartElement("SellerAgreementNo");
        writer.writeCharacters(agreement.getAgreementNumber());
        writer.writeEndElement(); // SellerAgreementNo
        writer.writeCharacters(System.lineSeparator());
        if( agreement.getAgreementName() != null ) {
            writer.writeStartElement("Title");
            writer.writeCharacters(agreement.getAgreementName());
            writer.writeEndElement(); // Title
            writer.writeCharacters(System.lineSeparator());
        }
        // pricelists
        List<AgreementPricelist> agreementPricelists = em.createNamedQuery(AgreementPricelist.GET_ALL_BY_AGREEMENT).
                setParameter("agreementUniqueId", agreement.getUniqueId()).
                getResultList();
        if( agreementPricelists != null && !agreementPricelists.isEmpty() ) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.
                ofPattern(EXPORT_DATE_TIME_PATTERN).
                withZone(ZoneId.systemDefault());
            for( AgreementPricelist agreementPricelist : agreementPricelists ) {
                writer.writeStartElement("VersionList");
                writer.writeCharacters(System.lineSeparator());
                writer.writeStartElement("VersionNo");
                writer.writeCharacters(agreementPricelist.getNumber());
                writer.writeEndElement(); // VersionNo
                writer.writeCharacters(System.lineSeparator());
                writer.writeStartElement("VersionValidFromTime");
                Instant instant = Instant.ofEpochMilli(agreementPricelist.getValidFrom().getTime());
                writer.writeCharacters(dateTimeFormatter.format(instant));
                writer.writeEndElement(); // VersionValidFromTime
                writer.writeCharacters(System.lineSeparator());
                writer.writeEndElement(); // VersionList
                writer.writeCharacters(System.lineSeparator());
            }
        }
        writer.writeEndElement(); // Head
        writer.writeCharacters(System.lineSeparator());
    }

    private void writeGeneralPricelistHead(XMLStreamWriter writer, GeneralPricelist generalPricelist) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeGeneralPricelistHead( generalPricelist->uniqueId: {0} )", new Object[] {generalPricelist.getUniqueId()} );
        writer.writeStartElement("Head");
        writer.writeCharacters(System.lineSeparator());
        writer.writeStartElement("SellerAgreementNo");
        writer.writeCharacters(generalPricelist.getGeneralPricelistNumber());
        writer.writeEndElement(); // SellerAgreementNo
        writer.writeCharacters(System.lineSeparator());
        if( generalPricelist.getGeneralPricelistName() != null ) {
            writer.writeStartElement("Title");
            writer.writeCharacters(generalPricelist.getGeneralPricelistName());
            writer.writeEndElement(); // Title
            writer.writeCharacters(System.lineSeparator());
        }

        // pricelists
        List<GeneralPricelistPricelist> generalPricelistPricelists = em.createNamedQuery(GeneralPricelistPricelist.GET_ALL_BY_GENERAL_PRICELIST).
                setParameter("generalPricelistUniqueId", generalPricelist.getUniqueId()).
                getResultList();
        if( generalPricelistPricelists != null && !generalPricelistPricelists.isEmpty() ) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.
                ofPattern(EXPORT_DATE_TIME_PATTERN).
                withZone(ZoneId.systemDefault());
            for( GeneralPricelistPricelist generalPricelistPricelist : generalPricelistPricelists ) {
                writer.writeStartElement("VersionList");
                writer.writeStartElement("VersionNo");
                writer.writeCharacters(generalPricelistPricelist.getNumber());
                writer.writeEndElement(); // VersionNo
                writer.writeCharacters(System.lineSeparator());
                writer.writeStartElement("VersionValidFromTime");
                Instant instant = Instant.ofEpochMilli(generalPricelistPricelist.getValidFrom().getTime());
                writer.writeCharacters(dateTimeFormatter.format(instant));
                writer.writeEndElement(); // VersionValidFromTime
                writer.writeCharacters(System.lineSeparator());
                writer.writeEndElement(); // VersionList
                writer.writeCharacters(System.lineSeparator());
            }
        }
        writer.writeEndElement(); // Head
        writer.writeCharacters(System.lineSeparator());
    }

    private void writeOrganizations(XMLStreamWriter writer, XmlIdGenerator xmlIdGenerator) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeOrganizations( ... )");
        Map<Long, Long> organizationsInAgreements = xmlIdGenerator.getOrganizationsMap();
        if( organizationsInAgreements != null && !organizationsInAgreements.isEmpty() ) {
            writer.writeStartElement("ListOrganization");
            writer.writeCharacters(System.lineSeparator());
            for( long organizationUniqueId : organizationsInAgreements.keySet() ) {
                Organization organization = em.find(Organization.class, organizationUniqueId);
                writeOrganization(writer, organization, xmlIdGenerator);
            }
            writer.writeEndElement(); // ListOrganization
            writer.writeCharacters(System.lineSeparator());
        }
    }

    private void writeOrganization(XMLStreamWriter writer, Organization organization, XmlIdGenerator xmlIdGenerator) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeOrganization( organization->uniqueId: {0} )", new Object[] {organization.getUniqueId()} );
        writer.writeStartElement("Organization");
        writer.writeAttribute("id", Long.toString(xmlIdGenerator.getOrganizationsXmlId(organization.getUniqueId())));
        writer.writeCharacters(System.lineSeparator());

        // gln
        writer.writeStartElement("GLN");
        writer.writeCharacters(organization.getGln());
        writer.writeEndElement(); // GLN
        writer.writeCharacters(System.lineSeparator());

        // url
        if( organization.getElectronicAddress().getWeb() != null ) {
            writer.writeStartElement("InformationPageUrl");
            writer.writeCharacters(organization.getElectronicAddress().getWeb());
            writer.writeEndElement(); // InformationPageUrl
            writer.writeCharacters(System.lineSeparator());
        }

        // name
        writer.writeStartElement("Name");
        writer.writeCharacters(organization.getOrganizationName());
        writer.writeEndElement(); // Name
        writer.writeCharacters(System.lineSeparator());

        writer.writeEndElement(); // Organization
        writer.writeCharacters(System.lineSeparator());
    }

    private void writeProducts(XMLStreamWriter writer,
            XmlIdGenerator xmlIdGenerator) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeProducts( ... )");
        Map<Long, Long> referencedArticles = xmlIdGenerator.getArticlesMap();
        if( referencedArticles != null && !referencedArticles.isEmpty() ) {
            int i = 1;
            int numberOfArticles = referencedArticles.keySet().size();
            Iterator<Long> referencedArticlesIdIterator = referencedArticles.keySet().iterator();
            while( referencedArticlesIdIterator.hasNext() ) {
                //LOG.log( Level.FINEST, "Write article: {0} of {1}", new Object[] {i, numberOfArticles});
                // this call will populate xmlIdGenerated->fitsToArticlesMap with
                // all necessary fits to connections, even in multiple steps
                Long articleId = referencedArticlesIdIterator.next();
                writeProduct(writer, articleId, xmlIdGenerator, false);
                i++;
            }
            Map<Long, Long> referencedFitsToArticles = xmlIdGenerator.getFitsToArticlesMap();
            if( referencedFitsToArticles != null && !referencedFitsToArticles.isEmpty() ) {
                int x = 1;
                int numberOfFitsToArticle = referencedFitsToArticles.keySet().size();
                Iterator<Long> fitsToArticleIdIterator = referencedFitsToArticles.keySet().iterator();
                while( fitsToArticleIdIterator.hasNext() ) {
                    LOG.log( Level.FINEST, "Write fitsToArticle: {0} of {1}", new Object[] {x, numberOfFitsToArticle});
                    Long fitsToArticleId = fitsToArticleIdIterator.next();
                    writeProduct(writer, fitsToArticleId, xmlIdGenerator, true);
                    x++;
                }
            }
        }
    }

    /**
     *
     * @param writer
     * @param articleId
     * @param xmlIdGenerator
     * @param fitsToArticles whether to check in the xmlIdGenerator -> fitsToArticlesMap or articlesMap
     * @throws XMLStreamException
     */

    //PELLE (Product = artikel)
    private void writeProduct(XMLStreamWriter writer,
            Long articleId,
            XmlIdGenerator xmlIdGenerator,
            boolean fitsToArticles) throws XMLStreamException {
        Article article = em.find(Article.class, articleId);
        writer.writeStartElement("Product");
        if( fitsToArticles ) {
            writer.writeAttribute("id", xmlIdGenerator.getFitsToArticlesMapXmlId(article.getUniqueId()).toString());
        } else {
            writer.writeAttribute("id", xmlIdGenerator.getArticlesXmlId(article.getUniqueId()).toString());
        }
        writer.writeCharacters(System.lineSeparator());

        // product identifier, same as VGRProdNo, required to be "c,<thenumber>,Product" by systems reading the file
        writer.writeStartElement("Identifier");
        writer.writeCharacters("c," + article.getCatalogUniqueNumber().getUniqueId().toString() + ",Product");
        writer.writeEndElement(); // Identifier
        writer.writeCharacters(System.lineSeparator());

        // main category
        Category category;
        //if( article.getBasedOnProduct() == null || article.getStatus() == Product.Status.DISCONTINUED) { //HJAL-1825 removed one term.
        //case 1
        if( article.getBasedOnProduct() == null ) {
            category = article.getCategory();
        } else {
            category = article.getBasedOnProduct().getCategory();
        }


        //PELLE 2020-02-20
        //case 2

        if (category.getCode() == null){
            if (article.getFitsToProducts() != null && article.getFitsToProducts().size() > 0) {
                category = article.getFitsToProducts().get(0).getCategory();
            }
        }

        //case 3
        if (category.getCode() == null){
            if (article.getFitsToArticles() != null && article.getFitsToArticles().size() > 0) {
                if (article.getFitsToArticles().get(0).getBasedOnProduct() != null) {
                    category = article.getFitsToArticles().get(0).getBasedOnProduct().getCategory();
                }
                // kan tänkas att detta ska med? PA 2020-10-05
                else{
                    category = article.getFitsToArticles().get(0).getCategory();
                }
            }
        }
        if (category.getCode() == null){
            if (article.getFitsToArticles() != null && article.getFitsToArticles().size() > 0) {
                if (article.getFitsToArticles().get(0).getFitsToProducts() != null && article.getFitsToArticles().get(0).getFitsToProducts().size() > 0) {
                    category = article.getFitsToArticles().get(0).getFitsToProducts().get(0).getCategory();
                }
            }
        }


        if( category.getCode() != null && category.getCode().length() > 6 ) {
            // we do not want to include categories longer than 6 digits
            // let's find the parent category with six digits
            category = category.getParent();
        }
        String code = getCategoryCode(category);
        if( code != null && !code.isEmpty() ) {
            writer.writeStartElement("_x0031_1fd813b4adb47f58d1d2e4bf67ce658");
            writer.writeCharacters(code);
            writer.writeEndElement(); // _x0031_1fd813b4adb47f58d1d2e4bf67ce658
            writer.writeCharacters(System.lineSeparator());
        }

        // ce
        writer.writeStartElement("_x0031_7fa973736524d658b7b944561195907");
        writer.writeCharacters(System.lineSeparator());
        writer.writeStartElement("CEmark");
        boolean articleCEMarked = isArticleCEMarked(article);
        writer.writeCharacters(articleCEMarked ? "true": "false");
        writer.writeEndElement(); // CEmark
        writer.writeCharacters(System.lineSeparator());
        CVCEDirective directive = getArticleDirective(article);
        if( directive != null ) {
            Long directiveXmlId = xmlIdGenerator.getDirectivesXmlId(directive.getUniqueId());
            writer.writeStartElement("Directive");
            writer.writeAttribute("idref", directiveXmlId.toString());
            writer.writeEndElement(); // Directive
            writer.writeCharacters(System.lineSeparator());
        }
        writer.writeEndElement(); // _x0031_7fa973736524d658b7b944561195907
        writer.writeCharacters(System.lineSeparator());

        Article.Type articleType = getArticleType(article);
        if( articleType == Article.Type.T ) {
            if( article.getBasedOnProduct() != null && category.getCode() != null ) {
                // if article is of type T and has code we want to write the product based on as a connection
                Long productXmlId = xmlIdGenerator.getProductsXmlId(article.getBasedOnProduct().getUniqueId());
                if( productXmlId == null ) {
                    productXmlId = xmlIdGenerator.generateProductsXmlId(article.getBasedOnProduct().getUniqueId());
                }
                writeFitsToElement(ACCESSORY_ELEMENT_NAME, writer, productXmlId);
            }
            writeFitsTo(writer, article, ACCESSORY_ELEMENT_NAME, xmlIdGenerator);
        } else if( articleType == Article.Type.R ) {
            writeFitsTo(writer, article, SPAREPART_ELEMENT_NAME, xmlIdGenerator);
        }

        boolean hideBasedOnTemplateProduct = (article.getBasedOnProduct() != null && category.getArticleType() == Article.Type.T && category.getCode() != null);
        if( article.getBasedOnProduct() != null && !hideBasedOnTemplateProduct) {
            Long productXmlId = xmlIdGenerator.getProductsXmlId(article.getBasedOnProduct().getUniqueId());
            if( productXmlId == null ) {
                productXmlId = xmlIdGenerator.generateProductsXmlId(article.getBasedOnProduct().getUniqueId());
            }
            writer.writeStartElement("BasedOnTemplateProduct");
            writer.writeAttribute("idref", productXmlId.toString());
            writer.writeEndElement(); // BasedOnTemplateProduct
            writer.writeCharacters(System.lineSeparator());
        }

        // main image
        ArticleMediaImage articleMediaImageMain = getArticleMediaMainImage(article.getUniqueId());
        if( articleMediaImageMain != null ) {
            writer.writeStartElement("Bild");
            writer.writeCharacters(System.lineSeparator());
            writer.writeStartElement("OriginalContentUrl");
            writer.writeCharacters(articleMediaImageMain.getMediaImage().getUrl());
            writer.writeEndElement(); // OriginalContentUrl
            writer.writeCharacters(System.lineSeparator());
            writer.writeEndElement(); // Bild
            writer.writeCharacters(System.lineSeparator());
        }

        // trademark
        String trademark = getArticleTrademark(article);
        if( trademark != null ) {
            writer.writeStartElement("Brand");
            writer.writeCharacters(trademark);
            writer.writeEndElement();
            writer.writeCharacters(System.lineSeparator());
        }

        // extended categories and main category, main category first, unless the category
        // has no code, in that case we do not print it
        if( category.getCode() != null && !category.getCode().isEmpty() ) {
            Long mainCategoryXmlId = xmlIdGenerator.getClassificationsXmlId(category.getUniqueId());
            writer.writeStartElement("Classification");
            writer.writeAttribute("idref", mainCategoryXmlId.toString());
            writer.writeEndElement(); // Classification
            writer.writeCharacters(System.lineSeparator());
        }
        if( article.getExtendedCategories() != null && !article.getExtendedCategories().isEmpty() ) {
            for( Category extendedCategory : article.getExtendedCategories() ) {
                if( category.getCode() != null && !category.getCode().isEmpty() ) {
                    if( extendedCategory.getCode() != null && extendedCategory.getCode().length() > 6 ) {
                        // we do not want to include categories longer than 6 digits
                        // let's find the parent category with six digits
                        extendedCategory = extendedCategory.getParent();
                    }
                    Long categoryXmlId = xmlIdGenerator.getClassificationsXmlId(extendedCategory.getUniqueId());
                    writer.writeStartElement("Classification");
                    writer.writeAttribute("idref", categoryXmlId.toString());
                    writer.writeEndElement();// Classification
                    writer.writeCharacters(System.lineSeparator());
                }
            }
        }

        // documents
        List<ArticleMediaDocument> articleMediaDocuments = getArticleMediaDocuments(article.getUniqueId());
        if( articleMediaDocuments != null && !articleMediaDocuments.isEmpty() ) {
            for( ArticleMediaDocument articleMediaDocument : articleMediaDocuments ) {
                writer.writeStartElement("Documents");
                writer.writeCharacters(System.lineSeparator());

                if( articleMediaDocument.getMediaDocument().getDescription() != null ) {
                    writer.writeStartElement("Description");
                    writer.writeCharacters(articleMediaDocument.getMediaDocument().getDescription());
                    writer.writeEndElement(); // Description
                    writer.writeCharacters(System.lineSeparator());
                }

                if( articleMediaDocument.getMediaDocument().getDocumentType() != null ) {
                    Long documentTypeXmlId = xmlIdGenerator.getDocumentTypesXmlId(articleMediaDocument.getMediaDocument().getDocumentType().getUniqueId());
                    if( documentTypeXmlId != null ) {
                        writer.writeStartElement("DocumentType");
                        writer.writeAttribute("idref", documentTypeXmlId.toString());
                        writer.writeEndElement(); // DocumentType
                        writer.writeCharacters(System.lineSeparator());
                    }
                }

                writer.writeStartElement("File");
                writer.writeCharacters(System.lineSeparator());
                writer.writeStartElement("OriginalContentUrl");
                writer.writeCharacters(articleMediaDocument.getMediaDocument().getUrl());
                writer.writeEndElement(); // OriginalContentUrl
                writer.writeCharacters(System.lineSeparator());
                writer.writeEndElement(); // File
                writer.writeCharacters(System.lineSeparator());

                writer.writeEndElement(); // Documents
                writer.writeCharacters(System.lineSeparator());
            }
        }

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.
            ofPattern(EXPORT_DATE_TIME_PATTERN).
            withZone(ZoneId.systemDefault());

        // expire time
        if( article.getReplacementDate() != null ) {
            writer.writeStartElement("ExpireTime");
            Instant instant = Instant.ofEpochMilli(article.getReplacementDate().getTime());
            writer.writeCharacters(dateTimeFormatter.format(instant));
            writer.writeEndElement();
            writer.writeCharacters(System.lineSeparator());
        }

        // gtin
        if( article.getGtin() != null ) {
            writer.writeStartElement("GTIN");
            writer.writeCharacters(article.getGtin());
            writer.writeEndElement(); // GTIN
            writer.writeCharacters(System.lineSeparator());
        }

        // images
        List<ArticleMediaImage> articleMediaImages = getArticleMediaImages(article.getUniqueId());
        if( articleMediaImages != null && !articleMediaImages.isEmpty() ) {
            for( ArticleMediaImage articleMediaImage : articleMediaImages ) {
                writer.writeStartElement("Images");
                writer.writeCharacters(System.lineSeparator());
                writer.writeStartElement("File");
                writer.writeCharacters(System.lineSeparator());
                writer.writeStartElement("OriginalContentUrl");
                writer.writeCharacters(articleMediaImage.getMediaImage().getUrl());
                writer.writeEndElement(); // OriginalContentUrl
                writer.writeCharacters(System.lineSeparator());
                writer.writeEndElement(); // File
                writer.writeCharacters(System.lineSeparator());

                writer.writeEndElement(); // Images
                writer.writeCharacters(System.lineSeparator());
            }
        }

        // articles are not template products, products are
        // HJAL-2021 products and P-TISO are template products
        writer.writeStartElement("IsTemplateProduct");
        writer.writeCharacters("false");
        /*if( articleType == Article.Type.T && article.getBasedOnProduct() != null && category.getCode() != null )
            writer.writeCharacters("true");
        else
            writer.writeCharacters("false");

         */
        writer.writeEndElement(); // IsTemplateProduct
        writer.writeCharacters(System.lineSeparator());

        writeOrderInformation(writer, article, xmlIdGenerator);

        // fits to
        if( articleType == Article.Type.I ) {
            writeFitsTo(writer, article, SETTING_ELEMENT_NAME, xmlIdGenerator);
        } else if( articleType == Article.Type.Tj ) {
            writeFitsTo(writer, article, SERVICE_ELEMENT_NAME, xmlIdGenerator);
        }

        // replaces list
        List<Article> replacesArticles = em.createNamedQuery(Article.FIND_REPLACES).
                setParameter("articleUniqueId", article.getUniqueId()).
                getResultList();
        if( replacesArticles != null && !replacesArticles.isEmpty() ) {
            for( Article replacesArticle : replacesArticles ) {
                // an article replaces by this may be in the list of pricelist entries
                Long replacesArticleXmlId = xmlIdGenerator.getArticlesXmlId(replacesArticle.getUniqueId());
                if( replacesArticleXmlId != null ) {
                    writer.writeStartElement("ReplacesList");
                    writer.writeAttribute("idref", replacesArticleXmlId.toString());
                    writer.writeEndElement();
                    writer.writeCharacters(System.lineSeparator());
                }
            }
        }

        // supplier
        writer.writeStartElement("Supplier");
        writer.writeAttribute("idref", xmlIdGenerator.getOrganizationsXmlId(article.getOrganization().getUniqueId()).toString());
        writer.writeEndElement();
        writer.writeCharacters(System.lineSeparator());

        // supplier product name
        writer.writeStartElement("SupplierProductName");
        writer.writeCharacters(article.getArticleName());
        writer.writeEndElement(); // SupplierProductName
        writer.writeCharacters(System.lineSeparator());

        // supplier product number
        writer.writeStartElement("SupplierProductNumber");
        writer.writeCharacters(article.getArticleNumber());
        writer.writeEndElement(); // SupplierProductNumber
        writer.writeCharacters(System.lineSeparator());

        // last updated / created
        writer.writeStartElement("UpdatedTime");
        Date lastUpdated = article.getLastUpdated() == null ? article.getCreated(): article.getLastUpdated();
        Instant instant = Instant.ofEpochMilli(lastUpdated.getTime());
        writer.writeCharacters(dateTimeFormatter.format(instant));
        writer.writeEndElement(); // UpdatedTime
        writer.writeCharacters(System.lineSeparator());

        // catalogue unique number
        writer.writeStartElement("VGRProdNo");
        writer.writeCharacters(article.getCatalogUniqueNumber().getUniqueId().toString());
        writer.writeEndElement(); // VGRProdNo
        writer.writeCharacters(System.lineSeparator());

        // supplemented information
        String supplementedInformation = getArticleSupplementedInformation(article);
        if( supplementedInformation != null ) {
            writer.writeStartElement("d0823a697d0f40dc97c13f2bda60c6d9");
            writer.writeCharacters(stripNonValidXMLCharacters(supplementedInformation));
            writer.writeEndElement(); // d0823a697d0f40dc97c13f2bda60c6d9
            writer.writeCharacters(System.lineSeparator());
        }

        writer.writeEndElement(); // Product
        writer.writeCharacters(System.lineSeparator());
    }
    // Akutchange 1.3.1
    private String stripNonValidXMLCharacters(String in) {
        StringBuffer out = new StringBuffer(); // Used to hold the output.
        char current; // Used to reference the current character.

        if (in == null || ("".equals(in))) return ""; // vacancy test.
        for (int i = 0; i < in.length(); i++) {
            current = in.charAt(i); // NOTE: No IndexOutOfBoundsException caught here; it should not happen.
            if ((current == 0x9) ||
                    (current == 0xA) ||
                    (current == 0xD) ||
                    ((current >= 0x20) && (current <= 0xD7FF)) ||
                    ((current >= 0xE000) && (current <= 0xFFFD)) ||
                    ((current >= 0x10000) && (current <= 0x10FFFF)))
                out.append(current);
        }
        return out.toString();
    }


    private void fetchFitsTo(XMLStreamWriter writer, List<Article> fitsToArticles, XmlIdGenerator xmlIdGenerator ) {
        //LOG.log(Level.FINEST, "fetchFitsTo( ... )" );
        if( fitsToArticles != null && !fitsToArticles.isEmpty() ) {
            for( Article fitsToArticle : fitsToArticles ) {
                if( canSeeFitsToArticle(fitsToArticle, xmlIdGenerator) ) {
                    Long articleXmlId = xmlIdGenerator.getArticlesXmlId(fitsToArticle.getUniqueId());
                    Long fitsToArticleXmlId = xmlIdGenerator.getFitsToArticlesMapXmlId(fitsToArticle.getUniqueId());
                    if( articleXmlId == null && fitsToArticleXmlId == null) {
                        xmlIdGenerator.generateFitsToArticlesMapXmlId(fitsToArticle.getUniqueId());
                        fetchFitsTo(writer, fitsToArticle.getFitsToArticles(), xmlIdGenerator);
                    }
                }
            }
        }
    }

    private void writeFitsTo(XMLStreamWriter writer, Article article, String elementName, XmlIdGenerator xmlIdGenerator) throws XMLStreamException {
        //LOG.log(Level.FINEST, "writeFitsTo( ... )" );
        // we want to order the connections based on catalogue unique number ascending
        // so a treemap with keys of that will work nice
        Map<Long, Long> fitsTos = new TreeMap<>();
        if( (article.getFitsToArticles() != null && !article.getFitsToArticles().isEmpty()) ) {
            for( Article fitsToArticle : article.getFitsToArticles() ) {
                if( canSeeFitsToArticle(fitsToArticle, xmlIdGenerator) ) {
                    Long articleXmlId = xmlIdGenerator.getArticlesXmlId(fitsToArticle.getUniqueId());
                    Long fitsToArticleXmlId = xmlIdGenerator.getFitsToArticlesMapXmlId(fitsToArticle.getUniqueId());
                    if( articleXmlId == null && fitsToArticleXmlId == null) {
                        fitsToArticleXmlId = xmlIdGenerator.generateFitsToArticlesMapXmlId(fitsToArticle.getUniqueId());
                    }
                    Long fitsTo = articleXmlId == null ? fitsToArticleXmlId: articleXmlId;
                    fitsTos.put(fitsToArticle.getCatalogUniqueNumber().getUniqueId(), fitsTo);
                    fetchFitsTo(writer, fitsToArticle.getFitsToArticles(), xmlIdGenerator);
                }
            }
        }
        if( (article.getFitsToProducts() != null && !article.getFitsToProducts().isEmpty()) ) {
            for( Product fitsToProduct : article.getFitsToProducts() ) {
                // customer unique products can always be seen even if no articles
                // based on it is visible to customer, information is needed
                Long productXmlId = xmlIdGenerator.getProductsXmlId(fitsToProduct.getUniqueId());
                if( productXmlId == null ) {
                    productXmlId = xmlIdGenerator.generateProductsXmlId(fitsToProduct.getUniqueId());
                }
                fitsTos.put(fitsToProduct.getCatalogUniqueNumber().getUniqueId(), productXmlId);
            }
        }
        for( Long fitsToCatalogueUniqueId : fitsTos.keySet() ) {
            Long fitsTo = fitsTos.get(fitsToCatalogueUniqueId);
            writeFitsToElement(elementName, writer, fitsTo);
        }
    }

    private void writeFitsToElement(String elementName, XMLStreamWriter writer, Long fitsTo) throws XMLStreamException {
        writer.writeStartElement(elementName);
        writer.writeAttribute("idref", fitsTo.toString());
        writer.writeEndElement();
        writer.writeCharacters(System.lineSeparator());
    }

    private void writeTemplateProducts(XMLStreamWriter writer, XmlIdGenerator xmlIdGenerator) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeTemplateProducts( ... )" );
        Map<Long, Long> referencedProducts = xmlIdGenerator.getProductsMap();
        if( referencedProducts != null && !referencedProducts.isEmpty() ) {
            int i = 1;
            int totalReferencedProducts = referencedProducts.keySet().size();
            Iterator<Long> referencedProductsIdIterator = referencedProducts.keySet().iterator();
            while( referencedProductsIdIterator.hasNext() ) {
                LOG.log( Level.FINEST, "Write template product: {0} of {1}", new Object[] {i, totalReferencedProducts});
                Long productId = referencedProductsIdIterator.next();
                writeTemplateProduct(writer, productId, xmlIdGenerator);
                i++;
            }
        }
    }
    //PELLE TemplateProduct = produkt
    private void writeTemplateProduct(XMLStreamWriter writer, Long productId, XmlIdGenerator xmlIdGenerator) throws XMLStreamException {
        LOG.log(Level.FINEST, "writeTemplateProduct( ... )" );
        Product product = em.find(Product.class, productId);
        writer.writeStartElement("Product");
        writer.writeAttribute("id", xmlIdGenerator.getProductsXmlId(product.getUniqueId()).toString());
        writer.writeCharacters(System.lineSeparator());

        // product identifier, same as VGRProdNo, required to be "c,<thenumber>,Product" by systems reading the file
        writer.writeStartElement("Identifier");
        writer.writeCharacters("c," + product.getCatalogUniqueNumber().getUniqueId().toString() + ",Product");
        writer.writeEndElement(); // Identifier
        writer.writeCharacters(System.lineSeparator());

        // main category
        Category category = product.getCategory();
        if( category.getCode() != null && category.getCode().length() > 6 ) {
            // we do not want to include categories longer than 6 digits
            // let's find the parent category with six digits
            category = category.getParent();
        }
        String code = getCategoryCode(category);
        if( code != null && !code.isEmpty() ) {
            writer.writeStartElement("_x0031_1fd813b4adb47f58d1d2e4bf67ce658");
            writer.writeCharacters(code);
            writer.writeEndElement(); // _x0031_1fd813b4adb47f58d1d2e4bf67ce658
            writer.writeCharacters(System.lineSeparator());
        }

        // ce
        writer.writeStartElement("_x0031_7fa973736524d658b7b944561195907");
        writer.writeStartElement("CEmark");
        writer.writeCharacters(product.isCeMarked() ? "true": "false");
        writer.writeEndElement(); // CEmark
        writer.writeCharacters(System.lineSeparator());
        if( product.getCeDirective() != null ) {
            Long directiveXmlId = xmlIdGenerator.getDirectivesXmlId(product.getCeDirective().getUniqueId());
            writer.writeStartElement("Directive");
            writer.writeAttribute("idref", directiveXmlId.toString());
            writer.writeEndElement(); // Directive
            writer.writeCharacters(System.lineSeparator());
        }
        writer.writeEndElement(); // _x0031_7fa973736524d658b7b944561195907
        writer.writeCharacters(System.lineSeparator());

        // main image
        writer.writeStartElement("Bild");
        writer.writeEndElement(); // Bild
        writer.writeCharacters(System.lineSeparator());

        // trademark
        if( product.getTrademark() != null ) {
            writer.writeStartElement("Brand");
            writer.writeCharacters(product.getTrademark());
            writer.writeEndElement();
            writer.writeCharacters(System.lineSeparator());
        }

        // extended categories and main category, main category first, unless the category
        // has no code, in that case we do not print it
        if( category.getCode() != null && !category.getCode().isEmpty() ) {
            Long mainCategoryXmlId = xmlIdGenerator.getClassificationsXmlId(category.getUniqueId());
            writer.writeStartElement("Classification");
            writer.writeAttribute("idref", mainCategoryXmlId.toString());
            writer.writeEndElement(); // Classification
            writer.writeCharacters(System.lineSeparator());
        }
        if( product.getExtendedCategories() != null && !product.getExtendedCategories().isEmpty() ) {
            for( Category extendedCategory : product.getExtendedCategories() ) {
                if( category.getCode() != null && !category.getCode().isEmpty() ) {
                    if( extendedCategory.getCode() != null && extendedCategory.getCode().length() > 6 ) {
                        // we do not want to include categories longer than 6 digits
                        // let's find the parent category with six digits
                        extendedCategory = extendedCategory.getParent();
                    }
                    Long categoryXmlId = xmlIdGenerator.getClassificationsXmlId(extendedCategory.getUniqueId());
                    writer.writeStartElement("Classification");
                    writer.writeAttribute("idref", categoryXmlId.toString());
                    writer.writeEndElement();// Classification
                    writer.writeCharacters(System.lineSeparator());
                }
            }
        }

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.
            ofPattern(EXPORT_DATE_TIME_PATTERN).
            withZone(ZoneId.systemDefault());


        // expire time
        if( product.getReplacementDate() != null ) {
            writer.writeStartElement("ExpireTime");
            Instant instant = Instant.ofEpochMilli(product.getReplacementDate().getTime());
            writer.writeCharacters(dateTimeFormatter.format(instant));
            writer.writeEndElement();
            writer.writeCharacters(System.lineSeparator());
        }

        // products are always template products
        writer.writeStartElement("IsTemplateProduct");
        writer.writeCharacters("true");
        writer.writeEndElement(); // IsTemplateProduct
        writer.writeCharacters(System.lineSeparator());

        // order unit
        Long orderUnitXmlId = xmlIdGenerator.getOrdereUnitsXmlId(product.getOrderUnit().getUniqueId());
        writer.writeStartElement("OrderUnit");
        writer.writeAttribute("idref", orderUnitXmlId.toString());
        writer.writeEndElement(); // OrderUnit
        writer.writeCharacters(System.lineSeparator());

        // article quantity in outer package
        if( product.getArticleQuantityInOuterPackage() != null ) {
            writer.writeStartElement("PackQty");
            writer.writeCharacters(product.getArticleQuantityInOuterPackage().toString());
            writer.writeEndElement(); // PackQty
            writer.writeCharacters(System.lineSeparator());
        }

        // package level base
        if( product.getPackageLevelBase() != null ) {
            writer.writeStartElement("Package1");
            writer.writeCharacters(product.getPackageLevelBase().toString());
            writer.writeEndElement(); // Package1
            writer.writeCharacters(System.lineSeparator());
        }

        // package level middle
        if( product.getPackageLevelMiddle() != null ) {
            writer.writeStartElement("Package2");
            writer.writeCharacters(product.getPackageLevelMiddle().toString());
            writer.writeEndElement(); // Package2
            writer.writeCharacters(System.lineSeparator());
        }

        // package level middle
        if( product.getPackageLevelTop() != null ) {
            writer.writeStartElement("PackagePallet");
            writer.writeCharacters(product.getPackageLevelTop().toString());
            writer.writeEndElement(); // PackagePallet
            writer.writeCharacters(System.lineSeparator());
        }

        // supplier
        writer.writeStartElement("Supplier");
        writer.writeAttribute("idref", xmlIdGenerator.getOrganizationsXmlId(product.getOrganization().getUniqueId()).toString());
        writer.writeEndElement();
        writer.writeCharacters(System.lineSeparator());

        // supplier product name
        writer.writeStartElement("SupplierProductName");
        writer.writeCharacters(product.getProductName());
        writer.writeEndElement(); // SupplierProductName
        writer.writeCharacters(System.lineSeparator());

        // supplier product number
        writer.writeStartElement("SupplierProductNumber");
        writer.writeCharacters(product.getProductNumber());
        writer.writeEndElement(); // SupplierProductNumber
        writer.writeCharacters(System.lineSeparator());

        // last updated / created
        writer.writeStartElement("UpdatedTime");
        Date lastUpdated = product.getLastUpdated() == null ? product.getCreated(): product.getLastUpdated();
        Instant instant = Instant.ofEpochMilli(lastUpdated.getTime());
        writer.writeCharacters(dateTimeFormatter.format(instant));
        writer.writeEndElement(); // UpdatedTime
        writer.writeCharacters(System.lineSeparator());

        // catalogue unique number
        writer.writeStartElement("VGRProdNo");
        writer.writeCharacters(product.getCatalogUniqueNumber().getUniqueId().toString());
        writer.writeEndElement(); // VGRProdNo
        writer.writeCharacters(System.lineSeparator());

        // supplemented information
        if( product.getSupplementedInformation() != null ) {
            writer.writeStartElement("d0823a697d0f40dc97c13f2bda60c6d9");
            writer.writeCharacters(product.getSupplementedInformation());
            writer.writeEndElement(); // d0823a697d0f40dc97c13f2bda60c6d9
            writer.writeCharacters(System.lineSeparator());
        }

        writer.writeEndElement(); // Product
        writer.writeCharacters(System.lineSeparator());
    }

    private String getCategoryCode(Category category) {
        String code = category.getCode();
        if( code != null && code.length() > 6 ) {
            code = code.substring(0, 6);
        }
        return code;
    }

    private void writeContainsChangesSince( XMLStreamWriter writer ) throws XMLStreamException {
        writer.writeStartElement("ContainsChangesSince");
        writer.writeEndElement();
        writer.writeCharacters(System.lineSeparator());
    }

    private void writeExportNumber( XMLStreamWriter writer, int number ) throws XMLStreamException {
        writer.writeStartElement("ExportNumber");
        writer.writeCharacters("" + number);
        writer.writeEndElement();
        writer.writeCharacters(System.lineSeparator());
    }

    private void writeTimeCreated( XMLStreamWriter writer ) throws XMLStreamException {
        writer.writeStartElement("TimeCreated");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(EXPORT_DATE_TIME_PATTERN);
        writer.writeCharacters(dateTimeFormatter.format(ZonedDateTime.now()));
        writer.writeEndElement();
        writer.writeCharacters(System.lineSeparator());
    }

    public List<Organization> getCustomers() {
        LOG.log(Level.FINEST, "getCustomers()");
        return em.createNamedQuery(Organization.FIND_BY_ORGANIZATION_TYPE).
                setParameter("organizationType", Organization.OrganizationType.CUSTOMER).
                getResultList();
    }

    private List<Long> getGeneralPricelistsByExportSettings(long exportSettingsUniqueId) {
        LOG.log(Level.FINEST, "getGeneralPricelistsByExportSettings( exportSettingsUniqueId: {0} )", new Object[] {exportSettingsUniqueId});
        return em.createQuery("SELECT gp.uniqueId FROM ExportSettings e JOIN e.generalPricelists gp WHERE e.uniqueId = :exportSettingsUniqueId").
                setParameter("exportSettingsUniqueId", exportSettingsUniqueId).
                getResultList();
    }

    private List<Long> getAgreementsForCustomerAndStatus(long customerUniqueId, List<Agreement.Status> statuses) {
        LOG.log(Level.FINEST, "getAgreementsForCustomerAndStatus( customerUniqueId: {0}, statuses: {1} )", new Object[] {customerUniqueId, statuses});
        return em.createQuery("SELECT DISTINCT(a.uniqueId) FROM Agreement a "
                + "LEFT JOIN a.sharedWithCustomers swc "
                + "LEFT JOIN a.sharedWithCustomerBusinessLevels swcbl "
                + "WHERE (a.customer.uniqueId = :customerUniqueId OR a.supplier.uniqueId = :customerUniqueId OR swc.uniqueId = :customerUniqueId OR swcbl.organization.uniqueId = :customerUniqueId) "
                + "AND a.status IN :statuses").
                setParameter("customerUniqueId", customerUniqueId).
                setParameter("statuses", statuses).
                getResultList();
    }

    private List<Long> getAgreementsForCustomerAndBusinessLevelAndStatus(long customerUniqueId, long businessLevelUniqueId, List<Agreement.Status> statuses) {
        LOG.log(Level.FINEST, "getAgreementsForCustomerAndBusinessLevelAndStatus( customerUniqueId: {0}, businessLevelUniqueId: {1}, statuses: {2} )", new Object[] {customerUniqueId, businessLevelUniqueId, statuses});
        return em.createQuery("SELECT a.uniqueId FROM Agreement a "
                + "LEFT JOIN a.sharedWithCustomerBusinessLevels swcbl "
                + "WHERE (a.customerBusinessLevel.uniqueId = :businessLevelUniqueId OR swcbl.uniqueId = :businessLevelUniqueId) "
                + "AND a.status IN :statuses").
                setParameter("businessLevelUniqueId", businessLevelUniqueId).
                setParameter("statuses", statuses).
                getResultList();
    }

    public AgreementPricelist getCurrentPricelist(long agreementUniqueId) {
        LOG.log(Level.FINEST, "getCurrentPricelist( agreementUniqueId: {0} )", new Object[] {agreementUniqueId} );
        List<AgreementPricelist> pricelists = em.createNamedQuery(AgreementPricelist.FIND_BY_PASSED_VALID_FROM_AND_AGREEMENT).
                setParameter("agreementUniqueId", agreementUniqueId).
                setParameter("validFrom", new Date()).
                getResultList();
        if( pricelists != null && !pricelists.isEmpty() ) {
            // query is ORDER BY validFrom DESC so first is most recent
            return pricelists.get(0);
        }
        return null;
    }

    public List<AgreementPricelist> getFuturePricelists(long agreementUniqueId) {
        LOG.log(Level.FINEST, "getFuturePricelists( agreementUniqueId: {0} )", new Object[] {agreementUniqueId} );
        return em.createNamedQuery(AgreementPricelist.FIND_BY_NOT_PASSED_VALID_FROM_AND_AGREEMENT).
                setParameter("agreementUniqueId", agreementUniqueId).
                setParameter("validFrom", new Date()).
                getResultList();
    }

    public GeneralPricelistPricelist getCurrentGeneralPricelistPricelist(long generalPricelistUniqueId) {
        LOG.log(Level.FINEST, "getCurrentGeneralPricelistPricelist( generalPricelistUniqueId: {0} )", new Object[] {generalPricelistUniqueId} );
        List<GeneralPricelistPricelist> pricelists = em.createNamedQuery(GeneralPricelistPricelist.FIND_BY_PASSED_VALID_FROM_AND_GENERAL_PRICELIST).
                setParameter("generalPricelistUniqueId", generalPricelistUniqueId).
                setParameter("validFrom", new Date()).
                getResultList();
        if( pricelists != null && !pricelists.isEmpty() ) {
            // query is ORDER BY validFrom DESC so first is most recent
            return pricelists.get(0);
        }
        return null;
    }


    private ArticleMediaImage getArticleMediaMainImage(long articleUniqueId) {
        //LOG.log(Level.FINEST, "getArticleMediaMainImage( articleUniqueId: {0} )", new Object[] {articleUniqueId} );
        List<ArticleMediaImage> articleMediaMainImages = em.createNamedQuery(ArticleMediaImage.FIND_BY_ARTICLE_AND_MAIN).
                setParameter("articleUniqueId", articleUniqueId).
                getResultList();
        // there shall be a maximum of one
        if( articleMediaMainImages != null && !articleMediaMainImages.isEmpty() ) {
            return articleMediaMainImages.get(0);
        }
        return null;
    }

    private List<ArticleMediaImage> getArticleMediaImages(long articleUniqueId) {
        //LOG.log(Level.FINEST, "getArticleMediaImages( articleUniqueId: {0} )", new Object[] {articleUniqueId} );
        return em.createNamedQuery(ArticleMediaImage.FIND_BY_ARTICLE_MAIN_FIRST).
                setParameter("articleUniqueId", articleUniqueId).
                getResultList();
    }

    private List<ArticleMediaDocument> getArticleMediaDocuments(long articleUniqueId) {
        //LOG.log(Level.FINEST, "getArticleMediaDocuments( articleUniqueId: {0} )", new Object[] {articleUniqueId} );
        return em.createNamedQuery(ArticleMediaDocument.FIND_BY_ARTICLE).
                setParameter("articleUniqueId", articleUniqueId).
                getResultList();
    }

    private boolean isArticleCEMarked( Article article ) {
        if( article.getBasedOnProduct() != null ) {
            return article.isCeMarkedOverridden() ? article.isCeMarked(): article.getBasedOnProduct().isCeMarked();
        } else {
            return article.isCeMarked();
        }
    }

    private CVCEDirective getArticleDirective( Article article ) {
        if( article.getBasedOnProduct() != null ) {
            return article.isCeDirectiveOverridden() ? article.getCeDirective(): article.getBasedOnProduct().getCeDirective();
        } else {
            return article.getCeDirective();
        }
    }

    private String getArticleTrademark( Article article ) {
        if( article.getBasedOnProduct() != null ) {
            return article.isTrademarkOverridden() ? article.getTrademark(): article.getBasedOnProduct().getTrademark();
        } else {
            return article.getTrademark();
        }
    }

    private void writeOrderInformation( XMLStreamWriter writer, Article article, XmlIdGenerator xmlIdGenerator ) throws XMLStreamException {
        // order unit
        CVOrderUnit orderUnit = getOrderUnit(article);
        Long orderUnitXmlId = xmlIdGenerator.getOrdereUnitsXmlId(orderUnit.getUniqueId());
        writer.writeStartElement("OrderUnit");
        writer.writeAttribute("idref", orderUnitXmlId.toString());
        writer.writeEndElement(); // OrderUnit
        writer.writeCharacters(System.lineSeparator());

        // article quantity in outer package
        Double packageQuantity = getArticleQuantityInOuterPackage(article);
        if( packageQuantity != null ) {
            writer.writeStartElement("PackQty");
            writer.writeCharacters(packageQuantity.toString());
            writer.writeEndElement(); // PackQty
            writer.writeCharacters(System.lineSeparator());
        }

        // package level base
        Integer packageLevelBase = getPackageLevelBase(article);
        if( packageLevelBase != null ) {
            writer.writeStartElement("Package1");
            writer.writeCharacters(packageLevelBase.toString());
            writer.writeEndElement(); // Package1
            writer.writeCharacters(System.lineSeparator());
        }

        // package level middle
        Integer packageLevelMiddle = getPackageLevelMiddle(article);
        if( packageLevelMiddle != null ) {
            writer.writeStartElement("Package2");
            writer.writeCharacters(packageLevelMiddle.toString());
            writer.writeEndElement(); // Package2
            writer.writeCharacters(System.lineSeparator());
        }

        // package level middle
        Integer packageLevelTop = getPackageLevelTop(article);
        if( packageLevelTop != null ) {
            writer.writeStartElement("PackagePallet");
            writer.writeCharacters(packageLevelTop.toString());
            writer.writeEndElement(); // PackagePallet
            writer.writeCharacters(System.lineSeparator());
        }
    }

    private CVOrderUnit getOrderUnit( Article article ) {
        //LOG.log(Level.FINEST, "getOrderUnit" );
        if( article.getBasedOnProduct() != null ) {
            //LOG.log(Level.FINEST, "is basedOnProduct" );
            //LOG.log(Level.FINEST, "is article.isOrderInformationOverridden() " + article.isOrderInformationOverridden() );
            //if (article.isOrderInformationOverridden())
            //    LOG.log(Level.FINEST, "article.getOrderUnit() " + article.getOrderUnit().getName());
            //else
            //    LOG.log(Level.FINEST, "article.getBasedOnProduct().getOrderUnit() " + article.getBasedOnProduct().getOrderUnit().getName());
            return article.isOrderInformationOverridden() ? article.getOrderUnit(): article.getBasedOnProduct().getOrderUnit();
        } else {
            //LOG.log(Level.FINEST, "is NOT basedOnProduct" );
            //LOG.log(Level.FINEST, " article.getOrderUnit() " +  article.getOrderUnit().getName());
            return article.getOrderUnit();
        }
    }

    private Double getArticleQuantityInOuterPackage( Article article ) {
        if( article.getBasedOnProduct() != null ) {
            return article.isOrderInformationOverridden() ? article.getArticleQuantityInOuterPackage(): article.getBasedOnProduct().getArticleQuantityInOuterPackage();
        } else {
            return article.getArticleQuantityInOuterPackage();
        }
    }

    private Integer getPackageLevelBase( Article article ) {
        if( article.getBasedOnProduct() != null ) {
            return article.isOrderInformationOverridden() ? article.getPackageLevelBase(): article.getBasedOnProduct().getPackageLevelBase();
        } else {
            return article.getPackageLevelBase();
        }
    }

    private Integer getPackageLevelMiddle( Article article ) {
        if( article.getBasedOnProduct() != null ) {
            return article.isOrderInformationOverridden() ? article.getPackageLevelMiddle(): article.getBasedOnProduct().getPackageLevelMiddle();
        } else {
            return article.getPackageLevelMiddle();
        }
    }

    private Integer getPackageLevelTop( Article article ) {
        if( article.getBasedOnProduct() != null ) {
            return article.isOrderInformationOverridden() ? article.getPackageLevelTop(): article.getBasedOnProduct().getPackageLevelTop();
        } else {
            return article.getPackageLevelTop();
        }
    }

    private String getArticleSupplementedInformation( Article article ) {
        if( article.getBasedOnProduct() != null ) {
            return article.isSupplementedInformationOverridden() ? article.getSupplementedInformation(): article.getBasedOnProduct().getSupplementedInformation();
        } else {
            return article.getSupplementedInformation();
        }
    }

    /**
     * Returns the first entered delivery time on the agreement from the order in the gui.
     * If the gui order changes, this will need to change also.
     *
     * @return
     */
    private Integer getDeliveryTimeByGuiOrder(Integer deliveryTimeH, Integer deliveryTimeT, Integer deliveryTimeR, Integer deliveryTimeI, Integer deliveryTimeTJ) {
        LOG.log(Level.FINEST, "getDeliveryTimeByGuiOrder( ... )" );
        if( deliveryTimeH != null ) {
            return deliveryTimeH;
        } else if( deliveryTimeT != null ) {
            return deliveryTimeT;
        } else if( deliveryTimeR != null ) {
            return deliveryTimeR;
        } else if( deliveryTimeI != null ) {
            return deliveryTimeI;
        } else if( deliveryTimeTJ != null ) {
            return deliveryTimeTJ;
        }
        return null;
    }

    /**
     * Returns the first warranty quantity on the agreement from the order in the gui.
     * If the gui order changes, this will need to change also.
     *
     * @return
     */
    private Integer getWarrantyQuantityByGuiOrder(Integer warrantyQuantityH, Integer warrantyQuantityT, Integer warrantyQuantityR, Integer warrantyQuantityI, Integer warrantyQuantityTJ) {
        LOG.log(Level.FINEST, "getWarrantyQuantityByGuiOrder( ... )" );
        if( warrantyQuantityH != null ) {
            return warrantyQuantityH;
        } else if( warrantyQuantityT != null ) {
            return warrantyQuantityT;
        } else if( warrantyQuantityR != null ) {
            return warrantyQuantityR;
        } else if( warrantyQuantityI != null ) {
            return warrantyQuantityI;
        } else if( warrantyQuantityTJ != null ) {
            return warrantyQuantityTJ;
        }
        return null;
    }

    public static boolean isSameDay( long time1, long time2 ) {
        Calendar c1 = Calendar.getInstance();
        c1.setTimeInMillis(time1);

        Calendar c2 = Calendar.getInstance();
        c2.setTimeInMillis(time2);

        if( c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) &&
                c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) &&
                c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH) ) {
            return true;
        }
        return false;
    }

    private boolean canSeeFitsToArticle(Article fitsToArticle, XmlIdGenerator xmlIdGenerator) {
        boolean canSee = true;
        boolean customerUnique;
        if( fitsToArticle.getBasedOnProduct() == null ) {
            customerUnique = fitsToArticle.isCustomerUnique();
        } else {
            if( fitsToArticle.isCustomerUniqueOverridden() ) {
                customerUnique = fitsToArticle.isCustomerUnique();
            } else {
                customerUnique = fitsToArticle.getBasedOnProduct().isCustomerUnique();
            }
        }
        if( customerUnique ) {
            LOG.log( Level.FINEST, "Found customer unique article {0}", new Object[] {fitsToArticle.getUniqueId()});
            if( !xmlIdGenerator.getArticlesMap().containsKey(fitsToArticle.getUniqueId()) ) {
                LOG.log( Level.FINEST, "Customer unique article {0} cannot be seen", new Object[] {fitsToArticle.getUniqueId()});
                canSee = false;
            }
        }
        return canSee;
    }

}
