package se.inera.hjalpmedelstjansten.export;

import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.clustering.ClusterController;
import se.inera.hjalpmedelstjansten.model.entity.ExportSettings;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import jakarta.ejb.DependsOn;
import jakarta.ejb.ScheduleExpression;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.ejb.Timeout;
import jakarta.ejb.TimerConfig;
import jakarta.ejb.TimerService;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * This timer wakes up at regular intervals and checks the database to see if there
 * is any ExportSettings that have not yet been executed today. If so, it populates
 * a cluster list with the ids of the ExportSettings. The ExportGenerationWorkerTimer
 * is then responsible for handling these.
 *
 * @see se.inera.hjalpmedelstjansten.export.ExportGenerationTimerWorker
 */
@Singleton
@Startup
@DependsOn("PropertyLoader")
public class ExportGenerationTimer {

    public static final String QUEUE_NAME = "export_queue";

    @Inject
    private HjmtLogger LOG;

    @Inject
    private boolean exportGenerationTimerEnabled;

    @Inject
    private String exportGenerationTimerHour;

    @Inject
    private String exportGenerationTimerMinute;

    @Inject
    private String exportGenerationTimerSecond;

    @Inject
    private ClusterController clusterController;

    @Resource
    private TimerService timerService;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Timeout
    @TransactionTimeout(value = 8, unit = TimeUnit.HOURS)
    private void schedule() {
        LOG.log( Level.FINEST, "schedule" );
        // attempt to get lock
        boolean lockReceived = clusterController.getLock(this.getClass().getName(), 30);
        if( lockReceived ) {
            LOG.log( Level.FINEST, "Received lock!" );
            long start = System.currentTimeMillis();
            List<ExportSettings> exportSettingses = em.createNamedQuery(ExportSettings.FIND_ALL).
                getResultList();
            for( ExportSettings exportSettings : exportSettingses ) {
                if( !exportSettings.isEnabled() ) {
                    LOG.log( Level.INFO, "Export settings disabled id: {0}", new Object[] {exportSettings.getUniqueId()} );
                    continue;
                }
                // we only create one xml per day
                if( exportSettings.getLastExported() != null && ExportGenerationController.isSameDay(exportSettings.getLastExported().getTime(), new Date().getTime()) ) {
                    LOG.log( Level.FINEST, "Export for export settings {0} have already been generated today. Don't add to queue", new Object[] {exportSettings.getUniqueId()} );
                    continue;
                }
                clusterController.addLongToQueue(QUEUE_NAME, exportSettings.getUniqueId());
            }
            long total = System.currentTimeMillis() - start;
            LOG.logPerformance(this.getClass().getName(), "schedule()", total, null);
            if( total > (1000 * 60) ) {
                LOG.log( Level.WARNING, "Scheduled job took: {0} ms which is more than 1 minute which is a long time for this job, a developer should take a look at this.", new Object[] {total});
            }
            //release lock
            clusterController.releaseLock(this.getClass().getName());
        } else {
            LOG.log( Level.INFO, "Did not receive lock!" );
        }

    }

    @PostConstruct
    private void initialize() {
        LOG.log( Level.FINEST, "initialize(exportGenerationTimerHour: {0}, exportGenerationTimerMinute: {1}, exportGenerationTimerSecond: {2})", new Object[] {exportGenerationTimerHour, exportGenerationTimerMinute, exportGenerationTimerSecond} );
        if( exportGenerationTimerEnabled ) {
            LOG.log( Level.FINEST, "Timer is ENABLED" );
            if (timerService.getTimers().isEmpty()) {
                String name = this.getClass().getName();
                TimerConfig configuration = new TimerConfig();
                configuration.setPersistent(false);
                configuration.setInfo(name);
                ScheduleExpression scheduleExpression = new ScheduleExpression();
                scheduleExpression.
                        hour(exportGenerationTimerHour).
                        minute(exportGenerationTimerMinute).
                        second(exportGenerationTimerSecond);
                timerService.createCalendarTimer(scheduleExpression, configuration);
            }
        } else {
            LOG.log( Level.INFO, "Timer is NOT ENABLED" );
        }
    }

}
