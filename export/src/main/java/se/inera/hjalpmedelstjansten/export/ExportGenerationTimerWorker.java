package se.inera.hjalpmedelstjansten.export;

import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.clustering.ClusterController;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import jakarta.ejb.DependsOn;
import jakarta.ejb.ScheduleExpression;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.ejb.Timeout;
import jakarta.ejb.TimerConfig;
import jakarta.ejb.TimerService;
import jakarta.inject.Inject;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * The worker wakes up at regular intervals and checks a specific cluster list to
 * see if there is anything to do. If so, the worker will work until there is nothing
 * left to do. The cluster list is populated by the ExportGenerationTimer.
 *
 */
@Singleton
@Startup
@DependsOn("PropertyLoader")
public class ExportGenerationTimerWorker {

    @Inject
    private HjmtLogger LOG;

    @Inject
    private boolean exportGenerationWorkerTimerEnabled;

    @Inject
    private String exportGenerationWorkerTimerHour;

    @Inject
    private String exportGenerationWorkerTimerMinute;

    @Inject
    private String exportGenerationWorkerTimerSecond;

    @Inject
    ExportGenerationController exportGenerationController;

    @Inject
    private ClusterController clusterController;

    @Resource
    private TimerService timerService;

    @Timeout
    @TransactionTimeout(value = 8, unit = TimeUnit.HOURS)
    private void schedule() {
        LOG.log( Level.FINEST, "schedule" );
        long start = System.currentTimeMillis();
        Long nextId = clusterController.getNextLongFromQueue(ExportGenerationTimer.QUEUE_NAME);
        int numberOfExportsHandled = 0;
        while( nextId != null ) {
            if (nextId == 2389664) { //special case for VGR
                Calendar nowDate = Calendar.getInstance();
                //GP på tis, tors och lör
                if (nowDate.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY || nowDate.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY || nowDate.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY){
                    exportGenerationController.exportGP(nextId, false, null);
                }
                else {//Inköpsavtal på mån, ons, fre och sön
                    exportGenerationController.exportAvtal(nextId, false, null);
                }
            }
            else{
                exportGenerationController.export(nextId, false, null);
            }
            nextId = clusterController.getNextLongFromQueue(ExportGenerationTimer.QUEUE_NAME);
            numberOfExportsHandled++;
        }
        if( numberOfExportsHandled > 0 ) {
            LOG.log( Level.FINEST, "Worker handled {0} exports", new Object[] {numberOfExportsHandled} );
            long total = System.currentTimeMillis() - start;
            LOG.logPerformance(this.getClass().getName(), "schedule()", total, null);
        } else {
            LOG.log( Level.FINEST, "Nothing to do" );
        }
    }

    @PostConstruct
    private void initialize() {
        LOG.log( Level.FINEST, "initialize(exportGenerationWorkerTimerHour: {0}, exportGenerationWorkerTimerMinute: {1}, exportGenerationWorkerTimerSecond: {2})", new Object[] {exportGenerationWorkerTimerHour, exportGenerationWorkerTimerMinute, exportGenerationWorkerTimerSecond} );
        if( exportGenerationWorkerTimerEnabled ) {
            LOG.log( Level.FINEST, "Timer is ENABLED" );
            if (timerService.getTimers().isEmpty()) {
                String name = this.getClass().getName();
                TimerConfig configuration = new TimerConfig();
                configuration.setPersistent(false);
                configuration.setInfo(name);
                ScheduleExpression scheduleExpression = new ScheduleExpression();
                scheduleExpression.
                        hour(exportGenerationWorkerTimerHour).
                        minute(exportGenerationWorkerTimerMinute).
                        second(exportGenerationWorkerTimerSecond);
                timerService.createCalendarTimer(scheduleExpression, configuration);
            }
        } else {
            LOG.log( Level.INFO, "Timer is NOT ENABLED" );
        }
    }

}
