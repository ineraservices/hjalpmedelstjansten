package se.inera.hjalpmedelstjansten.property;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Properties;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junitpioneer.jupiter.SetEnvironmentVariable;

class AppConfigurationServiceTest {

    private AppConfigurationService appConfigurationService = new AppConfigurationService();

    @Test
    @Disabled("The way this test uses java internals to modify environment variables is not allowed from java 17+. Test needs to be rewritten.")
    @SetEnvironmentVariable(key = "PROPERTYINENV", value = "propertyInEnvValue")
    void shallReturnPropertyFromEnvWhenOnlyExistsInEnv() {
        final var propertyKey = "propertyInEnv";
        final var expectedValue = "propertyInEnvValue";

        final var actualValue = appConfigurationService.getString(propertyKey);

        assertEquals(expectedValue, actualValue);
    }

    @Test
    void shallReturnPropertyFromPropertiesWhenOnlyExistsInProperties()  {
        final var propertyKey = "propertyNotInEnv";
        final var expectedValue = "propertyValue";

        addProperty(propertyKey, expectedValue);

        final var actualValue = appConfigurationService.getString(propertyKey);

        assertEquals(expectedValue, actualValue);
    }

    @Test
    @Disabled("The way this test uses java internals to modify environment variables is not allowed from java 17+. Test needs to be rewritten.")
    @SetEnvironmentVariable(key = "PROPERTYINENV", value = "propertyInEnvValue")
    void shallReturnPropertyFromEnvWhenOverridden() {
        final var propertyKey = "propertyInEnv";
        final var expectedValue = "propertyInEnvValue";

        addProperty(propertyKey, "propertyInPropertiesValue");

        final var actualValue = appConfigurationService.getString(propertyKey);

        assertEquals(expectedValue, actualValue);
    }

    private void addProperty(String propertyNotInEnv, String expectedValue)  {
        final var properties = new Properties();
        properties.setProperty(propertyNotInEnv, expectedValue);

        try {
            final var f1 = appConfigurationService.getClass().getDeclaredField("properties");
            f1.setAccessible(true);
            f1.set(appConfigurationService, properties);
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }
}
