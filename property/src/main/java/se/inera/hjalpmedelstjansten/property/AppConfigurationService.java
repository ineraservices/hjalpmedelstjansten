package se.inera.hjalpmedelstjansten.property;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;

import jakarta.ejb.Singleton;
import jakarta.enterprise.inject.spi.InjectionPoint;
import jakarta.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

/**
 * Reads configuration files and exposes them with @Produces allowing classes to
 * get properties by writing<br /><br />
 *
 * @Inject
 * String theNameOfTheStringPropertyAsItSaysInPropertyFile;
 *
 * <br /><br />or<br /><br />
 *
 * @Inject
 * boolean theNameOfTheBooleanPropertyAsItSaysInPropertyFile
 *
 * <br /><br />or<br /><br />
 *
 * @Inject
 * boolean theNameOfTheIntPropertyAsItSaysInPropertyFile
 *
 * @Inject
 * Date theNameOfTheDatePropertyAsItSaysInPropertyFile
 *
 */
@Singleton
public class AppConfigurationService {

    @Inject
    HjmtLogger LOG;

    private Properties properties = new Properties();

    @jakarta.enterprise.inject.Produces
    public String getString(InjectionPoint point) {
        String fieldName = point.getMember().getName();
        return getString(fieldName);
    }

    @jakarta.enterprise.inject.Produces
    public boolean getBoolean(InjectionPoint point) {
        String stringValue = getString(point);
        return Boolean.parseBoolean(stringValue);
    }

    @jakarta.enterprise.inject.Produces
    public int getInt(InjectionPoint point) {
        String stringValue = getString(point);
        return Integer.parseInt(stringValue);
    }

    public String getString(String fieldName) {
        final var env = System.getenv(fieldName.toUpperCase());
        if (env == null || env.isBlank()) {
            return properties.getProperty(fieldName);
        }
        return env;
    }

    public boolean getBoolean(String fieldName) {
        String stringValue = getString(fieldName);
        return Boolean.parseBoolean(stringValue);
    }

    public int getInt(String fieldName) {
        String stringValue = getString(fieldName);
        return Integer.parseInt(stringValue);
    }

    public void loadProperties( String propertyFileName ) {
        try ( InputStream appPropertiesInputStream = this.getClass().getClassLoader().getResourceAsStream(propertyFileName) ) {
            Reader appPropertiesReader = new InputStreamReader(appPropertiesInputStream, StandardCharsets.UTF_8);
            properties.load(appPropertiesReader);
        } catch (IOException ex) {
            System.err.println( "Failed to load " + propertyFileName );
            ex.printStackTrace(System.err);
        }
    }

}
