package se.inera.hjalpmedelstjansten.property;

import jakarta.ejb.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

/**
 * Reads texts from property file to Properties object
 *
 */
@Singleton
public class TextController {

    private Properties texts = new Properties();
    private Properties messages = new Properties();

    public Properties getTexts() {
        return texts;
    }
    public Properties getMessages() { return messages; }

    public void loadTexts(String propertyFileName) {
        try ( InputStream textsPropertiesInputStream = this.getClass().getClassLoader().getResourceAsStream(propertyFileName) ) {
            Reader textsReader = new InputStreamReader(textsPropertiesInputStream, StandardCharsets.UTF_8);
            texts.load(textsReader);
        } catch (IOException ex) {
            System.err.println( "Failed to load " + propertyFileName );
            ex.printStackTrace(System.err);
        }
    }

    public void loadMessages(String propertyFileName) {
        try ( InputStream messagesPropertiesInputStream = this.getClass().getClassLoader().getResourceAsStream(propertyFileName) ) {
            Reader textsReader = new InputStreamReader(messagesPropertiesInputStream, StandardCharsets.UTF_8);
            messages.load(textsReader);
        } catch (IOException ex) {
            System.err.println( "Failed to load " + propertyFileName );
            ex.printStackTrace(System.err);
        }
    }

}
