@Library( 'essLib')_

// Context
String gitUrl
String gitBranch
String buildBranch
String buildUrl
String buildTag
Map    cloneInfo
String commit
String version
String commitTag

node('linux') {
  stage( 'Setup') {
    // Cleanup
    deleteDir()

    // Setup
    gitUrl      = essJob.getScmUrl()
    gitBranch   = essJob.getScmBranch()

    // Clone
    buildUrl        = "${env.BUILDREPO_URL}"
    buildBranch     = "${params.BUILDBRANCH}"
    buildTag        = ''
    cloneInfo       = essGit.clone( url: buildUrl, branch: buildBranch )

    // Get info
    commit    = essGit.getCommit()
    version   = "${essCmn.getVersion()}.${env.BUILD_NUMBER}"
    commitTag = "dockerimage/${version}"
  }

  stage('Debug') {
    if(env.DEBUG_PIPELINE == "true") {
      sh 'ls -R'
      sh 'cat Jenkinsfile'
      sh 'cat Jenkins.properties'

      echo "buildtag: ${buildTag}"
      echo "commit: ${commit}"
      echo "version: ${version}"
      echo "commitTag: ${commitTag}"
      echo "TEST_MODE: ${env.TEST_MODE}"
      echo "BUILDREPO_URL: ${env.BUILDREPO_URL}"
    }
  }

  stage('Building application in maven-jdk') {
    docker.withRegistry('https://docker.drift.inera.se', 'Nexus image registry') {
      docker.image('hjmtj/app-builder-image:21.0.2').inside {
        sh 'mvn clean install --batch-mode --no-transfer-progress'
      }
    }
  }

  stage('Build frontend') {
    script {
      docker.image('node:20-alpine').inside {
          sh 'npm --prefix frontend/html install --legacy-peer-deps --cache=".CustomCacheHereDueToBug"'
          sh 'npm --prefix frontend/html run build:prod'
      }
    }
  }

  stage('Build web-image') {
    if(env.TEST_MODE != "true") {
      essDocker.build( project: "hjmtj", name: "web", version: version, commit: commit, url: buildUrl, file: "kubernetes/docker/web/Dockerfile", tag: buildTag )
    }
  }

  stage('Build backend-image') {
    if(env.TEST_MODE != "true") {
      essDocker.build( project: "hjmtj", name: "backend", version: version, commit: commit, url: buildUrl, file: "kubernetes/docker/backend/Dockerfile", tag: buildTag )
    }
  }

 stage('Build external-api-web-image') {
    if(env.TEST_MODE != "true") {
      essDocker.build( project: "hjmtj", name: "external-api-web", version: version, commit: commit, url: buildUrl, file: "kubernetes/docker/external-api-web/Dockerfile", tag: buildTag )
    }
  }

  stage('Build external-api-backend-image' ) {
    if(env.TEST_MODE != "true") {
      essDocker.build( project: "hjmtj", name: "external-api-backend", version: version, commit: commit, url: buildUrl, file: "kubernetes/docker/external-api-backend/Dockerfile", tag: buildTag )
    }
  }

  stage('Build export-image') {
    if(env.TEST_MODE != "true") {
      essDocker.build( project: "hjmtj", name: "export", version: version, commit: commit, url: buildUrl, file: "kubernetes/docker/export/Dockerfile", tag: buildTag )
    }
  }

  stage('Set tag on commit') {
    essGit.setTagOnCommit( tag: commitTag, commit: commit)
  }

  stage('Update job description') {
    currentBuild.description="Version: ${version} built from branch: ${buildBranch}"
  }
}
